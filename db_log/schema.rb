# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20190806034245) do

  create_table "_user_checkin_logs_ghc", id: :bigint, force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci" do |t|
    t.datetime "last_update",              default: -> { "CURRENT_TIMESTAMP" }, null: false
    t.string   "hint",        limit: 64,                                        null: false, collation: "ascii_general_ci"
    t.string   "value",       limit: 4096,                                      null: false, collation: "ascii_general_ci"
    t.index ["hint"], name: "hint_uidx", unique: true, using: :btree
  end

  create_table "_user_checkin_logs_gho", id: :string, limit: 36, force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci" do |t|
    t.integer  "user_id"
    t.integer  "present_time"
    t.datetime "created_at",                            null: false
    t.datetime "updated_at",                            null: false
    t.date     "checkin_date"
    t.string   "test_field",   limit: 256, default: ""
    t.string   "app_channel"
    t.string   "app_version"
    t.string   "app_channesl"
    t.index ["app_channel"], name: "index_user_checkin_logs_on_app_channel", using: :btree
    t.index ["app_version"], name: "index_user_checkin_logs_on_app_version", using: :btree
    t.index ["checkin_date"], name: "index_user_checkin_logs_on_checkin_date", using: :btree
    t.index ["id"], name: "index_user_checkin_logs_on_id", unique: true, using: :btree
    t.index ["user_id", "created_at"], name: "index_user_checkin_logs_on_user_id_and_created_at", unique: true, using: :btree
    t.index ["user_id"], name: "index_user_checkin_logs_on_user_id", using: :btree
  end

  create_table "active_user_day_stats", id: :string, limit: 36, force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb4" do |t|
    t.date     "stat_date"
    t.string   "app_channel"
    t.string   "app_version"
    t.integer  "users_count", default: 0
    t.datetime "created_at",              null: false
    t.datetime "updated_at",              null: false
    t.integer  "filter_type", default: 0
    t.integer  "stat_type",   default: 0
    t.index ["app_channel"], name: "index_active_user_day_stats_on_app_channel", using: :btree
    t.index ["app_version"], name: "index_active_user_day_stats_on_app_version", using: :btree
    t.index ["id"], name: "index_active_user_day_stats_on_id", unique: true, using: :btree
    t.index ["stat_date"], name: "index_active_user_day_stats_on_stat_date", using: :btree
  end

  create_table "active_user_hour_stats", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb4" do |t|
    t.date     "stat_date"
    t.integer  "stat_hour"
    t.decimal  "users_count", precision: 10, default: 0
    t.string   "app_channel"
    t.string   "app_version"
    t.integer  "filter_type",                default: 0
    t.datetime "created_at",                             null: false
    t.datetime "updated_at",                             null: false
    t.index ["stat_date", "stat_hour", "app_channel", "app_version", "filter_type"], name: "active_user_hour_stats_ssaaf", using: :btree
  end

  create_table "active_user_month_stats", id: :string, limit: 36, force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb4" do |t|
    t.integer  "stat_year"
    t.integer  "stat_month"
    t.string   "app_channel"
    t.string   "app_version"
    t.integer  "users_count", default: 0
    t.datetime "created_at",              null: false
    t.datetime "updated_at",              null: false
    t.integer  "filter_type", default: 0
    t.index ["app_channel"], name: "index_active_user_month_stats_on_app_channel", using: :btree
    t.index ["app_version"], name: "index_active_user_month_stats_on_app_version", using: :btree
    t.index ["id"], name: "index_active_user_month_stats_on_id", unique: true, using: :btree
    t.index ["stat_month"], name: "index_active_user_month_stats_on_stat_month", using: :btree
    t.index ["stat_year"], name: "index_active_user_month_stats_on_stat_year", using: :btree
  end

  create_table "activity_coin_reward_logs", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb4" do |t|
    t.integer  "admin_id"
    t.integer  "user_id"
    t.integer  "original_coins", default: 0
    t.integer  "current_coins",  default: 0
    t.integer  "operation_type", default: 0
    t.datetime "created_at",                 null: false
    t.datetime "updated_at",                 null: false
    t.index ["admin_id"], name: "index_activity_coin_reward_logs_on_admin_id", using: :btree
    t.index ["user_id"], name: "index_activity_coin_reward_logs_on_user_id", using: :btree
  end

  create_table "client_connection_logs", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb4" do |t|
    t.integer  "user_id"
    t.integer  "node_id"
    t.string   "node_ip"
    t.string   "target_url"
    t.string   "client_ip"
    t.string   "client_ip_country"
    t.string   "client_ip_province"
    t.string   "client_ip_city"
    t.boolean  "is_proxy"
    t.text     "addition_content",   limit: 65535
    t.datetime "created_at",                       null: false
    t.datetime "updated_at",                       null: false
    t.string   "uuid"
    t.string   "app_channel"
    t.string   "app_version"
    t.string   "app_version_number"
    t.index ["created_at"], name: "index_client_connection_logs_on_created_at", using: :btree
    t.index ["user_id"], name: "index_client_connection_logs_on_user_id", using: :btree
    t.index ["uuid"], name: "index_client_connection_logs_on_uuid", using: :btree
  end

  create_table "compensation_activity_logs", id: :string, limit: 36, force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci" do |t|
    t.integer  "user_id"
    t.integer  "promo_user_id"
    t.datetime "user_signup_at",                comment: "用户注册时间"
    t.datetime "user_recharge_at",              comment: "用户充值时间"
    t.integer  "node_type_id",                  comment: "套餐id"
    t.integer  "reward_days",                   comment: "奖励天数"
    t.datetime "created_at",       null: false
    t.datetime "updated_at",       null: false
    t.index ["promo_user_id"], name: "index_activity_logs_on_promo_user_id", using: :btree
    t.index ["user_id"], name: "index_activity_logs_on_user_id", using: :btree
  end

  create_table "comsunny_transaction_logs", id: :string, limit: 36, force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb4" do |t|
    t.integer  "user_id"
    t.bigint   "timestamp"
    t.string   "channel_type"
    t.string   "sub_channel_type"
    t.string   "transaction_type"
    t.string   "transaction_id"
    t.integer  "transaction_fee"
    t.boolean  "trade_success"
    t.text     "message_detail",   limit: 65535
    t.text     "optional",         limit: 65535
    t.datetime "created_at",                     null: false
    t.datetime "updated_at",                     null: false
    t.index ["created_at"], name: "index_comsunny_transaction_logs_on_created_at", using: :btree
    t.index ["id"], name: "index_comsunny_transaction_logs_on_id", unique: true, using: :btree
    t.index ["transaction_id"], name: "index_comsunny_transaction_logs_on_transaction_id", using: :btree
    t.index ["user_id"], name: "index_comsunny_transaction_logs_on_user_id", using: :btree
  end

  create_table "connection_failed_logs", id: :string, limit: 36, force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb4" do |t|
    t.integer  "fail_type",          default: 1
    t.string   "username"
    t.string   "device_uuid"
    t.string   "device_model"
    t.string   "node_type"
    t.string   "node_region"
    t.string   "node_name"
    t.string   "ip"
    t.string   "ip_country"
    t.string   "ip_province"
    t.string   "ip_city"
    t.string   "app_channel"
    t.string   "app_version"
    t.string   "app_version_number"
    t.string   "operator"
    t.datetime "created_at",                     null: false
    t.datetime "updated_at",                     null: false
    t.integer  "user_id"
    t.index ["created_at"], name: "index_connection_failed_logs_on_created_at", using: :btree
    t.index ["device_uuid"], name: "index_connection_failed_logs_on_device_uuid", using: :btree
    t.index ["id"], name: "index_connection_failed_logs_on_id", unique: true, using: :btree
    t.index ["username"], name: "index_connection_failed_logs_on_username", using: :btree
  end

  create_table "connection_failed_stats", id: :string, limit: 36, force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb4" do |t|
    t.string   "app_version"
    t.string   "app_channel"
    t.integer  "connection_failed_count", default: 0
    t.integer  "crash_count",             default: 0
    t.integer  "unconnected_count",       default: 0
    t.datetime "created_at",                              null: false
    t.datetime "updated_at",                              null: false
    t.date     "stat_date"
    t.boolean  "is_total",                default: false
    t.index ["app_channel"], name: "index_connection_failed_stats_on_app_channel", using: :btree
    t.index ["app_version"], name: "index_connection_failed_stats_on_app_version", using: :btree
    t.index ["created_at"], name: "index_connection_failed_stats_on_created_at", using: :btree
    t.index ["id"], name: "index_connection_failed_stats_on_id", unique: true, using: :btree
  end

  create_table "consumption_logs", id: :string, limit: 36, force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb4" do |t|
    t.integer  "user_id"
    t.string   "node_type_name"
    t.integer  "coins",           default: 0
    t.datetime "created_at",                      null: false
    t.datetime "updated_at",                      null: false
    t.integer  "node_type_id"
    t.string   "app_channel"
    t.string   "app_version"
    t.boolean  "is_regular_time", default: false
    t.integer  "time_type",       default: 0
    t.integer  "node_id"
    t.string   "node_name"
    t.string   "node_url"
    t.index ["created_at"], name: "index_consumption_logs_on_created_at", using: :btree
    t.index ["id"], name: "index_consumption_logs_on_id", unique: true, using: :btree
    t.index ["is_regular_time", "created_at"], name: "index_consumption_logs_on_is_regular_time_and_created_at", using: :btree
    t.index ["node_name", "node_url"], name: "index_consumption_logs_on_node_name_and_node_url", using: :btree
    t.index ["user_id"], name: "index_consumption_logs_on_user_id", using: :btree
  end

  create_table "dynamic_server_connection_failed_logs", id: :string, limit: 36, force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb4" do |t|
    t.integer  "dynamic_server_id"
    t.integer  "user_id"
    t.string   "app_channel"
    t.string   "app_version"
    t.string   "app_version_number"
    t.string   "device_model"
    t.string   "platform"
    t.string   "operator"
    t.string   "ip"
    t.string   "ip_country"
    t.string   "ip_province"
    t.string   "ip_city"
    t.datetime "created_at",         null: false
    t.datetime "updated_at",         null: false
    t.index ["created_at"], name: "index_dynamic_server_connection_failed_logs_on_created_at", using: :btree
    t.index ["id"], name: "index_dynamic_server_connection_failed_logs_on_id", unique: true, using: :btree
  end

  create_table "log_active_user_period_month_stats", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci" do |t|
    t.integer  "stat_year"
    t.integer  "stat_month"
    t.integer  "users_count",               default: 0
    t.text     "period",      limit: 65535
    t.string   "app_channel"
    t.string   "app_version"
    t.integer  "filter_type",               default: 0
    t.datetime "created_at",                            null: false
    t.datetime "updated_at",                            null: false
    t.index ["stat_year", "stat_month", "app_channel", "app_version", "filter_type"], name: "log_active_user_period_month_stats_ssaaf", using: :btree
  end

  create_table "log_navigation_ad_click_day_stats", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci" do |t|
    t.date     "stat_date"
    t.string   "ad_name"
    t.integer  "clicked_count"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
    t.index ["stat_date"], name: "index_log_navigation_ad_click_day_stats_on_stat_date", using: :btree
  end

  create_table "log_navigation_ad_click_logs", id: :bigint, force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci" do |t|
    t.integer  "user_id",            null: false
    t.string   "uuid"
    t.integer  "ad_id",              null: false
    t.string   "ad_name"
    t.string   "app_channel"
    t.string   "app_version"
    t.string   "app_version_number"
    t.date     "created_date"
    t.datetime "created_at",         null: false
    t.datetime "updated_at",         null: false
    t.index ["app_channel", "app_version", "created_at"], name: "lnacl_aac", using: :btree
    t.index ["app_channel"], name: "index_log_navigation_ad_click_logs_on_app_channel", using: :btree
    t.index ["app_version"], name: "index_log_navigation_ad_click_logs_on_app_version", using: :btree
    t.index ["created_date"], name: "index_log_navigation_ad_click_logs_on_created_date", using: :btree
    t.index ["user_id"], name: "index_log_navigation_ad_click_logs_on_user_id", using: :btree
  end

  create_table "log_non_active_user_day_stats", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci" do |t|
    t.date     "stat_date"
    t.integer  "users_count",           default: 0
    t.integer  "nonactive_users_count", default: 0
    t.string   "app_channel"
    t.string   "app_version"
    t.integer  "filter_type",           default: 0
    t.datetime "created_at",                        null: false
    t.datetime "updated_at",                        null: false
    t.index ["stat_date", "app_channel", "app_version", "filter_type"], name: "non_active_user_day_stats_saaf", using: :btree
  end

  create_table "log_operation_summary_day_stats", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci" do |t|
    t.date     "stat_date"
    t.decimal  "total_recharge_amount",      precision: 8, scale: 2, default: "0.0"
    t.integer  "new_users_count",                                    default: 0
    t.integer  "active_users_count",                                 default: 0
    t.integer  "paid_users_count",                                   default: 0
    t.decimal  "paid_total_price",           precision: 8, scale: 2, default: "0.0"
    t.integer  "seventh_paid_users_count",                           default: 0
    t.decimal  "seventh_paid_total_price",   precision: 8, scale: 2, default: "0.0"
    t.integer  "thirtieth_paid_users_count",                         default: 0
    t.decimal  "thirtieth_paid_total_price", precision: 8, scale: 2, default: "0.0"
    t.integer  "second_day"
    t.integer  "third_day"
    t.decimal  "second_day_rate",            precision: 8, scale: 2, default: "0.0"
    t.integer  "seventh_day"
    t.integer  "fifteen_day"
    t.integer  "thirtieth_day"
    t.integer  "seven_days_users_count"
    t.decimal  "seven_days_total_price",     precision: 8, scale: 2
    t.integer  "thirty_days_users_count"
    t.decimal  "thirty_days_total_price",    precision: 8, scale: 2
    t.integer  "platform_id"
    t.integer  "channel_account_id"
    t.integer  "package_id"
    t.string   "app_channel"
    t.string   "app_version"
    t.integer  "filter_type"
    t.datetime "created_at",                                                         null: false
    t.datetime "updated_at",                                                         null: false
    t.index ["app_channel"], name: "index_log_operation_summary_day_stats_on_app_channel", using: :btree
    t.index ["app_version"], name: "index_log_operation_summary_day_stats_on_app_version", using: :btree
    t.index ["stat_date"], name: "index_log_operation_summary_day_stats_on_stat_date", using: :btree
  end

  create_table "log_operation_summary_month_stats", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci" do |t|
    t.integer  "stat_year"
    t.integer  "stat_month"
    t.decimal  "total_recharge_amount", precision: 11, scale: 2, default: "0.0"
    t.integer  "new_users_count",                                default: 0
    t.integer  "active_users_count",                             default: 0
    t.integer  "platform_id"
    t.integer  "channel_account_id"
    t.integer  "package_id"
    t.string   "app_channel"
    t.string   "app_version"
    t.integer  "filter_type"
    t.datetime "created_at",                                                     null: false
    t.datetime "updated_at",                                                     null: false
    t.index ["app_channel"], name: "index_log_operation_summary_month_stats_on_app_channel", using: :btree
    t.index ["app_version"], name: "index_log_operation_summary_month_stats_on_app_version", using: :btree
    t.index ["channel_account_id"], name: "index_log_operation_summary_month_stats_on_channel_account_id", using: :btree
    t.index ["package_id"], name: "index_log_operation_summary_month_stats_on_package_id", using: :btree
    t.index ["platform_id"], name: "index_log_operation_summary_month_stats_on_platform_id", using: :btree
  end

  create_table "log_popup_ad_click_day_stats", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci" do |t|
    t.date     "stat_date"
    t.integer  "clicked_count", default: 0
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
    t.index ["stat_date"], name: "index_log_popup_ad_click_day_stats_on_stat_date", using: :btree
  end

  create_table "log_popup_ad_click_logs", id: :bigint, force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci" do |t|
    t.string   "uuid"
    t.string   "app_channel"
    t.string   "app_version"
    t.string   "app_version_number"
    t.date     "created_date"
    t.datetime "created_at",         null: false
    t.datetime "updated_at",         null: false
  end

  create_table "log_popup_ad_day_stats", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci" do |t|
    t.date     "stat_date"
    t.integer  "showed_count", default: 0
    t.datetime "created_at",               null: false
    t.datetime "updated_at",               null: false
    t.index ["stat_date"], name: "index_log_popup_ad_day_stats_on_stat_date", using: :btree
  end

  create_table "log_popup_ad_logs", id: :bigint, force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci" do |t|
    t.string   "uuid"
    t.string   "app_channel"
    t.string   "app_version"
    t.string   "app_version_number"
    t.date     "created_date"
    t.datetime "created_at",         null: false
    t.datetime "updated_at",         null: false
    t.index ["app_channel", "app_version", "created_at"], name: "lpadcl_aac", using: :btree
    t.index ["app_channel", "app_version", "created_at"], name: "lpal_aac", using: :btree
  end

  create_table "log_splash_screen_ad_click_hour_stats", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci" do |t|
    t.date     "stat_date"
    t.integer  "stat_hour"
    t.integer  "clicked_count", default: 0
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
    t.index ["stat_date", "stat_hour"], name: "lssachs_ss", using: :btree
  end

  create_table "log_splash_screen_ad_click_logs", id: :bigint, force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci" do |t|
    t.integer  "splash_screen_ad_id", null: false
    t.string   "uuid"
    t.string   "app_channel"
    t.string   "app_version"
    t.string   "app_version_number"
    t.date     "created_date"
    t.datetime "created_at",          null: false
    t.datetime "updated_at",          null: false
    t.index ["app_channel", "app_version", "created_at"], name: "ssadcl_aac", using: :btree
  end

  create_table "log_splash_screen_ad_hour_stats", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci" do |t|
    t.date     "stat_date"
    t.integer  "stat_hour"
    t.integer  "showed_count", default: 0
    t.datetime "created_at",               null: false
    t.datetime "updated_at",               null: false
    t.index ["stat_date", "stat_hour"], name: "lssahs_ss", using: :btree
  end

  create_table "log_splash_screen_ad_logs", id: :bigint, force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci" do |t|
    t.integer  "splash_screen_ad_id", null: false
    t.string   "uuid"
    t.string   "app_channel"
    t.string   "app_version"
    t.string   "app_version_number"
    t.date     "created_date"
    t.datetime "created_at",          null: false
    t.datetime "updated_at",          null: false
    t.index ["app_channel", "app_version", "created_at"], name: "ssadl_aac", using: :btree
  end

  create_table "log_user_change_logs", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci" do |t|
    t.integer  "user_id"
    t.integer  "change_type", default: 1
    t.string   "uuid"
    t.string   "platform"
    t.string   "ip"
    t.string   "before"
    t.string   "after"
    t.datetime "created_at",              null: false
    t.datetime "updated_at",              null: false
    t.index ["ip"], name: "index_log_user_change_logs_on_ip", using: :btree
    t.index ["user_id"], name: "index_log_user_change_logs_on_user_id", using: :btree
    t.index ["uuid"], name: "index_log_user_change_logs_on_uuid", using: :btree
  end

  create_table "log_user_checkin_day_stats", id: :string, limit: 36, force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci" do |t|
    t.date     "stat_date"
    t.string   "app_channel"
    t.string   "app_version"
    t.integer  "active_users_count",  default: 0
    t.integer  "checkin_users_count", default: 0
    t.integer  "filter_type",         default: 0
    t.datetime "created_at",                      null: false
    t.datetime "updated_at",                      null: false
    t.index ["app_channel"], name: "index_log_user_checkin_day_stats_on_app_channel", using: :btree
    t.index ["app_version"], name: "index_log_user_checkin_day_stats_on_app_version", using: :btree
    t.index ["id"], name: "index_log_user_checkin_day_stats_on_id", unique: true, using: :btree
    t.index ["stat_date"], name: "index_log_user_checkin_day_stats_on_stat_date", using: :btree
  end

  create_table "log_user_month_retention_rate_stats", id: :string, limit: 36, force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb4" do |t|
    t.date     "stat_date"
    t.string   "app_channel"
    t.string   "app_version"
    t.integer  "users_count",               default: 0
    t.integer  "first_month_users_count"
    t.integer  "second_month_users_count"
    t.integer  "third_month_users_count"
    t.integer  "fourth_month_users_count"
    t.integer  "fifth_month_users_count"
    t.integer  "sixth_month_users_count"
    t.integer  "seventh_month_users_count"
    t.integer  "eighth_month_users_count"
    t.integer  "ninth_month_users_count"
    t.integer  "filter_type",                                        unsigned: true
    t.datetime "created_at",                            null: false
    t.datetime "updated_at",                            null: false
    t.index ["app_channel"], name: "index_log_user_month_retention_rate_stats_on_app_channel", using: :btree
    t.index ["app_version"], name: "index_log_user_month_retention_rate_stats_on_app_version", using: :btree
    t.index ["id"], name: "index_log_user_month_retention_rate_stats_on_id", unique: true, using: :btree
    t.index ["stat_date"], name: "index_log_user_month_retention_rate_stats_on_stat_date", using: :btree
  end

  create_table "log_user_share_stat_logs", id: :string, limit: 36, force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci" do |t|
    t.date     "stat_date"
    t.string   "app_channel"
    t.string   "app_version"
    t.string   "platform_name"
    t.integer  "copy_link_count",           default: 0
    t.integer  "weixin_text_count",         default: 0
    t.integer  "friend_circle_text_count",  default: 0
    t.integer  "qq_text_count",             default: 0
    t.integer  "weibo_text_count",          default: 0
    t.integer  "weixin_image_count",        default: 0
    t.integer  "friend_circle_image_count", default: 0
    t.integer  "qq_image_count",            default: 0
    t.integer  "weibo_image_count",         default: 0
    t.integer  "filter_type",               default: 0
    t.datetime "created_at",                            null: false
    t.datetime "updated_at",                            null: false
    t.index ["app_channel"], name: "index_log_user_share_stat_logs_on_app_channel", using: :btree
    t.index ["app_version"], name: "index_log_user_share_stat_logs_on_app_version", using: :btree
    t.index ["id"], name: "index_log_user_share_stat_logs_on_id", unique: true, using: :btree
    t.index ["platform_name"], name: "index_log_user_share_stat_logs_on_platform_name", using: :btree
    t.index ["stat_date"], name: "index_log_user_share_stat_logs_on_stat_date", using: :btree
  end

  create_table "log_user_traffic_day_stats", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci" do |t|
    t.date     "stat_date"
    t.integer  "users_count",                  default: 0
    t.text     "traffic_period", limit: 65535
    t.string   "app_channel"
    t.string   "app_version"
    t.integer  "filter_type",                  default: 0
    t.datetime "created_at",                               null: false
    t.datetime "updated_at",                               null: false
    t.index ["stat_date", "app_channel", "app_version", "filter_type"], name: "log_user_traffic_day_stats_saaf", using: :btree
  end

  create_table "log_user_traffic_logs", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci" do |t|
    t.integer  "user_id"
    t.date     "stat_date"
    t.bigint   "used_bytes",  default: 0
    t.string   "app_channel"
    t.string   "app_version"
    t.datetime "created_at",              null: false
    t.datetime "updated_at",              null: false
    t.index ["app_channel"], name: "index_log_user_traffic_logs_on_app_channel", using: :btree
    t.index ["app_version"], name: "index_log_user_traffic_logs_on_app_version", using: :btree
    t.index ["stat_date", "user_id", "app_channel", "app_version"], name: "log_user_traffic_logs_suaa", using: :btree
    t.index ["stat_date", "user_id"], name: "index_log_user_traffic_logs_on_stat_date_and_user_id", using: :btree
    t.index ["stat_date"], name: "index_log_user_traffic_logs_on_stat_date", using: :btree
    t.index ["user_id"], name: "index_log_user_traffic_logs_on_user_id", using: :btree
  end

  create_table "log_user_week_retention_rate_stats", id: :string, limit: 36, force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb4" do |t|
    t.date     "stat_date"
    t.string   "app_channel"
    t.string   "app_version"
    t.integer  "users_count",              default: 0
    t.integer  "first_week_users_count"
    t.integer  "second_week_users_count"
    t.integer  "third_week_users_count"
    t.integer  "fourth_week_users_count"
    t.integer  "fifth_week_users_count"
    t.integer  "sixth_week_users_count"
    t.integer  "seventh_week_users_count"
    t.integer  "eighth_week_users_count"
    t.integer  "ninth_week_users_count"
    t.integer  "filter_type",              default: 0,              unsigned: true
    t.datetime "created_at",                           null: false
    t.datetime "updated_at",                           null: false
    t.index ["app_channel"], name: "index_log_user_week_retention_rate_stats_on_app_channel", using: :btree
    t.index ["app_version"], name: "index_log_user_week_retention_rate_stats_on_app_version", using: :btree
    t.index ["id"], name: "index_log_user_week_retention_rate_stats_on_id", unique: true, using: :btree
    t.index ["stat_date"], name: "index_log_user_week_retention_rate_stats_on_stat_date", using: :btree
  end

  create_table "log_website_navigation_click_day_stats", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci" do |t|
    t.date     "stat_date"
    t.string   "website_name"
    t.boolean  "is_domestic"
    t.integer  "info_type"
    t.integer  "clicked_count"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
    t.index ["stat_date"], name: "index_log_website_navigation_click_day_stats_on_stat_date", using: :btree
  end

  create_table "log_website_navigation_click_logs", id: :bigint, force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci" do |t|
    t.integer  "user_id",            null: false
    t.string   "uuid"
    t.integer  "website_id",         null: false
    t.string   "website_name"
    t.integer  "nav_type"
    t.integer  "info_type"
    t.boolean  "is_domestic"
    t.string   "app_channel"
    t.string   "app_version"
    t.string   "app_version_number"
    t.date     "created_date"
    t.datetime "created_at",         null: false
    t.datetime "updated_at",         null: false
    t.index ["app_channel", "app_version", "created_at"], name: "lwnc_aac", using: :btree
    t.index ["app_channel"], name: "index_log_website_navigation_click_logs_on_app_channel", using: :btree
    t.index ["app_version"], name: "index_log_website_navigation_click_logs_on_app_version", using: :btree
    t.index ["created_date"], name: "index_log_website_navigation_click_logs_on_created_date", using: :btree
    t.index ["user_id"], name: "index_log_website_navigation_click_logs_on_user_id", using: :btree
  end

  create_table "new_user_day_stats", id: :string, limit: 36, force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb4" do |t|
    t.date     "stat_date"
    t.string   "app_channel"
    t.string   "app_version"
    t.integer  "users_count", default: 0
    t.datetime "created_at",              null: false
    t.datetime "updated_at",              null: false
    t.integer  "filter_type", default: 0
    t.index ["app_channel"], name: "index_new_user_day_stats_on_app_channel", using: :btree
    t.index ["app_version"], name: "index_new_user_day_stats_on_app_version", using: :btree
    t.index ["id"], name: "index_new_user_day_stats_on_id", unique: true, using: :btree
    t.index ["stat_date"], name: "index_new_user_day_stats_on_stat_date", using: :btree
  end

  create_table "new_user_hour_stats", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb4" do |t|
    t.date     "stat_date"
    t.integer  "stat_hour"
    t.integer  "users_count", default: 0
    t.string   "app_channel"
    t.string   "app_version"
    t.integer  "filter_type", default: 0
    t.datetime "created_at",              null: false
    t.datetime "updated_at",              null: false
    t.index ["stat_date", "stat_hour", "app_channel", "app_version", "filter_type"], name: "new_user_hour_stats_ssaa", using: :btree
  end

  create_table "new_user_month_stats", id: :string, limit: 36, force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb4" do |t|
    t.integer  "stat_year"
    t.integer  "stat_month"
    t.string   "app_channel"
    t.string   "app_version"
    t.integer  "users_count", default: 0
    t.datetime "created_at",              null: false
    t.datetime "updated_at",              null: false
    t.integer  "filter_type", default: 0
    t.index ["app_channel"], name: "index_new_user_month_stats_on_app_channel", using: :btree
    t.index ["app_version"], name: "index_new_user_month_stats_on_app_version", using: :btree
    t.index ["id"], name: "index_new_user_month_stats_on_id", unique: true, using: :btree
    t.index ["stat_month"], name: "index_new_user_month_stats_on_stat_month", using: :btree
    t.index ["stat_year"], name: "index_new_user_month_stats_on_stat_year", using: :btree
  end

  create_table "node_connection_count_day_stats", id: :string, limit: 36, force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb4" do |t|
    t.date     "stat_date"
    t.integer  "user_id"
    t.integer  "users_count", default: 0
    t.string   "app_channel"
    t.string   "app_version"
    t.datetime "created_at",                  null: false
    t.datetime "updated_at",                  null: false
    t.boolean  "is_total",    default: false
    t.index ["app_channel"], name: "index_node_connection_count_day_stats_on_app_channel", using: :btree
    t.index ["app_version"], name: "index_node_connection_count_day_stats_on_app_version", using: :btree
    t.index ["id"], name: "index_node_connection_count_day_stats_on_id", unique: true, using: :btree
    t.index ["stat_date"], name: "index_node_connection_count_day_stats_on_stat_date", using: :btree
    t.index ["user_id"], name: "index_node_connection_count_day_stats_on_user_id", using: :btree
  end

  create_table "node_connection_count_scope_day_stats", id: :string, limit: 36, force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb4" do |t|
    t.date     "stat_date"
    t.integer  "users_count"
    t.integer  "count_1_2",   default: 0
    t.integer  "count_3_5",   default: 0
    t.integer  "count_6_9",   default: 0
    t.integer  "count_10_19", default: 0
    t.integer  "count_20_49", default: 0
    t.integer  "count_50",    default: 0
    t.string   "app_channel"
    t.string   "app_version"
    t.datetime "created_at",                  null: false
    t.datetime "updated_at",                  null: false
    t.boolean  "is_total",    default: false
    t.index ["app_channel"], name: "index_node_connection_count_scope_day_stats_on_app_channel", using: :btree
    t.index ["app_version"], name: "index_node_connection_count_scope_day_stats_on_app_version", using: :btree
    t.index ["id"], name: "index_node_connection_count_scope_day_stats_on_id", unique: true, using: :btree
    t.index ["stat_date"], name: "index_node_connection_count_scope_day_stats_on_stat_date", using: :btree
    t.index ["users_count"], name: "index_node_connection_count_scope_day_stats_on_users_count", using: :btree
  end

  create_table "node_connection_logs", id: :string, limit: 36, force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb4" do |t|
    t.integer  "user_id"
    t.integer  "node_type_id"
    t.integer  "node_region_id"
    t.integer  "node_id"
    t.string   "node_type_name"
    t.string   "node_region_name"
    t.string   "node_name"
    t.integer  "wait_time",          default: 0
    t.string   "ip"
    t.integer  "status",             default: 0
    t.datetime "created_at",                     null: false
    t.datetime "updated_at",                     null: false
    t.datetime "user_registered_at"
    t.string   "app_channel"
    t.string   "app_version"
    t.string   "ip_country"
    t.string   "ip_province"
    t.string   "ip_city"
    t.string   "domain"
    t.index ["created_at"], name: "index_node_connection_logs_on_created_at", using: :btree
    t.index ["id"], name: "index_node_connection_logs_on_id", unique: true, using: :btree
    t.index ["node_id"], name: "index_node_connection_logs_on_node_id", using: :btree
    t.index ["node_region_id"], name: "index_node_connection_logs_on_node_region_id", using: :btree
    t.index ["node_type_id"], name: "index_node_connection_logs_on_node_type_id", using: :btree
    t.index ["user_id", "user_registered_at", "app_channel", "app_version", "created_at"], name: "ncl_uuaac", using: :btree
  end

  create_table "node_connection_server_logs", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb4" do |t|
    t.integer  "user_id"
    t.integer  "node_id"
    t.string   "ip"
    t.boolean  "is_domestic"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
    t.index ["user_id", "created_at"], name: "index_node_connection_server_logs_on_user_id_and_created_at", using: :btree
  end

  create_table "node_connection_user_day_logs", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb4" do |t|
    t.date     "created_date"
    t.integer  "user_id"
    t.datetime "user_registered_at"
    t.string   "app_channel"
    t.string   "app_version"
    t.datetime "created_at",         null: false
    t.datetime "updated_at",         null: false
    t.index ["created_date", "user_id"], name: "index_node_connection_user_day_logs_on_created_date_and_user_id", unique: true, using: :btree
    t.index ["created_date", "user_registered_at", "app_channel", "app_version"], name: "node_connection_user_day_logs_cuaa", using: :btree
  end

  create_table "node_online_users", id: :string, limit: 36, force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb4" do |t|
    t.integer  "user_id"
    t.integer  "node_region_id"
    t.integer  "node_id"
    t.string   "ip"
    t.string   "ip_country"
    t.string   "ip_province"
    t.string   "ip_city"
    t.string   "ip_area"
    t.datetime "created_at",     null: false
    t.datetime "updated_at",     null: false
    t.index ["created_at"], name: "index_node_online_users_on_created_at", using: :btree
    t.index ["id"], name: "index_node_online_users_on_id", unique: true, using: :btree
    t.index ["node_id"], name: "index_node_online_users_on_node_id", using: :btree
    t.index ["node_region_id"], name: "index_node_online_users_on_node_region_id", using: :btree
    t.index ["user_id"], name: "index_node_online_users_on_user_id", using: :btree
  end

  create_table "node_switch_logs", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb4" do |t|
    t.integer  "node_id"
    t.integer  "status",                    default: 0
    t.string   "node_type_name"
    t.string   "node_ip"
    t.string   "robot_ip",       limit: 20
    t.datetime "created_at",                            null: false
    t.datetime "updated_at",                            null: false
    t.index ["node_id", "status", "created_at"], name: "index_node_switch_logs_on_node_id_and_status_and_created_at", using: :btree
  end

  create_table "node_switch_statuses", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb4" do |t|
    t.integer  "node_id"
    t.integer  "status",                default: 0
    t.integer  "total_failed_rate",     default: 0
    t.integer  "total_test_count",      default: 0
    t.integer  "average_response_time", default: 0
    t.integer  "last_failed_rate",      default: 0
    t.integer  "last_test_count",       default: 0
    t.string   "last_test_status"
    t.integer  "switch_on_count",       default: 0
    t.integer  "switch_off_count",      default: 0
    t.integer  "total_failed_count",    default: 0
    t.datetime "created_at",                        null: false
    t.datetime "updated_at",                        null: false
    t.index ["node_id", "created_at"], name: "index_node_switch_statuses_on_node_id_and_created_at", using: :btree
    t.index ["status"], name: "index_node_switch_statuses_on_status", using: :btree
    t.index ["total_failed_rate"], name: "index_node_switch_statuses_on_total_failed_rate", using: :btree
  end

  create_table "proxy_connection_failed_logs", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb4" do |t|
    t.integer  "user_id"
    t.string   "uuid"
    t.string   "node_id"
    t.string   "node_ip"
    t.string   "target_url"
    t.string   "client_ip"
    t.string   "client_ip_country"
    t.string   "client_ip_province"
    t.string   "client_ip_city"
    t.string   "operator"
    t.datetime "created_at",         null: false
    t.datetime "updated_at",         null: false
    t.index ["created_at"], name: "index_proxy_connection_failed_logs_on_created_at", using: :btree
  end

  create_table "proxy_connection_logs", id: :string, limit: 36, force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb4" do |t|
    t.integer  "user_id"
    t.integer  "node_id"
    t.string   "node_ip"
    t.text     "description", limit: 65535
    t.text     "data",        limit: 65535
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
    t.index ["created_at"], name: "index_proxy_connection_logs_on_created_at", using: :btree
    t.index ["id"], name: "index_proxy_connection_logs_on_id", unique: true, using: :btree
    t.index ["node_id"], name: "index_proxy_connection_logs_on_node_id", using: :btree
    t.index ["user_id"], name: "index_proxy_connection_logs_on_user_id", using: :btree
  end

  create_table "proxy_server_online_users_day_stats", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb4" do |t|
    t.date     "stat_date"
    t.integer  "min_count",                    default: 0
    t.integer  "max_count",                    default: 0
    t.integer  "average_count",                default: 0
    t.datetime "created_at",                               null: false
    t.datetime "updated_at",                               null: false
    t.datetime "min_time"
    t.datetime "max_time"
    t.text     "node_type_info", limit: 65535
    t.index ["stat_date"], name: "index_proxy_server_online_users_day_stats_on_stat_date", using: :btree
  end

  create_table "proxy_server_online_users_logs", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb4" do |t|
    t.integer  "node_id"
    t.integer  "online_users_count", default: 0
    t.datetime "created_at",                     null: false
    t.datetime "updated_at",                     null: false
    t.integer  "node_type_id"
    t.string   "ip"
    t.index ["created_at"], name: "index_proxy_server_online_users_logs_on_created_at", using: :btree
    t.index ["node_id"], name: "index_proxy_server_online_users_logs_on_node_id", using: :btree
  end

  create_table "proxy_server_status_day_stats", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb4" do |t|
    t.date     "stat_date"
    t.integer  "node_id"
    t.decimal  "min_bandwidth_percent",       precision: 6, scale: 3, default: "0.0"
    t.datetime "min_bandwidth_time"
    t.decimal  "max_bandwidth_percent",       precision: 6, scale: 3, default: "0.0"
    t.datetime "max_bandwidth_time"
    t.decimal  "average_bandwidth_percent",   precision: 6, scale: 3, default: "0.0"
    t.decimal  "min_cpu_percent",             precision: 4, scale: 1, default: "0.0"
    t.datetime "min_cpu_time"
    t.decimal  "max_cpu_percent",             precision: 4, scale: 1, default: "0.0"
    t.datetime "max_cpu_time"
    t.decimal  "average_cpu_percent",         precision: 4, scale: 1, default: "0.0"
    t.decimal  "min_memory_percent",          precision: 4, scale: 1, default: "0.0"
    t.datetime "min_memory_time"
    t.decimal  "max_memory_percent",          precision: 4, scale: 1, default: "0.0"
    t.datetime "max_memory_time"
    t.decimal  "average_memory_percent",      precision: 4, scale: 1, default: "0.0"
    t.integer  "min_network_speed_up",                                default: 0
    t.datetime "min_network_speed_up_time"
    t.integer  "max_network_speed_up",                                default: 0
    t.datetime "max_network_speed_up_time"
    t.integer  "average_network_speed_up",                            default: 0
    t.integer  "min_network_speed_down",                              default: 0
    t.datetime "min_network_speed_down_time"
    t.integer  "max_network_speed_down",                              default: 0
    t.datetime "max_network_speed_down_time"
    t.integer  "average_network_speed_down",                          default: 0
    t.decimal  "min_disk_percent",            precision: 4, scale: 1, default: "0.0"
    t.datetime "min_disk_time"
    t.decimal  "max_disk_percent",            precision: 4, scale: 1, default: "0.0"
    t.datetime "max_disk_time"
    t.decimal  "average_disk_percent",        precision: 4, scale: 1, default: "0.0"
    t.datetime "created_at",                                                          null: false
    t.datetime "updated_at",                                                          null: false
    t.index ["node_id"], name: "index_proxy_server_status_day_stats_on_node_id", using: :btree
    t.index ["stat_date"], name: "index_proxy_server_status_day_stats_on_stat_date", using: :btree
  end

  create_table "proxy_server_status_logs", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb4" do |t|
    t.integer  "node_id"
    t.decimal  "bandwidth_percent",  precision: 8, scale: 5, default: "0.0"
    t.decimal  "cpu_percent",        precision: 4, scale: 1, default: "0.0"
    t.decimal  "memory_percent",     precision: 4, scale: 1, default: "0.0"
    t.bigint   "transfer",                                   default: 0
    t.bigint   "network_speed_up",                           default: 0
    t.bigint   "network_speed_down",                         default: 0
    t.integer  "disk_percent",                               default: 0
    t.datetime "created_at",                                                 null: false
    t.datetime "updated_at",                                                 null: false
    t.index ["created_at"], name: "index_proxy_server_status_logs_on_created_at", using: :btree
    t.index ["node_id"], name: "index_proxy_server_status_logs_on_node_id", using: :btree
  end

  create_table "proxy_test_domain_logs", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb4" do |t|
    t.integer  "user_id"
    t.integer  "node_id"
    t.integer  "proxy_test_domain_id"
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
    t.boolean  "is_successed",         default: true
    t.index ["node_id"], name: "index_proxy_test_domain_logs_on_node_id", using: :btree
    t.index ["proxy_test_domain_id"], name: "index_proxy_test_domain_logs_on_proxy_test_domain_id", using: :btree
    t.index ["user_id"], name: "index_proxy_test_domain_logs_on_user_id", using: :btree
  end

  create_table "transaction_hour_stats", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb4" do |t|
    t.date     "stat_date"
    t.integer  "stat_hour"
    t.decimal  "total_recharge_amount", precision: 8, scale: 2, default: "0.0"
    t.integer  "recharge_users_count",                          default: 0
    t.string   "app_channel"
    t.string   "app_version"
    t.integer  "filter_type",                                   default: 0
    t.datetime "created_at",                                                    null: false
    t.datetime "updated_at",                                                    null: false
    t.index ["stat_date", "stat_hour", "app_channel", "app_version", "filter_type"], name: "transaction_hour_stats_ssaaf", using: :btree
  end

  create_table "transaction_logs", id: :string, limit: 36, force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb4" do |t|
    t.string   "order_number"
    t.integer  "user_id"
    t.integer  "plan_id"
    t.decimal  "price",                   precision: 8, scale: 2, default: "0.0"
    t.integer  "coins",                                           default: 0
    t.integer  "payment_method",                                  default: 0
    t.string   "status"
    t.datetime "processed_at"
    t.datetime "created_at",                                                      null: false
    t.datetime "updated_at",                                                      null: false
    t.string   "platform_transaction_id"
    t.string   "app_channel"
    t.string   "app_version"
    t.boolean  "is_regular_time",                                 default: false
    t.integer  "time_type",                                       default: 0
    t.integer  "node_type_id"
    t.string   "plan_name"
    t.integer  "plan_coins",                                      default: 0
    t.integer  "present_coins",                                   default: 0
    t.datetime "user_created_at"
    t.index ["app_channel", "app_version", "status", "user_created_at", "created_at"], name: "transaction_logs_aasuc", using: :btree
    t.index ["created_at", "status"], name: "index_transaction_logs_on_created_at_and_status", using: :btree
    t.index ["created_at"], name: "index_transaction_logs_on_created_at", using: :btree
    t.index ["id"], name: "index_transaction_logs_on_id", unique: true, using: :btree
    t.index ["order_number"], name: "index_transaction_logs_on_order_number", unique: true, using: :btree
    t.index ["plan_id"], name: "index_transaction_logs_on_plan_id", using: :btree
    t.index ["status", "user_created_at", "created_at"], name: "transaction_logs_suc", using: :btree
    t.index ["status"], name: "index_transaction_logs_on_status", using: :btree
    t.index ["user_id", "created_at"], name: "index_transaction_logs_on_user_id_and_created_at", using: :btree
    t.index ["user_id"], name: "index_transaction_logs_on_user_id", using: :btree
  end

  create_table "transaction_month_stats", id: :string, limit: 36, force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb4" do |t|
    t.integer  "year"
    t.integer  "month"
    t.decimal  "total_recharge_amount", precision: 12, scale: 2, default: "0.0"
    t.integer  "consume_coins",                                  default: 0
    t.integer  "recharge_users_count",                           default: 0
    t.datetime "created_at",                                                     null: false
    t.datetime "updated_at",                                                     null: false
    t.string   "app_channel"
    t.string   "app_version"
    t.integer  "renew_users_count",                              default: 0
    t.integer  "renew_new_users_count",                          default: 0
    t.decimal  "renew_amount",          precision: 12, scale: 2, default: "0.0"
    t.decimal  "renew_new_amount",      precision: 12, scale: 2, default: "0.0"
    t.integer  "filter_type",                                    default: 0
    t.index ["created_at"], name: "index_transaction_month_stats_on_created_at", using: :btree
    t.index ["id"], name: "index_transaction_month_stats_on_id", unique: true, using: :btree
    t.index ["month"], name: "index_transaction_month_stats_on_month", using: :btree
    t.index ["year", "app_channel", "app_version", "filter_type"], name: "transaction_month_stats_saaf", using: :btree
    t.index ["year"], name: "index_transaction_month_stats_on_year", using: :btree
  end

  create_table "transaction_payment_failed_logs", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb4" do |t|
    t.string   "order_number"
    t.integer  "user_id"
    t.string   "platform"
    t.string   "app_channel"
    t.string   "app_version"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
  end

  create_table "transaction_stats", id: :string, limit: 36, force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb4" do |t|
    t.date     "stat_date"
    t.decimal  "total_recharge_amount", precision: 8,  scale: 2, default: "0.0"
    t.integer  "consume_coins",                                  default: 0
    t.integer  "recharge_users_count",                           default: 0
    t.datetime "created_at",                                                     null: false
    t.datetime "updated_at",                                                     null: false
    t.string   "app_channel"
    t.string   "app_version"
    t.integer  "renew_users_count",                              default: 0
    t.integer  "renew_new_users_count",                          default: 0
    t.decimal  "renew_amount",          precision: 12, scale: 2, default: "0.0"
    t.decimal  "renew_new_amount",      precision: 12, scale: 2, default: "0.0"
    t.integer  "filter_type",                                    default: 0
    t.index ["created_at"], name: "index_transaction_stats_on_created_at", using: :btree
    t.index ["id"], name: "index_transaction_stats_on_id", unique: true, using: :btree
    t.index ["stat_date", "app_channel", "app_version", "filter_type"], name: "transaction_stats_saaf", using: :btree
    t.index ["stat_date"], name: "index_transaction_stats_on_stat_date", using: :btree
  end

  create_table "transaction_type_day_stats", id: :bigint, force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci" do |t|
    t.date     "stat_date"
    t.integer  "users_count",                    default: 0
    t.text     "type_users_count", limit: 65535
    t.string   "app_channel"
    t.string   "app_version"
    t.integer  "filter_type",                    default: 0
    t.datetime "created_at",                                 null: false
    t.datetime "updated_at",                                 null: false
    t.index ["stat_date", "app_channel", "app_version"], name: "transaction_type_day_stats_saa", using: :btree
    t.index ["stat_date"], name: "index_transaction_type_day_stats_on_stat_date", using: :btree
  end

  create_table "user_account_status_logs", id: :string, limit: 36, force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb4" do |t|
    t.integer  "admin_id"
    t.string   "admin_username"
    t.integer  "user_id"
    t.integer  "operation_type",               default: 0
    t.text     "description",    limit: 65535
    t.datetime "created_at",                               null: false
    t.datetime "updated_at",                               null: false
    t.index ["admin_id"], name: "index_user_account_status_logs_on_admin_id", using: :btree
    t.index ["id"], name: "index_user_account_status_logs_on_id", unique: true, using: :btree
    t.index ["user_id"], name: "index_user_account_status_logs_on_user_id", using: :btree
  end

  create_table "user_active_hour_logs", id: :bigint, force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb4" do |t|
    t.date     "created_date"
    t.integer  "created_hour"
    t.integer  "user_id"
    t.string   "app_version"
    t.string   "app_channel"
    t.integer  "active_type",  default: 0
    t.datetime "created_at",               null: false
    t.datetime "updated_at",               null: false
    t.index ["created_date", "created_hour", "app_channel", "app_version"], name: "user_signin_hour_logs_ccaa", using: :btree
    t.index ["created_date", "created_hour", "user_id"], name: "user_signin_hour_logs_ccu", unique: true, using: :btree
    t.index ["user_id"], name: "index_user_active_hour_logs_on_user_id", using: :btree
  end

  create_table "user_app_launch_logs", id: :string, limit: 36, force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb4" do |t|
    t.integer  "user_id"
    t.integer  "device_id"
    t.string   "device_name"
    t.string   "device_model"
    t.string   "platform"
    t.string   "system_version"
    t.string   "operator"
    t.string   "net_env"
    t.string   "app_version"
    t.string   "app_version_number"
    t.string   "app_channel"
    t.string   "ip"
    t.decimal  "longitude",          precision: 16, scale: 8, default: "0.0"
    t.decimal  "latitude",           precision: 16, scale: 8, default: "0.0"
    t.string   "address"
    t.string   "coord_method"
    t.datetime "created_at",                                                  null: false
    t.datetime "updated_at",                                                  null: false
    t.index ["created_at"], name: "index_user_app_launch_logs_on_created_at", using: :btree
    t.index ["device_id"], name: "index_user_app_launch_logs_on_device_id", using: :btree
    t.index ["id"], name: "index_user_app_launch_logs_on_id", unique: true, using: :btree
    t.index ["user_id"], name: "index_user_app_launch_logs_on_user_id", using: :btree
  end

  create_table "user_area_stats", id: :string, limit: 36, force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci" do |t|
    t.string   "app_version"
    t.string   "app_channel"
    t.string   "country"
    t.string   "province"
    t.boolean  "is_domestic"
    t.integer  "users_count"
    t.integer  "filter_type"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
    t.index ["id"], name: "index_user_area_stats_on_id", unique: true, using: :btree
  end

  create_table "user_checkin_logs", id: :string, limit: 36, force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci" do |t|
    t.integer  "user_id"
    t.integer  "present_time"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
    t.date     "checkin_date"
    t.string   "app_channel"
    t.string   "app_version"
    t.index ["app_channel"], name: "index_user_checkin_logs_on_app_channel", using: :btree
    t.index ["app_version"], name: "index_user_checkin_logs_on_app_version", using: :btree
    t.index ["checkin_date"], name: "index_user_checkin_logs_on_checkin_date", using: :btree
    t.index ["id"], name: "index_user_checkin_logs_on_id", unique: true, using: :btree
    t.index ["user_id", "created_at"], name: "index_user_checkin_logs_on_user_id_and_created_at", unique: true, using: :btree
    t.index ["user_id"], name: "index_user_checkin_logs_on_user_id", using: :btree
  end

  create_table "user_connection_first_day_stats", id: :string, limit: 36, force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb4" do |t|
    t.integer  "user_id"
    t.string   "domain"
    t.integer  "visits_count", default: 0
    t.date     "stat_date"
    t.datetime "created_at",               null: false
    t.datetime "updated_at",               null: false
    t.index ["domain", "user_id", "visits_count", "stat_date"], name: "iucfds_four_index", using: :btree
    t.index ["id"], name: "index_user_connection_first_day_stats_on_id", unique: true, using: :btree
  end

  create_table "user_connection_first_month_stats", id: :string, limit: 36, force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb4" do |t|
    t.integer  "user_id"
    t.string   "domain"
    t.integer  "visits_count", default: 0
    t.integer  "stat_year"
    t.integer  "stat_month"
    t.datetime "created_at",               null: false
    t.datetime "updated_at",               null: false
    t.index ["created_at"], name: "index_user_connection_first_month_stats_on_created_at", using: :btree
    t.index ["domain"], name: "index_user_connection_first_month_stats_on_domain", using: :btree
    t.index ["id"], name: "index_user_connection_first_month_stats_on_id", unique: true, using: :btree
    t.index ["stat_month"], name: "index_user_connection_first_month_stats_on_stat_month", using: :btree
    t.index ["stat_year"], name: "index_user_connection_first_month_stats_on_stat_year", using: :btree
    t.index ["user_id"], name: "index_user_connection_first_month_stats_on_user_id", using: :btree
  end

  create_table "user_connection_first_year_stats", id: :string, limit: 36, force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb4" do |t|
    t.integer  "user_id"
    t.string   "domain"
    t.integer  "visits_count", default: 0
    t.integer  "stat_year"
    t.datetime "created_at",               null: false
    t.datetime "updated_at",               null: false
    t.index ["created_at"], name: "index_user_connection_first_year_stats_on_created_at", using: :btree
    t.index ["domain"], name: "index_user_connection_first_year_stats_on_domain", using: :btree
    t.index ["id"], name: "index_user_connection_first_year_stats_on_id", unique: true, using: :btree
    t.index ["stat_year"], name: "index_user_connection_first_year_stats_on_stat_year", using: :btree
    t.index ["user_id"], name: "index_user_connection_first_year_stats_on_user_id", using: :btree
  end

  create_table "user_connection_logs", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb4" do |t|
    t.integer  "user_id"
    t.integer  "session_id"
    t.integer  "node_region_id"
    t.integer  "node_id"
    t.string   "target_address"
    t.string   "target_ip"
    t.string   "target_domain"
    t.integer  "in_bytes"
    t.integer  "out_bytes"
    t.integer  "cost_time"
    t.datetime "created_at",                         null: false
    t.datetime "updated_at",                         null: false
    t.string   "domain_description"
    t.string   "top_level_domain"
    t.string   "client_ip"
    t.string   "ip_country"
    t.string   "ip_province"
    t.string   "ip_city"
    t.string   "first_proxy_ip"
    t.boolean  "is_collected",       default: false
    t.index ["created_at"], name: "index_user_connection_logs_on_created_at", using: :btree
    t.index ["is_collected"], name: "index_user_connection_logs_on_is_collected", using: :btree
    t.index ["user_id", "created_at"], name: "index_user_connection_logs_on_user_id_and_created_at", using: :btree
    t.index ["user_id"], name: "index_user_connection_logs_on_user_id", using: :btree
  end

  create_table "user_connection_logs_error", unsigned: true, force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin" do |t|
    t.datetime "oper_date"
    t.string   "original_data", limit: 2048, default: ""
    t.string   "error_str",     limit: 512,  default: ""
  end

  create_table "user_connection_region_stats", id: :string, limit: 36, force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb4" do |t|
    t.integer  "user_id"
    t.string   "domain"
    t.string   "ip"
    t.string   "ip_country"
    t.string   "ip_province"
    t.string   "ip_city"
    t.integer  "visits_count", default: 0
    t.datetime "created_at",               null: false
    t.datetime "updated_at",               null: false
    t.index ["created_at"], name: "index_user_connection_region_stats_on_created_at", using: :btree
    t.index ["domain"], name: "index_user_connection_region_stats_on_domain", using: :btree
    t.index ["id"], name: "index_user_connection_region_stats_on_id", unique: true, using: :btree
    t.index ["ip_city"], name: "index_user_connection_region_stats_on_ip_city", using: :btree
    t.index ["ip_country"], name: "index_user_connection_region_stats_on_ip_country", using: :btree
    t.index ["ip_province"], name: "index_user_connection_region_stats_on_ip_province", using: :btree
    t.index ["user_id"], name: "index_user_connection_region_stats_on_user_id", using: :btree
  end

  create_table "user_connection_second_day_stats", id: :string, limit: 36, force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb4" do |t|
    t.string   "domain"
    t.integer  "visit_users_count", default: 0
    t.integer  "visits_count",      default: 0
    t.date     "stat_date"
    t.datetime "created_at",                    null: false
    t.datetime "updated_at",                    null: false
    t.index ["created_at"], name: "index_user_connection_second_day_stats_on_created_at", using: :btree
    t.index ["domain"], name: "index_user_connection_second_day_stats_on_domain", using: :btree
    t.index ["id"], name: "index_user_connection_second_day_stats_on_id", unique: true, using: :btree
    t.index ["stat_date"], name: "index_user_connection_second_day_stats_on_stat_date", using: :btree
  end

  create_table "user_connection_second_month_stats", id: :string, limit: 36, force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb4" do |t|
    t.string   "domain"
    t.integer  "visit_users_count", default: 0
    t.integer  "visits_count",      default: 0
    t.integer  "stat_year"
    t.integer  "stat_month"
    t.datetime "created_at",                    null: false
    t.datetime "updated_at",                    null: false
    t.index ["created_at"], name: "index_user_connection_second_month_stats_on_created_at", using: :btree
    t.index ["domain"], name: "index_user_connection_second_month_stats_on_domain", using: :btree
    t.index ["id"], name: "index_user_connection_second_month_stats_on_id", unique: true, using: :btree
    t.index ["stat_month"], name: "index_user_connection_second_month_stats_on_stat_month", using: :btree
    t.index ["stat_year"], name: "index_user_connection_second_month_stats_on_stat_year", using: :btree
  end

  create_table "user_connection_second_year_stats", id: :string, limit: 36, force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb4" do |t|
    t.string   "domain"
    t.integer  "visit_users_count", default: 0
    t.integer  "visits_count",      default: 0
    t.integer  "stat_year"
    t.datetime "created_at",                    null: false
    t.datetime "updated_at",                    null: false
    t.index ["created_at"], name: "index_user_connection_second_year_stats_on_created_at", using: :btree
    t.index ["domain"], name: "index_user_connection_second_year_stats_on_domain", using: :btree
    t.index ["id"], name: "index_user_connection_second_year_stats_on_id", unique: true, using: :btree
    t.index ["stat_year"], name: "index_user_connection_second_year_stats_on_stat_year", using: :btree
  end

  create_table "user_expired_reward_logs", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci" do |t|
    t.integer  "admin_id"
    t.integer  "user_id"
    t.datetime "original_expired_at"
    t.datetime "current_expired_at"
    t.integer  "user_node_type_id"
    t.integer  "node_type_id"
    t.integer  "operation_type",      default: 0
    t.datetime "created_at",                      null: false
    t.datetime "updated_at",                      null: false
    t.index ["admin_id"], name: "index_user_expired_reward_logs_on_admin_id", using: :btree
    t.index ["node_type_id"], name: "index_user_expired_reward_logs_on_node_type_id", using: :btree
    t.index ["user_id"], name: "index_user_expired_reward_logs_on_user_id", using: :btree
    t.index ["user_node_type_id"], name: "index_user_expired_reward_logs_on_user_node_type_id", using: :btree
  end

  create_table "user_hardware_stats", id: :string, limit: 36, force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb4" do |t|
    t.string   "app_version"
    t.string   "app_channel"
    t.string   "device_model"
    t.string   "platform"
    t.integer  "quantity"
    t.datetime "created_at",               null: false
    t.datetime "updated_at",               null: false
    t.integer  "filter_type",  default: 0
    t.index ["id"], name: "index_user_hardware_stats_on_id", unique: true, using: :btree
  end

  create_table "user_iap_verification_logs", id: :string, limit: 36, force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb4" do |t|
    t.integer  "user_id"
    t.string   "iap_id"
    t.text     "receipt_data",   limit: 65535
    t.integer  "status"
    t.text     "result",         limit: 65535
    t.string   "ip"
    t.datetime "created_at",                   null: false
    t.datetime "updated_at",                   null: false
    t.string   "transaction_id"
    t.index ["user_id"], name: "index_user_iap_verification_logs_on_user_id", using: :btree
  end

  create_table "user_no_operation_logs", id: :string, limit: 36, force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb4" do |t|
    t.string   "username"
    t.string   "device_uuid"
    t.string   "device_model"
    t.string   "ip"
    t.string   "ip_country"
    t.string   "ip_province"
    t.string   "ip_city"
    t.string   "app_channel"
    t.string   "app_version"
    t.string   "app_version_number"
    t.string   "operator"
    t.datetime "created_at",         null: false
    t.datetime "updated_at",         null: false
    t.integer  "user_id"
    t.index ["created_at"], name: "index_user_no_operation_logs_on_created_at", using: :btree
    t.index ["device_uuid"], name: "index_user_no_operation_logs_on_device_uuid", using: :btree
    t.index ["id"], name: "index_user_no_operation_logs_on_id", unique: true, using: :btree
    t.index ["username"], name: "index_user_no_operation_logs_on_username", using: :btree
  end

  create_table "user_no_operation_stats", id: :string, limit: 36, force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb4" do |t|
    t.date     "stat_date"
    t.string   "app_version"
    t.string   "app_channel"
    t.integer  "no_operations_count", default: 0
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
    t.boolean  "is_total",            default: false
  end

  create_table "user_operation_day_stats", id: :string, limit: 36, force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb4" do |t|
    t.date     "stat_date"
    t.string   "app_channel"
    t.string   "app_version"
    t.integer  "users_count",  default: 0
    t.integer  "visits_count", default: 0
    t.datetime "created_at",                   null: false
    t.datetime "updated_at",                   null: false
    t.boolean  "is_total",     default: false
    t.integer  "interface_id"
    t.index ["app_channel"], name: "index_user_operation_day_stats_on_app_channel", using: :btree
    t.index ["app_version"], name: "index_user_operation_day_stats_on_app_version", using: :btree
    t.index ["id"], name: "index_user_operation_day_stats_on_id", unique: true, using: :btree
    t.index ["stat_date"], name: "index_user_operation_day_stats_on_stat_date", using: :btree
  end

  create_table "user_operation_logs", id: :string, limit: 36, force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb4" do |t|
    t.integer  "user_id"
    t.integer  "device_id"
    t.string   "app_channel"
    t.string   "app_version"
    t.string   "app_version_number"
    t.string   "user_app_launch_log_id", limit: 36
    t.string   "user_signin_log_id",     limit: 36
    t.integer  "interface_id"
    t.datetime "created_at",                        null: false
    t.datetime "updated_at",                        null: false
    t.index ["created_at"], name: "index_user_operation_logs_on_created_at", using: :btree
    t.index ["id"], name: "index_user_operation_logs_on_id", unique: true, using: :btree
    t.index ["interface_id", "user_id", "app_channel", "app_version", "created_at"], name: "iuol_iuaac", using: :btree
    t.index ["user_id", "created_at"], name: "index_user_operation_logs_on_user_id_and_created_at", using: :btree
  end

  create_table "user_operator_stats", id: :string, limit: 36, force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb4" do |t|
    t.string   "app_version"
    t.string   "app_channel"
    t.string   "operator"
    t.integer  "quantity"
    t.datetime "created_at",              null: false
    t.datetime "updated_at",              null: false
    t.integer  "filter_type", default: 0
    t.index ["id"], name: "index_user_operator_stats_on_id", unique: true, using: :btree
  end

  create_table "user_promo_logs", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb4" do |t|
    t.integer  "promoter_id"
    t.string   "promo_code"
    t.integer  "accepted_user_id"
    t.integer  "promoter_coins"
    t.integer  "accepted_user_coins"
    t.datetime "created_at",          null: false
    t.datetime "updated_at",          null: false
    t.index ["accepted_user_id"], name: "index_user_promo_logs_on_accepted_user_id", using: :btree
    t.index ["created_at"], name: "index_user_promo_logs_on_created_at", using: :btree
    t.index ["promoter_id"], name: "index_user_promo_logs_on_promoter_id", using: :btree
  end

  create_table "user_proxy_logs", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci" do |t|
    t.integer  "user_id"
    t.date     "used_date"
    t.bigint   "total_bytes"
    t.string   "last_url"
    t.string   "app_channel"
    t.string   "app_version"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
    t.index ["used_date", "total_bytes", "app_channel", "app_version"], name: "user_proxy_logs_utaa", using: :btree
    t.index ["user_id", "used_date"], name: "index_user_proxy_logs_on_user_id_and_used_date", unique: true, using: :btree
  end

  create_table "user_retention_rate_stats", id: :string, limit: 36, force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb4" do |t|
    t.date     "stat_date"
    t.string   "app_channel"
    t.string   "app_version"
    t.integer  "users_count",                                         default: 0
    t.integer  "second_day"
    t.integer  "third_day"
    t.integer  "seventh_day"
    t.integer  "fifteen_day"
    t.integer  "thirtieth_day"
    t.datetime "created_at",                                                          null: false
    t.datetime "updated_at",                                                          null: false
    t.integer  "filter_type",                                         default: 0
    t.integer  "paid_users_count",                                    default: 0
    t.decimal  "total_price",                precision: 12, scale: 2, default: "0.0"
    t.integer  "second_paid_users_count"
    t.decimal  "second_total_price",         precision: 12, scale: 2
    t.integer  "seventh_paid_users_count"
    t.decimal  "seventh_total_price",        precision: 12, scale: 2
    t.integer  "thirtieth_paid_users_count"
    t.decimal  "thirtieth_total_price",      precision: 12, scale: 2
    t.integer  "seventh_users_count"
    t.integer  "thirtieth_users_count"
    t.decimal  "seventh_17_total_price",     precision: 12, scale: 2
    t.decimal  "thirtieth_130_total_price",  precision: 12, scale: 2
    t.index ["app_channel"], name: "index_user_retention_rate_stats_on_app_channel", using: :btree
    t.index ["app_version"], name: "index_user_retention_rate_stats_on_app_version", using: :btree
    t.index ["id"], name: "index_user_retention_rate_stats_on_id", unique: true, using: :btree
    t.index ["stat_date"], name: "index_user_retention_rate_stats_on_stat_date", using: :btree
  end

  create_table "user_share_logs", id: :string, limit: 36, force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb4" do |t|
    t.integer  "user_id"
    t.integer  "share_type",         default: 0
    t.string   "platform_name"
    t.string   "app_channel"
    t.string   "app_version"
    t.string   "app_version_number"
    t.datetime "created_at",                     null: false
    t.datetime "updated_at",                     null: false
    t.index ["app_channel"], name: "index_user_share_logs_on_app_channel", using: :btree
    t.index ["app_version"], name: "index_user_share_logs_on_app_version", using: :btree
    t.index ["app_version_number"], name: "index_user_share_logs_on_app_version_number", using: :btree
    t.index ["created_at"], name: "index_user_share_logs_on_created_at", using: :btree
    t.index ["id"], name: "index_user_share_logs_on_id", unique: true, using: :btree
    t.index ["share_type"], name: "index_user_share_logs_on_share_type", using: :btree
    t.index ["user_id"], name: "index_user_share_logs_on_user_id", using: :btree
  end

  create_table "user_share_stats", id: :string, limit: 36, force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb4" do |t|
    t.string   "app_version"
    t.string   "app_channel"
    t.integer  "share_type"
    t.integer  "users_count"
    t.integer  "times_count"
    t.datetime "created_at",              null: false
    t.datetime "updated_at",              null: false
    t.integer  "filter_type", default: 0
    t.index ["id"], name: "index_user_share_stats_on_id", unique: true, using: :btree
  end

  create_table "user_signin_failed_logs", id: :string, limit: 36, force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb4" do |t|
    t.string   "username"
    t.string   "device_uuid", limit: 40
    t.integer  "fail_type",              default: 0
    t.string   "ip"
    t.datetime "created_at",                         null: false
    t.datetime "updated_at",                         null: false
    t.integer  "user_id"
    t.index ["created_at"], name: "index_user_signin_failed_logs_on_created_at", using: :btree
    t.index ["device_uuid"], name: "index_user_signin_failed_logs_on_device_uuid", using: :btree
    t.index ["fail_type"], name: "index_user_signin_failed_logs_on_fail_type", using: :btree
    t.index ["id"], name: "index_user_signin_failed_logs_on_id", unique: true, using: :btree
    t.index ["username"], name: "index_user_signin_failed_logs_on_username", using: :btree
  end

  create_table "user_signin_failed_stats", id: :string, limit: 36, force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb4" do |t|
    t.date     "stat_date"
    t.integer  "not_exist_count",         default: 0
    t.integer  "wrong_password_count",    default: 0
    t.integer  "failed_connection_count", default: 0
    t.integer  "crash_count",             default: 0
    t.datetime "created_at",                              null: false
    t.datetime "updated_at",                              null: false
    t.boolean  "is_total",                default: false
    t.index ["id"], name: "index_user_signin_failed_stats_on_id", unique: true, using: :btree
    t.index ["stat_date"], name: "index_user_signin_failed_stats_on_stat_date", using: :btree
  end

  create_table "user_signin_logs", id: :string, limit: 36, force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb4" do |t|
    t.integer  "user_id"
    t.integer  "session_id"
    t.integer  "device_id"
    t.string   "device_name"
    t.string   "device_model"
    t.string   "platform"
    t.string   "system_version"
    t.string   "operator"
    t.string   "net_env"
    t.string   "app_version"
    t.string   "app_version_number"
    t.string   "app_channel"
    t.string   "ip"
    t.string   "ip_country"
    t.string   "ip_province"
    t.string   "ip_city"
    t.decimal  "longitude",          precision: 16, scale: 8, default: "0.0"
    t.decimal  "latitude",           precision: 16, scale: 8, default: "0.0"
    t.string   "address"
    t.string   "coord_method"
    t.datetime "created_at",                                                  null: false
    t.datetime "updated_at",                                                  null: false
    t.index ["created_at"], name: "index_user_signin_logs_on_created_at", using: :btree
    t.index ["device_id"], name: "index_user_signin_logs_on_device_id", using: :btree
    t.index ["id"], name: "index_user_signin_logs_on_id", unique: true, using: :btree
    t.index ["session_id"], name: "index_user_signin_logs_on_session_id", using: :btree
    t.index ["user_id", "created_at"], name: "index_user_signin_logs_on_user_id_and_created_at", using: :btree
    t.index ["user_id"], name: "index_user_signin_logs_on_user_id", using: :btree
  end

  create_table "user_signin_period_day_stats", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb4" do |t|
    t.date     "stat_date"
    t.string   "app_channel"
    t.string   "app_version"
    t.text     "period_data", limit: 65535
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
    t.index ["app_channel"], name: "index_user_signin_period_day_stats_on_app_channel", using: :btree
    t.index ["app_version"], name: "index_user_signin_period_day_stats_on_app_version", using: :btree
    t.index ["created_at"], name: "index_user_signin_period_day_stats_on_created_at", using: :btree
    t.index ["stat_date"], name: "index_user_signin_period_day_stats_on_stat_date", using: :btree
  end

  create_table "user_status_stats", id: :string, limit: 36, force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb4" do |t|
    t.string   "app_version"
    t.string   "app_channel"
    t.boolean  "is_enabled"
    t.integer  "quantity"
    t.datetime "created_at",              null: false
    t.datetime "updated_at",              null: false
    t.integer  "filter_type", default: 0
    t.index ["id"], name: "index_user_status_stats_on_id", unique: true, using: :btree
  end

end

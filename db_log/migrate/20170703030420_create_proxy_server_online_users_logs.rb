class CreateProxyServerOnlineUsersLogs < ActiveRecord::Migration[5.0]
  def change
    create_table :proxy_server_online_users_logs do |t|
      t.integer :node_id, index: true
      t.integer :online_users_count, default: 0
      t.timestamps
    end
    add_index :proxy_server_online_users_logs, :created_at
  end
end

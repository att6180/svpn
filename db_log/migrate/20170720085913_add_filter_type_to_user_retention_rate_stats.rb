class AddFilterTypeToUserRetentionRateStats < ActiveRecord::Migration[5.0]
  def change
    remove_column :user_retention_rate_stats, :is_total
    add_column :user_retention_rate_stats, :filter_type, :integer, default: 0
  end
end

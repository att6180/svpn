class CreateUserNoOperationStats < ActiveRecord::Migration[5.0]
  def change
    create_table :user_no_operation_stats, id: false do |t|
      t.string :id, limit: 36, primary_key: true, null: false
      t.date :stat_date
      t.string :app_version
      t.string :app_channel
      t.integer :no_operations_count, default: 0
      t.timestamps
    end
  end
end

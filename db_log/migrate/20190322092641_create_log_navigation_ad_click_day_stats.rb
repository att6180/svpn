class CreateLogNavigationAdClickDayStats < ActiveRecord::Migration[5.0]
  def change
    create_table :log_navigation_ad_click_day_stats do |t|
      t.date :stat_date, index: true
      t.string :ad_name
      t.integer :clicked_count
      t.timestamps
    end
  end
end

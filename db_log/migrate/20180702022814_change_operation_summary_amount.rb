class ChangeOperationSummaryAmount < ActiveRecord::Migration[5.0]
  def change
    change_column :log_operation_summary_month_stats, :total_recharge_amount, :decimal, precision: 11, scale: 2, default: 0
  end
end

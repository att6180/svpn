class AddNodeTypeInfoToProxyServerOnlineUsersDayStats < ActiveRecord::Migration[5.0]
  def change
    add_column :proxy_server_online_users_day_stats, :node_type_info, :text
  end
end

class AddIpRegionToNodeConnectionLogs < ActiveRecord::Migration[5.0]
  def change
    add_column :node_connection_logs, :ip_country, :string, index: true
    add_column :node_connection_logs, :ip_province, :string, index: true
    add_column :node_connection_logs, :ip_city, :string, index: true
  end
end

class CreateUserNoOperationLogs < ActiveRecord::Migration[5.0]
  def change
    create_table :user_no_operation_logs, id: false do |t|
      t.string :id, limit: 36, primary_key: true, null: false
      t.string :username, index: true
      t.string :device_uuid, index: true
      t.string :device_model
      t.string :ip
      t.string :ip_country
      t.string :ip_province
      t.string :ip_city
      t.string :app_channel
      t.string :app_version
      t.string :app_version_number
      t.string :operator
      t.timestamps
    end
    add_index :user_no_operation_logs, :id, unique: true
    add_index :user_no_operation_logs, :created_at
  end
end

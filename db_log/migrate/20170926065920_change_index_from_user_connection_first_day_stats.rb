class ChangeIndexFromUserConnectionFirstDayStats < ActiveRecord::Migration[5.0]
  def change
    remove_index :user_connection_first_day_stats, :user_id
    remove_index :user_connection_first_day_stats, :domain
    remove_index :user_connection_first_day_stats, :stat_date

    add_index :user_connection_first_day_stats, [:domain, :user_id, :visits_count, :stat_date], name: 'iucfds_four_index'
  end
end

class CreateTransactionPaymentFailedLogs < ActiveRecord::Migration[5.0]
  def change
    create_table :transaction_payment_failed_logs do |t|
      t.string :order_number
      t.integer :user_id
      t.string :platform
      t.string :app_channel
      t.string :app_version
      t.timestamps
    end
  end
end

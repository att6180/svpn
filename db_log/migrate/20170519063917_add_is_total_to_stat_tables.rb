class AddIsTotalToStatTables < ActiveRecord::Migration[5.0]
  def change
    add_column :active_user_day_stats, :is_total, :boolean, default: false, index: true
    add_column :active_user_month_stats, :is_total, :boolean, default: false, index: true
    add_column :connection_failed_stats, :is_total, :boolean, default: false, index: true
    add_column :new_user_day_stats, :is_total, :boolean, default: false, index: true
    add_column :new_user_month_stats, :is_total, :boolean, default: false, index: true
    add_column :node_connection_count_day_stats, :is_total, :boolean, default: false, index: true
    add_column :node_connection_count_scope_day_stats, :is_total, :boolean, default: false, index: true
    add_column :user_no_operation_stats, :is_total, :boolean, default: false, index: true
    add_column :user_signin_failed_stats, :is_total, :boolean, default: false, index: true
    add_column :user_operation_day_stats, :is_total, :boolean, default: false, index: true
    add_column :user_operation_day_stats, :interface_id, :integer, index: true
    add_column :user_retention_rate_stats, :is_total, :boolean, default: false, index: true
  end
end

class CreateConnectionFailedStats < ActiveRecord::Migration[5.0]
  def change
    create_table :connection_failed_stats, id: false do |t|
      t.string :id, limit: 36, primary_key: true, null: false
      t.string :app_version, index: true
      t.string :app_channel, index: true
      t.integer :connection_failed_count, default: 0
      t.integer :crash_count, default: 0
      t.integer :unconnected_count, default: 0
      t.timestamps
    end
    add_index :connection_failed_stats, :id, unique: true
    add_index :connection_failed_stats, :created_at
  end
end

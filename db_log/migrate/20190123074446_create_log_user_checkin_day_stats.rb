class CreateLogUserCheckinDayStats < ActiveRecord::Migration[5.0]
  def change
    create_table :log_user_checkin_day_stats, id: false do |t|
      t.string :id, limit: 36, primary_key: true, null: false
      t.date :stat_date, index: true
      t.string :app_channel, index: true
      t.string :app_version, index: true
      t.integer :active_users_count, default: 0
      t.integer :checkin_users_count, default: 0
      t.integer :filter_type, default: 0
      t.timestamps
    end
    add_index :log_user_checkin_day_stats, :id, unique: true
  end
end

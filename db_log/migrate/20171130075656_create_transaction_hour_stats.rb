class CreateTransactionHourStats < ActiveRecord::Migration[5.0]
  def change
    create_table :transaction_hour_stats do |t|
      t.date :stat_date
      t.integer :stat_hour
      t.decimal :total_recharge_amount, precision: 8, scale: 2, default: 0
      t.integer :recharge_users_count, default: 0
      t.string :app_channel
      t.string :app_version
      t.integer :filter_type, default: 0
      t.timestamps
    end
    add_index :transaction_hour_stats, [:stat_date, :stat_hour, :app_channel, :app_version, :filter_type], name: "transaction_hour_stats_ssaaf"

    # add index to transactios_logs
    add_index :transaction_logs, [:created_at, :status]
  end
end

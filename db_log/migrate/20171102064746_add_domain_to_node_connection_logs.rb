class AddDomainToNodeConnectionLogs < ActiveRecord::Migration[5.0]
  def change
    add_column :node_connection_logs, :domain, :string
  end
end

class CreateNodeDetails < ActiveRecord::Migration[5.0]
  def change
    create_table :node_details, id: false do |t|
      t.string :id, limit: 36, primary_key: true, null: false
      t.integer :node_id
      # 连接数
      t.integer :connections_count, default: 0
      # 总带宽
      t.integer :max_bandwidth, limit: 8, default: 0
      # 当前带宽
      t.integer :current_bandwidth, limit: 8, default: 0
      # CPU百分比
      t.decimal :cpu_percent, precision: 4, scale: 1, default: 0
      # 内存百分比
      t.decimal :memory_percent, precision: 4, scale: 1, default: 0
      # 用户使用的流量总和
      t.integer :transfer, limit: 8, default: 0
      # 当前网络速度
      t.integer :network_speed, limit: 8, default: 0
      t.timestamps
    end
    add_index :node_details, :id, unique: true
    add_index :node_details, :node_id, unique: true
  end
end

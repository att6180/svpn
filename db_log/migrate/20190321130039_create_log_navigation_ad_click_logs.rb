class CreateLogNavigationAdClickLogs < ActiveRecord::Migration[5.0]
  def change
    create_table :log_navigation_ad_click_logs, id: false do |t|
      t.integer :id, limit: 8, primary_key: true, auto_increment: true
      t.integer :user_id, index: true, null: false
      t.string :uuid
      t.integer :ad_id, null: false
      t.string :ad_name
      t.string :app_channel, index: true
      t.string :app_version, index: true
      t.string :app_version_number
      t.date :created_date, index: true
      t.timestamps
    end
    add_index :log_navigation_ad_click_logs, [:app_channel, :app_version, :created_at],
      name: 'lnacl_aac'
  end
end

class AddFilterTypeToUserStatTables < ActiveRecord::Migration[5.0]
  def change
    remove_column :user_hardware_stats, :is_total
    add_column :user_hardware_stats, :filter_type, :integer, default: 0
    remove_column :user_operator_stats, :is_total
    add_column :user_operator_stats, :filter_type, :integer, default: 0
    remove_column :user_status_stats, :is_total
    add_column :user_status_stats, :filter_type, :integer,  default: 0
    remove_column :user_share_stats, :is_total
    add_column :user_share_stats, :filter_type, :integer,  default: 0
  end
end

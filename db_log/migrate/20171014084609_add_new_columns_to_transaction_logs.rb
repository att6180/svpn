class AddNewColumnsToTransactionLogs < ActiveRecord::Migration[5.0]
  def change
    add_column :transaction_logs, :is_regular_time, :boolean, default: false, index: true
    add_column :transaction_logs, :time_type, :integer, default: 0
    add_column :transaction_logs, :node_type_id, :integer, index: true

    add_column :consumption_logs, :is_regular_time, :boolean, default: false, index: true
    add_column :consumption_logs, :time_type, :integer, default: 0
  end
end

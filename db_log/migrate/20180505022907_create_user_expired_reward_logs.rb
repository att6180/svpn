class CreateUserExpiredRewardLogs < ActiveRecord::Migration[5.0]
  def change
    create_table :user_expired_reward_logs do |t|
      t.integer :admin_id, index: true
      t.integer :user_id, index: true
      t.datetime :original_expired_at
      t.datetime :current_expired_at
      t.integer :user_node_type_id, index: true
      t.integer :node_type_id, index: true
      t.integer :operation_type, default: 0
      t.timestamps
    end
  end
end

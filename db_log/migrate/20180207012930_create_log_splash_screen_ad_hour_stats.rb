class CreateLogSplashScreenAdHourStats < ActiveRecord::Migration[5.0]
  def change
    create_table :log_splash_screen_ad_hour_stats do |t|
      t.date :stat_date
      t.integer :stat_hour
      t.integer :showed_count, default: 0
      t.timestamps
    end
    add_index :log_splash_screen_ad_hour_stats, [:stat_date, :stat_hour],
      name: 'lssahs_ss'
  end
end

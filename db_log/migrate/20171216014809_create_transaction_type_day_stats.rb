class CreateTransactionTypeDayStats < ActiveRecord::Migration[5.0]
  def change
    create_table :transaction_type_day_stats, id: false do |t|
      t.bigint :id, primary_key: true, auto_increment: true, null: false
      t.date :stat_date
      t.integer :users_count, default: 0
      t.text :type_users_count
      t.string :app_channel
      t.string :app_version
      t.integer :filter_type, default: 0
      t.timestamps
    end
    add_index :transaction_type_day_stats, :stat_date
    add_index :transaction_type_day_stats, [:stat_date, :app_channel, :app_version],
      name: "transaction_type_day_stats_saa"
  end
end

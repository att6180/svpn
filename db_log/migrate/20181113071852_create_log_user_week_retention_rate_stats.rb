class CreateLogUserWeekRetentionRateStats < ActiveRecord::Migration[5.0]
  def change
    create_table :log_user_week_retention_rate_stats, id: false do |t|
      t.string :id, limit: 36, primary_key: true, null: false
      t.date :stat_date, index: true
      t.string :app_channel, limit: 191, index: true
      t.string :app_version, limit: 191, index: true
      t.integer :users_count, default: 0
      t.integer :first_week_users_count
      t.integer :second_week_users_count
      t.integer :third_week_users_count
      t.integer :fourth_week_users_count
      t.integer :fifth_week_users_count
      t.integer :sixth_week_users_count
      t.integer :seventh_week_users_count
      t.integer :eighth_week_users_count
      t.integer :ninth_week_users_count
      t.integer :filter_type, default: 0
      t.timestamps
    end
    add_index :log_user_week_retention_rate_stats, :id, unique: true
  end
end

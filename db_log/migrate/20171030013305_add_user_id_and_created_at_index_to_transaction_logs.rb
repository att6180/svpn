class AddUserIdAndCreatedAtIndexToTransactionLogs < ActiveRecord::Migration[5.0]
  def change
    add_index :transaction_logs, :created_at
    add_index :transaction_logs, [:user_id, :created_at]
  end
end

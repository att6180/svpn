class AddTransactionIdIndexToComsunnyTransactionLogs < ActiveRecord::Migration[5.0]
  def change
    add_index :comsunny_transaction_logs, :transaction_id
  end
end

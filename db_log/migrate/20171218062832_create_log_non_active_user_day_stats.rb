class CreateLogNonActiveUserDayStats < ActiveRecord::Migration[5.0]
  def change
    create_table :log_non_active_user_day_stats do |t|
      t.date :stat_date
      t.integer :users_count, default: 0
      t.integer :nonactive_users_count, default: 0
      t.string :app_channel
      t.string :app_version
      t.integer :filter_type, default: 0
      t.timestamps
    end
    add_index :log_non_active_user_day_stats, [:stat_date, :app_channel, :app_version, :filter_type],
      name: 'non_active_user_day_stats_saaf'
  end
end

class RenameUserSiginHourLog < ActiveRecord::Migration[5.0]
  def change
    rename_table :user_signin_hour_logs, :user_active_hour_logs
  end
end

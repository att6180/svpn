class CreateLogSplashScreenAdClickHourStats < ActiveRecord::Migration[5.0]
  def change
    create_table :log_splash_screen_ad_click_hour_stats do |t|
      t.date :stat_date
      t.integer :stat_hour
      t.integer :clicked_count, default: 0
      t.timestamps
    end
    add_index :log_splash_screen_ad_click_hour_stats, [:stat_date, :stat_hour],
      name: 'lssachs_ss'
  end
end

class CreateDynamicServerConnectionFailedLogs < ActiveRecord::Migration[5.0]
  def change
    create_table :dynamic_server_connection_failed_logs, id: false do |t|
      t.string :id, limit: 36, primary_key: true, null: false
      t.integer :dynamic_server_id
      t.integer :user_id
      t.string :app_channel
      t.string :app_version
      t.string :app_version_number
      t.string :device_model
      t.string :platform
      t.string :operator
      t.string :ip
      t.string :ip_country
      t.string :ip_province
      t.string :ip_city
      t.timestamps
    end
    add_index :dynamic_server_connection_failed_logs, :id, unique: true
    add_index :dynamic_server_connection_failed_logs, :created_at
  end
end

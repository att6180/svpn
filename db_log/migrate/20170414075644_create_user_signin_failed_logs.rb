class CreateUserSigninFailedLogs < ActiveRecord::Migration[5.0]
  def change
    create_table :user_signin_failed_logs, id: false do |t|
      t.string :id, limit: 36, primary_key: true, null: false
      t.string :username, index: true
      t.string :device_uuid, limit: 40, index: true
      t.integer :fail_type, default: 0, index: true
      t.string :ip
      t.timestamps
    end
    add_index :user_signin_failed_logs, :id, unique: true
    add_index :user_signin_failed_logs, :created_at
  end
end

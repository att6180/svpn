class CreateUserAccountStatusLogs < ActiveRecord::Migration[5.0]
  def change
    create_table :user_account_status_logs, id: false do |t|
      t.string :id, limit: 36, primary_key: true, null: false
      t.integer :admin_id, index: true
      t.string :admin_username
      t.integer :user_id, index: true
      t.integer :operation_type, default: 0
      t.text :description
      t.timestamps
    end
    add_index :user_account_status_logs, :id, unique: true
  end
end

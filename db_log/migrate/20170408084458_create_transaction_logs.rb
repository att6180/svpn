class CreateTransactionLogs < ActiveRecord::Migration[5.0]
  def change
    create_table :transaction_logs, id: false do |t|
      t.string :id, limit: 36, primary_key: true, null: false
      t.string :order_number
      t.integer :user_id, index: true
      t.integer :plan_id, index: true
      t.decimal :price, precision: 8, scale: 2, default: 0
      t.integer :coins, default: 0
      t.integer :payment_method, default: 0
      t.string :status, index: true
      t.datetime :processed_at
      t.timestamps
    end
    add_index :transaction_logs, :id, unique: true
    add_index :transaction_logs, :order_number, unique: true
  end
end

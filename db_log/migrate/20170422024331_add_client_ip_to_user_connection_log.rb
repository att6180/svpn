class AddClientIpToUserConnectionLog < ActiveRecord::Migration[5.0]
  def change
    add_column :user_connection_logs, :client_ip, :string
    add_column :user_connection_logs, :ip_country, :string
    add_column :user_connection_logs, :ip_province, :string
    add_column :user_connection_logs, :ip_city, :string
    add_column :user_connection_logs, :first_proxy_ip, :string
  end
end

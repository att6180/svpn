class CreateConsumptionLogs < ActiveRecord::Migration[5.0]
  def change
    create_table :consumption_logs, id: false do |t|
      t.string :id, limit: 36, primary_key: true, null: false
      t.integer :user_id, index: true
      t.string :node_type
      t.string :node_region
      t.string :node_name
      t.integer :coins, default: 0
      t.timestamps
    end
    add_index :consumption_logs, :id, unique: true
    add_index :consumption_logs, :created_at
  end
end

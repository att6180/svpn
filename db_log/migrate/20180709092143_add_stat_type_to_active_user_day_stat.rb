class AddStatTypeToActiveUserDayStat < ActiveRecord::Migration[5.0]
  def change
    add_column :active_user_day_stats, :stat_type, :integer, default: 0
  end
end

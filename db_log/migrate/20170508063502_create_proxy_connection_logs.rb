class CreateProxyConnectionLogs < ActiveRecord::Migration[5.0]
  def change
    create_table :proxy_connection_logs, id: false do |t|
      t.string :id, limit: 36, primary_key: true, null: false
      t.integer :user_id, index: true
      t.integer :node_id, index: true
      t.string :node_ip
      t.text :description
      t.text :data
      t.timestamps
    end
    add_index :proxy_connection_logs, :id, unique: true
    add_index :proxy_connection_logs, :created_at
  end
end

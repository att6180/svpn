class AddThirdDayToOperationSummaryDayStats < ActiveRecord::Migration[5.0]
  def change
    add_column :log_operation_summary_day_stats, :third_day, :integer, after: :second_day
    add_column :log_operation_summary_day_stats, :fifteen_day, :integer, after: :seventh_day
  end
end

class AddUserIdToFailedLogs < ActiveRecord::Migration[5.0]
  def change
    add_column :user_signin_failed_logs, :user_id, :integer, index:true
    add_column :connection_failed_logs, :user_id, :integer, index:true
    add_column :user_no_operation_logs, :user_id, :integer, index:true
  end
end

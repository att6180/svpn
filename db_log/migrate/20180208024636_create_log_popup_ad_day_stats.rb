class CreateLogPopupAdDayStats < ActiveRecord::Migration[5.0]
  def change
    create_table :log_popup_ad_day_stats do |t|
      t.date :stat_date
      t.integer :showed_count, default: 0
      t.timestamps
    end
    add_index :log_popup_ad_day_stats, :stat_date
  end
end

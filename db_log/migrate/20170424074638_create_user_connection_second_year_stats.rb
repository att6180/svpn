class CreateUserConnectionSecondYearStats < ActiveRecord::Migration[5.0]
  def change
    create_table :user_connection_second_year_stats, id: false do |t|
      t.string :id, limit: 36, primary_key: true, null: false
      t.string :domain, index: true
      t.integer :visit_users_count, default: 0
      t.integer :visits_count, default: 0
      t.integer :stat_year, index: true
      t.timestamps
    end
    add_index :user_connection_second_year_stats, :id, unique: true
    add_index :user_connection_second_year_stats, :created_at
  end
end

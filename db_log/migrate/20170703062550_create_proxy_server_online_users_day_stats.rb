class CreateProxyServerOnlineUsersDayStats < ActiveRecord::Migration[5.0]
  def change
    create_table :proxy_server_online_users_day_stats do |t|
      t.date :stat_date, index: true
      t.integer :min_count, default: 0
      t.integer :max_count, default: 0
      t.integer :average_count, default: 0
      t.timestamps
    end
  end
end

class AddIsTotalToUserStatTables < ActiveRecord::Migration[5.0]
  def change
    add_column :user_hardware_stats, :is_total, :boolean, default: false, index: true
    add_column :user_operator_stats, :is_total, :boolean, default: false, index: true
    add_column :user_status_stats, :is_total, :boolean, default: false, index: true
    add_column :user_share_stats, :is_total, :boolean, default: false, index: true
  end
end

class AddMinMaxTimeToProxyServerOnlineUsersDayStats < ActiveRecord::Migration[5.0]
  def change
    add_column :proxy_server_online_users_day_stats, :min_time, :datetime
    add_column :proxy_server_online_users_day_stats, :max_time, :datetime
  end
end

class CreateLogUserMonthRetentionRateStats < ActiveRecord::Migration[5.0]
  def change
    create_table :log_user_month_retention_rate_stats, id: false do |t|
      t.string :id, limit: 36, primary_key: true, null: false
      t.date :stat_date, index: true
      t.string :app_channel, limit: 191, index: true
      t.string :app_version, limit: 191, index: true
      t.integer :users_count, default: 0
      t.integer :first_month_users_count
      t.integer :second_month_users_count
      t.integer :third_month_users_count
      t.integer :fourth_month_users_count
      t.integer :fifth_month_users_count
      t.integer :sixth_month_users_count
      t.integer :seventh_month_users_count
      t.integer :eighth_month_users_count
      t.integer :ninth_month_users_count
      t.integer :filter_type, default: 0
      t.timestamps
    end
    add_index :log_user_month_retention_rate_stats, :id, unique: true
  end
end

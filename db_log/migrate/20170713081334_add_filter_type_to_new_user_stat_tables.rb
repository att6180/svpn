class AddFilterTypeToNewUserStatTables < ActiveRecord::Migration[5.0]
  def change
    remove_column :new_user_day_stats, :is_total
    add_column :new_user_day_stats, :filter_type, :integer, default: 0
    remove_column :new_user_month_stats, :is_total
    add_column :new_user_month_stats, :filter_type, :integer, default: 0
  end
end

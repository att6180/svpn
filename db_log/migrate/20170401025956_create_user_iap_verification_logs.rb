class CreateUserIapVerificationLogs < ActiveRecord::Migration[5.0]
  def change
    create_table :user_iap_verification_logs, id: false do |t|
      t.string :id, limit: 36, primary_key: true, null: false
      # 用户ID
      t.integer :user_id, index: true
      # IAP ID
      t.string :iap_id
      # 支付凭证数据
      t.text :receipt_data
      # 支付状态
      t.integer :status
      # 验证结果
      t.text :result
      # 客户端IP
      t.string :ip
      t.timestamps
    end
  end
end

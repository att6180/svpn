class AddNodeTypeIdToConsumptionLogs < ActiveRecord::Migration[5.0]
  def change
    add_column :consumption_logs, :node_type_id, :integer, index: true
  end
end

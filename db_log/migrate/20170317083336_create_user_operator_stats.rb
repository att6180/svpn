class CreateUserOperatorStats < ActiveRecord::Migration[5.0]
  def change
    create_table :user_operator_stats, id: false do |t|
      t.string :id, limit: 36, primary_key: true, null: false
      t.string :app_version
      t.string :app_channel
      t.string :operator
      t.integer :quantity
      t.timestamps
    end
    add_index :user_operator_stats, :id, unique: true
  end
end

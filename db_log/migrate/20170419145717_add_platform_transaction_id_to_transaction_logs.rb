class AddPlatformTransactionIdToTransactionLogs < ActiveRecord::Migration[5.0]
  def change
    add_column :transaction_logs, :platform_transaction_id, :string
  end
end

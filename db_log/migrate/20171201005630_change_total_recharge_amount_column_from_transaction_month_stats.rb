class ChangeTotalRechargeAmountColumnFromTransactionMonthStats < ActiveRecord::Migration[5.0]
  def change
    change_column :transaction_month_stats, :total_recharge_amount, :decimal, precision: 12, scale: 2, default: 0
  end
end

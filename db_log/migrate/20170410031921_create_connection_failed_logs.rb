class CreateConnectionFailedLogs < ActiveRecord::Migration[5.0]
  def change
    create_table :connection_failed_logs, id: false do |t|
      t.string :id, limit: 36, primary_key: true, null: false
      t.integer :fail_type, default: 1
      t.string :username, index: true
      t.string :device_uuid, index: true
      t.string :device_model
      t.string :node_type
      t.string :node_region
      t.string :node_name
      t.string :ip
      t.string :ip_country
      t.string :ip_province
      t.string :ip_city
      t.string :app_channel
      t.string :app_version
      t.string :app_version_number
      t.string :operator
      t.timestamps
    end
    add_index :connection_failed_logs, :id, unique: true
    add_index :connection_failed_logs, :created_at
  end
end

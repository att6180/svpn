class CreateLogUserTrafficDayStats < ActiveRecord::Migration[5.0]
  def change
    create_table :log_user_traffic_day_stats do |t|
      t.date :stat_date
      t.integer :users_count, default: 0
      t.text :traffic_period
      t.string :app_channel
      t.string :app_version
      t.integer :filter_type, default: 0
      t.timestamps
    end
    add_index :log_user_traffic_day_stats, [:stat_date, :app_channel, :app_version, :filter_type],
      name: "log_user_traffic_day_stats_saaf"
  end
end

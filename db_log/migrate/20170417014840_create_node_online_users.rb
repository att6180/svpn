class CreateNodeOnlineUsers < ActiveRecord::Migration[5.0]
  def change
    create_table :node_online_users, id: false do |t|
      t.string :id, limit: 36, primary_key: true, null: false
      t.integer :user_id, index: true
      t.integer :node_region_id, index: true
      t.integer :node_id, index: true
      t.string :ip
      t.string :ip_country
      t.string :ip_province
      t.string :ip_city
      t.timestamps
    end
    add_index :node_online_users, :id, unique: true
    add_index :node_online_users, :created_at
  end
end

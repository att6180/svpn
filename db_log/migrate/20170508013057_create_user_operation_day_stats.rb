class CreateUserOperationDayStats < ActiveRecord::Migration[5.0]
  def change
    create_table :user_operation_day_stats, id: false do |t|
      t.string :id, limit: 36, primary_key: true, null: false
      t.date :stat_date, index: true
      t.string :app_channel, index: true
      t.string :app_version, index: true
      t.integer :users_count, default: 0
      t.integer :visits_count, default: 0
      t.timestamps
    end
    add_index :user_operation_day_stats, :id, unique: true
  end
end

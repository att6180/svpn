class AddPlanNameToTransactionLogs < ActiveRecord::Migration[5.0]
  def change
    add_column :transaction_logs, :plan_name, :string
    add_column :transaction_logs, :plan_coins, :integer, default: 0
    add_column :transaction_logs, :present_coins, :integer, default: 0
  end
end

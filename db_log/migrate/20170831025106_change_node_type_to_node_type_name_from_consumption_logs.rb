class ChangeNodeTypeToNodeTypeNameFromConsumptionLogs < ActiveRecord::Migration[5.0]
  def change
    rename_column :consumption_logs, :node_type, :node_type_name
  end
end

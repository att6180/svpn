class CreateLogUserShareStatLogs < ActiveRecord::Migration[5.0]
  def change
    create_table :log_user_share_stat_logs, id: false do |t|
      t.string :id, limit: 36, primary_key: true, null: false
      t.date :stat_date, index: true
      t.string :app_channel, limit: 191, index: true
      t.string :app_version, limit: 191, index: true
      t.string :platform_name, index: true
      t.integer :copy_link_count, default: 0
      t.integer :weixin_text_count, default: 0
      t.integer :friend_circle_text_count, default: 0
      t.integer :qq_text_count, default: 0
      t.integer :weibo_text_count, default: 0
      t.integer :weixin_image_count, default: 0
      t.integer :friend_circle_image_count, default: 0
      t.integer :qq_image_count, default: 0
      t.integer :weibo_image_count, default: 0
      t.integer :filter_type, default: 0
      t.timestamps
    end
    add_index :log_user_share_stat_logs, :id, unique: true
  end
end

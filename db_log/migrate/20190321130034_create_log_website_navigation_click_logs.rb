class CreateLogWebsiteNavigationClickLogs < ActiveRecord::Migration[5.0]
  def change
    create_table :log_website_navigation_click_logs, id: false do |t|
      t.integer :id, limit: 8, primary_key: true, auto_increment: true
      t.integer :user_id, index: true, null: false
      t.string :uuid
      t.integer :website_id, null: false
      t.string :website_name
      t.integer :nav_type
      t.integer :info_type
      t.boolean :is_domestic
      t.string :app_channel, index: true
      t.string :app_version, index: true
      t.string :app_version_number
      t.date :created_date, index: true
      t.timestamps
    end
    add_index :log_website_navigation_click_logs, [:app_channel, :app_version, :created_at],
      name: 'lwnc_aac'
  end
end

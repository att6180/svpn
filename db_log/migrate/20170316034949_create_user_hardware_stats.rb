class CreateUserHardwareStats < ActiveRecord::Migration[5.0]
  def change
    create_table :user_hardware_stats, id: false do |t|
      t.string :id, limit: 36, primary_key: true, null: false
      t.string :app_version
      t.string :app_channel
      t.string :device_model
      t.string :platform
      t.integer :quantity
      t.timestamps
    end
    add_index :user_hardware_stats, :id, unique: true
  end
end

class CreateLogOperationSummaryDayStats < ActiveRecord::Migration[5.0]
  def change
    create_table :log_operation_summary_day_stats do |t|
      t.date :stat_date, index: true
      t.decimal :total_recharge_amount, precision: 8, scale: 2, default: 0
      t.integer :new_users_count, default: 0
      t.integer :active_users_count, default: 0
      t.integer :paid_users_count, default: 0
      t.decimal :payment_rate, precision: 8, scale: 2, default: 0
      t.decimal :paid_total_price, precision: 8, scale: 2, default: 0
      t.integer :seventh_paid_users_count, default: 0
      t.decimal :seventh_payment_rate, precision: 8, scale: 2, default: 0
      t.decimal :seventh_paid_total_price, precision: 8, scale: 2, default: 0
      t.integer :thirtieth_paid_users_count, default: 0
      t.decimal :thirtieth_payment_rate, precision: 8, scale: 2, default: 0
      t.decimal :thirtieth_paid_total_price, precision: 8, scale: 2, default: 0
      t.integer :second_day, default: 0
      t.decimal :second_day_rate, precision: 8, scale: 2, default: 0
      t.integer :seventh_day, default: 0
      t.decimal :seventh_day_rate, precision: 8, scale: 2, default: 0
      t.integer :thirtieth_day, default: 0
      t.decimal :thirtieth_day_rate, precision: 8, scale: 2, default: 0
      t.integer :platform_id
      t.integer :channel_account_id
      t.integer :package_id
      t.string :app_channel, index: true
      t.string :app_version, index: true
      t.integer :filter_type
      t.timestamps
    end
  end
end

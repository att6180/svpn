class CreateActiveUserMonthStats < ActiveRecord::Migration[5.0]
  def change
    create_table :active_user_month_stats, id: false do |t|
      t.string :id, limit: 36, primary_key: true, null: false
      t.integer :stat_year, index: true
      t.integer :stat_month, index: true
      t.string :app_channel, index: true
      t.string :app_version, index: true
      t.integer :users_count, default: 0
      t.timestamps
    end
    add_index :active_user_month_stats, :id, unique: true
  end
end

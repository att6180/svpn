class AddUserIdIndexToClientConnectionLogs < ActiveRecord::Migration[5.0]
  def change
    add_index :client_connection_logs, :user_id
  end
end

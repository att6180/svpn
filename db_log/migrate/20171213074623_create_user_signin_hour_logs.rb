class CreateUserSigninHourLogs < ActiveRecord::Migration[5.0]
  def change
    create_table :user_signin_hour_logs, id: false do |t|
      t.bigint :id, primary_key: true, auto_increment: true, null: false
      t.date :created_date
      t.integer :created_hour
      t.integer :user_id, index: true
      t.string :app_version
      t.string :app_channel
      t.timestamps
    end
    add_index :user_signin_hour_logs, [:created_date, :created_hour, :user_id],
      name: 'user_signin_hour_logs_ccu', unique: true
    add_index :user_signin_hour_logs, [:created_date, :created_hour, :app_channel, :app_version],
      name: "user_signin_hour_logs_ccaa"
  end
end

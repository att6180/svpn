class CreateProxyConnectionFailedLogs < ActiveRecord::Migration[5.0]
  def change
    create_table :proxy_connection_failed_logs do |t|
      t.integer :user_id
      t.string :uuid
      t.string :node_id
      t.string :node_ip
      t.string :target_url
      t.string :client_ip
      t.string :client_ip_country
      t.string :client_ip_province
      t.string :client_ip_city
      t.string :operator
      t.timestamps
    end
    add_index :proxy_connection_failed_logs, :created_at
  end
end

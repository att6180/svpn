class AddCreatedAtAndIsRegularTimeIndexToConsumptionLogs < ActiveRecord::Migration[5.0]
  def change
    add_index :consumption_logs, [:is_regular_time, :created_at]
  end
end

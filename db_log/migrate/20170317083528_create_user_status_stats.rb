class CreateUserStatusStats < ActiveRecord::Migration[5.0]
  def change
    create_table :user_status_stats, id: false do |t|
      t.string :id, limit: 36, primary_key: true, null: false
      t.string :app_version
      t.string :app_channel
      t.boolean :is_enabled
      t.integer :quantity
      t.timestamps
    end
    add_index :user_status_stats, :id, unique: true
  end
end

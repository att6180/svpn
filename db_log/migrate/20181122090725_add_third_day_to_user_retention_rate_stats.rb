class AddThirdDayToUserRetentionRateStats < ActiveRecord::Migration[5.0]
  def change
    add_column :user_retention_rate_stats, :third_day, :integer, after: :second_day
    add_column :user_retention_rate_stats, :fifteen_day, :integer, after: :seventh_day
  end
end

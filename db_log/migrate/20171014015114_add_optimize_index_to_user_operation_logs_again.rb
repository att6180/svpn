class AddOptimizeIndexToUserOperationLogsAgain < ActiveRecord::Migration[5.0]
  def change
    add_index :user_operation_logs, [:user_id, :created_at]
  end
end

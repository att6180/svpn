class AddTransactionIdToUserIapVerificationLogs < ActiveRecord::Migration[5.0]
  def change
    add_column :user_iap_verification_logs, :transaction_id, :string, index: true
  end
end

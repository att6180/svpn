class CreateNodeSwitchLogs < ActiveRecord::Migration[5.0]
  def change
    create_table :node_switch_logs do |t|
      t.integer :node_id
      t.integer :status, default: 0
      t.string :node_type_name
      t.string :node_ip
      t.timestamps
    end
    add_index :node_switch_logs, [:node_id, :status, :created_at]
  end
end

class RemoveUserAndNodeDetails < ActiveRecord::Migration[5.0]
  def change
    drop_table :user_details
    drop_table :node_details

    # 为user_connection_logs表加入汇总标记字段
    add_column :user_connection_logs, :is_collected, :boolean, default: false
  end
end

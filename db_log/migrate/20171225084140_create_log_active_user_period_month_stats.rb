class CreateLogActiveUserPeriodMonthStats < ActiveRecord::Migration[5.0]
  def change
    create_table :log_active_user_period_month_stats do |t|
      t.integer :stat_year
      t.integer :stat_month
      t.integer :users_count, default: 0
      t.text :period
      t.string :app_channel
      t.string :app_version
      t.integer :filter_type, default: 0
      t.timestamps
    end
    add_index :log_active_user_period_month_stats, [:stat_year, :stat_month, :app_channel, :app_version, :filter_type],
      name: 'log_active_user_period_month_stats_ssaaf'
  end
end

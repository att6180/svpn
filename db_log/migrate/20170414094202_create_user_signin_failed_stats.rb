class CreateUserSigninFailedStats < ActiveRecord::Migration[5.0]
  def change
    create_table :user_signin_failed_stats, id: false do |t|
      t.string :id, limit: 36, primary_key: true, null: false
      t.date :stat_date, index: true
      t.integer :not_exist_count, default: 0
      t.integer :wrong_password_count, default: 0
      t.integer :failed_connection_count, default: 0
      t.integer :crash_count, default: 0
      t.timestamps
    end
    add_index :user_signin_failed_stats, :id, unique: true
  end
end

class AddPlatformToUserShareLogAndUserShareStat < ActiveRecord::Migration[5.0]
  def change
    add_column :user_share_logs, :platform_name, :string, after: :share_type
  end
end

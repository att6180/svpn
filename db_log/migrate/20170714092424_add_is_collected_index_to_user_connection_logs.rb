class AddIsCollectedIndexToUserConnectionLogs < ActiveRecord::Migration[5.0]
  def change
    add_index :user_connection_logs, :is_collected
  end
end

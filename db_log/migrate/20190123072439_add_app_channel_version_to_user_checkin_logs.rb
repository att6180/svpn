class AddAppChannelVersionToUserCheckinLogs < ActiveRecord::Migration[5.0]
  def change
    add_column :user_checkin_logs, :checkin_date, :date
    add_column :user_checkin_logs, :app_channel, :string, limit: 191
    add_column :user_checkin_logs, :app_version, :string, limit: 191

    add_index :user_checkin_logs, :checkin_date
    add_index :user_checkin_logs, :app_channel
    add_index :user_checkin_logs, :app_version
  end
end

class CreateUserPromoLogs < ActiveRecord::Migration[5.0]
  def change
    create_table :user_promo_logs do |t|
      t.integer :promoter_id, index: true
      t.string :promo_code
      t.integer :accepted_user_id, index: true
      t.integer :promoter_coins
      t.integer :accepted_user_coins
      t.timestamps
    end
    add_index :user_promo_logs, :created_at
  end
end

class AddOptimizeIndexToUserConnectionLogs < ActiveRecord::Migration[5.0]
  def change
    add_index :user_connection_logs, [:user_id, :created_at]
  end
end

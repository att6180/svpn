class AddPaymentColumnsToUserRetentionRateStats < ActiveRecord::Migration[5.0]
  def change
    add_column :user_retention_rate_stats, :paid_users_count, :integer
    add_column :user_retention_rate_stats, :total_price, :decimal, precision: 12, scale: 2
    add_column :user_retention_rate_stats, :second_paid_users_count, :integer
    add_column :user_retention_rate_stats, :second_total_price, :decimal, precision: 12, scale: 2
    add_column :user_retention_rate_stats, :seventh_paid_users_count, :integer
    add_column :user_retention_rate_stats, :seventh_total_price, :decimal, precision: 12, scale: 2
    add_column :user_retention_rate_stats, :thirtieth_paid_users_count, :integer
    add_column :user_retention_rate_stats, :thirtieth_total_price, :decimal, precision: 12, scale: 2
  end
end

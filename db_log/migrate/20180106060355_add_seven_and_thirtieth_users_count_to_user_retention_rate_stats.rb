class AddSevenAndThirtiethUsersCountToUserRetentionRateStats < ActiveRecord::Migration[5.0]
  def change
    add_column :user_retention_rate_stats, :seventh_users_count, :integer
    add_column :user_retention_rate_stats, :thirtieth_users_count, :integer
  end
end

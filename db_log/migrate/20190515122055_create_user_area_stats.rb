class CreateUserAreaStats < ActiveRecord::Migration[5.0]
  def change
    create_table :user_area_stats, id: false do |t|
      t.string :id, limit: 36, primary_key: true, null: false
      t.string :app_version
      t.string :app_channel
      t.string :country
      t.string :province
      t.boolean :is_domestic
      t.integer :users_count
      t.integer :filter_type
      t.timestamps
    end
    add_index :user_area_stats, :id, unique: true
  end
end

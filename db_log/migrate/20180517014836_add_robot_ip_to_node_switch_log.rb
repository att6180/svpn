class AddRobotIpToNodeSwitchLog < ActiveRecord::Migration[5.0]
  def change
    add_column :node_switch_logs, :robot_ip, :string, limit: 20, after: :node_ip
  end
end

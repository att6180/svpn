class AddIpAreaToNodeOnlineUser < ActiveRecord::Migration[5.0]
  def change
    add_column :node_online_users, :ip_area, :string, after: :ip_city
  end
end

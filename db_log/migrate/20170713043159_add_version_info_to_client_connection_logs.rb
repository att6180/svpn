class AddVersionInfoToClientConnectionLogs < ActiveRecord::Migration[5.0]
  def change
    add_column :client_connection_logs, :app_channel, :string
    add_column :client_connection_logs, :app_version, :string
    add_column :client_connection_logs, :app_version_number, :string
  end
end

class CreateNodeSwitchStatuses < ActiveRecord::Migration[5.0]
  def change
    create_table :node_switch_statuses do |t|
      t.integer :node_id
      # 当前状态
      t.integer :status, default: 0
      # 总失败率
      t.integer :total_failed_rate, default: 0
      # 总测试次数
      t.integer :total_test_count, default: 0
      # 平均响应(ms)
      t.integer :average_response_time, default: 0
      # 最后15次失败率
      t.integer :last_failed_rate, default: 0
      # 最后次数
      t.integer :last_test_count, default: 0
      # 最后15次状态
      t.string :last_test_status
      # 上架次数
      t.integer :switch_on_count, default: 0
      # 下架次数
      t.integer :switch_off_count, default: 0
      # 总失败次数
      t.integer :total_failed_count, default: 0
      t.timestamps
    end
    add_index :node_switch_statuses, [:node_id, :created_at]
    add_index :node_switch_statuses, :total_failed_rate
    add_index :node_switch_statuses, :status
  end
end

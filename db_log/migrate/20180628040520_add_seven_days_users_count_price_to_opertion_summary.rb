class AddSevenDaysUsersCountPriceToOpertionSummary < ActiveRecord::Migration[5.0]
  def change
    add_column :log_operation_summary_day_stats, :seven_days_users_count, :integer, default: 0, after: :thirtieth_day_rate
    add_column :log_operation_summary_day_stats, :seven_days_total_price, :decimal, precision: 8, scale: 2, default: 0, after: :seven_days_users_count
    add_column :log_operation_summary_day_stats, :thirty_days_users_count, :integer, default: 0, after: :seven_days_total_price
    add_column :log_operation_summary_day_stats, :thirty_days_total_price, :decimal, precision: 8, scale: 2, default: 0, after: :thirty_days_users_count
    remove_column :log_operation_summary_day_stats, :payment_rate
    remove_column :log_operation_summary_day_stats, :seventh_payment_rate
    remove_column :log_operation_summary_day_stats, :thirtieth_payment_rate
    remove_column :log_operation_summary_day_stats, :seventh_day_rate
    remove_column :log_operation_summary_day_stats, :thirtieth_day_rate
  end
end

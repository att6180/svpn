class CreateLogUserTrafficLogs < ActiveRecord::Migration[5.0]
  def change
    create_table :log_user_traffic_logs do |t|
      t.integer :user_id, index: true
      t.date :stat_date, index: true
      t.integer :used_bytes, limit: 8, default: 0
      t.string :app_channel, index: true
      t.string :app_version, index: true
      t.timestamps
    end
    add_index :log_user_traffic_logs, [:stat_date, :user_id]
    add_index :log_user_traffic_logs, [:stat_date, :user_id, :app_channel, :app_version],
      name: 'log_user_traffic_logs_suaa'
  end
end

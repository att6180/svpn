class AddTagToUserActiveLogs < ActiveRecord::Migration[5.0]
  def change
    add_column :user_active_hour_logs, :active_type, :integer, default: 0, after: :app_channel
  end
end

class AddUserCreatedAtToTransactionLogs < ActiveRecord::Migration[5.0]
  def change
    add_column :transaction_logs, :user_created_at, :datetime
    add_index :transaction_logs, [:app_channel, :app_version, :status, :user_created_at, :created_at],
      name: "transaction_logs_aasuc"
    add_index :transaction_logs, [:status, :user_created_at, :created_at],
      name: "transaction_logs_suc"
  end
end

class CreateNodeConnectionServerLogs < ActiveRecord::Migration[5.0]
  def change
    create_table :node_connection_server_logs do |t|
      t.integer :user_id
      t.integer :node_id
      t.string :ip
      t.boolean :is_domestic
      t.timestamps
    end
    add_index :node_connection_server_logs, [:user_id, :created_at]
  end
end

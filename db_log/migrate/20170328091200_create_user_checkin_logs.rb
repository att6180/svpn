class CreateUserCheckinLogs < ActiveRecord::Migration[5.0]
  def change
    create_table :user_checkin_logs, id: false do |t|
      t.string :id, limit: 36, primary_key: true, null: false
      t.integer :user_id, index: true
      t.integer :present_time
      t.timestamps
    end
    add_index :user_checkin_logs, :id, unique: true
  end
end

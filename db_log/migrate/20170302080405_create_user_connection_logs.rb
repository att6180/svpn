class CreateUserConnectionLogs < ActiveRecord::Migration[5.0]
  def change
    create_table :user_connection_logs do |t|
      # user id
      t.integer :user_id, index: true
      # session id
      t.integer :session_id, index: true
      # node region id
      t.integer :node_region_id, index: true
      # node id
      t.integer :node_id, index: true
      # 目标服务器完整地址
      t.string :target_address
      # 目标服务器IP
      t.string :target_ip
      # 目标网站域名
      t.string :target_domain, index: true
      # 入站流量
      t.integer :in_bytes
      # 出站流量
      t.integer :out_bytes
      # 花费时间
      t.integer :cost_time
      # ip解析
      t.string :ip_country
      t.string :ip_province
      t.string :ip_city
      t.timestamps
    end
    add_index :user_connection_logs, :id, unique: true
    add_index :user_connection_logs, :created_at
  end
end

class CreateProxyTestDomainLogs < ActiveRecord::Migration[5.0]
  def change
    create_table :proxy_test_domain_logs do |t|
      t.integer :user_id, index: true
      t.integer :node_id, index: true
      t.integer :proxy_test_domain_id, index: true
      t.integer :try_times, default: 0
      t.integer :failure_times, default: 0
      t.string :client_ip
      t.string :ip_country
      t.string :ip_province
      t.string :ip_city
      t.timestamps
    end
  end
end

class CreateUserSigninPeriodDayStats < ActiveRecord::Migration[5.0]
  def change
    create_table :user_signin_period_day_stats do |t|
      t.date :stat_date, index: true
      t.string :app_channel, index: true
      t.string :app_version, index: true
      t.text :period_data
      t.timestamps
    end
    add_index :user_signin_period_day_stats, :created_at
  end
end

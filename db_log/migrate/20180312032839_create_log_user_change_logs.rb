class CreateLogUserChangeLogs < ActiveRecord::Migration[5.0]
  def change
    create_table :log_user_change_logs do |t|
      t.integer :user_id
      t.integer :change_type, default: 1
      t.string :uuid
      t.string :platform
      t.string :ip
      t.string :before
      t.string :after
      t.timestamps
    end
    add_index :log_user_change_logs, :user_id
    add_index :log_user_change_logs, :uuid
    add_index :log_user_change_logs, :ip
  end
end

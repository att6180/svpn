class AddUuidToClientConnectionLogs < ActiveRecord::Migration[5.0]
  def change
    add_column :client_connection_logs, :uuid, :string
    add_index :client_connection_logs, :uuid
  end
end

class AddVersionToTransactionStats < ActiveRecord::Migration[5.0]
  def change
      add_column :transaction_logs, :app_channel, :string, index: true
      add_column :transaction_logs, :app_version, :string, index: true

      add_column :transaction_stats, :app_channel, :string, index: true
      add_column :transaction_stats, :app_version, :string, index: true
      add_column :transaction_stats, :is_total, :boolean, default: false, index: true

      add_column :transaction_month_stats, :app_channel, :string, index: true
      add_column :transaction_month_stats, :app_version, :string, index: true
      add_column :transaction_month_stats, :is_total, :boolean, default: false, index: true

      add_column :consumption_logs, :app_channel, :string, index: true
      add_column :consumption_logs, :app_version, :string, index: true
      remove_column :consumption_logs, :node_region
      remove_column :consumption_logs, :node_name
  end
end

class CreateNodeConnectionUserDayLogs < ActiveRecord::Migration[5.0]
  def change
    create_table :node_connection_user_day_logs do |t|
      t.date :created_date
      t.integer :user_id
      t.datetime :user_registered_at
      t.string :app_channel
      t.string :app_version
      t.timestamps
    end
    add_index :node_connection_user_day_logs, [:created_date, :user_id], unique: true
    add_index :node_connection_user_day_logs, [:created_date, :user_registered_at, :app_channel, :app_version],
      name: 'node_connection_user_day_logs_cuaa'
  end
end

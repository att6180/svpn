class CreateNewUserHourStats < ActiveRecord::Migration[5.0]
  def change
    create_table :new_user_hour_stats do |t|
      t.date :stat_date
      t.integer :stat_hour
      t.integer :users_count, default: 0
      t.string :app_channel
      t.string :app_version
      t.integer :filter_type, default: 0
      t.timestamps
    end
    add_index :new_user_hour_stats, [:stat_date, :stat_hour, :app_channel, :app_version, :filter_type],
      name: "new_user_hour_stats_ssaa"
  end
end

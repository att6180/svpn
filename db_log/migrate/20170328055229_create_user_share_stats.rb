class CreateUserShareStats < ActiveRecord::Migration[5.0]
  def change
    create_table :user_share_stats, id: false do |t|
      t.string :id, limit: 36, primary_key: true, null: false
      t.string :app_version
      t.string :app_channel
      t.integer :share_type
      t.integer :users_count
      t.integer :times_count
      t.timestamps
    end
    add_index :user_share_stats, :id, unique: true
  end
end

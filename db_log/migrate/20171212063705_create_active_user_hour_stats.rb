class CreateActiveUserHourStats < ActiveRecord::Migration[5.0]
  def change
    create_table :active_user_hour_stats do |t|
      t.date :stat_date
      t.integer :stat_hour
      t.decimal :users_count, default: 0
      t.string :app_channel
      t.string :app_version
      t.integer :filter_type, default: 0
      t.timestamps
    end
    add_index :active_user_hour_stats, [:stat_date, :stat_hour, :app_channel, :app_version, :filter_type],
      name: "active_user_hour_stats_ssaaf"
  end
end

class CreateLogSplashScreenAdClickLogs < ActiveRecord::Migration[5.0]
  def change
    create_table :log_splash_screen_ad_click_logs, id: false do |t|
      t.integer :id, limit: 8, primary_key: true, auto_increment: true
      t.integer :splash_screen_ad_id, null: false
      t.string :uuid
      t.string :app_channel
      t.string :app_version
      t.string :app_version_number
      t.date :created_date
      t.timestamps
    end
    add_index :log_splash_screen_ad_click_logs, [:app_channel, :app_version, :created_at],
      name: 'ssadcl_aac'
  end
end

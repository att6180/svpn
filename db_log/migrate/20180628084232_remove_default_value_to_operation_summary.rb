class RemoveDefaultValueToOperationSummary < ActiveRecord::Migration[5.0]
  def change

    change_column_default(:log_operation_summary_day_stats, :seven_days_users_count, nil)
    change_column_default(:log_operation_summary_day_stats, :seven_days_total_price, nil)
    change_column_default(:log_operation_summary_day_stats, :thirty_days_users_count, nil)
    change_column_default(:log_operation_summary_day_stats, :thirty_days_total_price, nil)
    change_column_default(:log_operation_summary_day_stats, :second_day, nil)
    change_column_default(:log_operation_summary_day_stats, :seventh_day, nil)
    change_column_default(:log_operation_summary_day_stats, :thirtieth_day, nil)
  end
end

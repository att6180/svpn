class AddUniqueIndexToUserCheckinLog < ActiveRecord::Migration[5.0]
  def change
    add_index :user_checkin_logs, [:user_id, :created_at], unique: true
  end
end

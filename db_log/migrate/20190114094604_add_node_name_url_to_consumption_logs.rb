class AddNodeNameUrlToConsumptionLogs < ActiveRecord::Migration[5.0]
  def change
    add_column :consumption_logs, :node_id, :integer
    add_column :consumption_logs, :node_name, :string, limit: 191
    add_column :consumption_logs, :node_url, :string, limit: 191
    add_index :consumption_logs, [:node_name, :node_url]
  end
end

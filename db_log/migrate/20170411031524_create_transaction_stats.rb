class CreateTransactionStats < ActiveRecord::Migration[5.0]
  def change
    create_table :transaction_stats, id: false do |t|
      t.string :id, limit: 36, primary_key: true, null: false
      t.date :stat_date, index: true
      t.decimal :total_recharge_amount, precision: 8, scale: 2, default: 0
      t.integer :consume_coins, default: 0
      t.integer :recharge_users_count, default: 0
      t.timestamps
    end
    add_index :transaction_stats, :id, unique: true
    add_index :transaction_stats, :created_at
  end
end

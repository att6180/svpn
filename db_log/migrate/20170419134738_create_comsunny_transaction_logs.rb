class CreateComsunnyTransactionLogs < ActiveRecord::Migration[5.0]
  def change
    create_table :comsunny_transaction_logs, id: false do |t|
      t.string :id, limit: 36, primary_key: true, null: false
      t.integer :user_id, index: true
      t.integer :timestamp, limit: 8
      t.string :channel_type
      t.string :sub_channel_type
      t.string :transaction_type
      t.string :transaction_id
      t.integer :transaction_fee
      t.boolean :trade_success
      t.text :message_detail
      t.text :optional
      t.timestamps
    end
    add_index :comsunny_transaction_logs, :id, unique: true
    add_index :comsunny_transaction_logs, :created_at
  end
end

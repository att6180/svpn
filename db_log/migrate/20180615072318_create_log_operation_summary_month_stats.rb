class CreateLogOperationSummaryMonthStats < ActiveRecord::Migration[5.0]
  def change
    create_table :log_operation_summary_month_stats do |t|
      t.integer :stat_year
      t.integer :stat_month
      t.decimal :total_recharge_amount, precision: 8, scale: 2, default: 0
      t.integer :new_users_count, default: 0
      t.integer :active_users_count, default: 0
      t.integer :platform_id, index: true
      t.integer :channel_account_id, index: true
      t.integer :package_id, index: true
      t.string :app_channel, index: true
      t.string :app_version, index: true
      t.integer :filter_type
      t.timestamps
    end
  end
end

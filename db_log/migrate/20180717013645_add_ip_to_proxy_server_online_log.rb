class AddIpToProxyServerOnlineLog < ActiveRecord::Migration[5.0]
  def change
    add_column :proxy_server_online_users_logs, :ip, :string
  end
end

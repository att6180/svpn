class AddSeventhTotalPriceAndThirtiethTotalPriceToUserRetentionRateStats < ActiveRecord::Migration[5.0]
  def change
    add_column :user_retention_rate_stats, :seventh_17_total_price, :decimal, precision: 12, scale: 2
    add_column :user_retention_rate_stats, :thirtieth_130_total_price, :decimal, precision: 12, scale: 2
  end
end

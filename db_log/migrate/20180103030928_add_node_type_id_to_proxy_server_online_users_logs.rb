class AddNodeTypeIdToProxyServerOnlineUsersLogs < ActiveRecord::Migration[5.0]
  def change
    add_column :proxy_server_online_users_logs, :node_type_id, :integer, index: true
  end
end

class RmoveUnusefulIndexFromUserConnectionLogs < ActiveRecord::Migration[5.0]
  def change
    remove_index :user_connection_logs, :id
    remove_index :user_connection_logs, :session_id
    remove_index :user_connection_logs, :node_region_id
    remove_index :user_connection_logs, :node_id
    remove_index :user_connection_logs, :target_domain
  end
end

class CreateUserShareLogs < ActiveRecord::Migration[5.0]
  def change
    create_table :user_share_logs, id: false do |t|
      t.string :id, limit: 36, primary_key: true, null: false
      t.integer :user_id, index: true
      t.integer :share_type, default: 0, index: true
      t.string :app_channel, index: true
      t.string :app_version, index: true
      t.string :app_version_number, index: true
      t.timestamps
    end
    add_index :user_share_logs, :id, unique: true
    add_index :user_share_logs, :created_at
  end
end

class CreateNodeConnectionCountScopeDayStats < ActiveRecord::Migration[5.0]
  def change
    create_table :node_connection_count_scope_day_stats, id: false do |t|
      t.string :id, limit: 36, primary_key: true, null: false
      t.date :stat_date, index: true
      t.integer :users_count, index: true
      t.integer :count_1_2, default: 0
      t.integer :count_3_5, default: 0
      t.integer :count_6_9, default: 0
      t.integer :count_10_19, default: 0
      t.integer :count_20_49, default: 0
      t.integer :count_50, default: 0
      t.string :app_channel, index: true
      t.string :app_version, index: true
      t.timestamps
    end
    add_index :node_connection_count_scope_day_stats, :id, unique: true
  end
end

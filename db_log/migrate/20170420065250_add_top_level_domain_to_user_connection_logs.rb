class AddTopLevelDomainToUserConnectionLogs < ActiveRecord::Migration[5.0]
  def change
    add_column :user_connection_logs, :top_level_domain, :string, index: true
  end
end

class CreateLogWebsiteNavigationClickDayStats < ActiveRecord::Migration[5.0]
  def change
    create_table :log_website_navigation_click_day_stats do |t|
      t.date :stat_date, index: true
      t.string :website_name
      t.boolean :is_domestic 
      t.integer :info_type
      t.integer :clicked_count
      t.timestamps
    end
  end
end

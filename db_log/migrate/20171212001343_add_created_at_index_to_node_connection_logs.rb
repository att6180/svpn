class AddCreatedAtIndexToNodeConnectionLogs < ActiveRecord::Migration[5.0]
  def change
    add_index :node_connection_logs, :created_at
  end
end

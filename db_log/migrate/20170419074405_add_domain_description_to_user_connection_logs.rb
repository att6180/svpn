class AddDomainDescriptionToUserConnectionLogs < ActiveRecord::Migration[5.0]
  def change
    add_column :user_connection_logs, :domain_description, :string
    remove_column :user_connection_logs, :ip_country
    remove_column :user_connection_logs, :ip_province
    remove_column :user_connection_logs, :ip_city
  end
end

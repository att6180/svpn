class AddRegisteredAtToNodeConnectionLog < ActiveRecord::Migration[5.0]
  def change
    # 同时记录用户的注册时间，方便留存率的统计
    add_column :node_connection_logs, :user_registered_at, :datetime, index: true
    add_column :node_connection_logs, :app_channel, :string, index: true
    add_column :node_connection_logs, :app_version, :string, index: true
  end
end

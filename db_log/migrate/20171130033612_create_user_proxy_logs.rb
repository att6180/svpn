class CreateUserProxyLogs < ActiveRecord::Migration[5.0]
  def change
    create_table :user_proxy_logs do |t|
      t.integer :user_id
      t.date :used_date
      t.integer :total_bytes, limit: 8
      t.string :last_url
      t.string :app_channel
      t.string :app_version
      t.timestamps
    end
    add_index :user_proxy_logs, [:user_id, :used_date], unique: true
    add_index :user_proxy_logs, [:used_date, :total_bytes, :app_channel, :app_version],
      name: 'user_proxy_logs_utaa'
  end
end

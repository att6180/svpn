class CreateProxyServerStatusLogs < ActiveRecord::Migration[5.0]
  def change
    create_table :proxy_server_status_logs do |t|
      t.integer :node_id, index: true
      t.decimal :bandwidth_percent, precision: 8, scale: 5, default: 0
      t.decimal :cpu_percent, precision: 4, scale: 1, default: 0
      t.decimal :memory_percent, precision: 4, scale: 1, default: 0
      t.integer :transfer, limit: 8, default: 0
      t.integer :network_speed_up, limit: 8, default: 0
      t.integer :network_speed_down, limit: 8, default: 0
      t.integer :disk_percent, default: 0
      t.timestamps
    end
    add_index :proxy_server_status_logs, :created_at
  end
end

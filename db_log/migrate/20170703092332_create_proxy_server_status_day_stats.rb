class CreateProxyServerStatusDayStats < ActiveRecord::Migration[5.0]
  def change
    create_table :proxy_server_status_day_stats do |t|
      t.date :stat_date, index: true
      t.integer :node_id, index: true

      t.decimal :min_bandwidth_percent, precision: 6, scale: 3, default: 0
      t.datetime :min_bandwidth_time
      t.decimal :max_bandwidth_percent, precision: 6, scale: 3, default: 0
      t.datetime :max_bandwidth_time
      t.decimal :average_bandwidth_percent, precision: 6, scale: 3, default: 0

      t.decimal :min_cpu_percent, precision: 4, scale: 1, default: 0
      t.datetime :min_cpu_time
      t.decimal :max_cpu_percent, precision: 4, scale: 1, default: 0
      t.datetime :max_cpu_time
      t.decimal :average_cpu_percent, precision: 4, scale: 1, default: 0

      t.decimal :min_memory_percent, precision: 4, scale: 1, default: 0
      t.datetime :min_memory_time
      t.decimal :max_memory_percent, precision: 4, scale: 1, default: 0
      t.datetime :max_memory_time
      t.decimal :average_memory_percent, precision: 4, scale: 1, default: 0

      t.integer :min_network_speed_up, default: 0
      t.datetime :min_network_speed_up_time
      t.integer :max_network_speed_up, default: 0
      t.datetime :max_network_speed_up_time
      t.integer :average_network_speed_up, default: 0

      t.integer :min_network_speed_down, default: 0
      t.datetime :min_network_speed_down_time
      t.integer :max_network_speed_down, default: 0
      t.datetime :max_network_speed_down_time
      t.integer :average_network_speed_down, default: 0

      t.decimal :min_disk_percent, precision: 4, scale: 1, default: 0
      t.datetime :min_disk_time
      t.decimal :max_disk_percent, precision: 4, scale: 1, default: 0
      t.datetime :max_disk_time
      t.decimal :average_disk_percent, precision: 4, scale: 1, default: 0

      t.timestamps
    end
  end
end

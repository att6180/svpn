class CreateNodeConnectionLogs < ActiveRecord::Migration[5.0]
  def change
    create_table :node_connection_logs, id: false do |t|
      t.string :id, limit: 36, primary_key: true, null: false
      t.integer :user_id, index: true
      t.integer :node_type_id, index: true
      t.integer :node_region_id, index: true
      t.integer :node_id, index: true
      t.string :node_type_name
      t.string :node_region_name
      t.string :node_name
      t.integer :wait_time, default: 0
      t.string :ip
      t.integer :status, default: 0
      t.timestamps
    end
    add_index :node_connection_logs, :id, unique: true
    add_index :node_connection_logs, :created_at
  end
end

class CreateUserConnectionRegionStats < ActiveRecord::Migration[5.0]
  def change
    create_table :user_connection_region_stats, id: false do |t|
      t.string :id, limit: 36, primary_key: true, null: false
      t.integer :user_id, index: true
      t.string :domain, index: true
      t.string :ip
      t.string :ip_country, index: true
      t.string :ip_province, index: true
      t.string :ip_city, index: true
      t.integer :visits_count, default: 0
      t.timestamps
    end
    add_index :user_connection_region_stats, :id, unique: true
    add_index :user_connection_region_stats, :created_at
  end
end

class AddIsSuccessedToProxyTestDomainLogs < ActiveRecord::Migration[5.0]
  def change
    add_column :proxy_test_domain_logs, :is_successed, :boolean, index: true, default: true
    remove_column :proxy_test_domain_logs, :client_ip
    remove_column :proxy_test_domain_logs, :ip_country
    remove_column :proxy_test_domain_logs, :ip_province
    remove_column :proxy_test_domain_logs, :ip_city
    remove_column :proxy_test_domain_logs, :try_times
    remove_column :proxy_test_domain_logs, :failure_times
  end
end

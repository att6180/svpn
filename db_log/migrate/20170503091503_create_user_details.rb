class CreateUserDetails < ActiveRecord::Migration[5.0]
  def change
    create_table :user_details, id: false do |t|
      t.string :id, limit: 36, primary_key: true, null: false
      t.integer :user_id
      t.integer :used_bytes, limit: 8, default: 0
      t.datetime :last_connection_at, index: true
      t.integer :last_connected_node_id
      t.datetime :last_inspect_expiration_at
      t.timestamps
    end
    add_index :user_details, :id, unique: true
    add_index :user_details, :user_id, unique: true

    add_column :connection_failed_stats, :stat_date, :date

  end
end

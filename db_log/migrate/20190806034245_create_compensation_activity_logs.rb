class CreateCompensationActivityLogs < ActiveRecord::Migration[5.0]
  def change
    create_table :compensation_activity_logs, id: false do |t|
      t.string :id, limit: 36, primary_key: true, null: false
      t.integer :user_id
      t.integer :promo_user_id
      t.datetime :user_signup_at, comment: '用户注册时间'
      t.datetime :user_recharge_at, comment: '用户充值时间'
      t.integer :node_type_id, comment: '套餐id'
      t.integer :reward_days, comment: '奖励天数'
      t.timestamps
      t.index ["user_id"], name: "index_activity_logs_on_user_id", using: :btree
      t.index ["promo_user_id"], name: "index_activity_logs_on_promo_user_id", using: :btree
    end
  end
end

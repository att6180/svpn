class CreateUserOperationLogs < ActiveRecord::Migration[5.0]
  def change
    create_table :user_operation_logs, id: false do |t|
      # id
      t.string :id, limit: 36, primary_key: true, null: false
      # 用户ID
      t.integer :user_id, index: true
      # 设备id
      t.integer :device_id, index: true
      # 渠道
      t.string :app_channel, index: true
      # 版本名
      t.string :app_version, index: true
      # 版本号
      t.string :app_version_number
      # app运行日志ID
      t.string :user_app_launch_log_id, limit: 36, index: true
      # app登录日志ID
      t.string :user_signin_log_id, limit: 36, index: true
      # 操作界面ID
      t.integer :interface_id, index: true
      t.timestamps
    end
    add_index :user_operation_logs, :id, unique: true
    add_index :user_operation_logs, :created_at
  end
end

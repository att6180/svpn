class AddOptimizeIndexToUserOperationLogs < ActiveRecord::Migration[5.0]
  def change
    remove_index :user_operation_logs, :user_id
    remove_index :user_operation_logs, :device_id
    remove_index :user_operation_logs, :app_channel
    remove_index :user_operation_logs, :app_version
    remove_index :user_operation_logs, :user_app_launch_log_id
    remove_index :user_operation_logs, :user_signin_log_id
    remove_index :user_operation_logs, :interface_id

    add_index :user_operation_logs,
      [:interface_id, :user_id, :app_channel, :app_version, :created_at],
      name: 'iuol_iuaac'
  end
end

class OptimizeIndexFromNodeConnectionLogs < ActiveRecord::Migration[5.0]
  def change
    remove_index :node_connection_logs, :user_id
    remove_index :node_connection_logs, :created_at

    add_index :node_connection_logs,
      [:user_id, :user_registered_at, :app_channel, :app_version, :created_at],
      name: 'ncl_uuaac'
  end
end

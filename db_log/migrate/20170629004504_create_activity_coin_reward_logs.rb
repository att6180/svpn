class CreateActivityCoinRewardLogs < ActiveRecord::Migration[5.0]
  def change
    create_table :activity_coin_reward_logs do |t|
      t.integer :admin_id, index: true
      t.integer :user_id, index: true
      t.integer :original_coins, default: 0
      t.integer :current_coins, default: 0
      # default for modify coins
      t.integer :operation_type, default: 0
      t.timestamps
    end
  end
end

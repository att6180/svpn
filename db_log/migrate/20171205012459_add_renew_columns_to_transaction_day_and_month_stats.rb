class AddRenewColumnsToTransactionDayAndMonthStats < ActiveRecord::Migration[5.0]
  def change
      add_column :transaction_stats, :renew_users_count, :integer, default: 0
      add_column :transaction_stats, :renew_new_users_count, :integer, default: 0
      add_column :transaction_stats, :renew_amount, :decimal, precision: 12, scale: 2, default: 0
      add_column :transaction_stats, :renew_new_amount, :decimal, precision: 12, scale: 2, default: 0
      add_column :transaction_stats, :filter_type, :integer, default: 0
      remove_column :transaction_stats, :is_total
      add_index :transaction_stats,
        [:stat_date, :app_channel, :app_version, :filter_type],
        name: "transaction_stats_saaf"

      add_column :transaction_month_stats, :renew_users_count, :integer, default: 0
      add_column :transaction_month_stats, :renew_new_users_count, :integer, default: 0
      add_column :transaction_month_stats, :renew_amount, :decimal, precision: 12, scale: 2, default: 0
      add_column :transaction_month_stats, :renew_new_amount, :decimal, precision: 12, scale: 2, default: 0
      add_column :transaction_month_stats, :filter_type, :integer, default: 0
      remove_column :transaction_month_stats, :is_total
      add_index :transaction_month_stats,
        [:year, :app_channel, :app_version, :filter_type],
        name: "transaction_month_stats_saaf"
  end
end

class CreateClientConnectionLogs < ActiveRecord::Migration[5.0]
  def change
    create_table :client_connection_logs do |t|
      t.integer :user_id
      t.integer :node_id
      t.string :node_ip
      t.string :target_url
      t.string :client_ip
      t.string :client_ip_country
      t.string :client_ip_province
      t.string :client_ip_city
      t.boolean :is_proxy
      t.text :addition_content
      t.timestamps
    end
    add_index :client_connection_logs, :created_at
  end
end

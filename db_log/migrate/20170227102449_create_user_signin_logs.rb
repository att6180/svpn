class CreateUserSigninLogs < ActiveRecord::Migration[5.0]
  def change
    create_table :user_signin_logs, id: false do |t|
      # id
      t.string :id, limit: 36, primary_key: true, null: false
      # 用户ID
      t.integer :user_id, index: true
      # Session ID
      t.integer :session_id, index: true
      # 设备uuid
      t.integer :device_id, index: true
      # 设备名
      t.string :device_name
      # 设备型号
      t.string :device_model
      # 设备平台
      t.string :platform
      # 设备系统版本
      t.string :system_version
      # 运营商
      t.string :operator
      # 网络环境
      t.string :net_env
      # app版本名
      t.string :app_version
      # app版本号
      t.string :app_version_number
      # app渠道
      t.string :app_channel
      # ip
      t.string :ip
      t.string :ip_country
      t.string :ip_province
      t.string :ip_city
      # 经度
      t.decimal :longitude, precision: 16, scale: 8, default: 0
      # 纬度
      t.decimal :latitude, precision: 16, scale: 8, default: 0
      # 地址
      t.string :address
      # 获取方法(ip, gps)
      t.string :coord_method
      t.timestamps
    end
    add_index :user_signin_logs, :id, unique: true
    add_index :user_signin_logs, :created_at
  end
end

# SVPN后端服务

后端服务包含:
- 客户端API
- 代理服务器API
- 后台管理API
- 客户端和代理服务器API文档(/doc/index)
- 后台管理API在线文档(/doc/manage)

### 环境
- ruby: 2.3.3
- rails: >= 5.0.2
- mysql: >= 5.7
- redis: >= 3.2.8
- nodejs: >= 4.7

### 项目本地初始化流程

```bash
# 安装依赖 for Ubuntu 16.04
apt install -y software-properties-common build-essential git-core wget \
zlib1g-dev libssl-dev libreadline-dev libyaml-dev \
libxml2-dev libxslt1-dev libffi-dev curl libcurl3 \
libcurl3-gnutls libcurl4-openssl-dev nodejs \
mysql-server libmysqlclient-dev redis-server

# 安装依赖 for macOS(先安装xcode)
xcode-select --install
ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
brew install libxml2 libxslt libiconv mysql redis
brew services start mysql
brew services start redis

# 拉取代码库
git clone git@bitbucket.org:zhy8871/fulmo3.git
# 安装依赖
bundle install
# 创建默认数据库
rails db:create
rails log:db:create
rails manage:db:create
rails misc:db:create
# 导入数据库迁移
rails db:schema:load
rails log:db:schema:load
rails manage:db:schema:load
rails misc:db:schema:load
# 创建mongodb索引
rake db:mongoid:create_indexes
# 导入默认种子数据
rails db:seed
# 本地运行
rails s
```

### 代码分支与服务器部署

- dev分支对应开发服(http://xiaoguoqi.com)
- staging分支对应测试服(http://staging.xiaoguoqi.com)
- master分支对应生产服
- police分支对应警用版

- 部署开发服: `bundle exec mina dev deploy`
- 部署测试服: `bundle exec mina staging deploy`
- 部署开发服:

```
bundle exec mina pro_cron deploy && \
bundle exec mina pro1 deploy && \
bundle exec mina pro2 deploy && \
bundle exec mina pro3 deploy && \
bundle exec mina pro4 deploy
```
- 部署警用版本服: `bundle exec mina police1 deploy`

# API文档编写及生成流程

- 客户端与代理服务器API文档源文件位置: `misc/libs/slate/source/index.html.md`

```bash
# 编辑项目API文档
vim misc/libs/slate/source/
# 进入文档目录
cd misc/libs/slate/
# 安装依赖
bundle install
# 进入后端服务项目根目录
cd svpn_backend
# 生成
./docs_build
```

- 后台管理api文档在增删改接口后，在命令行执行如下命令更新文档:

```
rails swagger:docs
```

class CreateVerificationCodes < ActiveRecord::Migration[5.0]

  def change
    create_table :verification_codes do |t|
      t.string :telephone, index: true
      t.string :verification_code
      t.datetime :expired_at, index: true
      t.boolean :is_used, default: false
      t.datetime :used_at
      t.timestamps
    end
  end

end

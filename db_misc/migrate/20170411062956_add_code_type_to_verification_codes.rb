class AddCodeTypeToVerificationCodes < ActiveRecord::Migration[5.0]
  def change
    add_column :verification_codes, :code_type, :integer, default: 0
  end
end

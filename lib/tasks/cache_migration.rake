namespace :cache_migration do

  # 将今日已签到的用户加入缓存
  task checkin_cache_today: :environment do
    total_pages = UserCheckinLog.today.page(1).per(100).total_pages
    current_page = 1
    while current_page <= total_pages
      logs = UserCheckinLog.today.page(current_page).per(500)
      logs.each do |log|
        Rails.cache.write( "#{User::CACHE_KEY_CHECKIN_TODAY}#{log.user_id}", 1, expires_in: DateUtils.remaining_time)
      end
      current_page += 1
    end
  end

end

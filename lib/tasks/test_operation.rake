namespace :test_operation do

  # 测试uuid主键插入性能
  task db_performance: :environment do
    ActiveSupport::Notifications.subscribe('sql.active_record') do |*args|
      event = ActiveSupport::Notifications::Event.new(*args)
      puts event.duration #how long it took to run the sql command
    end
    (1..10000000).each do |i|
      #time = Benchmark.measure do
        UserSigninLog.create(
          id: Utils::Gen.generate_uuid,
          user_id: 25,
          session_id: 26,
          device_id: 24,
          device_name: 'jonasIphone',
          device_model: 'matebook',
          platform: 'pc',
          system_version: '1.0.0',
          operator: 'china_mobile',
          net_env: 'wifi',
          app_version: 'jichu',
          app_version_number: '1.0.0',
          app_channel: 'moren',
          ip: '127.0.0.1',
          ip_country: '本机地址',
          ip_province: '本机地址'
        )
      #end
      #puts time
    end
  end

  task clear: :environment do
    SystemEnum.delete_all
    NodeRegion.delete_all
    Node.delete_all
    UserGroup.delete_all
    User.delete_all
    Device.delete_all
    UserSession.delete_all
    UserAppLaunchLog.delete_all
    UserSigninLog.delete_all

    VerificationCode.delete_all

    Manage::Admin.delete_all
  end

  task reset_all_db: :environment do
    system 'RAILS_ENV=development bundle exec rails db:drop'
    system 'RAILS_ENV=development bundle exec rails log:db:drop'
    system 'RAILS_ENV=development bundle exec rails misc:db:drop'
    system 'RAILS_ENV=development bundle exec rails manage:db:drop'

    system 'RAILS_ENV=development bundle exec rails db:create'
    system 'RAILS_ENV=development bundle exec rails log:db:create'
    system 'RAILS_ENV=development bundle exec rails misc:db:create'
    system 'RAILS_ENV=development bundle exec rails manage:db:create'

    system 'RAILS_ENV=development bundle exec rails db:migrate'
    system 'RAILS_ENV=development bundle exec rails log:db:migrate'
    system 'RAILS_ENV=development bundle exec rails misc:db:migrate'
    system 'RAILS_ENV=development bundle exec rails manage:db:migrate'
  end

  task user_share: :environment do
    app_channels = SystemEnum.enums_by_type(0)
    app_versions = SystemEnum.enums_by_type(1)
    user_id = User.last.id
    (1..100).each do |i|
      app_channel = app_channels[rand(0..(app_channels.count-1))].name
      app_version = app_versions[rand(0..(app_versions.count-1))].name
      UserShareLog.create(
        user_id: user_id,
        app_channel: app_channel,
        app_version: app_version,
        app_version_number: "0.2",
        share_type: UserShareLog.share_types.keys[rand(0..3)]
      )
    end
  end

  # 创建测试连接数据
  task node_connection_logs: :environment do
    (1..142).each do |index|
      user = User.find(rand(1..142))
      user.node_connection_logs.create(
        node_type_id: rand(1..2),
        node_id: rand(1..9),
        node_region_id: rand(1..3),
        node_type_name: '精英服务器',
        node_region_name: '香港',
        wait_time: rand(1..1000),
        node_name: '香港01',
        ip: '192.168.1.1',
        status: 'success',
        user_registered_at: user.created_at,
        created_at: "2017-04-#{rand(1..30)} 11:00:00"
      )
    end
  end

  # 测啥redis自动过期效率
  task redis_expire: :environment do
    (1..500000).each do |index|
      cache_key = "verification_code_replay_#{index}"
      replay_count = RedisCache.incr(cache_key, Random.rand(10))
    end
  end

end

namespace :transaction_stat_migration do

  # 更新套餐名
  # 此任务因为2017-11-10下午做活动，把六个套餐的套餐名更改
  # 导致客户端之前下的订单历史中的套餐名也随之更新成非当时下单的套餐（因为数据库用plan_id关联)
  # 现将订单表增加一个plan_name字段，用于记录当时下单的套餐名，从而让客户端能正常显示当时的套餐名
  task update_plan_name: :environment do
    # 2017-11-10 15:21之前的订单补上plan_name
    tl_base = TransactionLog.where("created_at <= '2017-11-10 15:21'")
    origin_names = [
      {id: 8, name: "6钻", coins: 6, present_coins: 0},
      {id: 9, name: "30钻", coins: 30, present_coins: 0},
      {id: 10, name: "50钻", coins: 50, present_coins: 0},
      {id: 44, name: "6钻", coins: 6, present_coins: 0},
      {id: 45, name: "30钻", coins: 30, present_coins: 0},
      {id: 46, name: "50钻", coins: 50, present_coins: 0},
    ]
    puts origin_names.inspect
    # 取出tl_base的所有plan_id，并剔除掉origin_names里的plan_ids，然后更新plan_name
    plan_ids = tl_base.select(:plan_id).group(:plan_id).pluck(:plan_id)
    re_plan_ids = plan_ids - origin_names.map{|n| n[:id]}
    puts re_plan_ids.inspect
    # 剩下的套餐id与名称对应，并更新至订单表
    plans = Plan.select(:id, :name, :coins, :present_coins)
      .where(id: re_plan_ids)
      .to_a
    re_origin_names = []
    plans.each do |plan|
      re_origin_names << {
        id: plan.id,
        name: plan.name,
        coins: plan.coins,
        present_coins: plan.present_coins
      }
    end
    re_origin_names = re_origin_names + origin_names
    puts re_origin_names.inspect
    re_origin_names.each do |n|
      tl_base.where(plan_id: n[:id]).update_all(
        plan_name: n[:name],
        plan_coins: n[:coins],
        present_coins: n[:present_coins]
      )
    end
    # 2017-11-10 15:21之后的订单补上plan_name
    new_tl_base = TransactionLog.where("created_at > '2017-11-10 15:21'")
    plan_ids = new_tl_base.select(:plan_id).group(:plan_id).pluck(:plan_id)
    origin_names = Plan.select(:id, :name, :coins, :present_coins).where(id: plan_ids).to_a.map(&:serializable_hash)
    puts origin_names.inspect
    origin_names.each do |n|
      new_tl_base.where(plan_id: n["id"]).update_all(
        plan_name: n["name"],
        plan_coins: n["coins"],
        present_coins: n["present_coins"]
      )
    end
  end

  # 今日充值总额重载
  task recharge_amount_today: :environment do
    result = TransactionLog.by_date(Time.now.to_date).by_paid.sum(:price).to_i
    cache_key = "#{TransactionLog::CACHE_KEY_RECHARGE_AMOUNT}#{DateUtils.time2ymd(Time.now)}"
    $redis.set(cache_key, result)
    $redis.expireat(cache_key, Time.now.end_of_day.to_i)
  end

  # 今日消费钻石重载
  task consume_coins_today: :environment do
    result = ConsumptionLog.by_date(Time.now.to_date).sum(:coins)
    cache_key = "#{ConsumptionLog::CACHE_KEY_CONSUME_COINS}#{DateUtils.time2ymd(Time.now)}"
    $redis.set(cache_key, result)
    $redis.expireat(cache_key, Time.now.end_of_day.to_i)
  end

  # 今日充值人数（去重）重载
  task recharge_users_today: :environment do
    cache_key = "#{TransactionLog::CACHE_KEY_RECHARGE_USERS}#{DateUtils.time2ymd(Time.now)}"
    logs = TransactionLog.select("user_id").by_date(Time.now.to_date).by_paid.distinct(:user_id)
    logs.each do |log|
      $redis.sadd(cache_key, log.user_id)
    end
    $redis.expireat(cache_key, Time.now.end_of_day.to_i)
  end

  # 本月充值人数（去重）重载
  task recharge_users_month: :environment do
    now = Time.now
    year = now.year
    month = now.month
    logs = TransactionLog.select("user_id").by_year_and_month(year, month).by_paid.distinct(:user_id)
    year_month = now.strftime("%Y%m")
    cache_key = "#{TransactionLog::CACHE_KEY_RECHARGE_USERS}#{year_month}"
    logs.each do |log|
      $redis.sadd(cache_key, log.user_id)
    end
    $redis.expireat(cache_key, Time.now.end_of_month.to_i)
  end

  # 本年充值人数（去重）重载
  task recharge_users_year: :environment do
    now = Time.now
    year = now.year
    cache_key = "#{TransactionLog::CACHE_KEY_RECHARGE_USERS}#{year}"
    logs = TransactionLog.select("user_id").by_year(year).by_paid.distinct(:user_id)
    logs.each do |log|
      $redis.sadd(cache_key, log.user_id)
    end
    $redis.expireat(cache_key, Time.now.end_of_year.to_i)
  end
end


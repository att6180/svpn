require 'csv'

namespace :manual_migration_temp do

  # 服务器在线补全服务类型字段
  task migrate_proxy_server_online_users_node_type_id: :environment do
    ProxyServerOnlineUsersLog.where("created_at > ?", Time.now.beginning_of_year)
      .order(created_at: :desc)
      .find_in_batches(start: 1, batch_size: 1000) do |logs|
        nodes = Node.where(id: logs.map(&:node_id)).to_a.group_by(&:id)
        logs.each do |log|
          log.update(node_type_id: nodes[log.node_id]&.first&.node_type_id)
        end
      end

    first_date = Time.now.beginning_of_year.to_date
    ProxyServerOnlineUsersDayStat.where("stat_date >= ?", first_date).delete_all
    end_date = Time.now.to_date
    while first_date < end_date
      ProxyServerOnlineUsersDayStat.stat(first_date)
      first_date = first_date.next_day
    end
  end

  # 将登录日志表的数据迁移到登录日志小时的二级表，加快活跃统计查询
  task :migrate_user_signin_hour_logs, [:begin_user_id] => [:environment] do |t, args|
    begin_id = args.has_key?(:begin_user_id) ? args[:begin_user_id].to_i : nil

    UserActiveHourLog.delete_all if begin_id.nil?
    SQL_PREFIX = "INSERT IGNORE INTO user_signin_hour_logs(" + \
      "created_date, created_hour, user_id, app_channel, app_version, created_at, updated_at) VALUES".freeze
    sql = ""
    page_size = 2000
    current_page = 1
    users = User.select(:id).order(id: :asc)
    users = users.where("id >= ?", begin_id) if !begin_id.nil?
    users.find_in_batches(start: 1, batch_size: 50) do |users|
      logs = ApplicationLogRecord.using_db(DB_LOG_SLAVE1) do
        UserSigninLog.select("user_id, app_channel, app_version, created_at")
          .where(user_id: users.map(&:id))
          .page(current_page).per(page_size)
      end
      current_page = 1
      total_pages = logs.total_pages
      while total_pages >= current_page
        logs = ApplicationLogRecord.using_db(DB_LOG_SLAVE1) do
          UserSigninLog.select("user_id, app_channel, app_version, created_at")
            .where(user_id: users.map(&:id))
            .page(current_page).per(page_size).to_a
        end
        #puts "origin size: #{logs.size}"
        logs.uniq! {|log| "#{log.created_at.to_date}_#{log.created_at.hour}_#{log.user_id}"}
        #puts "compress size: #{logs.size}"
        logs.each do |log|
          sql << <<-EOF
            ('#{log.created_at.to_date}', #{log.created_at.hour}, #{log.user_id},
            '#{log.app_channel}', '#{log.app_version}',
            '#{log.created_at}', '#{log.created_at}'),
          EOF
        end
        UserActiveHourLog.connection.execute("#{SQL_PREFIX}#{sql.chomp('').chomp(',')};")
        sql = ""
        puts "pages: #{current_page} / #{total_pages}"
        current_page += 1
      end
      puts "last user_id: #{users&.last&.id}"
    end
  end

  # 将连接日志表的数据迁移到连接日志的二级表(按用户id)，加快留存统计查询
  task :migrate_user_connection_user_day_logs2, [:begin_user_id] => [:environment] do |t, args|
    begin_id = args.has_key?(:begin_user_id) ? args[:begin_user_id].to_i : nil

    NodeConnectionUserDayLog.delete_all if begin_id.nil?
    SQL_PREFIX = "INSERT IGNORE INTO node_connection_user_day_logs(" + \
      "created_date, user_id, user_registered_at, app_channel, app_version, created_at, updated_at) VALUES".freeze
    sql = ""
    page_size = 1000
    current_page = 1
    users = User.select(:id).order(id: :asc)
    users = users.where("id >= ?", begin_id) if !begin_id.nil?
    users.find_in_batches(start: 1, batch_size: 50) do |users|
      logs = NodeConnectionLog.using_db(DB_LOG_SLAVE1) do
        NodeConnectionLog.select("user_id, user_registered_at, app_channel, app_version, created_at")
          .where(user_id: users.map(&:id))
          .page(current_page).per(page_size)
      end
      current_page = 1
      total_pages = logs.total_pages
      while total_pages >= current_page
        logs = NodeConnectionLog.using_db(DB_LOG_SLAVE1) do
          NodeConnectionLog.select("user_id, user_registered_at, app_channel, app_version, created_at")
            .where(user_id: users.map(&:id))
            .page(current_page).per(page_size).to_a
        end
        #puts "origin size: #{logs.size}"
        logs.uniq! {|log| "#{log.created_at.to_date}_#{log.user_id}"}
        #puts "compress size: #{logs.size}"
        logs.each do |log|
          sql << <<-EOF
            ('#{log.created_at.to_date}', #{log.user_id}, '#{log.user_registered_at}',
            '#{log.app_channel}', '#{log.app_version}',
            '#{log.created_at}', '#{log.created_at}'),
          EOF
        end
        NodeConnectionUserDayLog.connection.execute("#{SQL_PREFIX}#{sql.chomp('').chomp(',')};")
        sql = ""
        puts "pages: #{current_page} / #{total_pages}"
        current_page += 1
      end
      puts "last user_id: #{users&.last&.id}"
    end
  end

  # 将连接日志表数据迁移到连接日志的二级表(按时间)，加快留存统计查询
  task :migrate_user_connection_user_day_logs, [:begin_date, :end_date] => [:environment] do |t, args|
    begin_date = args.has_key?(:begin_date) ? args[:begin_date].to_date : "2017-06-05".to_date
    end_date = args.has_key?(:end_date) ? args[:end_date].to_date : "2018-01-01".to_date

    #NodeConnectionUserDayLog.delete_all
    SQL_PREFIX = "INSERT IGNORE INTO node_connection_user_day_logs(" + \
      "created_date, user_id, user_registered_at, app_channel, app_version, created_at, updated_at) VALUES".freeze
    sql = ""
    page_size = 1000
    while begin_date <= end_date
      current_page = 1
      logs = NodeConnectionLog.using_db(DB_LOG_SLAVE1) do
        NodeConnectionLog.where("DATE(created_at) = ?", begin_date)
          .page(current_page).per(page_size)
      end
      total_pages = logs.total_pages
      while total_pages >= current_page
        logs = NodeConnectionLog.using_db(DB_LOG_SLAVE1) do
          NodeConnectionLog.select("user_id, user_registered_at, app_channel, app_version, created_at")
            .where("DATE(created_at) = ?", begin_date)
            .page(current_page).per(page_size).to_a
        end
        puts "origin size: #{logs.size}"
        logs.uniq! {|log| "#{begin_date}_#{log.user_id}"}
        puts "compress size: #{logs.size}"
        logs.each do |log|
          sql << <<-EOF
            ('#{begin_date}', #{log.user_id}, '#{log.user_registered_at}',
            '#{log.app_channel}', '#{log.app_version}',
            '#{log.created_at}', '#{log.created_at}'),
          EOF
        end
        NodeConnectionUserDayLog.connection.execute("#{SQL_PREFIX}#{sql.chomp('').chomp(',')};")
        sql = ""
        puts "pages: #{current_page} / #{total_pages}"
        current_page += 1
      end
      puts "#{begin_date} done."
      begin_date = begin_date.next_day
    end
  end

  # 补充订单表用户注册时间为空的记录
  # 用于做充值渗透分析
  task update_user_created_at_transaction_logs: :environment do
    logs = TransactionLog.where(status: 'paid', user_created_at: nil).includes(:user)
    logs.each do |log|
      log.update_columns(user_created_at: log.user.created_at) if log.user.present?
    end
    puts "补充订单表用户注册时间为空的记录 完成"
  end

  # 将充值过的用户的注册日期记入订单表
  # 用于做充值渗透分析
  task add_user_created_at_transaction_logs: :environment do
    User.select(:id, :created_at)
      .where("total_payment_amount > 0")
      .find_in_batches(start: 1, batch_size: 500) do |users|
      users.each do |user|
        user.transaction_logs.update_all(user_created_at: user.created_at)
      end
      puts "last user id: #{users&.last&.id}"
    end
  end

  # 重新统计用户系统平台统计信息到缓存
  task user_platforms: :environment do
    platforms = User.select("create_platform, COUNT(id) AS users_count")
      .where("created_at < ?", Time.now.beginning_of_day)
      .group(:create_platform)
    platforms.each do |p|
      puts p.inspect
      $redis.hset(User::CACHE_KEY_PLATFORMS, p.create_platform, p.users_count)
    end
  end

  # 将流量大于0的用户的流量记入缓存
  task regenerate_user_traffic: :environment do
    UserAddition.where("used_bytes > 0").find_in_batches(start: 1, batch_size: 500) do |users|
      users.each do |user|
        $redis.hset(
          User::CACHE_KEY_TRAFFICS,
          user.user_id,
          user.used_bytes
        )
      end
    end
  end

  # 将已注册用户id写入redis，统计用户总数使用
  task users_count_cache: :environment do
    User.select(:id).find_in_batches(start: 1, batch_size: 500) do |users|
      ids = users.map(&:id)
      ids.each do |id|
        $redis.sadd(User::CACHE_KEY_IDS, id)
      end
    end
  end

  # 今日注册用户id写入redis缓存
  task users_count_today_cache: :environment do
    date = DateUtils.time2ymd(Time.now)
    cache_key = "#{User::CACHE_KEY_IDS}_#{date}"
    User.select(:id).where("DATE(created_at) = ?", Time.now.to_date).find_in_batches(start: 1, batch_size: 500) do |users|
      ids = users.map(&:id)
      puts "#{cache_key}, count: #{ids.length}"
      ids.each do |id|
        $redis.sadd(cache_key, id)
      end
    end
    $redis.expire(cache_key, DateUtils.remaining_time)
  end

  # 今日活跃用户id写入redis缓存
  task active_users_count_today_cache: :environment do
    date = DateUtils.time2ymd(Time.now)
    cache_key = "#{User::CACHE_KEY_ACTIVE_IDS}_#{date}"
    User.select(:id).where("DATE(current_signin_at) = ?", Time.now.to_date).find_in_batches(start: 1, batch_size: 500) do |users|
      ids = users.map(&:id)
      puts "#{cache_key}, count: #{ids.length}"
      ids.each do |id|
        $redis.sadd(cache_key, id)
      end
    end
    $redis.expire(cache_key, DateUtils.remaining_time)
  end

  # 已充值用户id写入redis缓存
  task paid_users_count_cache: :environment do
    cache_key = User::CACHE_KEY_PAID_IDS
    User.select(:id).where("total_payment_amount > 0").find_in_batches(start: 1, batch_size: 500) do |users|
      ids = users.map(&:id)
      puts "#{cache_key}, count: #{ids.length}"
      ids.each do |id|
        $redis.sadd(cache_key, id)
      end
    end
  end

  # 累计总充值金额写入redis缓存
  task total_charge_amount: :environment do
    amount = User.where("total_payment_amount > 0").sum(:total_payment_amount).to_i
    $redis.hset(RedisCache::CACHE_KEY_GLOBAL_COUNTER, TransactionLog::CACHE_HKEY_RECHARGE_AMOUNT, amount)
  end

  # 补充已存在订单的渠道和版本信息
  task regenerate_user_promo_token: :environment do
    User.find_in_batches(start: 1, batch_size: 100) do |users|
      users.each do |user|
        loop do
          user.promo_code = Utils::Gen.friendly_code(6)
          break unless User.find_by(promo_code: user.promo_code)
        end
        user.save
      end
    end
  end

  # 补充已存在订单的渠道和版本信息
  task improve_transaction_log_version: :environment do
    user_ids = TransactionLog.select(:user_id).group(:user_id).map(&:user_id)
    user_versions = User.select(:id, :create_app_channel, :create_app_version).where(id: user_ids).index_by(&:id)
    TransactionLog.where(app_channel: nil, app_version: nil).each do |log|
      versions = user_versions[log.user_id]
      log.update_columns(
        app_channel: versions.create_app_channel,
        app_version: versions.create_app_version
      )
    end

    user_ids = ConsumptionLog.select(:user_id).group(:user_id).map(&:user_id)
    user_versions = User.select(:id, :create_app_channel, :create_app_version).where(id: user_ids).index_by(&:id)
    ConsumptionLog.where(app_channel: nil, app_version: nil).each do |log|
      versions = user_versions[log.user_id]
      log.update_columns(
        app_channel: versions.create_app_channel,
        app_version: versions.create_app_version
      )
    end

    # 补充node_type_id为空的消费记录
    node_type_names = ConsumptionLog.select(:node_type).group(:node_type).map(&:node_type)
    node_types = NodeType.select(:id, :name).where(name: node_type_names).index_by(&:name)
    ConsumptionLog.where(node_type_id: nil).each do |log|
      node_type_id = node_types[log.node_type].id
      log.update_columns(
        node_type_id: node_type_id
      )
    end
  end

  task find_user_id_from_failed_log: :environment do
    puts "user_signin_failed_logs fill user_id starting..."
    logs = UserSigninFailedLog.all
    usernames = logs.map(&:username)
    users = User.select(:id, :username).by_username(usernames).index_by(&:username)
    logs.each do |log|
      user_id = users[log.username]&.id
      if user_id.present? && log.user_id.blank?
        log.update_columns(user_id: user_id)
      else
        log.destroy
      end
    end
    puts "user_signin_failed_logs fill user_id completed"

    puts "connection_failed_logs fill user_id starting..."
    logs = ConnectionFailedLog.all
    usernames = logs.map(&:username)
    users = User.select(:id, :username).by_username(usernames).index_by(&:username)
    logs.each do |log|
      user_id = users[log.username]&.id
      if user_id.present? && log.user_id.blank?
        log.update_columns(user_id: user_id)
      else
        log.destroy
      end
    end
    puts "connection_failed_logs fill user_id completed"

    puts "user_no_operation_logs fill user_id starting..."
    logs = UserNoOperationLog.all
    usernames = logs.map(&:username)
    users = User.select(:id, :username).by_username(usernames).index_by(&:username)
    logs.each do |log|
      user_id = users[log.username]&.id
      if user_id.present? && log.user_id.blank?
        log.update_columns(user_id: user_id)
      else
        log.destroy
      end
    end
    puts "user_no_operation_logs fill user_id completed"
  end

  task create_const_platform: :environment do
    system_enums = [
      { enum_type: 2, name: 'IOS' },
      { enum_type: 2, name: 'Android' },
      { enum_type: 2, name: 'PC' },
      { enum_type: 2, name: 'MAC' }
    ]
    system_enums.each do |enum|
      SystemEnum.create(enum)
    end
  end

  task run_operation_summary_stat: :environment do
    (Date.new(2018, 06, 01)..Date.new(2018, 06, 28)).each do |date|
      Log::OperationSummaryDayStat.stat(date)
    end
    puts "run operation summary stat done."
  end

  task renew_user_traffic_day_stat: :environment do
    (Date.new(2018, 06, 01)..Date.new(2018, 06, 27)).each do |date|
      Log::UserTrafficDayStat.stat(date)
    end
    puts "renew user traffic day stat done."
  end

  desc "初始化账号生成顺序规则，从11459229开始自增"
  task init_increase_user_id_start_at: :environment do
     $redis.hset(User::CACHE_KEY_ID_INCREASE, User::CACHE_KEY_ID_INCREASE_START_AT, 11459229)
  end

  task generate_user_500: :environment do
    ARGV.each { |a| task a.to_sym do ; end }
    file = ARGV[1]

    CSV.open(file , "ab") do |write|
      user_ids = []
      1..500.times.each do ||
        user = {
          device_uuid: SecureRandom.uuid,
          device_model: "iPhone 7",
          platform: "ios",
          system_version: "10.3.3",
          operator: "中国联通",
          net_env: "wifi",
          app_channel: "appStore",
          app_version: "jichu",
          app_version_number: "1.1.3"
        }
        response = RestClient::Request.execute(
          method: :post,
          url: "https://zhulaoban8.com/api/client/v4/signin",
          payload: user,
          timeout: 10
         )
         result = JSON.parse(response.body, symbolize_names: true)
         user =  result[:data][:user]
         user_ids << user[:id]
         write << [user[:username], Utils::AesEncrypt.decrypt(user[:password], "86712786e2205b50e80721462334364d")]
      end
      puts user_ids.join(',')
    end
  end

end

task start: :environment do
  system 'bundle exec rails s -b 0.0.0.0 -p 3001'
end

task sidekiq: :environment do
  system 'bundle exec sidekiq -C config/sidekiq.log | tee ./log/sidekiq.log'
end


namespace :manual_migration do

  task stat_all: :environment do
    Rake::Task["manual_migration:active_users_stat"].invoke
    Rake::Task["manual_migration:new_users_count_stat"].invoke
    Rake::Task["manual_migration:node_connection_users_count_stat"].invoke
    Rake::Task["manual_migration:user_interface_operation_stat"].invoke
    Rake::Task["manual_migration:retention_rate_stat"].invoke
    Rake::Task["manual_migration:transaction_stat"].invoke
    Rake::Task["manual_migration:transaction_month_stat"].invoke
    Rake::Task["manual_migration:user_connections_stat"].invoke
    Rake::Task["manual_migration:user_signin_period_stat"].invoke
    Rake::Task["manual_migration:proxy_user_online_stat"].invoke

    #Rake::Task["manual_migration:connection_failed_stat"].invoke
    #Rake::Task["manual_migration:user_signin_failed_stat"].invoke
    #Rake::Task["manual_migration:user_no_operation_stat"].invoke
  end

  # 为所有用户生成最后登录版本信息
  task user_last_signin_versions: :environment do
    User.select(:id).find_in_batches(start: 1, batch_size: 1000) do |users|
      users.each do |user|
        log = UserSigninLog.where(user_id: user.id).order(created_at: :desc).take
        if log.present?
          RedisCache.hset_list(
            UserSigninLog::CACHE_KEY_VERSIONS,
            user.id,
            [log.app_channel, log.app_version, log.app_version_number, log.platform]
          )
          StatService.record_signin_users_count(
            user.id,
            log.app_channel,
            log.app_version,
            log.platform,
            log.app_version_number
          )
        end
      end
      puts "#{users&.last&.id} done."
    end
  end

  # 生成注册版本统计
  task signup_version_stat: :environment do
    items = User.select("create_app_channel, COUNT(id) AS users_count").group(:create_app_channel)
    items.each do |item|
      $redis.hset("#{StatService::VERSION_SIGNUP_CHANNEL}", item.create_app_channel, item.users_count)
    end
    puts "channel done."
    items = User.select("create_app_version, COUNT(id) AS users_count").group(:create_app_version)
    items.each do |item|
      $redis.hset("#{StatService::VERSION_SIGNUP_VERSION}", item.create_app_version, item.users_count)
    end
    puts "version done."
    items = User.select("create_app_channel, create_app_version, COUNT(id) AS users_count")
      .group(:create_app_channel, :create_app_version)
    items.each do |item|
      $redis.hset("#{StatService::VERSION_SIGNUP_CHANNEL_VERSION}", "#{item.create_app_channel}___#{item.create_app_version}", item.users_count)
    end
    puts "channel and version done."
    items = User.select("create_platform, create_app_version_number, COUNT(id) AS users_count")
      .group(:create_platform, :create_app_version_number)
    items.each do |item|
      $redis.hset("#{StatService::VERSION_SIGNUP_VERSION_NUM}", "#{item.create_platform}___#{item.create_app_version_number}", item.users_count)
    end
    puts "platform version_number done."
  end

  # 生成小时活跃用户统计
  task :active_user_hour_stat, [:first_date] => [:environment] do |t, args|
    has_arg = args.has_key?(:first_date)
    if has_arg
      first_date = args[:first_date].to_date
    else
      ActiveUserHourStat.delete_all
      first_date = UserSigninLog.order(created_at: :asc).limit(1).take.created_at.to_date
    end
    end_date = Time.now.yesterday.to_date
    if first_date.present?
      while end_date > first_date do
        ActiveUserHourStat.stat(end_date)
        end_date = end_date.prev_day
        puts "#{end_date} done."
      end
    end
    puts "小时活跃用户统计完成"
  end

  # 非活跃人数日统计
  task :non_active_users_day_stat, [:end_date] => [:environment] do |t, args|
    if args.has_key?(:end_date)
      end_date = args[:end_date].to_date
    else
      Log::NonActiveUserDayStat.delete_all
      end_date = Time.now.yesterday.to_date
    end
    first_date = User.first.created_at.to_date
    while end_date > first_date do
      Log::NonActiveUserDayStat.stat(end_date)
      puts "#{end_date} done"
      end_date = end_date.prev_day
    end
    puts "非活跃人数日统计完成"
  end

  # 重新统计活跃用户
  task :active_users_stat, [:first_date] => [:environment] do |t, args|
    has_arg = args.has_key?(:first_date)
    # day
    if has_arg
      first_date = args[:first_date].to_date
    else
      ActiveUserDayStat.delete_all
      first_date = UserSigninLog.order(created_at: :asc).take&.created_at&.to_date
    end
    end_date = Time.now.yesterday
    while first_date <= end_date do
      ActiveUserDayStat.stat(first_date)
      first_date = first_date.next_day
    end
    # month
    if !has_arg
      ActiveUserMonthStat.delete_all
      first_date = UserSigninLog.order(created_at: :asc).take.created_at.to_date
      end_date = Time.now.last_month
      while first_date.year <= end_date.year && first_date.month <= end_date.month do
        ActiveUserMonthStat.stat(first_date)
        first_date = first_date.next_month
      end
    end
    puts "活跃用户统计完成"
  end

  # 生成小时新增用户统计
  task :new_user_hour_stat, [:first_date] => [:environment] do |t, args|
    has_arg = args.has_key?(:first_date)
    if has_arg
      first_date = args[:first_date].to_date
    else
      NewUserHourStat.delete_all
      first_date = User.first.created_at.to_date
    end
    end_date = Time.now.yesterday.to_date
    if first_date.present?
      while end_date > first_date do
        NewUserHourStat.stat(end_date)
        end_date = end_date.prev_day
        puts "#{end_date} done."
      end
    end
    puts "小时新用户统计完成"
  end

  # 生成用户表自第一条记录的时间起，每天的注册数记录到新增用户表
  task :new_users_count_stat, [:first_date] => [:environment] do |t, args|
    has_arg = args.has_key?(:first_date)
    # day
    if has_arg
      first_date = args[:first_date].to_date
    else
      NewUserDayStat.delete_all
      first_date = User.first.created_at.to_date
    end
    end_date = Time.now.yesterday
    while first_date <= end_date do
      NewUserDayStat.stat(first_date)
      first_date = first_date.next_day
    end
    # month
    if !has_arg
      NewUserMonthStat.delete_all
      first_date = User.first.created_at.to_date
      end_date = Time.now.last_month
      while first_date.year <= end_date.year && first_date.month <= end_date.month do
        NewUserMonthStat.stat(first_date)
        first_date = first_date.next_month
      end
    end
    puts "新增用户统计完成"
  end

  # 服务器连接用户数日统计表
  task :node_connection_users_count_stat, [:first_date] => [:environment] do |t, args|
    has_arg = args.has_key?(:first_date)
    # day
    if has_arg
      first_date = args[:first_date].to_date
    else
      NodeConnectionCountDayStat.delete_all
      # 用户数日统计完后会紧接着统计次数，所以这里要一起全部删除
      NodeConnectionCountScopeDayStat.delete_all
      first_date = NodeConnectionLog.order(created_at: :asc).take.created_at.to_date
    end
    end_date = Time.now.yesterday
    while first_date <= end_date do
      NodeConnectionCountDayStat.stat(first_date)
      first_date = first_date.next_day
    end
    puts "服务器连接用户统计完成"
  end

  # 用户界面操作日志统计
  task user_interface_operation_stat: :environment do
    UserOperationDayStat.delete_all
    first_date = UserOperationLog.order(created_at: :asc).take.created_at.to_date
    end_date = Time.now.yesterday
    while first_date <= end_date do
      UserOperationDayStat.stat(first_date)
      first_date = first_date.next_day
    end
    puts "用户页面点击统计完成"
  end

  # 生成用户表自第一条记录的时间起，每天的用户注册数、充值用户数、充值总额记录到留存表
  task :retention_rate_stat, [:end_date] => [:environment] do |t, args|
    if args.has_key?(:end_date)
      end_date = args[:end_date].to_date
    else
      UserRetentionRateStat.delete_all
      end_date = Time.now.yesterday.to_date
    end
    first_date = User.first.created_at.to_date
    while end_date > first_date do
      UserRetentionRateStat.stat(end_date)
      puts "#{end_date} done"
      end_date = end_date.prev_day
    end
    puts "留存统计完成"
  end

  # 生成用户表自第一条记录的时间起，每天的用户注册数、充值用户数、充值总额记录到留存表
  task :retention_rate_stat_onday, [:end_date] => [:environment] do |t, args|
    if args.has_key?(:end_date)
      end_date = args[:end_date].to_date
    else
      UserRetentionRateStat.delete_all
      end_date = Time.now.yesterday.to_date
    end
    first_date = User.first.created_at.to_date
    while end_date > first_date do
      UserRetentionRateStat.onday_stat(end_date, true)
      puts "#{end_date} done"
      end_date = end_date.prev_day
    end
    puts "留存统计完成"
  end

  # 生成日充值类型统计
  task :transaction_type_day_stat, [:end_date] => [:environment] do |t, args|
    if args.has_key?(:end_date)
      end_date = args[:end_date].to_date
    else
      TransactionTypeDayStat.delete_all
      end_date = Time.now.yesterday.to_date
    end
    first_date = TransactionLog.by_paid.order(created_at: :asc).take&.created_at&.to_date
    first_date = "2017-06-06".to_date if first_date.nil?
    while end_date > first_date do
      TransactionTypeDayStat.stat(end_date)
      puts "#{end_date} done"
      end_date = end_date.prev_day
    end
    puts "日充值类型统计完成"
  end

  # 生成日充值消费统计
  task :transaction_stat, [:first_date] => [:environment] do |t, args|
    has_arg = args.has_key?(:first_date)
    if has_arg
      first_date = args[:first_date].to_date
    else
      TransactionStat.delete_all
      first_date = TransactionLog.order(created_at: :asc).take&.created_at&.to_date
    end
    end_date = Time.now.yesterday.to_date
    if first_date.present?
      while end_date > first_date do
        TransactionStat.stat(end_date)
        puts "#{end_date} done."
        end_date = end_date.prev_day
      end
    end
    puts "充值日统计完成"
  end

  # 生成小时充值统计
  task :transaction_hour_stat, [:first_date] => [:environment] do |t, args|
    has_arg = args.has_key?(:first_date)
    if has_arg
      first_date = args[:first_date].to_date
    else
      TransactionHourStat.delete_all
      first_date = TransactionLog.order(created_at: :asc).take&.created_at&.to_date
    end
    end_date = Time.now.yesterday.to_date
    if first_date.present?
      while end_date > first_date do
        TransactionHourStat.stat(end_date)
        end_date = end_date.prev_day
        puts "#{end_date} done."
      end
    end
    puts "小时充值统计完成"
  end

  # 重新统计去向IP
  task :user_connections_stat, [:first_date] => [:environment] do |t, args|
    has_arg = args.has_key?(:first_date)
    if has_arg
      first_date = args[:first_date].to_date
    else
      UserConnectionFirstDayStat.delete_all
      first_date = UserConnectionLog.order(created_at: :asc).take.created_at.to_date
    end
    end_date = Time.now.yesterday
    while first_date <= end_date do
      UserConnectionFirstDayStat.stat(first_date)
      first_date = first_date.next_day
    end
    puts "日去向IP统计完成"
    if !has_arg
      UserConnectionFirstMonthStat.delete_all
      first_date = UserConnectionLog.order(created_at: :asc).take.created_at.to_date
      end_date = Time.now.last_month
      while first_date.year <= end_date.year && first_date.month <= end_date.month do
        UserConnectionFirstMonthStat.stat(first_date)
        first_date = first_date.next_month
      end
      puts "月去向IP统计完成"
      UserConnectionFirstYearStat.delete_all
      first_date = UserConnectionLog.order(created_at: :asc).take.created_at.to_date
      end_date = Time.now.last_year
      while first_date.year <= end_date.year do
        UserConnectionFirstYearStat.stat(first_date)
        first_date = first_date.next_year
      end
      puts "年去向IP统计完成"
      UserConnectionRegionStat.delete_all
      UserConnectionRegionStat.stat
      puts "域名区域统计完成"
    end
  end

  # 生成登录时间段统计
  task :user_signin_period_stat, [:first_date] => [:environment] do |t, args|
    has_arg = args.has_key?(:first_date)
    if has_arg
      first_date = args[:first_date].to_date
    else
      UserSigninPeriodDayStat.delete_all
      first_date = UserSigninLog.order(created_at: :asc).take&.created_at&.to_date
    end
    end_date = Time.now.yesterday
    if first_date.present?
      while first_date <= end_date do
        UserSigninPeriodDayStat.stat(first_date)
        first_date = first_date.next_day
      end
    end
    puts "登录时间段统计完成"
  end

  task :proxy_user_online_stat, [:first_date] => [:environment] do |t, args|
    has_arg = args.has_key?(:first_date)
    if has_arg
      first_date = args[:first_date].to_date
    else
      ProxyServerOnlineUsersDayStat.delete_all
      first_date = ProxyServerOnlineUsersLog.order(created_at: :asc).take&.created_at&.to_date
    end
    end_date = Time.now.yesterday
    if first_date.present?
      while first_date <= end_date do
        ProxyServerOnlineUsersDayStat.stat(first_date)
        first_date = first_date.next_day
      end
    end
    puts "在线人数历史记录统计完成"
  end

  # 重新统计连接失败信息
  task connection_failed_stat: :environment do
    ConnectionFailedStat.delete_all
    first_date = ConnectionFailedLog.order(created_at: :asc).take&.created_at&.to_date
    end_date = Time.now.to_date.yesterday
    if first_date.present?
      while first_date <= end_date do
        ConnectionFailedStat.stat(first_date)
        first_date = first_date.next_day
      end
    end
    puts "连接失败统计完成"
  end

  # 用户未操作日志统计
  task user_no_operation_stat: :environment do
    UserNoOperationStat.delete_all
    first_date = UserNoOperationLog.order(created_at: :asc).take&.created_at&.to_date
    end_date = Time.now.yesterday
    if first_date.present?
      while first_date <= end_date do
        UserNoOperationStat.stat(first_date)
        first_date = first_date.next_day
      end
    end
    puts "用户未操作统计完成"
  end

  task user_addition: :environment do
    # 为所有用户建立详情记录
    User.find_in_batches(start: 1, batch_size: 100) do |users|
      users.each do |user|
        UserAddition.create(user_id: user.id, last_inspect_expiration_at: Time.now)
      end
    end
  end

  task node_addition: :environment do
    # 为所有节点建立详情记录
    Node.find_in_batches(start: 1, batch_size: 100) do |nodes|
      nodes.each do |node|
        NodeAddition.create(node_id: node.id)
      end
    end
  end

  # 为服务器连接日志生成用户的注册时间
  task add_user_registered_at: :environment do
    NodeConnectionLog.find_in_batches(start: 1, batch_size: 100) do |logs|
      logs.each do |log|
        log.update_columns(
          user_registered_at: log.user.created_at,
          app_channel: log.user.create_app_channel,
          app_version: log.user.create_app_version
        )
      end
    end
  end

  # 生成月充值消费统计
  task transaction_month_stat: :environment do
    TransactionMonthStat.delete_all
    first_date = TransactionLog.order(created_at: :asc).take&.created_at&.to_date
    end_date = Time.now.last_month
    while first_date.year <= end_date.year && first_date.month <= end_date.month do
      TransactionMonthStat.stat(first_date)
      first_date = first_date.next_month
      puts "#{first_date} done."
    end
    puts "充值月统计完成"
  end

  # 生成登录失败统计
  task user_signin_failed_stat: :environment do
    UserSigninFailedStat.delete_all
    first_date = UserSigninFailedLog.order(created_at: :asc).take&.created_at&.to_date
    end_date = Time.now.yesterday
    if first_date.present?
      while first_date <= end_date do
        UserSigninFailedStat.stat(first_date)
        first_date = first_date.next_day
      end
    end
    puts "登录失败统计完成"
  end

  # 为所有用户生成第一次关联的用户ID
  task device_first_user_id: :environment do
    Device.find_in_batches(start: 1, batch_size: 100) do |devices|
      devices.each do |device|
        user = device.users.order(created_at: :asc).take
        if user.present?
          device.update_columns(first_user_id: user.id)
          puts user.id
        end
      end
    end
  end

  # 统计登录ip运营商使用情况
  task user_login_ip_area_stat: :environment do
    CACHE_KEY_IP_AREA_STAT = 'ip_area_stat'

    $redis.del(CACHE_KEY_IP_AREA_STAT)
    puts '分析中...'
    User.find_in_batches(start: 1, batch_size: 5000) do |users|
      users.pluck(:last_signin_ip).each do |ip|
        next if ip.blank?
        ip_region = Ipnet.find_by_ip(ip)
        area = ip_region[:area]
        if $redis.hget(CACHE_KEY_IP_AREA_STAT, area).blank?
          $redis.hset(CACHE_KEY_IP_AREA_STAT, area, 1)
        else
          $redis.hincrby(CACHE_KEY_IP_AREA_STAT, area, 1)
        end
      end
    end
    puts '<---------ip运营商使用情况----------->'
    list = $redis.hgetall(CACHE_KEY_IP_AREA_STAT)
    list.sort_by{|k,v| v.to_i}.reverse!.first(100).each {|key, value|
      puts "#{key} : #{value}"
    }
  end

  # 统计最近五天每个小时ip运营商解析数
  task user_sigin_log_ip_area_stat: :environment do
    CACHE_KEY_IP_AREA_STAT = 'user_sigin_log_ip_area_stat'

    $redis.del(CACHE_KEY_IP_AREA_STAT)
    puts '分析中...'
    UserSigninLog.where("created_at > ?", Time.now - 5.days).
      order(created_at: :desc).find_in_batches(start: 1, batch_size: 5000) do |logs|
      logs.pluck(:ip, :created_at).each do |h|
        ip, time = h
        next if ip.blank?
        ip_region = Ipnet.find_by_ip(ip)
        area = ip_region[:area]
        next if area != '电信'
        key = "#{time.day}号_#{time.hour}点"
        if $redis.hget(CACHE_KEY_IP_AREA_STAT, key).blank?
          $redis.hset(CACHE_KEY_IP_AREA_STAT, key, 1)
        else
          $redis.hincrby(CACHE_KEY_IP_AREA_STAT, key, 1)
        end
      end
    end
    puts '<---------ip运营商使用情况----------->'
    list = $redis.hgetall(CACHE_KEY_IP_AREA_STAT)
    list.sort_by{|k,v| k.scan(/\d/).join().to_i}.first(120).each {|key, value|
      puts "#{key} : #{value}"
    }
  end

end

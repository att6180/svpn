namespace :yc_data_migration do

  CACHE_KEY = "yc_users"
  EXPIRED_AT = "2018-11-15 00:00:00".to_datetime


  # 迁移洋葱用户，设备数据
  task register: :environment do

    spec = ActiveRecord::Base.connection_config
    @@new_spec = spec.clone
    @@new_spec[:database] = "yc"

    class YcUser < ApplicationRecord
      establish_connection @@new_spec
      self.table_name = "users"
      has_many :yc_user_node_types, foreign_key: :user_id
    end

    class YcUserNodeType < ApplicationRecord
      establish_connection @@new_spec
      self.table_name = "user_node_types"
    end

    yc_register_users = YcUser.where("username like '1%'").pluck(:username)
    jg_register_users = User.where("username like '1%'").pluck(:username)

    # 洋葱注册手机号在坚果存在的
    coexist_users = yc_register_users & jg_register_users

    YcUser.where(username: coexist_users).order(id: :asc).find_each.with_index do |user, index|
      if !$redis.sismember(CACHE_KEY, user.username)
        $redis.sadd(CACHE_KEY, user.username)
        jg_user = User.find_by(username: user.username)
        jg_user.user_node_types.each do |jn|
          puts "# node_type_id->>>>>>>>>>>>>>>#{jn.id} | user_id->>>>>>>>>>#{jg_user.username}"
          yn = user.yc_user_node_types.where(node_type_id: jn.node_type_id)&.first
          next if yn.blank?
          if yn.expired_at >= EXPIRED_AT
            jn.update(expired_at: (jn.expired_at + (yn.expired_at.to_i - EXPIRED_AT.to_i) + 90.days))
          else
            jn.update(expired_at: "2019-02-13 00:00:00")
          end
        end
      end
    end

  end


  # 迁移洋葱用户，设备数据
  task atuo: :environment do

    spec = ActiveRecord::Base.connection_config
    @@new_spec = spec.clone
    @@new_spec[:database] = "yc"

    class YcUser < ApplicationRecord
      establish_connection @@new_spec
      self.table_name = "users"
      has_many :yc_user_node_types, foreign_key: :user_id
    end

    class YcUserNodeType < ApplicationRecord
      establish_connection @@new_spec
      self.table_name = "user_node_types"
    end

    yc_register_users = YcUser.where("username like '1%'").pluck(:username)
    jg_register_users = User.where("username like '1%'").pluck(:username)

    # 洋葱注册手机号在坚果存在的
    coexist_users = yc_register_users & jg_register_users

    YcUser.where("username not in (?)", coexist_users).order(id: :asc).find_each.with_index do |user, index|
      if !$redis.sismember(CACHE_KEY, user.username)
        $redis.sadd(CACHE_KEY, user.username)
        new_user = User.new(user.attributes.except("id"))
        user.yc_user_node_types.each do |node_type|
          new_node_type = node_type.attributes.except("id", "user_id")
          new_user.user_node_types.build(new_node_type)
        end
        new_user.save!
        new_user.user_node_types.each do |node_type|
          next if user.yc_user_node_types.blank?
          yc_node_type = user.yc_user_node_types.find_by(node_type_id: node_type.node_type_id)
          next if yc_node_type.blank?
          if yc_node_type["expired_at"] <= EXPIRED_AT
            node_type.update(expired_at: "2019-02-13 00:00:00")
          else
            node_type.update(expired_at: yc_node_type["expired_at"] + 90.days)
          end
        end
        puts "# Processing position at #{index}"
      end
    end

  end
end

module RedisCache

  CACHE_KEY_GLOBAL_COUNTER = "global_counter"

  class << self

    def hset(key, hkey, hvalue, expire_at=nil)
      raise "hvalue must be string." if !hvalue.is_a?(String)
      $redis.hset(key, hkey, hvalue)
      $redis.expire(key, expire_at) if expire_at
    end

    def hfetch(key, hkey, expire_at=nil)
      cache_result = $redis.hget(key, hkey)
      if cache_result.nil? && block_given?
        cache_result = yield(self)
        if !cache_result.nil?
          $redis.hset(key, hkey, cache_result)
          $redis.expire(key, expire_at) if expire_at
        end
      end
      cache_result
    end

    def hdel(key, hkey)
      $redis.hdel(key, hkey)
    end

    def hset_list(key, hkey, hvalue, expire_at=nil)
      raise "hvalue must be array." if !hvalue.is_a?(Array)
      $redis.hset(key, hkey, hvalue.join("|||"))
      $redis.expire(key, expire_at) if expire_at
    end

    def hfetch_list(key, hkey, expire_at=nil)
      cache_result = $redis.hget(key, hkey)
      if cache_result.nil? && block_given?
        cache_result = yield(self)
        if !cache_result.nil?
          raise ("block result must be array") if !cache_result.is_a?(Array)
          $redis.hset(key, hkey, cache_result.join("|||"))
          $redis.expire(key, expire_at) if expire_at
        end
      end
      cache_result.is_a?(String) ? cache_result.split("|||") : cache_result
    end

    def hset_bool(key, hkey, hvalue, expire_at=nil)
      raise "hvalue must be boolean." if !(!!hvalue == hvalue)
      cache_value = hvalue ? '1' : '0'
      $redis.hset(key, hkey, cache_value)
      $redis.expire(key, expire_at) if expire_at
    end

    def hfetch_bool(key, hkey, expire_at=nil)
      raise "must have block." if !block_given?
      cache_result = $redis.hget(key, hkey)
      if cache_result.nil?
        cache_result = yield(self)
        cache_value = cache_result ? '1' : '0'
        $redis.hset(key, hkey, cache_value)
        $redis.expire(key, expire_at) if expire_at
      else
        cache_result = cache_result == '1' ? true : false
      end
      cache_result
    end

    def hset_json(key, hkey, hvalue, expire_at=nil)
      $redis.hset(key, hkey, hvalue) if !hvalue.nil?
      $redis.expire(key, expire_at) if expire_at
    end

    def hget_json(key, hkey)
      result = $redis.hget(key, hkey)
      result.nil? ? nil : JSON.parse(result, symbolize_names: true)
    end

    def hfetch_json(key, hkey, expire_at=nil)
      cache_result = $redis.hget(key, hkey)
      if cache_result.nil? && block_given?
        cache_result = yield(self).to_a.map { |r| r.attributes.symbolize_keys }
        $redis.hset(key, hkey, cache_result.to_json) if !cache_result.nil?
      else
        cache_result = JSON.parse(cache_result, symbolize_names: true)
      end
      cache_result
    end

    def incr(key, expire_at=nil)
      count = $redis.incr(key)
      if $redis.ttl(key) == -1
        $redis.expire(key, expire_at)
      end
      count
    end

    # 检测是否是频繁的访问(1秒钟内多次)
    def is_concurrent?(flag)
      # 使用redis加锁，避免ios并发重复请求，并且在5秒钟之后过期
      lock_key = "#{flag}_#{Time.now.to_i}"
      lock_result = $redis.multi do
        $redis.setnx(lock_key, Time.now.to_i)
        $redis.expire(lock_key, 5)
      end
      !lock_result.first
    end

  end
end

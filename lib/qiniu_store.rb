require 'qiniu'

class QiniuStore

  BUCKET_ROUTE = 'routes'
  BUCKET_DEBUG_LOG = 'debuglogs'
  TOKEN_EXPIRE_TIME = 3600

  def self.upload(file_location, file_name, bucket_name)
    put_policy = Qiniu::Auth::PutPolicy.new(
      bucket_name,
      file_name,
      TOKEN_EXPIRE_TIME
    )

    uptoken = Qiniu::Auth.generate_uptoken(put_policy)

    code, result, reponse_headers = Qiniu::Storage.upload_with_token_2(
      uptoken,
      file_location,
      file_name,
      nil,
      bucket: bucket_name
    )
    code
  end

  def self.upload_route(file_location, file_name)
    upload(file_location, file_name, CONFIG.qiniu_routes_bucket)
  end

  def self.upload_debug_log(file_location, file_name)
    upload(file_location, file_name, CONFIG.qiniu_debug_log_bucket)
  end

end

class Sms

  # 云片短信验证码服务
  # API: https://www.yunpian.com/api2.0/document-demo.html
  def self.send(telephone, area_code = '86')
    sms_uri = URI(CONFIG.sms_host_url)
    code = rand(1000..9999)
    http = Net::HTTP.new(sms_uri.host, sms_uri.port)
    http.use_ssl = true

    #http.verify_mode = OpenSSL::SSL::VERIFY_NONE
    request = Net::HTTP::Post.new(sms_uri.path)
    request['Content-type'] = 'application/x-www-form-urlencoded'
    request['Accept'] = 'application/json'
    request.set_form_data({
      mobile: "+#{area_code}#{telephone}",
      text: "验证码: #{code}",
      apikey: CONFIG.sms_api_key,
      tpl_id: (area_code == '86' ? CONFIG.sms_china_tpl_id : CONFIG.sms_tpl_id),
      tpl_value: "#{URI::encode('#code#')}=#{code}"
    })
    response = http.request(request)
    result = JSON.parse(response.body)
    result['code'] == 0 ? code.to_s : nil
  end

  # 批量发送短信
  # https://www.yunpian.com/api2.0/scene-notify.html
  # telephones: 手机号列表使用,号分割
  # tpl_id: 短信模板ID
  # keyword: 用于替换的关键字
  # content: 用于替换的内容
  def self.batch_send(telephones, tpl_id, keywords, contents)
    telephone_count = telephones.split(',')
    sms_uri = URI(CONFIG.sms_notify_batch_url)
    http = Net::HTTP.new(sms_uri.host, sms_uri.port)
    http.use_ssl = true
    #http.verify_mode = OpenSSL::SSL::VERIFY_NONE
    request = Net::HTTP::Post.new(sms_uri.path)
    request['Content-type'] = 'application/x-www-form-urlencoded'
    request['Accept'] = 'application/json'
    form_data = {
      apikey: CONFIG.sms_api_key,
      mobile: telephones,
      tpl_id: tpl_id,
      tpl_value: ""
    }
    if !keywords.nil? && !contents.nil?
      tpl_value = ""
      keywords = keywords.split('&')
      contents = contents.split('&')
      keywords.each_with_index do |keyword, index|
        tel_value_key = URI::encode("##{keyword}#")
        tpl_value += "#{tel_value_key}=#{contents[index]}"
        tpl_value += "&" if (keywords.length - 1) != index
      end
    end
    form_data[:tpl_value] = tpl_value if !keywords.nil? && !contents.nil?
    request.set_form_data(form_data)
    response = http.request(request)
    result = JSON.parse(response.body)
    result['total_count'] == telephone_count ? 0 : nil
  end

end

class Comsunny
  # comsunny支付平台类
  attr_reader :timestamp, :channel_type, :sub_channel_type, :transaction_type, \
    :transaction_id, :transaction_fee, :trade_success, :message_detail, :optional

  PAYMENT_TYPES = {
    alipay_qrcode: "CS_ALI_QRCODE",
    alipay_h5: "CS_ALI_WAP",
    wx_qrcode: "CS_WX_QRCODE",
    wx_h5b: "CS_WX_H5B",
    wx_h5: "CS_WX_WAP",
    qq: "CS_QQWALLET",
    alipay_origin: "ORIGIN_ALIPAY"
  }

  # 创建订单并返回支付URL
  # 参数列表:
  #   options:
  #     order_id: 订单号
  #     fee: 总价
  #     title: 商品名称
  #     ip: 客户ip
  #     created_at: 订单创建时间
  #     platform: 客户平台
  #   request_url:  收银台界面url
  #
  #   tips: comsunny v2平台post只支持application/json
  def self.create_order(options, request_url)
    timestamp = Time.now.to_i * 1000
    app_id = CONFIG.comsunny_app_id
    app_secret = CONFIG.comsunny_app_secret
    return_url = CONFIG.comsunny_h5_return_url

    app_sign2 = Digest::MD5.hexdigest(
      [
        options[:order_id],
        app_id,
        timestamp,
        app_secret,
        options[:fee]
      ].join('')
    )

    params = {
      appid: app_id,
      bill_no: options[:order_id],
      title: options[:plan_name],
      real_ip: options[:ip],
      total_fee: options[:fee],
      timestamp: timestamp,
      app_sign2: app_sign2,
      return_url: return_url,
      userId: options[:user_id]
    }
    request_url + params.to_query
  end

  # 查询订单
  def self.query_order(options)
    url = CONFIG.comsunny_query_order_url
    url_path = "/2/rest/detail_bills"
    params = {
      # app_id: options[:app_id],
      channelBillId: options[:channel_order_id], 
      bill_no: options[:order_id],
      csMerBillId: options[:cs_order_id],
      # spay_result: true,
      # timestamp: (Time.now.to_f * 1000).to_i,
      startTime: (options[:start_at].to_time.to_i * 1000).to_s, 
      endTime: (options[:end_at].to_time.to_i * 1000).to_s
    }
    # sign_string = "#{options[:app_id]}#{params[:timestamp]}#{options[:app_key]}"
    # signature = Digest::MD5.hexdigest(sign_string)
    # params[:app_sign] = signature
    params_json = params.to_json
    uri = URI(url)
    http = Net::HTTP.new(uri.host, uri.port)
    http.use_ssl = uri.port == 443
    begin
      request = Net::HTTP::Post.new(url_path, 'Content-Type': 'application/json')
      request.body = params_json
      response = http.request(request)
      result = JSON.parse(response.body)
    rescue => e
      result  = ""
    end
    result
  end

  def self.convert_payment_type(sub_channel_type)
    result = if sub_channel_type.include?('ALI_QRCODE')
      :alipay_qrcode
    elsif sub_channel_type.include?('ALI_WAP')
      :alipay_h5
    elsif sub_channel_type.include?('WX_QRCODE') || sub_channel_type.include?('BC_NATIVE')
      :wx_qrcode
    elsif sub_channel_type.include?('WX_H5B')
      :wx_h5b
    elsif sub_channel_type.include?('WX_WAP')
      :wx_h5
    elsif sub_channel_type.include?('QQWALLET')
      :qq
    elsif sub_channel_type.include?('ORIGIN_ALIPAY')
      :alipay_origin
    elsif sub_channel_type.include?('CS_UNION_WAP')
      :union_wap
    else
      :other
    end
    result
  end

  # -----------------
  # 异步接收支付结果

  def initialize(app_id, secret, options)
    @app_id = app_id
    @secret = secret

    @signature = options[:signature]
    @timestamp = options[:timestamp]
    @channel_type = options[:channel_type]
    @sub_channel_type = options[:sub_channel_type]
    @transaction_type = options[:transaction_type]
    @transaction_id = options[:transaction_id]
    @transaction_fee = options[:transaction_fee]
    @trade_success = options[:trade_success]
    @message_detail = options[:message_detail]
    @optional = options[:optional]
  end

  def signature_correct?
    signature = Digest::MD5.hexdigest("#{@app_id}#{@transaction_id}#{@transaction_type}#{@channel_type}#{@transaction_fee}#{@secret}")
    signature == @signature
  end

  def type_is_pay?
    @transaction_type == "PAY"
  end

  def fee_correct?(fee)
    (fee * 100).to_i == @transaction_fee
  end

  def trade_success?
    @trade_success
  end

  def payment_type
    Comsunny.convert_payment_type(@sub_channel_type)
  end

end

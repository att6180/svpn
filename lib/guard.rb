module Guard

  CACHE_KEY_SIGNUP_IP = "guard_signup_ip"
  CACHE_KEY_BLOCK_IP = "guard_block_ip"
  CACHE_KEY_USER_TELEPHONES = "guard_user_telephones"
  CACHE_KEY_BLOCK_TELEPHONES = "guard_block_telephones"
  CACHE_KEY_NODE_CONNECT = "guard_node_connect"
  CACHE_KEY_NODE_FAKE_IP_LIST = "guard_node_fake_ip_list"

  # 判断该ip是否超过每天注册上限
  def self.is_signup_exceeded?(ip)

    return true if $redis.sismember(CACHE_KEY_BLOCK_IP, ip)

    today_signup_ip_cache_key = "#{CACHE_KEY_SIGNUP_IP}_#{DateUtils.time2ymd(Time.now)}"
    count = $redis.hincrby(today_signup_ip_cache_key, ip, 1)
    if $redis.ttl(today_signup_ip_cache_key) == -1
      $redis.expire(today_signup_ip_cache_key, DateUtils.remaining_time)
    end
    if Setting.everyday_ip_max_signup_enabled &&
            count >= Setting.everyday_ip_max_signup_count.to_i
      $redis.sadd(CACHE_KEY_BLOCK_IP, ip)
      return true
    end
    false
  end

  # 判断该手机号是否超过每天发送上限
  def self.is_tel_send_exceeded?(telephone)

    return true if $redis.sismember(CACHE_KEY_USER_TELEPHONES, telephone)

    today_telephone_cache_key = "#{CACHE_KEY_USER_TELEPHONES}_#{DateUtils.time2ymd(Time.now)}"
    count = $redis.hincrby(today_telephone_cache_key, telephone, 1)
    if $redis.ttl(today_telephone_cache_key) == -1
      $redis.expire(today_telephone_cache_key, DateUtils.remaining_time)
    end
    if Setting.everyday_telephone_max_send_enabled &&
            count >= Setting.everyday_telephone_max_send_count.to_i

      today_tel_block_cache_key = "#{CACHE_KEY_BLOCK_TELEPHONES}_#{DateUtils.time2ymd(Time.now)}"
      if $redis.ttl(today_tel_block_cache_key) == -1
        $redis.expire(today_tel_block_cache_key, DateUtils.remaining_time)
      end
      $redis.sadd(today_tel_block_cache_key, telephone)
      return true
    end
    false
  end

  # 判断该用户连接服务器在某一个时间段内是否超过次数
  def self.is_node_connnect_exceeded?(user_id)
  
    return true if Setting.node_connect_block_user_list.split(/[\s,]+/).include?(user_id.to_s)

    count = $redis.hincrby(CACHE_KEY_NODE_CONNECT, user_id, 1)
    if $redis.ttl(CACHE_KEY_NODE_CONNECT) == -1
      $redis.expire(CACHE_KEY_NODE_CONNECT, Setting.node_connect_interval_seconds.to_i)
    end
    if Setting.node_connect_interval_enabled == "true" &&
      count >= Setting.node_connect_interval_max_count.to_i && !Setting.tool_ignore_ids.include?(user_id)

      Setting.node_connect_block_user_list += ",#{user_id}"
      return true 
    end
    false
  end

  # 随机返回一个虚拟ip
  def self.pick_fake_ip(user)
    platform = user.last_signin_versions[3]
    case platform
    when "android"
      return Setting.node_connect_fake_ip_list_android.split(/[\s,]+/).sample
    when "ios"
      return Setting.node_connect_fake_ip_list_ios.split(/[\s,]+/).sample
    when "mac"
      return Setting.node_connect_fake_ip_list_mac.split(/[\s,]+/).sample
    when "pc"
      return Setting.node_connect_fake_ip_list_pc.split(/[\s,]+/).sample
    else
      return Setting.node_connect_fake_ip_list_ios.split(/[\s,]+/).sample
    end
  end
end

class Jpush

  CACHE_LAST_SENT_TIME = 'jpush_last_sent_time'

  # 是否允许推送(1分钟内只能推送一次)
  def self.allow_to_push?(uuid)
    last_time = $redis.hget(CACHE_LAST_SENT_TIME, uuid)
    # 如果有最后发送时间
    if last_time.present?
      # 如果当前时间减最后发送时间大于1分钟，则可以发送
      result = Time.now - DateTime.parse(last_time) > 1.minutes
    else
      result = true
    end
    # 只要可以发送，就记下最后一次发送时间
    $redis.hset(CACHE_LAST_SENT_TIME, uuid, Time.now) if result
    result
  end

end

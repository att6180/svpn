require 'openssl'
require 'base64'

class RsaCrypt

  def self.verify(data, signature)
    pubkey = OpenSSL::PKey::RSA.new(CONFIG.google_pay_public_key)
    pubkey.verify(OpenSSL::Digest::SHA1.new, Base64.decode64(signature), data.to_s.strip)
  end
end

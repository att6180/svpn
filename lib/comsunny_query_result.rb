class ComsunnyQueryResult
  attr_reader :timestamp, :channel_type, :sub_channel_type, :transaction_type, \
    :transaction_id, :transaction_fee, :trade_success, :message_detail, :optional, \
    :channel_transaction_id, :cs_transaction_id

  def initialize(result)
    @result = result
    result_bills = result["bills"].first
    if result_bills.present?
      @timestamp = result_bills["success_time"]
      @channel_type = result_bills["channel"]
      @sub_channel_type = result_bills["sub_channel"]
      @transaction_id = result_bills["bill_no"]
      @channel_transaction_id = result_bills["channelBillId"]
      @cs_transaction_id = result_bills["csMerBillId"]
      @transaction_fee = result_bills["total_fee"]
      @trade_success = result_bills["spay_result"]
      @message_detail = ""
      @optional = result_bills["optional"]
    end
  end

  def is_paid?
    return false if !@result.has_key?("result_code") || @result["result_code"] != 0
    return false if !@result.has_key?("bills")
    return false if @result["bills"].length <= 0
    return false if !@result["bills"]&.first.has_key?("spay_result")
    @result["bills"].first["spay_result"]
  end

  def fee_correct?(fee)
    (fee * 100).to_i == @transaction_fee
  end

  def payment_type
    Comsunny.convert_payment_type(@sub_channel_type)
  end

end

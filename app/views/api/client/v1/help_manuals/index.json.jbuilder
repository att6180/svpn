json.help_manuals @help_manuals do |hm|
  json.id hm.id
  json.title hm.title
  json.content hm.content
end
json.partial! "api/shared/page", items: @help_manuals

json.new_message @has_new_message
json.logs @logs do |log|
  json.id log.id
  json.member_flag log.memberable_type == 'User' ? 0 : 1
  json.member log.memberable_type == 'User' ? '我' : '客服回复'
  json.content log.content
  json.created_at format_api_time(log.created_at)
end
json.partial! "api/shared/page", items: @logs

json.feedbacks @feedbacks do |feedback|
  json.id feedback.id
  json.type Feedback.feedback_types[feedback.feedback_type]
  json.content feedback.content
  json.is_processed feedback.is_processed
  json.processed_at format_api_time(feedback.processed_at)
  json.reply_content feedback.reply_content
  json.created_at format_api_time(feedback.created_at)
  json.has_read feedback.has_read?
  json.has_read_at feedback.has_read_at.present? ? format_api_time(feedback.has_read_at) : nil
end
json.partial! "api/shared/page", items: @feedbacks

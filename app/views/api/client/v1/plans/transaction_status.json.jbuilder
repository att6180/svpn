json.is_completed @is_completed

if @is_completed
  json.partial! 'api/client/v1/users/profile', user: current_user
end

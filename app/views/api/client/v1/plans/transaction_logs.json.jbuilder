json.logs @logs do |log|
  json.order_number log.order_number
  json.created_at format_api_time(log.created_at)
  json.has_paid log.status == 'paid' ? true : false
  json.plan_name log.plan_name
  json.price log.price
  json.coins log.coins
end
json.partial! "api/shared/page", items: @logs

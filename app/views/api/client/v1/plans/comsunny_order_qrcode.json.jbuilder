json.transaction_log do
  json.order_number @log.order_number
  json.payment_url @payment_url
end

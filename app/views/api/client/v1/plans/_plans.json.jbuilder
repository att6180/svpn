json.plans plans do |plan|
  json.id plan.id
  json.name plan.name
  json.is_regular_time plan.is_regular_time
  json.price plan.price
  json.coins plan.coins
  json.present_coins plan.present_coins
  json.description plan.description
  if plan.is_iap?
    json.iap_id plan.iap_id
  end
end

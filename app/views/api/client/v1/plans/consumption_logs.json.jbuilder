json.logs @logs do |log|
  json.created_at format_api_time(log.created_at)
  json.node_type_name log.node_type.name
  json.coins log.coins
end
json.partial! "api/shared/page", items: @logs

json.user_node_type_is_expired @user_node_type_is_expired
json.is_consumed @is_consumed

if @user_node_type_is_expired
  if @is_consumed
    json.partial! 'api/client/v2/nodes/connect_detail', \
      token: @token, user: current_user, node: @node, \
      user_node_type: @user_node_type
  end
else
  json.partial! 'api/client/v2/nodes/connect_detail', \
    token: @token, user: current_user, node: @node, \
    user_node_type: @user_node_type
end
json.tab_id @tabs[@node_type_id.to_i]&.id

json.cache! ['node_index_region_v1', node_types] do
  json.node_types node_types do |node_type|
    json.id node_type.id
    json.name node_type.name
    json.level node_type.level
    json.expense_coins node_type.expense_coins
    json.user_group_id node_type.user_group_id
    json.user_group_level node_type.user_group.level
    json.description node_type.description

    json.node_regions node_type.node_regions do |region|
      if region.is_enabled?
        json.id region.id
        json.name region.name
        json.abbr region.abbr
        json.nodes region.nodes do |node|
          if node.enabled?
            json.id node.id
            json.name node.name
            json.url node.url
            json.port node.port
            json.password node.password
            json.encrypt_method node.encrypt_method
            json.connections_count node.connections_count
            json.max_connections_count node.max_connections_count
            json.description node.description
          end
        end
      end
    end
    #end

  end
end

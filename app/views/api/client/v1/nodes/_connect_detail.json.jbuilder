json.proxy_session_token token
json.user do
  json.current_coins user.current_coins
end
json.user_node_type do
  json.id user_node_type.id
  json.expired_at format_api_time(user_node_type.expired_at)
  json.status I18n.t("user_node_type.status.#{user_node_type.status}")
  json.used_count user_node_type.used_count
end
json.node do
  json.id node.id
  json.name node.name
  json.url node.url
  json.port node.port
  json.encrypt_method node.encrypt_method
  json.password node.password
end

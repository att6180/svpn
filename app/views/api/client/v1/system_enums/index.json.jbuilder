json.enums @enums do |enum|
  json.id enum.id
  json.name enum.name
end

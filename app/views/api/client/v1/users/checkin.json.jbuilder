json.has_checkin_today @has_checkin_today
json.expired_at format_api_time(@new_expired_at)
json.days_of_month @days_of_month
json.checkin_days current_user.checkin_days
json.last_checkin_at format_api_time(@last_log.created_at)
json.present_time @last_log.present_time
json.calendar @days

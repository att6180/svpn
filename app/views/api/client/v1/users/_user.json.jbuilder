json.extract! user, :id, :username, :password,:email, :wechat, :weibo, :qq, \
  :total_payment_amount, :current_coins, :total_coins

json.used_bytes Utils.b2mb(user.user_addition.used_bytes)
json.max_bytes user.max_bytes
json.limit_bytes user.limit_bytes
json.is_checkin_today user.has_checkin_today?
json.is_enabled user.is_enabled
json.created_at format_api_time(user.created_at)
json.new_message user.has_new_message?

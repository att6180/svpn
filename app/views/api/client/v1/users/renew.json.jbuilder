json.user_node_type_id @user_node_type.id
json.user_node_type_expired_at format_api_time(@user_node_type.expired_at)
json.current_coins current_user.current_coins
json.is_renewd @renewd

json.user do
  json.current_coins user.current_coins
  json.promo_users_count user.promo_users_count
  json.promo_coins_count user.promo_coins_count
  json.is_register user.username_changed?
  json.is_expired user.plan_expired?
end

json.group do
  json.id user.user_group.id
  json.name user.user_group.name
  json.level user.user_group.level
  json.need_coins user.user_group.need_coins
  json.next_name user.next_group.present? ? user.next_group.name : nil
  json.next_id user.next_group.present? ? user.next_group.id : nil
  json.next_need_coins user.next_group_need_coins
end

if @user_node_types.present?
  json.partial! 'api/client/v1/user_node_types/user_node_types', user_node_types: @user_node_types
end

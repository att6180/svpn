json.servers @servers do |server|
  json.extract! server, :id, :url, :region, :priority
end

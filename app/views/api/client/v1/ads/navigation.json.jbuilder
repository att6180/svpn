json.navigation_ad_update_at Setting.navigation_ad_update_at
json.hit_cache @hit_cache

if !@hit_cache
  json.ads @ads do |ad|
    json.id ad.id
    json.priority ad.priority
    json.name ad.name
    json.image_url ad.image_url
    json.link_url ad.link_url
    json.updated_at format_api_time(ad.updated_at)
  end
end

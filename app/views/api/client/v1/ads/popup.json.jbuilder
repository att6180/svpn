json.is_enabled Setting.popup_ad_enabled == "true"
json.day_times Setting.popup_ad_day_times.to_i
json.image_url Setting.popup_ad_image_url
json.link_url Setting.popup_ad_link_url
json.open_link_type Setting.popup_ad_open_link_type.to_i
json.updated_at Setting.popup_ad_updated_at

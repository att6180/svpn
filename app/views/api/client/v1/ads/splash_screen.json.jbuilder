enabled = Setting.splash_screen_ad_enabled == "true"
json.is_enabled enabled
json.overtime Setting.splash_screen_ad_overtime.to_i
if enabled && @ad.present?
  json.ad do
    json.id @ad.id
    json.duration @ad.duration
    json.image_url @ad.image_url
    json.link_url @ad.link_url
    json.ad_type @ad.ad_type_before_type_cast
    json.open_link_type @ad.open_link_type_before_type_cast
    json.updated_at DateUtils.time2str(@ad.last_modification_time)
  end
else
  json.ad @ad
end

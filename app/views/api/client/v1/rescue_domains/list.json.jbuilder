json.domain_list @rescue_domains do |rescue_domain|
  json.domain_name rescue_domain.domain
  json.domain_type rescue_domain.domain_type
  json.response_time rescue_domain.response_time || ""
end
json.should_update Setting.rescue_domain_should_update
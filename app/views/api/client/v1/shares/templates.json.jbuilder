json.template_update_time Setting.share_template_update_time
json.text_logo_url Setting.share_text_logo_url
json.text_logo_update_time Setting.share_text_logo_update_time.to_i
json.weixin_enabled Setting.share_weixin_enabled == "true"
json.friend_circle_enabled Setting.share_friend_circle_enabled == "true"
json.qq_enabled Setting.share_qq_enabled == "true"
json.weibo_enabled Setting.share_weibo_enabled == "true"
json.hit_cache @hit_cache

if !@hit_cache
  json.templates @templates do |template|
    json.id template.id
    json.platform_name template.platform_name
    json.priority template.sequence
    json.copy_link_text template.copy_link_text.gsub('/tgm', current_user.promo_code)
    json.text_title template.text_title.gsub('/tgm', current_user.promo_code)
    json.text_content template.text_content.gsub('/tgm', current_user.promo_code)
    json.text_url template.text_url
    json.image_text_content template.image_text_content.gsub('/tgm', current_user.promo_code)
    json.image_promo_text template.image_promo_text.gsub('/tgm', current_user.promo_code)
    json.landing_page_qrcode_url template.landing_page_qrcode_url
    json.background_image_download_url template.background_image_download_url
    json.background_image_update_at format_api_time(template.background_image_update_at)
  end
end

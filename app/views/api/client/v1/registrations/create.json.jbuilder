json.proxy_session_id @user_session_id
json.app_launch_id ""
json.signin_log_id ""
json.user do
  json.api_token @api_token if @api_token.present?
  json.partial! 'api/client/v3/users/user', user: @user, reg: true
end
json.group do
  json.id @user.user_group.id
  json.name @user.user_group.name
  json.level @user.user_group.level
end
json.partial! 'api/client/v1/user_node_types/user_node_types', user_node_types: @user_node_types
json.partial! 'api/client/v2/nodes/nodes', node_types: @node_types
json.ip_region do
  json.country @ip_region[:country]
  json.province @ip_region[:province]
  json.city @ip_region[:city]
end
if @settings.present?
  json.settings do
    @settings.map{ |setting| json.set! setting.key, setting.value }
  end
end

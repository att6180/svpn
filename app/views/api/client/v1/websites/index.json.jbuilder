json.websites @items do |item|
  json.id item.id
  json.website_type item.website_type
  json.name item.name
  json.url item.url
  json.icon_url item.icon_url
  json.description item.description
end
json.partial! "api/shared/page", items: @items

json.only_has_username @only_has_username
if @only_has_username
  json.username @user.username
else
  json.proxy_session_id @user_session_id
  json.proxy_session_token ""
  json.app_launch_id ""
  json.signin_log_id ""
  json.user do
    json.api_token @api_token if @api_token.present?
    json.partial! 'api/client/v1/users/user', user: @user, reg: false
  end
  json.group do
    json.id @user.user_group.id
    json.name @user.user_group.name
    json.level @user.user_group.level
    # 已在v2接口去掉
    json.next_name @user.next_group.present? ? @user.next_group.name : nil
    json.next_id @user.next_group.present? ? @user.next_group.id : nil
    json.next_need_coins @user.next_group_need_coins
  end
  json.device do
    json.partial! 'api/client/v1/devices/device', device: @device
  end
  json.partial! 'api/client/v1/user_node_types/user_node_types', user_node_types: @user_node_types
  # 已在v2接口去掉
  json.partial! 'api/client/v1/nodes/nodes', node_types: @node_types
end

if @settings.present?
  json.settings do
    @settings.map{ |setting| json.set! setting.key, setting.value.strip }
  end
end

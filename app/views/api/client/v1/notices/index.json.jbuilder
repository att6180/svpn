if @notice.present?
  json.notice do
    json.id @notice.id
    json.content @notice.content
    json.created_at format_api_time(@notice.created_at)
  end
else
  json.notice nil
end

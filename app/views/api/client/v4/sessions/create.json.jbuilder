json.only_has_username @only_has_username
if @only_has_username
  json.username @user.username
else
  json.proxy_session_id @user_session_id
  json.app_launch_id ""
  json.signin_log_id ""
  json.user do
    json.api_token @api_token if @api_token.present?
    json.partial! 'api/client/v4/users/user', user: @user, reg: false
  end
  json.group do
    json.id @user_group.id
    json.name @user_group.name
    json.level @user_group.level
  end
  json.partial! 'api/client/v1/user_node_types/user_node_types', user_node_types: @user_node_types
  json.partial! 'api/client/v2/nodes/nodes', node_types: @node_types
  json.show_icon @show_icon
  json.recommend do
    json.node_type_id @node_country[:node_type_id]
    json.country_id @node_country[:id]
    json.country_name @node_country[:name]
    json.country_abbr @node_country[:abbr]
  end
end
json.ip_region do
  json.country @ip_region[:country]
  json.province @ip_region[:province]
  json.city @ip_region[:city]
end
if @settings.present?
  # 渲染公告, 替换指定字符
  render_notice(@settings, {
    "PWD": @user.password,
    "ACCOUNT": @user.username
  })
  json.settings do
    @settings.map{ |setting| json.set! setting.key, setting.value }
  end
end

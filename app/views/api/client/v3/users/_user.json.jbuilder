json.extract! user, :id, :username, :email, \
  :total_payment_amount, :current_coins, :total_coins

json.password encrypt_string(user.password)
json.is_checkin_today reg ? false : user.has_checkin_today?
json.is_enabled user.is_enabled
json.created_at format_api_time(user.created_at)
json.new_message reg ? false : user.has_new_message?
json.promo_code user.promo_code
json.promo_users_count user.promo_users_count
json.promo_coins_count user.promo_coins_count
json.binded_promo_code user.binded_promo_code

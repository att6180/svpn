json.partial! 'api/client/v2/user_node_types/user_node_types', user_node_types: @user_node_types
json.partial! 'api/client/v3/nodes/nodes', node_types: @node_types
json.show_icon @show_icon

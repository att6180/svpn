json.user_node_types @user_node_types do |user_node_type|
  json.id user_node_type.id
  json.name @node_type_indexs[user_node_type.node_type_id].name
  json.level @node_type_indexs[user_node_type.node_type_id].level
  json.status I18n.t("user_node_type.status.#{user_node_type.status}")
  json.expired_at format_api_time(user_node_type.expired_at)
  json.used_count user_node_type.used_count
  json.node_type_id user_node_type.node_type_id
end

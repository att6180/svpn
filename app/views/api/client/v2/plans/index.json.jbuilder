json.tabs @tabs do |item|
  tab = item[:tab]
  json.id tab.id
  json.name tab.name
  json.is_recommend tab.is_recommend
  json.description tab.description
  json.plans item[:plans] do |plan|
    json.id plan.id
    json.name plan.name
    json.price plan.price
    json.currency_symbol plan.currency_symbol
    json.is_iap plan.is_iap
    json.iap_id plan.iap_id
    json.is_regular_time plan.is_regular_time
    json.coins plan.coins
    json.present_coins plan.present_coins
    json.is_recommend plan.is_recommend
    json.description1 plan.description1
    json.description2 plan.description2
    json.description3 plan.description3
  end
end

if @notice.present?
  json.notice @notice[:notice]
  json.marquee @notice[:marquee]
  json.alert @notice[:alert]
else
  json.notice nil
  json.marquee nil
  json.alert nil
end

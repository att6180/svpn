json.feedbacks @feedbacks do |feedback|
  json.id feedback.id
  json.type Feedback.feedback_types[feedback.feedback_type]
  json.content feedback.content
  json.status feedback.status
  json.created_at format_api_time(feedback.created_at)
  json.has_read feedback.has_read2?
end
json.partial! "api/shared/page", items: @feedbacks

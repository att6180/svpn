json.node_types node_types do |node_type|
  json.id node_type.id
  json.name node_type.name
  json.level node_type.level
  json.expense_coins node_type.expense_coins
  json.user_group_id node_type.user_group_id
  json.user_group_level @user_group_indexs[node_type.user_group_id].level
  json.description node_type.description
  if @tabs.present?
    json.tab_id @tabs[node_type.id]&.id
  end
end

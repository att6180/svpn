json.proxy_session_token encrypt_string(token)
json.user do
  json.current_coins user.current_coins
end
json.user_node_type do
  json.id user_node_type.id
  json.expired_at format_api_time(user_node_type.expired_at)
  json.status I18n.t("user_node_type.status.#{user_node_type.status}")
  json.used_count user_node_type.used_count
end
json.node do
  json.id node.id
  json.name encrypt_string(node.name)
  json.url encrypt_string(node.url)
  json.port encrypt_string(node.port)
  json.encrypt_method encrypt_string(node.encrypt_method)
  json.password encrypt_string(node.password)
end

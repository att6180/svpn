json.navigation_config_update_at @update_at
json.hit_cache @hit_cache

if !@hit_cache
  json.tabs @tabs do |item|
    json.tab item[:tab]
    json.websites item[:websites] do |website|
      json.id website.id
      json.website_type website.website_type
      json.name website.name
      json.url website.url
      json.icon_url website.icon_url
      json.nav_type website.nav_type_before_type_cast
      json.info_type website.info_type_before_type_cast
      json.description website.description
      json.updated_at format_api_time(website.updated_at)
    end
  end
end

json.only_has_username @only_has_username
if @only_has_username
  json.username @user.username
else
  json.proxy_session_id @user_session_id
  json.proxy_session_token ""
  json.app_launch_id ""
  json.signin_log_id ""
  json.user do
    json.api_token @api_token if @api_token.present?
    json.partial! 'api/client/v1/users/user', user: @user, reg: false
  end
  json.group do
    json.id @user.user_group.id
    json.name @user.user_group.name
    json.level @user.user_group.level
  end
  json.device do
    json.partial! 'api/client/v1/devices/device', device: @device
  end
  json.partial! 'api/client/v1/user_node_types/user_node_types', user_node_types: @user_node_types
  json.partial! 'api/client/v2/nodes/nodes', node_types: @node_types
end
json.ip_region do
  json.country @ip_region[:country]
  json.province @ip_region[:province]
  json.city @ip_region[:city]
end

if @settings.present?
  # 渲染公告, 替换指定字符
  render_notice(@settings, {
    "PWD": @user.password,
    "ACCOUNT": @user.username
  })
  json.settings do
    @settings.map{ |setting| json.set! setting.key, setting.value }
  end
end

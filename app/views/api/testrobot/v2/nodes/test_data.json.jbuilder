json.session_token @user_session.token
json.nodes @nodes do |node|
  json.id node.id
  json.name node.name
  json.url node.url
  json.port node.port
  json.password node.password
  json.encrypt_method node.encrypt_method
  json.node_type_id node.node_type_id
end

json.domains @domains do |domain|
  json.id domain.id
  json.domain domain.domain
end

json.test_address @address do |is_domestic, address|
  json.in_china is_domestic
  json.address address.pluck(:domain)
end

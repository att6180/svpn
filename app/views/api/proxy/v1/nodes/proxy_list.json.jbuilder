json.proxy_list @nodes do |node|
  json.node_id node.id
  json.node_type_id node.node_type.id
  json.region_id node.node_region_id
  json.country_id node.node_region.node_country_id
  json.continent_id node.node_region.node_continent_id
  json.server node.url
  json.port node.port
  json.method node.encrypt_method
  json.password node.password
  json.in_china Utils::IP.is_domestic?(node.url) ? 1 : 0
end

if @user.present?
  json.channel @user.create_app_channel
  json.version @user.create_app_version
else
  json.channel "appStore"
  json.version "jichu"
end
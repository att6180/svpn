json.logs @logs do |log|
  json.stat_date log.stat_date
  json.stat_end_date log.stat_date.end_of_week
  json.users_count log.total_users_count
  if @total_logs.present?
    total = @total_logs[log.stat_date.strftime("%Y%m%d")].total_users_count
    json.users_percent calc_percent(log.total_users_count, total)
  else
    json.users_percent 100
  end
  json.first_week_users_count log.total_first_week_users_count
  json.first_week_users_rate UserRetentionRateStat.retention_rate(log.total_first_week_users_count, log.total_users_count)
  json.second_week_users_count log.total_second_week_users_count
  json.second_week_users_rate UserRetentionRateStat.retention_rate(log.total_second_week_users_count, log.total_users_count)
  json.third_week_users_count log.total_third_week_users_count
  json.third_week_users_rate UserRetentionRateStat.retention_rate(log.total_third_week_users_count, log.total_users_count)
  json.fourth_week_users_count log.total_fourth_week_users_count
  json.fourth_week_users_rate UserRetentionRateStat.retention_rate(log.total_fourth_week_users_count, log.total_users_count)
end
json.partial! "api/shared/page", items: @logs

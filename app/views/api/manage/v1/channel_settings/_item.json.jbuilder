json.id item.id
json.app_channel item.app_channel
json.app_version item.app_version
json.settings JSON.parse(item.settings)

json.node_isps @node_isps do |node_isp|
  json.partial! "api/manage/v1/node_isps/node_isp", node_isp: node_isp
end
json.partial! "api/shared/page", items: @node_isps

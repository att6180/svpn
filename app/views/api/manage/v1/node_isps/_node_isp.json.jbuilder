json.id node_isp.id
json.name node_isp.name
json.is_enabled node_isp.is_enabled
json.created_at format_api_time(node_isp.created_at)

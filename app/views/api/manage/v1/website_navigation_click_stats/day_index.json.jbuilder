json.logs @logs do |log|
  json.website_name log.website_name
  json.clicked_count log.total_clicked_count
end

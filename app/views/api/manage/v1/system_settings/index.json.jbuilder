json.settings @settings do |setting|
  json.partial! 'api/manage/v1/system_settings/setting', setting: setting
end
json.partial! "api/shared/page", items: @settings

json.users users do |user|
  json.user_id user.id
  json.username user.username
  json.created_at format_api_time(user.created_at)
  json.last_signin_at user.last_signin_log.present? ? format_api_time(user.last_signin_log.first.created_at) : ""
  json.last_signin_version user.last_app_signin_version_info
  json.ip user.last_signin_log.present? ? user.last_signin_log.first.ip : ""
  json.ip_country user.last_signin_log.present? ? user.last_signin_log.first.ip_country : ""
  json.ip_province user.last_signin_log.present? ? user.last_signin_log.first.ip_province : ""
  json.ip_city user.last_signin_log.present? ? user.last_signin_log.first.ip_city : ""
end
json.partial! "api/shared/page", items: users

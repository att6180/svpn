json.logs @logs do |log|
  json.created_at format_api_time(log.created_at)
  json.status log.status
  json.node_id log.node_id
  json.node_name log.node.name
  json.node_type_name log.node_type_name
  json.ip log.node_ip
end
json.partial! "api/shared/page_simple", items: @logs

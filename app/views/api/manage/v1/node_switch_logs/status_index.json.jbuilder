json.logs @logs do |log|
  next if !log.node.present?
  json.node_id log.node_id
  json.node_name log.node.name
  json.node_type_name @node_type_indexs[log.node.node_type_id].name
  json.ip log.node.url
  json.is_domestic log.node.is_domestic?
  json.total_failed_rate log.total_failed_rate
  json.total_test_count log.total_test_count
  json.average_response_time log.average_response_time
  json.last_failed_rate log.last_failed_rate
  json.last_test_count log.last_test_count
  json.last_test_status log.last_test_status
  json.switch_on_count log.switch_on_count
  json.switch_off_count log.switch_off_count
  json.created_at format_api_time(log.created_at)
  json.status log.status
end

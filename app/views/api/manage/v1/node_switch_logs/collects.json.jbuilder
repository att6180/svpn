nodes_count = 0
nodes_enabled_count = 0
json.node_types @node_types do |node_type|
  node_type_count = Node.nodes_count(node_type.id)
  nodes_count += node_type_count
  node_type_enabled_count = Node.nodes_enabled_count(node_type.id)
  nodes_enabled_count += node_type_enabled_count
  json.id node_type.id
  json.name node_type.name
  json.nodes_count node_type_count
  json.nodes_enabled_count node_type_enabled_count
  json.nodes_disabled_count node_type_count - node_type_enabled_count
end
json.nodes_count nodes_count
json.nodes_enabled_count nodes_enabled_count
json.nodes_disabled_count nodes_count - nodes_enabled_count


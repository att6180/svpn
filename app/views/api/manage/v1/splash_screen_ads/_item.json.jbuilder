json.id item.id
json.name item.name
json.duration item.duration
json.date_type item.date_type
json.date_start_at item.date_type == 0 ? "" : item.start_at.to_date
json.date_end_at item.date_type == 0 ? "" : item.end_at.to_date
json.time_type item.time_type
json.time_start_at item.time_type == 0 ? "" : item.start_at.strftime("%H:%M:%S")
json.time_end_at item.time_type == 0 ? "" : item.end_at.strftime("%H:%M:%S")
json.image_url item.image_url
json.link_url item.link_url
json.last_modification_time DateUtils.time2str(item.last_modification_time)
json.selected_count SplashScreenAd.get_selected_count(item.id)
json.imporessions_count SplashScreenAd.get_imporessions_count(item.id)
json.is_enabled item.is_enabled
json.ad_type item.ad_type #I18n.t("splash_screen_ad.ad_type.#{item.ad_type}")
json.open_link_type item.open_link_type
json.app_channel item.app_channel
json.app_version item.app_version

json.is_enabled Setting.splash_screen_ad_enabled
json.overtime Setting.splash_screen_ad_overtime

json.items @items do |item|
  json.partial! "api/manage/v1/splash_screen_ads/item", item: item
end
json.partial! "api/shared/page", items: @items

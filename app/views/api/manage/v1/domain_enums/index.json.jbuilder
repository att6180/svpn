json.domain_enums @enums do |enum|
  json.partial! 'api/manage/v1/domain_enums/domain_enum', enum: enum
end
json.partial! "api/shared/page", items: @enums

json.id enum.id
json.domain enum.domain
json.description enum.description
json.created_at format_api_time(enum.created_at)

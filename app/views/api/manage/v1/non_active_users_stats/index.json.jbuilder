json.logs @logs do |log|
  json.stat_date log.stat_date
  json.users_count log.users_count
  json.nonactive_users_count log.nonactive_users_count
  json.rate calc_percent(log.nonactive_users_count, log.users_count)
end

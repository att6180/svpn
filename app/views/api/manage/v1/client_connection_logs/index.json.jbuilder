json.logs @logs do |log|
  json.user_id log.user_id
  json.username log.user.present? ? log.user.username : '未知'
  json.node_id log.node_id
  json.node_name log.node.present? ? log.node.name : '未知'
  json.node_ip log.node.present? ? log.node.url : '未知'
  json.target_url log.target_url
  json.is_proxy log.is_proxy
  json.app_channel log.app_channel
  json.app_version log.app_version
  json.app_version_number log.app_version_number
  json.created_at format_api_time(log.created_at)
end
json.partial! "api/shared/page_simple", items: @logs

json.id node_icp.id
json.tag node_icp.tag
json.is_enabled node_icp.is_enabled
json.isps node_icp.isps
json.created_at format_api_time(node_icp.created_at)

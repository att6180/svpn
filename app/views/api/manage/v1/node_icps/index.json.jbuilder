json.node_icps @node_icps do |node_icp|
  json.partial! "api/manage/v1/node_icps/node_icp", node_icp: node_icp
end
json.partial! "api/shared/page", items: @node_icps

json.logs @logs do |log|
  json.ad_name log.ad_name
  json.clicked_count log.total_clicked_count
end

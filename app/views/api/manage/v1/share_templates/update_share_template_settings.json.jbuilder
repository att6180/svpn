json.text_logo_url Setting.share_text_logo_url
json.text_logo_update_time Setting.share_text_logo_update_time.to_i
json.weixin_enabled Setting.share_weixin_enabled == "true"
json.friend_circle_enabled Setting.share_friend_circle_enabled == "true"
json.qq_enabled Setting.share_qq_enabled == "true"
json.weibo_enabled Setting.share_weibo_enabled == "true"

json.share_templates @share_templates do |share_template|
  json.partial! "api/manage/v1/share_templates/share_template", share_template: share_template
end
json.partial! "api/shared/page", items: @share_templates

json.items @items do |item|
  json.partial! 'api/manage/v1/navigation_ads/item', item: item
end
json.partial! "api/shared/page", items: @items

json.id item.id
json.priority item.priority
json.name item.name
json.image_url item.image_url
json.link_url item.link_url
json.is_enabled item.is_enabled
json.updated_at format_api_time(item.updated_at)
json.created_at format_api_time(item.created_at)

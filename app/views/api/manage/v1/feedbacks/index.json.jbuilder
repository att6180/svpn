json.feedbacks @feedbacks do |feedback|
  json.partial! "api/manage/v1/feedbacks/feedback", feedback: feedback
end
json.partial! "api/shared/page", items: @feedbacks

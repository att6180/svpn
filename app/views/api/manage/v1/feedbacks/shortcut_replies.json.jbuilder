json.items @items do |item|
  json.id item.id
  json.name item.name
  json.replies item.feedback_shortcut_replies do |reply|
    json.id reply.id
    json.content reply.content
  end
end

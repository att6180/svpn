json.logs @logs do |log|
  json.id log.id
  json.member_id log.memberable.id
  json.member_type log.memberable_type == 'User' ? 'user' : 'admin'
  json.member_name log.memberable_type == 'User' ? log.memberable.username : "管理员#{log.memberable.username}"
  json.content h(log.content)
  json.created_at format_api_time(log.created_at)
end
json.partial! "api/shared/page", items: @logs

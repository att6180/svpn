json.feedback do
  json.partial! "api/manage/v1/feedbacks/feedback", feedback: @feedback
end
json.reply do
  json.id @reply.id
  json.member_id @reply.memberable.id
  json.member_type @reply.memberable_type == 'User' ? 'user' : 'admin'
  json.member_name @reply.memberable_type == 'User' ? @reply.memberable.username : "管理员#{@reply.memberable.username}"
  json.content @reply.content
  json.created_at format_api_time(@reply.created_at)
end

json.id feedback.id
json.app_channel feedback.app_channel
json.app_version feedback.app_version
json.feedback_type t("feedback_types.#{feedback.feedback_type}")
json.user_id feedback.user.id
json.user_name feedback.user.username
json.content h(feedback.content)
json.created_at format_api_time(feedback.created_at)
json.status feedback.status
json.status_updated_at feedback.status_updated_at.present? ? format_api_time(feedback.status_updated_at) : nil
json.has_read feedback.has_read2?
json.has_read_at feedback.has_read_at2.present? ? format_api_time(feedback.has_read_at2) : nil

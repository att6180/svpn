json.logs @logs do |log|
  json.stat_date log.stat_date
  json.users_count log.t_users_count
  if @total_logs.present?
    total = @total_logs[log.stat_date.strftime("%Y%m%d")].t_users_count
    json.users_percent calc_percent(log.t_users_count, total)
  else
    json.users_percent 100
  end

  json.paid_users_count log.t_paid_users_count
  json.paid_total_price log.t_total_price
  json.average_price calc_per_average(log.t_total_price, log.t_paid_users_count)
  json.payment_rate calc_percent(log.t_paid_users_count, log.t_users_count)

  json.second_paid_users_count log.t_second_paid_users_count
  json.second_paid_total_price log.t_second_total_price
  json.second_average_price calc_per_average(log.t_second_total_price, log.t_second_paid_users_count)
  json.second_payment_rate calc_percent(log.t_second_paid_users_count, log.t_users_count)

  json.seventh_paid_users_count log.t_seventh_paid_users_count
  json.seventh_paid_total_price log.t_seventh_total_price
  json.seventh_average_price calc_per_average(log.t_seventh_total_price, log.t_seventh_paid_users_count)
  json.seventh_payment_rate calc_percent(log.t_seventh_paid_users_count, log.t_users_count)

  json.thirtieth_paid_users_count log.t_thirtieth_paid_users_count
  json.thirtieth_paid_total_price log.t_thirtieth_total_price
  json.thirtieth_average_price calc_per_average(log.t_thirtieth_total_price, log.t_thirtieth_paid_users_count)
  json.thirtieth_payment_rate calc_percent(log.t_thirtieth_paid_users_count, log.t_users_count)

  json.seven_days_users_count log.t_seventh_users_count
  json.seven_days_total_price log.t_seventh_17_total_price
  json.thirty_days_users_count log.t_thirtieth_users_count
  json.thirty_days_total_price log.t_thirtieth_130_total_price
end
json.partial! "api/shared/page", items: @logs

json.logs @logs do |log|
  json.stat_date log.stat_date
  json.users_count log.t_users_count

  json.payment_rate calc_percent(log.t_paid_users_count, log.t_users_count)
  json.second_payment_rate calc_percent(log.t_second_paid_users_count, log.t_users_count)
  json.seventh_payment_rate calc_percent(log.t_seventh_paid_users_count, log.t_users_count)
  json.thirtieth_payment_rate calc_percent(log.t_thirtieth_paid_users_count, log.t_users_count)
end

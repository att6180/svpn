json.logs @logs do |log|
  json.user_id log.user_id
  json.username log.user.username
  json.orders_count log.orders_count
  json.total_price log.total_price
end
json.partial! "api/shared/page", items: @logs

json.logs @logs do |log|
  json.stat_date log.stat_date
  json.users_count log.users_count
  price_types = JSON.parse(log.type_users_count)
  json.price_types price_types do |pt|
    json.price_type pt["price_type"]
    json.names pt["names"] || []
    json.users_count pt["users_count"]
    json.rate calc_percent(pt["users_count"], log.users_count)
  end
end

json.extract! user_group, :id, :name, :level, :need_coins, :is_enabled
json.created_at format_api_time(user_group.created_at)

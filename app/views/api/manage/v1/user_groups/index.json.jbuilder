json.user_groups @user_groups do |user_group|
  json.partial! 'api/manage/v1/user_groups/user_group', user_group: user_group
end
json.partial! "api/shared/page", items: @user_groups

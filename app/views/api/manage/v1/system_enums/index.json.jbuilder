json.system_enums @system_enums do |system_enum|
  json.partial! "api/manage/v1/system_enums/system_enum", system_enum: system_enum
end
json.partial! "api/shared/page", items: @system_enums

json.channels @channels do |system_enum|
  json.partial! "api/manage/v1/system_enums/system_enum", system_enum: system_enum
end
json.versions @versions do |system_enum|
  json.partial! "api/manage/v1/system_enums/system_enum", system_enum: system_enum
end
json.platforms @platforms do |system_enum|
  json.partial! "api/manage/v1/system_enums/system_enum", system_enum: system_enum
end
json.channel_accounts @channel_accounts do |system_enum|
  json.partial! "api/manage/v1/system_enums/system_enum", system_enum: system_enum
end
json.packages @packages do |system_enum|
  json.partial! "api/manage/v1/system_enums/system_enum", system_enum: system_enum
end
json.relations @relations do |relation|
  json.platform relation.platform
  json.channel_account relation.channel_account
  json.package relation.package
end

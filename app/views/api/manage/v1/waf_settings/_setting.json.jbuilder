json.id setting&.id
json.brush_time setting&.brush_time
json.brush_count setting&.brush_count
json.trigger_count setting&.trigger_count
json.created_at format_api_time(setting&.created_at)

json.configs @configs do |config|
  json.partial! 'api/manage/v1/anti_brush_configs/config', config: config
end
json.partial! "api/shared/page", items: @configs

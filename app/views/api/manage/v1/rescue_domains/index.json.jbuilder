json.rescue_domains @rescue_domains do |rescue_domain|
  json.partial! "api/manage/v1/rescue_domains/rescue_domain", rescue_domain: rescue_domain
end
json.partial! "api/shared/page", items: @rescue_domains

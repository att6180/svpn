json.items @items do |item|
  json.partial! "api/manage/v1/connection_log_filters/item", item: item
end
json.customer_services @customer_services do |cs|
  json.partial! 'api/manage/v1/customer_services/cs', cs: cs
end
json.partial! "api/shared/page", items: @customer_services

total_users_count = @channels.map{|i| i[:users_count]}.sum
json.channels @channels do |item|
    json.name item[:name]
    json.users_count item[:users_count]
    json.rate calc_percent(item[:users_count], total_users_count)
end
total_users_count = @versions.map{|i| i[:users_count]}.sum
json.versions @versions do |item|
    json.name item[:name]
    json.users_count item[:users_count]
    json.rate calc_percent(item[:users_count], total_users_count)
end
total_users_count = @platform_vns.map{|i| i[:users_count]}.sum
json.version_numbers @platform_vns do |item|
    json.platform item[:platform]
    json.version_number item[:version_number]
    json.users_count item[:users_count]
    json.rate calc_percent(item[:users_count], total_users_count)
end
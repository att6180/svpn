total_users_count = @channels.map{|k, v| v.to_i}.sum
json.channels @channels do |k, v|
    json.name k
    json.users_count v.to_i
    json.rate calc_percent(v.to_i, total_users_count)
end
total_users_count = @versions.map{|k, v| v.to_i}.sum
json.versions @versions do |k, v|
    json.name k
    json.users_count v.to_i
    json.rate calc_percent(v.to_i, total_users_count)
end
total_users_count = @platform_vns.map{|k, v| v.to_i}.sum
json.version_numbers @platform_vns do |k, v|
    platform, version_number = k.split('___')
    json.platform platform
    json.version_number version_number
    json.users_count v.to_i
    json.rate calc_percent(v.to_i, total_users_count)
end
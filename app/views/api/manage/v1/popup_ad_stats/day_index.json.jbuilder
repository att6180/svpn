json.logs @logs do |log|
  json.stat_date log.stat_date
  json.showed_count log.showed_count
  json.clicked_count log.total_clicked_count
end

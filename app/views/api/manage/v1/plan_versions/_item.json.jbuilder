json.id item.id
json.channel item.channel
json.version item.version
json.audit_template_id item.audit_template_id
json.domestic_template_id item.domestic_template_id
json.oversea_template_id item.oversea_template_id
json.iap_template_id item.iap_template_id
json.show_icon item.show_icon
json.tab_id_sort item.tab_id_sort
json.status item.status
json.created_at format_api_time(item.created_at)

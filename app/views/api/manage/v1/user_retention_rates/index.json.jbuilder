json.logs @logs do |log|
  json.stat_date log.stat_date
  json.users_count log.total_users_count
  if @total_logs.present?
    total = @total_logs[log.stat_date.strftime("%Y%m%d")].total_users_count
    json.users_percent calc_percent(log.total_users_count, total)
  else
    json.users_percent 100
  end
  json.second_day log.total_second_day
  json.second_day_rate UserRetentionRateStat.retention_rate(log.total_second_day, log.total_users_count)
  json.third_day log.total_third_day
  json.third_day_rate UserRetentionRateStat.retention_rate(log.total_third_day, log.total_users_count)
  json.seventh_day log.total_seventh_day
  json.seventh_day_rate UserRetentionRateStat.retention_rate(log.total_seventh_day, log.total_users_count)
  json.fifteen_day log.total_fifteen_day
  json.fifteen_day_rate UserRetentionRateStat.retention_rate(log.total_fifteen_day, log.total_users_count)
  json.thirtieth_day log.total_thirtieth_day
  json.thirtieth_day_rate UserRetentionRateStat.retention_rate(log.total_thirtieth_day, log.total_users_count)
end
json.partial! "api/shared/page", items: @logs

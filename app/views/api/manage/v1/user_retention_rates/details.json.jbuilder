json.users @users do |user|
  json.user_id user.id
  json.username user.username
  json.created_at format_api_time(user.created_at)
  json.created_version_info user.created_version_info
  json.ip user.create_ip
  ip_region = Ipnet.find_by_ip(user.create_ip)
  json.ip_country ip_region[:country]
  json.ip_province ip_region[:province]
  json.ip_city ip_region[:city]
end
json.partial! "api/shared/page", items: @users

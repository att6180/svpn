json.id plan.id
json.platform plan.platform
json.app_version plan.app_version
json.app_version_number plan.app_version_number
json.name plan.name
json.is_regular_time plan.is_regular_time
json.time_type plan.time_type
json.node_type_name plan.node_type.present? ? plan.node_type.name : ''
json.node_type_id plan.node_type_id
json.coins plan.coins
json.price plan.price
json.present_coins plan.present_coins
json.is_enabled plan.is_enabled
json.is_audit plan.is_audit
json.is_iap plan.is_iap
json.iap_id plan.iap_id
json.description plan.description
json.created_at format_api_time(plan.created_at)

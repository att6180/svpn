json.plans @plans do |plan|
  json.partial! 'api/manage/v1/plans/plan', plan: plan
end
json.partial! "api/shared/page", items: @plans

json.id item.id
json.platform item.platform
json.app_version item.app_version
json.app_version_number item.app_version_number
json.title item.title
json.content item.content
json.created_at format_api_time(item.created_at)

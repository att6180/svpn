json.help_manuals @items do |item|
  json.partial! 'api/manage/v1/help_manuals/help_manual', item: item
end
json.partial! "api/shared/page", items: @items

json.items @items do |item|
  json.partial! "api/manage/v1/plan_template_items/item", item: item
end
json.partial! "api/shared/page", items: @items

json.logs @logs do |log|
  json.fail_type log.fail_type
  json.user_id log.user_id
  json.username log.user.username
  json.device_uuid log.device_uuid
  json.device_model log.device_model
  json.node_type log.node_type
  json.node_region log.node_region
  json.node_name log.node_name
  json.ip log.ip
  json.ip_country log.ip_country
  json.ip_province log.ip_province
  json.ip_city log.ip_city
  json.operator log.operator
  json.created_at format_api_time(log.created_at)
end
json.partial! "api/shared/page", items: @logs

json.logs logs do |log|
  json.stat_date log[:stat_date]
  json.connection_failed_total log[:type1]
  json.crash_total log[:type2]
  json.unconnected_total log[:type3]
  json.total log[:type1] + log[:type2] + log[:type3]
end

json.users @users do |user|
  json.id user.id
  json.user_id user.user_id
  json.username user.user.username
  json.ip user.ip
  json.ip_country user.ip_country
  json.ip_province user.ip_province
  json.ip_city user.ip_city
  json.ip_area user.ip_area
  json.created_at format_api_time(user.created_at)
end
json.partial! "api/shared/page", items: @users

json.logs logs do |log|
  json.stat_date log.stat_date

  json.min_bandwidth_percent log.min_bandwidth_percent
  json.min_bandwidth_time format_api_time(log.min_bandwidth_time)
  json.max_bandwidth_percent log.max_bandwidth_percent
  json.max_bandwidth_time format_api_time(log.max_bandwidth_time)
  json.average_bandwidth_percent log.average_bandwidth_percent

  json.min_cpu_percent log.min_cpu_percent
  json.min_cpu_time format_api_time(log.min_cpu_time)
  json.max_cpu_percent log.max_cpu_percent
  json.max_cpu_time format_api_time(log.max_cpu_time)
  json.average_cpu_percent log.average_cpu_percent

  json.min_memory_percent log.min_memory_percent
  json.min_memory_time format_api_time(log.min_memory_time)
  json.max_memory_percent log.max_memory_percent
  json.max_memory_time format_api_time(log.max_memory_time)
  json.average_memory_percent log.average_memory_percent

  json.min_network_speed_up log.min_network_speed_up
  json.min_network_speed_up_time format_api_time(log.min_network_speed_up_time)
  json.max_network_speed_up log.max_network_speed_up
  json.max_network_speed_up_time format_api_time(log.max_network_speed_up_time)
  json.average_network_speed_up log.average_network_speed_up

  json.min_network_speed_down log.min_network_speed_down
  json.min_network_speed_down_time format_api_time(log.min_network_speed_down_time)
  json.max_network_speed_down log.max_network_speed_down
  json.max_network_speed_down_time format_api_time(log.max_network_speed_down_time)
  json.average_network_speed_down log.average_network_speed_down

  json.min_disk_percent log.min_disk_percent
  json.min_disk_time format_api_time(log.min_disk_time)
  json.max_disk_percent log.max_disk_percent
  json.max_disk_time format_api_time(log.max_disk_time)
  json.average_disk_percent log.average_disk_percent
end

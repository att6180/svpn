json.node_types @node_types do |node_type|
  json.id node_type.id
  json.name node_type.name
end
json.partial! "api/manage/v1/nodes/online_users_stat", logs: @logs
json.partial! "api/shared/page", items: @logs

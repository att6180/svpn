json.logs @logs do |log|
  json.time log.hm
  json.online_count log.online_count
end
json.partial! "api/shared/page", items: @logs

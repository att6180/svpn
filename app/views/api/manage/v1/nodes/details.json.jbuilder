json.nodes @nodes do |node|
  json.id node.id
  json.name node.name
  json.url node.url
  json.node_region_id node.node_region.id
  json.node_region_name node.node_region.name
  json.connections_count node.cache_online_users
  json.max_connections_count node.max_connections_count
  connections_percent = calc_percent(node.cache_online_users, node.max_connections_count)
  json.connections_percent "#{connections_percent}%"
  json.current_bandwidth node.node_addition.current_bandwidth
  json.cpu_percent node.node_addition.cpu_percent
  json.memory_percent node.node_addition.memory_percent
  json.transfer node.node_addition.transfer
  json.network_speed_up node.node_addition.network_speed_up
  json.network_speed_down node.node_addition.network_speed_down
  json.status node.status
end
json.partial! "api/shared/page", items: @nodes

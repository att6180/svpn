json.nodes @nodes do |node|
  json.partial! "api/manage/v1/nodes/node", node: node
end
json.partial! "api/shared/page", items: @nodes

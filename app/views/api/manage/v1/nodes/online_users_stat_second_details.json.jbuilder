json.logs @logs do |log|
  json.created_at format_api_time(log.created_at)
  json.node_id log.node_id
  json.node_name log.node.name
  json.online_count log.online_users_count
end
json.partial! "api/shared/page", items: @logs

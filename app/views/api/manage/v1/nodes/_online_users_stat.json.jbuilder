json.logs logs do |log|
  json.stat_date log.stat_date
  json.min_count log.min_count
  json.min_time format_api_time(log.min_time)
  json.max_count log.max_count
  json.max_time format_api_time(log.max_time)
  json.average_count log.average_count
  if log.node_type_info.present?
    node_type_list = JSON.parse(log.node_type_info)
    json.node_type_list node_type_list do |k, v|
      json.node_id k.to_i
      json.min_count v["min_count"]
      min_time = v["min_time"] ? format_api_time(DateTime.parse(v["min_time"])) : nil
      json.min_time min_time
      json.max_count v["max_count"]
      max_time = v["max_time"] ? format_api_time(DateTime.parse(v["max_time"])) : nil
      json.max_time max_time
      json.average_count v["average_count"]
    end
  end
end

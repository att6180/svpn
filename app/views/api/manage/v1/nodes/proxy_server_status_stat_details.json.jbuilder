json.logs @logs do |log|
  json.hour_minute log.created_at.strftime("%H:%M")
  json.node_id log.node_id
  json.node_name log.node.name
  json.bandwidth_percent log.bandwidth_percent
  json.cpu_percent log.cpu_percent
  json.memory_percent log.memory_percent
  json.transfer log.transfer
  json.network_speed_up log.network_speed_up
  json.network_speed_down log.network_speed_down
  json.disk_percent log.disk_percent
end
json.partial! "api/shared/page", items: @logs

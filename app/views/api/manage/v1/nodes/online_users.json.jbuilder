json.online_users @node_types do |node_type|
  json.name node_type[:node_type_name]
  json.online_users node_type[:online_user_count]
end

json.isp_online_users @isp_online_users do |node|
  json.node_isp_name node[:node_isp_name]
  json.node_type_name node[:node_type_name]
  json.node_count node[:node_count]
  json.online_user_count node[:online_user_count]
end

json.covered_online_users @total_nodes.covered.sum(&:cache_online_users)
json.smart_online_users @total_nodes.smart.sum(&:cache_online_users)
json.customization_online_users @total_nodes.customization.sum(&:cache_online_users)

json.nodes @nodes do |node|
  json.id node.id
  json.node_isp_name node.node_isp&.name
  json.name node.name
  json.types node.types
  json.is_domestic node.is_domestic
  json.is_enabled node.enabled?
  json.ip node.url
  json.level node.level
  json.is_domestic node.is_domestic
  json.node_type_id node.node_region.node_type.id
  json.node_type_name node.node_region.node_type.name
  json.region_id node.node_region.id
  json.region_name node.node_region.name
  json.region_abbr node.node_region.abbr
  json.connections_count node.cache_online_users
end
json.partial! "api/shared/page", items: @nodes

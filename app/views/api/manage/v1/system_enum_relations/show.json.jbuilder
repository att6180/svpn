json.id @item.id
json.platform @item.platform_id
json.channel_account @item.channel_account_id
json.package @item.package_id
json.channel @item.channel
json.version @item.version
json.description @item.description

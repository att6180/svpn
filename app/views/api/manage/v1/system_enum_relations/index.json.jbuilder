json.items @items do |item|
  json.partial! "api/manage/v1/system_enum_relations/item", item: item
end
json.partial! "api/shared/page", items: @items

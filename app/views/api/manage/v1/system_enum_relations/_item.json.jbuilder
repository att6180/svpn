json.id item.id
json.platform item.platform&.name
json.channel_account item.channel_account&.name
json.package item.package&.name
json.channel item.channel
json.version item.version
json.description item.description

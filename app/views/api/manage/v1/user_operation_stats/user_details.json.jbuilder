json.logs @logs do |log|
  json.user_id log.user_id
  json.username log.user.present? ? log.user.username : nil
end
json.partial! "api/shared/page", items: @logs

json.logs @logs do |log|
  json.user_id log.user_id
  json.username @users[log.user_id]&.username || nil 
  json.created_at format_api_time(log.created_at)
  json.app_channel log.app_channel
  json.app_version log.app_version
  json.app_version_number log.app_version_number
end
json.partial! "api/shared/page", items: @logs

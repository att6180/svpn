json.date_list @date_list

json.logs do
  @ui_list.each do |key, value|
    json.set! key, value
  end
end

json.interfaces @interfaces do |i|
  json.interface_id i[:interface_id]
  json.interface_name i[:interface_name]
  json.visits_count i[:visits_count]
  json.users_count i[:users_count]
end

json.user_taps @user_taps do |user_tap|
  json.partial! "api/manage/v1/user_taps/user_tap", user_tap: user_tap
end
json.partial! "api/shared/page", items: @user_taps

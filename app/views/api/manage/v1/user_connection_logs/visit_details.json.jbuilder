json.logs @logs do |log|
  json.created_at format_api_time(log.created_at)
  json.client_ip log.client_ip
  json.ip_country log.ip_country
  json.ip_province log.ip_province
  json.ip_city log.ip_city
end
json.partial! "api/shared/page_simple", items: @logs

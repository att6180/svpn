json.logs @logs do |log|
  json.user_id log[:user_id].to_i
  json.username @users_group[log[:user_id].to_i]&.first&.username
  json.visits_count log[:views]
end
#json.partial! "api/shared/page", items: @logs

json.logs @logs do |log|
  json.ip_country log[:ip_country]
  json.ip_province log[:ip_province]
  json.ip_city log[:ip_city]
  json.users_count log[:users]
  json.total_visits_count log[:views]
  json.percent calc_percent(log[:users], log[:domain_views])
end

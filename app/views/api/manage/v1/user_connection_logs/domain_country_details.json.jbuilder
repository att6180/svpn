json.logs @logs do |log|
  json.ip_country log[:ip_country]
  json.users_count log[:users]
end

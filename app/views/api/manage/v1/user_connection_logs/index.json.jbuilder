json.logs @logs do |log|
  json.domain log[:domain]
  json.domain_description DomainEnum.desc_by_domain(log[:domain])
  json.users_count log[:users]
  json.visits_count log[:views]
end
#json.partial! "api/shared/page", items: @logs

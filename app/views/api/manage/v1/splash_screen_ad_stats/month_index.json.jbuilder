json.logs @logs do |log|
  json.stat_year log.y
  json.stat_month log.m
  json.showed_count log.total_showed_count
  json.clicked_count log.total_clicked_count
end

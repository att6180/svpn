json.id admin.id
json.username admin.username
json.is_enabled admin.is_enabled
json.current_signin_at admin.current_signin_at.present? ? format_api_time(admin.current_signin_at) : nil
json.current_signin_ip admin.current_signin_ip
json.last_signin_at admin.last_signin_at.present? ? format_api_time(admin.last_signin_at) : nil
json.last_signin_ip admin.last_signin_ip
json.created_at format_api_time(admin.created_at)
json.role do
  json.partial! 'api/manage/v1/admin_roles/role', role: admin.role
end

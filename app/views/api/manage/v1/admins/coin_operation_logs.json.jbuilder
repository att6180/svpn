json.logs @logs do |log|
  json.id log.id
  json.admin_id log.admin_id
  json.admin_name log.admin.username
  json.user_id log.user_id
  json.username log.user.username
  json.original_coins log.original_coins
  json.current_coins log.current_coins
  json.increased_coins log.operation_type == 0 ? 0 : log.current_coins - log.original_coins
  json.operation_type I18n.t("activity_coin_reward_log.operation_type_#{log.operation_type}")
  json.created_at format_api_time(log.created_at)
end
json.partial! "api/shared/page", items: @logs

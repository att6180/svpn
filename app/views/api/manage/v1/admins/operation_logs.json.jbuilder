json.logs @logs do |log|
  json.id log.id
  json.admin_id log.admin.id
  json.admin_name log.admin.username
  json.content log.log
  json.created_at format_api_time(log.created_at)
end
json.partial! "api/shared/page", items: @logs

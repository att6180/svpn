json.logs @logs do |log|
  json.id log.id
  json.admin_id log.admin_id
  json.admin_name log.admin.username
  json.user_id log.user_id
  json.username log.user.username
  json.original_expired_at format_api_time(log.original_expired_at)
  json.current_expired_at format_api_time(log.current_expired_at)
  json.node_type_name log.node_type.name
  json.created_at format_api_time(log.created_at)
end
json.partial! "api/shared/page", items: @logs

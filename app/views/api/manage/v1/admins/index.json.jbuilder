json.admins @admins do |admin|
  json.partial! 'api/manage/v1/admins/admin', admin: admin
end
json.partial! "api/shared/page", items: @admins

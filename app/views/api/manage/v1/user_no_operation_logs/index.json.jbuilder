json.logs @logs do |log|
  json.stat_date log[:stat_date]
  json.no_operations_count log[:no_operations_count]
end
json.partial! "api/shared/page", items: @logs

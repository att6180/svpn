json.token @token
json.admin_id @admin.id
json.admin_username @admin.username
json.app_versions @app_versions do |version|
  json.id version.id
  json.name version.name
end
json.app_channels @app_channels do |channel|
  json.id channel.id
  json.name channel.name
end
json.role do
  json.partial! 'api/manage/v1/admin_roles/role', role: @admin.role
end

json.items @items do |item|
  json.partial! "api/manage/v1/proxy_test_domains/item", item: item
end
json.partial! "api/shared/page", items: @items

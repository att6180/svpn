json.id item.id
json.domain item.domain
json.is_domestic item.is_domestic
json.created_at format_api_time(item.created_at)

json.regions @regions do |region|
  json.partial! "api/manage/v1/node_regions/node_region", region: region
end
json.partial! "api/shared/page", items: @regions

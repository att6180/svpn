json.notices @notices do |notice|
  json.partial! "api/manage/v1/notices/notice", notice: notice
end
json.partial! "api/shared/page", items: @notices

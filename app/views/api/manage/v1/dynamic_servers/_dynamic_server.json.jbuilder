json.id server.id
json.url server.url
json.region server.region
json.priority server.priority
json.is_enabled server.is_enabled?
json.failure_count server.cache_failure_count
json.created_at format_api_time(server.created_at)

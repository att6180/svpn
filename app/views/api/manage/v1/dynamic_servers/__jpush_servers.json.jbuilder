json.type type
json.sent_time Time.now.to_i
json.servers servers do |server|
  json.url server.url
  json.region server.region
  json.priority server.priority
end

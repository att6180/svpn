json.dynamic_servers @servers do |server|
  json.partial! 'api/manage/v1/dynamic_servers/dynamic_server', server: server
end
json.partial! "api/shared/page", items: @servers

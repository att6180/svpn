json.dynamic_servers @logs do |log|
  server = log.dynamic_server
  json.id log.dynamic_server_id
  json.url server.url
  json.region server.region
  json.priority server.priority
  json.is_enabled server.is_enabled?
  json.failed_count log.failure_count
end
json.partial! "api/shared/page", items: @logs

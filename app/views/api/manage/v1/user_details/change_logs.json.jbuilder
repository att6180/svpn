json.logs @logs do |log|
  json.id log.id
  json.change_type I18n.t("user_change_log.type_#{log.change_type}")
  json.uuid log.uuid
  json.platform log.platform
  json.ip log.ip
  json.before log.before
  json.after log.after
  json.created_at format_api_time(log.created_at)
end
json.partial! "api/shared/page", items: @logs

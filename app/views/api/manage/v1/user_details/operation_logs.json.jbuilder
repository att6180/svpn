times = @logs.present? && @logs.first.interface_id == 1 ? 0 : 1
step = 1
json.logs @logs do |log|
  # 如果是启动页，就给开启次数+1
  if log.interface_id == 1
    times += 1
    step = 1
  end
  json.times times
  json.step step
  json.interface_id log.interface_id
  json.interface_name I18n.t("user_operation_log.ui_#{log.interface_id}")
  json.created_at format_api_time(log.created_at)
  step += 1
end

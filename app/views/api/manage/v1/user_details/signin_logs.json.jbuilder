json.logs @logs do |log|
  json.id log.id
  json.created_at format_api_time(log.created_at)
  json.device_model log.device_model
  json.platform log.platform
  json.system_version log.system_version
  json.app_channel log.app_channel
  json.app_version log.app_version
  json.app_version_number log.app_version_number
  json.ip log.ip
  json.ip_country log.ip_country
  json.ip_province log.ip_province
  json.ip_city log.ip_city
end
json.partial! "api/shared/page", items: @logs

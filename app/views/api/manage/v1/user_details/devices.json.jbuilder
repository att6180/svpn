json.devices @devices do |device|
  json.id device.id
  json.uuid device.uuid
  json.name device.name
  json.model device.model
  json.platform device.platform
  json.system_version device.system_version
  json.net_env device.net_env
  json.created_at format_api_time(device.created_at)
end
json.partial! "api/shared/page", items: @devices

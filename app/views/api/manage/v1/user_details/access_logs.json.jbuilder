json.logs @logs do |log|
  json.id log.id.to_s
  first_node = @first_nodes[log.first_proxy_ip]
  json.first_node_id first_node.present? ? first_node.id : nil
  json.first_node_name first_node.present? ? first_node.name : nil
  json.first_node_ip log.first_proxy_ip
  second_node = @group_nodes[log.node_id]
  json.second_node_id second_node.id
  json.second_node_name second_node.name
  json.second_node_ip second_node.url
  node_type = @group_node_types[second_node.node_type_id]
  json.node_type_id node_type.id
  json.node_type_name node_type.name
  json.created_at format_api_time(log.created_at)
  json.client_ip log.client_ip
  json.client_ip_country log.ip_country
  json.client_ip_province log.ip_province
  json.client_ip_city log.ip_city
  json.domain log.domain
  json.domain_description DomainEnum.desc_by_domain(log.domain)
end
json.partial! "api/shared/page_simple", items: @page_logs

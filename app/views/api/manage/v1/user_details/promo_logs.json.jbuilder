json.logs @logs do |log|
  json.id log.id
  json.user_id log.accepted_user_id
  json.username @accepted_users[log.accepted_user_id]&.username
  json.promoter_coins log.promoter_coins
  json.user_coins log.accepted_user_coins
  json.created_at format_api_time(log.created_at)
end
json.partial! "api/shared/page", items: @logs

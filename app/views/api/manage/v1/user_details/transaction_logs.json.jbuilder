json.total_payment_amount @user.total_payment_amount
json.filter_payment_amount @filter_payment_amount
json.partial! 'api/manage/v1/transaction_logs/transaction_logs', logs: @logs

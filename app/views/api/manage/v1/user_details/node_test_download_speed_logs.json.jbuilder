json.download_url_list @download_url_list

json.logs @logs do |log|
  json.id log.id.to_s
  json.client_ip log.client_ip
  json.client_ip_country log.ip_country
  json.client_ip_province log.ip_province
  json.client_ip_city log.ip_city
  json.client_ip_area log.ip_area
  json.download_url log.download_url
  json.waste_time log.waste_time
  json.created_at format_api_time(log.created_at)
end
json.partial! "api/shared/page_simple", items: @logs

json.user do
  json.username user.username
  json.password user.password
  json.current_coins user.current_coins
  json.user_group_id user.user_group_id
  json.is_enabled user.is_enabled
end

json.user_node_types user_node_types do |node_type|
  if node_type.node_type.is_enabled?
    json.id node_type.id
    json.node_type_name node_type.node_type.name
    json.expired_at l(node_type.expired_at, format: :fdts)
  end
end


json.user_node_types @user_node_types do |user_node_type|
  json.id user_node_type.id
  json.name user_node_type.node_type.name
  json.level user_node_type.node_type.level
  json.status I18n.t("user_node_type.status.#{user_node_type.status}")
  json.expired_at format_api_time(user_node_type.expired_at)
  json.used_count user_node_type.used_count
  json.node_type_id user_node_type.node_type_id
end
json.id @user.id
json.area_code @user.area_code
json.username @user.username
json.last_signin_at format_api_time(@user.current_signin_at)
json.last_signin_version @user.last_signin_versions
json.group_name @user_group_indexs[@user.user_group_id].name
json.total_payment_amount @user.total_payment_amount
json.current_coins @user.current_coins
json.total_used_coins @user.total_used_coins
json.uuid @user.create_device_uuid
json.is_enabled @user.is_enabled?
json.create_at format_api_time(@user.created_at)
json.create_version @user.created_version_info

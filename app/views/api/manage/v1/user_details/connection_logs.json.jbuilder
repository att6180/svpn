json.connection_logs @logs do |log|
  json.id log.id
  json.created_at format_api_time(log.created_at)
  json.wait_time log.wait_time
  json.node_type_name log.node_type_name
  json.node_region_name log.node_region_name
  json.node_name log.node_name
  json.node_url log.node.url
  json.ip log.ip
  json.ip_country log.ip_country
  json.ip_province log.ip_province
  json.ip_city log.ip_city
  json.status_value log.status
  json.domain log.domain
  json.status_title I18n.t("node_connection_log.status.#{log.status}")
end
json.partial! "api/shared/page", items: @logs

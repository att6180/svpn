json.logs @logs do |log|
  json.id log.id
  json.node_id log.node_id
  json.node_name log.node.name
  json.node_ip log.node.url
  json.ip log.ip
  json.is_domestic log.is_domestic
  json.created_at format_api_time(log.created_at)
end
json.partial! "api/shared/page", items: @logs

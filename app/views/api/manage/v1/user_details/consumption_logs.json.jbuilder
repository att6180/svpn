json.consump_coins @consume_coins
json.filter_consume_coins @filter_consume_coins
json.logs @logs do |log|
  json.id log.id
  json.user_id log.user_id
  json.username log.user.username
  json.node_type log.node_type_name
  json.node_name log.node_name
  json.node_url log.node_url
  json.coins log.coins
  json.created_at format_api_time(log.created_at)
end
json.partial! "api/shared/page", items: @logs

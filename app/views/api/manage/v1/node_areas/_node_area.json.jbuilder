json.id node_area.id
json.name node_area.name
json.sequence node_area.sequence
json.abbr node_area.abbr
json.created_at format_api_time(node_area.created_at)

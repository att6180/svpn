json.node_areas @node_areas do |node_area|
  json.partial! "api/manage/v1/node_areas/node_area", node_area: node_area
end
json.partial! "api/shared/page", items: @node_areas

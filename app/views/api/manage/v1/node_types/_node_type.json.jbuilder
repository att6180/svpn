json.id node_type.id
json.name node_type.name
json.level node_type.level
json.user_group_id node_type.user_group_id
json.user_group_name node_type.user_group.name
json.expense_coins node_type.expense_coins
json.can_be_monthly node_type.can_be_monthly
json.times_for_monthly node_type.times_for_monthly
json.limit_speed_up node_type.limit_speed_up
json.limit_speed_down node_type.limit_speed_down
json.description node_type.description
json.is_enabled node_type.is_enabled
json.created_at format_api_time(node_type.created_at)

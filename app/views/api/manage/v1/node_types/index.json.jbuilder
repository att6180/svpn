json.node_types @node_types do |node_type|
  json.partial! "api/manage/v1/node_types/node_type", node_type: node_type
end
json.partial! "api/shared/page", items: @node_types

json.logs @logs do |log|
  json.stat_date @date
  json.total_users @total_users
  if @date.today?
    json.stat_hour log[:h]
    json.users_count log[:users_count]
  else
    json.stat_hour log.stat_hour
    json.users_count log.users_count
  end
end
json.prev_logs @prev_logs do |log|
  json.stat_date log.stat_date
  json.prev_total_users @prev_total_users
  json.stat_hour log.stat_hour
  json.users_count log.users_count
end

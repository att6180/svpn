json.logs logs do |log|
  json.user_id log.user_id
  json.username log.user.username
end
json.partial! "api/shared/page", items: logs

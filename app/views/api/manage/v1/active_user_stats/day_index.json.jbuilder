json.logs @logs do |log|
  json.stat_date log.stat_date
  json.users_count log.total_users_count
  if @total_logs.present?
    total = @total_logs[log.stat_date.strftime("%Y%m%d")].total_users_count
    json.users_percent calc_percent(log.total_users_count, total)
  else
    json.users_percent 100
  end
end
json.partial! "api/shared/page", items: @logs

json.logs @logs do |log|
    json.stat_year log.stat_year
    json.stat_month log.stat_month
    json.users_count log.users_count
    period = JSON.parse(log.period)
    if period.present?
        json.d1 period["d_1"]
        json.d2_5 period["d2_5"]
        json.d6_10 period["d6_10"]
        json.d11_15 period["d11_15"]
        json.d16_20 period["d16_20"]
        json.d21_25 period["d21_25"]
        json.d26_31 period["d26_31"]
    end
end
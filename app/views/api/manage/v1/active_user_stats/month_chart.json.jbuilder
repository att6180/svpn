json.logs @logs do |log|
  json.stat_year log.stat_year
  json.stat_month log.stat_month
  json.users_count log.total_users_count
end

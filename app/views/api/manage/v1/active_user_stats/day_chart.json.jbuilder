json.logs @logs do |log|
  json.stat_date log.stat_date
  json.users_count log.total_users_count
end

json.logs @logs do |log|
  json.stat_date log[:d]
  json.stat_hour log[:h]
  json.showed_count log[:showed_count]
end
json.prev_logs @prev_logs do |log|
  json.stat_date log[:d]
  json.stat_hour log[:h]
  json.showed_count log[:showed_count]
end

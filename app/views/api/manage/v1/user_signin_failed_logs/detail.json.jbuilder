json.logs @logs do |log|
  json.fail_type log.fail_type
  json.user_id log.user_id
  json.username log.user.username
  json.device_uuid log.device_uuid
  json.ip log.ip
  json.created_at format_api_time(log.created_at)
end
json.partial! "api/shared/page", items: @logs

json.logs logs do |log|
  json.stat_date log[:stat_date]
  json.not_exist_count log[:type0]
  json.wrong_password_count log[:type1]
  json.failed_connection_count log[:type2]
  json.crash_count log[:type3]
end

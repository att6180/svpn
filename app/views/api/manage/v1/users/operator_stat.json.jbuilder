json.items @items do |item|
  json.operator item.operator
  json.total item.total
  json.percent calc_percent(item.total, @total)
end

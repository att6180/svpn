json.logs @logs do |log|
  json.stat_date log.stat_date
  json.active_users_count log.active_users_count
  json.checkin_users_count log.checkin_users_count
end

json.items @items do |item|
  json.device_model item.device_model
  json.platform item.platform
  json.total item.total
  json.percent calc_percent(item.total, @total)
end
json.platforms @platforms do |platform|
  json.platform platform.first
  json.total platform.last.to_i
  json.percent calc_percent(platform.last.to_i, @platform_total)
end

json.users users do |user|
  json.id user.id
  json.username user.username
  json.uuid user.create_device_uuid
  json.is_enabled user.is_enabled?
  json.total_payment_amount user.total_payment_amount
  json.current_coins user.current_coins
  json.total_coins user.total_coins
  json.total_used_coins user.total_used_coins
  json.group_name user.user_group.name
  json.group_id user.user_group.id
  json.last_app_launched_at format_api_time(user.current_signin_at)
  json.last_app_launched_version user.last_app_signin_version_info
  json.created_at format_api_time(user.created_at)
  json.created_version user.created_version_info
end

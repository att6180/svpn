json.users users do |user|
  json.id user.id
  json.username user.username
  json.last_signin_at user.last_signin_at.present? ? format_api_time(user.last_signin_at) : nil
  json.ip user.current_signin_ip
  ip_region = Ipnet.find_by_ip(user.current_signin_ip)
  json.ip_country ip_region.present? ? ip_region[:country] : ''
  json.ip_province ip_region.present? ? ip_region[:province] : ''
  json.ip_city ip_region.present? ? ip_region[:city] : ''
end
json.partial! "api/shared/page", items: users

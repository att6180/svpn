json.failure_usernames @failure_usernames
json.succeed_users @users do |user|
  json.username user.username
  json.password user.password
end

# json.items @items do |item|
#   json.share_type I18n.t("user_share_log.#{item.share_type}")
#   json.share_type_id UserShareStat.share_types[item.share_type]
#   json.total_users item.total_users
#   json.total_times item.total_times
#   json.per_average_share calc_per_average(item.total_times, item.total_users)
# end

if @stat_type == 1
  json.logs @logs do |log|
    json.platform_name log.platform_name
    json.text_count log.total_text_count
    json.copy_link_count log.total_copy_link_count
    json.image_count log.total_image_count
  end
elsif @stat_type == 2
  json.logs @logs do |log|
    json.platform_name log.platform_name
    json.weixin_text_count log.total_weixin_text_count
    json.friend_circle_text_count log.total_friend_circle_text_count
    json.qq_text_count log.total_qq_text_count
    json.weibo_text_count log.total_weibo_text_count
  end
else
  json.logs @logs do |log|
    json.platform_name log.platform_name
    json.weixin_image_count log.total_weixin_image_count
    json.friend_circle_image_count log.total_friend_circle_image_count
    json.qq_image_count log.total_qq_image_count
    json.weibo_image_count log.total_weibo_image_count
  end
end

json.items @items do |item|
  json.is_enabled item.is_enabled
  json.total item.total
  json.percent calc_percent(item.total, @total)
end

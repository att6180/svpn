json.users_total @users_total
json.foreign_users_total @foreign_users_total
json.domestic_users_total @domestic_users_total

json.foreign_users @foreign_users do |user|
  json.country user.country
  json.total user.total
end

json.domestic_users @domestic_users do |user|
  json.country user.province
  json.total user.total
end

json.logs @logs do |log|
  json.user_id log.user_id
  json.username log.user.username
  json.share_count log.share_count
end
json.partial! "api/shared/page", items: @logs


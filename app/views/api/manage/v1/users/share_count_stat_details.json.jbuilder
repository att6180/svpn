json.logs @logs do |log|
  json.id log.id
  json.user_id log.user_id
  json.username log.user.username
  json.app_channel log.app_channel
  json.app_version log.app_version
  json.app_version_number log.app_version_number
  json.created_at format_api_time(log.created_at)
end
json.partial! "api/shared/page", items: @logs

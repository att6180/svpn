json.logs @users do |user|
  log = @status_logs[user.id]
  if log.present?
    json.id log.id
    json.username user.username
    json.user_id user.id
    json.admin_id log.admin_id
    json.admin_username log.admin_username
    json.created_at format_api_time(log.created_at)
  end
end
json.partial! "api/shared/page", items: @users

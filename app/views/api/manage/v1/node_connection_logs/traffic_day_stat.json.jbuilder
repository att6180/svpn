json.logs @logs do |log|
    json.stat_date log.stat_date
    period = JSON.parse(log.traffic_period)
    if period.present?
        json.t10m period["t10m"]
        json.t11_20m period["t11_20m"]
        json.t21_50m period["t21_50m"]
        json.t51_100m period["t51_100m"]
        json.t101_200m period["t101_200m"]
        json.t201_500m period["t201_500m"]
        json.t501_1g period["t501_1g"]
        json.t1g_2g period["t1_2g"]
        json.t2g_5g period["t2_5g"]
        json.t5g_10g period["t5_10g"]
        json.t10g period["t10g"]
    end
    json.users_count log.users_count
end
json.logs @logs.result do |log|
  json.user_id log['user_id']
  json.username @users[log['user_id']].username
end
json.partial! "api/shared/page", items: @logs

json.logs @logs do |log|
  json.stat_date @date
  json.total_price @total_price
  if @date.today?
    json.stat_hour log[:h]
    json.total_recharge_amount log[:total_price]
    json.recharge_users_count log[:users_count]
  else
    json.stat_hour log.stat_hour
    json.total_recharge_amount log.total_recharge_amount
    json.recharge_users_count log.recharge_users_count
  end
end
json.prev_logs @prev_logs do |log|
  json.stat_date log.stat_date
  json.prev_total_price @prev_total_price
  json.stat_hour log.stat_hour
  json.total_recharge_amount log.total_recharge_amount
  json.recharge_users_count log.recharge_users_count
end

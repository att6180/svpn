json.logs logs do |log|
  json.id log.id
  json.order_number log.order_number
  json.username log.user.username
  json.user_id log.user_id
  json.plan_name log.plan_name
  json.price log.price
  json.coins log.coins
  json.payment_method I18n.t("transaction_log.payment_methods.#{log.payment_method}")
  json.is_completed log.status == 'paid' ? true : false
  json.processed_at format_api_time(log.processed_at)
  json.created_at format_api_time(log.created_at)
end
json.partial! "api/shared/page", items: logs

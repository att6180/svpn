json.logs @logs do |log|
  if @has_month
    json.stat_date log.stat_date.to_s
  else
    json.stat_year log.year
    json.stat_month log.month
  end
  json.total_recharge_amount log.total_recharge_amount
  json.total_consume_coins log.consume_coins
  json.recharge_users_count log.recharge_users_count
  json.renew_users_count log.renew_users_count
  json.renew_new_users_count log.renew_new_users_count
  json.renew_amount log.renew_amount
  json.renew_new_amount log.renew_new_amount
end

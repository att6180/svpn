json.logs @logs do |log|
  json.id log.id
  json.order_number log.order_number
  json.user_id log.user_id
  json.username log.user.username
  json.plan_name log.plan&.name
  json.price log.price
  json.created_at format_api_time(log.created_at)
end
json.partial! "api/shared/page", items: @logs

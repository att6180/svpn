json.logs @logs do |log|
  json.id log.id
  json.user_id log.user_id
  json.username log.user.username
  json.node_type log.node_type_name
  json.coins log.coins
  json.created_at format_api_time(log.created_at)
end
json.partial! "api/shared/page", items: @logs

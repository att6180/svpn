json.logs @logs do |log|
  json.user_id log.user_id
  json.username log.user.username
  json.amount log.amount.to_f
end
json.partial! "api/shared/page", items: @logs

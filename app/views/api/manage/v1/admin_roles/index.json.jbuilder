json.roles @roles do |role|
  json.partial! 'api/manage/v1/admin_roles/role', role: role
end
json.partial! "api/shared/page", items: @roles

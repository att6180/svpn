json.id log.id
json.user_id log.user_id
json.username log.user.username
json.operation_type I18n.t("user_status_log.#{log.operation_type}")
json.operation_type_value UserAccountStatusLog.operation_types[log.operation_type]
json.admin_id log.admin.id
json.admin_name log.admin.username
json.description log.description
json.created_at format_api_time(log.created_at)

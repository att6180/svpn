json.logs @logs do |log|
  json.partial! "api/manage/v1/user_status_logs/log", log: log
end
json.partial! "api/shared/page", items: @logs

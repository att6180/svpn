json.items @items do |item|
  json.partial! 'api/manage/v1/feedback_shortcut_replies/reply', reply: item
end

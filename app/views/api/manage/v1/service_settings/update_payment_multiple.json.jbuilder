json.settings do
  @settings.map { |key, value| json.set! key, value }
end

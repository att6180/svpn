json.items @items do |item|
  json.partial! "api/manage/v1/plan_client_tabs/item", item: item
end
json.partial! "api/shared/page", items: @items

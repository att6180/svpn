json.logs @logs do |log|
  json.stat_date log.stat_date
  json.users_count log.total_users_count
  json.first_month_users_count log.total_first_month_users_count
  json.first_month_users_rate UserRetentionRateStat.retention_rate(log.total_first_month_users_count, log.total_users_count)
  json.second_month_users_count log.total_second_month_users_count
  json.second_month_users_rate UserRetentionRateStat.retention_rate(log.total_second_month_users_count, log.total_users_count)
  json.third_month_users_count log.total_third_month_users_count
  json.third_month_users_rate UserRetentionRateStat.retention_rate(log.total_third_month_users_count, log.total_users_count)
  json.fourth_month_users_count log.total_fourth_month_users_count
  json.fourth_month_users_rate UserRetentionRateStat.retention_rate(log.total_fourth_month_users_count, log.total_users_count)
end

json.node_diversions @node_diversions do |node_diversion|
  json.partial! "api/manage/v1/node_diversions/node_diversion", node_diversion: node_diversion
end
json.partial! "api/shared/page", items: @node_diversions

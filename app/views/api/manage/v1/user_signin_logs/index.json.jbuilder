json.logs @logs do |log|
  json.id log.id
  json.user_id log.user_id
  json.username log.user.present? ? log.user.username : nil
  json.ip log.ip
  json.ip_country log.ip_country
  json.ip_province log.ip_province
  json.ip_city log.ip_city
  json.created_at format_api_time(log.created_at)
end
json.partial! "api/shared/page", items: @logs

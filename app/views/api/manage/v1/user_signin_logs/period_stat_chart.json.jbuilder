json.logs @logs do |log|
  json.period log[:period]
  json.users_count log[:users_count]
  json.times_count log[:times_count]
  json.average_times log[:average_times]
end

json.logs @logs do |log|
  json.stat_date log.stat_date
  data = JSON.parse(log.period_data)
  json.data data do |d|
    json.period d["period"]
    json.users_count d["users_count"]
    json.times_count d["times_count"]
    json.average calc_per_average(d["times_count"], d["users_count"])
  end
end
json.partial! "api/shared/page", items: @logs

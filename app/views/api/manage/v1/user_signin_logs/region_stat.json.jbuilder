json.logs @logs do |log|
  if @stat_type == 'country'
    json.country log.ip_country
  elsif @stat_type == 'province'
    json.province log.ip_province
  else
    json.city log.ip_city
  end
  json.users_count log.users_count
  json.times_count log.times_count
  json.average_times calc_per_average(log.times_count, log.users_count)
  json.users_percent calc_percent(log.users_count, @total_users_count)
end
json.partial! "api/shared/page", items: @logs

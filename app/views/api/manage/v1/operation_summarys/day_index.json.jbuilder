json.logs @logs do |log|
  json.stat_date log.stat_date
  json.total_recharge_amount log.t_total_recharge_amount
  json.new_users_count log.t_new_users_count
  json.active_users_count log.t_active_users_count
  json.paid_users_count log.t_paid_users_count
  json.payment_rate calc_percent(log.t_paid_users_count, log.t_new_users_count)
  json.paid_total_price log.t_paid_total_price
  json.seven_days_users_count log.t_seven_days_users_count
  json.seven_days_users_count_percent calc_percent(log.t_seven_days_users_count, log.t_new_users_count)
  json.seven_days_total_price log.t_seven_days_total_price
  json.thirty_days_users_count log.t_thirty_days_users_count
  json.thirty_days_users_count_percent calc_percent(log.t_thirty_days_users_count, log.t_new_users_count)
  json.thirty_days_total_price log.t_thirty_days_total_price
  json.second_day log.t_second_day
  json.second_day_rate Log::OperationSummaryDayStat.retention_rate(log.t_second_day, log.t_new_users_count)
  json.third_day log.t_third_day
  json.third_day_rate Log::OperationSummaryDayStat.retention_rate(log.t_third_day, log.t_new_users_count)
  json.seventh_day log.t_seventh_day
  json.seventh_day_rate Log::OperationSummaryDayStat.retention_rate(log.t_seventh_day, log.t_new_users_count)
  json.fifteen_day log.t_fifteen_day
  json.fifteen_day_rate Log::OperationSummaryDayStat.retention_rate(log.t_fifteen_day, log.t_new_users_count)
  json.thirtieth_day log.t_thirtieth_day
  json.thirtieth_day_rate Log::OperationSummaryDayStat.retention_rate(log.t_thirtieth_day, log.t_new_users_count)

  if @total_logs.present?
    date = log.stat_date.strftime("%Y%m%d")
    total_recharge_amount = @total_logs[date].t_total_recharge_amount
    json.total_recharge_amount_percent calc_percent(log.t_total_recharge_amount, total_recharge_amount)

    new_users_count = @total_logs[date].t_new_users_count
    json.new_users_count_percent calc_percent(log.t_new_users_count, new_users_count)

    active_users_count = @total_logs[date].t_active_users_count
    json.active_users_count_percent calc_percent(log.t_active_users_count, active_users_count)

    paid_total_price = @total_logs[date].t_paid_total_price
    json.paid_total_price_percent calc_percent(log.t_paid_total_price, paid_total_price)

    json.seven_days_total_price_percent calc_percent(log.t_seven_days_total_price, @total_logs[date].t_seven_days_total_price)
    json.thirty_days_total_price_percent calc_percent(log.t_thirty_days_total_price, @total_logs[date].t_thirty_days_total_price)

  else
    json.total_recharge_amount_percent 100
    json.new_users_count_percent 100
    json.active_users_count_percent 100
    json.paid_total_price_percent 100
    json.seven_days_total_price_percent nil
    json.thirty_days_total_price_percent nil
  end
end
json.partial! "api/shared/page", items: @logs

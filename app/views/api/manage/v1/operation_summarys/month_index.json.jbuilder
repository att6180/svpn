json.logs @logs do |log|
  json.stat_year log.stat_year
  json.stat_month log.stat_month
  json.total_recharge_amount log.t_total_recharge_amount
  json.new_users_count log.t_new_users_count
  json.active_users_count log.t_active_users_count

  if @total_logs.present?
    date = "#{log.stat_year}#{log.stat_month}"
    total_recharge_amount = @total_logs[date].t_total_recharge_amount
    json.total_recharge_amount_percent calc_percent(log.t_total_recharge_amount, total_recharge_amount)
    new_users_count = @total_logs[date].t_new_users_count
    json.new_users_count_percent calc_percent(log.t_new_users_count, new_users_count)
    active_users_count = @total_logs[date].t_active_users_count
    json.active_users_count_percent calc_percent(log.t_active_users_count, active_users_count)
  else
    json.total_recharge_amount_percent 100
    json.new_users_count_percent 100
    json.active_users_count_percent 100
  end
end
json.partial! "api/shared/page", items: @logs

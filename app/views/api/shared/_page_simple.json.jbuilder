if items.present?
  if defined?(items.current_page)
    json.current_page items.current_page
    json.total_pages items.total_pages
  elsif items.is_a?(Array)
    json.current_page 1
    json.total_pages 1
  end
else
  json.current_page 1
  json.total_pages 1
end

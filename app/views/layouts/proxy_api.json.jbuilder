json.success @success
json.messages @messages if !@success
json.merge! JSON.parse(yield) if @success

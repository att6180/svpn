class UpdateUserSigninInfoJob < ActiveJob::Base

  def perform(params)
    SessionService.update_signin_info(params)
  end

end

class UserConnectionSecondYearStatJob < ActiveJob::Base

  def perform(*args)
    # 用户访问日志二级年统计
    UserConnectionSecondYearStat.stat
  end

end

class ResetUserNodeTypeJob < ActiveJob::Base

  def perform(*args)
    # 重置所有用户的服务器类型为按次，使用次数为0
    UserNodeType.update_all(status: :times, used_count: 0)
  end

end

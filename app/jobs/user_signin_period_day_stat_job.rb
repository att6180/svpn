class UserSigninPeriodDayStatJob < ActiveJob::Base

  def perform(*args)
    # 统计每天的用户登录时间段信息
    UserSigninPeriodDayStat.stat
  end

end

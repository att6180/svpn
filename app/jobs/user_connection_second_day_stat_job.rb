class UserConnectionSecondDayStatJob < ActiveJob::Base

  def perform(*args)
    # 用户访问日志二级日统计
    UserConnectionSecondDayStat.stat
  end

end

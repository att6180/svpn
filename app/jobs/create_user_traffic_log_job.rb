class CreateUserTrafficLogJob < ActiveJob::Base

  def perform(user_id, used_bytes, stat_date, app_channel, app_version)
    # 根据userid跟日期，渠道版本判断是否存在记录
    # 不存在创建
    # 存在累加流量
    user_traffics = Log::UserTrafficLog.find_or_initialize_by(
      user_id: user_id,
      stat_date: stat_date,
      app_channel: app_channel,
      app_version: app_version
    )
    user_traffics.used_bytes += used_bytes
    user_traffics.save
  end
end

class UserConnectionSecondMonthStatJob < ActiveJob::Base

  def perform(*args)
    # 用户访问日志二级月统计
    UserConnectionSecondMonthStat.stat
  end

end

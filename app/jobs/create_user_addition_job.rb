class CreateUserAdditionJob < ActiveJob::Base

  def perform(user_id, inspect_expiration_time)
    UserAddition.create(
      user_id: user_id,
      last_inspect_expiration_at: inspect_expiration_time
    )
  end

end

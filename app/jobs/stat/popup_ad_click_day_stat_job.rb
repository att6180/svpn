module Stat
  class PopupAdClickDayStatJob < ActiveJob::Base

    def perform(*args)
      # 统计昨天的弹出广告点击次数
      Log::PopupAdClickDayStat.stat
    end

  end
end

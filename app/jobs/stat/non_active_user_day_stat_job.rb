module Stat
  class NonActiveUserDayStatJob < ActiveJob::Base

    def perform(*args)
      # 统计每天的非活跃用户数据
      Log::NonActiveUserDayStat.stat
    end

  end
end

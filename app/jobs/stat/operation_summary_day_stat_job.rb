module Stat
  class OperationSummaryDayStatJob < ActiveJob::Base

    def perform(*args)
      # 统计前一天或指定日期运营汇总信息
      Log::OperationSummaryDayStat.stat
    end
  end
end

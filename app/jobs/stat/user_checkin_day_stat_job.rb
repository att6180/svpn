module Stat
  class UserCheckinDayStatJob < ActiveJob::Base
    queue_as :stat_worker

    def perform(*args)
      # 统计每天的签到用户数据
      Log::UserCheckinDayStat.stat
    end
  end
end

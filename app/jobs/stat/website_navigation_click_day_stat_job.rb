module Stat
  class WebsiteNavigationClickDayStatJob < ActiveJob::Base

    def perform(*args)
      # 统计昨天各个导航网址点击次数
      Log::WebsiteNavigationClickDayStat.stat
    end

  end
end

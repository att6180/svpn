module Stat
  class SidekiqRetryStatJob < ActiveJob::Base

    def perform(*args)
      SidekiqNotify.notify
    end
  end
end
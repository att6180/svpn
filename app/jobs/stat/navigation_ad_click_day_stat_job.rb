module Stat
  class NavigationAdClickDayStatJob < ActiveJob::Base

    def perform(*args)
      # 统计昨天各个导航广告点击次数
      Log::NavigationAdClickDayStat.stat
    end

  end
end

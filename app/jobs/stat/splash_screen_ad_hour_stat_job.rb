module Stat
  class SplashScreenAdHourStatJob < ActiveJob::Base

    def perform(*args)
      # 统计昨天每小时的开屏广告展示次数
      Log::SplashScreenAdHourStat.stat
    end

  end
end

module Stat
  class SplashScreenAdClickHourStatJob < ActiveJob::Base

    def perform(*args)
      # 统计昨天每小时的开屏广告点击次数
      Log::SplashScreenAdClickHourStat.stat
    end

  end
end

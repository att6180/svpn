module Stat
  class UserWeekRetentionRateStatJob < ActiveJob::Base

    def perform(*args)
      # 统计每周留存
      Log::UserWeekRetentionRateStat.stat
    end
  end
end

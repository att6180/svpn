module Stat
  class PopupAdDayStatJob < ActiveJob::Base

    def perform(*args)
      # 统计昨天的弹出广告展示次数
      Log::PopupAdDayStat.stat
    end

  end
end

module Stat
  class OperationSummaryMonthStatJob < ActiveJob::Base

    def perform(*args)
      # 统计上个月的运营汇总信息
      Log::OperationSummaryMonthStat.stat
    end
  end
end

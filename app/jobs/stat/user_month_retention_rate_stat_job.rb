module Stat
  class UserMonthRetentionRateStatJob < ActiveJob::Base

    def perform(*args)
      # 统计每月留存
      Log::UserMonthRetentionRateStat.stat
    end
  end
end

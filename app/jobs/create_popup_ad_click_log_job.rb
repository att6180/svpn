class CreatePopupAdClickLogJob < ActiveJob::Base

  def perform(params)
    Log::PopupAdClickLog.create(
      uuid: params[:uuid],
      app_channel: params[:app_channel],
      app_version: params[:app_version],
      app_version_number: params[:app_version_number],
      created_date: params[:created_date],
      created_at: params[:created_at],
      updated_at: params[:created_at]
    )
  end

end

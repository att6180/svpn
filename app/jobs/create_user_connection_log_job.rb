class CreateUserConnectionLogJob < ActiveJob::Base

  def perform(params)
    node = Node.find_by(id: params[:node_id])
    begin
      ip_region = Ipnet.find_by_ip(params[:client_ip])
    rescue Exception => e
        message = "#{e.class.name}: #{e.message}: ip=>#{params[:client_ip]}"
        Rails.logger.error("connection log error: #{message}")
    end
    if node.present? && ip_region.present?
      node_type = node.node_type
      node_region = node.node_region
      NodeConnectionLog.create(
        user_id: params[:user_id],
        node_type_id: node_type.id,
        node_region_id: node_region.id,
        node_id: node.id,
        node_type_name: node_type.name,
        node_region_name: node_region.name,
        node_name: node.name,
        wait_time: params[:wait_time],
        domain: params[:domain],
        ip: params[:client_ip],
        ip_country: ip_region[:country],
        ip_province: ip_region[:province],
        ip_city: ip_region[:city],
        status: NodeConnectionLog.statuses.keys[params[:success].to_i],
        user_registered_at: params[:user_registered_at],
        app_channel: params[:app_channel],
        app_version: params[:app_version]
      )
    end
  end

end

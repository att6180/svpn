class CreateNodeConnectionServerLogJob < ActiveJob::Base

  def perform(user_id, user_created_at, current_time, app_channel, app_version, node_id, ip, is_domestic)
    # 记录每个用户每天的连接日志(用于留存统计)
    # 利用数据库唯一索引省掉一次查询来尝试写入数据
    NodeConnectionUserDayLog.create_ignore_by_sql(
      user_id,
      current_time.to_date,
      user_created_at,
      app_channel,
      app_version,
      current_time,
      current_time
    )
    # 后端记录用户连接日志
    NodeConnectionServerLog.create(
      user_id: user_id,
      node_id: node_id,
      ip: ip,
      is_domestic: is_domestic,
      created_at: current_time,
      updated_at: current_time
    )
  end

end

class DynamicServerFailedStatJob < ActiveJob::Base

  def perform(*args)
    # 统计动态服务器失败信息
    DynamicServer.failed_stat
  end

end

module Trend
  class OnlineUsersJob < ActiveJob::Base

    def perform(*args)
      # 向态势图提交注册用户信息
      TrendService.online_users
    end

  end
end

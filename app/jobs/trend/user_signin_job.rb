module Trend
  class UserSigninJob < ActiveJob::Base
    queue_as :bigdata_worker

    def perform(params)
      # 向态势图提交注册用户信息
      TrendService.user_signin(params)
    end

  end
end

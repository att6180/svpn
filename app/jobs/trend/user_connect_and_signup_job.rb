module Trend
  class UserConnectAndSignupJob < ActiveJob::Base
    queue_as :bigdata_worker

    def perform(params, type)
      # 向态势图提交注册和连接用户信息
      TrendService.connect_and_signup(params, type)
    end

  end
end

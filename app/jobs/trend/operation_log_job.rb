module Trend
  class OperationLogJob < ActiveJob::Base

    def perform(params)
      # 向态势图提交用户操作日志
      TrendService.operation_log(params)
    end

  end
end

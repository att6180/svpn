class RechargeCompensationJob < ActiveJob::Base

  def perform(params) 
    user = User.find(params[:user_id])
    promo_user = User.find(params[:promo_user_id])
    if !(CompensationActivityLog.where(user_id: params[:user_id]).size > 0)
      ActiveRecord::Base.transaction do
        # 为用户增加高级套餐的过期时间15天
        user_node_type = UserNodeType.find_or_create_by!(user_id: params[:user_id], node_type_id: 2)
        # 如果当前服务类型未到期，则基于服务类型过期时间累加，否则基于当前时间累加
        base_time = user_node_type.expired_at > Time.now ? user_node_type.expired_at : Time.now
        bought_time = base_time + 15.days
        # 累加过期时间
        user_node_type.update!(expired_at: bought_time)
        # 为用户新增活动日志
        CompensationActivityLog.create!(
          id: Utils::Gen.generate_uuid,
          user_id: params[:user_id],
          promo_user_id: params[:promo_user_id],
          user_signup_at: user.created_at,
          node_type_id: 2,
          user_recharge_at: params[:processed_at],
          reward_days: 15
        )
      end
    end
    if !(CompensationActivityLog.where(user_id: params[:promo_user_id]).size > 0)
      ActiveRecord::Base.transaction do
        # 为用户增加高级套餐的过期时间15天
        user_node_type = UserNodeType.find_or_create_by!(user_id: params[:promo_user_id], node_type_id: 2)
        # 如果当前服务类型未到期，则基于服务类型过期时间累加，否则基于当前时间累加
        base_time = user_node_type.expired_at > Time.now ? user_node_type.expired_at : Time.now
        bought_time = base_time + 15.days
        # 累加过期时间
        user_node_type.update!(expired_at: bought_time)
        # 为用户新增活动日志
        CompensationActivityLog.create!(
          id: Utils::Gen.generate_uuid,
          user_id: params[:promo_user_id],
          user_signup_at: promo_user.created_at,
          node_type_id: 2,
          reward_days: 15
        )
      end
    end
  end
end
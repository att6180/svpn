class CreateAuthenticationFailureLogJob < ActiveJob::Base

  def perform(params)
    Mongo::ProxyAuthenticationFailureLog.create(
      user_id: params[:user_id],
      node_id: params[:node_id],
      token: params[:token]
    )
  end
end

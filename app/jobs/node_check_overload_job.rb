class NodeCheckOverloadJob < ActiveJob::Base

  def perform(*args)
    # 禁用连接数超过80%的节点
    Node.check_overload!
  end

end

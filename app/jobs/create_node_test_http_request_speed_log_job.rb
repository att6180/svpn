class CreateNodeTestHttpRequestSpeedLogJob < ActiveJob::Base

  def perform(data, app_channel, app_version, user, ip, created_at)
    return if !data.is_a? Array
    ip_region = Ipnet.find_by_ip(ip)
    Mongo::NodeTestHttpRequestSpeedLog.create(
      data.map do |ps|
        {
          user_id: user.id,
          user_name: user.username,
          client_ip: ip,
          ip_country: ip_region[:country],
          ip_province: ip_region[:province],
          ip_city: ip_region[:city],
          ip_area: ip_region[:area],
          request_url: ps['request_url'],
          max_rt: ps['max_rt'],
          min_rt: ps['min_rt'],
          avg_rt: ps['avg_rt'],
          failure_rate: ps['failure_rate'],
          app_channel: app_channel,
          app_version: app_version,
          created_at: created_at
        }
      end
    )
  end

end

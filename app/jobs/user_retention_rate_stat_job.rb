class UserRetentionRateStatJob < ActiveJob::Base

  def perform(*args)
    # 统计每天的用户留存数据
    UserRetentionRateStat.stat
  end

end

class TransactionMonthStatJob < ActiveJob::Base

  def perform(*args)
    # 统计昨天的充值信息
    TransactionMonthStat.stat
  end

end

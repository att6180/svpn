class TransactionStatJob < ActiveJob::Base

  def perform(*args)
    # 统计昨天的充值信息
    TransactionStat.stat
  end

end

class UserNoOperationStatJob < ActiveJob::Base

  def perform(*args)
    # 统计用户未操作数据
    UserNoOperationStat.stat
  end

end

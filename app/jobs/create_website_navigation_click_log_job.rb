class CreateWebsiteNavigationClickLogJob < ActiveJob::Base

  def perform(params)
    Log::WebsiteNavigationClickLog.create(
      user_id: params[:user_id],
      uuid: params[:uuid],
      website_id: params[:website_id],
      website_name: params[:website_name],
      nav_type: params[:nav_type],
      info_type: params[:info_type],
      is_domestic: params[:is_domestic],
      app_channel: params[:app_channel],
      app_version: params[:app_version],
      app_version_number: params[:app_version_number],
      created_at: params[:created_at],
      created_date: params[:created_date],
    )
  end
end

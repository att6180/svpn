class UserOperatorStatJob < ActiveJob::Base

  def perform(*args)
    # 统计用户的硬件信息
    UserOperatorStat.stat
  end

end

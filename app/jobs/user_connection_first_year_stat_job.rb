class UserConnectionFirstYearStatJob < ActiveJob::Base

  def perform(*args)
    # 用户访问日志一级年统计
    UserConnectionFirstYearStat.stat
  end

end

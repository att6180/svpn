class TransactionHourStatJob < ActiveJob::Base

  def perform(*args)
    # 统计昨天的小时充值信息
    TransactionHourStat.stat
  end

end

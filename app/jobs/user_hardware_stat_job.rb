class UserHardwareStatJob < ActiveJob::Base

  def perform(*args)
    # 统计用户的硬件信息
    UserHardwareStat.stat
  end

end

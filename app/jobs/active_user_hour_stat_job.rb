class ActiveUserHourStatJob < ActiveJob::Base

  def perform(*args)
    # 统计每天的活跃用户数据
    ActiveUserHourStat.stat
  end

end

class NodeConnectionCountDayStatJob < ActiveJob::Base

  def perform(*args)
    # 每天统计用户连接代理服务器的次数
    NodeConnectionCountDayStat.stat
  end

end

class UserSigninFailedStatJob < ActiveJob::Base

  def perform(*args)
    # 统计用户的登录失败信息
    UserSigninFailedStat.stat
  end

end

class CreateUserActiveLogJob < ActiveJob::Base

  def perform(params)
    SessionService.create_active_hour_log(params)
  end
end

class UserConnectionFirstDayStatJob < ActiveJob::Base

  def perform(*args)
    # 用户访问日志一级日统计
    UserConnectionFirstDayStat.stat
  end

end

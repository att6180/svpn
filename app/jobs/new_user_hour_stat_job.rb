class NewUserHourStatJob < ActiveJob::Base

  def perform(*args)
    # 统计每小时的新增用户数据
    NewUserHourStat.stat
  end

end

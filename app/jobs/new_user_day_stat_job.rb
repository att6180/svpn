class NewUserDayStatJob < ActiveJob::Base

  def perform(*args)
    # 统计每天的新增用户数据
    NewUserDayStat.stat
  end

end

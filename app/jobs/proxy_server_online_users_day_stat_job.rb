class ProxyServerOnlineUsersDayStatJob < ActiveJob::Base

  def perform(*args)
    # 统计在线用户历史记录
    ProxyServerOnlineUsersDayStat.stat
  end

end

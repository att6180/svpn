class ActiveUserPeriodMonthStatJob < ActiveJob::Base

  def perform(*args)
    # 统计每月日期区间的活跃用户数据
    Log::ActiveUserPeriodMonthStat.stat
  end

end

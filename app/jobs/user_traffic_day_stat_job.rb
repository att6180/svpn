class UserTrafficDayStatJob < ActiveJob::Base

  def perform(*args)
    Log::UserTrafficDayStat.stat
  end

end

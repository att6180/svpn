class CreateTransactionPaymentFailedLogJob < ActiveJob::Base

  def perform(order_number, user_id, platform, app_channel, app_version, created_at)
    TransactionPaymentFailedLog.create(
      order_number: order_number,
      user_id: user_id,
      platform: platform,
      app_channel: app_channel,
      app_version: app_version,
      created_at: created_at,
      updated_at: created_at
    )
  end

end

class UserConnectionFirstMonthStatJob < ActiveJob::Base

  def perform(*args)
    # 用户访问日志一级月统计
    UserConnectionFirstMonthStat.stat
  end

end

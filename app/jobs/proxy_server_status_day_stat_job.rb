class ProxyServerStatusDayStatJob < ActiveJob::Base

  def perform(*args)
    # 统计服务器状态信息历史记录
    ProxyServerStatusDayStat.stat
  end

end

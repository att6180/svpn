class NewUserMonthStatJob < ActiveJob::Base

  def perform(*args)
    # 统计每月的新增用户数据
    NewUserMonthStat.stat
  end

end

class TransactionTypeDayStatJob < ActiveJob::Base

  def perform(*args)
    # 统计昨天的充值类型信息
    TransactionTypeDayStat.stat
  end

end

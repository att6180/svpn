class CreateNodeSwitchLogJob < ActiveJob::Base

  def perform(node_id, status, node_type_name, ip, robot_ip, created_at)
    NodeSwitchLog.create(
      node_id: node_id,
      status: status,
      node_type_name: node_type_name,
      node_ip: ip,
      robot_ip: robot_ip,
      created_at: created_at,
      updated_at: created_at
    )
  end

end

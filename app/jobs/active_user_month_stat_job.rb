class ActiveUserMonthStatJob < ActiveJob::Base

  def perform(*args)
    # 统计每月的活跃用户数据
    ActiveUserMonthStat.stat
  end

end

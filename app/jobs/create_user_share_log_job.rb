class CreateUserShareLogJob < ActiveJob::Base

  def perform(params)
    UserShareLog.create(
      user_id: params[:user_id],
      share_type: params[:share_type],
      platform_name: params[:platform_name],
      app_channel: params[:app_channel],
      app_version: params[:app_version],
      app_version_number: params[:app_version_number]
    )
  end
end

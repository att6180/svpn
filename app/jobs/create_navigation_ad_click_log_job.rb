class CreateNavigationAdClickLogJob < ActiveJob::Base

  def perform(params)
    Log::NavigationAdClickLog.create(
      user_id: params[:user_id],
      uuid: params[:uuid],
      ad_id: params[:ad_id],
      ad_name: params[:ad_name],
      app_channel: params[:app_channel],
      app_version: params[:app_version],
      app_version_number: params[:app_version_number],
      created_at: params[:created_at],
      created_date: params[:created_date],
    )
  end
end

class UserAreaStatJob < ActiveJob::Base

  def perform(*args)
    # 统计用户地域信息
    UserAreaStat.stat
  end
end

class CreateUserChangeLogJob < ActiveJob::Base

  def perform(params)
    Log::UserChangeLog.create(
      user_id: params[:user_id],
      change_type: params[:change_type],
      uuid: params[:uuid],
      platform: params[:platform],
      ip: params[:ip],
      before: params[:before],
      after: params[:after],
      created_at: params[:created_at],
      updated_at: params[:created_at]
    )
  end

end

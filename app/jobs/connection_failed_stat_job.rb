class ConnectionFailedStatJob < ActiveJob::Base

  def perform(*args)
    # 统计用户的连接失败信息
    ConnectionFailedStat.stat
  end

end

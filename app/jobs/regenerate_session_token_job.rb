class RegenerateSessionTokenJob < ActiveJob::Base

  def perform(session_id, token)
    # 重新生成用户的session token
    UserService.regenerate_session_token(session_id, token)
  end

end

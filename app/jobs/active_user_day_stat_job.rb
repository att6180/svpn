class ActiveUserDayStatJob < ActiveJob::Base

  def perform(*args)
    # 统计每天的活跃用户数据
    ActiveUserDayStat.stat
  end

end

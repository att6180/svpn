class CreateUserOperationLogJob < ActiveJob::Base

  def perform(params, current_device_id)
    operations = params[:operations].split('|')
    log_list = []
    user_ids = []
    operations.each do |os|
      operation = os.split(',')
      user_ids << operation[1]
    end
    operations.each do |os|
      next if os.blank?
      operation = os.split(',')
      user_id = operation[1]
      ts = operation[2].to_i
      created_at = ts > 0 ? Utils.ts2t(ts) : Time.now
      if $redis.sismember(User::CACHE_KEY_IDS, user_id.to_i)
        log_list << {
          user_id: user_id,
          device_id: current_device_id,
          user_app_launch_log_id: params[:app_launch_id],
          user_signin_log_id: params[:signin_log_id],
          app_channel: params[:app_channel],
          app_version: params[:app_version],
          app_version_number: params[:app_version_number],
          interface_id: operation[0],
          created_at: created_at,
          updated_at: created_at
        }
      end
    end
    Mongo::UserOperationLog.create(log_list)
  end

end

class CreateNodeTestDownloadSpeedLogJob < ActiveJob::Base

  def perform(data, app_channel, app_version, user, ip, created_at)
    return if !data.is_a? Array

    ip_region = Ipnet.find_by_ip(ip)
    Mongo::NodeTestDownloadSpeedLog.create(
      data.map do |ds|
        {
          user_id: user.id,
          user_name: user.username,
          client_ip: ip,
          ip_country: ip_region[:country],
          ip_province: ip_region[:province],
          ip_city: ip_region[:city],
          ip_area: ip_region[:area],
          download_url: ds['download_url'],
          waste_time: ds['waste_time'],
          app_channel: app_channel,
          app_version: app_version,
          created_at: created_at
        }
      end
    )
  end

end

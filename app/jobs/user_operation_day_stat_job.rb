class UserOperationDayStatJob < ActiveJob::Base

  def perform(*args)
    # 统计前一天的用户的操作记录
    UserOperationDayStat.stat
  end

end

class UserStatusStatJob < ActiveJob::Base

  def perform(*args)
    # 统计用户的硬件信息
    UserStatusStat.stat
  end

end

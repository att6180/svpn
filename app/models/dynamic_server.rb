class DynamicServer < ApplicationRecord

  CACHE_KEY_ID = "dynamic_server_failed_count"

  scope :sorted, -> { order(priority: :asc) }
  scope :enabled, -> { where(is_enabled: true) }

  validates :url, :priority, uniqueness: true

  has_many :dynamic_server_connection_failed_logs

  def cache_failure_count
    $redis.hget(CACHE_KEY_ID, self.id).to_i
  end

  def self.increase_failed_count(id, count = 1)
    $redis.hincrby(CACHE_KEY_ID, id, count)
  end

  def self.failed_stat
    DynamicServer.enabled.each do |server|
      server.update(failed_count: server.dynamic_server_connection_failed_logs.count)
    end
  end

end

class ClientConnectionLog < ApplicationLogRecord
  belongs_to :user
  belongs_to :node
end

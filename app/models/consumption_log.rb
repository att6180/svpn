class ConsumptionLog < ApplicationLogRecord

  include Uuidable

  CACHE_KEY_CONSUME_COINS = "consumption_log_consume_coins_"

  belongs_to :user
  belongs_to :node_type

  scope :by_date, ->(d) { where(created_at: d.beginning_of_day..d.end_of_day) }
  scope :between_date, ->(start_at, end_at) { where(created_at: start_at.beginning_of_day..end_at.end_of_day ) }
  scope :by_year_and_month, -> (year, month) { where("YEAR(created_at) = ? AND MONTH(created_at) = ?", year, month) }
  scope :by_year, -> (year) { where("YEAR(created_at) = ?", year) }
  scope :by_channel_version, ->(channel, version) { where(app_channel: channel, app_version: version)}
  scope :by_coins, -> { where(is_regular_time: false) }

  after_commit :after_commit_trace, on: [:create]

  def self.consume_coins_today
    $redis.get("#{CACHE_KEY_CONSUME_COINS}#{DateUtils.time2ymd(Time.now)}").to_i
  end

  private

  def after_commit_trace
    # 累计今日消费钻石数
    if self.coins > 0
      cache_key = "#{CACHE_KEY_CONSUME_COINS}#{DateUtils.time2ymd(Time.now)}"
      $redis.incrby(cache_key, self.coins)
      $redis.expire(cache_key, DateUtils.remaining_time) if $redis.ttl(cache_key) == -1
    end
  end

end

module Uuidable
  extend ActiveSupport::Concern

  included do
    self.primary_key = :id
    before_create :generate_uuid
  end

  protected

  def generate_uuid
    self.id = UUIDTools::UUID.timestamp_create.to_s if self.id.blank?
  end

end

class ComsunnyTransactionLog < ApplicationLogRecord
  include Uuidable

  belongs_to :user

  def self.create_log!(user, comsunny)
    ComsunnyTransactionLog.create!(
      user_id: user.id,
      timestamp: comsunny.timestamp,
      channel_type: comsunny.channel_type,
      sub_channel_type: comsunny.sub_channel_type,
      transaction_type: comsunny.transaction_type,
      transaction_id: comsunny.transaction_id,
      transaction_fee: comsunny.transaction_fee,
      trade_success: comsunny.trade_success,
      message_detail: comsunny.message_detail.to_json,
      optional: comsunny.optional.to_json
    )
  end

end

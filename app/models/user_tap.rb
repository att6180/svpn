class UserTap < ApplicationRecord
  belongs_to :user
  belongs_to :node

  validates :user_id, uniqueness: true
end
class NodeIsp < ApplicationRecord

  CACHE_KEY = "node_isps"

  has_many :nodes

  validates :name, presence: true, uniqueness: true

  scope :enabled, -> { where(is_enabled: true) }
  scope :sorted_by_enabled, -> { order(is_enabled: :desc) }

  after_commit :after_commit_trace

  def self.cache_list
    Rails.cache.fetch(CACHE_KEY, expires_in: 30.days) do
      NodeIsp.enabled.to_a
    end
  end

  private

  def after_commit_trace
    Rails.cache.delete(CACHE_KEY)
  end

end

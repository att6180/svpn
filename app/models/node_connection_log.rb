class NodeConnectionLog < ApplicationLogRecord
  include Uuidable

  enum status: { error: 0, success: 1}
  # status: 0: error, 1: success
  # 此处不用枚举是因为批量插入不支持枚举

  belongs_to :user
  belongs_to :node

  scope :status_by_flag, ->(flag) { where(status: flag) }
  scope :between_date, ->(start_at, end_at) { where(created_at: start_at.beginning_of_day..end_at.end_of_day ) }

end

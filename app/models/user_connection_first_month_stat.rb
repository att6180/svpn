class UserConnectionFirstMonthStat < ApplicationLogRecord
  include Uuidable

  belongs_to :user

  scope :between_date, -> (start_at, end_at) {
    where(
      "stat_year >= ? AND stat_month >= ? AND stat_year <= ? && stat_month <= ?",
      start_at.year, start_at.month, end_at.year, end_at.month
    )
  }
  scope :select_and_group, -> {
    select("domain, COUNT(DISTINCT user_id) as users_count, SUM(visits_count) as total_visits_count").group(:domain)
  }

  # 统计前一月的去向IP
  def self.stat(date = nil)
    last_month = date || Time.now.last_month
    year = last_month.year
    month = last_month.month

    stats = []

    items = UserConnectionLog
      .select("user_id, top_level_domain, COUNT(*) as visits_count")
      .group(:user_id, :top_level_domain)
      .where("YEAR(created_at) = ? AND MONTH(created_at) = ?", year, month)

    if items.present?
      items.each do |item|
        stats << UserConnectionFirstMonthStat.new(
          id: Utils::Gen.generate_uuid,
          user_id: item.user_id,
          domain: item.top_level_domain,
          visits_count: item.visits_count,
          stat_year: year,
          stat_month: month
        )
      end
    end
    UserConnectionFirstMonthStat.bulk_insert(stats, use_provided_primary_key: true) if stats.present?
  end
end

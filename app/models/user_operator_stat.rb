class UserOperatorStat < ApplicationLogRecord
  include Uuidable

  scope :stat_result, -> { select("operator, SUM(quantity) AS total").group("operator").order("total DESC") }

  def self.stat
    UserOperatorStat.delete_all

    stats = []

    # 全部
    items = User.select("create_operator, count(*) as quantity")
      .group(:create_operator)
    items.each do |item|
      stats << UserOperatorStat.new(
        id: Utils::Gen.generate_uuid,
        operator: item.create_operator,
        quantity: item.quantity
      )
    end

    # 渠道
    items = User.select("create_app_channel, create_operator, count(*) as quantity")
      .group(:create_app_channel, :create_operator)
    items.each do |item|
      stats << UserOperatorStat.new(
        id: Utils::Gen.generate_uuid,
        app_channel: item.create_app_channel,
        operator: item.create_operator,
        quantity: item.quantity,
        filter_type: 1
      )
    end

    # 版本
    items = User.select("create_app_version, create_operator, count(*) as quantity")
      .group(:create_app_version, :create_operator)
    items.each do |item|
      stats << UserOperatorStat.new(
        id: Utils::Gen.generate_uuid,
        app_version: item.create_app_version,
        operator: item.create_operator,
        quantity: item.quantity,
        filter_type: 1
      )
    end

    # 渠道和版本
    items = User.select("create_app_version, create_app_channel, " \
      "create_operator, count(*) as quantity")
      .group( :create_app_version, :create_app_channel, :create_operator)
    items.each do |item|
      stats << UserOperatorStat.new(
        id: Utils::Gen.generate_uuid,
        app_version: item.create_app_version,
        app_channel: item.create_app_channel,
        operator: item.create_operator,
        quantity: item.quantity,
        filter_type: 2
      )
    end

    UserOperatorStat.bulk_insert(stats, use_provided_primary_key: true) if stats.present?
  end

end

class UserConnectionSecondDayStat < ApplicationLogRecord
  include Uuidable

  scope :between_date, -> (start_at, end_at) { where("stat_date >= ? AND stat_date <= ?", start_at, end_at) }

  # 根据用户访问日志一级日期表(user_connection_first_day_stats)统计
  def self.stat
    stat_date = Time.now.to_date.yesterday
    logs = UserConnectionSecondDayStat.eager_stat(stat_date, 1, 50)
    if logs.total_pages > 0
      (1..logs.total_pages).each do |index|
        logs = UserConnectionSecondDayStat.eager_stat(stat_date, index, 50)
        stats = []
        logs.each do |log|
          stat = UserConnectionSecondDayStat.new(
            id: Utils::Gen.generate_uuid,
            domain: log.domain,
            visit_users_count: log.users_count,
            visits_count: log.total_visits_count,
            stat_date: stat_date
          )
          stats << stat
        end
        UserConnectionSecondDayStat.bulk_insert(stats, use_provided_primary_key: true)
      end
    end
  end

  def self.eager_stat(stat_date, page, limit)
    UserConnectionFirstDayStat
      .select(
        "domain, " \
        "COUNT(user_id) as users_count, " \
        "SUM(visits_count) as total_visits_count"
      )
      .where(stat_date: stat_date)
      .group(:domain)
      .page(page).limit(limit)
  end

end

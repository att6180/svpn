class FeedbackShortcutReply < ApplicationRecord
  belongs_to :feedback_shortcut_reply_category
  validates :feedback_shortcut_reply_category_id, :content, presence: true
  after_commit :after_commit_trace, on: [:create, :update, :destroy]

  private

  def after_commit_trace
    Rails.cache.delete(FeedbackShortcutReplyCategory::CACHE_KEY)
  end
end

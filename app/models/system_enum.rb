class SystemEnum < ApplicationRecord

  TYPE_CACHE_KEY_PREFIX = "system_enums_type_"

  # 数据类型
  enum enum_type: {
    channel: 0,
    version: 1,
    platform: 2,
    channel_account: 3,
    package: 4
  }, _prefix: :type

  has_many :users

  validates :name, uniqueness: { scope: :enum_type }

  scope :sorted_by_id, -> { order(id: :asc) }

  after_commit :after_commit_trace, on: [:create, :update, :destroy]

  def self.enums_by_type(type)
    Rails.cache.fetch("#{TYPE_CACHE_KEY_PREFIX}#{type}", expires_in: 30.days) do
      SystemEnum.where(enum_type: type).order(created_at: :desc).to_a
    end
  end

  def self.enums_by_channel
    SystemEnum.enums_by_type("channel")
  end

  def self.enums_by_version
    SystemEnum.enums_by_type("version")
  end

  def self.enums_by_platform
    SystemEnum.enums_by_type("platform")
  end

  def self.enums_by_channel_account
    SystemEnum.enums_by_type("channel_account")
  end

  def self.enums_by_package
    SystemEnum.enums_by_type("package")
  end

  def self.get_name_by_id(id, type)
    list = SystemEnum.enums_by_type(type)
    item = list.select { |e| e.id == id }
    item.present? ? item.first.name : nil
  end

  private

  def after_commit_trace
    Rails.cache.delete("#{TYPE_CACHE_KEY_PREFIX}#{self.enum_type}")
  end

end

class NodeConnectionCountDayStat < ApplicationLogRecord
  include Uuidable

  belongs_to :user

  scope :between_date, ->(start_at, end_at) {
    select("user_id, SUM(users_count) as total_used_count")
      .where(stat_date: start_at..end_at)
      .where.not(user_id: nil)
      .group(:user_id)
  }

  TIMES_SCOPES = {
    "1-2": 1..2,
    "3-5": 3..5,
    "6-9": 6..9,
    "10-19": 10..19,
    "20-49": 20..49,
    "50": 50
  }

  def self.stat_used_count(sql)
    query = sanitize_sql([
      "SELECT COUNT(CASE WHEN total_used_count>=1 AND total_used_count<=2 THEN 1 ELSE NULL END) AS count_1_2, " \
      "COUNT(CASE WHEN total_used_count>=3 AND total_used_count<=5 THEN 1 ELSE NULL END) AS count_3_5, " \
      "COUNT(CASE WHEN total_used_count>=6 AND total_used_count<=9 THEN 1 ELSE NULL END) AS count_6_9, " \
      "COUNT(CASE WHEN total_used_count>=10 AND total_used_count<=19 THEN 1 ELSE NULL END) AS count_10_19, " \
      "COUNT(CASE WHEN total_used_count>=20 AND total_used_count<=49 THEN 1 ELSE NULL END) AS count_20_49, " \
      "COUNT(CASE WHEN total_used_count>=50 THEN 1 ELSE NULL END) AS count_50 " \
      "FROM (#{sql}) AS ncg;"])
    NodeConnectionCountDayStat.connection.exec_query(query)
  end

  # 根据次数段为结果加条件
  def self.by_times_scope(times_scope, items, page, limit)
    # 转换参数为integer型
    offset = (page.to_i - 1) * limit.to_i
    condition = TIMES_SCOPES[times_scope.to_sym]
    query_string = "SELECT user_id, total_used_count FROM (#{items.to_sql}) as m "
    total_query_string = "SELECT COUNT(*) as users_count FROM (#{items.to_sql}) as m "
    if condition == 50
      query_string += " WHERE total_used_count >= #{condition}"
      total_query_string += " WHERE total_used_count >= #{condition}"
    else
      query_string += " WHERE total_used_count BETWEEN #{condition.first} AND #{condition.last}"
      total_query_string += " WHERE total_used_count BETWEEN #{condition.first} AND #{condition.last}"
    end
    query_string += " LIMIT #{limit} OFFSET #{offset}"
    query = sanitize_sql([query_string])
    total_query = sanitize_sql([total_query_string])
    # TODO: 考虑Service Object的特性（没有固定形态，完全用来封装业务逻辑)
    # 这里可以写一个完全针对 NodeConnectionCountDayStat 类的带分页属性的逻辑，而不是试图使用一个通用的 PageCustomer
    # http://iempire.ru/2015/11/08/kaminari-custom-query/
    # https://wjp2013.github.io/rails/service-object/
    PageCustomer.new(NodeConnectionCountDayStat, query, total_query, page, limit)
  end

  # 统计昨天的服务器用户使用数
  def self.stat(date = nil)
    yesterday = date || Time.now.to_date.yesterday

    stats = []

    items = NodeConnectionLog
      .select("user_id, COUNT(*) as users_count")
      .group(:user_id)
      .where("DATE(created_at) = ?", yesterday)

    if items.present?
      items.each do |item|
        stats << NodeConnectionCountDayStat.new(
          id: Utils::Gen.generate_uuid,
          stat_date: yesterday,
          user_id: item.user_id,
          users_count: item.users_count,
          is_total: true
        )
      end
    else
      stats << NodeConnectionCountDayStat.new(
        id: Utils::Gen.generate_uuid,
        stat_date: yesterday,
        users_count: 0,
        is_total: true
      )
    end

    items = NodeConnectionLog
      .select("user_id, app_version, app_channel, COUNT(*) as users_count")
      .group(:user_id, :app_version, :app_channel)
      .where("DATE(created_at) = ?", yesterday)

    if items.present?
      items.each do |item|
        stats << NodeConnectionCountDayStat.new(
          id: Utils::Gen.generate_uuid,
          stat_date: yesterday,
          user_id: item.user_id,
          users_count: item.users_count,
          app_version: item.app_version,
          app_channel: item.app_channel
        )
      end
      stats << NodeConnectionCountDayStat.new(
        id: Utils::Gen.generate_uuid,
        stat_date: yesterday,
        users_count: 0
      )
    else
      stats << NodeConnectionCountDayStat.new(
        id: Utils::Gen.generate_uuid,
        stat_date: yesterday,
        users_count: 0
      )
    end

    NodeConnectionCountDayStat.bulk_insert(stats, use_provided_primary_key: true)
    # 统计次数段
    #NodeConnectionCountScopeDayStat.stat(yesterday)
  end

end

class Device < ApplicationRecord

  CACHE_KEY = "devices"

  has_and_belongs_to_many :users
  has_many :user_signin_logs
  has_many :user_operation_logs
  has_one :user_session

  after_commit :after_commit_trace, on: [:create]

  # 判断指定uuid是否存在
  def self.uuid_is_exist?(uuid)
    result = RedisCache.hfetch(CACHE_KEY, uuid) do
      d = Device.find_by(uuid: uuid)
      d.present? ? "1" : nil
    end
    result == "1" ? true : false
  end

  # 判断该设备是否修改过用户名或密码
  def username_or_password_is_changed?
    self.username_changed? || self.password_changed?
  end

  # 返回最后一次修改过用户名或密码的用户
  def last_changed_user
    User.find_by(id: self.last_changed_user_id)
  end

  # 返回此设备第一次登录时分配的帐号
  def first_assign_user
    User.find(self.first_user_id)
  end

  # 返回此设备最后一次登录的帐号
  def last_assign_user
    User.find(self.last_user_id)
  end

  # 最后一次登录的关联用户
  def last_signin_user
    self.user_signin_logs.recent.take.user
  end

  # 根据注册用户查找或创建设备
  # 不存在则创建，并于用户关联
  # 如果存在，则检查是否与注册用户关联，未关联则关联
  def self.find_or_create_by_user(user, params)
    device = Device.find_or_initialize_by(uuid: params[:device_uuid].downcase)
    device.uuid = params[:device_uuid].downcase
    device.name = params[:device_name]
    device.model = params[:device_model]
    device.platform = params[:platform]
    device.system_version = params[:system_version]
    device.operator = params[:operator]
    device.net_env = params[:net_env]
    device.app_version = params[:app_version]
    device.app_version_number = params[:app_version_number]
    device.app_channel = params[:app_channel]
    # 如果设备是第一次被创建，则与用户关联
    device.first_user_id = user.id if device.new_record?
    device.last_user_id = user.id
    device.save if device.changed?
    if device.new_record?
      device.users << user
    else
      device.users << user if !device.users.find_by(id: user.id).present?
    end
    device
  end

  private

  def after_commit_trace
    # 写入缓存
    RedisCache.hset(CACHE_KEY, self.uuid, "1")
  end

end

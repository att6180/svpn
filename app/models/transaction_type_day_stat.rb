class TransactionTypeDayStat < ApplicationLogRecord
  #serialize :type_users_count, Array

  scope :by_date, -> (date) { where("stat_date = ?", date) }
  scope :between_date, ->(start_date, end_date) { where("stat_date >= ? AND stat_date <= ?", start_date, end_date) }

  # 统计昨天的或指定某一天的类型人数
  def self.stat(date = nil)
    yesterday = date || Time.now.to_date.yesterday
    stats = []
    # 取出目前所有套餐的价格类型
    price_types = Plan.enabled.except_audit.map{|p| p.price.to_i}.uniq.sort
    # 取出每个价格类型对应的套餐名
    @price_names = Plan.group_price_names

    stats += TransactionTypeDayStat.all_stat(yesterday, price_types)
    stats += TransactionTypeDayStat.filter_stat(yesterday, "channel", price_types)
    stats += TransactionTypeDayStat.filter_stat(yesterday, "version", price_types)
    stats += TransactionTypeDayStat.filter_all_stat(yesterday, price_types)
    TransactionTypeDayStat.bulk_insert(stats)
  end

  # 对比结果集，补全空缺的价格类型
  # items为hash类型的array
  def self.feed_price_type(all_types, items)
    items_types = items.map{|i| i[:price_type]}
    items.each do |item|
      item.delete(:app_channel)
      item.delete(:app_version)
    end
    feed_types = all_types - items_types
    feed_items = []
    feed_types.each do |t|
      feed_items << { price_type: t, users_count: 0 }
    end
    (items + feed_items).sort_by{|i| i[:price_type]}
      .each do |p|
      p["names"] = @price_names[p[:price_type]]
    end
  end

  # 全部
  def self.all_stat(date, price_types)
    stats = []
    users_count = 0
    type_users_count = []
    TransactionLog.using_db(DB_LOG_SLAVE1) do
      type_users_count = TransactionLog.by_date(date)
        .select("FLOOR(price) AS price_type")
        .select("COUNT(DISTINCT user_id) AS users_count")
        .by_paid
        .group(:price)
        .use_index_aasuc
      users_count = type_users_count.map(&:users_count).sum
    end
    feeded_result = feed_price_type(price_types, StatService.to_hash(type_users_count, :id))
    stats << TransactionTypeDayStat.new(
      stat_date: date,
      users_count: users_count,
      type_users_count: feeded_result.to_json
    )
    stats
  end

  # 渠道或版本
  def self.filter_stat(date, filter_type, price_types)
    stats = []
    group_users_count = {}
    group_type_users_count = {}
    # 取出目前所有渠道或版本的筛选类型
    filter_types = SystemEnum.enums_by_type(filter_type).map(&:name)
    TransactionLog.using_db(DB_LOG_SLAVE1) do
      type_users_count = []
      type_users_count = TransactionLog.by_date(date)
        .select("COUNT(DISTINCT user_id) AS users_count")
        .select("FLOOR(price) AS price_type")
        .by_paid
        .use_index_aasuc
        .group(:price)
        .select("app_#{filter_type}")
        .group("app_#{filter_type}")
      group_type_users_count = StatService.to_hash(type_users_count, :id)
        .group_by{|i| i["app_#{filter_type}".to_sym]}
    end
    group_type_users_count.each do |k, v|
      group_users_count[k] = v.map{|i| i[:users_count]}.sum
    end
    # 合并所有筛选项
    all_filter_types = (filter_types + group_type_users_count.map{|k, v| k}).uniq
    all_filter_types.each do |t|
      if group_type_users_count.has_key?(t)
        feeded_result = feed_price_type(price_types, group_type_users_count[t])
      else
        feeded_result = feed_price_type(price_types, [])
      end
      stats << TransactionTypeDayStat.new(
        stat_date: date,
        users_count: group_users_count.dig(t) || 0,
        type_users_count: feeded_result.to_json,
        filter_type: 1,
        "app_#{filter_type}": t,
      )
    end
    stats
  end

  # 渠道和版本
  def self.filter_all_stat(date, price_types)
    stats = []
    group_users_count = {}
    group_type_users_count = {}
    # 取出目前所有渠道或版本的筛选类型
    filter_types = SystemEnumRelation.cache_list
    TransactionLog.using_db(DB_LOG_SLAVE1) do
      type_users_count = []
      type_users_count = TransactionLog.by_date(date)
        .select("COUNT(DISTINCT user_id) AS users_count")
        .select("FLOOR(price) AS price_type")
        .by_paid
        .use_index_aasuc
        .group(:price)
        .select(:app_channel, :app_version)
        .group(:app_channel, :app_version)
      group_type_users_count = StatService.to_hash(type_users_count, :id)
        .group_by{|i| "#{i[:app_channel]}___#{i[:app_version]}"}
    end
    group_type_users_count.each do |k, v|
      group_users_count[k] = v.map{|i| i[:users_count]}.sum
    end
    # 合并所有筛选项
    all_filter_types = (filter_types.map{|i| "#{i.channel}___#{i.version}"} + group_type_users_count.map{|k, v| k}).uniq
    all_filter_types.each do |t|
      st = t.split('___')
      st_channel = st.first
      st_version = st.last
      if group_type_users_count.has_key?(t)
        feeded_result = feed_price_type(price_types, group_type_users_count[t])
      else
        feeded_result = feed_price_type(price_types, [])
      end
      stats << TransactionTypeDayStat.new(
        stat_date: date,
        users_count: group_users_count.dig(t) || 0,
        type_users_count: feeded_result.to_json,
        app_channel: st_channel,
        app_version: st_version,
        filter_type: 2
      )
    end
    stats
  end
end

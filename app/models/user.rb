class User < ApplicationRecord

  CACHE_KEY_CHECKIN_TODAY = "user_checkin_"
  CACHE_KEY_APP_API_TOKENS = "user_app_api_tokens"
  CACHE_KEY_IDS = "user_ids"
  CACHE_KEY_ACTIVE_IDS = "user_active_ids"
  CACHE_KEY_PAID_IDS = "user_paid_ids"
  CACHE_KEY_PLATFORMS = "user_hardware_platforms"
  CACHE_KEY_TRAFFICS = "user_traffics"
  CACHE_KEY_ONLINE_COUNT = "user_online_count_"
  CACHE_KEY_ID_INCREASE = "user_increase_id"
  CACHE_KEY_ID_INCREASE_START_AT = "user_increase_id_start_at"
  CACHE_KEY_MALICIOUS_ACT_IDS = "user_malicious_act_ids"
  CACHE_KEY_CONSUME_COINS_LOCK = "user_consume_coins_lock"
  CACHE_KEY_NODE_TYPE_EXPIRED_LOCK = "user_node_type_expired_lock"

  DOMESTIC_USER_RANGE = ['中国', '台湾', '香港', '澳门', '']

  belongs_to :user_group
  has_and_belongs_to_many :devices
  has_one :user_addition
  has_one :user_tap
  has_many :feedbacks
  has_many :transaction_logs
  has_many :consumption_logs
  has_one :user_session
  has_many :user_node_types
  has_many :user_signin_logs
  has_many :last_signin_log, -> { order('created_at DESC').limit(1) }, class_name: "UserSigninLog"
  has_many :user_app_launch_logs
  has_many :last_app_launch_log, -> { order('created_at DESC').limit(1) }, class_name: "UserAppLaunchLog"
  has_many :user_operation_logs
  has_many :user_connection_logs
  has_many :user_share_logs
  has_many :user_checkin_logs
  has_many :user_account_status_logs
  has_many :last_status_log, -> { order('created_at DESC').limit(1) }, class_name: "UserAccountStatusLog"
  has_many :user_iap_verification_logs
  has_many :node_connection_logs
  has_many :proxy_connection_logs
  has_many :activity_coin_reward_logs
  has_many :client_connection_logs
  has_many :feedback_logs, as: :memberable
  has_many :node_connection_server_logs
  has_many :user_promo_logs

  scope :online_users, -> { where(is_online: true) }
  scope :by_created_date, ->(date) { where(created_at: date.to_date.beginning_of_day..date.to_date.end_of_day) }
  scope :by_created_month, ->(year, month) { where("YEAR(created_at) = ? AND MONTH(created_at) = ?", year, month)}
  scope :yesterday_ago, -> { where("created_at <= ?", Time.now.yesterday.end_of_day)}
  scope :search_by_username, ->(keyword) { where("username = ?", keyword) }
  scope :search_by_id, ->(id) { where(id: id) }
  scope :by_username, ->(username) { where(username: username) }
  scope :group_by_hour, -> { select("HOUR(created_at) as h").group('h').order("h ASC") }
  scope :use_index_ccc, -> { from("#{self.table_name} USE INDEX(users_ccc)") }
  scope :foreign_users, -> { where.not(create_ip_country: DOMESTIC_USER_RANGE) }
  scope :domestic_users, -> { where(create_ip_country: DOMESTIC_USER_RANGE) }

  before_create :before_register_trace
  after_commit :after_commit_trace, on: :create
  before_update :before_update_trace

  # 重置密码
  def reset_password!(password)
    self.password = password
    self.reset_password_count += 1
    self.app_api_token = Utils::Gen.friendly_token
    self.save
  end

  # 封号
  def disable!
    self.update_columns(is_enabled: false) if self.is_enabled?
  end

  def enabled!
    self.update_columns(is_enabled: true) if !self.is_enabled?
  end

  # 恶意行为
  def is_malicious_act?
    $redis.sismember(CACHE_KEY_MALICIOUS_ACT_IDS, self.id)
  end

  # 是否充过值
  def has_paid?
    self.total_payment_amount > 0
  end

  # 今天是否有签到过
  def has_checkin_today?
    RedisCache.hfetch_bool(
      "#{CACHE_KEY_CHECKIN_TODAY}#{DateUtils.time2ymd(Time.now)}",
      self.id,
      DateUtils.remaining_time
    ) do
      self.user_checkin_logs.today.exists?
    end
  end

  # 签到
  def checkin!
    # 找出最后一次签到
    last_checkin = self.user_checkin_logs.order(created_at: :desc).take
    # 如果最后一次签到在昨天，则增加1天连续签到天数
    # 如果从未签到 或 最后一次签到不是昨天，则重置连续签到天数为1天
    yesterday = Time.now.yesterday
    checkin_log = nil
    present_time = ChannelSetting.get_item(
      self.create_app_channel,
      self.create_app_version, ChannelSetting::CHECKIN_TIME
    )
    new_expired_at = nil
    if !self.has_checkin_today?
      ActiveRecord::Base.transaction do
        checkin_log = self.user_checkin_logs.create!(
          checkin_date: Time.now.to_date,
          app_channel: self.create_app_channel,
          app_version: self.create_app_version,
          present_time: present_time
        )
        new_expired_at = self.increment_base_node_type_time!(present_time)
        if last_checkin.present? && last_checkin.created_at.to_date == yesterday.to_date
          self.checkin_days += 1
        elsif last_checkin.blank? || (last_checkin.present? && last_checkin.created_at.to_date != yesterday.to_date)
          self.checkin_days = 1
        end
        self.save!
        self.set_cache_checkin!(true)
      end
    else
      checkin_log = self.user_checkin_logs.order(created_at: :desc).take
      user_base_node_type = self.user_node_types.find_or_create_by(node_type_id: NodeType.base_type_id)
      new_expired_at = user_base_node_type.expired_at
    end
    [checkin_log, new_expired_at]
  end

  # 离线
  def offline!
    self.update_columns(is_online: false, last_offline_time: Time.now)
  end

  # 已使用的流量
  def used_bytes
    result = $redis.hget(CACHE_KEY_TRAFFICS, self.id)
    result.nil? ? 0 : result.to_i
  end

  # 获取本月签到的天数
  def checkin_days_of_month
    self.user_checkin_logs.count_of_month(Time.now.year, Time.now.month)
  end

  def next_group
    self.user_group.next_group
  end

  # 升级到
  def next_group_need_coins
    need_coins = next_group.present? ? next_group.need_coins - self.total_coins : -1
  end

  # 更改用户组
  def set_group(group)
    return if group.blank?
    self.user_group = group
  end

  # 检查当前用户是否在指定用户组
  def is_group?(group_id)
    self.user_group_id == group_id
  end

  # 检查资料完善情况，如果完整，则升级到vip2
  def inspect_profile
    if self.email.present? && self.wechat.present? && self.weibo.present? && self.qq.present?
      if !self.is_group?('vip2')
        set_group('vip2')
        self.save
      end
    else
      if self.is_group?('vip2')
        set_group('vip1')
        self.save
      end
    end
  end

  # 总用户数
  def self.users_count
    $redis.scard(CACHE_KEY_IDS) || 0
  end

  # 今日注册用户数
  def self.today_users_count
    today_users_cache_key = "#{CACHE_KEY_IDS}_#{DateUtils.time2ymd(Time.now)}"
    $redis.scard(today_users_cache_key) || 0
  end

  # 最后一次连接为今天的用户数
  def self.active_users_count
    active_cache_key = "#{CACHE_KEY_ACTIVE_IDS}_#{DateUtils.time2ymd(Time.now)}"
    $redis.scard(active_cache_key) || 0
  end

  # 已充值用户数
  def self.paid_users_count
    $redis.scard(CACHE_KEY_PAID_IDS) || 0
  end

  # 在线的用户数
  def self.online_users_count
    current = Node.enabled.level1.sum(&:cache_online_users)
    # 检查并更新当天最高
    today_cache_key = "#{CACHE_KEY_ONLINE_COUNT}#{DateUtils.time2ymd(Time.now)}"
    today = $redis.hget(today_cache_key, "count")&.to_i || 0
    if current > today
      $redis.hset(today_cache_key, "count", current)
      $redis.hset(today_cache_key, "time", DateUtils.time2str(Time.now))
      $redis.expire(today_cache_key, DateUtils.remaining_time) if $redis.ttl(today_cache_key) == -1
    end
    # 检查并更新历史最高
    if current > Setting.peak_online_users_count.to_i
      Setting.peak_online_users_count = current
      Setting.peak_online_users_count_at = Time.now.strftime("%Y-%m-%d %H:%M:%S")
    end
    current
  end

  # 今天最高在线人数
  def self.peak_online_users_count_today
    today_cache_key = "#{CACHE_KEY_ONLINE_COUNT}#{DateUtils.time2ymd(Time.now)}"
    count = $redis.hget(today_cache_key, "count")&.to_i || 0
    time = $redis.hget(today_cache_key, "time") || DateUtils.time2str(Time.now)
    [count, time]
  end

  # 是否更改了用户名
  # 增加国际短信功能之后只需判断用户名是否为数字 18/11/18
  def username_changed?
    /^(?:\+?|\b)\d+$/ === self.username
    # /^\d{11}$/ === self.username && self.username.first == "1"
  end

  # 套餐是否过期
  def plan_expired?
    return self.user_node_types.pluck(:expired_at).all? {|expired_at| expired_at < Time.now} && self.current_coins < 1
  end

  # 是否修改了帐号或密码
  def username_or_password_changed?
    self.username_changed? || self.reset_password_count > 0
  end

  def self.last_signin_versions_by_id(user_id)
    channel, version, number, platform = RedisCache.hfetch_list(UserSigninLog::CACHE_KEY_VERSIONS, user_id) do
      log = UserSigninLog.select(:platform, :app_channel, :app_version, :app_version_number)
        .where(user_id: user_id)
        .recent.take
      log.present? ? [log.app_channel, log.app_version, log.app_version_number, log.platform] : nil
    end
    channel.present? ? [channel, version, number, platform] : ""
  end

  # 获取最后一次登录的版本相关信息
  def last_signin_versions
    User.last_signin_versions_by_id(self.id)
  end

  # 最后一次登录的版本相关信息字符
  def last_app_signin_version_info
    channel, version, number = self.last_signin_versions
    return "" if !channel.present?
    "#{channel}_#{version}_#{number}"
  end

  # 注册时的版本信息
  def created_version_info
    "#{self.create_app_channel}_#{self.create_app_version}_#{self.create_app_version_number}"
  end

  # 是否有新消息
  def has_new_message?
    RedisCache.hfetch_bool(Feedback::CACHE_KEY_NEW_MESSAGE, self.id) do
      self.feedbacks.where(has_read: false, is_processed: true).exists?
    end
  end

  # 是否有新消息(for new feedback)
  def has_new_message2?
    RedisCache.hfetch_bool(Feedback::CACHE_KEY_NEW_MESSAGE2, self.id) do
      self.feedbacks.where(has_read2: false, status: 1).exists?
    end
  end

  # 记录最后连接的服务器
  def record_last_node(node)
    self.user_addition.update_columns(last_connected_node_id: node.id)
  end

  # 根据服务器类型ID获得指定用户服务器类型
  def user_node_type_by_type_id(id)
    self.user_node_types.find_or_create_by(node_type_id: id)
  end

  # 为用户的基础服务器类型累加时间(单位: 分钟)
  def increment_base_node_type_time!(t = 1)
    base_type = NodeType.sorted_by_level.take
    user_base_node_type = self.user_node_types.find_or_create_by(node_type_id: base_type.id)
    new_expired_at = user_base_node_type.expired_at < Time.now ? Time.now + t.minutes : user_base_node_type.expired_at + t.minutes
    user_base_node_type.update_columns(expired_at: new_expired_at)
    new_expired_at
  end

  # 为用户的基础服务器类型使用次数累加1
  def increment_base_node_type_used_count!
    base_type = NodeType.sorted_by_level.take
    user_base_node_type = self.user_node_types.find_or_create_by(node_type_id: base_type.id)
    user_base_node_type.increment!(:used_count, 1)
    # 检查使用次数如果达到要求，则升级成包月
    user_base_node_type.inspect_and_switch_state!
  end

  # 检查用户对应的服务器类型的服务是否到期,扣除虚拟币
  def inspect_expiration(node_id, kick_user = false, auto_renew = false)
    result = true
    new_user_node_type = nil
    # 得到用户当前连接的服务器
    node = Node.find_by(id: node_id)
    node_type = node.node_type
    # 根据服务器类型，得到需要扣除的虚拟币数
    expense_coins = node_type.expense_coins
    # 根据服务器类型ID获得用户对应的服务器类型
    user_node_type = self.user_node_types.find_or_create_by(node_type_id: node_type.id)
    # 只检查需要虚拟币并且状态是按次的服务器类型
    if expense_coins > 0 && user_node_type.status_times?
      # 如果此服务器类型已到期
      if user_node_type.expired_at <= Time.now
        # 检查用户当前是否有足够的虚拟币
        if self.current_coins >= expense_coins && auto_renew && Setting.auto_renew.to_i == 1
          # 找到用户最后一次登录的日志，取出渠道和版本信息
          versions = self.last_signin_log.take
          app_channel = versions&.app_channel
          app_version = versions&.app_version
          if app_channel.blank? || app_version.blank?
            app_channel = self.create_app_channel
            app_version = self.create_app_version
          end
          ActiveRecord::Base.transaction do
            # 减去虚拟币并增加过期时间
            self.update!(current_coins: self.current_coins - expense_coins, total_used_coins: self.total_used_coins + expense_coins)
            # 为指定的用户服务器类型增加过期时间
            new_expired_at = user_node_type.expired_at < Time.now ? Time.now + 24.hours : user_node_type.expired_at + 24.hours
            user_node_type.update!(expired_at: new_expired_at, used_count: user_node_type.used_count + 1)
            # 写入消费日志
            self.consumption_logs.create(
              node_type_name: node_type.name,
              node_type_id: node_type.id,
              coins: expense_coins,
              app_channel: app_channel,
              app_version: app_version
            )
          end
          # 累加使用次数
          self.increment_base_node_type_used_count! if !node_type.is_base_type?
          # 检查使用次数如果达到要求，则升级成包月
          user_node_type.inspect_and_switch_state!
          new_user_node_type = user_node_type
        else
          result = false
          # 重置session_token，原因为虚拟币不够
          id, _ = UserSession.read_by_user_id(self.id)
          UserSession.regenerate_token!(self.id, id, UserSession::ERR_NOT_ENOUGH_COINS) if !id.nil? && kick_user
        end
      end
      yield if block_given?
    end
    [result, new_user_node_type]
  end

  # 在代理服务器写入用户流量时检查用户对应的服务器类型的服务是否到期
  # 如果到期，则扣除虚拟币并为对应的服务器类型增加时间
  # 如果没有足够的虚拟币，则session_token，随后代理服务器会因为session_token验证不通过而断开用户链接
  # 从而导致代理服务器将不再记录此用户的流量信息，本方法也不会被再次执行
  def inspect_expiration_for_proxy(node_id)
    result = true
    # 最后检查时间超过20分钟 并且 帐号已到达过期时间
    # TODO: 这里的时间后期可在后台进行配置
    if self.user_addition.last_inspect_expiration_at < Time.now - 10.minutes
      result, node_type = inspect_expiration(node_id, true, self.auto_renew) do
        # 更新最后检查时间
        self.user_addition.update_columns(last_inspect_expiration_at: Time.now)
      end
    end
    result
  end

  # 为用户扣除指定服务器类型需要的钻石数
  # 如果是代理服务器续费记录服务器id、名称、url
  def consume_coins(node_type, node = nil)
    expense_coins = node_type.expense_coins
    # 检查用户当前是否有足够的虚拟币
    return false if self.current_coins < expense_coins
    # TODO: 做成后台可配置, 每次购买增加的小时数
    increment_hours = 24
    # 找到用户最后一次登录的日志，取出渠道和版本信息
    app_channel, app_version = self.last_signin_versions
    if app_channel.blank? || app_version.blank?
      app_channel = self.create_app_channel
      app_version = self.create_app_version
    end
    # 根据服务器类型ID获得用户对应的服务器类型
    user_node_type = self.user_node_types.find_or_create_by(node_type_id: node_type.id)
    $lock_manager.lock("#{CACHE_KEY_CONSUME_COINS_LOCK}_#{self.id}", 2000) do |locked|
      if locked
        ActiveRecord::Base.transaction do
          # 减去虚拟币并增加过期时间
          self.update!(current_coins: self.current_coins - expense_coins, total_used_coins: self.total_used_coins + expense_coins)
          # 为指定的用户服务器类型增加过期时间
          new_expired_at = user_node_type.expired_at < Time.now ? Time.now + increment_hours.hours : user_node_type.expired_at + increment_hours.hours
          user_node_type.update!(expired_at: new_expired_at, used_count: user_node_type.used_count + 1)
          # 写入消费日志
          self.consumption_logs.create!(
            node_type_id: node_type.id,
            node_type_name: node_type.name,
            node_id: node&.id,
            node_name: node&.name,
            node_url: node&.url,
            coins: expense_coins,
            app_channel: app_channel,
            app_version: app_version
          )
          # 累加服务类型使用次数
          self.increment_base_node_type_used_count! if !node_type.is_base_type?
          # 检查使用次数如果达到要求，则升级成包月
          user_node_type.inspect_and_switch_state!
        end
      else
        puts "consume coins is locked!!!"
      end
    end
    user_node_type
  end

  # 获得指定的用户服务类型
  def node_type_by_id(node_type_id)
    self.user_node_types.find_or_create_by(node_type_id: node_type_id)
  end

  # 检查服务器类型是否到期
  def node_type_is_expired?(node_type_id)
      user_node_type = self.user_node_types.find_or_create_by(node_type_id: node_type_id)
      user_node_type.status_times? && user_node_type.expired_at <= Time.now
  end

  # 绑定推广人
  def bind_promoter!(promoter, promo_code)
    promoter_coins = Setting.promo_promoter_coins.to_i
    accepted_user_coins = Setting.promo_accepted_user_coins.to_i
    ActiveRecord::Base.transaction do
      # 被推广者
      self.promo_user_id = promoter.id
      self.binded_promo_code = promoter.promo_code
      self.current_coins += accepted_user_coins
      self.save!
      # 推广者
      promoter.current_coins += promoter_coins
      promoter.promo_users_count += 1
      promoter.promo_coins_count += promoter_coins
      promoter.save!
      # 写入日志
      UserPromoLog.create!(
        promoter_id: promoter.id,
        promo_code: promo_code,
        accepted_user_id: self.id,
        promoter_coins: promoter_coins,
        accepted_user_coins: accepted_user_coins
      )
    end
    true
  end

  # 判断当前用户是否需要上传客户端调试日志
  def need_upload_client_log?
    return false if Setting.client_debug_log_enabled == "0" || Setting.client_debug_log_user_ids.blank?
    user_ids = Setting.client_debug_log_user_ids.split(',').select{|id| id.present?}
    user_ids.include?(self.id.to_s)
  end

  # 读写相关字段缓存值
  def self.set_cache_app_api_token(user_id, token)
    RedisCache.hset(CACHE_KEY_APP_API_TOKENS, user_id, token)
  end

  def self.get_cache_app_api_token(user_id)
    RedisCache.hfetch(CACHE_KEY_APP_API_TOKENS, user_id) do
      User.find(user_id).app_api_token
    end
  end

  def set_cache_checkin!(bool)
    RedisCache.hset_bool(
      "#{CACHE_KEY_CHECKIN_TODAY}#{DateUtils.time2ymd(Time.now)}",
      self.id,
      bool,
      DateUtils.remaining_time
    )
  end

  # 写入已充值用户id缓存
  def self.save_cache_paid_id!(user_id)
    $redis.sadd(CACHE_KEY_PAID_IDS, user_id)
  end

  # 写入活跃人数缓存
  def self.save_cache_active_list!(user_id)
    active_cache_key = "#{CACHE_KEY_ACTIVE_IDS}_#{DateUtils.time2ymd(Time.now)}"
    $redis.sadd(active_cache_key, user_id)
    $redis.expire(active_cache_key, DateUtils.remaining_time) if $redis.ttl(active_cache_key) == -1
  end

  # 写入系统平台统计
  def self.save_platform_list(platform)
    $redis.hincrby(CACHE_KEY_PLATFORMS, platform, 1)
  end

  # 是否是机器人
  def self.is_robot?(user_id)
    return false if Setting.tool_ignore_ids.nil?
    Setting.tool_ignore_ids.include?(user_id)
  end

  private

  def has_password?
    self.password.present? && self.password_confirmation.present?
  end

  def before_update_trace
    if self.app_api_token_changed?
      User.set_cache_app_api_token(self.id, self.app_api_token)
    end
    # 写入今日活跃人数缓存
    if self.current_signin_at_changed?
      User.save_cache_active_list!(self.id)
    end
  end

  # 注册记录创建前
  def before_register_trace
    # 登录相关默认
    self.is_online = true
    self.signin_count = 1
    self.current_signin_at = Time.now
    self.last_signin_at = Time.now
    # 生成app api token
    self.app_api_token = Utils::Gen.friendly_token
    # 设置用户组
    set_group(UserGroup.base_id)
    # 版本渠道赠送钻石数量
    new_user_conis = ChannelSetting.get_item(
      self.create_app_channel,
      self.create_app_version, ChannelSetting::NEW_USER_COINS
    )
    # 手机号注册赠送钻石
    if self.username_changed?
      new_user_conis += Setting.new_user_or_change_username_to_telephone_coins.to_i
    end
    self.current_coins = new_user_conis 
    self.auto_renew = true
    # 生成推广码
    loop do
      self.promo_code = Utils::Gen.friendly_code(6)
      break unless User.find_by(promo_code: self.promo_code)
    end
  end

  # 2018-07-09修改生成规则为账号从11459229开始自增+1
  # 生成分配用户名
  def generate_username
    # sequence = self.id.to_s
    # zero_length = 11 - prefix.length
    # zeros = "0" * (zero_length - sequence.length)
    # "#{prefix}#{zeros}#{self.id}"
    prefix = "sup"
    start_at = $redis.hincrby(CACHE_KEY_ID_INCREASE, CACHE_KEY_ID_INCREASE_START_AT, 1)
    "#{prefix}#{start_at}"
  end

  def after_commit_cache!
    # 写入签到缓存
    self.set_cache_checkin!(false)
    # 写入app_api_token缓存
    User.set_cache_app_api_token(self.id, self.app_api_token)
    # 写入反馈回复消息缓存
    Feedback.write_new_message_cache(self.id)
    # 写入注册用户id列表缓存
    $redis.sadd(CACHE_KEY_IDS, self.id)
    # 写入今日注册用户id列表缓存
    today_users_cache_key = "#{CACHE_KEY_IDS}_#{DateUtils.time2ymd(Time.now)}"
    $redis.sadd(today_users_cache_key, self.id)
    $redis.expire(today_users_cache_key, DateUtils.remaining_time) if $redis.ttl(today_users_cache_key) == -1
    # 写入今日活跃人数缓存
    User.save_cache_active_list!(self.id)
    # 写入硬件平台统计
    User.save_platform_list(self.create_platform)
  end

  # 注册记录提交后
  def after_commit_trace
    after_commit_cache!
    # 更新用户名，这里放在记录提交之后做，是因为用户名要基于ID来命名
    if self.username.blank? && self.password.blank?
      self.update_columns(username: generate_username, password: Utils::Gen.friendly_code)
    end
    # 创建用户详情信息，用于数据库单向同步，防止数据冲突
    inspect_expiration_time = DateUtils.time2str(Time.now - 20.minutes)
    CreateUserAdditionJob.perform_later(self.id, inspect_expiration_time)
  end

end

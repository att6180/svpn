class DynamicServerConnectionFailedLog < ApplicationLogRecord
  include Uuidable
  belongs_to :dynamic_server, counter_cache: :failed_count
  belongs_to :user

  scope :between_date, ->(start_at, end_at) { where(created_at: start_at.beginning_of_day..end_at.end_of_day)}
  scope :group_by_server, -> {
    select("dynamic_server_id, COUNT(id) AS failure_count")
      .group(:dynamic_server_id)
  }

end

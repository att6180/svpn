module Manage
  class ApplicationManageRecord < ActiveRecord::Base
    self.abstract_class = true

    establish_connection DB_MANAGE

    scope :recent, -> { order(created_at: :desc) }
  end
end

module Manage
  class Role < ApplicationManageRecord
    has_many :admin

    validates :name, uniqueness: true

    scope :sorted_by_id, -> { order(id: :asc)}
    scope :except_reserved, -> { where.not(name: %W(super_admin guest))}
  end
end

class Manage::Activity < Manage::ApplicationManageRecord

  belongs_to :admin
  belongs_to :recipientable, polymorphic: true

  serialize :parameters

  def log
    I18n.t("activity.#{self.key}", self.parameters)
  end

end

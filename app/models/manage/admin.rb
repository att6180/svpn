module Manage
  class Admin < ApplicationManageRecord
    has_secure_token :api_token
    has_secure_password

    belongs_to :role
    has_many :activities
    has_many :activity_coin_reward_logs
    has_many :feedback_logs, as: :memberable

    validates :username, presence: true, uniqueness: true, length: { minimum: 5 }, on: :create
    validates :password, presence: true, confirmation: true, length: { minimum: 6 }, on: :create
    validates :password_confirmation, presence: true, on: :create

    scope :sorted, -> { order(id: :asc) }

    def create_log(options)
      self.activities.create(
        key: "admin.#{options[:key]}",
        parameters: options[:params],
        recipientable: options[:recipient]
      )
    end

    # 记录登录信息
    def sign_in(remote_ip)
      self.update_columns(
        signin_count: self.signin_count + 1,
        current_signin_at: Time.now,
        current_signin_ip: remote_ip,
        last_signin_at: self.current_signin_at,
        last_signin_ip: self.current_signin_ip
      )
    end

    # 是否是超级管理员
    def super_admin?
      self.username == 'admin'
    end

    # 是否能对模块进行操作
    def can?(module_name)
      super_admin? || (self.role.present? && self.role[module_name])
    end

    def self.disallow_login?(ip)
      cache_key = "admin_auth_fail_#{ip.gsub('.', '')}"
      cached_count = Rails.cache.read(cache_key)
      failed_count = cached_count.present? ? cached_count : 0
      failed_count >= Setting.system_allow_login_fail_count.to_i
    end

    def self.auth_fail_count(ip)
      return if Setting.system_login_fail_limit.to_i == 0
      cache_key = "admin_auth_fail_#{ip.gsub('.', '')}"
      cached_count = Rails.cache.read(cache_key)
      failed_count = cached_count.present? ? cached_count : 0
      Rails.cache.write(cache_key, failed_count + 1, expires_in: Setting.system_login_disable_minutes.to_i.minutes)
    end

  end
end

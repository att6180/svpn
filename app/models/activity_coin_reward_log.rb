class ActivityCoinRewardLog < ApplicationLogRecord
  # 字段说明:
  # operation_type:
  #   0: 管理员修改钻石
  #   1: 管理批量奖励钻石

  belongs_to :admin, class_name: Manage::Admin
  belongs_to :user
end

class FeedbackShortcutReplyCategory < ApplicationRecord
  CACHE_KEY = "shortcut_replies"
  has_many :feedback_shortcut_replies, dependent: :destroy
  validates :name, presence: true
  after_commit :after_commit_trace, on: [:create, :update, :destroy]

  # 缓存全部列表
  def self.cache_list
    Rails.cache.fetch(CACHE_KEY, expires_in: 24.hours) do
      FeedbackShortcutReplyCategory.order(:name).includes(:feedback_shortcut_replies).to_a
    end
  end

  private

  def after_commit_trace
    Rails.cache.delete(CACHE_KEY)
  end

end

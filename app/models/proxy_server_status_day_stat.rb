class ProxyServerStatusDayStat < ApplicationLogRecord

  belongs_to :node

  scope :between_date, ->(start_at, end_at) { where(stat_date: start_at..end_at)}

  # 统计前一天的服务器状态历史记录
  def self.stat(date = nil)
    yesterday = date || Time.now.to_date.yesterday

    logs = ProxyServerStatusLog.group_by_day_average.by_date(yesterday)
    result = JSON.parse(logs.index_by(&:node_id).to_json(except: :id))

    nodes = Node.enabled.level1
    nodes.each do |node|
      next if !result.has_key?(node.id.to_s)

      min_bandwidth = ProxyServerStatusLog.min_item(yesterday, "bandwidth_percent")
      if min_bandwidth.present?
        result[node.id.to_s]["min_bandwidth_percent"] = min_bandwidth.bandwidth_percent
        result[node.id.to_s]["min_bandwidth_time"] = min_bandwidth.created_at
      end
      max_bandwidth = ProxyServerStatusLog.max_item(yesterday, "bandwidth_percent")
      if max_bandwidth.present?
        result[node.id.to_s]["max_bandwidth_percent"] = max_bandwidth.bandwidth_percent
        result[node.id.to_s]["max_bandwidth_time"] = max_bandwidth.created_at
      end

      min_cpu = ProxyServerStatusLog.min_item(yesterday, "cpu_percent")
      if min_cpu.present?
        result[node.id.to_s]["min_cpu_percent"] = min_cpu.cpu_percent
        result[node.id.to_s]["min_cpu_time"] = min_cpu.created_at
      end
      max_cpu = ProxyServerStatusLog.max_item(yesterday, "cpu_percent")
      if max_cpu.present?
        result[node.id.to_s]["max_cpu_percent"] = max_cpu.cpu_percent
        result[node.id.to_s]["max_cpu_time"] = max_cpu.created_at
      end
      min_memory = ProxyServerStatusLog.min_item(yesterday, "memory_percent")
      if min_memory.present?
        result[node.id.to_s]["min_memory_percent"] = min_memory.memory_percent
        result[node.id.to_s]["min_memory_time"] = min_memory.created_at
      end
      max_memory = ProxyServerStatusLog.max_item(yesterday, "memory_percent")
      if max_memory.present?
        result[node.id.to_s]["max_memory_percent"] = max_memory.memory_percent
        result[node.id.to_s]["max_memory_time"] = max_memory.created_at
      end

      min_network_speed_up = ProxyServerStatusLog.min_item(yesterday, "network_speed_up")
      if min_network_speed_up.present?
        result[node.id.to_s]["min_network_speed_up"] = min_network_speed_up.network_speed_up
        result[node.id.to_s]["min_network_speed_up_time"] = min_network_speed_up.created_at
      end
      max_network_speed_up = ProxyServerStatusLog.max_item(yesterday, "network_speed_up")
      if max_network_speed_up.present?
        result[node.id.to_s]["max_network_speed_up"] = max_network_speed_up.network_speed_up
        result[node.id.to_s]["max_network_speed_up_time"] = max_network_speed_up.created_at
      end

      min_network_speed_down = ProxyServerStatusLog.min_item(yesterday, "network_speed_down")
      if min_network_speed_down.present?
        result[node.id.to_s]["min_network_speed_down"] = min_network_speed_down.network_speed_down
        result[node.id.to_s]["min_network_speed_down_time"] = min_network_speed_down.created_at
      end
      max_network_speed_down = ProxyServerStatusLog.max_item(yesterday, "network_speed_down")
      if max_network_speed_down.present?
        result[node.id.to_s]["max_network_speed_down"] = max_network_speed_down.network_speed_down
        result[node.id.to_s]["max_network_speed_down_time"] = max_network_speed_down.created_at
      end

      min_disk = ProxyServerStatusLog.min_item(yesterday, "disk_percent")
      if min_disk.present?
        result[node.id.to_s]["min_disk_percent"] = min_disk.disk_percent
        result[node.id.to_s]["min_disk_time"] = min_disk.created_at
      end
      max_disk = ProxyServerStatusLog.max_item(yesterday, "disk_percent")
      if max_disk.present?
        result[node.id.to_s]["max_disk_percent"] = max_disk.disk_percent
        result[node.id.to_s]["max_disk_time"] = max_disk.created_at
      end
    end

    stats = []
    result.each do |node_id, node|
      stats << ProxyServerStatusDayStat.new(
        stat_date: node["stat_date"],
        node_id: node["node_id"],

        min_bandwidth_percent: node["min_bandwidth_percent"],
        min_bandwidth_time: node["min_bandwidth_time"],
        max_bandwidth_percent: node["max_bandwidth_percent"],
        max_bandwidth_time: node["max_bandwidth_time"],
        average_bandwidth_percent: node["average_bandwidth_percent"],

        min_cpu_percent: node["min_cpu_percent"],
        min_cpu_time: node["min_cpu_time"],
        max_cpu_percent: node["max_cpu_percent"],
        max_cpu_time: node["max_cpu_time"],
        average_cpu_percent: node["average_cpu_percent"],

        min_memory_percent: node["min_memory_percent"],
        min_memory_time: node["min_memory_time"],
        max_memory_percent: node["max_memory_percent"],
        max_memory_time: node["max_memory_time"],
        average_memory_percent: node["average_memory_percent"],

        min_network_speed_up: node["min_network_speed_up"],
        min_network_speed_up_time: node["min_network_speed_up_time"],
        max_network_speed_up: node["max_network_speed_up"],
        max_network_speed_up_time: node["max_network_speed_up_time"],
        average_network_speed_up: node["average_network_speed_up"],

        min_network_speed_down: node["min_network_speed_down"],
        min_network_speed_down_time: node["min_network_speed_down_time"],
        max_network_speed_down: node["max_network_speed_down"],
        max_network_speed_down_time: node["max_network_speed_down_time"],
        average_network_speed_down: node["average_network_speed_down"],

        min_disk_percent: node["min_disk_percent"],
        min_disk_time: node["min_disk_time"],
        max_disk_percent: node["max_disk_percent"],
        max_disk_time: node["max_disk_time"],
        average_disk_percent: node["average_disk_percent"]
      )
    end
    ProxyServerStatusDayStat.bulk_insert(stats)

  end

end

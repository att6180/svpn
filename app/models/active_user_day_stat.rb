class ActiveUserDayStat < ApplicationLogRecord
  include Uuidable

  scope :between_months, -> (start_at, end_at) { where(stat_date: start_at..end_at) }

  scope :stat_result, -> {
    select("stat_date, SUM(users_count) as total_users_count")
      .group("stat_date") }

  # 统计前一天的活跃用户数
  def self.stat(date = nil)
    yesterday = date || Time.now.to_date.yesterday

    stats = []
    # 全部统计
    items = UserActiveHourLog
      .select("COUNT(DISTINCT user_id) as users_count")
      .where("created_date = ?", yesterday)

    if items.present?
      item = items.take
      stats << ActiveUserDayStat.new(
        id: Utils::Gen.generate_uuid,
        stat_date: yesterday,
        users_count: item.users_count
      )
    end

    # 渠道
    items = UserActiveHourLog
      .select("COUNT(DISTINCT user_id) as users_count, app_channel")
      .group(:app_channel)
      .where("created_date = ?", yesterday)

    if items.present?
      items.each do |item|
        stats << ActiveUserDayStat.new(
          id: Utils::Gen.generate_uuid,
          stat_date: yesterday,
          app_channel: item.app_channel,
          users_count: item.users_count,
          filter_type: 1
        )
      end
      stats << ActiveUserDayStat.new(
        id: Utils::Gen.generate_uuid,
        stat_date: yesterday,
        users_count: 0,
        filter_type: 1
      )
    else
      stats << ActiveUserDayStat.new(
        id: Utils::Gen.generate_uuid,
        stat_date: yesterday,
        users_count: 0,
        filter_type: 1
      )
    end

    # 版本
    items = UserActiveHourLog
      .select("COUNT(DISTINCT user_id) as users_count, app_version")
      .group(:app_version)
      .where("created_date = ?", yesterday)

    if items.present?
      items.each do |item|
        stats << ActiveUserDayStat.new(
          id: Utils::Gen.generate_uuid,
          stat_date: yesterday,
          app_version: item.app_version,
          users_count: item.users_count,
          filter_type: 1
        )
      end
      stats << ActiveUserDayStat.new(
        id: Utils::Gen.generate_uuid,
        stat_date: yesterday,
        users_count: 0,
        filter_type: 1
      )
    else
      stats << ActiveUserDayStat.new(
        id: Utils::Gen.generate_uuid,
        stat_date: yesterday,
        users_count: 0,
        filter_type: 1
      )
    end

    # 渠道和版本
    items = UserActiveHourLog
      .select("COUNT(DISTINCT user_id) as users_count, app_version, app_channel")
      .group(:app_version, :app_channel)
      .where("created_date = ?", yesterday)

    if items.present?
      items.each do |item|
        stats << ActiveUserDayStat.new(
          id: Utils::Gen.generate_uuid,
          stat_date: yesterday,
          app_version: item.app_version,
          app_channel: item.app_channel,
          users_count: item.users_count,
          filter_type: 2
        )
      end
      stats << ActiveUserDayStat.new(
        id: Utils::Gen.generate_uuid,
        stat_date: yesterday,
        users_count: 0,
        filter_type: 2
      )
    else
      stats << ActiveUserDayStat.new(
        id: Utils::Gen.generate_uuid,
        stat_date: yesterday,
        users_count: 0,
        filter_type: 2
      )
    end

    ActiveUserDayStat.bulk_insert(stats, use_provided_primary_key: true) if stats.present?

    # 统计前一天的签到用户数，签到统计依赖于活跃统计数据
    Stat::UserCheckinDayStatJob.perform_later
  end
end

class UserHardwareStat < ApplicationLogRecord
  include Uuidable

  scope :stat_result, -> { select("device_model, platform, SUM(quantity) AS total").group("device_model, platform").order("total DESC") }
  scope :stat_result_by_platform, -> { select("platform, SUM(quantity) AS total").group("platform").order("total DESC") }

  # 获取用户按平台统计的数据
  def self.platform_stat
    list = $redis.hgetall(User::CACHE_KEY_PLATFORMS)
    platform = list.nil? ? [] : list.sort_by{|k, v| v.to_i}.reverse
  end

  # 统计用户硬件数据
  def self.stat
    UserHardwareStat.delete_all

    stats = []
    limit = 50

    # 全部
    items = User.select("create_device_model, create_platform, count(*) as quantity")
      .yesterday_ago
      .group(:create_device_model, :create_platform)
      .order("quantity desc")
      .limit(limit)
    items.each do |item|
      stats << UserHardwareStat.new(
        id: Utils::Gen.generate_uuid,
        device_model: item.create_device_model,
        platform: item.create_platform,
        quantity: item.quantity
      )
    end

    # 渠道
    items = User.select("create_app_channel, create_device_model, create_platform, count(*) as quantity")
      .yesterday_ago
      .group(:create_app_channel, :create_device_model, :create_platform)
      .order("quantity desc")
      .limit(limit)
    items.each do |item|
      stats << UserHardwareStat.new(
        id: Utils::Gen.generate_uuid,
        app_channel: item.create_app_channel,
        device_model: item.create_device_model,
        platform: item.create_platform,
        quantity: item.quantity,
        filter_type: 1
      )
    end

    # 版本
    items = User.select("create_app_version, create_device_model, create_platform, count(*) as quantity")
      .yesterday_ago
      .group( :create_app_version, :create_device_model, :create_platform)
      .order("quantity desc")
      .limit(limit)
    items.each do |item|
      stats << UserHardwareStat.new(
        id: Utils::Gen.generate_uuid,
        app_version: item.create_app_version,
        device_model: item.create_device_model,
        platform: item.create_platform,
        quantity: item.quantity,
        filter_type: 1
      )
    end

    # 渠道和版本
    items = User.select("create_app_version, create_app_channel, create_device_model, create_platform, count(*) as quantity")
      .yesterday_ago
      .group(:create_app_version, :create_app_channel, :create_device_model, :create_platform)
      .order("quantity desc")
      .limit(limit)
    items.each do |item|
      stats << UserHardwareStat.new(
        id: Utils::Gen.generate_uuid,
        app_version: item.create_app_version,
        app_channel: item.create_app_channel,
        device_model: item.create_device_model,
        platform: item.create_platform,
        quantity: item.quantity,
        filter_type: 2
      )
    end

    UserHardwareStat.bulk_insert(stats, use_provided_primary_key: true) if stats.present?
  end

end

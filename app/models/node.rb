class Node < ApplicationRecord

  CACHE_KEY_ONLINE_USERS = "node_online_users_count_"
  CACHE_KEY_NODES_COUNT = "node_type_nodes_count_"
  CACHE_KEY_NODES_ENABLED_COUNT = "node_type_nodes_enabled_count_"
  CACHE_KEY_NODES_ISP = 'node_by_isp_'
  CACHE_KEY_NODES_OUT_REGION = "node_by_out_region_"
  CACHE_KEY_NODES_DIVERSION = 'node_by_diversion_'
  CACHE_KEY_NODES_LINE_LIST = "node_customize_lines"

  NET_TYPES = ::NodeIcp::NET_TYPES

  enum status: { disabled: 0, enabled: 1, standby: -1, deprecated: -2 }
  enum types: { covered: 1, smart: 2, customization: 3 }

  attr_accessor :cache_online_users

  belongs_to :node_region, counter_cache: true
  belongs_to :user_group
  belongs_to :node_type
  belongs_to :node_isp, required: false
  belongs_to :node_out_region, class_name: "NodeRegion", foreign_key: :node_region_out_id
  has_one :node_addition
  has_many :user_taps
  has_many :proxy_server_status_logs
  has_many :proxy_server_status_day_stats
  has_many :client_connection_logs

  validates :node_type_id, :node_region_id, :name, :url, presence: true
  validates :node_region_out_id, presence: true, if: -> {level == 1}
  validates :id, uniqueness: true

  after_commit :after_commit_trace, on: :create
  after_commit :after_commit_trace_update, on: :update
  before_update :before_update_trace
  after_commit :delete_cache

  scope :enabled_disabled, -> { where(status: [:enabled, :disabled]) }
  scope :enabled_standby, -> { where(status: [:enabled, :standby]) }
  scope :sorted, -> { order(connections_count: :asc, id: :asc) }
  scope :sorted_by_region_id, -> { order(node_region_id: :asc) }
  scope :overload, -> { where("connections_count / max_connections_count > 0.8") }
  scope :unoverload, -> { where("connections_count / max_connections_count <= 0.8") }
  scope :level1, -> { where(level: 1) }
  scope :level2, -> { where(level: 2) }
  scope :oversea, -> { where(is_domestic: false) }
  scope :domestic, -> { where(is_domestic: true) }
  scope :ismart, -> { where(types: [:covered, :smart]) }
  scope :icustomization, -> { where(types: [:covered, :customization]) }

  def domestic_flag
    self.is_domestic? ? 1 : 0
  end

  def record_online_users!(count)
    if self.enabled?
      $redis.hset("#{CACHE_KEY_ONLINE_USERS}#{self.types_before_type_cast}_#{self.node_type_id}_#{self.domestic_flag}", self.id, count)
    else
      self.delete_from_online_users_cache!
    end
  end

  def cache_online_users
    $redis.hget("#{CACHE_KEY_ONLINE_USERS}#{self.types_before_type_cast}_#{self.node_type_id}_#{self.domestic_flag}", self.id).to_i
  end

  def increase_online_users!
    $redis.hincrby("#{CACHE_KEY_ONLINE_USERS}#{self.types_before_type_cast}_#{self.node_type_id}_#{self.domestic_flag}", self.id, 1)
  end

  def delete_from_online_users_cache!
    $redis.hdel("#{CACHE_KEY_ONLINE_USERS}#{self.types_before_type_cast}_#{self.node_type_id}_#{self.domestic_flag}", self.id)
  end

  # 选择硬核直连在线人数最少的服务器，如果redis中没数据，则从mysql中选择
  # 硬核直连可分配coverr、smart类型服务器
  def self.select_smart_node(node_type, is_domestic, ip_region)
    domestic = is_domestic ? 1 : 0
    ip_area = ip_region[:area]
    ip_province = ip_region[:province]
    ip_city = ip_region[:city]

    list = smart_cache_list_by_node_type(node_type, domestic)
    if list.present?
      node_ids = list.keys.map(&:to_i)
      # 获取当前ip解析运营商匹配的服务器
      ids = by_isp_nodes(node_type, domestic, ip_area)
      node_ids = node_ids & ids if ids.size > 0

      # 获取当前ip解析省份，城市匹配该区域的服务器
      ids = by_diversion_nodes(ip_province, ip_city, node_type)
      node_ids = node_ids & ids if ids.size > 0

      list = list.select{|k,v| node_ids.include?(k.to_i)}
      # 在线人数最少的node_id
      node_id = list.sort_by{|k,v| v.to_i}&.first&.first
      node = Node.find_by(id: node_id)
    else
      node = Node.select_smart_node_from_mysql(node_type, domestic, ip_region)
      # 将所有mysql目前启用的一级代理的在线人数写入缓存
      Node.enabled.level1.ismart.each do |n|
        n.record_online_users!(n.connections_count)
      end
    end
    node.increase_online_users! if node.present?
    node
  end

  def self.select_smart_node_from_mysql(node_type, is_domestic, ip_region)
    ip_area = ip_region[:area]
    ip_province = ip_region[:province]
    ip_city = ip_region[:city]

    nodes = node_type.nodes.ismart.level1
    # 获取当前运营商下的代理商匹配node_ids
    isp_node_ids = nodes.by_isp_nodes(node_type, is_domestic, ip_area)
    nodes = nodes.where(id: isp_node_ids) if isp_node_ids.present?

    # 获取当前ip省份，城市下匹配地域node ids
    diversion_node_ids = nodes.by_diversion_nodes(ip_province, ip_city, node_type)
    nodes = nodes.where(id: diversion_node_ids) if diversion_node_ids.present?

    # 如果是国内用户，则获取国内的节点，否则获得国外的节点
    node = if is_domestic
      nodes.domestic.enabled.sorted.take
    else
      nodes.oversea.enabled.sorted.take
    end
    node
  end

  # 选择定制专线在线人数最少的服务器，如果redis中没数据，则从mysql中选择
  # 定制专线可分配coverrd、customization类型服务器
  def self.select_customize_node(node_type, node_country_id, node_region_id)
    list = customize_cache_list_by_node_type(node_type)
    if list.present?
      node_ids = list.keys.map(&:to_i)

      ids = by_out_region(node_type, node_country_id, node_region_id)
      node_ids = node_ids & ids

      list = list.select{|k,v| node_ids.include?(k.to_i)}
      # 在线人数最少的node_id
      node_id = list.sort_by{|k,v| v.to_i}&.first&.first
      node = Node.find_by(id: node_id)
    else
      node = Node.select_customize_node_from_mysql(node_type, node_country_id, node_region_id)
      # 将所有mysql目前启用的一级代理的在线人数写入缓存
      Node.enabled.level1.icustomization.each do |n|
        n.record_online_users!(n.connections_count)
      end
    end
    node.increase_online_users! if node.present?
    node
  end

  def self.select_customize_node_from_mysql(node_type, node_country_id, node_region_id)
    nodes = node_type.nodes.enabled.icustomization.level1

    out_region_nodes_ids = nodes.by_out_region(node_type, node_country_id, node_region_id)
    nodes = nodes.where(id: out_region_nodes_ids) if out_region_nodes_ids.present?
    nodes.sorted.take
  end

  # 根据出口地域获得所属一级代理服务器
  def self.by_out_region(node_type, node_country_id, node_region_id)

    cache_key = "#{CACHE_KEY_NODES_OUT_REGION}#{node_type.id}_#{node_country_id}_#{node_region_id}"
    Rails.cache.fetch(cache_key, expires_in: 30.days) do
      ids = NodeRegion.enabled
      .the_where(
        node_country_id: node_country_id,
        id: node_region_id
      )
      .pluck(:id)

      node_type.nodes.enabled
        .icustomization
        .level1
        .where(node_region_out_id: ids)
        .pluck(:id)
    end
  end

  # 根据ip解析运营商筛选节点
  def self.by_isp_nodes(node_type, domestic, ip_area)

    return [] if ip_area.blank? || NET_TYPES[ip_area.to_sym].blank?

    cache_key = "#{CACHE_KEY_NODES_ISP}#{node_type.id}_#{domestic}_#{ip_area}"
    Rails.cache.fetch(cache_key, expires_in: 30.days) do
      tag = NET_TYPES[ip_area.to_sym]
      isp_ids = NodeIcp.enabled
        .find_by(tag: tag)
        &.sisps

      return [] if isp_ids.blank?

      node_type.nodes.enabled
      .ismart
      .level1
      .where(node_isp_id: isp_ids, is_domestic: domestic)
      .pluck(:id)
    end
  end

  # 根据ip解析省市筛选节点
  def self.by_diversion_nodes(ip_province, ip_city, node_type)

    cache_key = "#{CACHE_KEY_NODES_DIVERSION}#{ip_province}_#{ip_city}_#{node_type.id}"
    Rails.cache.fetch(cache_key, expires_in: 30.days) do
      region_ids = NodeDiversion.enabled
      .the_where(
        province: ip_province,
        city: ip_city,
        node_type_id: node_type.id
      )
      .pluck(:node_region_id)

      node_type.nodes.enabled
        .icustomization
        .level1
        .where(node_region_id: region_ids)
        .pluck(:id)
    end
  end

  # 获取二级代理线路列表
  def self.cache_customize_lines
    Rails.cache.fetch(CACHE_KEY_NODES_LINE_LIST, expires_in: 30.days)do
      region_out_ids = Node
        .enabled
        .icustomization
        .level1
        .pluck(:node_region_out_id)
        .uniq
        .compact

      regions = NodeRegion
        .joins(:node_type, :node_continent, :node_country)
        .where("node_regions.id in (?)", region_out_ids)
        .select("node_areas.id as node_continent_id, node_areas.sequence as continent_sequence," \
          "node_areas.name as node_continent_name," \
          "node_countries_node_regions.id as node_country_id, node_countries_node_regions.sequence as country_sequence," \
          "node_countries_node_regions.name as node_country_name," \
          "node_countries_node_regions.abbr as abbr," \
          "node_regions.id as node_region_id, node_regions.name as node_region_name, node_regions.sequence as region_sequence," \
          "node_types.id as node_type_id, node_types.name as node_type_name")
        .order("node_regions.node_type_id ASC, node_areas.sequence ASC, node_countries_node_regions.sequence ASC," \
            "node_regions.sequence ASC")

      names = [
        ['node_type_id', 'node_type_name', nil, 'node_continents'],
        ['node_continent_id', 'node_continent_name', 'continent_sequence', 'node_countries'],
        ['node_country_id', 'node_country_name', 'country_sequence', 'node_regions', "abbr"],
        ['node_region_id', 'node_region_name', 'region_sequence', nil, "abbr"]
      ]

      tree = -> (level, t, list = []) {
        return if level >= 4
        id, name, sequence, key, abbr = names[level]
        parent_level1, _, _, _ = names[level-1] if level >= 1
        parent_level2, _, _, _ = names[level-2] if level >= 2
        parent_level3, _, _, _ = names[level-3] if level >= 3
        t.group_by{|r| [r[id], r[name], r[sequence], r[abbr]]}.each do |k, v|
          id, name, sequence, abbr = k; t = v
          list << {
            id: id, name: name, abbr: abbr, prority: sequence,
            "#{parent_level1}": v.first[parent_level1],
            "#{parent_level2}": v.first[parent_level2],
            "#{parent_level3}": v.first[parent_level3],
            "#{key}": tree.call(level+1, t)
          }
        end
        list
      }

      list = tree.call(0, regions)
      delete_by_key = -> (obj, key) {
        case obj
        when Hash
          obj.each_value { |e| delete_by_key.call(e, key) }
        when Array
          obj.map do |e|
            e.delete(key) if e.is_a?(Hash) && e.has_key?(key) && e[key].size == 1
          end
          obj.each { |e| delete_by_key.call(e, key) }
        end
      }
      list = delete_by_key.call(list, :node_regions)
    end
  end

  def is_level1?
    self.level == 1
  end

  def is_overload?
    self.connections_count >= (self.max_connections_count || 0) && self.max_connections_count > 0
  end

  def user_group_name
    self.user_group_id.blank? ? '全部' : self.user_group.name
  end

  # 停用连接百分比达到80以上的节点
  def self.check_overload!
    overload_nodes = Node.overload.enabled
    overload_nodes.update_all(status: :disabled) if overload_nodes.present?
    unoverload_nodes = Node.unoverload.disabled
    unoverload_nodes = Node.update_all(status: :enabled) if unoverload_nodes.present?
    true
  end

  # 指定服务类型的服务器数
  def self.nodes_count(node_type_id)
    cache_key = "#{CACHE_KEY_NODES_COUNT}#{node_type_id}"
    result = $redis.scard(cache_key)
    if result <= 0
      nodes = Node.enabled_disabled.where(node_type_id: node_type_id).level1.pluck(:id)
      nodes.each do |id|
        $redis.sadd(cache_key, id)
      end
      nodes.length
    else
      result
    end
  end

  def self.nodes_enabled_count(node_type_id)
    cache_key = "#{CACHE_KEY_NODES_ENABLED_COUNT}#{node_type_id}"
    result = $redis.scard(cache_key)
    if result <= 0
      nodes = Node.where(node_type_id: node_type_id).level1.enabled.pluck(:id)
      nodes.each do |id|
        $redis.sadd(cache_key, id)
      end
      nodes.length
    else
      result
    end
  end

  def self.nodes_disabled_count(node_type_id)
    NodeType.nodes_count(node_type_id) - NodeType.nodes_enabled_count(node_type_id)
  end

  # 从所有服务类型中移除
  def self.remove_from_nodes_status(node_id)
    node_type_ids = NodeType.cache_list.map(&:id)
    node_type_ids.each do |id|
      $redis.srem("#{CACHE_KEY_NODES_COUNT}#{id}", node_id)
      $redis.srem("#{CACHE_KEY_NODES_ENABLED_COUNT}#{id}", node_id)
    end
  end

  # 重置服务器状态缓存
  def self.reset_nodes_status
    node_type_ids = NodeType.cache_list.map(&:id)
    node_type_ids.each do |id|
      $redis.del("#{CACHE_KEY_NODES_COUNT}#{id}")
      $redis.del("#{CACHE_KEY_NODES_ENABLED_COUNT}#{id}")
    end
  end

  # 重置服务器在线人数缓存
  def self.reset_node_online_cache!
    node_type_ids = NodeType.cache_list.map(&:id)
    node_type_ids.each do |id|
      Node.types.each do |k, v|
        $redis.del("#{CACHE_KEY_ONLINE_USERS}#{v}_#{id}_1")
        $redis.del("#{CACHE_KEY_ONLINE_USERS}#{v}_#{id}_0")
      end
    end
  end

  # 从缓存获取可为硬核智连分配服务器列表
  def self.smart_cache_list_by_node_type(node_type, domestic)
    list = {}
    Node.types.except(:customization).each do |_, type|
      # 根据服务器类型、服务类型、服务器所属国内外从缓存得到服务器列表
      list.merge! $redis.hgetall("#{CACHE_KEY_ONLINE_USERS}#{type}_#{node_type.id}_#{domestic}")
    end
    list
  end

  # 从缓存获取可为定制专线分配服务器列表
  def self.customize_cache_list_by_node_type(node_type)
    list = {}
    Node.types.except(:smart).each do |_, type|
      [0, 1].each do |domestic| # 0，1是否海内外
        list.merge! $redis.hgetall("#{CACHE_KEY_ONLINE_USERS}#{type}_#{node_type.id}_#{domestic}")
      end
    end
    list
  end

  private

  def after_commit_trace
    NodeAddition.create(node_id: self.id)
    self.record_online_users!(self.connections_count)

    # 增加到节点总数缓存
    if self.is_level1? && (self.enabled? || self.disabled?)
      $redis.sadd("#{CACHE_KEY_NODES_COUNT}#{self.node_type_id}", self.id)
    end

    # 如果是启用的节点，也加入启用缓存中
    if self.is_level1? && self.enabled?
      $redis.sadd("#{CACHE_KEY_NODES_ENABLED_COUNT}#{self.node_type_id}", self.id)
    end
  end

  def after_commit_trace_update
    # 从所有服务类型中移除
    Node.remove_from_nodes_status(self.id)
    $redis.sadd("#{CACHE_KEY_NODES_COUNT}#{self.node_type_id}", self.id)
    # 如果是启用的节点，也加入启用缓存中
    if self.is_level1? && self.enabled?
      $redis.sadd("#{CACHE_KEY_NODES_ENABLED_COUNT}#{self.node_type_id}", self.id)
    end
  end

  def before_update_trace
    if self.changed?
      # 如果节点被设置禁用，则移除在线用户数缓存
      domestic_was = self.is_domestic_was ? 1 : 0
      node_was = Node.types.fetch(self.types_was)
      $redis.hdel("#{CACHE_KEY_ONLINE_USERS}#{node_was}_#{self.node_type_id_was}_#{domestic_was}", self.id)
      # 只有一级代理和已开启的服务器才会记录在线人数
      if self.is_level1? && self.enabled?
        # 添加最新更改后的信息
        self.record_online_users!(self.connections_count)
      end
    end
  end

  # 回调删除缓存
  def delete_cache
    Rails.cache.delete(CACHE_KEY_NODES_LINE_LIST)
    Rails.cache.delete_matched("#{CACHE_KEY_NODES_ISP}*")
    Rails.cache.delete_matched("#{CACHE_KEY_NODES_OUT_REGION}*")
    Rails.cache.delete_matched("#{CACHE_KEY_NODES_DIVERSION}*")
    Rails.cache.delete_matched("#{NodeCountry::CACHE_KEY_CONNECTION}*")
  end

end

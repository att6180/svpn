class NavigationAd < ApplicationRecord

  CACHE_KEY = "navigation_ads"

  validates :priority, :name, presence: true

  scope :sorted, -> { order(priority: :asc) }
  scope :enabled, -> { where(is_enabled: true) }

  after_commit :after_commit_trace

  def self.cache_list
    Rails.cache.fetch(CACHE_KEY, expires_in: 30.days) do
      NavigationAd.enabled.sorted.to_a
    end
  end

  private

  def after_commit_trace
    Setting.navigation_ad_update_at = Time.now.to_i
    Rails.cache.delete(CACHE_KEY)
  end

end

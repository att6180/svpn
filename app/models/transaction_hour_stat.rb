class TransactionHourStat < ApplicationLogRecord

  scope :by_date, -> (date) { where(stat_date: date) }
  scope :sorted, -> { order(stat_date: :asc, stat_hour: :asc)}
  scope :group_by_hour, -> { group(:stat_hour) }
  scope :select_sum_price, -> { select("SUM(total_recharge_amount) as total_recharge_amount") }
  scope :select_sum_users, -> { select("SUM(recharge_users_count) as recharge_users_count") }

  # 统计昨天每小时的充值总额、充值人数
  def self.stat(date = nil)
    yesterday = date || Time.now.to_date.yesterday

    stats = []
    items = []
    hour_range = *(0..23)
    stat_range = []

    TransactionHourStat.using_db(DB_LOG_SLAVE1) do
      # 全部
      items = TransactionLog.by_date(yesterday)
        .by_paid
        .without_iap
        .select_sum_price
        .select_distinct_users
        .group_by_hour
        .use_index_created_at_status
    end

    items.each do |item|
      stat_range << item.h
      stats << TransactionHourStat.new(
        stat_date: yesterday,
        stat_hour: item.h,
        total_recharge_amount: item.total_price,
        recharge_users_count: item.users_count
      )
    end
    # 检查缺失的小时段，并补充
    fill_range = hour_range - stat_range
    if fill_range.length > 0
      fill_range.each do |h|
        stats << TransactionHourStat.new(
          stat_date: yesterday,
          stat_hour: h,
          total_recharge_amount: 0,
          recharge_users_count: 0
        )
      end
    end

    TransactionHourStat.using_db(DB_LOG_SLAVE1) do
      # 渠道
      items = TransactionLog.by_date(yesterday)
        .by_paid
        .without_iap
        .select_sum_price
        .select_distinct_users
        .group_by_hour
        .use_index_created_at_status
        .select(:app_channel)
        .group(:app_channel)
    end

    # 检查每个渠道中缺失的小时段并补充
    channel_list = items.map{|item| item.app_channel}.uniq
    channel_hours = {}
    channel_list.each do |channel|
      channel_hours[channel] = []
    end

    if items.present?
      items.each do |item|
        channel_hours[item.app_channel] << item.h
        stats << TransactionHourStat.new(
          stat_date: yesterday,
          stat_hour: item.h,
          app_channel: item.app_channel,
          total_recharge_amount: item.total_price,
          recharge_users_count: item.users_count,
          filter_type: 1
        )
      end
    end
    fill_channel_range = {}
    channel_hours.each do |channel, hours|
      fill_channel_range[channel] = hour_range - hours
    end
    fill_channel_range.each do |channel, hours|
      hours.each do |h|
        stats << TransactionHourStat.new(
          stat_date: yesterday,
          stat_hour: h,
          app_channel: channel,
          total_recharge_amount: 0,
          recharge_users_count: 0,
          filter_type: 1
        )
      end if hours.is_a?(Array) && hours.length > 0
    end

    TransactionHourStat.using_db(DB_LOG_SLAVE1) do
      # 版本
      items = TransactionLog.by_date(yesterday)
        .by_paid
        .without_iap
        .select_sum_price
        .select_distinct_users
        .group_by_hour
        .use_index_created_at_status
        .select(:app_version)
        .group(:app_version)
    end
    # 检查每个版本中缺失的小时段并补充
    version_list = items.map{|item| item.app_version}.uniq
    version_hours = {}
    version_list.each do |version|
      version_hours[version] = []
    end

    if items.present?
      items.each do |item|
        version_hours[item.app_version] << item.h
        stats << TransactionHourStat.new(
          stat_date: yesterday,
          stat_hour: item.h,
          app_version: item.app_version,
          total_recharge_amount: item.total_price,
          recharge_users_count: item.users_count,
          filter_type: 1
        )
      end
    end
    fill_version_range = {}
    version_hours.each do |version, hours|
      fill_version_range[version] = hour_range - hours
    end
    fill_version_range.each do |version, hours|
      hours.each do |h|
        stats << TransactionHourStat.new(
          stat_date: yesterday,
          stat_hour: h,
          app_channel: version,
          total_recharge_amount: 0,
          recharge_users_count: 0,
          filter_type: 1
        )
      end if hours.is_a?(Array) && hours.length > 0
    end

    TransactionHourStat.using_db(DB_LOG_SLAVE1) do
      # 渠道和版本
      items = TransactionLog.by_date(yesterday)
        .by_paid
        .without_iap
        .select_sum_price
        .select_distinct_users
        .group_by_hour
        .use_index_created_at_status
        .select(:app_channel, :app_version)
        .group(:app_channel, :app_version)
    end
    # 检查每个渠道版本中缺失的小时段并补充
    cv_list = items.map{|item| "#{item.app_channel}___#{item.app_version}" }.uniq
    cv_hours = {}
    cv_list.each do |cv|
      cv_hours[cv] = []
    end

    if items.present?
      items.each do |item|
        cv_hours["#{item.app_channel}___#{item.app_version}"] << item.h
        stats << TransactionHourStat.new(
          stat_date: yesterday,
          stat_hour: item.h,
          app_channel: item.app_channel,
          app_version: item.app_version,
          total_recharge_amount: item.total_price,
          recharge_users_count: item.users_count,
          filter_type: 2
        )
      end
    end
    fill_cv_range = {}
    cv_hours.each do |cv, hours|
      fill_cv_range[cv] = hour_range - hours
    end
    fill_cv_range.each do |cv, hours|
      hours.each do |h|
        cv_array = cv.split('___')
        stats << TransactionHourStat.new(
          stat_date: yesterday,
          stat_hour: h,
          app_channel: cv_array[0],
          app_version: cv_array[1],
          total_recharge_amount: 0,
          recharge_users_count: 0,
          filter_type: 2
        )
      end if hours.is_a?(Array) && hours.length > 0
    end

    TransactionHourStat.bulk_insert(stats)
  end
end

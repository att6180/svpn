class V2::PlanClientTab < ApplicationRecord

  CACHE_KEY = "v2_plan_client_tabs"

  scope :enabled, -> { where(status: 1) }

  after_commit :after_commit_trace

  def self.cache_list
    Rails.cache.fetch(CACHE_KEY, expires_in: 7.days) do
      V2::PlanClientTab.enabled.to_a
    end
  end

  private

  def after_commit_trace
    Rails.cache.delete(CACHE_KEY)
    # 删除针对套餐模板的套餐缓存
    Rails.cache.delete_matched("#{::V2::Plan::CACHE_KEY}tp_*")
  end

end

class V2::Notice < ApplicationRecord

  CACHE_KEY_PREFIX = "user_notices_"
  CACHE_KEY_USER_NOTICES = "user_notices"

  enum notice_type: { notice: 1, marquee: 2, alert: 3 }
  enum open_type: { nothing: 1, webview: 2, browser: 3, inapp: 4 }

  #validates :platform, inclusion: { in: %w(all ios android pc mac robot plug_in) }
  validates :notice_type, :open_type, :description, presence: true

  scope :by_platform, ->(platform) { where(platform: ['all', platform]) }
  scope :by_app_channel, ->(channel) { where(app_channel: ['all', channel]) }
  scope :by_app_version, ->(version) { where(app_version: ['all', version]) }
  scope :select_for_client, -> { select(:user_names, :notice_type, :open_type, :button_text, :link_url, :alert_text, :marquee_text) }

  after_commit :after_commit_trace

  def self.by_platform_and_app_version(platform, app_channel, app_version)
    # 关于按平台、版本、版本号获取键值的规则
    # 是利用各个列的值的排序转hash，然后排在后面的键值覆盖前面的键值实现的
    # 所以这三个列的值在取名时，不要取排在 all 或 '' 的前面的值
    # 排序根据平台、渠道、版本、版本号正序排列，后台的同名配置会覆盖前面的配置
    cache_key = "#{CACHE_KEY_PREFIX}#{platform}_#{app_channel}_#{app_version}"
    result = Rails.cache.fetch(cache_key, expires_in: 30.days) do
      notices = V2::Notice.by_platform(platform)
        .by_app_channel(app_channel)
        .by_app_version(app_version)
        .select_for_client
        .order(platform: :asc, app_channel: :asc, app_version: :asc)
        .to_a
      group_notices = notices.as_json(except: ["id", "user_names"])
        .group_by{|k| k["notice_type"]}
        .inject({}) { |h, (k, v)| h[k] = [v.last]; h }
      group_notices
    end
  end

  # 根据用户名称，平台版本渠道匹配公告
  # 如果该用户没有指定公告，则直接返回版本公告
  # 如果该用户有指定公告，返回用户指定公告
  # 如果该用户公告类型缺失，则到版本公告里获取缺失公告
  def self.by_specific_user_and_app_version(username, platform, app_channel, app_version)
    user_notices = RedisCache.hget_json(CACHE_KEY_USER_NOTICES, username)
    version_notices = by_platform_and_app_version(platform, app_channel, app_version)
    # 如果该用户没有指定公告，直接返回版本渠道配置的公告
    return deep_replace_key(version_notices) if user_notices.blank?
    unkeys = user_notices.keys.map(&:to_s)
    nkeys = V2::Notice.notice_types.keys
    # 取用户公告跟版本渠道公告的差集
    diffkeys = nkeys - unkeys
    diff_notices = version_notices.select{|k,v| diffkeys.include?(k)}
    deep_replace_key(user_notices.merge(diff_notices))
  end

  private

  def self.deep_replace_key(hash)
    hash = hash.deep_symbolize_keys
    # 把hash里的enum相关值替换为原始数字
    hash.each_value{|v|
      v = v.first
      v.update(
        notice_type: V2::Notice.notice_types[v[:notice_type]],
        open_type: V2::Notice.open_types[v[:open_type]]
      )
    }
    hash
  end

  # 重新为指定用户生成新的公告缓存
  def renew_user_notices_cache
    # 获得所有指定了用户的公告
    notices = V2::Notice.where.not(user_names: nil).select_for_client
    # 获得指定用户公告里所有的用户id合并去重
    user_names = notices.pluck(:user_names).map{|u| u.split(/[\s,]+/)}.flatten.uniq
    user_names.each do |username|
      group_notices = notices.select{ |notice| notice[:user_names].split(/[\s,]+/).include?(username) }
        .as_json(except: ["id", "user_names"])
        .group_by{|k| k["notice_type"]}
      RedisCache.hset_json(CACHE_KEY_USER_NOTICES, username, group_notices.to_json)
    end
  end

  def after_commit_trace
    Rails.cache.delete(CACHE_KEY_USER_NOTICES)
    Rails.cache.delete_matched("#{CACHE_KEY_PREFIX}*")
    renew_user_notices_cache
  end
end

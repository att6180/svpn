class V2::PlanVersion < ApplicationRecord

  CACHE_KEY = "v2_plan_version_"

  scope :by_channel_version, -> (channel, version) {
    where(channel: [channel, 'all'])
      .where(version: [version, 'all'])
  }
  scope :enabled, -> { where(status: 1) }

  validate :check_version, on: [:create, :update]

  after_commit :clear_cache

  def self.by_version(channel, version)
    Rails.cache.fetch("#{CACHE_KEY}#{channel}_#{version}", expires_in: 7.days) do
      V2::PlanVersion.by_channel_version(channel, version)
        .order(channel: :desc, version: :desc)
        .enabled
        .first
    end
  end

  private

  def check_version
    if self.channel == "all" || self.version == "all"
      errors.add(:channel, '渠道和版本必须全部为all或者全部都不为all') if self.channel != self.version
    end
  end

  def clear_cache
    # 清除缓存
    Rails.cache.delete_matched("#{CACHE_KEY}*")
  end

end

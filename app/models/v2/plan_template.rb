# 第二版套餐 - 套餐模板
class V2::PlanTemplate < ApplicationRecord

  scope :enabled, -> { where(status: 1)}
end

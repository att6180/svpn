class V2::Plan < ApplicationRecord

  CACHE_KEY = "v2_plan_"

  enum time_type: ::Plan::TIME_TYPES

  belongs_to :template, class_name: "V2::PlanTemplate", foreign_key: 'template_id'
  belongs_to :client_tab, class_name: "V2::PlanClientTab", foreign_key: 'client_tab_id'
  belongs_to :node_type, optional: true

  scope :sorted, -> { order(status: :desc, id: :asc)}
  scope :enabled, -> { where(status: 1) }

  validates :template_id, :client_tab_id, :currency_symbol, :name, :price, presence: true
  validates :time_type, :node_type_id, presence: true, if: Proc.new {|p| p.is_regular_time?}
  validates :coins, presence: true, if: Proc.new {|p| !p.is_regular_time?}

  after_commit :after_commit_trace
  before_update :before_update_trace

  def is_enabled?
    self.status == 1
  end

  # 根据套餐模版ID查找套餐
  # 并根据提供的排序规则，按客户端标签ID分组并排序
  def self.by_template_id(template_id, tab_id_sort)
    sort_key = tab_id_sort.gsub(",", "_")
    # 此缓存除了在本类的after_commit中有删除操作外
    # V2::PlanClientTab的after_commit中也有删除操作
    Rails.cache.fetch("#{CACHE_KEY}tp_#{template_id}_#{sort_key}", expires_in: 7.days) do
      plans = V2::Plan.where(template_id: template_id).enabled
      group_plans = plans.group_by(&:client_tab_id)
      # 取出包含的客户端标签
      client_tab_ids = plans.map(&:client_tab_id)
      client_tabs = V2::PlanClientTab.where(id: client_tab_ids)
        .enabled
        .order("FIELD(id, #{tab_id_sort})")
      # 按客户端标签，将套餐一起放进数组内
      result = []
      client_tabs.each do |tab|
        result << {
          tab: tab,
          plans: group_plans[tab.id].sort_by(&:sort_weight)
        }
      end
      result
    end
  end

  # 创建订单并为指定用户应用套餐用于IAP支付
  # 这里是第二版的充值系统使用的IAP支付方法
  # 这个方法就不关系订单和套餐的id了
  def payment(user, payment_method, transaction_id, app_channel, app_version)
    transaction_log = nil
    ActiveRecord::Base.transaction do
      # 如果是钻石类套餐
      if !self.is_regular_time?
        # 为用户累加钻石和金额
        user.total_payment_amount = user.total_payment_amount + self.price
        user.current_coins = user.current_coins + self.coins + self.present_coins
        # 赠送的钻石不记入累计钻石中
        user.total_coins = user.total_coins + self.coins
        # 累计赠送钻石
        user.present_coins = user.present_coins + self.present_coins
        # 根据用户累计钻石数判断用户是否达到升级用户类型的要求
        user_group = UserGroup.find_by_coins(user.total_coins)
        # 如果用户当前等级小于此等级，则升级到此等级
        if user_group.present? && user.user_group.level < user_group.level
          user.set_group(user_group)
        end
        user.save! if user.changed?
      else
        # 为用户累加消费金额
        user.total_payment_amount = user.total_payment_amount + self.price
        user.current_coins = user.current_coins + self.coins + self.present_coins
        user.total_coins = user.total_coins + self.coins
        user.present_coins = user.present_coins + self.present_coins
        if user.total_coins > 0
          user_group = UserGroup.find_by_coins(user.total_coins)
          if user_group.present? && user.user_group.level < user_group.level
            user.set_group(user_group)
          end
        end
        user.save! if user.changed?

        node_type = self.node_type
        # 为用户类加过期时间
        user_node_type = UserNodeType.find_by(user_id: user.id, node_type_id: node_type.id)
        # 如果当前服务类型未到期，则基于服务类型过期时间累加，否则基于当前时间累加
        base_time = user_node_type.expired_at > Time.now ? user_node_type.expired_at : Time.now
        bought_time = DateUtils.increase_expire_time(base_time, self.time_type)
        # 累加过期时间
        user_node_type.update!(expired_at: bought_time)
      end
      # 写入交易日志
      transaction_log = user.transaction_logs.create!(
        plan_id: self.id,
        plan_name: self.name,
        price: self.price,
        coins: self.coins + self.present_coins,
        plan_coins: self.coins,
        present_coins: self.present_coins,
        payment_method: payment_method,
        platform_transaction_id: transaction_id,
        app_channel: app_channel,
        app_version: app_version,
        status: TransactionLog::STATUS_PAID,
        processed_at: Time.now,
        is_regular_time: self.is_regular_time,
        time_type: self.time_type,
        node_type_id: self.node_type_id,
        user_created_at: user.created_at
      )
    end
    transaction_log
  end

  private

  def after_commit_trace
    Rails.cache.delete_matched("#{CACHE_KEY}tp_#{self.template_id}_*")
  end

  def before_update_trace
    # 清除属于之前模版的缓存
    if self.template_id_was != self.template_id
      Rails.cache.delete_matched("#{CACHE_KEY}tp_#{self.template_id_was}_*")
    end
  end

end

class UserConnectionRegionStat < ApplicationLogRecord
  include Uuidable

  def self.stat_by_domain(domain)
    UserConnectionRegionStat
      .select("ip_country, ip_province, ip_city, COUNT(user_id) as users_count, SUM(visits_count) as total_visits_count")
      .where(domain: domain)
      .group(:ip_country, :ip_province, :ip_city)
  end

  # 获得每个国家访问指定域名的详情
  def self.stat_country_by_domain(domain)
    UserConnectionRegionStat
      .select("ip_country, COUNT(user_id) as users_count")
      .where(domain: domain)
      .group(:ip_country)
  end

  # 统计域名区域
  def self.stat
    stats = []

    items = UserConnectionLog
      .select("user_id, top_level_domain, ip_country, ip_province, ip_city, COUNT(*) as visits_count")
      .group(:user_id, :top_level_domain, :ip_country, :ip_province, :ip_city)

    if items.present?
      items.each do |item|
        stats << UserConnectionRegionStat.new(
          id: Utils::Gen.generate_uuid,
          user_id: item.user_id,
          domain: item.top_level_domain,
          ip_country: item.ip_country,
          ip_province: item.ip_province,
          ip_city: item.ip_city,
          visits_count: item.visits_count
        )
      end
    end
    UserConnectionRegionStat.bulk_insert(stats, use_provided_primary_key: true) if stats.present?
  end

end

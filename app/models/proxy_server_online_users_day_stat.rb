class ProxyServerOnlineUsersDayStat < ApplicationLogRecord

  scope :between_date, ->(start_at, end_at) { where(stat_date: start_at..end_at)}

  # 统计前一天的在线人数历史记录
  def self.stat(date = nil)
    yesterday = date || Time.now.to_date.yesterday

    items = ProxyServerOnlineUsersLog.by_date(yesterday).group_by_hour_minute
    clear_items = JSON.parse(items.to_json(except: :id))
    min_item = clear_items.min { |a, b| a["online_count"] <=> b["online_count"] }
    if min_item.present?
      min_count = min_item["online_count"]
      min_time = Time.parse("#{yesterday.to_s} #{min_item["hm"]}")
    else
      min_count = 0
      min_time = nil
    end
    max_item = clear_items.max { |a, b| a["online_count"] <=> b["online_count"] }
    if max_item.present?
      max_count = max_item["online_count"]
      max_time = Time.parse("#{yesterday.to_s} #{max_item["hm"]}")
    else
      max_count = 0
      max_time = nil
    end

    online_count_list = clear_items.map{|item| item["online_count"]}
    average_count = ("%.1f" % (online_count_list.sum / online_count_list.count.to_f)).to_i

    # 统计服务类型
    node_type_info = node_type_stat(yesterday).to_json

    ProxyServerOnlineUsersDayStat.create(
      stat_date: yesterday,
      min_count: min_count,
      min_time: min_time,
      max_count: max_count,
      max_time: max_time,
      average_count: average_count,
      node_type_info: node_type_info
    )
  end

  # 统计每个服务类型的最高和最低在线人及时间
  def self.node_type_stat(yesterday)
    node_types = NodeType.cache_list
    node_type_info = Hash.new{|k,v| k[v] = {}}
    node_types.each do |node_type|
      items = ProxyServerOnlineUsersLog.where(node_type_id: node_type.id)
        .by_date(yesterday).group_by_hour_minute
      clear_items = JSON.parse(items.to_json(except: :id))
      min_item = clear_items.min { |a, b| a["online_count"] <=> b["online_count"] }
      if min_item.present?
        min_count = min_item["online_count"]
        min_time = Time.parse("#{yesterday.to_s} #{min_item["hm"]}")
      else
        min_count = 0
        min_time = nil
      end
      max_item = clear_items.max { |a, b| a["online_count"] <=> b["online_count"] }
      if max_item.present?
        max_count = max_item["online_count"]
        max_time = Time.parse("#{yesterday.to_s} #{max_item["hm"]}")
      else
        max_count = 0
        max_time = nil
      end
      online_count_list = clear_items.map{|item| item["online_count"]}
      average_count = ("%.1f" % (online_count_list.sum / online_count_list.count.to_f)).to_i
      node_type_info[node_type.id]['min_count'] = min_count
      node_type_info[node_type.id]['min_time'] = min_time
      node_type_info[node_type.id]['max_count'] = max_count
      node_type_info[node_type.id]['max_time'] = max_time
      node_type_info[node_type.id]['average_count'] = average_count
      node_type_info
    end
    node_type_info
  end

end

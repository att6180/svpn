class SplashScreenAd < ApplicationRecord

  CACHE_KEY_SELECTED_COUNT = "splash_screen_ads_selected_count"
  CACHE_KEY_IMPORESSIONS_COUNT = "splash_screen_ads_imporessions_count"
  CACHE_KEY_CLICK_COUNT = "splash_screen_ads_click_count"

  enum ad_type: { image: 1, gif: 2, video: 3 }
  enum open_link_type: { webview: 1, browser: 2 }

  scope :enabled, -> { where(is_enabled: true)}
  scope :by_app_channel, ->(channel) { where(app_channel: ['all', channel]) }
  scope :by_app_version, ->(version) { where(app_version: ['all', version]) }

  def self.pick_one(app_channel, app_version)
    now = Time.now
    now_date = now.to_date
    now_time = now.strftime("%H:%M:%S")
    if Setting.splash_screen_ad_enabled == "true"
      ad = SplashScreenAd.by_app_channel(app_channel)
        .by_app_version(app_version)
        .where(
          "((time_priority = 1 AND start_at <= ? AND end_at >= ?) OR " + \
          "(time_priority = 2 AND date(start_at) <= ? AND date(end_at) >= ?) OR " + \
          "(time_priority = 3 AND time(start_at) <= ? AND time(end_at) >= ?) OR " + \
          "time_priority = 4) AND is_enabled = 1",
          now, now, now_date, now_date, now_time, now_time
        ).order(app_channel: :desc, app_version: :desc, time_priority: :asc).first
    else
      ad = nil
    end
    SplashScreenAd.increase_selected_count(ad.id) if ad.present?
    ad
  end

  def self.increase_selected_count(id)
    $redis.hincrby(CACHE_KEY_SELECTED_COUNT, id, 1)
  end

  def self.get_selected_count(id)
    $redis.hget(CACHE_KEY_SELECTED_COUNT, id).to_i
  end

  # 开屏广告次数
  def self.increase_imporessions_count(id)
    $redis.hincrby(CACHE_KEY_IMPORESSIONS_COUNT, id, 1)
  end

  def self.get_imporessions_count(id)
    $redis.hget(CACHE_KEY_IMPORESSIONS_COUNT, id).to_i
  end

  # 弹出广告次数
  def self.increase_click_count(id)
    $redis.hincrby(CACHE_KEY_CLICK_COUNT, id, 1)
  end

  def self.get_click_count(id)
    $redis.hget(CACHE_KEY_CLICK_COUNT, id).to_i
  end

end

class NodeDiversion < ApplicationRecord

  belongs_to :node_type
  belongs_to :node_region

  validates :province, :node_region_id, :node_type_id, presence: true

  scope :enabled, -> { where(is_enabled: true) }

  after_commit :after_commit_trace

  private

  def after_commit_trace
    Rails.cache.delete_matched("#{Node::CACHE_KEY_NODES_DIVERSION}*")
  end
end

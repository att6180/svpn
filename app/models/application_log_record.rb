class ApplicationLogRecord < ActiveRecord::Base
  self.abstract_class = true

  @@connection_config = DB_LOG
  establish_connection @@connection_config

  scope :recent, -> { order(created_at: :desc) }
  scope :exclude_ids, ->(ids) { where.not(id: ids.map(&:to_i)) }
  scope :by_week, -> { where('created_at > ?', 7.days.ago.utc) }
  scope :order_by_created_at, ->(t) { order(created_at: t == 'asc' ? :asc : :desc) }

  # slave故障临时切换回主库
  def self.using_db(config)
   return nil if !defined?(config)
   result = nil
   if block_given?
     result = yield
   end
   result
  end

  #def self.using_db(config)
    #return nil if !defined?(config)
    #result = nil
    #@@connection_config = config
    #establish_connection @@connection_config
    #if block_given?
      #begin
        #result = yield
      #rescue => e
        #raise e
      #ensure
        #@@connection_config = DB_LOG
        #establish_connection @@connection_config
      #end
    #end
    #result
  #end

end

class UserConnectionSecondYearStat < ApplicationLogRecord
  include Uuidable

  scope :between_date, -> (start_at, end_at) { where("stat_year >= ? AND stat_year <= ?", start_at, end_at) }

  # 根据用户访问日志一级年表(user_connection_first_year_stats)统计
  def self.stat
    stat_year = Time.now.last_year.year
    logs = UserConnectionSecondYearStat.eager_stat(stat_year, 1, 50)
    if logs.total_pages > 0
      (1..logs.total_pages).each do |index|
        logs = UserConnectionSecondYearStat.eager_stat(stat_year, index, 50)
        stats = []
        logs.each do |log|
          stat = UserConnectionSecondYearStat.new(
            id: Utils::Gen.generate_uuid,
            domain: log.domain,
            visit_users_count: log.users_count,
            visits_count: log.total_visits_count,
            stat_year: stat_year
          )
          stats << stat
        end
        UserConnectionSecondYearStat.bulk_insert(stats, use_provided_primary_key: true)
      end
    end
  end

  def self.eager_stat(stat_year, page, limit)
    UserConnectionFirstYearStat
      .select(
        "domain, " \
        "COUNT(user_id) as users_count, " \
        "SUM(visits_count) as total_visits_count"
      )
      .where(stat_year: stat_year)
      .group(:domain)
      .page(page).limit(limit)
  end
end

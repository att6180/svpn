class UserPromoLog < ApplicationLogRecord
  belongs_to :promoter, class_name: 'User', foreign_key: :promoter_id
  belongs_to :accepted_user, class_name: 'User', foreign_key: :accepted_user_id
end

class NewUserDayStat < ApplicationLogRecord
  include Uuidable

  scope :between_months, -> (start_at, end_at) { where(stat_date: start_at..end_at) }

  scope :stat_result, -> {
    select("stat_date, SUM(users_count) as total_users_count")
      .group("stat_date") }

  # 统计前一天的新增用户数
  def self.stat(date = nil)
    yesterday = date || Time.now.to_date.yesterday

    stats = []

    # 全部
    items = User
      .select("COUNT(*) as users_count")
      .where("DATE(created_at) = ?", yesterday)

    if items.present?
      item = items.take
      stats << NewUserDayStat.new(
        id: Utils::Gen.generate_uuid,
        stat_date: yesterday,
        users_count: item.users_count
      )
    end

    # 渠道
    items = User
      .select("create_app_channel, count(*) as users_count")
      .group(:create_app_channel)
      .where("DATE(created_at) = ?", yesterday)

    if items.present?
      items.each do |item|
        stats << NewUserDayStat.new(
          id: Utils::Gen.generate_uuid,
          stat_date: yesterday,
          app_channel: item.create_app_channel,
          users_count: item.users_count,
          filter_type: 1
        )
      end
      stats << NewUserDayStat.new(
        id: Utils::Gen.generate_uuid,
        stat_date: yesterday,
        users_count: 0,
        filter_type: 1
      )
    else
      stats << NewUserDayStat.new(
        id: Utils::Gen.generate_uuid,
        stat_date: yesterday,
        users_count: 0,
        filter_type: 1
      )
    end

    # 版本
    items = User
      .select("create_app_version, count(*) as users_count")
      .group(:create_app_version)
      .where("DATE(created_at) = ?", yesterday)

    if items.present?
      items.each do |item|
        stats << NewUserDayStat.new(
          id: Utils::Gen.generate_uuid,
          stat_date: yesterday,
          app_version: item.create_app_version,
          users_count: item.users_count,
          filter_type: 1
        )
      end
      stats << NewUserDayStat.new(
        id: Utils::Gen.generate_uuid,
        stat_date: yesterday,
        users_count: 0,
        filter_type: 1
      )
    else
      stats << NewUserDayStat.new(
        id: Utils::Gen.generate_uuid,
        stat_date: yesterday,
        users_count: 0,
        filter_type: 1
      )
    end

    # 渠道和版本
    items = User
      .select("create_app_version, create_app_channel, count(*) as users_count")
      .group(:create_app_version, :create_app_channel)
      .where("DATE(created_at) = ?", yesterday)

    if items.present?
      items.each do |item|
        stats << NewUserDayStat.new(
          id: Utils::Gen.generate_uuid,
          stat_date: yesterday,
          app_version: item.create_app_version,
          app_channel: item.create_app_channel,
          users_count: item.users_count,
          filter_type: 2
        )
      end
      stats << NewUserDayStat.new(
        id: Utils::Gen.generate_uuid,
        stat_date: yesterday,
        users_count: 0,
        filter_type: 2
      )
    else
      stats << NewUserDayStat.new(
        id: Utils::Gen.generate_uuid,
        stat_date: yesterday,
        users_count: 0,
        filter_type: 2
      )
    end

    NewUserDayStat.bulk_insert(stats, use_provided_primary_key: true)
  end
end

class UserCheckinLog < ApplicationLogRecord
  include Uuidable

  belongs_to :user

  scope :today, -> { where("DATE(created_at) = ?", Time.now.to_date) }
  scope :month, ->(year, month) { where("YEAR(created_at) = ? AND MONTH(created_at) = ?", year, month).order(created_at: :asc) }
  scope :count_of_month, ->(year, month) { where("YEAR(created_at) = ? AND MONTH(created_at) = ?", year, month).count }
end

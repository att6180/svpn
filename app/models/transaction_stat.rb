class TransactionStat < ApplicationLogRecord
  include Uuidable

  scope :by_year_and_month, -> (year, month) { where("YEAR(stat_date) = ? AND MONTH(stat_date) = ?", year, month) }
  scope :by_year, -> (year) { where("YEAR(stat_date) = ?", year) }
  scope :channel_version_total, -> { where(filter_type: 0)}
  scope :stat_result, -> {
    select("stat_date, SUM(total_recharge_amount) as s_recharge_amount, SUM(consume_coins) AS s_consume_coins, SUM(recharge_users_count) AS s_users_count")
      .group(:stat_date)
  }

  # 按版本或渠道统计
  def self.filter_stat(yesterday, filter_type)
    stats = []
    items = []
    filter_types = SystemEnum.enums_by_type(filter_type).map(&:name)
    TransactionStat.using_db(DB_LOG_SLAVE1) do
      filter_types.each do |ft|
        total_recharge_amount = BigDecimal.new("0")
        total_consume_coins_count = 0
        recharge_users_count = 0
        renew_users_count = 0
        renew_new_users_count = 0
        renew_amount = BigDecimal.new("0")
        renew_new_amount = BigDecimal.new("0")
        # 本日之前充值过的每个渠道用户id列表(去重)
        before_users = TransactionLog.where("created_at < ?", yesterday)
          .where("app_#{filter_type} = ?", ft)
          .by_paid
          .without_iap
          .pluck("DISTINCT user_id")
        # 本日充值过的每个渠道用户列表(去重)
        now_users = TransactionLog.by_date(yesterday)
          .where("app_#{filter_type} = ?", ft)
          .by_paid
          .without_iap
          .pluck("DISTINCT user_id")
        # 充值总额
        total_recharge_amount = TransactionLog.by_date(yesterday)
          .where("app_#{filter_type} = ?", ft)
          .by_paid
          .without_iap
          .sum(:price)
        # 消费钻石数
        total_consume_coins_count = ConsumptionLog.by_date(yesterday)
          .where("app_#{filter_type} = ?", ft)
          .sum(:coins)
        # 充值人数
        recharge_users_count = now_users.length
        # 当日续费人数, 取交集
        renew_users = before_users & now_users
        renew_users_count = renew_users.length
        # 当日新增付费人数
        renew_new_users_count = recharge_users_count - renew_users_count
        # 当日续费用户充值
        renew_amount = TransactionLog.by_date(yesterday)
          .by_paid
          .without_iap
          .where(user_id: renew_users)
          .where("app_#{filter_type} = ?", ft)
          .sum(:price) if renew_users_count > 0
        # 当日新增付费用户充值
        if renew_new_users_count > 0
          renew_new_amount = total_recharge_amount - renew_amount
        end
        items << {
          filter_type_name: ft,
          total_recharge_amount: total_recharge_amount,
          consume_coins: total_consume_coins_count,
          recharge_users_count: recharge_users_count,
          renew_users_count: renew_users_count,
          renew_new_users_count: renew_new_users_count,
          renew_amount: renew_amount,
          renew_new_amount: renew_new_amount
        }
      end
    end
    if items.present?
      items.each do |item|
        stats << TransactionStat.new(
          id: Utils::Gen.generate_uuid,
          stat_date: yesterday,
          "app_#{filter_type}": item[:filter_type_name],
          total_recharge_amount: item[:total_recharge_amount],
          consume_coins: item[:consume_coins],
          recharge_users_count: item[:recharge_users_count],
          renew_users_count: item[:renew_users_count],
          renew_new_users_count: item[:renew_new_users_count],
          renew_amount: item[:renew_amount],
          renew_new_amount: item[:renew_new_amount],
          filter_type: 1
        )
      end
    end
    stats
  end

  # 全部
  def self.all_stat(yesterday)
    stats = []
    items = []
    total_recharge_amount = BigDecimal.new("0")
    total_consume_coins_count = 0
    recharge_users_count = 0
    renew_users_count = 0
    renew_new_users_count = 0
    renew_amount = BigDecimal.new("0")
    renew_new_amount = BigDecimal.new("0")
    TransactionStat.using_db(DB_LOG_SLAVE1) do
      # 本日之前充值过的用户id列表(去重)
      before_users = TransactionLog.where("created_at < ?", yesterday)
        .by_paid
        .without_iap
        .pluck("DISTINCT user_id")
      # 本日充值过的用户列表(去重)
      now_users = TransactionLog.by_date(yesterday)
        .by_paid
        .without_iap
        .pluck("DISTINCT user_id")
      # 充值总额
      total_recharge_amount = TransactionLog.by_date(yesterday)
        .by_paid
        .without_iap
        .sum(:price)
      # 消费钻石数
      total_consume_coins_count = ConsumptionLog.by_date(yesterday)
        .sum(:coins)
      # 充值人数
      recharge_users_count = now_users.length
      # 当日续费人数, 取交集
      renew_users = before_users & now_users
      renew_users_count = renew_users.length
      # 当日新增付费人数
      renew_new_users_count = recharge_users_count - renew_users_count
      # 当日续费用户充值
      if renew_users_count > 0
        renew_amount = TransactionLog.by_date(yesterday)
          .by_paid
          .without_iap
          .where(user_id: renew_users)
          .sum(:price)
      end
      # 当日新增付费用户充值
      if renew_new_users_count > 0
        renew_new_amount = total_recharge_amount - renew_amount
      end
    end
    stats << TransactionStat.new(
      id: Utils::Gen.generate_uuid,
      stat_date: yesterday,
      total_recharge_amount: total_recharge_amount,
      consume_coins: total_consume_coins_count,
      recharge_users_count: recharge_users_count,
      renew_users_count: renew_users_count,
      renew_new_users_count: renew_new_users_count,
      renew_amount: renew_amount,
      renew_new_amount: renew_new_amount
    )
    stats
  end

  # 渠道和版本
  def self.filter_all_stat(yesterday)
    stats = []
    items = []
    filter_types = SystemEnumRelation.cache_list
    TransactionStat.using_db(DB_LOG_SLAVE1) do
      filter_types.each do |ft|
        total_recharge_amount = BigDecimal.new("0")
        total_consume_coins_count = 0
        recharge_users_count = 0
        renew_users_count = 0
        renew_new_users_count = 0
        renew_amount = BigDecimal.new("0")
        renew_new_amount = BigDecimal.new("0")
        # 本日之前充值过的每个渠道用户id列表(去重)
        before_users = TransactionLog.where("created_at < ?", yesterday)
          .where(app_channel: ft.channel, app_version: ft.version)
          .by_paid
          .without_iap
          .pluck("DISTINCT user_id")
        # 本日充值过的每个渠道用户列表(去重)
        now_users = TransactionLog.by_date(yesterday)
          .where(app_channel: ft.channel, app_version: ft.version)
          .by_paid
          .without_iap
          .pluck("DISTINCT user_id")
        # 充值总额
        total_recharge_amount = TransactionLog.by_date(yesterday)
          .where(app_channel: ft.channel, app_version: ft.version)
          .by_paid
          .without_iap
          .sum(:price)
        # 消费钻石数
        total_consume_coins_count = ConsumptionLog.by_date(yesterday)
          .where(app_channel: ft.channel, app_version: ft.version)
          .sum(:coins)
        # 充值人数
        recharge_users_count = now_users.length
        # 当日续费人数, 取交集
        renew_users = before_users & now_users
        renew_users_count = renew_users.length
        # 当日新增付费人数
        renew_new_users_count = recharge_users_count - renew_users_count
        # 当日续费用户充值
        if renew_users_count > 0
          renew_amount = TransactionLog.by_date(yesterday)
            .by_paid
            .without_iap
            .where(user_id: renew_users)
            .where(app_channel: ft.channel, app_version: ft.version)
            .sum(:price)
        end
        # 当日新增付费用户充值
        if renew_new_users_count > 0
          renew_new_amount = total_recharge_amount - renew_amount
        end
        items << {
          channel: ft.channel,
          version: ft.version,
          total_recharge_amount: total_recharge_amount,
          consume_coins: total_consume_coins_count,
          recharge_users_count: recharge_users_count,
          renew_users_count: renew_users_count,
          renew_new_users_count: renew_new_users_count,
          renew_amount: renew_amount,
          renew_new_amount: renew_new_amount
        }
      end
    end
    if items.present?
      items.each do |item|
        stats << TransactionStat.new(
          id: Utils::Gen.generate_uuid,
          stat_date: yesterday,
          app_channel: item[:channel],
          app_version: item[:version],
          total_recharge_amount: item[:total_recharge_amount],
          consume_coins: item[:consume_coins],
          recharge_users_count: item[:recharge_users_count],
          renew_users_count: item[:renew_users_count],
          renew_new_users_count: item[:renew_new_users_count],
          renew_amount: item[:renew_amount],
          renew_new_amount: item[:renew_new_amount],
          filter_type: 2
        )
      end
    end
    stats
  end

  # 统计昨天的如下数据:
  # 当日续费人数：统计在本日之前（不包含本日）有过充值记录，并且本日有过充值行为的用户；需要去重
  # 当日新增付费人数：统计在本日之前（不包含本日）没有充值记录，但本日有过充值行为的用户；需要去重
  # 当日续费用户充值：统计在本日之前（不包含本日）有过充值记录，并且本日有过充值行为的用户所充值的总金额；不做去重
  # 当日新增付费用户充值：统计在本日之前（不包含本日）没有充值记录，但本日有过充值行为的用户所充值的总金额；不做去重
=begin
  # 一条sql语句取交集
  # 不采用这种方式是因为后面还要取差集(在B表中存在，在A表中不存在)和渠道版本的计算
  select uid from (
  select distinct(a.user_id) as uid from transaction_logs as a where date(a.created_at) < "2017-10-29" and a.status = 'paid'
  union all
  select distinct(b.user_id) as uid from transaction_logs as b where date(b.created_at) = "2017-10-29" and b.status = 'paid'
  ) temp group by uid having count(uid) != 1;
=end
  def self.stat(date = nil)
    yesterday = date || Time.now.to_date.yesterday
    stats = []

    # 渠道
    stats += TransactionStat.all_stat(yesterday)
    # 渠道
    stats += TransactionStat.filter_stat(yesterday, "channel")
    # 版本
    stats += TransactionStat.filter_stat(yesterday, "version")
    # 渠道和版本
    stats += TransactionStat.filter_all_stat(yesterday)

    TransactionStat.bulk_insert(stats, use_provided_primary_key: true)
  end
end

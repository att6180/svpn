module Mongo
  class ProxyAuthenticationFailureLog
    include Mongoid::Document
    include Mongoid::Timestamps

    store_in collection: "proxy_authentication_failure_logs"

    field :userid, as: :user_id, type: Integer
    field :nodeid, as: :node_id, type: Integer
    field :token, as: :token, type: String

    index({ user_id: 1, created_at: -1 })
    index({ created_date: -1 })
  end
end

module Mongo
  class UserOperationLog

    include Mongoid::Document
    include Mongoid::Timestamps

    store_in collection: "user_operation_logs"

    field :user_id, type: Integer
    field :device_id, type: Integer
    field :app_channel, type: String
    field :app_version, type: String
    field :app_version_number, type: String
    field :user_app_launch_log_id, type: String
    field :user_signin_log_id, type: String
    field :interface_id, type: Integer

    index({user_id: 1, created_at: -1})
    index({interface_id: 1, created_at: -1})

    scope :by_date, ->(date) { where(:created_at.gte => date.beginning_of_day, :created_at.lte => date.end_of_day) }
    scope :by_time_asc, -> { order(created_at: :asc) }
  end
end

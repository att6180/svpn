module Mongo
  class UserConnectionLog
    include Mongoid::Document

    store_in collection: "user_connection_logs"

    field :userid, as: :user_id, type: Integer
    field :nodeid, as: :node_id, type: Integer
    field :clientip, as: :client_ip, type: String
    field :ipcountry, as: :ip_country, type: String
    field :ipprovince, as: :ip_province, type: String
    field :ipcity, as: :ip_city, type: String
    field :topleveldomain, as: :domain, type: String
    field :addtime, as: :created_at, type: Time
    field :firstproxyip, as: :first_proxy_ip, type: String

    index({user_id: 1, addtime: -1 })
    index({user_id: 1, addtime: -1, topleveldomain: 1 })
  end
end

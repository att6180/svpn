module Mongo
  class NodeTestHttpRequestSpeedLog
    include Mongoid::Document

    store_in collection: "node_test_http_request_speed_logs"

    field :userid, as: :user_id, type: Integer
    field :username, as: :user_name, type: String
    field :clientip, as: :client_ip, type: String
    field :ipcountry, as: :ip_country, type: String
    field :ipprovince, as: :ip_province, type: String
    field :ipcity, as: :ip_city, type: String
    field :iparea, as: :ip_area, type: String
    field :requesturl, as: :request_url, type: String
    field :minrt, as: :min_rt, type: Integer
    field :maxrt, as: :max_rt, type: Integer
    field :avgrt, as: :avg_rt, type: Integer
    field :failurerate, as: :failure_rate, type: Integer
    field :appchannel, as: :app_channel, type: String
    field :app_version, as: :app_version, type: String
    field :createdat, as: :created_at, type: DateTime

    index({user_id: 1, created_at: -1 })
    index({user_id: 1, ping_url: 1, created_at: -1 })
  end
end

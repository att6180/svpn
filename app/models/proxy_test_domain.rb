class ProxyTestDomain < ApplicationRecord
  has_many :proxy_test_domain_logs

  validates :domain, uniqueness: true

  scope :domestic, -> { where(is_domestic: true) }
  scope :foreign, -> { where(is_domestic: false) }
end

class ProxyConnectionLog < ApplicationLogRecord
  include Uuidable

  belongs_to :user
end

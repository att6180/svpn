class UserConnectionFirstDayStat < ApplicationLogRecord
  include Uuidable

  belongs_to :user

  scope :between_date, -> (start_at, end_at) { where(stat_date: start_at.beginning_of_day..end_at.end_of_day) }
  scope :select_and_group, -> {
    select("domain, COUNT(DISTINCT user_id) as users_count, SUM(visits_count) as total_visits_count").group(:domain)
  }

  # 统计前一天的去向IP
  def self.stat(date = nil)
    yesterday = date || Time.now.to_date.yesterday

    stats = []

    # 全部统计
    items = UserConnectionLog
      .select("user_id, top_level_domain, COUNT(*) as visits_count")
      .group(:user_id, :top_level_domain)
      .where("DATE(created_at) = ?", yesterday)

    if items.present?
      items.each do |item|
        stats << UserConnectionFirstDayStat.new(
          id: Utils::Gen.generate_uuid,
          user_id: item.user_id,
          domain: item.top_level_domain,
          visits_count: item.visits_count,
          stat_date: yesterday
        )
      end
    end
    UserConnectionFirstDayStat.bulk_insert(stats, use_provided_primary_key: true) if stats.present?
  end

end

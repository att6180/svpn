class UserExpiredRewardLog < ApplicationLogRecord

  belongs_to :admin, class_name: Manage::Admin
  belongs_to :user
  belongs_to :user_node_type
  belongs_to :node_type
end

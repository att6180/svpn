class TransactionLog < ApplicationLogRecord

  include AASM
  include Uuidable

  STATUS_PAID = 'paid'
  CACHE_KEY_RECHARGE_AMOUNT = "transaction_log_recharge_amount_"
  CACHE_KEY_RECHARGE_USERS = "transaction_log_recharge_users_"
  CACHE_HKEY_RECHARGE_AMOUNT = "total_charge_amount"

  enum payment_method: {
    unknow: 0,
    iap: 1,
    alipay_qrcode: 2,
    wx_qrcode: 3,
    other: 4,
    alipay_h5: 5,
    wx_h5b: 6,
    qq: 7,
    wx_h5: 8,
    alipay_origin: 9,
    google_pay: 10,
    union_wap: 11
  }, _prefix: :payment_methods
  enum time_type: Plan::TIME_TYPES

  belongs_to :user
  belongs_to :plan
  belongs_to :node_type

  before_create :generate_order_number
  after_commit :after_commit_trace, on: [:create]
  after_commit :after_commit_trace_update, on: [:update, :create]

  scope :by_date, ->(d) { where(created_at: d.beginning_of_day..d.end_of_day) }
  scope :between_date, ->(start_at, end_at) { where(created_at: start_at.beginning_of_day..end_at.end_of_day) }
  scope :by_year_and_month, -> (year, month) { where("YEAR(created_at) = ? AND MONTH(created_at) = ?", year, month) }
  scope :by_year, -> (year) { where("YEAR(created_at) = ?", year) }
  scope :by_paid, -> { where(status: :paid) }
  scope :unpaid, -> { where.not(status: :paid) }
  scope :by_channel_version, ->(channel, version) { where(app_channel: channel, app_version: version)}
  scope :group_by_hour, -> { select("HOUR(created_at) as h").group('h').order("h ASC") }
  scope :select_sum_price, -> { select("SUM(price) as total_price")}
  scope :select_distinct_users, -> { select("COUNT(DISTINCT user_id) as users_count") }
  scope :without_iap, -> { where.not(payment_method: [:iap, :google_pay]) }
  scope :use_index_created_at_status, -> { from("#{self.table_name} USE INDEX(index_transaction_logs_on_created_at_and_status)") }
  scope :use_index_aasuc, -> { from("#{self.table_name} USE INDEX(transaction_logs_aasuc)") }

  aasm column: :status, whiny_transitions: false do
    state :pending, initial: true
    state :failed, :paid, :closed
    event :fail do
      transitions from: :pending, to: :failed
    end
    event :pay do
      after do
        self.update(processed_at: Time.now)
      end
      transitions from: :pending, to: :paid
    end
    event :close do
      transitions from: :pending, to: :closed
    end
  end

  # 为指定用户应用套餐并设置订单为完成状态，用于comsunny支付
  def payment_for_comsunny!(user, comsunny)
    ActiveRecord::Base.transaction do
      # 如果是钻石类套餐
      if !self.is_regular_time?
        # 为用户累加钻石和金额
        user.total_payment_amount = user.total_payment_amount + self.price
        user.current_coins = user.current_coins + self.coins
        # 赠送的钻石不记入累计钻石中
        user.total_coins = user.total_coins + self.plan_coins
        # 累计赠送钻石
        user.present_coins = user.present_coins + self.present_coins
        # 根据用户累计钻石数判断用户是否达到升级用户类型的要求
        user_group = UserGroup.find_by_coins(user.total_coins)
        # 如果用户当前等级小于此等级，则升级到此等级
        if user_group.present? && user.user_group.level < user_group.level
          user.set_group(user_group)
        end
        user.save! if user.changed?
      else
        # 为用户累加消费金额
        user.total_payment_amount = user.total_payment_amount + self.price
        user.current_coins = user.current_coins + self.coins
        user.total_coins = user.total_coins + self.plan_coins
        user.present_coins = user.present_coins + self.present_coins
        if user.total_coins > 0
          user_group = UserGroup.find_by_coins(user.total_coins)
          if user_group.present? && user.user_group.level < user_group.level
            user.set_group(user_group)
          end
        end
        user.save! if user.changed?

        node_type = self.node_type
        # 为用户类加过期时间
        user_node_type = UserNodeType.find_by(user_id: user.id, node_type_id: node_type.id)
        # 如果当前服务类型未到期，则基于服务类型过期时间累加，否则基于当前时间累加
        base_time = user_node_type.expired_at > Time.now ? user_node_type.expired_at : Time.now
        bought_time = DateUtils.increase_expire_time(base_time, self.time_type)
        # 累加过期时间
        user_node_type.update!(expired_at: bought_time)
        # 找到用户最后一次登录的日志，取出渠道和版本信息
        app_channel, app_version = user.last_signin_versions
        if app_channel.blank? || app_version.blank?
          app_channel = user.create_app_channel
          app_version = user.create_app_version
        end
        # 写入消费日志
        ConsumptionLog.create!(
          user_id: user.id,
          node_type_name: node_type.name,
          node_type_id: node_type.id,
          coins: 0,
          app_channel: app_channel,
          app_version: app_version,
          is_regular_time: self.is_regular_time,
          time_type: self.time_type
        )
      end
      # 创建comsunny交易日志
      ComsunnyTransactionLog.create_log!(user, comsunny)
      self.update!(
        payment_method: comsunny.payment_type,
        platform_transaction_id: comsunny.transaction_id,
        status: TransactionLog::STATUS_PAID,
        processed_at: Time.now
      )
    end
    # ActivityService.reward_activity(user, self.processed_at.to_s)
  end

  # 分组查出各渠道和版本的充值用户数及金额
  def self.retention_collection(reg_date, begin_connected_date, last_connected_date, is_range = true)
    result = []
    ApplicationLogRecord.using_db(DB_LOG_SLAVE1) do
      base_logs = TransactionLog.select("COUNT(DISTINCT user_id) as users_count, SUM(price) as total_price")
        .where("DATE(user_created_at) = ?", reg_date)
        .by_paid
      base_logs = if is_range
        base_logs.where("DATE(created_at) BETWEEN ? AND ?", begin_connected_date, last_connected_date)
      else
        base_logs.where("DATE(created_at) = ?", begin_connected_date)
      end
      # all
      result += base_logs.to_a.map(&:serializable_hash)
      # app_channel / app_version
      logs = base_logs.select(:app_channel, :app_version).group(:app_channel, :app_version)
      result += logs.to_a.map(&:serializable_hash)
      # app_channel
      logs = base_logs.select(:app_channel).group(:app_channel)
      result += logs.to_a.map(&:serializable_hash)
      # app_version
      logs = base_logs.select(:app_version).group(:app_version)
      result += logs.to_a.map(&:serializable_hash)
    end

    collection = {}
    result.each do |r|
      channel = r.dig("app_channel")
      version = r.dig("app_version")
      k = (channel.present? || version.present?) ? "#{channel}#{version}" : "all"
      vu = r.dig("users_count")
      vt = r.dig("total_price")
      collection[k] = { users_count: vu, total_price: vt }
    end
    collection.symbolize_keys!
  end

  # 查找指定时间范围内每个用户的订单数和充值金额
  def self.retentions(reg_date, begin_connected_date, last_connected_date, app_channel, app_version, is_range = true)
    result = TransactionLog.select("user_id, count(id) as orders_count, SUM(price) as total_price")
      .where("DATE(user_created_at) = ?", reg_date)
      .by_paid
      .group(:user_id)
    if app_channel.present? && app_version.present?
      result = result.where(app_channel: app_channel, app_version: app_version)
    elsif app_channel.present? && !app_version.present?
      result = result.where(app_channel: app_channel)
    elsif !app_channel.present? && app_version.present?
      result = result.where(app_version: app_version)
    end
    if is_range
      result = result.where(created_at: begin_connected_date.beginning_of_day..last_connected_date.end_of_day)
    else
      result = result.where("DATE(created_at) = ?", begin_connected_date)
    end
    result
  end

  def status_title
    I18n.t("transaction_log.status.#{self.state}", default: self.status.titleize)
  end

  def payment_method_title
    I18n.t("transaction_log.payment_methods.#{self.payment_method}", default: self.payment_method.titleize)
  end

  def self.amount_today
    cache_key = "payment_amount_today"
    Rails.cache.fetch(cache_key, expires_in: 15.minutes) do
      TransactionLog.where("DATE(created_at) = ?", Time.today).sum(:price).to_f
    end
  end

  def self.amount_month
    cache_key = "payment_amount_month"
    Rails.cache.fetch(cache_key, expires_in: 15.minutes) do
      TransactionLog.where(
        "DATE(created_at) > ? AND DATE(created_at) <= ?",
        Time.now.beginning_of_month,
        Time.now.end_of_month
      ).sum(:price).to_f
    end
  end

  def self.amount_year
    cache_key = "payment_amount_year"
    Rails.cache.fetch(cache_key, expires_in: 15.minutes) do
      TransactionLog.where(
        "DATE(created_at) > ? AND DATE(created_at) <= ?",
        Time.now.beginning_of_year,
        Time.now.end_of_year
      ).sum(:price).to_f
    end
  end

  # 根据指定最后的订单数，判断成功率报警
  def self.success_rate_monit
    # 取出指定数量最后创建的订单
    # 算出成功率，超过指定阀值则报警
    return if Setting.transaction_success_rate_enabled != "true"
    logs = TransactionLog.select(:status).recent.limit(Setting.transaction_success_rate_limit.to_i)
    total = logs.length
    success = logs.select{|l| l.status == 'paid'}.count
    rate = ((success.to_f / total) * 100).round
    if rate < Setting.transaction_success_rate_threshold.to_i
      SmsNotify.transaction_warn 
      PotatoNotify.transaction_warn
    end
  end

  # 今日充值总金额
  def self.recharge_amount_today
    result = $redis.get("#{CACHE_KEY_RECHARGE_AMOUNT}#{DateUtils.time2ymd(Time.now)}") || "0"
    BigDecimal.new(result)
  end

  # 今日充值总人数(去重)
  def self.recharge_users_today
    $redis.scard("#{CACHE_KEY_RECHARGE_USERS}#{DateUtils.time2ymd(Time.now)}").to_i
  end

  # 本月充值总人数(去重)
  def self.recharge_users_month
    month = Time.now.strftime("%Y%m")
    $redis.scard("#{CACHE_KEY_RECHARGE_USERS}#{month}").to_i
  end

  # 本年充值总人数(去重)
  def self.recharge_users_year
    $redis.scard("#{CACHE_KEY_RECHARGE_USERS}#{Time.now.year}").to_i
  end

  # 充值总金额
  def self.total_recharge_amount
    $redis.hget(RedisCache::CACHE_KEY_GLOBAL_COUNTER, CACHE_HKEY_RECHARGE_AMOUNT) || 0
  end

  # 累计总支付金额
  def self.increate_total_amount(price)
    $redis.hincrby(RedisCache::CACHE_KEY_GLOBAL_COUNTER, CACHE_HKEY_RECHARGE_AMOUNT, price.to_i)
  end

  private

  def generate_order_number
    self.order_number = Utils::Gen.serial_number
  end

  def after_commit_trace
    # 订单被创建时，创建队列任务来检查订单成功率
    TransactionMonitJob.perform_later if Setting.transaction_success_rate_enabled == "true"
  end

  def after_commit_trace_update
    if self.previous_changes.include?(:status) && self.paid?
      # iap支付方式不纳入统计
      if !self.payment_methods_iap?
        increase_amount_today if self.created_at.today?
        increase_recharge_users
        # 写入已支付用户id
        User.save_cache_paid_id!(self.user_id)
        # 写入总支付金额累计
        TransactionLog.increate_total_amount(self.price)
      end
    end
  end

  # 累计后台管理统计今日总金额
  def increase_amount_today
    cache_key = "#{CACHE_KEY_RECHARGE_AMOUNT}#{DateUtils.time2ymd(Time.now)}"
    $redis.incrby(cache_key, self.price.to_i)
    $redis.expireat(cache_key, Time.now.end_of_day.to_i) if $redis.ttl(cache_key) == -1
  end

  # 累计后台管理统计充值人数(去重)
  def increase_recharge_users
    now = Time.now
    today_key = "#{CACHE_KEY_RECHARGE_USERS}#{DateUtils.time2ymd(now)}"
    month_key = "#{CACHE_KEY_RECHARGE_USERS}#{now.strftime("%Y%m")}"
    year_key = "#{CACHE_KEY_RECHARGE_USERS}#{now.year}"
    $redis.sadd(today_key, self.user_id)
    $redis.expireat(today_key, Time.now.end_of_day.to_i) if $redis.ttl(today_key) == -1
    $redis.sadd(month_key, self.user_id)
    $redis.expireat(month_key, Time.now.end_of_month.to_i) if $redis.ttl(month_key) == -1
    $redis.sadd(year_key, self.user_id)
    $redis.expireat(year_key, Time.now.end_of_year.to_i) if $redis.ttl(year_key) == -1
  end

end

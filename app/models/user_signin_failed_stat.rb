class UserSigninFailedStat < ApplicationLogRecord
  include Uuidable

  scope :between_date, -> (start_at, end_at) { where("stat_date >= ? AND stat_date <= ?", start_at, end_at) }

  # 统计前一天的登录失败日志
  def self.stat(date = nil)
    stat_date = date || Time.now.to_date.yesterday
    items = UserSigninFailedLog.
      select(
        "SUM(CASE fail_type WHEN 0 THEN 1 ELSE 0 END) as type0, " \
        "SUM(CASE fail_type WHEN 1 THEN 1 ELSE 0 END) as type1, " \
        "SUM(CASE fail_type WHEN 2 THEN 1 ELSE 0 END) as type2, " \
        "SUM(CASE fail_type WHEN 3 THEN 1 ELSE 0 END) as type3").
      where("DATE(created_at) = ?", stat_date)

    items.each do |item|
      UserSigninFailedStat.create(
        stat_date: stat_date,
        not_exist_count: item.type0 || 0,
        wrong_password_count: item.type1 || 0,
        failed_connection_count: item.type2 || 0,
        crash_count: item.type3 || 0
      )
    end
  end
end

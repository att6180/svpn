class NodeConnectionUserDayLog < ApplicationLogRecord

  belongs_to :user

  # 插入数据,如果存在则忽略
  def self.create_ignore_by_sql(*params)
    sql = "INSERT IGNORE INTO node_connection_user_day_logs(user_id, created_date, " \
      "user_registered_at, app_channel, app_version, created_at, updated_at) " \
      "VALUES(?, ?, ?, ?, ?, ?, ?)"
    connection.execute(
      send(
        :sanitize_sql_array,
        [sql] + params
      )
    )
  end

  # 分组查出各渠道和版本的去重用户数
  def self.retention_collection(begin_reg_date, last_reg_date, begin_connected_date, last_connected_date, is_range = true)
    result = []
    ApplicationLogRecord.using_db(DB_LOG_SLAVE1) do
      base_logs = NodeConnectionUserDayLog.select("COUNT(DISTINCT user_id) as users_count").where(user_registered_at: begin_reg_date..last_reg_date)
      if is_range
        base_logs = base_logs.where("created_date >= ? AND created_date <= ?", begin_connected_date, last_connected_date)
      else
        base_logs = base_logs.where("created_date = ?", begin_connected_date)
      end
      # all
      result += base_logs.to_a.map(&:serializable_hash)
      # app_channel / app_version
      logs = base_logs.select(:app_channel, :app_version).group(:app_channel, :app_version)
      result += logs.to_a.map(&:serializable_hash)
      # app_channel
      logs = base_logs.select(:app_channel).group(:app_channel)
      result += logs.to_a.map(&:serializable_hash)
      # app_version
      logs = base_logs.select(:app_version).group(:app_version)
      result += logs.to_a.map(&:serializable_hash)
    end

    collection = {}
    result.each do |r|
      channel = r.dig("app_channel")
      version = r.dig("app_version")
      k = (channel.present? || version.present?) ? "#{channel}#{version}" : "all"
      v = r.dig("users_count")
      collection[k] = v
    end
    collection
  end

  # 查找指定注册时间和连接时间的记录
  def self.retentions(reg_date, begin_connected_date, last_connected_date, app_channel, app_version, is_range = true)
    result = NodeConnectionUserDayLog.select("DISTINCT user_id").where("DATE(user_registered_at) = ?", reg_date)
    if app_channel.present? && app_version.present?
      result = result.where(app_channel: app_channel, app_version: app_version)
    elsif app_channel.present? && !app_version.present?
      result = result.where(app_channel: app_channel)
    elsif !app_channel.present? && app_version.present?
      result = result.where(app_version: app_version)
    end
    if is_range
      result = result.where("created_date >= ? AND created_date <= ?", begin_connected_date, last_connected_date)
    else
      result = result.where("created_date = ?", begin_connected_date)
    end
    result
  end

end

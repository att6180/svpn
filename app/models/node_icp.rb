class NodeIcp < ApplicationRecord

  CACHE_KEY = "node_icps"

  NET_TYPES = {'电信': 1, '移动': 2, '联通': 3, "移动数据上网公共出口": 4, "鹏博士长城宽带": 5}

  scope :enabled, -> { where(is_enabled: true) }
  scope :sorted_by_enabled, -> { order(is_enabled: :desc) }

  validates :tag, :isps, presence: true
  # validates :isps, format: /,/

  after_commit :after_commit_trace

  def self.cache_list
    Rails.cache.fetch(CACHE_KEY, expires_in: 30.days) do
      NodeIcp.enabled.to_a
    end
  end

  def sisps
    self.isps.split(',')
  end

  private

  def after_commit_trace
    Rails.cache.delete(CACHE_KEY)
    Rails.cache.delete_matched("#{Node::CACHE_KEY_NODES_ISP}*")
  end
end

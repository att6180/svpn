class NodeCountry < NodeArea

  CACHE_KEY = "node_countries"
  CACHE_KEY_CONNECTION = "node_countries_connection_"

  has_many :node_regions

  validates :abbr, presence: true, uniqueness: true
  after_commit :after_commit_trace

  def self.cache_list
    Rails.cache.fetch(CACHE_KEY, expires_in: 30.days) do
      NodeCountry.all
    end
  end

  # 获取所有国家在线人数列表
  def self.cache_connections_count(node_type_id)
    Rails.cache.fetch("#{CACHE_KEY_CONNECTION}#{node_type_id}", expires_in: 30.minutes) do
      NodeCountry.joins(node_regions: [:out_nodes])
        .where("node_regions.node_type_id = ?", node_type_id)
        .select("node_areas.id, node_areas.name, node_areas.abbr, sum(nodes.connections_count) as connections_count, node_regions.node_type_id")
        .group(:node_region_out_id, :connections_count)
        .order("connections_count asc")
    end
  end

  # 排除当前ip所在地，获取连接数最少的国家
  def self.connections_least(ip_country)
    list = cache_connections_count(NodeType.base_type_id)
    list = list.reject{|i| i[:name] == ip_country}
    list.first || {}
  end

  private

  def after_commit_trace
    Rails.cache.delete(CACHE_KEY)
  end
end

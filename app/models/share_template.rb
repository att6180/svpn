class ShareTemplate < ApplicationRecord

  CACHE_KEY = "share_templates"

  validates :sequence, :platform_name, :copy_link_text, :text_title, :text_content,
    :text_url, :image_text_content, :image_promo_text, :landing_page_qrcode_url,
    :background_image_download_url, presence: true
  validates :platform_name, uniqueness: true

  scope :sorted, -> { order(sequence: :asc) }

  before_update :before_update_trace
  after_commit :after_commit_trace

  def self.cache_list
    Rails.cache.fetch(CACHE_KEY, expires_in: 30.days) do
      ShareTemplate.sorted.to_a
    end
  end

  private

  def before_update_trace
    self.background_image_update_at = Time.now
  end

  def after_commit_trace
    # 如果模版发生了任何改变，修改分享模版更新时间
    Setting.share_template_update_time = Time.now.to_i
    Rails.cache.delete(CACHE_KEY)
  end
end

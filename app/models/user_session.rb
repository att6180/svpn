class UserSession < ApplicationRecord
  # 虚拟币不够
  ERR_NOT_ENOUGH_COINS = 1
  # 登录后的重置(同一用户同时只能有一台设备登录)
  ERR_AFTER_SIGN_IN = 2
  # 服务到期
  ERR_SERVICE_EXPIRED = 3
  # 服务器不存在
  ERR_NODE_NOT_EXIST = 4
  # 服务器爆满
  ERR_NODE_OVERLOAD = 5
  # 用户等级不够
  ERR_USER_LEVEL_NOT_ENOUGH = 6
  # 服务到期，自动续费被关闭
  ERR_SERVICE_EXPIRED_AUTO_RENEW_DISABLED = 7
  # 用户帐号被禁用
  ERR_USER_DISABLED = 8

  CACHE_KEY = "user_sessions"

  belongs_to :user
  belongs_to :device

  after_commit :after_commit_trace, on: [:create]

  # 重新生成token
  # 将新的token信息写入缓存，并队列写入数据库
  def self.regenerate_token!(user_id, id, error_code = 0)
    token = Utils::Gen.friendly_token
    RedisCache.hset_list(CACHE_KEY, user_id, [id, token, error_code])
    RegenerateSessionTokenJob.perform_later(id, token)
    token
  end

  # 将指定token信息写入数据库
  def self.write_token!(id, token, error_code = 0)
    UserSession.where(id: id).update_all(token: token, error_code: error_code)
  end

  # 根据用户ID创建
  def self.create_by_user(user_id)
    UserSession.create(user_id: user_id, token: Utils::Gen.friendly_token)
  end

  # 查询缓存中的session数据，如果没有，就从数据库中查询
  # 两个都没有则返回nil
  def self.read_by_user_id(user_id)
    user_session = RedisCache.hfetch_list(CACHE_KEY, user_id) do
      us = UserSession.find_by(user_id: user_id)
      us.present? ? [us.id, us.token, us.error_code] : nil
    end
    user_session.nil? ? nil : [user_session[0].to_i, user_session[1], user_session[2].to_i]
  end

  private

  def after_commit_trace
    RedisCache.hset_list(CACHE_KEY, self.user_id, [self.id, self.token, self.error_code])
  end

end

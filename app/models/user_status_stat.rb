class UserStatusStat < ApplicationLogRecord
  include Uuidable

  scope :stat_result, -> { select("is_enabled, SUM(quantity) AS total").group("is_enabled").order("total DESC") }

  def self.stat
    UserStatusStat.delete_all

    stats = []

    # 全部
    items = User.select("is_enabled, count(*) as quantity")
      .group(:is_enabled)
    items.each do |item|
      stats << UserStatusStat.new(
        id: Utils::Gen.generate_uuid,
        is_enabled: item.is_enabled,
        quantity: item.quantity
      )
    end

    # 渠道
    items = User.select("create_app_channel, is_enabled, count(*) as quantity")
      .group(:create_app_channel, :is_enabled)
    items.each do |item|
      stats << UserStatusStat.new(
        id: Utils::Gen.generate_uuid,
        app_channel: item.create_app_channel,
        is_enabled: item.is_enabled,
        quantity: item.quantity,
        filter_type: 1
      )
    end

    # 版本
    items = User.select("create_app_version, is_enabled, count(*) as quantity")
      .group(:create_app_version, :is_enabled)
    items.each do |item|
      stats << UserStatusStat.new(
        id: Utils::Gen.generate_uuid,
        app_version: item.create_app_version,
        is_enabled: item.is_enabled,
        quantity: item.quantity,
        filter_type: 1
      )
    end

    # 渠道和版本
    items = User.select("create_app_version, create_app_channel, is_enabled, count(*) as quantity")
      .group(:create_app_version, :create_app_channel, :is_enabled)
    items.each do |item|
      stats << UserStatusStat.new(
        id: Utils::Gen.generate_uuid,
        app_version: item.create_app_version,
        app_channel: item.create_app_channel,
        is_enabled: item.is_enabled,
        quantity: item.quantity,
        filter_type: 2
      )
    end

    UserStatusStat.bulk_insert(stats, use_provided_primary_key: true) if stats.present?
  end
end

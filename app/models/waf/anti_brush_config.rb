class Waf::AntiBrushConfig < ApplicationRecord

  enum status: [
    :opened,
    :closed
  ]

  after_commit :write_cache, on: [:create, :update]
  after_destroy :destroy_cache

  def hash_salt
    (self.app_channel + self.app_version + self.app_version_no).hash
  end

  def write_cache
    attrs = %w(no created_at updated_at)
    RedisCache.hset("waf_anti_brush_config", self.id, self.attributes.except(*attrs).to_json)
  end

  def destroy_cache
    RedisCache.hdel("waf_anti_brush_config", self.id)
  end

end

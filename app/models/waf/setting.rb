class Waf::Setting < ApplicationRecord

  after_commit :write_cache, on: [:create, :update]

  def write_cache
    key = 'waf_setting'
    $redis.hset(key, :brush_time, self.brush_time)
    $redis.hset(key, :brush_count, self.brush_count)
    $redis.hset(key, :trigger_count, self.trigger_count)
  end
end

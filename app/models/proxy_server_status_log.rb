class ProxyServerStatusLog < ApplicationLogRecord

  belongs_to :node

  scope :by_date, ->(date) { where("DATE(created_at) = ?", date) }
  scope :group_by_day, -> {
    select("node_id, DATE(created_at) AS stat_date, " \
      "MIN(bandwidth_percent) AS min_bandwidth_percent, MAX(bandwidth_percent) AS max_bandwidth_percent, " \
      "CEIL(SUM(bandwidth_percent) / COUNT(bandwidth_percent)) AS average_bandwidth_percent, " \
      "MIN(cpu_percent) AS min_cpu_percent, MAX(cpu_percent) AS max_cpu_percent, " \
      "CEIL(SUM(cpu_percent) / COUNT(cpu_percent)) AS average_cpu_percent, " \
      "MIN(memory_percent) AS min_memory_percent, MAX(memory_percent) AS max_memory_percent, " \
      "CEIL(SUM(memory_percent) / COUNT(memory_percent)) AS average_memory_percent, " \
      "MIN(network_speed_up) AS min_network_speed_up, MAX(network_speed_up) AS max_network_speed_up, " \
      "CEIL(SUM(network_speed_up) / COUNT(network_speed_up)) AS average_network_speed_up, " \
      "MIN(network_speed_down) AS min_network_speed_down, MAX(network_speed_down) AS max_network_speed_down, " \
      "CEIL(SUM(network_speed_down) / COUNT(network_speed_down)) AS average_network_speed_down, " \
      "MIN(disk_percent) AS min_disk_percent, MAX(disk_percent) AS max_disk_percent, " \
      "CEIL(SUM(disk_percent) / COUNT(disk_percent)) AS average_disk_percent")
    .group("stat_date, node_id")
    .order("stat_date DESC")
  }
  scope :group_by_day_average, -> {
    select("node_id, DATE(created_at) AS stat_date, " \
      "ROUND((SUM(bandwidth_percent) / COUNT(bandwidth_percent)), 3) AS average_bandwidth_percent, " \
      "ROUND((SUM(cpu_percent) / COUNT(cpu_percent)), 3) AS average_cpu_percent, " \
      "ROUND((SUM(memory_percent) / COUNT(memory_percent)), 3) AS average_memory_percent, " \
      "ROUND((SUM(network_speed_up) / COUNT(network_speed_up)), 3) AS average_network_speed_up, " \
      "ROUND((SUM(network_speed_down) / COUNT(network_speed_down)), 3) AS average_network_speed_down, " \
      "ROUND((SUM(disk_percent) / COUNT(disk_percent)), 3) AS average_disk_percent")
    .group("stat_date, node_id")
    .order("stat_date DESC")
  }

  def self.min_item(date, column_name)
    select(:id, :created_at).select(column_name).by_date(date).order("#{column_name} ASC").first
  end

  def self.max_item(date, column_name)
    select(:id, :created_at).select(column_name).by_date(date).order("#{column_name} DESC").first
  end

  def self.average(date, column_name)
    result = select("SUM(#{column_name}) AS item_sum, COUNT(#{column_name}) AS item_count").by_date(date).take
    if result.present?
      per_average = "%.1f" % (result.item_sum.to_f / result.item_count)
      per_average.to_f
    else
      0
    end
  end

end

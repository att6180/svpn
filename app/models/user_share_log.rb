class UserShareLog < ApplicationLogRecord

  include Uuidable

  # 原v1分享类型
  # SHARE_TYPES = { weixin: 0, friend_circle: 1, qq: 2, weibo: 3 }

  SHARE_TYPES = {
    copy_link: 4,
    weixin_text: 0,
    friend_circle_text: 1,
    qq_text: 2,
    weibo_text: 3,
    weixin_image: 5,
    friend_circle_image: 6,
    qq_image: 7,
    weibo_image: 8
  }

  enum share_type: SHARE_TYPES

  belongs_to :user

  # 指定类型的每个去重的用户的分享次数
  scope :distinct_user, ->(type) {
    select("user_id, COUNT(user_id) as share_count").
    where("share_type = ?", type).
    group(:user_id).
    order("share_count DESC")
  }
  scope :yesterday_ago, -> { where("created_at <= ?", Time.now.yesterday.end_of_day)}
  scope :by_created_date, ->(date) { where(created_at: date.to_date.beginning_of_day..date.to_date.end_of_day) }
  # 获取每个平台下各个分享平台的分享类型次数
  scope :select_each_share_type, -> {
    select(<<-SQL
        platform_name,
      	count(
      		CASE WHEN share_type = 0 THEN 1 ELSE NULL END
      	) AS weixin_text,
        count(
      		CASE WHEN share_type = 1 THEN 1 ELSE NULL END
      	) AS friend_circle_text,
        count(
      		CASE WHEN share_type = 2 THEN 1 ELSE NULL END
      	) AS qq_text,
        count(
      		CASE WHEN share_type = 3 THEN 1 ELSE NULL END
      	) AS weibo_text,
        count(
      		CASE WHEN share_type = 4 THEN 1 ELSE NULL END
      	) AS copy_link,
        count(
      		CASE WHEN share_type = 5 THEN 1 ELSE NULL END
      	) AS weixin_image,
        count(
      		CASE WHEN share_type = 6 THEN 1 ELSE NULL END
      	) AS friend_circle_image,
        count(
      		CASE WHEN share_type = 7 THEN 1 ELSE NULL END
      	) AS qq_image,
        count(
      		CASE WHEN share_type = 8 THEN 1 ELSE NULL END
      	) AS weibo_image
      SQL
    ).
    group(:platform_name)
  }
  # 指定类型的分享记录
  scope :by_user, ->(user_id) { where(user_id: user_id) }
  # 指定用户的分享记录
  scope :by_type, ->(type) { where(share_type: type) }

end

class UserAddition < ApplicationRecord
  belongs_to :user

  scope :active_users, -> { where("DATE(last_connection_at) = ?", Date.today) }

end

class CustomerService < ApplicationRecord

  CACHE_KEY = "customer_services_alloc"

  validates :name, :potato_url, presence: true, uniqueness: true
  scope :online, -> { where(status: true) }
  scope :sorted, -> { order(alloc_count: :asc, updated_at: :asc) }

  after_destroy :delete_from_alloc_count_cache
  after_create :record_alloc_count

  def cache_alloc_count
    $redis.hget(CACHE_KEY, self.id).to_i
  end

  def increase_alloc_count
    $redis.hincrby(CACHE_KEY, self.id, 1)
  end

  # 如果客服上线从数据库恢复缓存数据
  # 如果下线删除缓存同步至数据库
  def update_alloc_count_cache!
    if self.status_changed?
      record_alloc_count(self.alloc_count) if self.status
      if !self.status
        self.alloc_count = self.cache_alloc_count
        delete_from_alloc_count_cache
      end
    end
  end

  # 等比分配在线状态下客服
  def self.allocation
    list = $redis.hgetall(CACHE_KEY)
    id = list.sort_by{|k,v| v.to_i}&.first&.first
    cs = CustomerService.find(id)
    cs.increase_alloc_count if cs.present?
    cs
  end

  def qrcode_url
    Utils.qrcode(self.potato_url)
  end

  private

  def delete_from_alloc_count_cache
    $redis.hdel(CACHE_KEY, self.id)
  end

  def record_alloc_count(count = self.alloc_count)
    $redis.hset(CACHE_KEY, self.id, count)
  end

end

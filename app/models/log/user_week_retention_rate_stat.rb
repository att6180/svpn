class Log::UserWeekRetentionRateStat < ApplicationLogRecord
  include Uuidable

  scope :between_months, -> (start_at, end_at) { where(stat_date: start_at..end_at) }

  scope :stat_result_user, -> {
    select(
      "stat_date, SUM(users_count) as total_users_count, SUM(first_week_users_count) as total_first_week_users_count, " \
      "SUM(second_week_users_count) as total_second_week_users_count, SUM(third_week_users_count) as total_third_week_users_count, " \
      "SUM(fourth_week_users_count) as total_fourth_week_users_count")
      .group("stat_date") }


  # 统计指定某周的留存数据
  def self.onweek_stat(date = nil)

    first_week = date
    end_week = date.end_of_week

    stats = Log::UserWeekRetentionRateStat.where(stat_date: first_week.to_date)

    return if stats.blank?

    # 次周, 汇总每个渠道和版本的去重用户数
    collection1 = (first_week + 2.weeks).today? ? NodeConnectionUserDayLog.retention_collection(first_week, end_week, first_week + 1.weeks, end_week + 1.weeks) : nil
    # 2周, 汇总每个渠道和版本的去重用户数
    collection2 = (first_week + 3.weeks).today? ? NodeConnectionUserDayLog.retention_collection(first_week, end_week, first_week + 2.weeks, end_week + 2.weeks) : nil
    # 3周, 汇总每个渠道和版本的去重用户数
    collection3 = (first_week + 4.weeks).today? ? NodeConnectionUserDayLog.retention_collection(first_week, end_week, first_week + 3.weeks, end_week + 3.weeks) : nil
    # 4周, 汇总每个渠道和版本的去重用户数
    collection4 = (first_week + 5.weeks).today? ? NodeConnectionUserDayLog.retention_collection(first_week, end_week, first_week + 4.weeks, end_week + 4.weeks) : nil

    stats.each do |stat|
      first_week_users_count = UserRetentionRateStat.get_users_count(collection1, stat.app_channel, stat.app_version)
      second_week_users_count = UserRetentionRateStat.get_users_count(collection2, stat.app_channel, stat.app_version)
      third_week_users_count = UserRetentionRateStat.get_users_count(collection3, stat.app_channel, stat.app_version)
      fourth_week_users_count = UserRetentionRateStat.get_users_count(collection4, stat.app_channel, stat.app_version)

      stat.first_week_users_count = first_week_users_count if (first_week + 2.weeks).today?
      stat.second_week_users_count = second_week_users_count if (first_week + 3.weeks).today?
      stat.third_week_users_count = third_week_users_count if (first_week + 4.weeks).today?
      stat.fourth_week_users_count = fourth_week_users_count if (first_week + 5.weeks).today?

      stat.save if stat.changed?
    end
  end

  # 每周统计，统计前1-4周的用户留存数据
  def self.stat(date = nil)
    last_week = date || Time.now.prev_week

    Log::UserWeekRetentionRateStat.stat_by_week(last_week)

    # 循环最近9周（不包括当前周）
    # 从用户表查找注册日期等于每周范围内的用户
    # 同时统计这些用户的 最后连接时间 在 从注册日 起的 第1-4周的数量
    first_week = last_week - 1.week
    end_week = last_week - 5.week
    while first_week > end_week
      Log::UserWeekRetentionRateStat.onweek_stat(first_week)
      first_week = first_week.prev_week
    end
  end

  # 统计指定周的新增用户数
  def self.stat_by_week(date)
    stats = []

    end_date = date.end_of_week

    # 全部
    users_count = User.where(created_at: date..end_date).count
    stats << Log::UserWeekRetentionRateStat.new(
      id: Utils::Gen.generate_uuid,
      stat_date: date,
      users_count: users_count
    )

    # 渠道
    user_items = User
      .select("create_app_channel as app_channel, count(*) as users_count")
      .group(:create_app_channel)
      .where(created_at: date..end_date)
      .to_a.map(&:serializable_hash)

    user_items.each do |item|
      stats << Log::UserWeekRetentionRateStat.new(
        id: Utils::Gen.generate_uuid,
        stat_date: date,
        app_channel: item["app_channel"],
        users_count: item["users_count"] || 0,
        filter_type: 1
      )
    end

    # 版本
    user_items = User
      .select("create_app_version as app_version, count(*) as users_count")
      .group(:create_app_version)
      .where(created_at: date..end_date)
      .to_a.map(&:serializable_hash)

    user_items.each do |item|
      stats << Log::UserWeekRetentionRateStat.new(
        id: Utils::Gen.generate_uuid,
        stat_date: date,
        app_version: item["app_version"],
        users_count: item["users_count"] || 0,
        filter_type: 1
      )
    end

    # 渠道和版本
    user_items = User
      .select("create_app_channel as app_channel, create_app_version as app_version, count(*) as users_count")
      .group(:create_app_version, :create_app_channel)
      .where(created_at: date..end_date)
      .to_a.map(&:serializable_hash)

    user_items.each do |item|
      stats << Log::UserWeekRetentionRateStat.new(
        id: Utils::Gen.generate_uuid,
        stat_date: date,
        app_version: item["app_version"],
        app_channel: item["app_channel"],
        users_count: item["users_count"] || 0,
        filter_type: 2
      )
    end

    Log::UserWeekRetentionRateStat.bulk_insert(stats, use_provided_primary_key: true) if stats.present?
  end

end

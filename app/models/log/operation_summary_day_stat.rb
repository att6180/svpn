class Log::OperationSummaryDayStat < ApplicationLogRecord

  extend ApplicationHelper
  ApplicationLogRecord.class_eval do
    scope :by_date_channel, -> (date, channel, version) {
      where(stat_date: date, app_channel: channel, app_version: version).order(filter_type: :asc)
    }
  end

  scope :between_months, -> (start_at, end_at) { where(stat_date: start_at..end_at) }
  scope :stat_result_transaction, -> {
    select(
      "stat_date, SUM(total_recharge_amount) as t_total_recharge_amount, " \
      "SUM(new_users_count) as t_new_users_count, SUM(active_users_count) as t_active_users_count, " \
      "SUM(paid_users_count) as t_paid_users_count,  " \
      "SUM(paid_total_price) as t_paid_total_price, SUM(seven_days_users_count) as t_seven_days_users_count, " \
      "SUM(seven_days_total_price) as t_seven_days_total_price, " \
      "SUM(thirty_days_users_count) as t_thirty_days_users_count, " \
      "SUM(thirty_days_total_price) as t_thirty_days_total_price, " \
      "SUM(second_day) as t_second_day, SUM(third_day) as t_third_day, " \
      "SUM(seventh_day) as t_seventh_day, SUM(fifteen_day) as t_fifteen_day, " \
      "SUM(thirtieth_day) as t_thirtieth_day")
      .group("stat_date")}


  def self.retention_rate(quantity, total)
    return nil if quantity.nil? || total.nil?
    return 0 if quantity == 0
    percent = "%.1f" % ((quantity.to_f / total.to_f) * 100)
    percent.to_f
  end

  # 统计指定日期的
  # - 充值金额
  # - 新增人数
  # - 活跃人数
  # - 当日付费人数
  # - 当当日付费金额
  def self.stat_by_date(date)

    yesterday = date || Time.now.to_date.yesterday

    stats = []
    filter_types = SystemEnumRelation.cache_list
    filter_types.insert(0, OpenStruct.new(nil)) # 查询全部

    filter_types.each do |t|
      channel = t.channel
      version = t.version

      # 充值金额 与“付费信息”里“日”的“充值金额”字段功能一致
      total_recharge_amount = TransactionStat
        .by_date_channel(yesterday, channel, version)
        &.take
        &.total_recharge_amount

      # 新增人数 与“新增用户”里“日”的“日新增用户”字段功能一致
      new_users_count = NewUserDayStat
        .by_date_channel(yesterday, channel, version)
        &.take
        &.users_count

      # 活跃人数 与“活跃用户”里“日”的“日活跃用户”字段功能一致
      active_users_count = ActiveUserDayStat
        .by_date_channel(yesterday, channel, version)
        &.take
        &.users_count

      rate = UserRetentionRateStat
        .by_date_channel(yesterday, channel, version)
        &.take

      # 当日付费人数 与“充值新增”中“付费渗透”里“日”的“当天充值用户”字段功能一致
      paid_users_count = rate&.paid_users_count
      # 当日付费金额 与“充值新增”中“付费渗透”里“日”的“当天充值金额”字段功能一致
      paid_total_price = rate&.total_price

      stats << Log::OperationSummaryDayStat.new(
        stat_date: yesterday,
        total_recharge_amount: total_recharge_amount || 0,
        new_users_count: new_users_count || 0,
        active_users_count: active_users_count || 0,
        paid_users_count: paid_users_count || 0,
        paid_total_price: paid_total_price || 0,
        app_channel: channel,
        app_version: version,
        platform_id: t.platform&.fetch(:id),
        channel_account_id: t.channel_account&.fetch(:id),
        package_id: t.package&.fetch(:id),
        filter_type: ((channel.nil? && version.nil?) ? 0 : 1)
      )
    end

    Log::OperationSummaryDayStat.bulk_insert(stats)
  end


  # 统计指定某天的数据
  # date: 指定日期，将会取此日期的前一天进行统计
  def self.onday_stat(date = nil)
    today = Time.now.to_date
    first_day = date || today.yesterday

    stats = Log::OperationSummaryDayStat.where(stat_date: first_day)
    if stats.present?
      stats.each do |stat|
        rate = UserRetentionRateStat
          .by_date_channel(stat.stat_date, stat.app_channel, stat.app_version)
          &.take

        # 7日付费人数 与“充值新增”中“付费渗透”里“日”的“7日充值用户”字段功能一致
        seventh_paid_users_count = first_day + 6.days < today ? (rate&.seventh_paid_users_count || 0) : nil
        # 7日付费金额 与“充值新增”中“付费渗透”里“日”的“7天充值金额”字段功能一致
        seventh_paid_total_price = first_day + 6.days < today ? (rate&.seventh_total_price || 0) : nil
        # 30日付费人数 与“充值新增”中“付费渗透”里“日”的“30日充值用户”字段功能一致
        thirtieth_paid_users_count = first_day + 29.days < today ? (rate&.thirtieth_paid_users_count || 0) : nil
        # 30日付费金额 与“充值新增”中“付费渗透”里“日”的“30天充值金额”字段功能一致
        thirtieth_paid_total_price = first_day + 29.days < today ? (rate&.thirtieth_total_price || 0) : nil

        # 7日付费去重人数：与“充值新增”中“付费渗透”里“日”的“7日充值去重人数”字段功能一致
        seven_days_users_count = first_day + 6.days < today ? (rate&.seventh_users_count || 0) : nil
        # 7日付费总金额：与“充值新增”中“付费渗透”里“日”的“7日充值总额”字段功能一致
        seven_days_total_price = first_day + 6.days < today ? (rate&.seventh_17_total_price || 0) : nil
        # 30日付费去重人数：与“充值新增”中“付费渗透”里“日”的“30日充值去重人数”字段功能一致
        thirty_days_users_count = first_day + 29.days < today ? (rate&.thirtieth_users_count || 0) : nil
        # 30日付费总金额：与“充值新增”中“付费渗透”里“日”的“30日充值总额”字段功能一致
        thirty_days_total_price = first_day + 29.days < today ? (rate&.thirtieth_130_total_price || 0) : nil

        # 次日留存 与“留存”里“日”的“次日留存用户”字段功能一致
        second_day = first_day.next_day < today ? (rate&.second_day || 0) : nil
        # 3日留存 与“留存”里“日”的“次日留存用户”字段功能一致
        third_day = first_day.next_day < today ? (rate&.third_day || 0) : nil
        # 7日留存 与“留存”里“日”的“7日留存用户”字段功能一致
        seventh_day = first_day + 6.days < today ? (rate&.seventh_day || 0) : nil
        # 15日留存 与“留存”里“日”的“次日留存用户”字段功能一致
        fifteen_day = first_day.next_day < today ? (rate&.fifteen_day || 0) : nil
        # 30日留存 与“留存”里“日”的“30日留存用户”字段功能一致
        thirtieth_day = first_day + 29.days < today ? (rate&.thirtieth_day || 0) : nil

        stat.seventh_paid_users_count = seventh_paid_users_count
        stat.seventh_paid_total_price = seventh_paid_total_price
        stat.thirtieth_paid_users_count = thirtieth_paid_users_count
        stat.thirtieth_paid_total_price = thirtieth_paid_total_price
        stat.second_day = second_day
        stat.third_day = third_day
        stat.seventh_day = seventh_day
        stat.fifteen_day = fifteen_day
        stat.thirtieth_day = thirtieth_day
        stat.seven_days_users_count = seven_days_users_count
        stat.seven_days_total_price = seven_days_total_price
        stat.thirty_days_users_count = thirty_days_users_count
        stat.thirty_days_total_price = thirty_days_total_price

        stat.save!
      end
    end
  end

  # 统计前一天或指定日期运营汇总信息
  # 涵盖 付费，新增，活跃，渗透等
  # date: 不指定时为统计昨天，指定时统计指定日期
  def self.stat(date = nil)

    today = date || Time.now.to_date
    yesterday = today.yesterday
    Log::OperationSummaryDayStat.stat_by_date(yesterday)
    # 循环最近30天（不包括昨天和今天）的日期
    # 从用户表查找注册日期等于每个日期的用户
    # 同时统计这些用户的 最后连接时间 在 从注册日 起的 第2天/7天内/30内 的数量
    first_day = yesterday - 1.days
    end_day = yesterday - 32.days
    while first_day > end_day
      Log::OperationSummaryDayStat.onday_stat(first_day)
      first_day = first_day.prev_day
    end
  end
end

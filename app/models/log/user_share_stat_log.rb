class Log::UserShareStatLog < ApplicationLogRecord
    include Uuidable

    enum share_type: UserShareLog::SHARE_TYPES

    scope :between_months, -> (start_at, end_at) { where(stat_date: start_at..end_at) }

    scope :stat_cover_result, -> {
      select("platform_name, (SUM(weixin_text_count) + SUM(friend_circle_text_count) + " \
      "SUM(qq_text_count) + SUM(weibo_text_count)) AS total_text_count," \
      "SUM(copy_link_count) AS total_copy_link_count, " \
      "(SUM(weixin_image_count) + SUM(friend_circle_image_count) + " \
      "SUM(qq_image_count) + SUM(weibo_image_count)) AS total_image_count").
      group(:platform_name)
    }

    scope :stat_text_result, -> {
      select("platform_name, SUM(weixin_text_count) AS total_weixin_text_count, " \
      "SUM(friend_circle_text_count) AS total_friend_circle_text_count, " \
      "SUM(qq_text_count) AS total_qq_text_count, SUM(weibo_text_count) AS total_weibo_text_count").
      group(:platform_name)
    }

    scope :stat_image_result, -> {
      select("platform_name, SUM(weixin_image_count) AS total_weixin_image_count, " \
      "SUM(friend_circle_image_count) AS total_friend_circle_image_count, " \
      "SUM(qq_image_count) AS total_qq_image_count, SUM(weibo_image_count) AS total_weibo_image_count").
      group(:platform_name)
    }

    # 统计前一天的用户分享数据
    def self.stat(date = nil)
      yesterday = date || Time.now.to_date.yesterday
      stats = []

      # 全部
      items = UserShareLog.select_each_share_type
        .by_created_date(yesterday)

      items.each do |item|
        stats << Log::UserShareStatLog.new(
          id: Utils::Gen.generate_uuid,
          stat_date: yesterday,
          platform_name: item[:platform_name],
          weixin_text_count: item[:weixin_text],
          friend_circle_text_count: item[:friend_circle_text],
          qq_text_count: item[:qq_text],
          weibo_text_count: item[:weibo_text],
          copy_link_count: item[:copy_link],
          weixin_image_count: item[:weixin_image],
          friend_circle_image_count: item[:friend_circle_image],
          qq_image_count: item[:qq_image],
          weibo_image_count: item[:weibo_image]
        )
      end

      # 渠道
      items = items = UserShareLog.select_each_share_type
        .by_created_date(yesterday)
        .group(:app_channel)
        .select(:app_channel)

      items.each do |item|
        stats << Log::UserShareStatLog.new(
          id: Utils::Gen.generate_uuid,
          stat_date: yesterday,
          platform_name: item[:platform_name],
          weixin_text_count: item[:weixin_text],
          friend_circle_text_count: item[:friend_circle_text],
          qq_text_count: item[:qq_text],
          weibo_text_count: item[:weibo_text],
          copy_link_count: item[:copy_link],
          weixin_image_count: item[:weixin_image],
          friend_circle_image_count: item[:friend_circle_image],
          qq_image_count: item[:qq_image],
          weibo_image_count: item[:weibo_image],
          app_channel: item[:app_channel],
          filter_type: 1
        )
      end

      # 版本
      items = UserShareLog.select_each_share_type
        .by_created_date(yesterday)
        .group(:app_version)
        .select(:app_version)

      items.each do |item|
        stats << Log::UserShareStatLog.new(
          id: Utils::Gen.generate_uuid,
          stat_date: yesterday,
          platform_name: item[:platform_name],
          weixin_text_count: item[:weixin_text],
          friend_circle_text_count: item[:friend_circle_text],
          qq_text_count: item[:qq_text],
          weibo_text_count: item[:weibo_text],
          copy_link_count: item[:copy_link],
          weixin_image_count: item[:weixin_image],
          friend_circle_image_count: item[:friend_circle_image],
          qq_image_count: item[:qq_image],
          weibo_image_count: item[:weibo_image],
          app_version: item[:app_version],
          filter_type: 1
        )
      end

      # 渠道和版本
      items = UserShareLog.select_each_share_type
          .by_created_date(yesterday)
          .group(:app_version, :app_channel)
          .select(:app_version, :app_channel)

      items.each do |item|
        stats << Log::UserShareStatLog.new(
          id: Utils::Gen.generate_uuid,
          stat_date: yesterday,
          platform_name: item[:platform_name],
          weixin_text_count: item[:weixin_text],
          friend_circle_text_count: item[:friend_circle_text],
          qq_text_count: item[:qq_text],
          weibo_text_count: item[:weibo_text],
          copy_link_count: item[:copy_link],
          weixin_image_count: item[:weixin_image],
          friend_circle_image_count: item[:friend_circle_image],
          qq_image_count: item[:qq_image],
          weibo_image_count: item[:weibo_image],
          app_channel: item[:app_channel],
          app_version: item[:app_version],
          filter_type: 2
        )
      end

      Log::UserShareStatLog.bulk_insert(stats, use_provided_primary_key: true) if stats.present?
    end
  end

class Log::UserCheckinDayStat < ApplicationLogRecord
  include Uuidable

  scope :between_months, -> (start_at, end_at) { where(stat_date: start_at..end_at) }
  
  # 统计前一天的签到用户数
  def self.stat(date = nil)
    yesterday = date || Time.now.to_date.yesterday

    stats = []
    # 全部统计
    items = UserCheckinLog
      .select("COUNT(DISTINCT user_id) as users_count")
      .where("checkin_date = ?", yesterday)

    active_item = ActiveUserDayStat
      .where(stat_date: yesterday, filter_type: 0)
      .take

    if items.present?
      item = items.take
      stats << Log::UserCheckinDayStat.new(
        id: Utils::Gen.generate_uuid,
        stat_date: yesterday,
        active_users_count: active_item.users_count,
        checkin_users_count: item.users_count
      )
    end

    # 渠道
    items = UserCheckinLog
      .select("COUNT(DISTINCT user_id) as users_count, app_channel")
      .group(:app_channel)
      .where("checkin_date = ?", yesterday)

    if items.present?
      items.each do |item|
        active_item = ActiveUserDayStat.where(
          stat_date: yesterday,
          filter_type: 1,
          app_channel: item.app_channel
         ).take
        stats << Log::UserCheckinDayStat.new(
          id: Utils::Gen.generate_uuid,
          stat_date: yesterday,
          app_channel: item.app_channel,
          active_users_count: active_item&.users_count,
          checkin_users_count: item.users_count,
          filter_type: 1
        )
      end
    end

    # 版本
    items = UserCheckinLog
      .select("COUNT(DISTINCT user_id) as users_count, app_version")
      .group(:app_version)
      .where("checkin_date = ?", yesterday)

    if items.present?
      items.each do |item|
        active_item = ActiveUserDayStat.where(
          stat_date: yesterday,
          filter_type: 1,
          app_version: item.app_version
         ).take
        stats << Log::UserCheckinDayStat.new(
          id: Utils::Gen.generate_uuid,
          stat_date: yesterday,
          app_version: item.app_version,
          active_users_count: active_item&.users_count,
          checkin_users_count: item.users_count,
          filter_type: 1
        )
      end
    end

    # 渠道和版本
    items = UserCheckinLog
      .select("COUNT(DISTINCT user_id) as users_count, app_version, app_channel")
      .group(:app_version, :app_channel)
      .where("checkin_date = ?", yesterday)

    if items.present?
      items.each do |item|
        active_item = ActiveUserDayStat.where(
          stat_date: yesterday,
          filter_type: 2,
          app_channel: item.app_channel,
          app_version: item.app_version
         ).take
        stats << Log::UserCheckinDayStat.new(
          id: Utils::Gen.generate_uuid,
          stat_date: yesterday,
          app_version: item.app_version,
          app_channel: item.app_channel,
          active_users_count: active_item&.users_count,
          checkin_users_count: item.users_count,
          filter_type: 2
        )
      end
    end

    Log::UserCheckinDayStat.bulk_insert(stats, use_provided_primary_key: true) if stats.present?
  end
end

class Log::NonActiveUserDayStat < ApplicationLogRecord

  scope :between_date, ->(start_at, end_at) { where(stat_date: start_at..end_at)}

  # 统计前一天或指定日期的非活跃用户(30天没有登录的用户)
  # date: 不指定时为统计昨天，指定时统计指定日期
  def self.stat(date = nil)
    yesterday = date || Time.now.to_date.yesterday
    stats = []

    nonactive_date = yesterday - 30.days

    stats += Log::NonActiveUserDayStat.all_stat(yesterday, nonactive_date)
    stats += Log::NonActiveUserDayStat.filter_stat(yesterday, nonactive_date, "channel")
    stats += Log::NonActiveUserDayStat.filter_stat(yesterday, nonactive_date, "version")
    stats += Log::NonActiveUserDayStat.filter_all_stat(yesterday, nonactive_date)

    Log::NonActiveUserDayStat.bulk_insert(stats)
  end

  def self.all_stat(yesterday, nonactive_date)
    stats = []
    # 全部
    users_count = User.count
    nonactive_users_count = User.where("DATE(current_signin_at) < ?", nonactive_date)
      .use_index_ccc
      .count
    stats << Log::NonActiveUserDayStat.new(
      stat_date: yesterday,
      users_count: users_count,
      nonactive_users_count: nonactive_users_count
    )
    stats
  end

  # 渠道或版本
  def self.filter_stat(yesterday, nonactive_date, filter_type)
    stats = []
    filter_types = SystemEnum.enums_by_type(filter_type).map(&:name)
    users_count = User.select("COUNT(id) as users_count")
      .use_index_ccc
      .select("create_app_#{filter_type}")
      .group("create_app_#{filter_type}")
      .to_a
    group_users_count = users_count.group_by{|i| i.send("create_app_#{filter_type}")}
    nonactive_users_count = User.select("COUNT(id) as users_count")
      .where("DATE(current_signin_at) < ?", nonactive_date)
      .use_index_ccc
      .select("create_app_#{filter_type}")
      .group("create_app_#{filter_type}")
      .to_a
    group_nonactive_users_count = nonactive_users_count.group_by{|i| i.send("create_app_#{filter_type}")}
    filter_types.each do |t|
      stats << Log::NonActiveUserDayStat.new(
        stat_date: yesterday,
        users_count: group_users_count.dig(t)&.first&.users_count || 0,
        nonactive_users_count: group_nonactive_users_count.dig(t)&.first&.users_count || 0,
        "app_#{filter_type}": t,
        filter_type: 1
      )
    end
    stats
  end

  # 渠道和版本
  def self.filter_all_stat(yesterday, nonactive_date)
    stats = []
    filter_types = SystemEnumRelation.cache_list
    users_count = User.select("COUNT(id) as users_count")
      .use_index_ccc
      .select(:create_app_channel, :create_app_version)
      .group(:create_app_channel, :create_app_version)
      .to_a
    group_users_count = users_count.group_by{|i| "#{i.create_app_channel}___#{i.create_app_version}"}
    nonactive_users_count = User.select("COUNT(id) as users_count")
      .where("DATE(current_signin_at) < ?", nonactive_date)
      .use_index_ccc
      .select(:create_app_channel, :create_app_version)
      .group(:create_app_channel, :create_app_version)
      .to_a
    group_nonactive_users_count = nonactive_users_count.group_by{|i| "#{i.create_app_channel}___#{i.create_app_version}"}
    filter_types.each do |t|
      stats << Log::NonActiveUserDayStat.new(
        stat_date: yesterday,
        users_count: group_users_count.dig("#{t.channel}___#{t.version}")&.first&.users_count || 0,
        nonactive_users_count: group_nonactive_users_count.dig("#{t.channel}___#{t.version}")&.first&.users_count || 0,
        app_channel: t.channel,
        app_version: t.version,
        filter_type: 2
      )
    end
    stats
  end

end

class Log::UserTrafficDayStat < ApplicationLogRecord

  scope :between_date, -> (start_at, end_at){ where(stat_date: start_at..end_at) }

  # 2018-06-27 换回原来的通过去向日志流量统计 --！
  # def self.stat(date = nil)
  #   yesterday = date || Time.now.to_date.yesterday
  #   stats = []
  #
  #   traffic_range = {
  #     t10m: [0, 10000000],
  #     t11_20m: [10000001, 20000000],
  #     t21_50m: [20000001, 50000000],
  #     t51_100m: [50000001, 100000000],
  #     t101_200m: [100000001, 200000000],
  #     t201_500m: [2000000001, 5000000000],
  #     t501_1g: [5000000001, 10000000000],
  #     t1_2g: [1000000001, 2000000000],
  #     t2_5g: [2000000001, 5000000000],
  #     t5_10g: [5000000001, 10000000000],
  #     t10g: [100000000001, 9900000000000],
  #   }
  #
  #   except_keys = [:app_channel, :app_version]
  #   item = { app_channel: nil, app_version: nil }
  #   item_channel = Hash.new{|k,v| k[v] = {}}
  #   item_version = Hash.new{|k,v| k[v] = {}}
  #   item_channel_version = Hash.new{|k,v| k[v] = {}}
  #
  #   traffic_range.each do |key, range|
  #     start_of, end_of = range
  #
  #     # 全部
  #     result = ApplicationLogRecord.using_db(DB_LOG_SLAVE1) do
  #       Log::UserTrafficLog
  #         .between_used_bytes(start_of, end_of)
  #         .by_date(yesterday)
  #         .count_users
  #     end
  #     item[key] = result
  #
  #     # 渠道
  #     channels = SystemEnum.enums_by_type("channel").map(&:name)
  #     channels.each do |channel|
  #       result = ApplicationLogRecord.using_db(DB_LOG_SLAVE1) do
  #         Log::UserTrafficLog
  #           .where(app_channel: channel)
  #           .between_used_bytes(start_of, end_of)
  #           .by_date(yesterday)
  #           .count_users
  #       end
  #       item_channel[channel][key] = result
  #     end
  #
  #     # 版本
  #     versions = SystemEnum.enums_by_type("version").map(&:name)
  #     versions.each do |version|
  #       result = ApplicationLogRecord.using_db(DB_LOG_SLAVE1) do
  #         Log::UserTrafficLog
  #           .where(app_version: version)
  #           .between_used_bytes(start_of, end_of)
  #           .by_date(yesterday)
  #           .count_users
  #       end
  #       item_version[version][key] = result
  #     end
  #
  #    # 渠道和版本
  #    app_channel_versions = SystemEnumRelation.cache_list
  #    app_channel_versions.each do |item|
  #      result = ApplicationLogRecord.using_db(DB_LOG_SLAVE1) do
  #        Log::UserTrafficLog
  #          .where(app_channel: item.channel, app_version: item.version)
  #          .between_used_bytes(start_of, end_of)
  #          .by_date(yesterday)
  #          .count_users
  #      end
  #      item_channel_version["#{item.channel}___#{item.version}"][key] = result
  #    end
  #   end
  #
  #   users_count = item.except(*except_keys).map{|k,v| v}.sum
  #   stats << Log::UserTrafficDayStat.new(
  #     stat_date: yesterday,
  #     traffic_period: item.to_json(except: except_keys),
  #     users_count: users_count,
  #     filter_type: 0
  #   )
  #
  #   item_channel.each do |k, item|
  #     users_count = item.except(*except_keys).map{|k,v| v}.sum
  #     stats << Log::UserTrafficDayStat.new(
  #       stat_date: yesterday,
  #       traffic_period: item.to_json(except: except_keys),
  #       users_count: users_count,
  #       app_channel: k,
  #       filter_type: 1
  #     )
  #   end
  #
  #   item_version.each do |k, item|
  #     users_count = item.except(*except_keys).map{|k,v| v}.sum
  #     stats << Log::UserTrafficDayStat.new(
  #       stat_date: yesterday,
  #       traffic_period: item.to_json(except: except_keys),
  #       users_count: users_count,
  #       app_version: k,
  #       filter_type: 1
  #     )
  #   end
  #
  #   item_channel_version.each do |k, item|
  #     channel, version = k.split('___')
  #     users_count = item.except(*except_keys).map{|k,v| v}.sum
  #     stats << Log::UserTrafficDayStat.new(
  #       stat_date: yesterday,
  #       traffic_period: item.to_json(except: except_keys),
  #       users_count: users_count,
  #       app_channel: channel,
  #       app_version: version,
  #       filter_type: 2
  #     )
  #   end
  #
  #   Log::UserTrafficDayStat.bulk_insert(stats)
  # end

  def self.stat(date = nil)
    yesterday = date || Time.now.to_date.yesterday

    result = TrendStatService.stat_traffic_day(DateUtils.time2str(yesterday.beginning_of_day))

    return if result.blank?

    traffic_range = {
      t10m: 0,
      t11_20m: 0,
      t21_50m: 0,
      t51_100m: 0,
      t101_200m: 0,
      t201_500m: 0,
      t501_1g: 0,
      t1_2g: 0,
      t2_5g: 0,
      t5_10g: 0,
      t10g: 0
    }
    except_keys = [:app_channel, :app_version]
    stats = []

    # 全部
    item = result.select{|i| i[:app_channel].nil? && i[:app_version].nil? }.first
    users_count = item.except(*except_keys).map{|k,v| v}.sum
    if item.present?
      stats << Log::UserTrafficDayStat.new(
        stat_date: yesterday,
        traffic_period: item.to_json(except: except_keys),
        users_count: users_count,
        filter_type: 0
      )
    end

    # 渠道
    filter_items = SystemEnum.enums_by_type("channel").map(&:name)
    items = result.select{|i| !i[:app_channel].nil? && i[:app_version].nil? }.group_by{|i| i[:app_channel]}
    filter_items.each do |filter_item|
      item = if items.has_key?(filter_item)
        traffic_range.merge(items[filter_item].first)
      else
        traffic_range
      end
      users_count = item.except(*except_keys).map{|k,v| v}.sum
      stats << Log::UserTrafficDayStat.new(
        stat_date: yesterday,
        traffic_period: item.to_json(except: except_keys),
        users_count: users_count,
        app_channel: filter_item,
        filter_type: 1
      )
    end

    # 版本
    filter_items = SystemEnum.enums_by_type("version").map(&:name)
    items = result.select{|i| i[:app_channel].nil? && !i[:app_version].nil? }.group_by{|i| i[:app_version]}
    filter_items.each do |filter_item|
      item = if items.has_key?(filter_item)
        traffic_range.merge(items[filter_item].first)
      else
        traffic_range
      end
      users_count = item.except(*except_keys).map{|k,v| v}.sum
      stats << Log::UserTrafficDayStat.new(
        stat_date: yesterday,
        traffic_period: item.to_json(except: except_keys),
        users_count: users_count,
        app_version: filter_item,
        filter_type: 1
      )
    end

    # 渠道和版本
    filter_items = SystemEnumRelation.cache_list
    items = result.select{|i| !i[:app_channel].nil? && !i[:app_version].nil? }
      .group_by{|i| "#{i[:app_channel]}___#{i[:app_version]}" }
    filter_items.each do |filter_item|
      join_cv = "#{filter_item.channel}___#{filter_item.version}"
      item = if items.has_key?(join_cv)
        traffic_range.merge(items[join_cv].first)
      else
        traffic_range
      end
      users_count = item.except(*except_keys).map{|k,v| v}.sum
      stats << Log::UserTrafficDayStat.new(
        stat_date: yesterday,
        traffic_period: item.to_json(except: except_keys),
        users_count: users_count,
        app_channel: filter_item.channel,
        app_version: filter_item.version,
        filter_type: 2
      )
    end

    Log::UserTrafficDayStat.bulk_insert(stats)
  end

end

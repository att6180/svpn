class Log::SplashScreenAdHourStat < ApplicationLogRecord

  attr_accessor :total_clicked_count

  scope :by_date, -> (date) { where(stat_date: date) }
  scope :between_date, -> (start_date, end_date) { where(stat_date: start_date..end_date) }
  scope :between_hour, -> (start_at, end_at) { where(stat_hour: start_at..end_at) }
  scope :sorted, -> { order(stat_date: :asc, stat_hour: :asc)}
  scope :group_by_date, -> {
    select("stat_date, SUM(showed_count) as total_showed_count")
      .group(:stat_date)
      .order(stat_date: :desc)
  }
  scope :group_by_month, -> {
    select("YEAR(stat_date) as y, MONTH(stat_date) as m, SUM(showed_count) as total_showed_count")
      .group("y, m")
      .order("y DESC, m DESC")
  }

  # 统计昨天每小时的开屏广告展示次数
  def self.stat(date = nil)
    yesterday = date || Time.now.to_date.yesterday

    stats = []

    items = Log::SplashScreenAdLog.select("HOUR(created_at) as h, COUNT(id) as showed_count")
      .group("h")
      .by_date(yesterday)

    items.each do |item|
      stats << Log::SplashScreenAdHourStat.new(
        stat_date: yesterday,
        stat_hour: item.h,
        showed_count: item.showed_count
      )
    end

    # 检查缺失的小时段，并补充
    StatService.missing_hours(items.map(&:h)) do |h|
      stats << Log::SplashScreenAdHourStat.new(
        stat_date: yesterday,
        stat_hour: h,
        showed_count: 0
      )
    end

    Log::SplashScreenAdHourStat.bulk_insert(stats)
  end

end

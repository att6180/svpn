class Log::ActiveUserPeriodMonthStat < ApplicationLogRecord

  scope :between_month, -> (start_at, end_at) {
    where("DATE(CONCAT(stat_year, '-', stat_month, '-01')) BETWEEN ? AND ?", start_at, end_at)
  }

  def self.stat(date = nil)
    stat_date = date || Time.now.to_date.prev_month
    
    stat_range = {
      d_1: 0,
      d2_5: 0,
      d6_10: 0,
      d11_15: 0,
      d16_20: 0,
      d21_25: 0,
      d26_31: 0
    }
    except_keys = [:app_channel, :app_version]
    stats = []

    # 全部
    result = TrendStatService.stat_active_users_month(DateUtils.time2str(stat_date.beginning_of_month))
    return if result.blank?
    item = result.select{|i| i[:app_channel].nil? && i[:app_version].nil? }.first
    users_count = item.except(*except_keys).map{|k,v| v}.sum
    if item.present?
      stats << Log::ActiveUserPeriodMonthStat.new(
        stat_year: stat_date.year,
        stat_month: stat_date.month,
        period: item.to_json(except: except_keys),
        users_count: users_count,
        filter_type: 0
      )
    end

    # 渠道
    channel_items = SystemEnum.enums_by_type("channel").map(&:name)
    channel_items.each do |channel|
      result = TrendStatService.stat_active_users_month(DateUtils.time2str(stat_date.beginning_of_month), channel, nil)
      items = result.select{|i| !i[:app_channel].nil? && i[:app_version].nil? }.group_by{|i| i[:app_channel]}
      item = if items.has_key?(channel)
        stat_range.merge(items[channel].first)
      else
        stat_range
      end
      users_count = item.except(*except_keys).map{|k,v| v}.sum
      stats << Log::ActiveUserPeriodMonthStat.new(
        stat_year: stat_date.year,
        stat_month: stat_date.month,
        period: item.to_json(except: except_keys),
        users_count: users_count,
        app_channel: channel,
        filter_type: 1
      )
    end

    # 版本
    version_items = SystemEnum.enums_by_type("version").map(&:name)
    version_items.each do |version|
      result = TrendStatService.stat_active_users_month(DateUtils.time2str(stat_date.beginning_of_month), nil, version)
      items = result.select{|i| i[:app_channel].nil? && !i[:app_version].nil? }.group_by{|i| i[:app_version]}
      item = if items.has_key?(version)
        stat_range.merge(items[version].first)
      else
        stat_range
      end
      users_count = item.except(*except_keys).map{|k,v| v}.sum
      stats << Log::ActiveUserPeriodMonthStat.new(
        stat_year: stat_date.year,
        stat_month: stat_date.month,
        period: item.to_json(except: except_keys),
        users_count: users_count,
        app_version: version,
        filter_type: 1
      )
    end

    # 渠道和版本
    channel_version_items = SystemEnumRelation.all.pluck(:channel, :version)
    channel_version_items.each do |channel, version|
      result = TrendStatService.stat_active_users_month(DateUtils.time2str(stat_date.beginning_of_month), channel, version)
      items = result.select{|i| !i[:app_channel].nil? && !i[:app_version].nil? }.group_by{|i| "#{i[:app_channel]}___#{i[:app_version]}" }
      join_cv = "#{channel}___#{version}"
      item = if items.has_key?(join_cv)
        stat_range.merge(items[join_cv].first)
      else
        stat_range
      end
      users_count = item.except(*except_keys).map{|k,v| v}.sum
      stats << Log::ActiveUserPeriodMonthStat.new(
        stat_year: stat_date.year,
        stat_month: stat_date.month,
        period: item.to_json(except: except_keys),
        users_count: users_count,
        app_channel: channel,
        app_version: version,
        filter_type: 2
      )
    end

    Log::ActiveUserPeriodMonthStat.bulk_insert(stats)
  end

end

class Log::SplashScreenAdLog < ApplicationLogRecord

  scope :by_date, -> (date) { where(created_date: date) }
  scope :group_by_hour, -> { select("HOUR(created_at) as h").group('h').order("h ASC") }
  scope :group_by_date_and_hour, -> {
    select("DATE(created_date) as d, HOUR(created_at) as h")
      .group('d, h')
      .order("d DESC, h ASC")
  }

end

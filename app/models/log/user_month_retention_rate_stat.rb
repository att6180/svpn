class Log::UserMonthRetentionRateStat < ApplicationLogRecord
  include Uuidable

  scope :between_months, -> (start_at, end_at) { where(stat_date: start_at..end_at) }

  scope :stat_result_user, -> {
    select(
      "stat_date, SUM(users_count) as total_users_count, SUM(first_month_users_count) as total_first_month_users_count, " \
      "SUM(second_month_users_count) as total_second_month_users_count, SUM(third_month_users_count) as total_third_month_users_count, " \
      "SUM(fourth_month_users_count) as total_fourth_month_users_count")
      .group("stat_date") }


  # 统计指定某月的留存数据
  def self.onmonth_stat(date = nil)

    first_month = date
    end_month = date.end_of_month

    stats = Log::UserMonthRetentionRateStat.where(stat_date: first_month.to_date)

    return if stats.blank?

    # 次月, 汇总每个渠道和版本的去重用户数
    collection1 = (first_month + 2.month).today? ? NodeConnectionUserDayLog.retention_collection(first_month, end_month, first_month + 1.month, end_month + 1.month) : nil
    # 2月, 汇总每个渠道和版本的去重用户数
    collection2 = (first_month + 3.month).today? ? NodeConnectionUserDayLog.retention_collection(first_month, end_month, first_month + 2.month, end_month + 2.month) : nil
    # 3月, 汇总每个渠道和版本的去重用户数
    collection3 = (first_month + 4.month).today? ? NodeConnectionUserDayLog.retention_collection(first_month, end_month, first_month + 3.month, end_month + 3.month) : nil
    # 4月, 汇总每个渠道和版本的去重用户数
    collection4 = (first_month + 5.month).today? ? NodeConnectionUserDayLog.retention_collection(first_month, end_month, first_month + 4.month, end_month + 4.month) : nil

    stats.each do |stat|
      first_month_users_count = UserRetentionRateStat.get_users_count(collection1, stat.app_channel, stat.app_version)
      second_month_users_count = UserRetentionRateStat.get_users_count(collection2, stat.app_channel, stat.app_version)
      third_month_users_count = UserRetentionRateStat.get_users_count(collection3, stat.app_channel, stat.app_version)
      fourth_month_users_count = UserRetentionRateStat.get_users_count(collection4, stat.app_channel, stat.app_version)

      stat.first_month_users_count = first_month_users_count if (first_month + 2.month).today?
      stat.second_month_users_count = second_month_users_count if (first_month + 3.month).today?
      stat.third_month_users_count = third_month_users_count if (first_month + 4.month).today?
      stat.fourth_month_users_count = fourth_month_users_count if (first_month + 5.month).today?

      stat.save if stat.changed?
    end
  end

  # 每月统计，统计前1-4月的用户留存数据
  def self.stat(date = nil)
    last_month = date || Time.now.prev_month.beginning_of_month

    Log::UserMonthRetentionRateStat.stat_by_month(last_month)

    # 循环最近9月（不包括当前月）
    # 从用户表查找注册日期等于每月范围内的用户
    # 同时统计这些用户的 最后连接时间 在 从注册日 起的 第1-4月的数量
    first_month = last_month - 1.month
    end_month = last_month - 5.month
    while first_month > end_month
      Log::UserMonthRetentionRateStat.onmonth_stat(first_month)
      first_month = first_month.prev_month
    end
  end

  # 统计指定月的新增用户数
  def self.stat_by_month(date)
    stats = []

    end_date = date.end_of_month

    # 全部
    users_count = User.where(created_at: date..end_date).count
    stats << Log::UserMonthRetentionRateStat.new(
      id: Utils::Gen.generate_uuid,
      stat_date: date,
      users_count: users_count
    )

    # 渠道
    user_items = User
      .select("create_app_channel as app_channel, count(*) as users_count")
      .group(:create_app_channel)
      .where(created_at: date..end_date)
      .to_a.map(&:serializable_hash)

    user_items.each do |item|
      stats << Log::UserMonthRetentionRateStat.new(
        id: Utils::Gen.generate_uuid,
        stat_date: date,
        app_channel: item["app_channel"],
        users_count: item["users_count"] || 0,
        filter_type: 1
      )
    end

    # 版本
    user_items = User
      .select("create_app_version as app_version, count(*) as users_count")
      .group(:create_app_version)
      .where(created_at: date..end_date)
      .to_a.map(&:serializable_hash)

    user_items.each do |item|
      stats << Log::UserMonthRetentionRateStat.new(
        id: Utils::Gen.generate_uuid,
        stat_date: date,
        app_version: item["app_version"],
        users_count: item["users_count"] || 0,
        filter_type: 1
      )
    end

    # 渠道和版本
    user_items = User
      .select("create_app_channel as app_channel, create_app_version as app_version, count(*) as users_count")
      .group(:create_app_version, :create_app_channel)
      .where(created_at: date..end_date)
      .to_a.map(&:serializable_hash)

    user_items.each do |item|
      stats << Log::UserMonthRetentionRateStat.new(
        id: Utils::Gen.generate_uuid,
        stat_date: date,
        app_version: item["app_version"],
        app_channel: item["app_channel"],
        users_count: item["users_count"] || 0,
        filter_type: 2
      )
    end

    Log::UserMonthRetentionRateStat.bulk_insert(stats, use_provided_primary_key: true) if stats.present?
  end

end

class Log::UserChangeLog < ApplicationLogRecord
  #enum change_type: { change_username: 1, change_password: 2, forgot_password: 3 }, _prefix: :types
  CHANGE_USERNAME = 1
  CHANGE_PASSWORD = 2
  FORGOT_PASSWORD = 3
  CHANGE_USERNAME_COINS = 4
end

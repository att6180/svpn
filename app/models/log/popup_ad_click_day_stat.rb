class Log::PopupAdClickDayStat < ApplicationLogRecord

  scope :between_date, -> (start_date, end_date) { where(stat_date: start_date..end_date) }
  scope :group_by_month, -> {
    select("YEAR(stat_date) as y, MONTH(stat_date) as m, SUM(clicked_count) as total_clicked_count")
      .group("y, m")
      .order("y DESC, m DESC")
  }

  # 统计昨天的弹出广告点击次数
  def self.stat(date = nil)
    yesterday = date || Time.now.to_date.yesterday

    stats = []

    items = Log::PopupAdClickLog.select("COUNT(id) as clicked_count").by_date(yesterday)

    items.each do |item|
      stats << Log::PopupAdClickDayStat.new(
        stat_date: yesterday,
        clicked_count: item.clicked_count
      )
    end

    Log::PopupAdClickDayStat.bulk_insert(stats)
  end

end

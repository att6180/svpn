class Log::WebsiteNavigationClickDayStat < ApplicationLogRecord

  scope :between_date, -> (start_date, end_date) { where(stat_date: start_date..end_date) }
  scope :group_by_website_name, -> {
    select("website_name, SUM(clicked_count) as total_clicked_count")
      .group(:website_name)
  }
  
  # 统计昨天各个导航网址点击次数
  def self.stat(date = nil)
    yesterday = date || Time.now.to_date.yesterday
    stats = []

    Log::WebsiteNavigationClickDayStat.using_db(DB_LOG_SLAVE1) do
      items = Log::WebsiteNavigationClickLog
        .select("website_name, is_domestic, info_type, count(id) as clicked_count")
        .group(:website_name, :is_domestic, :info_type)
        .by_date(yesterday)

      items.each do |item|
        stats << Log::WebsiteNavigationClickDayStat.new(
          stat_date: yesterday,
          website_name: item.website_name,
          is_domestic: item.is_domestic,
          info_type: item.info_type,
          clicked_count: item.clicked_count
        )
      end

      Log::WebsiteNavigationClickDayStat.bulk_insert(stats)
    end
  end

end

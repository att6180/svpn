class Log::UserTrafficLog < ApplicationLogRecord

  scope :by_date, -> (date) { where(stat_date: date) }
  scope :between_used_bytes, -> (start_of, end_of) { where(used_bytes: start_of..end_of) }
  scope :count_users, -> () { count(:user_id) }
end

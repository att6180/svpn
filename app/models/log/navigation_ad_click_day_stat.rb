class Log::NavigationAdClickDayStat < ApplicationLogRecord
    
  scope :between_date, -> (start_date, end_date) { where(stat_date: start_date..end_date) }
  scope :group_by_ad_name, -> {
    select("ad_name, SUM(clicked_count) as total_clicked_count")
      .group(:ad_name)
  }
  
  # 统计昨天各个导航广告点击次数
  def self.stat(date = nil)
    yesterday = date || Time.now.to_date.yesterday
    stats = []

    Log::NavigationAdClickDayStat.using_db(DB_LOG_SLAVE1) do
      items = Log::NavigationAdClickLog
        .select("ad_name, count(id) as clicked_count")
        .group(:ad_name) 
        .by_date(yesterday)

      items.each do |item|
        stats << Log::NavigationAdClickDayStat.new(
          stat_date: yesterday,
          ad_name: item.ad_name,
          clicked_count: item.clicked_count
        )
      end

      Log::NavigationAdClickDayStat.bulk_insert(stats)
    end
  end

end

class Log::OperationSummaryMonthStat < ApplicationLogRecord

  scope :between_month, -> (start_at, end_at) {
    where("DATE(CONCAT(stat_year, '-', stat_month, '-01')) BETWEEN ? AND ?", start_at, end_at)
  }

  scope :stat_result, -> {
    select(
      "stat_year, stat_month, SUM(total_recharge_amount) as t_total_recharge_amount, " \
      "SUM(new_users_count) as t_new_users_count, SUM(active_users_count) as t_active_users_count ")
      .group(:stat_year, :stat_month)
  }

  ## 统计上个月的如下数据:
  #	本月充值金额
  #	本月新增人数
  #	本月活跃人数
  def self.stat(date = nil)
    last_month = date || Time.now.last_month
    year = last_month.year
    month = last_month.month

    stats = []
    filter_types = SystemEnumRelation.cache_list
    # 查询全部
    filter_types.insert(0, OpenStruct.new(nil))

    filter_types.each do |t|
      channel = t.channel
      version = t.version

      # 充值金额
      total_recharge_amount = TransactionMonthStat
        .where(
          year: year,
          month: month,
          app_channel: channel,
          app_version: version
        )
        .order(filter_type: :asc)
        .take
        &.total_recharge_amount

      # 新增人数
      new_users_count = NewUserMonthStat
        .where(
          stat_year: year,
          stat_month: month,
          app_channel: channel,
          app_version: version
        )
        .order(filter_type: :asc)
        .take
        &.users_count

      # 活跃人数
      active_users_count = ActiveUserMonthStat
        .where(
          stat_year: year,
          stat_month: month,
          app_channel: channel,
          app_version: version
        )
        .order(filter_type: :asc)
        .take
        &.users_count

      stats << Log::OperationSummaryMonthStat.new(
        stat_year: year,
        stat_month: month,
        total_recharge_amount: total_recharge_amount,
        new_users_count: new_users_count,
        active_users_count: active_users_count,
        app_channel: channel,
        app_version: version,
        platform_id: t.platform&.fetch(:id),
        channel_account_id: t.channel_account&.fetch(:id),
        package_id: t.package&.fetch(:id),
        filter_type: ((channel.nil? && version.nil?) ? 0 : 1)
      )
    end

    Log::OperationSummaryMonthStat.bulk_insert(stats)
  end

end

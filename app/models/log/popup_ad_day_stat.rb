class Log::PopupAdDayStat < ApplicationLogRecord

  attr_accessor :total_clicked_count

  scope :between_date, -> (start_date, end_date) { where(stat_date: start_date..end_date) }
  scope :group_by_month, -> {
    select("YEAR(stat_date) as y, MONTH(stat_date) as m, SUM(showed_count) as total_showed_count")
      .group("y, m")
      .order("y DESC, m DESC")
  }

  # 统计昨天的弹出广告展示次数
  def self.stat(date = nil)
    yesterday = date || Time.now.to_date.yesterday

    stats = []

    items = Log::PopupAdLog.select("COUNT(id) as showed_count").by_date(yesterday)

    items.each do |item|
      stats << Log::PopupAdDayStat.new(
        stat_date: yesterday,
        showed_count: item.showed_count
      )
    end

    Log::PopupAdDayStat.bulk_insert(stats)
  end

end

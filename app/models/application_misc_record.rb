class ApplicationMiscRecord < ActiveRecord::Base
  self.abstract_class = true

  establish_connection DB_MISC
end

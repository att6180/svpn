class SystemEnumRelation < ApplicationRecord

  CACHE_KEY = "system_enum_relations"

  belongs_to :platform, class_name: "SystemEnum", foreign_key: "platform_id"
  belongs_to :channel_account, class_name: "SystemEnum", foreign_key: "channel_account_id"
  belongs_to :package, class_name: "SystemEnum", foreign_key: "package_id"

  validates :platform, :channel_account, :package, :channel, :version, presence: true

  scope :sorted, -> { order(channel: :asc, version: :asc)}

  after_commit :after_commit_trace

  def self.cache_list
    Rails.cache.fetch(CACHE_KEY, expires_in: 7.days) do
      list = []
      SystemEnumRelation.includes(:platform, :channel_account, :package).each do |relation|
        r = OpenStruct.new
        r.platform = {id: relation.platform_id, name: relation.platform&.name}
        r.channel_account = {id: relation.channel_account_id, name: relation.channel_account&.name}
        r.package = {id: relation.package_id, name: relation.package&.name}
        r.channel = relation.channel
        r.version = relation.version
        list << r
      end
      list
    end
  end

  private

  def after_commit_trace
    Rails.cache.delete(CACHE_KEY)
  end

end

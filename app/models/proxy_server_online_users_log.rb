class ProxyServerOnlineUsersLog < ApplicationLogRecord

  belongs_to :node

  scope :between_date, ->(start_at, end_at) { where(created_at: start_at.beginning_of_day..end_at.end_of_day) }
  scope :by_date, ->(date) { where("DATE(created_at) = ?", date) }
  scope :by_hour_minute, ->(hour_minute) { where("DATE_FORMAT(created_at, '%H:%i') = ?", hour_minute) }
  scope :group_by_hour_minute, -> {
    select("DATE_FORMAT(created_at, '%H:%i') as hm, SUM(online_users_count) as online_count")
      .group("DATE_FORMAT(created_at, '%H:%i')")
      .order("DATE_FORMAT(created_at, '%H:%i') ASC")
  }

end

class UserIapVerificationLog < ApplicationLogRecord

  include Uuidable

  belongs_to :user

end

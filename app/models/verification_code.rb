class VerificationCode < ApplicationMiscRecord

  CACHE_KEY = "verification_code_replay_"

  enum code_type: { modify_username: 0, modify_password: 1, reset_password: 2, register: 3 }

  scope :code_for_telephone, ->(telephone, type) { where(telephone: telephone, is_used: false, code_type: type).where("expired_at >= ?", Time.now) }
  # 获取手机号1小时内的验证码数量
  scope :count_in_hour, ->(telephone) { where(telephone: telephone).where("created_at > ?", Time.now - 1.hours).count }

  def self.create_code(telephone, code, code_type)
    VerificationCode.create(
      telephone: telephone,
      verification_code: code,
      expired_at: Time.now + 600.seconds,
      code_type: code_type
    )
  end

  def is_expired?
    self.expired_at < Time.now ? true : false
  end

  def is_correct?(code)
    cache_key = "#{CACHE_KEY}#{self.id}"
    replay_count = RedisCache.incr(cache_key, 600)
    return false if replay_count >= Setting.sms_failed_replay_max_count.to_i
    self.verification_code == code
  end

  def used!
    self.update_columns(is_used: true, used_at: Time.now)
  end

end

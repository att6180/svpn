class NodeContinent < NodeArea

  CACHE_KEY = "node_continents"

  after_commit :after_commit_trace

  def self.cache_list
    Rails.cache.fetch(CACHE_KEY, expires_in: 30.days) do
      NodeContinent.all
    end
  end

  private

  def after_commit_trace
    Rails.cache.delete(CACHE_KEY)
  end
end

class ActiveUserHourStat < ApplicationLogRecord

  scope :by_date, -> (date) { where(stat_date: date) }
  scope :sorted, -> { order(stat_date: :asc, stat_hour: :asc)}
  scope :group_by_hour, -> { group(:stat_hour) }
  scope :select_sum_users, -> { select("SUM(users_count) as users_count") }

  # 统计昨天每小时的活跃用户
  def self.stat(date = nil)
    yesterday = date || Time.now.to_date.yesterday

    stats = []
    items = []

    # 全部
    items = ApplicationLogRecord.using_db(DB_LOG_SLAVE1) do
      UserActiveHourLog.select("COUNT(user_id) as users_count")
      .by_created_date(yesterday)
      .group_by_hour
    end

    items.each do |item|
      stats << ActiveUserHourStat.new(
        stat_date: yesterday,
        stat_hour: item.h,
        users_count: item.users_count
      )
    end
    # 检查缺失的小时段，并补充
    StatService.missing_hours(items.map(&:h)) do |h|
      stats << ActiveUserHourStat.new(
        stat_date: yesterday,
        stat_hour: h,
        users_count: 0
      )
    end

    # 渠道
    items = ApplicationLogRecord.using_db(DB_LOG_SLAVE1) do
      UserActiveHourLog.select("COUNT(user_id) as users_count")
      .by_created_date(yesterday)
      .group_by_hour
      .select(:app_channel)
      .group(:app_channel)
    end

    # 检查每个渠道中缺失的小时段并补充
    hash_list = StatService.generate_hash_missing_hours(
      items.group_by{|item| item.app_channel}.map{|key, val| [key, val.map(&:h)]}
    )
    if items.present?
      items.each do |item|
        stats << ActiveUserHourStat.new(
          stat_date: yesterday,
          stat_hour: item.h,
          app_channel: item.app_channel,
          users_count: item.users_count,
          filter_type: 1
        )
      end
    end
    # 补充每个渠道的小时段
    StatService.hash_missing_hours(hash_list) do |channel, h|
      stats << ActiveUserHourStat.new(
        stat_date: yesterday,
        stat_hour: h,
        app_channel: channel,
        users_count: 0,
        filter_type: 1
      )
    end

    # 版本
    items = ApplicationLogRecord.using_db(DB_LOG_SLAVE1) do
      UserActiveHourLog.select("COUNT(user_id) as users_count")
      .by_created_date(yesterday)
      .group_by_hour
      .select(:app_version)
      .group(:app_version)
    end

    # 检查每个版本中缺失的小时段并补充
    hash_list = StatService.generate_hash_missing_hours(
      items.group_by{|item| item.app_version}
        .map{|key, val| [key, val.map(&:h)]}
    )
    if items.present?
      items.each do |item|
        stats << ActiveUserHourStat.new(
          stat_date: yesterday,
          stat_hour: item.h,
          app_version: item.app_version,
          users_count: item.users_count,
          filter_type: 1
        )
      end
    end
    # 补充每个渠道的小时段
    StatService.hash_missing_hours(hash_list) do |version, h|
      stats << ActiveUserHourStat.new(
        stat_date: yesterday,
        stat_hour: h,
        app_version: version,
        users_count: 0,
        filter_type: 1
      )
    end

    # 渠道和版本
    items = ApplicationLogRecord.using_db(DB_LOG_SLAVE1) do
      UserActiveHourLog.select("COUNT(user_id) as users_count")
      .by_created_date(yesterday)
      .group_by_hour
      .select(:app_channel, :app_version)
      .group(:app_channel, :app_version)
    end
    # 检查每个版本中缺失的小时段并补充
    hash_list = StatService.generate_hash_missing_hours(
      items.group_by{|item| "#{item.app_channel}___#{item.app_version}"}
        .map{|key, val| [key, val.map(&:h)]}
    )

    if items.present?
      items.each do |item|
        stats << ActiveUserHourStat.new(
          stat_date: yesterday,
          stat_hour: item.h,
          app_channel: item.app_channel,
          app_version: item.app_version,
          users_count: item.users_count,
          filter_type: 2
        )
      end
    end
    # 补充每个渠道的小时段
    StatService.hash_missing_hours(hash_list) do |cv, h|
      cv_array = cv.split('___')
      stats << ActiveUserHourStat.new(
        stat_date: yesterday,
        stat_hour: h,
        app_channel: cv_array[0],
        app_version: cv_array[1],
        users_count: 0,
        filter_type: 2
      )
    end
    ActiveUserHourStat.bulk_insert(stats)
  end
end

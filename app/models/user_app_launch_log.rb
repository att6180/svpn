class UserAppLaunchLog < ApplicationLogRecord
  include Uuidable

  belongs_to :user
  has_many :user_operation_logs

end

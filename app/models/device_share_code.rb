class DeviceShareCode < ApplicationRecord
  belongs_to :user

  def self.create_ignore_by_sql(*params)
    now = Time.now
    sql = "INSERT IGNORE INTO device_share_codes(user_id, sdid, uuid, apiid, created_at, updated_at) VALUES(?, ?, ?, ?, ?, ?)"
    connection.execute(
      send(
        :sanitize_sql_array,
        [sql] + params + [now, now]
      )
    )
  end
end

class Plan < ApplicationRecord

  CACHE_KEY = "plans_"

  TIME_TYPES = {monthly: 0, quarterly: 1, half_yearly: 2, yearly: 3}
  enum time_type: TIME_TYPES

  has_many :transaction_logs
  belongs_to :node_type, optional: true

  scope :enabled, -> { where(is_enabled: true) }
  scope :iap, -> { where(is_iap: true) }
  scope :except_iap, -> { where(is_iap: false) }
  scope :except_audit, -> { where(is_audit: false) }
  scope :sorted_by_id, -> { order(id: :asc) }
  scope :sorted, -> { order(is_enabled: :desc, id: :asc )}

  validates :name, :price, presence: true
  validates :time_type, :node_type_id, presence: true, if: Proc.new {|p| p.is_regular_time?}
  validates :coins, presence: true, if: Proc.new {|p| !p.is_regular_time?}
  validate :iap_id_uniqueness_create, on: :create
  validate :iap_id_uniqueness_update, on: :update

  before_validation :strip_whitespace
  before_validation :default_value
  after_commit :after_commit_trace

  # 公用套餐接口，会返回所有套餐类型（包括IAP和非IAP)
  def self.by_version(platform, app_version, app_version_number)
    cache_key = "#{CACHE_KEY}#{platform}_#{app_version}_#{app_version_number}"
    result = Rails.cache.fetch(cache_key, expires_in: 7.days) do
      plans = Plan.enabled
      plans = plans.where(platform: ['all', platform]) if platform.present?
      plans = plans.where(app_version: ['all', app_version]) if app_version.present?
      # 查找版本号是否在审核
      # 如果正在审核，就返回指定版本号的套餐
      # 否则返回未指定版本号的套餐
      # 如果平台、版本、版本号有通用也有指定版本，则两个都显示，除非将通用改为其它指定版本
      if SystemSetting.is_auditing?(platform, app_version, app_version_number)
        plans = plans.where(is_audit: true, app_version_number: app_version_number)
      else
        plans = plans.where(is_audit: false, app_version_number: [nil, ''])
      end
      plans.to_a
    end
    result
  end

  # IAP套餐接口，只返回IAP套餐，审核时使用
  def self.by_version_iap(platform, app_version, app_version_number)
    cache_key = "#{CACHE_KEY}#{platform}_#{app_version}_#{app_version_number}_iap"
    result = Rails.cache.fetch(cache_key, expires_in: 7.days) do
      plans = Plan.enabled.iap
      plans = plans.where(platform: ['all', platform]) if platform.present?
      plans = plans.where(app_version: ['all', app_version]) if app_version.present?
      # 查找版本号是否在审核
      # 如果正在审核，就返回指定版本号的套餐
      # 否则返回未指定版本号的套餐
      # 如果平台、版本、版本号有通用也有指定版本，则两个都显示，除非将通用改为其它指定版本
      if SystemSetting.is_auditing?(platform, app_version, app_version_number)
        plans = plans.where(is_audit: true, app_version_number: app_version_number)
      else
        plans = plans.where(is_audit: false, app_version_number: [nil, ''])
      end
      plans.to_a
    end
    result
  end

  # 获取按价格分组对应的套餐名HASH
  def self.group_price_names
    list = Plan.select(:name)
      .select("FLOOR(price) AS aprice")
      .enabled
      .to_a
      .map(&:serializable_hash)
      .map{|p| p.symbolize_keys!.except!(:id)}
      .group_by{|p| p[:aprice]}
    list.each {|k, v| list[k] = v.map{|p|p[:name]}.uniq}
  end

  # 创建订单并为指定用户应用套餐用于IAP支付
  def payment(user, payment_method, transaction_id, app_channel, app_version)
    transaction_log = nil
    ActiveRecord::Base.transaction do
      # 如果是钻石类套餐
      if !self.is_regular_time?
        # 为用户累加钻石和金额
        user.total_payment_amount = user.total_payment_amount + self.price
        user.current_coins = user.current_coins + self.coins
        # 赠送的钻石不记入累计钻石中
        user.total_coins = user.total_coins + self.coins
        # 累计赠送钻石
        user.present_coins = user.present_coins + self.present_coins
        # 根据用户累计钻石数判断用户是否达到升级用户类型的要求
        user_group = UserGroup.find_by_coins(user.total_coins)
        # 如果用户当前等级小于此等级，则升级到此等级
        if user_group.present? && user.user_group.level < user_group.level
          user.set_group(user_group)
        end
        user.save! if user.changed?
      else
        # 为用户累加消费金额
        user.update!(total_payment_amount: user.total_payment_amount + self.price)
        node_type = self.node_type
        # 为用户类加过期时间
        user_node_type = UserNodeType.find_by(user_id: user.id, node_type_id: node_type.id)
        # 如果当前服务类型未到期，则基于服务类型过期时间累加，否则基于当前时间累加
        base_time = user_node_type.expired_at > Time.now ? user_node_type.expired_at : Time.now
        bought_time = DateUtils.increase_expire_time(base_time, self.time_type)
        # 累加过期时间
        user_node_type.update!(expired_at: bought_time)
      end
      # 写入交易日志
      transaction_log = user.transaction_logs.create!(
        plan_id: self.id,
        plan_name: self.name,
        price: self.price,
        coins: self.coins + self.present_coins,
        plan_coins: self.coins,
        present_coins: self.present_coins,
        payment_method: payment_method,
        platform_transaction_id: transaction_id,
        app_channel: app_channel,
        app_version: app_version,
        status: TransactionLog::STATUS_PAID,
        processed_at: Time.now,
        is_regular_time: self.is_regular_time,
        time_type: self.time_type,
        node_type_id: self.node_type_id,
        user_created_at: user.created_at
      )
    end
    transaction_log
  end

  # 为指定用户应用套餐并设置订单为完成状态，用于comsunny支付
  # !!!此方法目前已被禁用，因为第二版充值系统改版，现在订单已不与套餐表做关联 2018.03.20
  def payment_for_comsunny!(user, comsunny, transaction_log)
    ActiveRecord::Base.transaction do
      # 如果是钻石类套餐
      if !self.is_regular_time?
        # 为用户累加钻石和金额
        user.total_payment_amount = user.total_payment_amount + transaction_log.price
        user.current_coins = user.current_coins + transaction_log.coins
        # 赠送的钻石不记入累计钻石中
        user.total_coins = user.total_coins + transaction_log.plan_coins
        # 累计赠送钻石
        user.present_coins = user.present_coins + transaction_log.present_coins
        # 根据用户累计钻石数判断用户是否达到升级用户类型的要求
        user_group = UserGroup.find_by_coins(user.total_coins)
        # 如果用户当前等级小于此等级，则升级到此等级
        if user_group.present? && user.user_group.level < user_group.level
          user.set_group(user_group)
        end
        user.save! if user.changed?
        # 创建comsunny交易日志
        ComsunnyTransactionLog.create_log!(user, comsunny)
      else
        # 为用户累加消费金额
        user.update!(total_payment_amount: user.total_payment_amount + transaction_log.price)
        node_type = transaction_log.node_type
        # 为用户类加过期时间
        user_node_type = UserNodeType.find_by(user_id: user.id, node_type_id: node_type.id)
        # 如果当前服务类型未到期，则基于服务类型过期时间累加，否则基于当前时间累加
        base_time = user_node_type.expired_at > Time.now ? user_node_type.expired_at : Time.now
        bought_time = DateUtils.increase_expire_time(base_time, transaction_log.time_type)
        # 累加过期时间
        user_node_type.update!(expired_at: bought_time)
        # 找到用户最后一次登录的日志，取出渠道和版本信息
        app_channel, app_version = user.last_signin_versions
        if app_channel.blank? || app_version.blank?
          app_channel = user.create_app_channel
          app_version = user.create_app_version
        end
        # 写入消费日志
        ConsumptionLog.create!(
          user_id: user.id,
          node_type_name: node_type.name,
          node_type_id: node_type.id,
          coins: 0,
          app_channel: app_channel,
          app_version: app_version,
          is_regular_time: self.is_regular_time,
          time_type: self.time_type
        )
      end
      transaction_log.update!(
        payment_method: comsunny.payment_type,
        platform_transaction_id: comsunny.transaction_id,
        status: TransactionLog::STATUS_PAID,
        processed_at: Time.now
      )
    end
  end

  private

  def after_commit_trace
    # 清除缓存
    Rails.cache.delete_matched("#{CACHE_KEY}*")
  end

  def iap_id_uniqueness_create
    if self.is_iap?
      if Plan.where(is_audit: self.is_audit, iap_id: self.iap_id).exists?
        errors.add(:iap_id, '已被使用')
      end
    end
  end

  def iap_id_uniqueness_update
    if self.is_iap?
      if Plan.where(is_audit: self.is_audit, iap_id: self.iap_id).where.not(id: self.id).exists?
        errors.add(:iap_id, '已被使用')
      end
    end
  end

  def strip_whitespace
    self.name = self.name.strip if self.name.present?
    self.iap_id = self.iap_id.strip if self.iap_id.present?
  end

  def default_value
    self.platform = 'all' if self.platform.blank?
    self.app_version = 'all' if self.app_version.blank?
    self.coins = 0 if self.coins.blank?
    self.present_coins = 0 if self.present_coins.blank?
  end

end

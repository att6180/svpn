class NodeOnlineUser < ApplicationLogRecord
  include Uuidable

  belongs_to :user
  belongs_to :node_region
  belongs_to :node

  scope :by_node, ->(id) { where(node_id: id) }
  scope :by_region, ->(country, province, city) { where(ip_country: country, ip_province: province, ip_city: city) }
  scope :by_node_region, -> (id) { where(node_region_id: id) }
  scope :group_region, -> {
    select("ip_country, ip_province, ip_city, count(user_id) as users_count")
      .group(:ip_country, :ip_province, :ip_city)
  }

  # 地域统计
  def self.region_stat
    NodeOnlineUser.select(:ip_country, :ip_province, :ip_city)
      .select("COUNT(id) AS users_count")
      .group(:ip_country, :ip_province, :ip_city).to_a
  end
end

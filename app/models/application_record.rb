class ApplicationRecord < ActiveRecord::Base
  self.abstract_class = true

  scope :recent, -> { order(created_at: :desc) }
  scope :exclude_ids, ->(ids) { where.not(id: ids.map(&:to_i)) }
  scope :by_week, -> { where('created_at > ?', 7.days.ago.utc) }

end

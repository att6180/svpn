class UserRetentionRateStat < ApplicationLogRecord
  include Uuidable

  scope :between_months, -> (start_at, end_at) { where(stat_date: start_at..end_at) }

  scope :stat_result_user, -> {
    select(
      "stat_date, SUM(users_count) as total_users_count, SUM(second_day) as total_second_day, " \
      "SUM(third_day) as total_third_day, SUM(seventh_day) as total_seventh_day, " \
      "SUM(fifteen_day) as total_fifteen_day, SUM(thirtieth_day) as total_thirtieth_day")
      .group("stat_date") }
  scope :stat_result_transaction, -> {
    select(
      "stat_date, SUM(users_count) as t_users_count, " \
      "SUM(paid_users_count) as t_paid_users_count, SUM(total_price) as t_total_price, " \
      "SUM(second_paid_users_count) as t_second_paid_users_count, SUM(second_total_price) as t_second_total_price, " \
      "SUM(seventh_paid_users_count) as t_seventh_paid_users_count, SUM(seventh_total_price) as t_seventh_total_price, " \
      "SUM(thirtieth_paid_users_count) as t_thirtieth_paid_users_count, SUM(thirtieth_total_price) as t_thirtieth_total_price, " \
      "SUM(seventh_users_count) as t_seventh_users_count, SUM(seventh_17_total_price) as t_seventh_17_total_price, " \
      "SUM(thirtieth_users_count) as t_thirtieth_users_count, SUM(thirtieth_130_total_price) as t_thirtieth_130_total_price")
      .group("stat_date") }

  def self.retention_rate(retention_count, new_users_count)
    return nil if retention_count.blank?
    return "0.0%" if new_users_count <= 0
    "%.1f%" % ((retention_count.to_f / new_users_count.to_f) * 100)
  end

  # 从collection中提取用户数
  def self.get_users_count(collection, channel, version)
    return nil if collection.nil?
    key = (channel.present? || version.present?) ? "#{channel}#{version}" : "all"
    collection.dig(key) || 0
  end

  # 从collection中提取充值用户数及金额
  def self.get_transaction_info(collection, channel, version)
    return nil if collection.nil?
    key = (channel.present? || version.present?) ? "#{channel}#{version}" : "all"
    collection.dig(key.to_sym)
  end

  # 统计指定某天的留存数据
  # rebuild: 是否创建当天新用户数(如果是每日统计，则为否，如果是重新统计所有天数的数据，则为true)
  # date: 指定日期，将会取此日期的前一天进行统计
  # 2018/11/23 增加3留，15留。7留从以前的3-7日留存改为第7天留存
  def self.onday_stat(date = nil, rebuild = false)
    today = Time.now.to_date
    first_day = date || today.yesterday
    end_of_day = first_day.end_of_day

    UserRetentionRateStat.stat_by_date(first_day) if rebuild
    # 查找有无当天的初始统计数据
    stats = UserRetentionRateStat.where(stat_date: first_day)
    # 次留, 汇总每个渠道和版本的去重用户数, 如果是重新跑数据(rebuild: true)并且要跑的这一天是昨天，就不生成2日留存数据
    collection2 = first_day.next_day.today? ? nil : NodeConnectionUserDayLog.retention_collection(first_day, end_of_day, first_day + 1.day, nil, false)
    # 3留, 汇总每个渠道和版本的去重用户数
    collection3 = first_day + 2.days < today ? NodeConnectionUserDayLog.retention_collection(first_day, end_of_day, first_day + 2.days, first_day + 2.days) : nil
    # 7留, 汇总每个渠道和版本的去重用户数
    collection7 = first_day + 6.days < today ? NodeConnectionUserDayLog.retention_collection(first_day, end_of_day, first_day + 6.days, first_day + 6.days) : nil
    # 15留, 汇总每个渠道和版本的去重用户数
    collection15 = first_day + 14.days < today ? NodeConnectionUserDayLog.retention_collection(first_day, end_of_day, first_day + 14.days, first_day + 14.days) : nil
    # 30留, 汇总每个渠道和版本的去重用户数
    collection30 = first_day + 29.days < today ? NodeConnectionUserDayLog.retention_collection(first_day, end_of_day, first_day + 29.days, first_day + 29.days) : nil
    # 次留, 汇总每个渠道和版本的充值用户数及金额
    transaction_collection2 = first_day.next_day.today? ? nil : TransactionLog.retention_collection(first_day, first_day + 1.day, nil, false)
    # 7留, 汇总每个渠道和版本的去重用户数
    transaction_collection7 = first_day + 6.days < today ? TransactionLog.retention_collection(first_day, first_day + 2.days, first_day + 6.days) : nil
    # 30留, 汇总每个渠道和版本的去重用户数
    transaction_collection30 = first_day + 29.days < today ? TransactionLog.retention_collection(first_day, first_day + 7.days, first_day + 29.days) : nil
    # 当天到7日，汇总每个渠道和版本的去重用户数
    transaction_collection17 = first_day + 6.days < today ? TransactionLog.retention_collection(first_day, first_day, first_day + 6.days) : nil
    # 当天到30日，汇总每个渠道和版本的去重用户数
    transaction_collection130 = first_day + 29.days < today ? TransactionLog.retention_collection(first_day, first_day, first_day + 29.days) : nil
    if stats.present?
      stats.each do |stat|
        # 如果当天统计数字为0，则直接跳过后面的统计
        if stat.users_count <= 0
          second_day_count = first_day.next_day.today? ? nil : 0
          third_day_count = first_day + 2.days < today ? 0 : nil
          seventh_day_count = first_day + 6.days < today ? 0 : nil
          fifteen_day_count = first_day + 14.days < today ? 0 : nil
          thirtieth_day_count = first_day + 29.days < today ? 0 : nil
          transaction_second_users_count = first_day.next_day.today? ? nil : 0
          transaction_second_price = first_day.next_day.today? ? nil : 0
          transaction_seventh_users_count = first_day + 6.days < today ? 0 : nil
          transaction_seventh_price = first_day + 6.days < today ? 0 : nil
          transaction_thirtieth_users_count = first_day + 29.days < today ? 0 : nil
          transaction_thirtieth_price = first_day + 29.days < today ? 0 : nil
          transaction_17_users_count = first_day + 6.days < today ? 0 : nil
          transaction_17_total_price = first_day + 6.days < today ? 0 : nil
          transaction_130_users_count = first_day + 29.days < today ? 0 : nil
          transaction_130_total_price = first_day + 29.days < today ? 0 : nil
        else
          # 如果渠道和版本都为空，并且过滤类型大于0，说明是补位数据，则忽略
          next if stat.app_channel.blank? && stat.app_version.blank? && stat.filter_type > 0
          # 留存次留
          second_day_count = UserRetentionRateStat.get_users_count(collection2, stat.app_channel, stat.app_version)
          # 留存3留
          third_day_count = UserRetentionRateStat.get_users_count(collection3, stat.app_channel, stat.app_version)
          # 留存7留
          seventh_day_count = UserRetentionRateStat.get_users_count(collection7, stat.app_channel, stat.app_version)
          # 留存15留
          fifteen_day_count = UserRetentionRateStat.get_users_count(collection15, stat.app_channel, stat.app_version)
          # 留存30留
          thirtieth_day_count = UserRetentionRateStat.get_users_count(collection30, stat.app_channel, stat.app_version)
          # 充值次留
          transaction_second_day = UserRetentionRateStat.get_transaction_info(transaction_collection2, stat.app_channel, stat.app_version)
          transaction_second_users_count = transaction_second_day.present? ? transaction_second_day.dig(:users_count) : nil
          transaction_second_price = transaction_second_day.present? ? (transaction_second_day.dig(:total_price) || 0) : nil
          # 充值7留
          transaction_seventh_day = UserRetentionRateStat.get_transaction_info(transaction_collection7, stat.app_channel, stat.app_version)
          transaction_seventh_users_count = transaction_seventh_day.present? ? transaction_seventh_day.dig(:users_count) : nil
          transaction_seventh_price = transaction_seventh_day.present? ? (transaction_seventh_day.dig(:total_price) || 0) : nil
          # 充值30留
          transaction_thirtieth_day = UserRetentionRateStat.get_transaction_info(transaction_collection30, stat.app_channel, stat.app_version)
          transaction_thirtieth_users_count = transaction_thirtieth_day.present? ? transaction_thirtieth_day.dig(:users_count) : nil
          transaction_thirtieth_price = transaction_thirtieth_day.present? ? (transaction_thirtieth_day.dig(:total_price) || 0) : nil
          # 充值当日到7日
          transaction_17_day = UserRetentionRateStat.get_transaction_info(transaction_collection17, stat.app_channel, stat.app_version)
          transaction_17_users_count = transaction_17_day.present? ? transaction_17_day.dig(:users_count) : nil
          transaction_17_total_price = transaction_17_day.present? ? (transaction_17_day.dig(:total_price) || 0) : nil
          # 充值当日到30日
          transaction_130_day = UserRetentionRateStat.get_transaction_info(transaction_collection130, stat.app_channel, stat.app_version)
          transaction_130_users_count = transaction_130_day.present? ? transaction_130_day.dig(:users_count) : nil
          transaction_130_total_price = transaction_130_day.present? ? (transaction_130_day.dig(:total_price) || 0): nil
        end
        # 更新这一天的留存率统计数据
        stat.second_day = second_day_count
        stat.third_day = third_day_count
        stat.seventh_day = seventh_day_count
        stat.fifteen_day = fifteen_day_count
        stat.thirtieth_day = thirtieth_day_count
        stat.second_paid_users_count = transaction_second_users_count
        stat.second_total_price = transaction_second_price
        stat.seventh_paid_users_count = transaction_seventh_users_count
        stat.seventh_total_price = transaction_seventh_price
        stat.thirtieth_paid_users_count = transaction_thirtieth_users_count
        stat.thirtieth_total_price = transaction_thirtieth_price
        stat.seventh_users_count = transaction_17_users_count
        stat.seventh_17_total_price = transaction_17_total_price
        stat.thirtieth_users_count = transaction_130_users_count
        stat.thirtieth_130_total_price = transaction_130_total_price
        stat.save if stat.changed?
      end
    end
    nil
  end

  # 每日统计，统计前一天及前30天的次留、七留、三十留的用户留存数据
  def self.stat(date = nil)
    today = date || Time.now.to_date
    yesterday = today.yesterday
    UserRetentionRateStat.stat_by_date(yesterday)
    # 循环最近30天（不包括昨天和今天）的日期
    # 从用户表查找注册日期等于每个日期的用户
    # 同时统计这些用户的 最后连接时间 在 从注册日 起的 第2天/7天内/30内 的数量
    first_day = yesterday - 1.days
    end_day = yesterday - 32.days
    while first_day > end_day
      UserRetentionRateStat.onday_stat(first_day, false)
      first_day = first_day.prev_day
    end
  end

  # 统计指定日期的新增用户数
  def self.stat_by_date(date)
    stats = []

    # 全部
    users_count = User.where("DATE(created_at) = ?", date).count
    transaction_coll = TransactionLog.by_date(date)
      .by_paid
      .where("DATE(user_created_at) = ?", date)
      .select("COUNT(DISTINCT user_id) as users_count, SUM(price) as total_price")&.take
    stats << UserRetentionRateStat.new(
      id: Utils::Gen.generate_uuid,
      stat_date: date,
      users_count: users_count,
      paid_users_count: transaction_coll.users_count,
      total_price: transaction_coll.total_price || 0
    )

    # 渠道
    user_items = User
      .select("create_app_channel as app_channel, count(*) as users_count")
      .group(:create_app_channel)
      .where("DATE(created_at) = ?", date)
      .to_a.map(&:serializable_hash)
    transaction_items = TransactionLog.by_date(date)
      .by_paid
      .where("DATE(user_created_at) = ?", date)
      .group(:app_channel)
      .select("app_channel, COUNT(DISTINCT user_id) as paid_users_count, SUM(price) as total_price")
      .to_a.map(&:serializable_hash)
    # 将两个结果按app_channel全组并将结果合并到一起
    items = (user_items + transaction_items).group_by{|h| h["app_channel"]}.map{|k, v| [k, v.reduce{|a, b| a.merge(b)}]}

    if items.present?
      items.each do |item|
        stat = item.last
        stats << UserRetentionRateStat.new(
          id: Utils::Gen.generate_uuid,
          stat_date: date,
          app_channel: stat["app_channel"],
          users_count: stat["users_count"] || 0,
          paid_users_count: stat["paid_users_count"] || 0,
          total_price: stat["total_price"] || 0,
          filter_type: 1
        )
      end
    end
    stats << UserRetentionRateStat.new(
      id: Utils::Gen.generate_uuid,
      stat_date: date,
      users_count: 0,
      paid_users_count: 0,
      total_price: 0,
      filter_type: 1
    )

    # 版本
    user_items = User
      .select("create_app_version as app_version, count(*) as users_count")
      .group(:create_app_version)
      .where("DATE(created_at) = ?", date)
      .to_a.map(&:serializable_hash)
    transaction_items = TransactionLog.by_date(date)
      .by_paid
      .where("DATE(user_created_at) = ?", date)
      .group(:app_version)
      .select("app_version, COUNT(DISTINCT user_id) as paid_users_count, SUM(price) as total_price")
      .to_a.map(&:serializable_hash)
    # 将两个结果按app_channel全组并将结果合并到一起
    items = (user_items + transaction_items).group_by{|h| h["app_version"]}.map{|k, v| [k, v.reduce{|a, b| a.merge(b)}]}

    if items.present?
      items.each do |item|
        stat = item.last
        stats << UserRetentionRateStat.new(
          id: Utils::Gen.generate_uuid,
          stat_date: date,
          app_version: stat["app_version"],
          users_count: stat["users_count"] || 0,
          paid_users_count: stat["paid_users_count"] || 0,
          total_price: stat["total_price"] || 0,
          filter_type: 1
        )
      end
    end

    # 渠道和版本
    user_items = User
      .select("create_app_channel as app_channel, create_app_version as app_version, count(*) as users_count")
      .group(:create_app_version, :create_app_channel)
      .where("DATE(created_at) = ?", date)
      .to_a.map(&:serializable_hash)
    transaction_items = TransactionLog.by_date(date)
      .by_paid
      .where("DATE(user_created_at) = ?", date)
      .group(:app_channel, :app_version)
      .select("app_channel, app_version, COUNT(DISTINCT user_id) as paid_users_count, SUM(price) as total_price")
      .to_a.map(&:serializable_hash)
    # 将两个结果按app_channel全组并将结果合并到一起
    items = (user_items + transaction_items).group_by do |h|
      channel = h["app_channel"]
      version = h["app_version"]
      "#{channel}_#{version}"
    end.map{|k, v| [k, v.reduce{|a, b| a.merge(b)}]}

    if items.present?
      items.each do |item|
        stat = item.last
        stats << UserRetentionRateStat.new(
          id: Utils::Gen.generate_uuid,
          stat_date: date,
          app_version: stat["app_version"],
          app_channel: stat["app_channel"],
          users_count: stat["users_count"] || 0,
          paid_users_count: stat["paid_users_count"] || 0,
          total_price: stat["total_price"] || 0,
          filter_type: 2
        )
      end
    end
    stats << UserRetentionRateStat.new(
      id: Utils::Gen.generate_uuid,
      stat_date: date,
      users_count: 0,
      paid_users_count: 0,
      total_price: 0,
      filter_type: 2
    )

    UserRetentionRateStat.bulk_insert(stats, use_provided_primary_key: true) if stats.present?
  end

end

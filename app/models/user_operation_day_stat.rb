class UserOperationDayStat < ApplicationLogRecord
  include Uuidable

  scope :between_date, -> (start_at, end_at) { where(stat_date: start_at..end_at) }

  scope :stat_result, -> {
    select("stat_date, interface_id, SUM(users_count) as total_users_count, SUM(visits_count) as total_visits_count")
      .group(:stat_date, :interface_id).order("stat_date ASC") }

  # 统计前一天的操作日志
  def self.stat(date = nil)
    yesterday = date || Time.now.to_date.yesterday

    stats = []

    items = UserOperationLog
      .select("interface_id, COUNT(DISTINCT user_id) as users_count, " \
        "COUNT(*) as visits_count").
      group(:interface_id).
      where("DATE(created_at) = ?", yesterday)

    if items.present?
      items.each do |item|
        stats << UserOperationDayStat.new(
          id: Utils::Gen.generate_uuid,
          stat_date: yesterday,
          interface_id: item.interface_id,
          users_count: item.users_count,
          visits_count: item.visits_count,
          is_total: true
        )
      end
    else
      stats << UserOperationDayStat.new(
        id: Utils::Gen.generate_uuid,
        stat_date: yesterday,
        users_count: 0,
        visits_count: 0,
        is_total: true
      )
    end

    items = UserOperationLog
      .select("interface_id, app_version, app_channel, " \
        "COUNT(DISTINCT user_id) as users_count, " \
        "COUNT(*) as visits_count").
      group(:app_version, :app_channel, :interface_id).
      where("DATE(created_at) = ?", yesterday)

    if items.present?
      items.each do |item|
        stats << UserOperationDayStat.new(
          id: Utils::Gen.generate_uuid,
          stat_date: yesterday,
          interface_id: item.interface_id,
          app_version: item.app_version,
          app_channel: item.app_channel,
          users_count: item.users_count,
          visits_count: item.visits_count
        )
      end
    end
    stats << UserOperationDayStat.new(
      id: Utils::Gen.generate_uuid,
      stat_date: yesterday,
      users_count: 0,
      visits_count: 0
    )
    UserOperationDayStat.bulk_insert(stats, use_provided_primary_key: true) if stats.present?
  end
end

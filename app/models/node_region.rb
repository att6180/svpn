class NodeRegion < ApplicationRecord

  has_many :nodes
  belongs_to :node_type
  belongs_to :node_country
  belongs_to :node_continent
  has_many :out_nodes, class_name: "Node", foreign_key: :node_region_out_id

  #validates :name, :abbr, presence: true

  scope :sorted, -> { order(is_enabled: :desc, node_type_id: :asc, node_continent_id: :asc, node_country_id: :asc, sequence: :asc) }
  scope :enabled, -> { where(is_enabled: true) }

  after_commit :after_commit_trace

  # (弃用)重新计算该区域所有节点的在线连接数
  def connections_count
    Rails.cache.fetch("node_region_#{self.id}_connections_count", expires_in: 5.minutes) do
      self.nodes.sum(:connections_count)
    end
  end

  # 获得所有区域在线人数列表
  def self.connections
    Node.select("node_region_id, SUM(connections_count) as connections_count")
      .group(:node_region_id)
      .index_by(&:node_region_id)
  end

  private

  def after_commit_trace
    Rails.cache.delete(Node::CACHE_KEY_NODES_LINE_LIST)
    Rails.cache.delete_matched("#{Node::CACHE_KEY_NODES_OUT_REGION}*")
    Rails.cache.delete_matched("#{NodeCountry::CACHE_KEY_CONNECTION}*")
  end
end

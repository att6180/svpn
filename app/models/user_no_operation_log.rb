class UserNoOperationLog < ApplicationLogRecord
  include Uuidable

  belongs_to :user

  scope :between_date, -> (start_at, end_at) { where(created_at: start_at.beginning_of_day..end_at.end_of_day) }
  scope :by_date, ->(date) { where("DATE(created_at) = ?", date) }
  scope :group_addition, -> {
    select("DATE(created_at) AS stat_date, COUNT(id) AS no_operations_count")
      .group("DATE(created_at)")
  }
end

class ChannelSetting < ApplicationRecord

  CACHE_KEY = "channel_settings"
  NEW_USER_COINS = "new_user_coins"
  CHECKIN_TIME = "checkin_time"

  serialize :settings

  after_commit :after_commit_trace, on: [:create, :update]
  after_commit :after_commit_trace_destroy, on: :destroy

  def hkey
    return "#{self.app_channel}_#{self.app_version}"
  end

  # 按照渠道和版本获取指定设置项
  def self.get_item(app_channel, app_version, key)
    cache_result = RedisCache.hget_json(CACHE_KEY, "#{app_channel}_#{app_version}")
    if !cache_result.nil? && cache_result.has_key?(key.to_sym)
      cache_result[key.to_sym]
    else
      case key
      when NEW_USER_COINS
        Setting.new_user_default_coins.to_i
      when CHECKIN_TIME
        Setting.checkin_present_time.to_i
      end
    end
  end

  private

  def after_commit_trace
    RedisCache.hset_json(CACHE_KEY, "#{self.app_channel}_#{self.app_version}", self.settings)
  end

  def after_commit_trace_destroy
    RedisCache.hdel(CACHE_KEY, "#{self.app_channel}_#{self.app_version}")
  end

end

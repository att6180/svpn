class NodeConnectionServerLog < ApplicationLogRecord

  scope :between_date, ->(start_at, end_at) { where(created_at: start_at.beginning_of_day..end_at.end_of_day) }

  belongs_to :node

end

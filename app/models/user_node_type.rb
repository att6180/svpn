class UserNodeType < ApplicationRecord

  CACHE_KEY = "user_node_types"

  enum status: { times: 0, monthly: 1 }, _prefix: :status

  belongs_to :user
  belongs_to :node_type

  before_create :before_create_track

  scope :sorted_by_created_at, -> { order(created_at: :asc) }
  scope :in_node_types, ->(ids) { where(node_type_id: ids) }

  # 检查用户服务类型是否缺少已启用的服务类型，如果有缺少，则添加
  def self.check_types(user, user_node_types, node_types)
    user_node_type_ids = user_node_types.map(&:node_type_id)
    node_type_ids = node_types.map(&:id)
    ids = node_type_ids - user_node_type_ids
    ids.each do |id|
      user.user_node_types << user.user_node_types.build(node_type_id: id)
    end
  end

  def self.create_all_type_by_user(user)
    node_types = NodeType.enabled.sorted_by_level
    node_types.each do |node_type|
      user.user_node_types << user.user_node_types.build(node_type_id: node_type.id)
    end
  end

  # 检查服务器类型使用次数并转换状态
  def inspect_and_switch_state!
    node_type = self.node_type
    # 使用次数如果达到要求，并且可以转换为包月
    if self.used_count >= node_type.times_for_monthly && node_type.can_be_monthly?
      # 更新状态为包月，并将过期时间设置到月底
      # 如果当前类型的过期时间是在当月，由设置为本月的月底
      # 如果当前类型的过期时间大于当月，则设置为类型当前月份的月底
      new_expired_at = self.expired_at.month > Time.now.month ? self.expired_at.end_of_month : Time.now.end_of_month
      self.update_columns(status: :monthly, expired_at: new_expired_at)
    end
  end

  private

  def before_create_track
    free_time = '2019-01-01 00:00:00'.to_time

    if Time.now >= free_time
      self.expired_at = Time.now
    else
      self.expired_at = free_time
    end
    # self.expired_at = Time.now
  end

end

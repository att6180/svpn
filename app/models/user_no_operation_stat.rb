class UserNoOperationStat < ApplicationLogRecord
  include Uuidable

  scope :between_date, -> (start_at, end_at) { where("stat_date >= ? AND stat_date <= ?", start_at, end_at) }
  scope :group_order_desc, -> { order("stat_date DESC")}
  scope :group_order_asc, -> { order("stat_date ASC")}

  # 统计前一天的未操作日志
  def self.stat(date = nil)
    yesterday = date || Time.now.to_date.yesterday

    stats = []

    items = UserNoOperationLog
      .select("COUNT(*) as total")
      .where("DATE(created_at) = ?", yesterday)

    if items.present?
      item = items.take
      stats << UserNoOperationStat.new(
        id: Utils::Gen.generate_uuid,
        stat_date: yesterday,
        no_operations_count: item.total,
        is_total: true
      )
    end

    items = UserNoOperationLog
      .select("app_channel, app_version, COUNT(*) as total")
      .group(:app_channel, :app_version)
      .where("DATE(created_at) = ?", yesterday)

    if items.present?
      items.each do |item|
        stats << UserNoOperationStat.new(
          id: Utils::Gen.generate_uuid,
          stat_date: yesterday,
          app_channel: item.app_channel,
          app_version: item.app_version,
          no_operations_count: item.total
        )
      end
    else
      stats << UserNoOperationStat.new(
        id: Utils::Gen.generate_uuid,
        stat_date: yesterday,
        no_operations_count: 0
      )
    end
    UserNoOperationStat.bulk_insert(stats, use_provided_primary_key: true) if stats.present?
  end

end

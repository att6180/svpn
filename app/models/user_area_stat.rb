class UserAreaStat < ApplicationLogRecord
  include Uuidable

  scope :oversea, -> { where(is_domestic: false) }
  scope :domestic, -> { where(is_domestic: true) }

  # 统计用户地域数据
  def self.stat
    UserAreaStat.delete_all

    stats = []

    UserAreaStat.using_db(DB_SLAVE1) do
      # 全部
      items = User.select("create_ip_country, create_ip_province, count(id) as users_count")
        .yesterday_ago
        .group(:create_ip_country, :create_ip_province)
        .order("users_count desc")
      
      items.each do |item|
        stats << UserAreaStat.new(
          id: Utils::Gen.generate_uuid,
          country: item.create_ip_country,
          province: item.create_ip_province,
          users_count: item.users_count,
          is_domestic: item.create_ip_country.in?(User::DOMESTIC_USER_RANGE)  
        )
      end

      # 渠道
      items = User.select("create_app_channel, create_ip_country, create_ip_province, count(id) as users_count")
        .where.not(create_app_channel: nil)
        .yesterday_ago
        .group(:create_app_channel, :create_ip_country, :create_ip_province)
        .order("users_count desc")
      items.each do |item|
        stats << UserAreaStat.new(
          id: Utils::Gen.generate_uuid,
          app_channel: item.create_app_channel,
          country: item.create_ip_country,
          province: item.create_ip_province,
          users_count: item.users_count,
          is_domestic: item.create_ip_country.in?(User::DOMESTIC_USER_RANGE), 
          filter_type: 1
        )
      end

      # 版本
      items = User.select("create_app_version, create_ip_country, create_ip_province, count(id) as users_count")
        .where.not(create_app_version: nil)
        .yesterday_ago
        .group( :create_app_version, :create_ip_country, :create_ip_province)
        .order("users_count desc")
      items.each do |item|
        stats << UserAreaStat.new(
          id: Utils::Gen.generate_uuid,
          app_version: item.create_app_version,
          country: item.create_ip_country,
          province: item.create_ip_province,
          users_count: item.users_count,
          is_domestic: item.create_ip_country.in?(User::DOMESTIC_USER_RANGE), 
          filter_type: 1
        )
      end

      # 渠道和版本
      items = User.select("create_app_version, create_app_channel, create_ip_country, create_ip_province, count(id) as users_count")
        .where.not(create_app_channel: nil, create_app_version: nil)
        .yesterday_ago
        .group(:create_app_version, :create_app_channel, :create_ip_country, :create_ip_province)
        .order("users_count desc")
      items.each do |item|
        stats << UserAreaStat.new(
          id: Utils::Gen.generate_uuid,
          app_version: item.create_app_version,
          app_channel: item.create_app_channel,
          country: item.create_ip_country,
          province: item.create_ip_province,
          users_count: item.users_count,
          is_domestic: item.create_ip_country.in?(User::DOMESTIC_USER_RANGE), 
          filter_type: 2
        )
      end
    end
    
    UserAreaStat.bulk_insert(stats, use_provided_primary_key: true) if stats.present?
  end

end

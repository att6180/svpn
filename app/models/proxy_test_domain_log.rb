class ProxyTestDomainLog < ApplicationLogRecord
  belongs_to :user
  belongs_to :node
  belongs_to :proxy_test_domain

  scope :by_group, ->(node_id) {
    select("is_successed, COUNT(user_id) as users_count")
      .group(:is_successed)
      .where(node_id: node_id)
  }
end

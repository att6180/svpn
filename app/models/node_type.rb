class NodeType < ApplicationRecord

  CACHE_KEY = "node_types"
  CACHE_KEY_BASE_ID = "node_types_base_id"

  has_many :node_regions
  has_many :nodes
  has_many :user_node_types
  belongs_to :user_group

  scope :sorted_by_level, -> { where(is_enabled: true).order(level: :asc) }
  scope :enabled, -> { where(is_enabled: true) }

  validates :name, :level, :user_group_id, :expense_coins, presence: true
  validates :name, :level, uniqueness: true
  validates :expense_coins, numericality: true, length: { in: 1..4 }

  after_commit :after_commit_trace

  def is_base_type?
    self.level <= 1
  end

  def self.cache_list
    Rails.cache.fetch(CACHE_KEY, expires_in: 30.days) do
      NodeType.enabled.sorted_by_level.to_a
    end
  end

  def self.base_type_id
    Rails.cache.fetch(CACHE_KEY_BASE_ID, expires_in: 30.days) do
      NodeType.sorted_by_level.take.id
    end
  end

  private

  def after_commit_trace
    Rails.cache.delete(CACHE_KEY)
    Rails.cache.delete(CACHE_KEY_BASE_ID)
  end

end

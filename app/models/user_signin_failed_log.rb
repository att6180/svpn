class UserSigninFailedLog < ApplicationLogRecord
  include Uuidable

  belongs_to :user

  enum fail_type: { not_exist: 0, wrong_password: 1, failed_connection: 2, app_crash: 3 }

  scope :between_date, -> (start_at, end_at) { where("DATE(created_at) >= ? AND DATE(created_at) <= ?", start_at, end_at) }
  scope :by_type_and_date, ->(t, d) { where("fail_type = ? AND DATE(created_at) = ?", t, d) }
  scope :group_addition, -> {
    select(
      "DATE(created_at) AS stat_date, " \
      "SUM(CASE fail_type WHEN 0 THEN 1 ELSE 0 END) as type0, " \
      "SUM(CASE fail_type WHEN 1 THEN 1 ELSE 0 END) as type1, " \
      "SUM(CASE fail_type WHEN 2 THEN 1 ELSE 0 END) as type2, " \
      "SUM(CASE fail_type WHEN 3 THEN 1 ELSE 0 END) as type3")
      .group("DATE(created_at)")
  }
end

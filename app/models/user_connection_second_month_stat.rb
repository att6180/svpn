class UserConnectionSecondMonthStat < ApplicationLogRecord
  include Uuidable

  scope :between_date, -> (start_at, end_at) {
    where(
      "stat_year >= ? AND stat_month >= ? AND stat_year <= ? && stat_month <= ?",
      start_at.year, start_at.month, end_at.year, end_at.month
    )
  }

  # 根据用户访问日志一级月表(user_connection_first_month_stats)统计
  def self.stat
    last_month = Time.now.last_month
    stat_year = last_month.year
    stat_month = last_month.month
    logs = UserConnectionSecondMonthStat.eager_stat(stat_year, stat_month, 1, 50)
    if logs.total_pages > 0
      (1..logs.total_pages).each do |index|
        logs = UserConnectionSecondMonthStat.eager_stat(stat_year, stat_month, index, 50)
        stats = []
        logs.each do |log|
          stat = UserConnectionSecondMonthStat.new(
            id: Utils::Gen.generate_uuid,
            domain: log.domain,
            visit_users_count: log.users_count,
            visits_count: log.total_visits_count,
            stat_year: stat_year,
            stat_month: stat_month
          )
          stats << stat
        end
        UserConnectionSecondMonthStat.bulk_insert(stats, use_provided_primary_key: true)
      end
    end
  end

  def self.eager_stat(stat_year, stat_month, page, limit)
    UserConnectionFirstMonthStat
      .select(
        "domain, " \
        "COUNT(user_id) as users_count, " \
        "SUM(visits_count) as total_visits_count"
      )
      .where(stat_year: stat_year, stat_month: stat_month)
      .group(:domain)
      .page(page).limit(limit)
  end
end

class UserAccountStatusLog < ApplicationLogRecord

  include Uuidable

  enum operation_type: { disable: 0, enable: 1 }, _prefix: :type

  belongs_to :admin, class_name: Manage::Admin
  belongs_to :user

  def self.by_user_ids(user_ids, operation_type)
    UserAccountStatusLog.where(user_id: user_ids, operation_type: operation_type).order(created_at: :asc)
  end

end

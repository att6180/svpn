class ActiveUserMonthStat < ApplicationLogRecord
  include Uuidable

  scope :between_month, -> (start_at, end_at) {
    where("DATE(CONCAT(stat_year, '-', stat_month, '-01')) BETWEEN ? AND ?", start_at, end_at)
  }

  scope :stat_result, -> {
    select("stat_year, stat_month, SUM(users_count) as total_users_count")
      .group(:stat_year, :stat_month) }

  # 统计前一月的活跃用户数
  def self.stat(date = nil)

    last_month = date || Time.now.last_month
    year = last_month.year
    month = last_month.month

    stats = []

    # 全部
    items = UserActiveHourLog
      .select("COUNT(DISTINCT user_id) as users_count")
      .where("YEAR(created_date) = ? AND MONTH(created_date) = ?", year, month)

    if items.present?
      item = items.take
      stats << ActiveUserMonthStat.new(
        id: Utils::Gen.generate_uuid,
        stat_year: year,
        stat_month: month,
        users_count: item.users_count
      )
    end

    # 版本和渠道
    items = UserActiveHourLog
      .select("COUNT(DISTINCT user_id) as users_count, app_version, app_channel")
      .group(:app_version, :app_channel)
      .where("YEAR(created_date) = ? AND MONTH(created_date) = ?", year, month)

    if items.present?
      items.each do |item|
        stats << ActiveUserMonthStat.new(
          id: Utils::Gen.generate_uuid,
          stat_year: year,
          stat_month: month,
          app_version: item.app_version,
          app_channel: item.app_channel,
          users_count: item.users_count,
          filter_type: 2
        )
      end
      stats << ActiveUserMonthStat.new(
        id: Utils::Gen.generate_uuid,
        stat_year: year,
        stat_month: month,
        users_count: 0,
        filter_type: 2
      )
    else
      stats << ActiveUserMonthStat.new(
        id: Utils::Gen.generate_uuid,
        users_count: 0,
        filter_type: 2
      )
    end

    # 渠道
    items = UserActiveHourLog
      .select("COUNT(DISTINCT user_id) as users_count, app_channel")
      .group(:app_channel)
      .where("YEAR(created_date) = ? AND MONTH(created_date) = ?", year, month)

    if items.present?
      items.each do |item|
        stats << ActiveUserMonthStat.new(
          id: Utils::Gen.generate_uuid,
          stat_year: year,
          stat_month: month,
          app_channel: item.app_channel,
          users_count: item.users_count,
          filter_type: 1
        )
      end
      stats << ActiveUserMonthStat.new(
        id: Utils::Gen.generate_uuid,
        stat_year: year,
        stat_month: month,
        users_count: 0,
        filter_type: 1
      )
    else
      stats << ActiveUserMonthStat.new(
        id: Utils::Gen.generate_uuid,
        users_count: 0,
        filter_type: 1
      )
    end

    # 版本
    items = UserActiveHourLog
      .select("COUNT(DISTINCT user_id) as users_count, app_version")
      .group(:app_version)
      .where("YEAR(created_date) = ? AND MONTH(created_date) = ?", year, month)

    if items.present?
      items.each do |item|
        stats << ActiveUserMonthStat.new(
          id: Utils::Gen.generate_uuid,
          stat_year: year,
          stat_month: month,
          app_version: item.app_version,
          users_count: item.users_count,
          filter_type: 1
        )
      end
      stats << ActiveUserMonthStat.new(
        id: Utils::Gen.generate_uuid,
        stat_year: year,
        stat_month: month,
        users_count: 0,
        filter_type: 1
      )
    else
      stats << ActiveUserMonthStat.new(
        id: Utils::Gen.generate_uuid,
        users_count: 0,
        filter_type: 1
      )
    end
    ActiveUserMonthStat.bulk_insert(stats, use_provided_primary_key: true) if stats.present?

  end
end

class UserConnectionLog < ApplicationLogRecord
  belongs_to :user
  belongs_to :node

  scope :between_date, -> (start_at, end_at) { where(created_at: start_at.beginning_of_day..end_at.end_of_day) }
  scope :between_month, -> (start_at, end_at) {
    where(
      "YEAR(created_at) >= ? AND MONTH(created_at) >= ? AND YEAR(created_at) <= ? && MONTH(created_at) <= ?",
      start_at.year, start_at.month, end_at.year, end_at.month
    )
  }
  scope :between_year, -> (start_at, end_at) { where("YEAR(created_at) >= ? AND YEAR(created_at) <= ?", start_at, end_at) }
  scope :uncollected, -> { where(is_collected: false) }
  scope :sorted_by_time, -> { order(created_at: :asc) }

  # 标记指定记录为已汇总
  def self.mark_uncollected(ids)
    UserConnectionLog.where(id: ids).update_all(is_collected: true)
  end

end

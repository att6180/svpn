class HelpManual < ApplicationRecord

  CACHE_KEY_PREFIX = "help_manuals_"

  scope :by_platform, ->(platform) { where(platform: ['all', platform]) }
  scope :by_version, ->(version) { where(app_version: ['all', version]) }

  after_commit :after_commit_trace

  private

  def after_commit_trace
    Rails.cache.delete_matched("#{CACHE_KEY_PREFIX}*")
  end

end

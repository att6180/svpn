class NodeArea < ApplicationRecord

  validates :name, presence: true, uniqueness: true

  scope :sorted, -> { order(sequence: :asc) }

  after_commit :after_commit_trace

  def can_destroy?
    self.class.reflect_on_all_associations.all? do |assoc|
      assoc.macro == :has_many && self.send(assoc.name).present?
    end
  end

  private

  def after_commit_trace
    Rails.cache.delete(Node::CACHE_KEY_NODES_LINE_LIST)
    Rails.cache.delete_matched("#{NodeCountry::CACHE_KEY_CONNECTION}*")
  end
end

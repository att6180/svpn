class RescueDomain < ApplicationRecord
  enum domain_type: [:owner, :third_party]
  scope :enabled, -> { where(is_enabled: true) }
end

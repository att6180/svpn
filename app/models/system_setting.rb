class SystemSetting < ApplicationRecord

  CACHE_KEY_PREFIX = "system_setting_"

  validates :platform, inclusion: { in: %w(all ios android pc mac robot) }
  validates :key, uniqueness: { scope: [:platform, :app_channel, :app_version, :app_version_number] }
  validate :include_han

  scope :by_platform, ->(platform) { where(platform: ['all', platform]) }
  scope :by_app_channel, ->(channel) { where(app_channel: ['all', channel]) }
  scope :by_app_version, ->(version) { where(app_version: ['all', version]) }
  scope :by_app_version_number, ->(version_number) { where(app_version_number: version_number) }
  scope :by_client, -> {where(is_client: true)}

  before_create :before_commit_trace
  before_update :before_commit_trace
  after_commit :after_commit_trace

  def self.by_platform_and_app_version(platform, app_channel, app_version, app_version_number)
    # 关于按平台、版本、版本号获取键值的规则
    # 是利用各个列的值的排序转hash，然后排在后面的键值覆盖前面的键值实现的
    # 所以这三个列的值在取名时，不要取排在 all 或 '' 的前面的值
    # 排序根据平台、渠道、版本、版本号正序排列，后台的同名配置会覆盖前面的配置
    cache_key = "#{CACHE_KEY_PREFIX}#{platform}_#{app_channel}_#{app_version}_#{app_version_number}"
    result = Rails.cache.fetch(cache_key, expires_in: 30.days) do
      settings = SystemSetting.by_client.by_platform(platform)
        .by_app_channel(app_channel)
        .by_app_version(app_version)
        .where(app_version_number: ['', app_version_number])
        .order(platform: :asc, app_channel: :asc, app_version: :asc, app_version_number: :asc)
        .to_a
      # 判断更新信息
      version_info = settings.select { |s| s.key == "#{platform.upcase}_VERSION" }.last
      setting = SystemSetting.new(key: 'IS_UPDATE', value: 0)
      if version_info.present? && version_info.value.include?('|')
        version_update = version_info.value.split('|')
        version = version_update[0].gsub('.', '').to_i
        update = version_update[1].to_i
        current_version = app_version_number.gsub('.', '').to_i
        # 如果当前版本小于配置版本
        if current_version < version
          setting = SystemSetting.new(key: 'IS_UPDATE', value: update)
        end
      end
      settings << setting
    end
    result
  end

  # 判断指定版本号是否在审核
  def self.is_auditing?(platform, app_version, app_version_number)
    version = SystemSetting.by_platform(platform)
      .by_app_version(app_version)
      .where(key: 'AUDITING_VERSIONS')
      .last
    return false if version.blank?
    version_list = version.value.split('|')
    version_list.include?(app_version_number)
  end

  # 获得指定平台、版本、版本号的极光推送key和secret
  def self.jpush_auth_data(platform, app_version)
    item = SystemSetting
      .by_platform(platform)
      .by_app_version(app_version)
      .where(key: 'JPUSH_AUTH')
      .take
    if item.present?
      data = item.value.split('|')
      {key: data.first, secret: data.last}
    else
      nil
    end
  end

  # 获取所有推送信息
  def self.all_jpush_auth_data
    items = SystemSetting.where(key: 'JPUSH_AUTH')
    items.map{|item| {key: item.value.split('|').first, secret: item.value.split('|').last}}
  end

  private

  def include_han
    key = nil
    key = :platform if Utils.include_han?(self.platform)
    key = :app_channel if Utils.include_han?(self.app_channel)
    key = :app_version if Utils.include_han?(self.app_version)
    errors.add(key, "不能包含汉字") if key.present?
  end

  def before_commit_trace
    self.value = '' if self.value.blank?
    self.app_version_number = '' if self.app_version_number.blank?
  end

  def after_commit_trace
    Rails.cache.delete_matched("#{CACHE_KEY_PREFIX}*")
    Rails.cache.delete_matched("#{HelpManual::CACHE_KEY_PREFIX}*")
  end

end

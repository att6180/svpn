class WebsiteNavigation < ApplicationRecord

  CACHE_KEY = "website_navigations"

  enum nav_type: { website: 1, app: 2 }
  enum info_type: { nav: 1, ad: 2 }

  scope :by_sorted, -> { order(website_type: :desc, sort_id: :asc)}

  after_commit :after_commit_trace

  # 根据用户所属海内外查找对应导航网站
  # 并根据导航客户端标签顺序排序
  def self.by_is_domestic(is_domestic)
    sort_key = Setting.navigation_tab_sort_key.split(',').map(&:to_i)
    tab_key = sort_key.join('_')

    Rails.cache.fetch("#{CACHE_KEY}_#{is_domestic}_#{tab_key}", expires_in: 7.days) do
      websites = WebsiteNavigation
        .where(is_domestic: is_domestic)
        .by_sorted
      group_websites = websites
        .group_by(&:nav_type_before_type_cast)
      result = []
      sort_key.each do |tab|
        result << {
          tab: tab,
          websites: group_websites[tab]
        }
      end
      result
    end
  end

  private

  def after_commit_trace
    # 根据当前记录国内外属性修改对应的全局配置
    self.is_domestic ?
      (Setting.navigation_domestic_config_update_at = Time.now.to_i)
      : (Setting.navigation_oversea_config_update_at = Time.now.to_i)
    Rails.cache.delete_matched("#{CACHE_KEY}*")
  end
end

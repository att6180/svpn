class UserActiveHourLog < ApplicationLogRecord

  scope :by_created_date, ->(date) { where("created_date = ?", date)}
  scope :group_by_hour, -> { select("created_hour as h").group(:created_hour).order("h ASC") }

  enum active_type: [
    :login,
    :connect,
    :flow,
    :checkin
  ]

  def self.create_ignore_by_sql(*params)
    sql = "INSERT IGNORE INTO user_active_hour_logs(user_id, created_date, created_hour, " \
      "app_channel, app_version, active_type, created_at, updated_at) " \
      "VALUES(?, ?, ?, ?, ?, ?, ?, ?)"
    connection.execute(
      send(
        :sanitize_sql_array,
        [sql] + params
      )
    )
  end

end

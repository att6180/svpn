class Feedback < ApplicationRecord

  CACHE_KEY_NEW_MESSAGE = "user_new_messages"
  CACHE_KEY_NEW_MESSAGE2 = "user_new_messages2"
  CACHE_KEY_USER_LIMIT = "feedback_user_limit_"

  # status: 0:未处理, 1:已回复
  enum feedback_type: { connection: 0, crash: 1, suggest: 2, other: 3 }, _prefix: :type

  belongs_to :user
  has_many :feedback_logs

  validates :content, length: { in: 1..200, message: I18n.t("api.invalid_feedback_content") }, on: :create
  validates :feedback_type, inclusion: { in: Feedback.feedback_types.map { |k, v| k }, message: '的值无效' }, on: :create

  scope :unprocessed, -> { where(is_processed: false)}
  scope :processed, -> { where(is_processed: true)}
  scope :sorted, -> { order(processed_at: :desc, created_at: :desc) }
  scope :new_sorted, -> { order(updated_at: :desc) }
  scope :status_unreply, -> { where(status: 0)}

  before_update :before_update_trace
  after_commit :after_commit_trace, on: :create

  def self.type_list
    list = Feedback.feedback_types.to_a
    list.each do |t|
      t[0] = I18n.t("feedback_types.#{t.first}", default: t.first.titleize)
    end
    list.sort_by! { |t| t[1] }
  end

  def self.write_new_message_cache(user_id)
    RedisCache.hset_bool(CACHE_KEY_NEW_MESSAGE2, user_id, false)
    RedisCache.hset_bool(CACHE_KEY_NEW_MESSAGE, user_id, false)
  end

  def self.allow_today?(user_id)
    cache_key = "#{CACHE_KEY_USER_LIMIT}#{DateUtils.time2ymd(Time.now)}"
    !($redis.hget(cache_key, user_id).to_i > Setting.feedback_max_count_per_day.to_i)
  end

  private

  def before_update_trace
    # 新反馈接口, 如果has_read2更改了, true:没有未读消息，false:有未读消息
    RedisCache.hset_bool(CACHE_KEY_NEW_MESSAGE2, self.user_id, !self.has_read2?)
    RedisCache.hset_bool(CACHE_KEY_NEW_MESSAGE, self.user_id, !self.has_read?)
  end

  def after_commit_trace
    # 记录用户今天发送反馈的数量
    cache_key = "#{CACHE_KEY_USER_LIMIT}#{DateUtils.time2ymd(Time.now)}"
    $redis.hincrby(cache_key, self.user_id, 1)
    $redis.expire(cache_key, DateUtils.remaining_time) if $redis.ttl(cache_key) == -1
  end

end

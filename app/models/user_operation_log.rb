class UserOperationLog < ApplicationLogRecord
  include Uuidable

  belongs_to :user
  belongs_to :device
  belongs_to :user_app_launch_log
  belongs_to :user_signin_log

  scope :by_date, ->(date) { where("DATE(created_at) = ?", date) }
  scope :between_date, ->(start_at, end_at) { where(created_at: start_at.beginning_of_day..end_at.end_of_day) }
  scope :by_time_asc, -> { order(created_at: :asc) }
  scope :by_created_date, ->(date) { where("DATE(created_at) = ?", date)}
  scope :stat_result_for_interface, -> {
    select("interface_id, COUNT(user_id) AS visits_count, COUNT(DISTINCT user_id) AS users_count")
      .group(:interface_id)
      .order(interface_id: :asc)
  }
  scope :group_by_interface, -> {
    select("interface_id, COUNT(user_id) AS visits_count")
      .group(:interface_id)
  }
end

class DomainEnum < ApplicationRecord

  CACHE_DOMAIN_ENUM = 'domain_enums'

  after_commit :handle_cache_on_create_and_update, on: [:create, :update]
  after_commit :handle_cache_on_destroy, on: :destroy

  validates :domain, uniqueness: true

  scope :sorted_by_domain, -> { order(domain: :asc) }

  def self.desc_by_domain(domain)
    result = $redis.hget(CACHE_DOMAIN_ENUM, domain)
    # 如果域名为facebook.com，但没有结果，则重新缓存所有域名描述
    # 因为设置了全局缓存过期时间为90天，为了防止长期未清理的永久缓存导致内存增长
    if %w(facebook.com twitter.com).include?(domain) && result.blank?
      DomainEnum.all.each do |de|
        $redis.hset(CACHE_DOMAIN_ENUM, de.domain, de.description)
      end
      result = $redis.hget(CACHE_DOMAIN_ENUM, domain)
    end
    result
  end

  private

  # 创建和更新缓存
  def handle_cache_on_create_and_update
    $redis.hset(CACHE_DOMAIN_ENUM, self.domain, self.description)
  end

  # 删除缓存
  def handle_cache_on_destroy
    $redis.hdel(CACHE_DOMAIN_ENUM, self.domain)
  end

end

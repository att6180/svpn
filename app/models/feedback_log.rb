class FeedbackLog < ApplicationRecord
  belongs_to :memberable, polymorphic: true

  validates :content, length: { in: 1..200, message: I18n.t("api.invalid_feedback_content") }, on: :create

  scope :sorted, -> { order(created_at: :desc) }
end

class ConnectionLogFilter < ApplicationRecord

  CACHE_KEY = "connection_log_filters"

  after_commit :after_commit_trace

  def self.cache_list
    Rails.cache.fetch(CACHE_KEY, expires_in: 7.days) do
      ConnectionLogFilter.all.map(&:rule)
    end
  end

  private

  def after_commit_trace
    Rails.cache.delete(CACHE_KEY)
  end

end

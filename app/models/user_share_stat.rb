# Deprecated ->> Log::UserShareStatLog
# 2018/12/17 以后通过分享平台统计
class UserShareStat < ApplicationLogRecord
  include Uuidable

  enum share_type: UserShareLog::SHARE_TYPES

  scope :stat_result, -> {
    select("share_type, SUM(users_count) AS total_users, " \
    "SUM(times_count) AS total_times")
    .group("share_type").order("share_type ASC")
  }

  def self.stat
    UserShareStat.delete_all

    # 全部
    items = UserShareLog.select("share_type, COUNT(DISTINCT user_id) as users_count, count(*) as times_count")
      .group(:share_type)
    items.each do |item|
      UserShareStat.create(
        share_type: UserShareLog.share_types[item.share_type],
        users_count: item.users_count,
        times_count: item.times_count
      )
    end

    # 渠道
    items = UserShareLog.select("app_channel, share_type, " \
      "COUNT(DISTINCT user_id) as users_count, count(*) as times_count")
      .group(:app_channel, :share_type)
    items.each do |item|
      UserShareStat.create(
        app_channel: item.app_channel,
        share_type: UserShareLog.share_types[item.share_type],
        users_count: item.users_count,
        times_count: item.times_count,
        filter_type: 1
      )
    end

    # 版本
    items = UserShareLog.select("app_version, share_type, " \
      "COUNT(DISTINCT user_id) as users_count, count(*) as times_count")
      .group(:app_version, :share_type)
    items.each do |item|
      UserShareStat.create(
        app_version: item.app_version,
        share_type: UserShareLog.share_types[item.share_type],
        users_count: item.users_count,
        times_count: item.times_count,
        filter_type: 1
      )
    end

    # 渠道和版本
    items = UserShareLog.select("app_version, app_channel, share_type, " \
      "COUNT(DISTINCT user_id) as users_count, count(*) as times_count")
      .group(:app_version, :app_channel, :share_type)
    items.each do |item|
      UserShareStat.create(
        app_version: item.app_version,
        app_channel: item.app_channel,
        share_type: UserShareLog.share_types[item.share_type],
        users_count: item.users_count,
        times_count: item.times_count,
        filter_type: 2
      )
    end

  end
end

class TransactionMonthStat < ApplicationLogRecord
  include Uuidable

  scope :by_year_and_month, -> (year, month) { where("year = ? AND month = ?", year, month) }
  scope :by_year, -> (year) { where("year = ?", year) }
  scope :channel_version_total, -> { where(filter_type: 0)}
  scope :stat_result, -> {
    select("year, month, SUM(total_recharge_amount) as s_recharge_amount, SUM(consume_coins) AS s_consume_coins, SUM(recharge_users_count) AS s_users_count")
      .group(:year, :month)
  }

  def self.filter_stat(year, month, filter_type)
    stats = []
    items = []
    filter_types = SystemEnum.enums_by_type(filter_type).map(&:name)
    TransactionMonthStat.using_db(DB_LOG_SLAVE1) do
      filter_types.each do |ft|
        total_recharge_amount = BigDecimal.new("0")
        total_consume_coins_count = 0
        recharge_users_count = 0
        renew_users_count = 0
        renew_new_users_count = 0
        renew_amount = BigDecimal.new("0")
        renew_new_amount = BigDecimal.new("0")
        # 本月之前充值过的每个渠道用户id列表(去重)
        before_users = TransactionLog.where("created_at < ?", Date.new(year, month))
          .where("app_#{filter_type} = ?", ft)
          .by_paid
          .without_iap
          .pluck("DISTINCT user_id")
        # 本月充值过的每个渠道用户列表(去重)
        now_users = TransactionLog.by_year_and_month(year, month)
          .where("app_#{filter_type} = ?", ft)
          .by_paid
          .without_iap
          .pluck("DISTINCT user_id")
        # 充值总额
        total_recharge_amount = TransactionLog.by_year_and_month(year, month)
          .where("app_#{filter_type} = ?", ft)
          .by_paid
          .without_iap
          .sum(:price)
        # 消费钻石数
        total_consume_coins_count = ConsumptionLog.by_year_and_month(year, month)
          .where("app_#{filter_type} = ?", ft)
          .sum(:coins)
        # 充值人数
        recharge_users_count = now_users.length
        # 当月续费人数, 取交集
        renew_users = before_users & now_users
        renew_users_count = renew_users.length
        # 当月新增付费人数
        renew_new_users_count = recharge_users_count - renew_users_count
        # 当月续费用户充值
        if renew_users_count > 0
          renew_amount = TransactionLog.by_year_and_month(year, month)
            .by_paid
            .without_iap
            .where(user_id: renew_users)
            .where("app_#{filter_type} = ?", ft)
            .sum(:price)
        end
        # 当月新增付费用户充值
        if renew_new_users_count > 0
          renew_new_amount = total_recharge_amount - renew_amount
        end
        items << {
          filter_type_name: ft,
          total_recharge_amount: total_recharge_amount,
          consume_coins: total_consume_coins_count,
          recharge_users_count: recharge_users_count,
          renew_users_count: renew_users_count,
          renew_new_users_count: renew_new_users_count,
          renew_amount: renew_amount,
          renew_new_amount: renew_new_amount
        }
      end
    end
    if items.present?
      items.each do |item|
        stats << TransactionMonthStat.new(
          id: Utils::Gen.generate_uuid,
          year: year,
          month: month,
          "app_#{filter_type}": item[:filter_type_name],
          total_recharge_amount: item[:total_recharge_amount],
          consume_coins: item[:consume_coins],
          recharge_users_count: item[:recharge_users_count],
          renew_users_count: item[:renew_users_count],
          renew_new_users_count: item[:renew_new_users_count],
          renew_amount: item[:renew_amount],
          renew_new_amount: item[:renew_new_amount],
          filter_type: 1
        )
      end
    end
    stats
  end

  # 全部
  def self.all_stat(year, month)
    stats = []
    items = []
    total_recharge_amount = BigDecimal.new("0")
    total_consume_coins_count = 0
    recharge_users_count = 0
    renew_users_count = 0
    renew_new_users_count = 0
    renew_amount = BigDecimal.new("0")
    renew_new_amount = BigDecimal.new("0")
    TransactionMonthStat.using_db(DB_LOG_SLAVE1) do
      # 本月之前充值过的用户id列表(去重)
      before_users = TransactionLog.where("created_at < ?", Date.new(year, month))
        .by_paid
        .without_iap
        .pluck("DISTINCT user_id")
      # 本月充值过的用户列表(去重)
      now_users = TransactionLog.by_year_and_month(year, month)
        .by_paid
        .without_iap
        .pluck("DISTINCT user_id")
      # 充值总额
      total_recharge_amount = TransactionLog.by_year_and_month(year, month)
        .by_paid
        .without_iap
        .sum(:price)
      # 消费钻石数
      total_consume_coins_count = ConsumptionLog.by_year_and_month(year, month)
        .sum(:coins)
      # 充值人数
      recharge_users_count = now_users.length
      # 当月续费人数, 取交集
      renew_users = before_users & now_users
      renew_users_count = renew_users.length
      # 当月新增付费人数
      renew_new_users_count = recharge_users_count - renew_users_count
      # 当月续费用户充值
      if renew_users_count > 0
        renew_amount = TransactionLog.by_year_and_month(year, month)
          .by_paid
          .without_iap
          .where(user_id: renew_users)
          .sum(:price)
      end
      # 当月新增付费用户充值
      if renew_new_users_count > 0
        renew_new_amount = total_recharge_amount - renew_amount
      end
    end
    stats << TransactionMonthStat.new(
      id: Utils::Gen.generate_uuid,
      year: year,
      month: month,
      total_recharge_amount: total_recharge_amount,
      consume_coins: total_consume_coins_count,
      recharge_users_count: recharge_users_count,
      renew_users_count: renew_users_count,
      renew_new_users_count: renew_new_users_count,
      renew_amount: renew_amount,
      renew_new_amount: renew_new_amount
    )
    stats
  end

  # 渠道和版本
  def self.filter_all_stat(year, month)
    stats = []
    items = []
    filter_types = SystemEnumRelation.cache_list
    TransactionMonthStat.using_db(DB_LOG_SLAVE1) do
      filter_types.each do |ft|
        total_recharge_amount = BigDecimal.new("0")
        total_consume_coins_count = 0
        recharge_users_count = 0
        renew_users_count = 0
        renew_new_users_count = 0
        renew_amount = BigDecimal.new("0")
        renew_new_amount = BigDecimal.new("0")
        # 本月之前充值过的每个渠道用户id列表(去重)
        before_users = TransactionLog.where("created_at < ?", Date.new(year, month))
          .where(app_channel: ft.channel, app_version: ft.version)
          .by_paid
          .without_iap
          .pluck("DISTINCT user_id")
        # 本月充值过的每个渠道用户列表(去重)
        now_users = TransactionLog.by_year_and_month(year, month)
          .where(app_channel: ft.channel, app_version: ft.version)
          .by_paid
          .without_iap
          .pluck("DISTINCT user_id")
        # 充值总额
        total_recharge_amount = TransactionLog.by_year_and_month(year, month)
          .where(app_channel: ft.channel, app_version: ft.version)
          .by_paid
          .without_iap
          .sum(:price)
        # 消费钻石数
        total_consume_coins_count = ConsumptionLog.by_year_and_month(year, month)
          .where(app_channel: ft.channel, app_version: ft.version)
          .sum(:coins)
        # 充值人数
        recharge_users_count = now_users.length
        # 当日续费人数, 取交集
        renew_users = before_users & now_users
        renew_users_count = renew_users.length
        # 当日新增付费人数
        renew_new_users_count = recharge_users_count - renew_users_count
        # 当月续费用户充值
        if renew_users_count > 0
          renew_amount = TransactionLog.by_year_and_month(year, month)
            .where(user_id: renew_users)
            .where(app_channel: ft.channel, app_version: ft.version)
            .by_paid
            .without_iap
            .sum(:price)
        end
        # 当月新增付费用户充值
        if renew_new_users_count > 0
          renew_new_amount = total_recharge_amount - renew_amount
        end
        items << {
          channel: ft.channel,
          version: ft.version,
          total_recharge_amount: total_recharge_amount,
          consume_coins: total_consume_coins_count,
          recharge_users_count: recharge_users_count,
          renew_users_count: renew_users_count,
          renew_new_users_count: renew_new_users_count,
          renew_amount: renew_amount,
          renew_new_amount: renew_new_amount
        }
      end
    end
    if items.present?
      items.each do |item|
        stats << TransactionMonthStat.new(
          id: Utils::Gen.generate_uuid,
          year: year,
          month: month,
          app_channel: item[:channel],
          app_version: item[:version],
          total_recharge_amount: item[:total_recharge_amount],
          consume_coins: item[:consume_coins],
          recharge_users_count: item[:recharge_users_count],
          renew_users_count: item[:renew_users_count],
          renew_new_users_count: item[:renew_new_users_count],
          renew_amount: item[:renew_amount],
          renew_new_amount: item[:renew_new_amount],
          filter_type: 2
        )
      end
    end
    stats
  end

  # 统计上个月的如下数据:
  #	本月续费人数：统计在本月之前（不包含本月）有过充值记录，并且本月有过充值行为的用户；需要去重
  #	本月新增付费人数：统计在本月之前（不包含本月）没有充值记录，但本月有过充值行为的用户；需要去重
  #	本月续费用户充值：统计在本月之前（不包含本月）有过充值记录，并且本月有过充值行为的用户所充值的总金额；不做去重
  #	本月新增付费用户充值：统计在本月之前（不包含本月）没有充值记录，但本月有过充值行为的用户所充值的总金额；不做去重
  def self.stat(date = nil)
    last_month = date || Time.now.last_month
    year = last_month.year
    month = last_month.month
    stats = []

    # 全部
    stats += TransactionMonthStat.all_stat(year, month)
    # 渠道
    stats += TransactionMonthStat.filter_stat(year, month, "channel")
    # 版本
    stats += TransactionMonthStat.filter_stat(year, month, "version")
    # 渠道和版本
    stats += TransactionMonthStat.filter_all_stat(year, month)

    TransactionMonthStat.bulk_insert(stats, use_provided_primary_key: true)
  end
end

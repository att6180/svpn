class ConnectionFailedStat < ApplicationLogRecord

  include Uuidable

  scope :stat_result, -> {
    select(
      "stat_date, SUM(connection_failed_count) as connection_failed_total, " \
      "SUM(crash_count) as crash_total, SUM(unconnected_count) as unconnected_total"
    ).group("stat_date") }

  # 统计前一天的连接失败日志
  def self.stat(date = nil)
    yesterday = date || Time.now.to_date.yesterday

    stats = []

    # 全部统计
    items = ConnectionFailedLog
      .select(
        "SUM(CASE fail_type WHEN 1 THEN 1 ELSE 0 END) as type1, " \
        "SUM(CASE fail_type WHEN 2 THEN 1 ELSE 0 END) as type2, " \
        "SUM(CASE fail_type WHEN 3 THEN 1 ELSE 0 END) as type3")
      .where("DATE(created_at) = ?", yesterday)

    if items.present?
      item = items.take
      stats << ConnectionFailedStat.new(
        id: Utils::Gen.generate_uuid,
        stat_date: yesterday,
        connection_failed_count: item.type1 || 0,
        crash_count: item.type2 || 0,
        unconnected_count: item.type3 || 0,
        is_total: true
      )
    end

    items = ConnectionFailedLog
      .select(
        "app_version, app_channel, " \
        "SUM(CASE fail_type WHEN 1 THEN 1 ELSE 0 END) as type1, " \
        "SUM(CASE fail_type WHEN 2 THEN 1 ELSE 0 END) as type2, " \
        "SUM(CASE fail_type WHEN 3 THEN 1 ELSE 0 END) as type3")
      .group(:app_version, :app_channel)
      .where("DATE(created_at) = ?", yesterday)

    if items.present?
      items.each do |item|
        stats << ConnectionFailedStat.new(
          id: Utils::Gen.generate_uuid,
          stat_date: yesterday,
          app_version: item.app_version,
          app_channel: item.app_channel,
          connection_failed_count: item.type1 || 0,
          crash_count: item.type2 || 0,
          unconnected_count: item.type3 || 0
        )
      end
      stats << ConnectionFailedStat.new(
        id: Utils::Gen.generate_uuid,
        stat_date: yesterday,
        connection_failed_count: 0,
        crash_count: 0,
        unconnected_count: 0
      )
    else
      stats << ConnectionFailedStat.new(
        id: Utils::Gen.generate_uuid,
        stat_date: yesterday,
        connection_failed_count: 0,
        crash_count: 0,
        unconnected_count: 0
      )
    end

    ConnectionFailedStat.bulk_insert(stats, use_provided_primary_key: true) if stats.present?

  end

end

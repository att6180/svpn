class UserSigninLog < ApplicationLogRecord
  include Uuidable

  CACHE_KEY_VERSIONS = "user_last_signin_versions"

  belongs_to :user
  belongs_to :device
  has_many :user_operation_logs

  scope :between_date, -> (start_at, end_at) { where(created_at: start_at.beginning_of_day..end_at.end_of_day) }
  scope :by_created_date, ->(date) { where(created_at: date.beginning_of_day..date.end_of_day) }
  scope :by_created_month, ->(year, month) { where("YEAR(created_at) = ? AND MONTH(created_at) = ?", year, month)}
  scope :by_country, -> {
    select("ip_country, COUNT(DISTINCT user_id) AS users_count, COUNT(user_id) AS times_count")
      .group(:ip_country) }
  scope :by_province, -> {
    select("ip_province, COUNT(DISTINCT user_id) AS users_count, COUNT(user_id) AS times_count")
      .group(:ip_province) }
  scope :by_city, -> {
    select("ip_city, COUNT(DISTINCT user_id) AS users_count, COUNT(user_id) AS times_count")
      .group(:ip_city) }
  scope :by_country_users_count, -> { select("COUNT(DISTINCT user_id) AS users_count") }
  scope :by_province_users_count, -> { select("COUNT(DISTINCT user_id) AS users_count") }
  scope :by_city_users_count, -> { select("COUNT(DISTINCT user_id) AS users_count") }
  scope :by_period, -> {
    select("HOUR(created_at) AS period, COUNT(DISTINCT user_id) AS users_count, COUNT(user_id) as times_count")
      .group("period")
      .order("period ASC") }
  scope :group_by_hour, -> { select("HOUR(created_at) as h").group('h').order("h ASC") }

  after_commit :after_commit_trace, on: [:create]

  private

  def after_commit_trace
    # 记录创建后，更新用户最后登录版本信息缓存
    RedisCache.hset_list(
      CACHE_KEY_VERSIONS,
      self.user_id,
      [self.app_channel, self.app_version, self.app_version_number, self.platform]
    )
  end

end

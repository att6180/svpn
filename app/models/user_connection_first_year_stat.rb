class UserConnectionFirstYearStat < ApplicationLogRecord
  include Uuidable

  belongs_to :user

  scope :between_date, -> (start_at, end_at) { where("stat_year >= ? AND stat_year <= ?", start_at.year, end_at.year) }
  scope :select_and_group, -> {
    select("domain, COUNT(DISTINCT user_id) as users_count, SUM(visits_count) as total_visits_count").group(:domain)
  }

  # 统计前一年的去向IP
  def self.stat(year = nil)
    year = year || Time.now.last_year.year

    stats = []

    items = UserConnectionLog
      .select("user_id, top_level_domain, COUNT(*) as visits_count")
      .group(:user_id, :top_level_domain)
      .where("YEAR(created_at) = ?", year)

    if items.present?
      items.each do |item|
        stats << UserConnectionFirstYearStat.new(
          id: Utils::Gen.generate_uuid,
          user_id: item.user_id,
          domain: item.top_level_domain,
          visits_count: item.visits_count,
          stat_year: year
        )
      end
    end
    UserConnectionFirstYearStat.bulk_insert(stats, use_provided_primary_key: true) if stats.present?
  end
end

class Notice < ApplicationRecord

  belongs_to :admin

  scope :enabled_one, -> { recent.where(is_enabled: true) }

  def self.disable_except(id)
    notices = Notice.where(is_enabled: true).where.not(id: id)
    notices.update_all(is_enabled: false) if notices.present?
  end

end

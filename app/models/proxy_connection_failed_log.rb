class ProxyConnectionFailedLog < ApplicationLogRecord

  scope :last_by_time, ->(time) { where("created_at > ?", Time.now - time.seconds)}

  belongs_to :node
  belongs_to :user
end

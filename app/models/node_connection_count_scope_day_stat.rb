class NodeConnectionCountScopeDayStat < ApplicationLogRecord
  include Uuidable

  scope :between_date, -> (start_at, end_at) { where(stat_date: start_at..end_at) }

  scope :stat_result, -> {
    select(
      "SUM(count_1_2) as total_count_1_2, " \
      "SUM(count_3_5) as total_count_3_5, " \
      "SUM(count_6_9) as total_count_6_9, " \
      "SUM(count_10_19) as total_count_10_19, " \
      "SUM(count_20_49) as total_count_20_49, " \
      "SUM(count_50) as total_count_50, " \
      "COUNT(users_id) as total_users_count")
      .group(:user_id)
    }

  # 统计昨天各个次数段有多少用户连接
  def self.stat(date = nil)
    yesterday = date || Time.now.to_date.yesterday

    stats = []

    items = NodeConnectionCountDayStat
      .select(
        "COUNT(*) as users_count, " \
        "SUM(CASE users_count WHEN users_count>=1 AND users_count<=2 THEN 1 ELSE 0 END) AS count_1_2, " \
        "SUM(CASE users_count WHEN users_count>=3 AND users_count<=5 THEN 1 ELSE 0 END) AS count_3_5, " \
        "SUM(CASE users_count WHEN users_count>=6 AND users_count<=9 THEN 1 ELSE 0 END) AS count_6_9, " \
        "SUM(CASE users_count WHEN users_count>=10 AND users_count<=19 THEN 1 ELSE 0 END) AS count_10_19, " \
        "SUM(CASE users_count WHEN users_count>=20 AND users_count<=49 THEN 1 ELSE 0 END) AS count_20_49, " \
        "SUM(CASE users_count WHEN users_count>=50 THEN 1 ELSE 0 END) AS count_50")
      .where("stat_date = ?", yesterday)
      .where.not(user_id: nil)

    if items.present?
      item = items.take
      stats << NodeConnectionCountScopeDayStat.new(
        id: Utils::Gen.generate_uuid,
        stat_date: yesterday,
        users_count: item.users_count,
        count_1_2: item.count_1_2 || 0,
        count_3_5: item.count_3_5 || 0,
        count_6_9: item.count_6_9 || 0,
        count_10_19: item.count_10_19 || 0,
        count_20_49: item.count_20_49 || 0,
        count_50: item.count_50 || 0,
        is_total: true
      )
    end

    items = NodeConnectionCountDayStat
      .select(
        "COUNT(*) as users_count, " \
        "app_channel, app_version, "\
        "SUM(CASE users_count WHEN users_count>=1 AND users_count<=2 THEN 1 ELSE 0 END) AS count_1_2, " \
        "SUM(CASE users_count WHEN users_count>=3 AND users_count<=5 THEN 1 ELSE 0 END) AS count_3_5, " \
        "SUM(CASE users_count WHEN users_count>=6 AND users_count<=9 THEN 1 ELSE 0 END) AS count_6_9, " \
        "SUM(CASE users_count WHEN users_count>=10 AND users_count<=19 THEN 1 ELSE 0 END) AS count_10_19, " \
        "SUM(CASE users_count WHEN users_count>=20 AND users_count<=49 THEN 1 ELSE 0 END) AS count_20_49, " \
        "SUM(CASE users_count WHEN users_count>=50 THEN 1 ELSE 0 END) AS count_50")
      .where("stat_date = ?", yesterday)
      .where.not(user_id: nil)
      .group(:app_version, :app_channel)

    if items.present?
      items.each do |item|
        stats << NodeConnectionCountScopeDayStat.new(
          id: Utils::Gen.generate_uuid,
          stat_date: yesterday,
          users_count: item.users_count,
          count_1_2: item.count_1_2 || 0,
          count_3_5: item.count_3_5 || 0,
          count_6_9: item.count_6_9 || 0,
          count_10_19: item.count_10_19 || 0,
          count_20_49: item.count_20_49 || 0,
          count_50: item.count_50 || 0,
          app_version: item.app_version,
          app_channel: item.app_channel
        )
      end
      stats << NodeConnectionCountScopeDayStat.new(
        id: Utils::Gen.generate_uuid,
        stat_date: yesterday,
        users_count: 0,
        count_1_2: 0,
        count_3_5: 0,
        count_6_9: 0,
        count_10_19: 0,
        count_20_49: 0,
        count_50: 0
      )
    else
      stats << NodeConnectionCountScopeDayStat.new(
        id: Utils::Gen.generate_uuid,
        stat_date: yesterday,
        users_count: 0,
        count_1_2: 0,
        count_3_5: 0,
        count_6_9: 0,
        count_10_19: 0,
        count_20_49: 0,
        count_50: 0,
        app_version: nil,
        app_channel: nil
      )
    end
    NodeConnectionCountScopeDayStat.bulk_insert(stats, use_provided_primary_key: true)
  end
end

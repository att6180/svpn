class UserGroup < ApplicationRecord

  CACHE_KEY = "user_groups"
  CACHE_KEY_BASE_ID = "user_group_base_id"

  has_many :users
  has_many :node_types

  # 找到一个达到指定虚拟币要求的最高等级的记录
  scope :find_by_coins, ->(coins) { where(is_enabled: true).where("need_coins <= ?", coins).order(level: :desc).first }
  scope :sorted_by_level, -> { order(level: :asc) }
  scope :enabled, -> { where(is_enabled: true) }

  validates :name, :level, uniqueness: true

  after_commit :after_commit_trace

  # 获得下一级
  def next_group
    UserGroup.enabled.where("level > ?", self.level).sorted_by_level.take
  end

  def self.cache_list
    Rails.cache.fetch(CACHE_KEY, expires_in: 30.days) do
      UserGroup.enabled.sorted_by_level.to_a
    end
  end

  # 基础用户组id
  def self.base_id
    Rails.cache.fetch(CACHE_KEY_BASE_ID) do
      UserGroup.sorted_by_level.take
    end
  end

  private

  def after_commit_trace
    Rails.cache.delete(CACHE_KEY_BASE_ID)
    Rails.cache.delete(CACHE_KEY)
  end

end

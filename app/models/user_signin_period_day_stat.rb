class UserSigninPeriodDayStat < ApplicationLogRecord

  scope :between_date, -> (start_at, end_at) { where(stat_date: start_at..end_at) }

  def self.stat(date = nil)
    yesterday = date || Time.now.to_date.yesterday
    logs = UserSigninLog.by_created_date(yesterday).by_period.index_by(&:period)
    (0..23).each do |h|
      if !logs.has_key?(h)
        logs[h] = 0
      end
    end
    period_logs = []
    logs.each do |key, value|
      if value == 0
        period_logs << {
          period: key.to_i,
          users_count: 0,
          times_count: 0
        }
      else
        period_logs << {
          period: value.period,
          users_count: value.users_count,
          times_count: value.times_count
        }
      end
    end
    UserSigninPeriodDayStat.create(stat_date: yesterday, period_data: period_logs.sort_by{|l| l[:period]}.to_json)
  end

end

class NewUserMonthStat < ApplicationLogRecord
  include Uuidable

  scope :between_month, -> (start_at, end_at) {
    where("DATE(CONCAT(stat_year, '-', stat_month, '-01')) BETWEEN ? AND ?", start_at, end_at)
  }

  scope :stat_result, -> {
    select("stat_year, stat_month, SUM(users_count) as total_users_count")
      .group(:stat_year, :stat_month) }

  # 统计前一月的新增用户数
  def self.stat(date = nil)

    last_month = date || Time.now.last_month
    year = last_month.year
    month = last_month.month

    stats = []

    # 全部
    items = User
      .select("COUNT(*) as users_count")
      .where("YEAR(created_at) = ? AND MONTH(created_at) = ?", year, month)

    if items.present?
      item = items.take
      stats << NewUserMonthStat.new(
        id: Utils::Gen.generate_uuid,
        stat_year: year,
        stat_month: month,
        users_count: item.users_count
      )
    end

    # 渠道
    items = User
      .select(
        "create_app_channel, COUNT(*) as users_count")
      .group(:create_app_channel)
      .where("YEAR(created_at) = ? AND MONTH(created_at) = ?", year, month)

    if items.present?
      items.each do |item|
        stats << NewUserMonthStat.new(
          id: Utils::Gen.generate_uuid,
          stat_year: year,
          stat_month: month,
          app_channel: item.create_app_channel,
          users_count: item.users_count,
          filter_type: 1
        )
      end
      stats << NewUserMonthStat.new(
        id: Utils::Gen.generate_uuid,
        stat_year: year,
        stat_month: month,
        users_count: 0,
        filter_type: 1
      )
    else
      stats << NewUserMonthStat.new(
        stat_year: year,
        stat_month: month,
        users_count: 0,
        filter_type: 1
      )
    end

    # 版本
    items = User
      .select(
        "create_app_version, COUNT(*) as users_count")
      .group(:create_app_version)
      .where("YEAR(created_at) = ? AND MONTH(created_at) = ?", year, month)

    if items.present?
      items.each do |item|
        stats << NewUserMonthStat.new(
          id: Utils::Gen.generate_uuid,
          stat_year: year,
          stat_month: month,
          app_version: item.create_app_version,
          users_count: item.users_count,
          filter_type: 1
        )
      end
      stats << NewUserMonthStat.new(
        id: Utils::Gen.generate_uuid,
        stat_year: year,
        stat_month: month,
        users_count: 0,
        filter_type: 1
      )
    else
      stats << NewUserMonthStat.new(
        stat_year: year,
        stat_month: month,
        users_count: 0,
        filter_type: 1
      )
    end

    # 渠道和版本
    items = User
      .select(
        "create_app_version, create_app_channel, " \
        "COUNT(*) as users_count")
      .group(:create_app_version, :create_app_channel)
      .where("YEAR(created_at) = ? AND MONTH(created_at) = ?", year, month)

    if items.present?
      items.each do |item|
        stats << NewUserMonthStat.new(
          id: Utils::Gen.generate_uuid,
          stat_year: year,
          stat_month: month,
          app_version: item.create_app_version,
          app_channel: item.create_app_channel,
          users_count: item.users_count,
          filter_type: 2
        )
      end
      stats << NewUserMonthStat.new(
        id: Utils::Gen.generate_uuid,
        stat_year: year,
        stat_month: month,
        users_count: 0,
        filter_type: 2
      )
    else
      stats << NewUserMonthStat.new(
        stat_year: year,
        stat_month: month,
        users_count: 0,
        filter_type: 2
      )
    end

    NewUserMonthStat.bulk_insert(stats, use_provided_primary_key: true)

  end
end

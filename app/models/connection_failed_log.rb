class ConnectionFailedLog < ApplicationLogRecord

  include Uuidable

  belongs_to :user

  scope :between_date, -> (start_at, end_at) { where(created_at: start_at.beginning_of_day..end_at.end_of_day) }
  scope :by_type_and_date, ->(t, d) { where("fail_type = ? AND DATE(created_at) = ?", t, d) }
  scope :group_addition, -> {
    select(
      "DATE(created_at) AS stat_date, " \
      "SUM(CASE fail_type WHEN 1 THEN 1 ELSE 0 END) as type1, " \
      "SUM(CASE fail_type WHEN 2 THEN 1 ELSE 0 END) as type2, " \
      "SUM(CASE fail_type WHEN 3 THEN 1 ELSE 0 END) as type3")
      .group("DATE(created_at)")
  }

end

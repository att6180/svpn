class Api::Client::V3::CommonsController < Api::Client::V1::ApiController
  skip_before_action :authenticate_user!, only: [:routes]

  def routes
    requires! :platform, type: String

    platform = params[:platform]
    platform_url_prefix = "#{Setting.qiniu_routes_url}#{platform}"
    url_tail = "?t=#{Time.now.to_i}"
    case params[:platform]
    when 'ios'
      @updated_at = Setting["route_#{platform}_updated_at"].to_i
      @d2o = "#{platform_url_prefix}_d2o.conf#{url_tail}"
      @o2d = "#{platform_url_prefix}_o2d.conf#{url_tail}"
      @glo_d2o = "#{platform_url_prefix}_glo_d2o.plist#{url_tail}"
      @glo_o2d = "#{platform_url_prefix}_glo_o2d.plist#{url_tail}"
    when 'android'
      @updated_at = Setting["route_#{platform}_updated_at"].to_i
      @d2o = "#{platform_url_prefix}_d2o#{url_tail}"
      @o2d = "#{platform_url_prefix}_o2d#{url_tail}"
      @glo_d2o = "#{platform_url_prefix}_glo_d2o#{url_tail}"
      @glo_o2d = "#{platform_url_prefix}_glo_o2d#{url_tail}"
    when 'pc'
      @updated_at = Setting["route_#{platform}_updated_at"].to_i
      @d2o = "#{platform_url_prefix}_d2o.pac#{url_tail}"
      @o2d = "#{platform_url_prefix}_o2d.pac#{url_tail}"
      @glo_d2o = "#{platform_url_prefix}_glo_d2o.pac#{url_tail}"
      @glo_o2d = "#{platform_url_prefix}_glo_o2d.pac#{url_tail}"
    when 'mac'
      @updated_at = Setting["route_#{platform}_updated_at"].to_i
      @d2o = "#{platform_url_prefix}_d2o.pac#{url_tail}"
      @o2d = "#{platform_url_prefix}_o2d.pac#{url_tail}"
      @glo_d2o = "#{platform_url_prefix}_glo_d2o.pac#{url_tail}"
      @glo_o2d = "#{platform_url_prefix}_glo_o2d.pac#{url_tail}"
    end
    @china_ip_updated_at = Setting["route_china_ip_updated_at"].to_i
    @china_ip_list = "#{Setting.qiniu_routes_url}china_ip.txt#{url_tail}"
    @foreign_ip_updated_at = Setting["route_foreign_ip_updated_at"].to_i
    @foreign_ip_list = "#{Setting.qiniu_routes_url}foreign_ip.txt#{url_tail}"
    @is_domestic = Utils::IP.is_domestic?(client_ip)
  end
end

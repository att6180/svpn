class Api::Client::V3::PlansController < Api::Client::V1::ApiController

  # comsunny平台创建订单
  def comsunny_order
    requires! :platform, type: String
    requires! :app_channel, type: String
    requires! :app_version, type: String
    requires! :plan_id, type: Integer

    # 验证套餐
    plan = Plan.find_by(id: params[:plan_id])
    if !plan.present?
      error!(api_t("invalid_plan_info")) and return
    end

    @log = TransactionLog.create(
      user_id: current_user.id,
      plan_id: plan.id,
      plan_name: plan.name,
      coins: plan.coins + plan.present_coins,
      plan_coins: plan.coins,
      present_coins: plan.present_coins,
      price: plan.price,
      app_channel: params[:app_channel],
      app_version: params[:app_version],
      is_regular_time: plan.is_regular_time,
      time_type: plan.time_type,
      node_type_id: plan.node_type_id,
      user_created_at: current_user.created_at
    )

    # 向支付平台创建订单并请求支付URL
    options = {
      order_id: @log.order_number,
      fee: plan.price.to_i * 100,
      created_at: @log.created_at.to_i,
      plan_name: '消费',
      ip: client_ip,
      user_id: current_user.id
    }
    @payment_url = Comsunny.create_order(options, Setting.comsunny_order_url)

    if @payment_url.blank?
      # 记录日志
      CreateTransactionPaymentFailedLogJob.perform_later(
        @log.order_number,
        current_user.id,
        params[:platform],
        params[:app_channel],
        params[:app_version],
        DateUtils.time2str(Time.now)
      )
      error!('支付订单创建失败', 1) and return
    end
  end

end

class Api::Client::V3::NodesController < Api::Client::V1::ApiController

  def index
    requires! :platform, type: String
    requires! :app_channel, type: String
    requires! :app_version, type: String
    requires! :app_version_number, type: String

    @node_types = NodeType.cache_list
    @node_type_indexs = @node_types.index_by(&:id)
    @user_group_indexs = UserGroup.cache_list.index_by(&:id)
    @user_node_types = current_user.user_node_types.in_node_types(@node_types.map{|n| n.id})
      .sorted_by_created_at

    # 获取渠道和版本
    plan_version = ::V2::PlanVersion.by_version(params[:app_channel], params[:app_version])
    error!(api_t("configuration_not_found"), 1) and return if plan_version.blank?
    @show_icon = plan_version.show_icon
    # 获得客户端标签列表，按id分组
    @tabs = ::V2::PlanClientTab.cache_list.index_by(&:node_type_id)
  end

end

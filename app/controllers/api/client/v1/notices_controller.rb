class Api::Client::V1::NoticesController < Api::Client::V1::ApiController

  def index
    @notice = Notice.enabled_one.take
  end

end

class Api::Client::V1::AdsController < Api::Client::V1::ApiController

  skip_before_action :authenticate_user!, only: [
    :splash_screen,
    :splash_screen_log,
    :splash_screen_click_log,
    :popup,
    :popup_log,
    :popup_click_log
  ]

  # 开屏广告信息获取
  def splash_screen
    requires! :uuid, type: String
    requires! :app_channel, type: String
    requires! :app_version, type: String
    requires! :app_version_number, type: String

    @ad = SplashScreenAd.pick_one(
      params[:app_channel],
      params[:app_version]
    )
  end

  def splash_screen_log
    requires! :ad_id, type: String
    requires! :uuid, type: String
    requires! :app_channel, type: String
    requires! :app_version, type: String
    requires! :app_version_number, type: String

    CreateSplashScreenAdLogJob.perform_later({
      ad_id: params[:ad_id],
      uuid: params[:uuid],
      app_channel: params[:app_channel],
      app_version: params[:app_version],
      app_version_number: params[:app_version_number],
      created_at: DateUtils.time2str(Time.now),
      created_date: Time.now.strftime("%Y-%m-%d")
    })
    SplashScreenAd.increase_imporessions_count(params[:ad_id])
  end

  # 开屏广告点击日志提交
  def splash_screen_click_log
    requires! :ad_id, type: String
    requires! :uuid, type: String
    requires! :app_channel, type: String
    requires! :app_version, type: String
    requires! :app_version_number, type: String

    CreateSplashScreenAdClickLogJob.perform_later({
      ad_id: params[:ad_id],
      uuid: params[:uuid],
      app_channel: params[:app_channel],
      app_version: params[:app_version],
      app_version_number: params[:app_version_number],
      created_at: DateUtils.time2str(Time.now),
      created_date: Time.now.strftime("%Y-%m-%d")
    })
    SplashScreenAd.increase_click_count(params[:ad_id])
  end

  # 弹出广告信息获取
  def pupop
    requires! :uuid, type: String
    requires! :app_channel, type: String
    requires! :app_version, type: String
    requires! :app_version_number, type: String
  end

  # 弹出广告日志提交
  def popup_log
    requires! :uuid, type: String
    requires! :app_channel, type: String
    requires! :app_version, type: String
    requires! :app_version_number, type: String

    CreatePopupAdLogJob.perform_later({
      uuid: params[:uuid],
      app_channel: params[:app_channel],
      app_version: params[:app_version],
      app_version_number: params[:app_version_number],
      created_at: DateUtils.time2str(Time.now),
      created_date: Time.now.strftime("%Y-%m-%d")
    })
  end

  # 弹出广告点击日志提交
  def popup_click_log
    requires! :uuid, type: String
    requires! :app_channel, type: String
    requires! :app_version, type: String
    requires! :app_version_number, type: String

    CreatePopupAdClickLogJob.perform_later({
      uuid: params[:uuid],
      app_channel: params[:app_channel],
      app_version: params[:app_version],
      app_version_number: params[:app_version_number],
      created_at: DateUtils.time2str(Time.now),
      created_date: Time.now.strftime("%Y-%m-%d")
    })
  end

  # 导航广告信息获取
  def navigation
    requires! :uuid, type: String
    requires! :app_channel, type: String
    requires! :app_version, type: String
    requires! :app_version_number, type: String
    optional! :update_at, type: Integer

    @hit_cache = params[:update_at].to_i == Setting.navigation_ad_update_at
    @ads = NavigationAd.cache_list
  end

  # 导航广告点击日志提交
  def navigation_click_log
    requires! :uuid, type: String
    requires! :ad_id, type: String
    requires! :ad_name, type: String
    requires! :app_channel, type: String
    requires! :app_version, type: String
    requires! :app_version_number, type: String

    CreateNavigationAdClickLogJob.perform_later({
      user_id: current_user.id,
      uuid: params[:uuid],
      ad_id: params[:ad_id],
      ad_name: params[:ad_name],
      app_channel: params[:app_channel],
      app_version: params[:app_version],
      app_version_number: params[:app_version_number],
      created_at: DateUtils.time2str(Time.now),
      created_date: Time.now.strftime("%Y-%m-%d")
    })
  end
end

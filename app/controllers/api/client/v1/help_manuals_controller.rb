class Api::Client::V1::HelpManualsController < Api::Client::V1::ApiController

  def index
    requires! :platform, type: String
    requires! :app_version, type: String
    requires! :app_version_number, type: String
    optional! :page, type: Integer
    optional! :limit, type: Integer

    platform = params[:platform]
    version = params[:app_version]
    version_number = params[:app_version_number]
    page = params[:page] || 1
    limit = params[:limit] || 30
    cache_key = "#{HelpManual::CACHE_KEY_PREFIX}#{platform}_#{version}_#{version_number}_#{page}_#{limit}"
    @help_manuals = Rails.cache.fetch(cache_key, expires_in: 1.week) do
      # 查找版本号是否在审核
      help_manuals = HelpManual.by_platform(platform).by_version(version).order(created_at: :asc)
      if SystemSetting.is_auditing?(platform, version, version_number)
        help_manuals = help_manuals.where(app_version_number: version_number)
      else
        help_manuals = help_manuals.where(app_version_number: [nil, ''])
      end
      help_manuals.page(page).per(limit).to_a
    end
  end

end

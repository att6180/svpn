class Api::Client::V1::UsersController < Api::Client::V1::ApiController

  before_action :verify_telephone, only: [:update_username]
  skip_before_action :authenticate_user!, only: [:push_proxy_message, :operation_log, :offline]

  # 修改用户名为手机号
  def update_username
    requires! :telephone, type: String
    requires! :verification_code, type: String

    telephone = params[:telephone].strip
    # 判断系统中是否已存在此手机号
    user = User.find_by(username: telephone)
    if user.present?
      error!(api_t("telphone_already_exists")) and return
    end

    # 验证手机号和验证码
    vcode = VerificationCode.code_for_telephone(telephone, :modify_username).last
    if vcode.present? && vcode&.is_correct?(params[:verification_code])
      vcode.used!
      before_username = current_user.username
      before_coins = current_user.current_coins
      before_username_changed = current_user.username_changed?
      change_params = { username: telephone }
      # 如果用户没有从分配账号修改成手机号过
      if !before_username_changed
        current_coins = current_user.current_coins + Setting.new_user_or_change_username_to_telephone_coins.to_i
        change_params.merge!(current_coins: current_coins) 
      end
      current_user.update_columns(change_params)
      # 修改设备标记
      device = Device.find_by(id: current_device_id)
      if device.present?
        device.update(username_changed: true, last_changed_user_id: current_user.id)
        device_uuid = device.uuid
        device_platform = device.platform
      end
      # 创建修改用户名为手机号日志
      CreateUserChangeLogJob.perform_later({
        user_id: current_user.id,
        change_type: Log::UserChangeLog::CHANGE_USERNAME,
        uuid: device_uuid,
        platform: device_platform,
        ip: client_ip,
        before: before_username,
        after: current_user.username,
        created_at: DateUtils.time2str(Time.now)
      })
      # 创建修改用户名为手机号赠送坚果日志
      CreateUserChangeLogJob.perform_later({
        user_id: current_user.id,
        change_type: Log::UserChangeLog::CHANGE_USERNAME_COINS,
        uuid: device_uuid,
        platform: device_platform,
        ip: client_ip,
        before: before_coins,
        after: current_user.current_coins,
        created_at: DateUtils.time2str(Time.now)
      }) if !before_username_changed
      @token = create_jwt(current_user, current_device_id)
      set_message(api_t("username_has_changed"))
    else
      error!(api_t("verification_code_is_invalid_or_expired"))
    end
  end

  # 修改密码
  def update_password
    requires! :verification_code, type: String
    requires! :password, type: String

    # 检查密码是否有效
    error!(api_t("invalid_password")) and return if !Utils.valid_password?(params[:password])

    # 检查用户是否已经更改用户名为手机号
    if current_user.username_changed?
      vcode = VerificationCode.code_for_telephone(current_user.username, :modify_password).last
      if vcode && vcode&.is_correct?(params[:verification_code])
        vcode.used!
        before_password = current_user.password
        current_user.update_columns(password: params[:password])
        # 修改设备标记
        device = Device.find_by(id: current_device_id)
        if device.present?
          device.update(password_changed: true, last_changed_user_id: current_user.id)
          device_uuid = device.uuid
          device_platform = device.platform
        end
        # 创建修改日志
        CreateUserChangeLogJob.perform_later({
          user_id: current_user.id,
          change_type: Log::UserChangeLog::CHANGE_PASSWORD,
          uuid: device_uuid,
          platform: device_platform,
          ip: client_ip,
          before: before_password,
          after: current_user.password,
          created_at: DateUtils.time2str(Time.now)
        })
        @token = create_jwt(current_user, current_device_id)
        set_message(api_t("password_has_changed"))
      else
        error!(api_t("verification_code_is_invalid_or_expired"))
      end
    else
      error!(api_t("username_is_not_changed"))
    end
  end

  # 用户完善信息
  def update_profile
    optional! :email, type: String
    optional! :wechat, type: String
    optional! :qq, type: String
    optional! :weibo, type: String

    email = params[:email]
    wechat = params[:wechat]
    qq = params[:qq]
    weibo = params[:weibo]
    if email.blank? && wechat.blank? && qq.blank? && weibo.blank?
      error!(api_t("at_lease_one_item"))
    else
      current_user.email = email if email.present?
      current_user.wechat = wechat if wechat.present?
      current_user.qq = qq if qq.present?
      current_user.weibo = weibo if weibo.present?
      if current_user.changed?
        error_detail!(current_user) if !current_user.save
      end
    end
  end

  # 提交操作日志
  def operation_log
    optional! :app_launch_id, type: String
    optional! :signin_log_id, type: String
    requires! :app_channel, type: String
    requires! :app_version, type: String
    requires! :app_version_number, type: String
    requires! :operations, type: String

    log_params = {
      app_launch_id: params[:app_launch_id],
      signin_log_id: params[:signin_log_id],
      app_channel: params[:app_channel],
      app_version: params[:app_version],
      app_version_number: params[:app_version_number],
      operations: params[:operations]
    }

    CreateUserOperationLogJob.perform_later(log_params, current_device_id)
    if Setting.trend_submit_enabled == "true"
      Trend::OperationLogJob.perform_later(log_params)
    end
  end

  # 提交分享日志
  def share
    requires! :share_type, type: Integer
    requires! :app_channel, type: String
    requires! :app_version, type: String
    requires! :app_version_number, type: String

    current_user.user_share_logs.create(
      share_type: UserShareLog.share_types.key(params[:share_type].to_i),
      app_channel: params[:app_channel],
      app_version: params[:app_version],
      app_version_number: params[:app_version_number]
    )
  end

  # 用户签到
  def checkin
    if RedisCache.is_concurrent?("client_checkin_lock_#{current_user.id}")
      error!(api_t('too_frequent_access'), 100) and return
    end

    # 检查当前用户是否有签到过
    @last_log, @new_expired_at = current_user.checkin!
    @has_checkin_today = current_user.has_checkin_today?
    @days_of_month = current_user.checkin_days_of_month
    logs = current_user.user_checkin_logs.month(Time.now.year, Time.now.month)
    @days = logs.map { |log| log.created_at.day }

    # 记录用户活跃日志
    CreateUserActiveLogJob.perform_later(
      user_id: current_user.id,
      created_at: DateUtils.time2str(Time.now),
      app_channel: current_user.create_app_channel,
      app_version: current_user.create_app_version,
      active_type: UserActiveHourLog.active_types[:checkin]
    )
  end

  # 获取当前用户最新信息
  def profile
    @node_types = NodeType.cache_list
    @node_type_indexs = @node_types.index_by(&:id)
    @user_node_types = current_user.user_node_types
      .in_node_types(@node_types.map(&:id))
      .sorted_by_created_at
  end

  # 设置用户为离线
  def offline
    requires! :user_id, type: Integer

    user = User.find_by(id: params[:user_id])
    user.offline! if user.present?
  end

  # 向用户推送消息
  # 比如用户的代理端检查用户有效期到期后，会重置用户的session_token
  # 这时候客户端会来请求此api来通知客户端代理连接丢失
  def push_proxy_message
    requires! :uuid, type: String
    optional! :platform, type: String
    optional! :app_version, type: String
    optional! :app_version_number, type: String

    # 弃用此接口
    return

    uuid = params[:uuid].gsub('-', '')
    if Jpush.allow_to_push?(uuid)
      title = api_t('proxy_connection_lost')
      # 获取对应版本及版本号的推送认证信息(key和scret)
      if params[:platform].present? && params[:app_version].present?
        jpush_auth_data = SystemSetting.jpush_auth_data(params[:platform], params[:app_version])
        if jpush_auth_data.blank?
          error!(api_t('invalid_jpush_auth_data'), 1) and return
        end
      else
        jpush_auth_data = {key: CONFIG.jpush_app_key, secret: CONFIG.jpush_secret}
      end
      jpush = JPush::Client.new(jpush_auth_data[:key], jpush_auth_data[:secret])
      audience = JPush::Push::Audience.new
      audience.set_alias([uuid])
      notification = JPush::Push::Notification.new.
        set_alert(title).
        set_android(
          alert: title,
      ).set_ios(
        alert: title,
      )
      payload =JPush::Push::PushPayload.new(
        platform: 'all',
        audience: audience,
        notification: notification
      )
      begin
        @result = jpush.pusher.push(payload)
      rescue
        error!(api_t('cannot_find_push_audience'), 2) and return
      end
    else
      @result = nil
    end
  end

  # 用户提交代理解析日志
  def proxy_connection_logs
    requires! :node_id, type: Integer
    requires! :description, type: String
    requires! :data, type: String

    node = Node.find_by(id: params[:node_id])
    if node.blank?
      error!(api_t('node_does_not_exist')) and return
    end

    current_user.proxy_connection_logs.create(
      node_id: node.id,
      node_ip: node.url,
      description: params[:description],
      data: params[:data]
    )
  end

  # 自动为用户续费
  def renew
    requires! :node_id, type: Integer

    node = Node.find_by(id: params[:node_id])
    if node.blank?
      error!(api_t('node_does_not_exist')) and return
    end

    # 判断自动续费开关是否开启
    if Setting.auto_renew.to_i == 0
      error!(api_t("auto_renew_is_disabled"), 4) and return
    end

    node_type = node.node_type
    # 判断服务是否到期
    if current_user.node_type_is_expired?(node_type.id)
      # 判断钻石够不够钻石
      if current_user.current_coins < node_type.expense_coins
        error!(api_t("coins_is_not_enough"), 3) and return
      end
      @user_node_type = current_user.consume_coins(node_type)
      @renewd = true
    else
      @user_node_type = current_user.node_type_by_id(node_type.id)
      @renewd = false
    end
  end

  # 绑定推广码
  def bind_promo_code
    requires! :app_channel, type: String
    requires! :app_version, type: String
    requires! :app_version_number, type: String
    requires! :promo_code, type: String

    # 检查本帐号是否已绑定过别人的推广码
    if current_user.promo_user_id.present?
      error!(api_t('already_promoed_user'), 1) and return
    end

    # 查找推广码对应的用户
    user = User.find_by(promo_code: params[:promo_code])
    if user.blank?
      error!(api_t('invalid_promo_code'), 2) and return
    end

    # 检查推广人目前的邀请人数
    if user.promo_users_count > Setting.promo_promoter_bind_count.to_i
      error!(api_t('promo_count_upper_limit'), 4) and return
    end

    # 检查是否是自己的推广码
    if current_user.id == user.id
      error!(api_t('cannot_use_promo_code'), 3) and return
    end

    # 绑定
    current_user.bind_promoter!(user, params[:promo_code])
    @accepted_coins = Setting.promo_accepted_user_coins.to_i
    set_message(api_t('promo_bind_successed', {coins: Setting.promo_accepted_user_coins.to_i}))
  end

  # 设置是否自动扣除钻石
  def set_auto_renew
    requires! :auto_renew, type: Integer

    auto_renew = params[:auto_renew] == '0' ? false : true
    current_user.update(auto_renew: auto_renew)
  end

  private

  def verify_telephone
    error!(api_t("invalid_telephone_number")) if params[:telephone].length != 11 || params[:telephone].first != '1'
  end

end

class Api::Client::V1::UserCheckinLogsController < Api::Client::V1::ApiController

  # 按指定年月获取当月已签到的日期列表
  def index
    optional! :year, type: Integer
    optional! :month, type: Integer

    year = params[:year] || Time.now.year
    month = params[:month] || Time.now.month

    logs = current_user.user_checkin_logs.month(year, month)
    @days = logs.map { |log| log.created_at.day }
  end

end

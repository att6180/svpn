class Api::Client::V1::LogsController < Api::Client::V1::ApiController

  skip_before_action :authenticate_user!, only: [:client_connection_logs]

  # 提交客户端去向日志
  def client_connection_logs
    requires! :uuid, type: String
    requires! :app_channel, type: String
    requires! :app_version, type: String
    requires! :app_version_number, type: String
    requires! :data, type: String
    # user_id,node_id,node_ip,target_url,is_proxy,created_at,addition_content

    # 暂时关闭客户端调试日志
    return Setting.client_connection_log_enabled != 'true'

    new_logs = []
    logs = params[:data].split('|')
    logs.each do |log|
      next if log.blank?
      log_params = log.split(',')
      ip = client_ip
      ip_region = Ipnet.find_by_ip(ip)
      user_id = log_params[0]
      node_id = log_params[1]
      node_ip = log_params[2]
      target_url = log_params[3]
      is_proxy = log_params[4] == '1'
      ts = log_params[5].to_i
      created_at = ts > 0 ? Utils.ts2t(ts) : Time.now
      addition_content = log_params[6].to_i
      new_logs << ClientConnectionLog.new(
        user_id: user_id,
        node_id: node_id,
        uuid: params[:uuid]&.downcase,
        node_ip: node_ip,
        target_url: target_url,
        client_ip: ip,
        client_ip_country: ip_region[:country],
        client_ip_province: ip_region[:province],
        client_ip_city: ip_region[:city],
        is_proxy: is_proxy,
        addition_content: addition_content,
        app_channel: params[:app_channel],
        app_version: params[:app_version],
        app_version_number: params[:app_version_number],
        created_at: created_at,
        updated_at: created_at
      )
    end
    ClientConnectionLog.bulk_insert(new_logs, disable_timestamps: true) if new_logs.present?
  end

  # 提交客户端调试日志
  def client_debug_logs
    requires! :platform, type: String
    requires! :uuid, type: String
    requires! :log_file, type: String

    #uuid = params[:uuid].gsub(/[-:]/, "")
    #date = Time.now.strftime("%Y%m%d")
    #file_name = "#{current_user.id}_#{params[:platform]}_#{uuid}_#{date}.txt"

    dir_path = Rails.root.join('log', 'client_debug_logs')
    FileUtils::mkdir_p dir_path if !File.directory?(dir_path)
    log_files = params[:log_file]
    log_files.each do |log_file|
      error!(api_t("invalid_file_type"), 1) and return if !log_file.is_a?(ActionDispatch::Http::UploadedFile) ||
        log_file.content_type != "text/plain"
      QiniuStore.upload_debug_log(log_file.tempfile, log_file.original_filename)
      #File.open(Rails.root.join('log', 'client_debug_logs', log_file.original_filename), "wb") do |file|
        #file.write(log_file.read)
      #end
    end
  end

end

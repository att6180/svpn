class Api::Client::V1::WebsitesController < Api::Client::V1::ApiController

  def index
    optional! :page, type: Integer
    optional! :limit, type: Integer

    is_domestic = Utils::IP.is_domestic?(client_ip)
    @items = WebsiteNavigation
      .by_sorted
      .where(is_domestic: !is_domestic)
      .page(param_page).per(param_limit)
  end

  # 网址导航点击日志提交
  def website_navigation_click_log
    requires! :uuid, type: String
    requires! :website_id, type: String
    requires! :website_name, type: String
    requires! :nav_type, type: String
    requires! :info_type, type: String
    requires! :app_channel, type: String
    requires! :app_version, type: String
    requires! :app_version_number, type: String

    website = WebsiteNavigation.find(params[:website_id])

    CreateWebsiteNavigationClickLogJob.perform_later({
      user_id: current_user.id,
      uuid: params[:uuid],
      website_id: params[:website_id],
      website_name: params[:website_name],
      nav_type: params[:nav_type],
      info_type: params[:info_type],
      is_domestic: website.is_domestic,
      app_channel: params[:app_channel],
      app_version: params[:app_version],
      app_version_number: params[:app_version_number],
      created_at: DateUtils.time2str(Time.now),
      created_date: Time.now.strftime("%Y-%m-%d")
    })
  end

end

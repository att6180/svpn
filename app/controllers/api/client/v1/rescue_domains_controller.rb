class Api::Client::V1::RescueDomainsController < Api::Client::V1::ApiController
  skip_before_action :authenticate_user!, only: [:list]
  
  def list
    @rescue_domains = RescueDomain.enabled.limit(10)
  end
end
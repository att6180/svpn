class Api::Client::V1::SessionsController < Api::Client::V1::ApiController

  skip_before_action :authenticate_user!

  # 登录
  # 如果提供了用户名和密码
  #   验证通过
  #   查找设备是否存在
  #     如果不存在则创建，并于用户帐号关联
  #     如果存在，则检查是否与当前用户关联，未关联则关联
  # 如果不提供手机号(用户名)和密码
  #   检查用户的uuid是否已存在
  #     如果存在
  #       如果修改过用户名或密码，则返回最后一次修改的用户名
  #       如果未修改过用户名或密码，则返回首次打开app分配的用户数据
  #     如果不存在
  #       创建用户帐号
  #       创建此设备，并与创建的用户帐号关联
  def create
    requires! :device_uuid, type: String
    optional! :device_name, type: String
    requires! :device_model, type: String
    requires! :platform, type: String
    requires! :system_version, type: String
    requires! :operator, type: String
    requires! :net_env, type: String
    requires! :app_version, type: String
    requires! :app_version_number, type: String
    requires! :app_channel, type: String
    optional! :username, type: String
    optional! :password, type: String
    optional! :app_launch_id, type: Integer
    optional! :longitude, type: String
    optional! :latitude, type: String
    optional! :address, type: String
    optional! :coord_method, type: String

    @only_has_username = false
    # 客户端ip
    @client_ip = client_ip
    # 获取客户端区域
    @ip_region = client_ip_region
    # 是否是新用户
    @is_new_user = false
    # 参数
    @request_params = params
    if params[:username].present? && params[:password].present?
      user = User.find_by(username: params[:username])
      error!(api_t("user_does_not_exist"), 1) and return if user.blank?
      error!(api_t("user_has_been_disabled"), 2) and return if !user.is_enabled?
      error!(api_t("invalid_username_or_password"), 3) and return if user.password != params[:password]
      after_signin(user)
    else
      # 根据uuid查找设备是否存在
      device = Device.find_by(uuid: params[:device_uuid].downcase)
      # 如果设备存在
      if device.present?
        # 如果设备没有修改过用户名或密码
        if !device.username_or_password_is_changed?
          # 返回第一次分配的帐号
          user = device.first_assign_user
          if user.is_enabled?
            after_signin(user)
          else
            error!(api_t("user_has_been_disabled"), 2)
          end
        # 否则返回最后一次修改了帐号或密码的用户名
        else
          @only_has_username = true
          @user = device.first_assign_user
          #error!(api_t("username_or_password_is_changed"), 4)
        end
      # 如果设备不存在
      else
        # 如果该ip超过每天注册上限
        error!(api_t('too_frequent_access'), 100) and return if Guard.is_signup_exceeded?(client_ip)

        # 创建用户并与设备关联
        user = UserService.new_user(params, @client_ip, @ip_region)
        if user.save
          after_signin(user)
        else
          error_detail!(user)
        end
      end
    end
  end

  private

  def after_signin(user)
    @user = user
    @node_types = NodeType.enabled.sorted_by_level.includes(:user_group, node_regions: :nodes)
    UserNodeType.check_types(@user, @user.user_node_types, @node_types)
    @user_node_types = @user.user_node_types.in_node_types(@node_types.map(&:id)).sorted_by_created_at.includes(:node_type)
    @device = Device.find_or_create_by_user(@user, @request_params)
    @user_session_id = SessionService.find_or_create_session(@user)
    SessionService.signin_log_perform_later(@is_new_user, @user.id, @user.username, @device.id, @user_session_id, @request_params, @client_ip, @ip_region)
    # 如果不是新用户登录, 重新生新生成token存入缓存并使用队列更新登录信息
    SessionService.update_signin_info_perform_later(@user, @client_ip) if !@is_new_user
    SessionService.signup_log_perform_later(@user.id, @user.username, @client_ip, @ip_region) if @is_new_user
    @api_token = create_jwt(@user, @device.id)
    @settings = SystemSetting.by_platform_and_app_version(
      @request_params[:platform],
      @request_params[:app_channel],
      @request_params[:app_version],
      @request_params[:app_version_number]
    )
  end

end

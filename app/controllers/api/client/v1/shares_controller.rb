class Api::Client::V1::SharesController < Api::Client::V1::ApiController

  # 分享模版获取
  def templates
    optional! :template_update_time, type: Integer

    @hit_cache = params[:template_update_time].to_i == Setting.share_template_update_time
    @templates = ShareTemplate.cache_list
  end

end

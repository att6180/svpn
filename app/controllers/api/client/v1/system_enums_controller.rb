class Api::Client::V1::SystemEnumsController < Api::Client::V1::ApiController

  skip_before_action :authenticate_user!

  def index
    enum_type = params[:type] || 0
    @enums = SystemEnum.enums_by_type(enum_type)
  end

end

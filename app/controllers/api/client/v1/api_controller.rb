class Api::Client::V1::ApiController < ActionController::API

  helper ApplicationHelper
  include ActionView::Layouts
  include ActionController::Helpers
  include ActionController::Caching

  layout 'client_api'

  helper_method :current_user, :encrypt_string

  before_action :setup_layout_elements
  before_action :authenticate_user!

  def append_info_to_payload(payload)
    if LogStasher.enabled?
      super
      LogStasher.store[:user_id] = current_user.id if current_user.present?
      LogStasher.store[:remote_client_ip] = client_ip
    end
  end

  class ParameterDecryptFail < ActionController::ParameterMissing
    def initialize(param)
      @param = param
      super(param)
    end
  end

  class ParameterDecodeFail < ActionController::ParameterMissing
    def initialize(param)
      @param = param
      super(param)
    end
  end

  class ParameterValueNotAllowed < ActionController::ParameterMissing
    attr_reader :values
    def initialize(param, values)
      @param = param
      @values = values
      super("参数#{param}的值只允许在#{values}以内")
    end
  end

  class AccessDenied < StandardError; end
  class PageNotFound < StandardError; end
  class UserIsDisabled < StandardError; end
  class UserIsNonExistent < StandardError; end
  class UserMaliciousAct < StandardError; end

  rescue_from(ActionController::ParameterMissing) do |err|
    render json: { success: false, error: 'ParameterInvalid', message: "参数#{err.param}未提供或为空" }, status: 200
  end
  rescue_from(ActiveRecord::RecordInvalid) do |err|
    render json: { success: false, error: 'RecordInvalid', message: err }, status: 400
  end
  rescue_from(AccessDenied) do |err|
    render json: { success: false, error: 'AccessDenied', message: err }, status: 403
  end
  rescue_from(ActiveRecord::RecordNotFound) do
    render json: { success: false, error: 'ResourceNotFound', message: '记录未找到' }, status: 404
  end
  rescue_from(ParameterDecryptFail) do |err|
    render json: { success: false, error: 'ParameterDecryptFail', message: "参数#{err.param}解密失败" }, status: 400
  end
  rescue_from(ParameterDecodeFail) do |err|
    render json: { success: false, error: 'ParameterDecodeFail', message: "参数#{err.param}解码失败" }, status: 400
  end

  def current_user
    @current_user
  end

  def current_device_id
    @current_device_id
  end

  def client_ip
    # 首先获取 X_REAL_IP，如果开头为10，或者为共享地址，则获取CDN_SRC_IP
    # 如果还是为空(本地开发环境)，则获取remote_ip
    result = request.headers["HTTP_X_REAL_IP"]&.split(',')&.first
    if result.blank? || Utils::IP.is_private_ip?(result)
      result = request.headers["HTTP_CDN_SRC_IP"]
    end
    result = request.remote_ip if result.blank?
    result
  end

  #def client_ip
    #result = request.headers["X-Forwarded-For"]&.split(',')&.first
    #result = request.remote_ip if result.blank?
    #result
  #end

  def client_ip_region
    Ipnet.find_by_ip(client_ip)
  end

  # 设置必选参数
  def requires!(name, opts = {})
    opts[:require] = true
    optional!(name, opts)
  end

  def optional!(name, opts = {})
    if params[name].blank? && opts[:require] == true
      raise ActionController::ParameterMissing.new(name)
    end

    if opts[:values] && params[name].present?
      values = opts[:values].to_a
      if !values.include?(params[name]) && !values.include?(params[name].to_i)
        raise ParameterValueNotAllowed.new(name, opts[:values])
      end
    end

    if params[name].blank? && opts[:default].present?
      params[name] = opts[:default]
    end
  end

  # 设置成功消息
  def set_message(msg)
    @message = msg
  end

  # 返回错误信息
  def error!(msg, code = nil)
    @success = false
    @message = msg
    json = { success: @success, message: @message }
    json[:error_code] = code if code.present?
    render json: json
  end

  # 显示详细错误信息
  def error_detail!(obj, code = nil)
    @success = false
    message = obj.errors.messages.first
    if message.present?
      if message.count > 1
        @message = message.last.first
      else
        @message = message
      end
    else
      @message = '请求失败'
    end
    json = { success: @success, message: @message }
    json[:error_code] = code if code.present?
    render json: json
  end

  def param_page
    params[:page] || 1
  end

  def param_limit
    result = params[:limit] || 25
    result = 50 if result.to_i > 50
    result
  end

  def api_t(key, param = nil)
    I18n.t("api.#{key}", param)
  end

  def setup_layout_elements
    @success = true
    @message = nil
  end

  # 创建客户端API访问Token
  def create_jwt(user, device_id)
    payload = { id: user.id, username: user.username, device_id: device_id }
    app_api_token = User.get_cache_app_api_token(user.id)
    JWT.encode(payload, app_api_token)
  end

  def encrypt_string(text)
    Utils::AesEncrypt.encrypt(text, CONFIG.client_secret_key)
  end

  # 加密请求参数
  def encrypt_params
    pure_params = params
    pure_params.delete_if{|k| ['format', 'controller', 'action'].include?(k) }
    data = Utils::AesEncrypt.encrypt(pure_params.to_json, CONFIG.api_encrypt_key)
  end

  # 加密返回数据
  def encrypt_response_data(template, locals = nil)
    rendered_string = render_to_string(template: "api/v1/client/#{template}", locals: locals, layout: false)
    Utils::AesEncrypt.encrypt(rendered_string, CONFIG.api_encrypt_key)
  end

  # 解密请求数据
  def decrypt_request_data!
    if params[:data].blank?
      raise ActionController::ParameterMissing.new(:data)
    end
    # decrypet
    begin
      decrypted_data = Utils::AesEncrypt.decrypt(params[:data], CONFIG.api_encrypt_key)
    rescue ArgumentError
      raise ParameterDecryptFail.new(:data)
    end
    # decode to json object
    begin
      decode_params = ActiveSupport::JSON.decode(decrypted_data).symbolize_keys
    rescue ActiveSupport::JSON.parse_error
      raise ParameterDecodeFail.new(:data)
    end
    raise ParameterDecodeFail.new(:data) if params.is_a?(Integer) || params.is_a?(Fixnum)
    params = ActionController::Parameters.new(decode_params)
  end

  # 根据headers认证用户
  def authenticate_user!
    auth_jwt!
    rescue JWT::ExpiredSignature
      error!('令牌过期', 101)
    rescue JWT::VerificationError
      error!('令牌验证错误', 101)
    rescue JWT::DecodeError
      error!('令牌非法', 101)
    rescue JWT::InvalidIssuerError
      error!(e, 101)
    rescue UserIsDisabled
      error!(api_t("user_has_been_disabled"), 102)
    rescue UserMaliciousAct
      render json: { success: false, message: "" }, status: 400
  end

  def auth_jwt!
    jwt = request.headers['HTTP_AUTHORIZATION']&.split(' ')&.last
    raise JWT::VerificationError.new if jwt.blank?
    # 读取令牌携带用户信息，此处不作令牌的验证，因为密钥要从用户信息里取，不会抛出异常
    payload, header = JWT.decode(jwt, nil, false, verify_expiration: true)
    user = User.find_by(id: payload['id'])
    raise(UserIsNonExistent, "用户不存在") if user.blank?
    app_api_token = User.get_cache_app_api_token(user.id)
    # 获取验证令牌的密钥
    secret_key = user ? app_api_token : ''
    # 用秘钥验证令牌，会抛出 JWT::ExpiredSignature 或 JWT::DecodeError 异常
    payload, header = JWT.decode(jwt, secret_key)
    # 判断用户账号是否在恶意行为名单
    raise(UserMaliciousAct, "当前帐号已被封") if user.is_malicious_act?
    # 判断用户帐号是否被封
    raise(UserIsDisabled, "当前帐号已被封") if !user.is_enabled?
    # 验证成功，设置当前用户
    @current_user = user
    @current_device_id = payload['device_id']
  end

  def authenticate_custom_token!(header_token)
    authenticate_token(header_token) || render_unauthorized
  end

  def authenticate_token(header_token)
    token = request.headers['AUTHTOKEN']&.split(' ')&.last
    header_token == token
  end

  def render_unauthorized(realm = "Application")
    render json: { success: false, error: 'Unautherized', messages: ['Authorization has been denied for this request.']}, status: 403
  end

  def authenticate_route_generater!
    authenticate_custom_token!(CONFIG.route_generater_api_token)
  end

end

class Api::Client::V1::DynamicServersController < Api::Client::V1::ApiController

  skip_before_action :authenticate_user!, only: :index

  def index
    @servers = DynamicServer.enabled.sorted
  end

end

class Api::Client::V1::CommonsController < Api::Client::V1::ApiController

  skip_before_action :authenticate_user!, only: [:client_log_token, :location_test, :location, :routes, :upload_routes]
  before_action :authenticate_route_generater!, only: [:upload_routes]

  def index
  end

  def client_log
  end

  def location_test
    result = <<-EOF
0.0.0.0/8
10.0.0.0/8
100.64.0.0/10
127.0.0.0/8
169.254.0.0/16
172.16.0.0/12
192.0.0.0/24
192.0.0.0/29
192.0.0.8/32
192.0.0.9/32
192.0.0.10/32
192.0.0.170/32
192.0.0.171/32
192.0.2.0/24
192.31.196.0/24
192.52.193.0/24
192.88.99.0/24
192.168.0.0/16
192.175.48.0/24
198.18.0.0/15
198.51.100.0/24
203.0.113.0/24
240.0.0.0/4
EOF
    result += "\n\n首先获取 X_REAL_IP，如果IP在如上列表范围里，则获取CDN_SRC_IP \n\n"
    result += "如果还是为空(本地开发环境)，则获取remote_ip\n\n"
    result += "否则就取 X_REAL_IP\n\n"
    forwarded = request.headers["X-Forwarded-For"]
    remote_ip = request.remote_ip
    result += "key: X-Forwarded-For(已忽略), value: #{forwarded}\n"
    result += "key: remote_ip, value: #{remote_ip}\n"
    x_real_ip = request.headers["HTTP_X_REAL_IP"]
    result += "key: X_REAL_IP, value: #{x_real_ip}\n"
    cdn_ip = request.headers["HTTP_CDN_SRC_IP"]
    result += "key: CDN_SRC_IP, value: #{cdn_ip}\n"
    finish_ip = client_ip
    result += "最终获取为: #{finish_ip}"
    result += "\n\n\n"
    request.headers.each do |k, v|
      result += "key: #{k}, value: #{v} \n\n" if k.start_with?("HTTP") && k != 'HTTP_COOKIE'
    end
    render plain: result
  end

  def location
    @client_ip = client_ip
    @is_domestic = Utils::IP.is_domestic?(@client_ip)
  end

  def routes
    requires! :platform, type: String

    case params[:platform]
    when 'ios'
      ios_d2o = File.mtime("public/#{CONFIG.route_ios_d2o}").to_i
      ios_o2d = File.mtime("public/#{CONFIG.route_ios_o2d}").to_i
      updated_at = ios_d2o > ios_o2d ? ios_d2o : ios_o2d
      @d2o = CONFIG.route_ios_d2o
      @o2d = CONFIG.route_ios_o2d
    when 'android'
      android_d2o = File.mtime("public/#{CONFIG.route_android_d2o}").to_i
      android_o2d = File.mtime("public/#{CONFIG.route_android_o2d}").to_i
      updated_at = android_d2o > android_o2d ? android_d2o : android_o2d
      @d2o = CONFIG.route_android_d2o
      @o2d = CONFIG.route_android_o2d
    when 'pc'
      pc_d2o = File.mtime("public/#{CONFIG.route_pc_d2o_o}").to_i
      pc_o2d = File.mtime("public/#{CONFIG.route_pc_o2d_o}").to_i
      updated_at = pc_d2o > pc_o2d ? pc_d2o : pc_o2d
      @d2o = CONFIG.route_pc_d2o_o
      @o2d = CONFIG.route_pc_o2d_o
    when 'mac'
      mac_d2o = File.mtime("public/#{CONFIG.route_mac_d2o}").to_i
      mac_o2d = File.mtime("public/#{CONFIG.route_mac_o2d}").to_i
      updated_at = mac_d2o > mac_o2d ? mac_d2o : mac_o2d
      @d2o = CONFIG.route_mac_d2o
      @o2d = CONFIG.route_mac_o2d
    end
    @updated_at = Utils.convert2minute(updated_at)
    @china_ip_updated_at = File.mtime("public/#{CONFIG.route_china_ip_list}").to_i
    @china_ip_list = CONFIG.route_china_ip_list
    @foreign_ip_updated_at = File.mtime("public/#{CONFIG.route_foreign_ip_list}").to_i
    @foreign_ip_list = CONFIG.route_foreign_ip_list
    @is_domestic = Utils::IP.is_domestic?(client_ip)
  end

  def upload_routes
    optional! :ios_d2o, type: String
    optional! :ios_o2d, type: String
    optional! :ios_glo_d2o, type: String
    optional! :ios_glo_o2d, type: String
    optional! :ios_v9_glo_d2o, type: String
    optional! :ios_v9_glo_o2d, type: String
    optional! :android_d2o, type: String
    optional! :android_o2d, type: String
    optional! :android_glo_d2o, type: String
    optional! :android_glo_o2d, type: String
    optional! :pc_d2o, type: String
    optional! :pc_o2d, type: String
    optional! :pc_glo_d2o, type: String
    optional! :pc_glo_o2d, type: String
    optional! :mac_d2o, type: String
    optional! :mac_o2d, type: String
    optional! :mac_glo_d2o, type: String
    optional! :mac_glo_o2d, type: String
    optional! :china_ip, type: String
    optional! :foreign_ip, type: String

    params_count = 0
    successed_count = 0
    [
      :ios_d2o, :ios_o2d, :ios_glo_d2o, :ios_glo_o2d,
      :ios_v9_glo_d2o, :ios_v9_glo_o2d, :android_d2o,
      :android_o2d, :android_glo_o2d, :android_glo_d2o,
      :pc_d2o, :pc_o2d, :pc_glo_d2o, :pc_glo_o2d, :mac_d2o, :mac_o2d,
      :mac_glo_d2o, :mac_glo_o2d, :china_ip, :foreign_ip
    ].each do |sym|
      if params[sym].present?
        params_count += 1
        key = sym.to_s
        file = params[sym]
        status = QiniuStore.upload_route(file.tempfile, file.original_filename)
        if status == 200
          successed_count += 1
          updated_key = key.gsub('_d2o', '').gsub('_o2d', '').gsub('_glo_d2o', '').gsub('_glo_o2d', '')
          Setting["route_#{updated_key}_updated_at"] = Time.now.to_i
        end
      end
    end
    if params_count <= 0
      error!(api_t("at_lease_one_params"), 1) and return
    end
    if params_count > successed_count
      error!(api_t("at_lease_one_failed"), 2) and return
    end
  end

end

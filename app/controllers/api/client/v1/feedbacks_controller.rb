class Api::Client::V1::FeedbacksController < Api::Client::V1::ApiController

  def index
    optional! :page, type: Integer
    optional! :limit, type: Integer
    current_user.feedbacks.processed.update_all(has_read: true, has_read_at: Time.now)
    # 因为update_all不会触发feedback的before_update，所以这里手动更新缓存
    RedisCache.hset_bool(Feedback::CACHE_KEY_NEW_MESSAGE, current_user.id, false)
    @feedbacks = current_user.feedbacks.sorted.page(param_page).per(param_limit)
  end

  def create
    requires! :feedback_type, type: Integer
    requires! :app_version, type: String
    requires! :app_version_number, type: String
    requires! :app_channel, type: String
    requires! :content, type: String
    optional! :email, type: String

    # 检查用户当天发送次数是否超过阀值
    error!(api_t("feedback_reached_daily_limit")) and return if !Feedback.allow_today?(current_user.id)

    @feedback = Feedback.new(
      user_id: current_user.id,
      feedback_type: Feedback.feedback_types.key(params[:feedback_type].to_i),
      app_version: params[:app_version],
      app_version_number: params[:app_version_number],
      app_channel: params[:app_channel],
      content: params[:content],
      email: params[:email],
      has_read2: true,
      has_read_at2: Time.now
    )
    if @feedback.save
      set_message(api_t("feedback_has_sent"))
    else
      error_detail!(@feedback)
    end
  end

  def reply
    requires! :feedback_id, type: Integer
    requires! :content, type: String

    feedback = Feedback.find_by(id: params[:feedback_id])
    if feedback.blank?
      error!(I18n.t("api.feedback_is_not_existence")) and return
    end
    if feedback.user_id != current_user.id
      error!(I18n.t("api.cannot_to_reply")) and return
    end
    log = feedback.feedback_logs.build(
      memberable: current_user,
      content: params[:content]
    )
    if log.save
      feedback.update(status: 0, status_updated_at: Time.now)
    else
      error_detail!(log)
    end
  end

  def replies
    requires! :feedback_id, type: Integer
    optional! :page, type: Integer
    optional! :limit, type: Integer

    feedback = Feedback.find_by(id: params[:feedback_id])
    if feedback.blank?
      error!(I18n.t("api.feedback_is_not_existence")) and return
    end
    limit = params[:limit] || 10
    limit = 30 if limit.to_i > 30
    # 设置反馈为已读
    feedback.update(has_read2: true, has_read_at2: Time.now)
    # 获取用户是否还有新的未读反馈
    @has_new_message = current_user.has_new_message2?
    @logs = feedback.feedback_logs.order(created_at: :desc)
      .page(param_page).per(limit)
  end

  def mark_as_read
    requires! :id, type: Integer

    feedback = current_user.feedbacks.find_by(id: params[:id])
    if feedback.present?
      feedback.update_columns(has_read2: true, has_read_at2: Time.now)
    else
      error!(I18n.t("api.feedback_is_not_existence"))
    end
  end

end

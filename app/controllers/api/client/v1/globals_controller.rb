class Api::Client::V1::GlobalsController < Api::Client::V1::ApiController

  def index
    @user_hit_test_speed = Setting.test_speed_user_ids
      .split('|')
      .map(&:to_i)
      .include?(current_user.id)
  end
end

class Api::Client::V1::FailedLogsController < Api::Client::V1::ApiController

  skip_before_action :authenticate_user!

  # 提交连接失败信息
  def connection_failed
    requires! :uuid, type: String
    requires! :app_channel, type: String
    requires! :app_version, type: String
    requires! :app_version_number, type: String
    requires! :device_model, type: String
    requires! :operator, type: String
    requires! :data, type: String

    new_logs = []
    usernames = []
    logs = params[:data].split('|')
    logs.each do |log|
      next if log.blank?
      log_items = log.split(',')
      usernames << log_items[1]
    end
    users = User.select(:id, :username).by_username(usernames).index_by(&:username)
    logs.each do |log|
      next if log.blank?
      log_items = log.split(',')

      ip = client_ip
      ip_region = Ipnet.find_by_ip(ip)
      ts = log_items[2].to_i
      created_at = ts > 0 ? Utils.ts2t(ts) : Time.now
      node_type = log_items[0] == '1' ? log_items[3] : nil
      node_region = log_items[0] == '1' ? log_items[4] : nil
      node_name = log_items[0] == '1' ? log_items[5] : nil
      username = log_items[1]
      user_id = users[username]&.id

      if user_id.present?
        new_logs << ConnectionFailedLog.new(
          id: Utils::Gen.generate_uuid,
          fail_type: log_items[0],
          user_id: user_id,
          username: username,
          device_uuid: params[:uuid]&.downcase,
          device_model: params[:device_model],
          node_type: node_type,
          node_region: node_region,
          node_name: node_name,
          ip: ip,
          ip_country: ip_region[:country],
          ip_province: ip_region[:province],
          ip_city: ip_region[:city],
          app_channel: params[:app_channel],
          app_version: params[:app_version],
          app_version_number: params[:app_version_number],
          operator: params[:operator],
          created_at: created_at,
          updated_at: created_at
        )
      end
    end
    ConnectionFailedLog.bulk_insert(new_logs, use_provided_primary_key: true, disable_timestamps: true) if new_logs.present?
  end

  # 提交登录失败信息
  def signin_failed
    requires! :uuid, type: String
    requires! :data, type: String
    # 类型,用户名,时间|类型,用户名,时间

    usernames = []
    param_logs = params[:data].split('|')
    param_logs.each do |log|
      next if log.blank?
      log_items = log.split(',')
      usernames << log_items[1]
    end
    users = User.select(:id, :username).by_username(usernames).index_by(&:username)
    param_logs.each do |plog|
      next if plog.blank?
      log_item = plog.split(',')
      ts = log_item[1].to_i
      created_at = ts > 0 ? Utils.ts2t(ts) : Time.now
      username = log_item[1]
      user_id = users[username]&.id
      if user_id.present?
        UserSigninFailedLog.create(
          fail_type: UserSigninFailedLog.fail_types.keys[log_item[0].to_i],
          user_id: user_id,
          username: log_item[1],
          device_uuid: params[:uuid]&.downcase,
          ip: client_ip,
          created_at: created_at
        )
      end
    end

  end

  # 提交动态服务器连接失败信息
  def dynamic_server_failed
    requires! :app_channel, type: String
    requires! :app_version, type: String
    requires! :app_version_number, type: String
    requires! :device_model, type: String
    requires! :platform, type: String
    requires! :operator, type: String
    requires! :data, type: String
    # 用户id,url,时间|用户id,url,时间
    param_logs = params[:data].split('|')
    urls = []
    param_logs.each do |plog|
      next if plog.blank?
      log_item = plog.split(',')
      urls << log_item[1]
    end
    servers = DynamicServer.where(url: urls)
    server_urls = servers.index_by(&:url)
    server_ids = servers.map(&:id)

    new_servers = []
    param_logs.each do |plog|
      next if plog.blank?
      log_item = plog.split(',')
      ts = log_item[2].to_i
      ip = client_ip
      ip_region = Ipnet.find_by_ip(ip)
      created_at = ts > 0 ? Utils.ts2t(ts) : Time.now
      server = server_urls[log_item[1]]
      if server.present?
        DynamicServer.increase_failed_count(server.id)
        new_servers << DynamicServerConnectionFailedLog.new(
          id: Utils::Gen.generate_uuid,
          dynamic_server_id: server.id,
          user_id: log_item[0],
          app_channel: params[:app_channel],
          app_version: params[:app_version],
          app_version_number: params[:app_version_number],
          device_model: params[:device_model],
          platform: params[:platform],
          operator: params[:operator],
          ip: client_ip,
          ip_country: ip_region[:country],
          ip_province: ip_region[:province],
          ip_city: ip_region[:city],
          created_at: created_at,
          updated_at: created_at
        )
      end
    end
    DynamicServerConnectionFailedLog.bulk_insert(new_servers, use_provided_primary_key: true, disable_timestamps: true) if new_servers.present?
  end

  # 提交未操作信息
  def no_operation
    requires! :uuid, type: String
    requires! :app_channel, type: String
    requires! :app_version, type: String
    requires! :app_version_number, type: String
    requires! :device_model, type: String
    requires! :operator, type: String
    requires! :data, type: String
    # 用户名,时间,ip解析|用户名,时间,ip解析

    new_logs = []
    usernames = []
    logs = params[:data].split('|')
    logs.each do |log|
      next if log.blank?
      log_items = log.split(',')
      usernames << log_items[0]
    end
    users = User.select(:id, :username).by_username(usernames).index_by(&:username)
    logs.each do |log|
      next if log.blank?
      log_items = log.split(',')
      ip = client_ip
      ip_region = Ipnet.find_by_ip(ip)
      username = log_items[0]
      user_id = users[username]&.id
      ts = log_items[1].to_i
      created_at = ts > 0 ? Utils.ts2t(ts) : Time.now
      if user_id.present?
        new_logs << UserNoOperationLog.new(
          id: Utils::Gen.generate_uuid,
          user_id: user_id,
          username: username,
          device_uuid: params[:uuid]&.downcase,
          device_model: params[:device_model],
          ip: ip,
          ip_country: ip_region[:country],
          ip_province: ip_region[:province],
          ip_city: ip_region[:city],
          app_channel: params[:app_channel],
          app_version: params[:app_version],
          app_version_number: params[:app_version_number],
          operator: params[:operator],
          created_at: created_at,
          updated_at: created_at
        )
      end
    end
    UserNoOperationLog.bulk_insert(new_logs, use_provided_primary_key: true, disable_timestamps: true) if new_logs.present?
  end

  # 代理服务器连接失败错误
  def proxy_connection_failed
    requires! :uuid, type: String
    requires! :app_channel, type: String
    requires! :app_version, type: String
    requires! :app_version_number, type: String
    requires! :operator, type: String
    requires! :data, type: String
    # user_id,node_id,node_ip,target_url,created_at

    new_logs = []
    logs = params[:data].split('|')
    logs.each do |log|
      next if log.blank?
      log_params = log.split(',')
      ip = client_ip
      ip_region = Ipnet.find_by_ip(ip)
      user_id = log_params[0]
      node_id = log_params[1]
      node_ip = log_params[2]
      target_url = log_params[3]
      ts = log_params[4].to_i
      created_at = ts > 0 ? Utils.ts2t(ts) : Time.now
      new_logs << ProxyConnectionFailedLog.new(
        user_id: user_id,
        uuid: params[:uuid]&.downcase,
        node_id: node_id,
        node_ip: node_ip,
        target_url: target_url,
        client_ip: ip,
        operator: params[:operator],
        client_ip_country: ip_region[:country],
        client_ip_province: ip_region[:province],
        client_ip_city: ip_region[:city],
        created_at: created_at,
        updated_at: Time.now
      )
    end
    if new_logs.present?
      ProxyConnectionFailedLog.bulk_insert(new_logs, disable_timestamps: true)
      # 触发预警检查
      SmsNotify.check_proxy_failed
    end
  end

end

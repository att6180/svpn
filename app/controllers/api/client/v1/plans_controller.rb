class Api::Client::V1::PlansController < Api::Client::V1::ApiController

  skip_before_action :authenticate_user!, only: [:comsunny_callback]

  def index
    optional! :platform, type: String
    optional! :app_version, type: String
    optional! :app_version_number, type: String

    @plans = Plan.by_version(
      params[:platform],
      params[:app_version],
      params[:app_version_number]
    )
  end

  def iap
    optional! :platform, type: String
    optional! :app_version, type: String
    optional! :app_version_number, type: String

    @plans = Plan.by_version_iap(
      params[:platform],
      params[:app_version],
      params[:app_version_number]
    )
  end

  # IAP二次验证
  def verify_iap
    optional! :app_channel, type: String
    optional! :app_version, type: String
    requires! :app_version_number, type: String
    requires! :receipt_data, type: String

    result = Monza::Receipt.verify(params[:receipt_data])
    if result.blank? || result.status != 0
      error!(api_t("receipt_data_verification_error")) and return
    end

    # 根据下单时间查找最新的记录
    in_app_item = result.receipt.in_app.sort_by{|r| r.purchase_date_ms}.last
    product_id = in_app_item.product_id
    if product_id.blank?
      error!(api_t("invalid_product_info")) and return
    end

    # 通过transaction_id判断此凭证是否验证过
    transaction_id = in_app_item.transaction_id
    iap_log = UserIapVerificationLog.find_by(transaction_id: transaction_id)
    if iap_log.present?
      error!(api_t("used_receipt")) and return
    end

    # 找到用户最后一次登录的日志，取出渠道和版本信息
    app_channel, app_version = current_user.last_signin_versions
    if app_channel.blank? || app_version.blank?
      app_channel = current_user.create_app_channel
      app_version = current_user.create_app_version
    end
    # 获取套餐数据, 判断客户端版本号是否在审核期间, 根据是否审核来根据iap_id来查找
    is_audit = SystemSetting.is_auditing?('ios', app_version, params[:app_version_number])
    plan = Plan.where(iap_id: product_id, is_audit: is_audit).take

    if plan.present? && plan.is_enabled? && plan.is_iap?
      transaction_log = plan.payment(
        current_user,
        :iap,
        transaction_id,
        app_channel,
        app_version
      )
      if transaction_log.present?
        # 保存iap日志
        created_iap_log = current_user.user_iap_verification_logs.create(
          iap_id: product_id,
          transaction_id: transaction_id,
          receipt_data: params[:receipt_data],
          status: result.status,
          result: result.original_json_response.to_json,
          ip: client_ip
        )
      end
    else
      # 为审核通过，这里只要购买成功，就认为是ID为14的6钻套餐
      #plan = Plan.find(15)
      #transaction_log = plan.payment(
        #current_user,
        #:iap,
        #created_iap_log.transaction_id,
        #app_channel,
        #params[:app_version]
      #)
      error!(api_t("invalid_plan_info"))
    end
  end

  # 二次验证google pay
  def verify_google_pay
    optional! :app_channel, type: String
    requires! :app_version, type: String
    requires! :app_version_number, type: String
    requires! :receipt_data, type: String
    requires! :signature, type: String

    # 通过公钥验证
    if !RsaCrypt.verify(params[:receipt_data], params[:signature])
      error!(api_t("receipt_data_verification_error")) and return
    end

    begin
      order = JSON.parse(params[:receipt_data], symbolize_names: true)
    rescue
      error!(api_t("invalid_product_info")) and return
    end

    # 通过transaction_id判断此凭证是否验证过
    transaction_id = order[:orderId]
    iap_log = UserIapVerificationLog.find_by(transaction_id: transaction_id)
    if iap_log.present?
      error!(api_t("used_receipt")) and return
    end

    # 找到用户最后一次登录的日志，取出渠道和版本信息
    app_channel, app_version = current_user.last_signin_versions
    if app_channel.blank? || app_version.blank?
      app_channel = current_user.create_app_channel
      app_version = current_user.create_app_version
    end

    product_id = order[:productId]
    plan = ::V2::Plan.where(iap_id: product_id).take

    if plan.present? && plan.is_enabled? && plan.is_iap?
      transaction_log = plan.payment(
        current_user,
        :google_pay,
        transaction_id,
        app_channel,
        app_version
      )
      if transaction_log.present?
        # 保存iap日志
        created_iap_log = current_user.user_iap_verification_logs.create(
          iap_id: product_id,
          transaction_id: transaction_id,
          receipt_data: params[:receipt_data],
          status: order[:purchaseState],
          result: order.to_json,
          ip: client_ip
        )
      end
    else
      error!(api_t("invalid_plan_info"))
    end
  end

  # comsunny平台创建订单
  def comsunny_order
    requires! :plan_id, type: Integer

    # 验证套餐
    plan = Plan.find_by(id: params[:plan_id])
    if !plan.present?
      error!(api_t("invalid_plan_info")) and return
    end

    @log = TransactionLog.create(
      user_id: current_user.id,
      plan_id: plan.id,
      plan_name: plan.name,
      coins: plan.coins + plan.present_coins,
      plan_coins: plan.coins,
      present_coins: plan.present_coins,
      price: plan.price,
      is_regular_time: plan.is_regular_time,
      time_type: plan.time_type,
      node_type_id: plan.node_type_id,
      app_channel: current_user.create_app_channel,
      app_version: current_user.create_app_version,
      user_created_at: current_user.created_at
    )

    # 向支付平台创建订单并请求支付URL
    @payment_url = Comsunny.create_order({
      order_id: @log.order_number,
      fee: plan.price.to_i * 100,
      created_at: @log.created_at.to_i,
      plan_name: '消费',
      ip: client_ip
    }, Setting.comsunny_order_url)

    # 如果返回为空，则记录日志
    if @payment_url.blank?
      # 记录日志
      CreateTransactionPaymentFailedLogJob.perform_later(
        @log.order_number,
        current_user.id,
        "", "", "",
        DateUtils.time2str(Time.now)
      )
    end
  end

  def comsunny_order_qrcode
    requires! :platform, type: String
    requires! :app_channel, type: String
    requires! :app_version, type: String
    requires! :plan_id, type: Integer
    optional! :type, type: String

    # 验证套餐
    plan = Plan.find_by(id: params[:plan_id])
    if !plan.present?
      error!(api_t("invalid_plan_info")) and return
    end

    @log = TransactionLog.create(
      user_id: current_user.id,
      plan_id: plan.id,
      plan_name: plan.name,
      coins: plan.coins + plan.present_coins,
      plan_coins: plan.coins,
      present_coins: plan.present_coins,
      price: plan.price,
      app_channel: params[:app_channel],
      app_version: params[:app_version],
      is_regular_time: plan.is_regular_time,
      time_type: plan.time_type,
      node_type_id: plan.node_type_id,
      user_created_at: current_user.created_at
    )

    # 向支付平台创建订单并请求支付URL
    options = {
      app_id: CONFIG.comsunny_app_id,
      secret: CONFIG.comsunny_key,
      order_id: @log.order_number,
      fee: plan.price.to_i * 100,
      created_at: @log.created_at.to_i,
      plan_name: '消费',
      user_id: current_user.id
    }
    options[:type] = Comsunny::PAYMENT_TYPES[params[:type].to_sym] if params[:type].present?
    @payment_url = Comsunny.create_order(options, CONFIG.comsunny_order_qrcode_url)
  end

  # comsunny接收支付平台回调
  def comsunny_callback
    comsunny = Comsunny.new(CONFIG.comsunny_app_id, CONFIG.comsunny_master_secret, params)
    # 验证签名
    if !comsunny.signature_correct?
      error!(api_t("signature_verification_failed")) and return
    end

    # 验证transaction_id是否重复
    log = ComsunnyTransactionLog.find_by(transaction_id: comsunny.transaction_id)
    if log.present?
      render plain: 'success', layout: false and return
    end

    # 验证后端订单
    transaction_log = TransactionLog.find_by(order_number: comsunny.transaction_id)
    if !transaction_log.present?
      error!(api_t("transaction_log_does_not_exist")) and return
    end

    # 验证订单是否已经支付
    if transaction_log.paid?
      render plain: 'success', layout: false and return
    end

    # 获取套餐信息并验证金额是否正确
    # 2018-03-21: 修改新版充值系统，由于套餐表要重建，所以订单和套餐表不需要关联了
    # 这里就不能使用订单里关联的套餐记录来做验证
    #plan = transaction_log.plan
    #if !comsunny.fee_correct?(transaction_log.price)
      #error!(api_t("invalid_plan_info")) and return
    #end

    # 查找用户
    user = transaction_log.user
    if !user.present?
      error!(api_t("user_does_not_exist")) and return
    end

    # 为用户设置套餐
    transaction_log.payment_for_comsunny!(user, comsunny)
    render plain: 'success', layout: false
  end

  # 订单号状态查询
  def transaction_status
    requires! :order_number, type: String

    log = TransactionLog.find_by(order_number: params[:order_number])
    if !log.present?
      error!(api_t("transaction_log_does_not_exist")) and return
    end
    @is_completed = log.status == 'paid'
  end

  # 订单记录
  def transaction_logs
    optional! :page, type: Integer
    optional! :limit, type: Integer

    limit = params[:limit] || 10
    @logs = current_user.transaction_logs
      .recent
      .page(param_page).per(limit)
  end

  # 消费记录
  def consumption_logs
    optional! :page, type: Integer
    optional! :limit, type: Integer

    limit = params[:limit] || 10
    @logs = current_user.consumption_logs
      .by_coins
      .recent
      .page(param_page).per(limit)
      .includes(:node_type)
  end

  # 获取已启用的支付方式列表
  def payment_methods
    methods = Setting.payment_enabled_methods
    _, _, version_number, platform = current_user.last_signin_versions
    version_number = current_user.create_app_version_number if version_number.blank?
    platform = current_user.create_platform if platform.blank?
    version_number = version_number.gsub(".", "").to_i
    is_domestic = Utils::IP.is_domestic?(client_ip)

    # 如果客户端为pc或mac，并且版本号小于等于1.0.5，则只开启支付宝扫码(alipay_qrcode)
    if version_number <= 105 && ["pc", "mac"].include?(platform)
      methods = ["alipay_qrcode"]
    end
    # 如果客户端为pc或mac,并且版本号大于1.0.5，则只屏蔽支付宝扫码(alipay_qrcode)
    if version_number > 105 && ["pc", "mac"].include?(platform)
      methods = methods - ["alipay_h5"] if is_domestic
      methods = methods - ["alipay_qrcode"] if !is_domestic
    end
    @methods = []
    methods.each do |method|
      @methods << [ method, I18n.t("transaction_log.client_payment_methods.#{method}") ]
    end
    logger.info @methods
  end

end

class Api::Client::V1::NodesController < Api::Client::V1::ApiController

  def index
    node_type_cache_key = Digest::MD5.hexdigest(NodeType.enabled.map(&:updated_at).join)
    @user_group_indexs = UserGroup.cache_list.index_by(&:id)
    @user_group = @user_group_indexs[current_user.user_group_id]
    flat_node_types = NodeType.cache_list
    @node_type_indexs = flat_node_types.index_by(&:id)
    @node_types = Rails.cache.fetch("node_type_index_v1_#{node_type_cache_key}", expires_in: 7.days) do
      NodeType.enabled.sorted_by_level.includes(:user_group, node_regions: :nodes).to_a
    end
    @user_node_types = current_user.user_node_types.in_node_types(@node_types.map{|n| n.id}).sorted_by_created_at.includes(:node_type)
  end

  # 连接到指定服务器线路
  def connect
    requires! :node_type_id, type: Integer
    optional! :allow_consume, type: Integer

    #if RedisCache.is_concurrent?("client_node_connect_lock_#{current_user.id}")
      #error!(api_t('too_frequent_access'), 100) and return
    #end

    # 判断当前用户是否超过连接上限，超过就返回虚拟ip
    fake_node_url = Guard.pick_fake_ip(current_user) if Guard.is_node_connnect_exceeded?(current_user.id)

    has_node = false
    now_client_ip = client_ip
    ip_region = Ipnet.find_by_ip(now_client_ip)
    is_domestic = Utils::IP.is_domestic_by_region?(ip_region)
    ip_area = ip_region[:area]
    node_type = NodeType.find_by(id: params[:node_type_id])
    # 检查服务器类型
    if node_type.blank? || !node_type&.is_enabled?
      error!(api_t("node_type_does_not_exist"), 1) and return
    end

    # 检查当前用户等级是否满足服务器类型要求
    if current_user.user_group.level < node_type.user_group.level
      error!(api_t("user_group_level_is_not_enough"), 2) and return
    end

    # 检查用户服务器类型是否在有效期内
    @user_node_type_is_expired = current_user.node_type_is_expired?(node_type.id)
    user_node_type = current_user.user_node_types.find_or_create_by(node_type_id: node_type.id)
    # 如果用户服务器类型过期了
    if @user_node_type_is_expired
      # 如果客户端确认要消费钻石
      if params[:allow_consume] == '1'
        # 扣除钻石
        if current_user.current_coins < node_type.expense_coins
          error!(api_t("coins_is_not_enough"), 3) and return
        end
        user_node_type = current_user.consume_coins(node_type)
        @is_consumed = true
        @node = Node.select_smart_node(node_type, is_domestic, ip_region)
        @user_node_type = user_node_type
        _, @token, _ = UserSession.read_by_user_id(current_user.id)
        if @node.blank?
          error!(api_t("no_node_available"), 4) and return
        else
          has_node = true
        end
      else
        # 如果客户端没确认要消费
        @is_consumed = false
      end
    else
      # 如果用户服务器类型未过期
      @is_consumed = false
      @node = Node.select_smart_node(node_type, is_domestic, ip_region)
      @user_node_type = user_node_type
      _, @token, _ = UserSession.read_by_user_id(current_user.id)
      if @node.blank?
        error!(api_t("no_node_available"), 4) and return
      else
        has_node = true
      end
    end
    @node.url = fake_node_url if fake_node_url.present? && @node.present?

    if has_node && !User.is_robot?(current_user.id)
      CreateNodeConnectionServerLogJob.perform_later(
        current_user.id,
        current_user.created_at.strftime("%Y-%m-%d %H:%M:%S"),
        Time.now.strftime("%Y-%m-%d %H:%M:%S"),
        current_user.create_app_channel,
        current_user.create_app_version,
        @node.id,
        now_client_ip,
        is_domestic
      )
      # 态势图
      NodeService.connected_perform_later(current_user.id, current_user.username, now_client_ip, ip_region)
    end

    # 记录用户活跃日志
    CreateUserActiveLogJob.perform_later(
      user_id: current_user.id,
      created_at: DateUtils.time2str(Time.now),
      app_channel: current_user.create_app_channel,
      app_version: current_user.create_app_version,
      active_type: UserActiveHourLog.active_types[:connect]
    )

  end

  # 连接成功或失败后写入日志
  def connection_log
    requires! :node_id, type: Integer
    requires! :wait_time, type: Integer
    requires! :success, type: Integer
    optional! :domain, type: Integer

    log_params = {
      node_id: params[:node_id],
      wait_time: params[:wait_time],
      success: params[:success],
      domain: params[:domain],
      user_id: current_user.id,
      user_registered_at: DateUtils.time2str(current_user.created_at),
      app_channel: current_user.create_app_channel,
      app_version: current_user.create_app_version,
      client_ip: client_ip
    }

    CreateUserConnectionLogJob.perform_later(log_params)
  end

  # 检查指定服务器是否可正常连接
  def check
    requires! :node_id, type: Integer

    # 检查节点状态
    node = Node.find_by(id: params[:node_id])
    if !node.present?
      error!(api_t("node_does_not_exist"), UserSession::ERR_NODE_NOT_EXIST) and return
    end

    if node.is_overload?
      error!(api_t("node_is_overload"), UserSession::ERR_NODE_OVERLOAD) and return
    end

    node_type = node.node_type
    if current_user.user_group.level < node_type.user_group.level
      error!(api_t("user_group_level_is_not_enough"), UserSession::ERR_USER_LEVEL_NOT_ENOUGH) and return
    end

    # 根据服务器类型ID获得用户对应的服务器类型
    user_node_type = current_user.user_node_type_by_type_id(node_type.id)
    # 如果要连接的服务器类型需要钻石 并且 用户当前的服务器类型是按次
    if node_type.expense_coins > 0 && user_node_type.status_times?
      # 如果用户服务器类型已到期
      if user_node_type.expired_at <= Time.now
        # 判断自动续费开关是否开启
        if Setting.auto_renew.to_i == 0
          error!(api_t("user_node_type_expired_auto_renew_is_disabled"), UserSession::ERR_SERVICE_EXPIRED_AUTO_RENEW_DISABLED) and return
        end
        error!(api_t("user_node_type_expired"), UserSession::ERR_SERVICE_EXPIRED) and return
      end
    end

  end

  # 获取二级代理线路列表，返回json，格式如下
  # 服务类型
  #   洲际
  #     国家
  #       区域
  def customize_lines
    @lines = Node.cache_customize_lines
    error!(api_t("no_line_available"), 4) and return if @lines.blank?
  end

  # 连接到定制线路服务器
  def customize_connect
    requires! :node_type_id, type: Integer
    requires! :node_country_id, type: Integer
    optional! :allow_consume, type: Integer
    optional! :node_region_id, type: Integer

    # 因为客户端连接逻辑问题暂时屏蔽
    #if RedisCache.is_concurrent?("client_node_connect_lock_#{current_user.id}")
      #error!(api_t('too_frequent_access'), 100) and return
    #end
    
    # 判断当前用户是否超过连接上限，超过就返回虚拟ip
    fake_node_url = Guard.pick_fake_ip(current_user) if Guard.is_node_connnect_exceeded?(current_user.id)

    has_node = false
    now_client_ip = client_ip
    ip_region = Ipnet.find_by_ip(now_client_ip)
    is_domestic = Utils::IP.is_domestic_by_region?(ip_region)
    node_country_id = params[:node_country_id]
    node_region_id = params[:node_region_id]

    @node_type_id = params[:node_type_id]
    node_type = NodeType.find_by(id: params[:node_type_id])
    # 检查服务器类型
    if node_type.blank? || !node_type&.is_enabled?
      error!(api_t("node_type_does_not_exist"), 1) and return
    end

    # 检查当前用户等级是否满足服务器类型要求
    if current_user.user_group.level < node_type.user_group.level
      error!(api_t("user_group_level_is_not_enough"), 2) and return
    end

    # 检查服务器国家
    node_country = NodeCountry.find_by(id: params[:node_country_id])
    if node_country.blank?
      error!(api_t("node_country_does_not_exist"), 5) and return
    end

    if params[:node_region_id].present?
      # 检查服务器地域
      node_region = NodeRegion.find_by(id: params[:node_region_id])
      if node_region.blank? || !node_region.is_enabled?
        error!(api_t("node_region_does_not_exist"), 6) and return
      end

      # 检查服务器地域是否属于该国家
      if node_region.node_country_id != node_country.id
        error!(api_t("node_region_does_not_belong_node_country"), 7) and return
      end
    end

    # 检查用户服务器类型是否在有效期内
    @user_node_type_is_expired = current_user.node_type_is_expired?(node_type.id)
    user_node_type = current_user.user_node_types.find_or_create_by(node_type_id: node_type.id)
    # 如果用户服务器类型过期了
    if @user_node_type_is_expired
      # 如果客户端确认要消费钻石
      if params[:allow_consume] == '1'
        # 扣除钻石
        if current_user.current_coins < node_type.expense_coins
          error!(api_t("coins_is_not_enough"), 3) and return
        end
        user_node_type = current_user.consume_coins(node_type)
        @is_consumed = true
        if current_user.user_tap.present?
          @node = Node.find(current_user.user_tap.node_id)
          @node.increase_online_users!
        else
          @node = Node.select_customize_node(node_type, node_country_id, node_region_id)
        end
        @user_node_type = user_node_type
        _, @token = UserSession.read_by_user_id(current_user.id)
        if @node.blank?
          has_error = true
          error!(api_t("no_node_available"), 4) and return
        else
          has_node = true
        end
      else
        # 如果客户端没确认要消费
        @is_consumed = false
      end
    else
      # 如果用户服务器类型未过期
      @is_consumed = false
      if current_user.user_tap.present?
          @node = Node.find(current_user.user_tap.node_id)
          @node.increase_online_users!
      else
        @node = Node.select_customize_node(node_type, node_country_id, node_region_id)
      end
      @user_node_type = user_node_type
      _, @token = UserSession.read_by_user_id(current_user.id)
      if @node.blank?
        error!(api_t("no_node_available"), 4) and return
      else
        has_node = true
      end
    end

    @node.url = fake_node_url if fake_node_url.present? && @node.present?
    # 获得客户端标签列表，按id分组
    @tabs = ::V2::PlanClientTab.cache_list.index_by(&:node_type_id)

    # 创建日志
    if has_node && !User.is_robot?(current_user.id)
      CreateNodeConnectionServerLogJob.perform_later(
        current_user.id,
        current_user.created_at.strftime("%Y-%m-%d %H:%M:%S"),
        Time.now.strftime("%Y-%m-%d %H:%M:%S"),
        current_user.create_app_channel,
        current_user.create_app_version,
        @node.id,
        "#{now_client_ip}/#{@token}/#{fake_node_url}",
        is_domestic
      )
      # 态势图
      NodeService.connected_perform_later(current_user.id, current_user.username, now_client_ip, ip_region)
    end

    # 记录用户活跃日志
    CreateUserActiveLogJob.perform_later(
      user_id: current_user.id,
      created_at: DateUtils.time2str(Time.now),
      app_channel: current_user.create_app_channel,
      app_version: current_user.create_app_version,
      active_type: UserActiveHourLog.active_types[:connect]
    )

  end

  # 获得客户端测速全局配置
  # 废弃，改用全局参数获取接口 client/v1/globals
  def test_speed_global_config
    @user_hit_test_speed = Setting.test_speed_user_ids
      .split('|')
      .map(&:to_i)
      .include?(current_user.id)
  end

  # 获得测速节点ip列表
  # 类型 http请求:1/下载:2
  # 数据格式 1=https://www.google.com|2=www.google.com/1.txt
  def test_speed_ip_list
    ip_list = Setting.test_speed_ip_list.split('|')
    @test_speed_ip_list = []
    ip_list.each do |ip|
      type, url = ip.split('=')
      @test_speed_ip_list << {type: type, url: url}
    end
  end

  # 提交节点http请求测速日志
  def node_test_request_speed_logs
    requires! :app_channel, type: String
    requires! :app_version, type: String
    requires! :data, type: String

    begin
      data = JSON.parse(params[:data])
    rescue
      error!(api_t("invalid_json_data"), 1) and return
    end

    CreateNodeTestHttpRequestSpeedLogJob.perform_later(
      data,
      params[:app_channel],
      params[:app_version],
      current_user,
      client_ip,
      Time.now.strftime("%Y-%m-%d %H:%M:%S")
    )
  end

  # 提交节点下载测速日志
  def node_test_download_speed_logs
    requires! :app_channel, type: String
    requires! :app_version, type: String
    requires! :data, type: String

    begin
      data = JSON.parse(params[:data])
    rescue
      error!(api_t("invalid_json_data"), 1) and return
    end

    CreateNodeTestDownloadSpeedLogJob.perform_later(
      data,
      params[:app_channel],
      params[:app_version],
      current_user,
      client_ip,
      Time.now.strftime("%Y-%m-%d %H:%M:%S")
    )
  end

end

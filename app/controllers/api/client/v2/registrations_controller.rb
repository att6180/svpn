class Api::Client::V2::RegistrationsController < Api::Client::V1::ApiController

  skip_before_action :authenticate_user!
  before_action :verify_telephone, only: [:create]

  # 发送验证码
  def send_verification_code
    requires! :area_code, type: Integer
    requires! :telephone, type: String
    requires! :type, type: Integer

    telephone = params[:telephone].strip
    area_code = params[:area_code]
    action_type = params[:type].to_i
    
    # 如果不是正确的手机号格式,纯数字
    if !(/^\d+$/ === telephone)
      error!(api_t("invalid_telephone_number")) and return
    end

    # 如果为修改用户名，则判断系统中是否已存在此手机号
    user = User.find_by(username: telephone)
    if action_type == 0 || action_type == 3
      if user.present?
        error!(api_t("telphone_already_exists")) and return
      end
    end
    # 用户不存在时，不能找回密码
    if action_type == 2
      if user.blank?
        error!(api_t("user_does_not_exist")) and return
      end
    end

    codes_count = VerificationCode.count_in_hour(telephone)
    if codes_count > 5
      error!(api_t("verification_code_sent_too_often"), 1)
    else
      # 如果该手机号超过每天发送短信次数上限
      error!(api_t("verification_code_sent_too_often"), 1) and return if Guard.is_tel_send_exceeded?(telephone)

      code = Sms.send(telephone, area_code)
      if code
        VerificationCode.create_code(telephone, code, VerificationCode.code_types.keys[action_type])
        set_message(api_t("verification_code_has_sent"))
      else
        error!(api_t("verification_code_failed_to_send"), 2)
      end
    end
  end

  # 用户注册
  def create
    requires! :device_uuid, type: String
    optional! :device_name, type: String
    requires! :device_model, type: String
    requires! :platform, type: String
    requires! :system_version, type: String
    requires! :operator, type: String
    requires! :net_env, type: String
    requires! :app_version, type: String
    requires! :app_version_number, type: String
    requires! :app_channel, type: String
    requires! :telephone, type: String
    requires! :password, type: String
    requires! :confirmation_password, type: String
    requires! :verification_code, type: String

    telephone = params[:telephone].strip
    # 客户端ip
    @client_ip = client_ip
    # 获取客户端区域
    @ip_region = client_ip_region
    # 参数
    @request_params = params
    # 检查密码是否有效
    error!(api_t("invalid_password"), 1) and return if !Utils.valid_password?(params[:password])
    # 检查确认密码是否一样
    error!(api_t('password_is_not_match'), 2) and return if params[:password] != params[:confirmation_password]
    # 检查验证码
    vcode = VerificationCode.code_for_telephone(telephone, :register).last
    if vcode.blank? || !vcode&.is_correct?(params[:verification_code])
      error!(api_t("verification_code_is_invalid_or_expired"), 3) and return
    end
    # 检查手机号是否有重复
    user = User.find_by(username: telephone)
    if user.present?
      error!(api_t('telephone_already_exist'), 4) and return
    end
    # 创建用户
    user = UserService.new_user(params, @client_ip, @ip_region)
    if user.save
      vcode.used!
      after_signin(user)
    else
      error_detail!(user) and return
    end
  end

  private

  def after_signin(user)
    @user = user
    @user_group_indexs = UserGroup.cache_list.index_by(&:id)
    @user_group = @user_group_indexs[user.user_group_id]
    @node_types = NodeType.cache_list
    @node_type_indexs = @node_types.index_by(&:id)
    UserNodeType.check_types(@user, @user.user_node_types, @node_types)
    @user_node_types = @user.user_node_types.in_node_types(@node_types.map(&:id)).sorted_by_created_at
    @device = Device.find_or_create_by_user(@user, @request_params)
    @user_session_id = SessionService.find_or_create_session(@user)
    SessionService.signup_log_perform_later(@user.id, @user.username, @client_ip, @ip_region)
    SessionService.signin_log_perform_later(true, @user.id, @user.username, @device.id, @user_session_id, @request_params, @client_ip, @ip_region)
    @api_token = create_jwt(@user, @device.id)
    @settings = SystemSetting.by_platform_and_app_version(
      @request_params[:platform],
      @request_params[:app_channel],
      @request_params[:app_version],
      @request_params[:app_version_number]
    )
  end

  def verify_telephone
    error!('手机号不正确') if params['telephone'].blank? || params['telephone'].length != 11 || params[:telephone].first != '1'
  end

end

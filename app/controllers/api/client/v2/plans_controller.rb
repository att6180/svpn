class Api::Client::V2::PlansController < Api::Client::V1::ApiController

  # 第二版套餐获取
  def index
    requires! :platform, type: String
    requires! :app_channel, type: String
    requires! :app_version, type: String
    requires! :app_version_number, type: String

    ip = client_ip
    is_domestic = Utils::IP.is_domestic?(ip)
    # 获取渠道和版本
    plan_version = ::V2::PlanVersion.by_version(params[:app_channel], params[:app_version])
    error!(api_t("configuration_not_found"), 1) and return if plan_version.blank?
    # 获取模版ID
    # 判断指定平台、版本、版本号是否为审核版本，是则返回审核套餐模板ID
    # 否则判断是否是国内，如果是则返回国内第三方套餐模版ID
    # 否则再判断用户是否支付过，如果是就返回国外第三方支付模版ID
    # 否则返回IAP套餐模版ID
    is_auditing = SystemSetting.is_auditing?(params[:platform], params[:app_version], params[:app_version_number])
    template_id = if is_auditing
      plan_version.audit_template_id
    else
      if is_domestic
        plan_version.domestic_template_id
      else
        current_user.has_paid? ? plan_version.oversea_template_id : plan_version.iap_template_id
      end
    end
    error!(api_t("plan_template_not_found"), 2) and return if template_id.blank?
    # 根据模版ID获取指定套餐
    @tabs = ::V2::Plan.by_template_id(template_id, plan_version.tab_id_sort)
  end

  # IAP二次验证
  def verify_iap
    optional! :app_channel, type: String
    optional! :app_version, type: String
    requires! :app_version_number, type: String
    requires! :receipt_data, type: String

    result = Monza::Receipt.verify(params[:receipt_data])
    if result.blank? || result.status != 0
      error!(api_t("receipt_data_verification_error")) and return
    end
    #logger.info "*" * 90
    #logger.info result.original_json_response
    #logger.info "*" * 90

    # 根据下单时间查找最新的记录
    in_app_item = result.receipt.in_app.sort_by{|r| r.purchase_date_ms}.last
    product_id = in_app_item.product_id
    if product_id.blank?
      error!(api_t("invalid_product_info")) and return
    end

    # 通过transaction_id判断此凭证是否验证过
    transaction_id = in_app_item.transaction_id
    iap_log = UserIapVerificationLog.find_by(transaction_id: transaction_id)
    if iap_log.present?
      error!(api_t("used_receipt")) and return
    end

    # 找到用户最后一次登录的日志，取出渠道和版本信息
    app_channel, app_version = current_user.last_signin_versions
    if app_channel.blank? || app_version.blank?
      app_channel = current_user.create_app_channel
      app_version = current_user.create_app_version
    end
    plan = ::V2::Plan.where(iap_id: product_id).take

    if plan.present? && plan.is_enabled? && plan.is_iap?
      transaction_log = plan.payment(
        current_user,
        :iap,
        transaction_id,
        app_channel,
        app_version
      )
      if transaction_log.present?
        # 保存iap日志
        created_iap_log = current_user.user_iap_verification_logs.create(
          iap_id: product_id,
          transaction_id: transaction_id,
          receipt_data: params[:receipt_data],
          status: result.status,
          result: result.original_json_response.to_json,
          ip: client_ip
        )
      end
    else
      # 为审核通过，这里只要购买成功，就认为是ID为14的6钻套餐
      #plan = Plan.find(15)
      #transaction_log = plan.payment(
        #current_user,
        #:iap,
        #created_iap_log.transaction_id,
        #app_channel,
        #params[:app_version]
      #)
      error!(api_t("invalid_plan_info"))
    end
  end

  # comsunny平台创建订单
  def comsunny_order
    requires! :platform, type: String
    requires! :app_channel, type: String
    requires! :app_version, type: String
    requires! :plan_id, type: Integer

    # 验证套餐
    plan = Plan.find_by(id: params[:plan_id])
    if !plan.present?
      error!(api_t("invalid_plan_info")) and return
    end

    @log = TransactionLog.create(
      user_id: current_user.id,
      plan_id: plan.id,
      plan_name: plan.name,
      coins: plan.coins + plan.present_coins,
      plan_coins: plan.coins,
      present_coins: plan.present_coins,
      price: plan.price,
      app_channel: params[:app_channel],
      app_version: params[:app_version],
      is_regular_time: plan.is_regular_time,
      time_type: plan.time_type,
      node_type_id: plan.node_type_id,
      user_created_at: current_user.created_at
    )

    # 向支付平台创建订单并请求支付URL
    options = {
      order_id: @log.order_number,
      fee: plan.price.to_i * 100,
      created_at: @log.created_at.to_i,
      plan_name: '消费',
      ip: client_ip,
      user_id: current_user.id
    }
    @payment_url = Comsunny.create_order(options, Setting.comsunny_order_url)

    if @payment_url.blank?
      # 记录日志
      CreateTransactionPaymentFailedLogJob.perform_later(
        @log.order_number,
        current_user.id,
        params[:platform],
        params[:app_channel],
        params[:app_version],
        DateUtils.time2str(Time.now)
      )
      error!('支付订单创建失败', 1) and return
    end
  end

  def comsunny_order_qrcode
    requires! :platform, type: String
    requires! :app_channel, type: String
    requires! :app_version, type: String
    requires! :app_version_number, type: String
    requires! :plan_id, type: Integer
    optional! :type, type: String

    # 验证套餐
    plan = ::V2::Plan.find_by(id: params[:plan_id])
    if !plan.present?
      error!(api_t("invalid_plan_info")) and return
    end

    @log = TransactionLog.create(
      user_id: current_user.id,
      plan_name: plan.name,
      coins: plan.coins + plan.present_coins,
      plan_coins: plan.coins,
      present_coins: plan.present_coins,
      price: plan.price,
      app_channel: params[:app_channel],
      app_version: params[:app_version],
      is_regular_time: plan.is_regular_time,
      time_type: plan.time_type,
      node_type_id: plan.node_type_id,
      user_created_at: current_user.created_at
    )

    # 向支付平台创建订单并请求支付URL
    options = {
      app_id: CONFIG.comsunny_app_id,
      secret: CONFIG.comsunny_key,
      order_id: @log.order_number,
      fee: plan.price.to_i * 100,
      created_at: @log.created_at.to_i,
      plan_name: '消费',
      user_id: current_user.id
    }
    options[:type] = Comsunny::PAYMENT_TYPES[params[:type].to_sym] if params[:type].present?
    @payment_url = Comsunny.create_order(options, CONFIG.comsunny_order_qrcode_url)
  end

end

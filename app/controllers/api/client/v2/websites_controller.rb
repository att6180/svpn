class Api::Client::V2::WebsitesController < Api::Client::V1::ApiController

  def index
    optional! :update_at, type: Integer

    is_domestic = Utils::IP.is_domestic?(client_ip)
    # 根据当前ip所属国内外读取对应的导航全局修改时间
    @update_at = if is_domestic
      Setting.navigation_oversea_config_update_at
    else
      Setting.navigation_domestic_config_update_at
    end
    @hit_cache = params[:update_at].to_i == @update_at
    @tabs = WebsiteNavigation
      .by_is_domestic(!is_domestic)
  end

end

class Api::Client::V2::CommonsController < Api::Client::V1::ApiController
  skip_before_action :authenticate_user!, only: [:routes]

  def routes
    requires! :platform, type: String
    optional! :version, type: Integer

    case params[:platform]
    when 'ios'
      if params[:version] == '2'
        @d2o = CONFIG.route_ios_d2o2
        @o2d = CONFIG.route_ios_o2d2
      else
        @d2o = CONFIG.route_ios_d2o
        @o2d = CONFIG.route_ios_o2d
      end
      @glo_d2o = CONFIG.route_ios_glo_d2o
      @glo_o2d = CONFIG.route_ios_glo_o2d
      ios_d2o = File.mtime("public/#{@d2o}").to_i
      ios_o2d = File.mtime("public/#{@o2d}").to_i
      ios_glo_d2o = File.mtime("public/#{@glo_d2o}").to_i
      ios_glo_o2d = File.mtime("public/#{@glo_o2d}").to_i
      updated_at = [ios_d2o, ios_o2d, ios_glo_d2o, ios_glo_o2d].max
    when 'android'
      android_d2o = File.mtime("public/#{CONFIG.route_android_d2o}").to_i
      android_o2d = File.mtime("public/#{CONFIG.route_android_o2d}").to_i
      updated_at = android_d2o > android_o2d ? android_d2o : android_o2d
      @d2o = CONFIG.route_android_d2o
      @o2d = CONFIG.route_android_o2d
    when 'pc'
      pc_d2o = File.mtime("public/#{CONFIG.route_pc_d2o}").to_i
      pc_o2d = File.mtime("public/#{CONFIG.route_pc_o2d}").to_i
      updated_at = pc_d2o > pc_o2d ? pc_d2o : pc_o2d
      @d2o = CONFIG.route_pc_d2o
      @o2d = CONFIG.route_pc_o2d
    when 'mac'
      mac_d2o = File.mtime("public/#{CONFIG.route_mac_d2o}").to_i
      mac_o2d = File.mtime("public/#{CONFIG.route_mac_o2d}").to_i
      updated_at = mac_d2o > mac_o2d ? mac_d2o : mac_o2d
      @d2o = CONFIG.route_mac_d2o
      @o2d = CONFIG.route_mac_o2d
    end
    @updated_at = Utils.convert2minute(updated_at)
    @china_ip_updated_at = File.mtime("public/#{CONFIG.route_china_ip_list}").to_i
    @china_ip_list = CONFIG.route_china_ip_list
    @foreign_ip_updated_at = File.mtime("public/#{CONFIG.route_foreign_ip_list2}").to_i
    @foreign_ip_list = CONFIG.route_foreign_ip_list2
    @is_domestic = Utils::IP.is_domestic?(client_ip)
  end
end

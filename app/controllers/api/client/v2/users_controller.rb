class Api::Client::V2::UsersController < Api::Client::V1::ApiController

  # 修改用户名为手机号
  def update_username
    requires! :area_code, type: Integer
    requires! :telephone, type: String
    requires! :verification_code, type: String

    telephone = params[:telephone].strip
    area_code = params[:area_code]
    # 判断系统中是否已存在此手机号
    user = User.find_by(username: telephone)
    if user.present?
      error!(api_t("telphone_already_exists")) and return
    end

    # 验证手机号和验证码
    vcode = VerificationCode.code_for_telephone(telephone, :modify_username).last
    if vcode.present? && vcode&.is_correct?(params[:verification_code])
      vcode.used!
      before_username = current_user.username
      before_coins = current_user.current_coins
      before_username_changed = current_user.username_changed?
      change_params = { area_code: area_code, username: telephone }
      # 如果用户没有从分配账号修改成手机号过
      if !before_username_changed
        current_coins = current_user.current_coins + Setting.new_user_or_change_username_to_telephone_coins.to_i
        change_params.merge!(current_coins: current_coins) 
      end
      current_user.update_columns(change_params)
      # 修改设备标记
      device = Device.find_by(id: current_device_id)
      if device.present?
        device.update(username_changed: true, last_changed_user_id: current_user.id)
        device_uuid = device.uuid
        device_platform = device.platform
      end
      # 创建修改用户名为手机号日志
      CreateUserChangeLogJob.perform_later({
        user_id: current_user.id,
        change_type: Log::UserChangeLog::CHANGE_USERNAME,
        uuid: device_uuid,
        platform: device_platform,
        ip: client_ip,
        before: before_username,
        after: current_user.username,
        created_at: DateUtils.time2str(Time.now)
      })
      # 创建修改用户名为手机号赠送坚果日志
      CreateUserChangeLogJob.perform_later({
        user_id: current_user.id,
        change_type: Log::UserChangeLog::CHANGE_USERNAME_COINS,
        uuid: device_uuid,
        platform: device_platform,
        ip: client_ip,
        before: before_coins,
        after: current_user.current_coins,
        created_at: DateUtils.time2str(Time.now)
      }) if !before_username_changed
      @token = create_jwt(current_user, current_device_id)
      set_message(api_t("username_has_changed"))
    else
      error!(api_t("verification_code_is_invalid_or_expired"))
    end
  end

  # 提交分享日志
  def share
    requires! :share_type, type: Integer
    requires! :platform_name, type: String
    requires! :app_channel, type: String
    requires! :app_version, type: String
    requires! :app_version_number, type: String

    CreateUserShareLogJob.perform_later(
      user_id: current_user.id,
      share_type: UserShareLog.share_types.key(params[:share_type].to_i),
      platform_name: params[:platform_name],
      app_channel: params[:app_channel],
      app_version: params[:app_version],
      app_version_number: params[:app_version_number]
    )
  end

end

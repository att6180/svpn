class Api::Client::V2::NodesController < Api::Client::V1::ApiController

  def index
    @node_types = NodeType.cache_list
    @node_type_indexs = @node_types.index_by(&:id)
    @user_group_indexs = UserGroup.cache_list.index_by(&:id)
    @user_node_types = current_user.user_node_types.in_node_types(@node_types.map{|n| n.id})
      .sorted_by_created_at
  end

  # 连接到指定服务器线路
  def connect
    requires! :node_type_id, type: Integer
    optional! :allow_consume, type: Integer

    # 因为客户端连接逻辑问题暂时屏蔽
    #if RedisCache.is_concurrent?("client_node_connect_lock_#{current_user.id}")
      #error!(api_t('too_frequent_access'), 100) and return
    #end

    # 判断当前用户是否超过连接上限，超过就返回虚拟ip
    fake_node_url = Guard.pick_fake_ip(current_user) if Guard.is_node_connnect_exceeded?(current_user.id)
    
    has_node = false
    now_client_ip = client_ip
    ip_region = Ipnet.find_by_ip(now_client_ip)
    is_domestic = Utils::IP.is_domestic_by_region?(ip_region)
    ip_area = ip_region[:area]

    @node_type_id = params[:node_type_id]
    node_type = NodeType.find_by(id: params[:node_type_id])
    # 检查服务器类型
    if node_type.blank? || !node_type&.is_enabled?
      error!(api_t("node_type_does_not_exist"), 1) and return
    end

    # 检查当前用户等级是否满足服务器类型要求
    if current_user.user_group.level < node_type.user_group.level
      error!(api_t("user_group_level_is_not_enough"), 2) and return
    end

    # 检查用户服务器类型是否在有效期内
    @user_node_type_is_expired = current_user.node_type_is_expired?(node_type.id)
    user_node_type = current_user.user_node_types.find_or_create_by(node_type_id: node_type.id)
    # 如果用户服务器类型过期了
    if @user_node_type_is_expired
      # 如果客户端确认要消费钻石
      if params[:allow_consume] == '1'
        # 扣除钻石
        if current_user.current_coins < node_type.expense_coins
          error!(api_t("coins_is_not_enough"), 3) and return
        end
        user_node_type = current_user.consume_coins(node_type)
        @is_consumed = true
        if current_user.user_tap.present?
          @node = Node.find(current_user.user_tap.node_id)
          @node.increase_online_users!
        else
          @node = Node.select_smart_node(node_type, is_domestic, ip_region)
        end
        @user_node_type = user_node_type
        _, @token = UserSession.read_by_user_id(current_user.id)
        if @node.blank?
          has_error = true
          error!(api_t("no_node_available"), 4) and return
        else
          has_node = true
        end
      else
        # 如果客户端没确认要消费
        @is_consumed = false
      end
    else
      # 如果用户服务器类型未过期
      @is_consumed = false
      if current_user.user_tap.present?
          @node = Node.find(current_user.user_tap.node_id)
          @node.increase_online_users!
      else
        @node = Node.select_smart_node(node_type, is_domestic, ip_region)
      end
      @user_node_type = user_node_type
      _, @token = UserSession.read_by_user_id(current_user.id)
      if @node.blank?
        error!(api_t("no_node_available"), 4) and return
      else
        has_node = true
      end
    end
    @node.url = fake_node_url if fake_node_url.present? && @node.present?
    # 获得客户端标签列表，按id分组
    @tabs = ::V2::PlanClientTab.cache_list.index_by(&:node_type_id)

    # 创建日志
    if has_node && !User.is_robot?(current_user.id)
      CreateNodeConnectionServerLogJob.perform_later(
        current_user.id,
        current_user.created_at.strftime("%Y-%m-%d %H:%M:%S"),
        Time.now.strftime("%Y-%m-%d %H:%M:%S"),
        current_user.create_app_channel,
        current_user.create_app_version,
        @node.id,
        "#{now_client_ip}/#{@token}/#{fake_node_url}",
        is_domestic
      )
      # 态势图
      NodeService.connected_perform_later(current_user.id, current_user.username, now_client_ip, ip_region)
    end

    # 记录用户活跃日志
    CreateUserActiveLogJob.perform_later(
      user_id: current_user.id,
      created_at: DateUtils.time2str(Time.now),
      app_channel: current_user.create_app_channel,
      app_version: current_user.create_app_version,
      active_type: UserActiveHourLog.active_types[:connect]
    )

  end

end

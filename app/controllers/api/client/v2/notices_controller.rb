class Api::Client::V2::NoticesController < Api::Client::V1::ApiController

  skip_before_action :authenticate_user!

  def index
    optional! :username, type: String
    requires! :platform, type: String
    requires! :app_channel, type: String
    requires! :app_version, type: String

    @notice = ::V2::Notice.by_specific_user_and_app_version(
      params[:username],
      params[:platform],
      params[:app_channel],
      params[:app_version]
    )
  end

end

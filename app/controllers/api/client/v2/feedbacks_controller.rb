class Api::Client::V2::FeedbacksController < Api::Client::V1::ApiController

  def index
    optional! :page, type: Integer
    optional! :limit, type: Integer

    @feedbacks = current_user.feedbacks
      .select(:id, :feedback_type, :content, :status, :has_read2, :created_at)
      .new_sorted
      .page(param_page).per(param_limit)
  end

  def mark_as_read
    requires! :id, type: Integer

    feedback = current_user.feedbacks.find_by(id: params[:id])
    if feedback.present?
      feedback.update_columns(has_read: true, has_read_at: Time.now)
    else
      error!(I18n.t("api.feedback_is_not_existence"))
    end
  end
end

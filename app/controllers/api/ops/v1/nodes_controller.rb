class Api::Ops::V1::NodesController < Api::Ops::V1::ApiController

  before_action :set_node, only: [:update, :node_type]

  def index
    @id = Node.last.id
  end

  # 新增服务器
  def create
    requires! :id, type: Integer
    requires! :node_type_id, type: Integer
    requires! :node_region_id, type: Integer
    optional! :node_isp_id, type: Integer
    optional! :node_region_out_id, type: Integer
    requires! :name, type: String
    requires! :url, type: String
    requires! :port, type: Integer
    requires! :password, type: String
    requires! :encrypt_method, type: String
    optional! :description, type: String
    optional! :max_connections_count, type: Integer
    optional! :is_domestic, type: String
    optional! :level, type: Integer
    optional! :status, type: String
    optional! :types, type: String

    @node = Node.new(allowed_params)
    error_detail!(@node) if !@node.save
  end

  # 更新服务器
  def update
    requires! :id, type: Integer
    optional! :node_type_id, type: Integer
    optional! :node_region_id, type: Integer
    optional! :node_isp_id, type: Integer
    optional! :node_region_out_id, type: Integer
    optional! :name, type: String
    optional! :url, type: String
    optional! :port, type: Integer
    optional! :password, type: String
    optional! :encrypt_method, type: String
    optional! :description, type: String
    optional! :max_connections_count, type: Integer
    optional! :is_domestic, type: String
    optional! :level, type: Integer
    optional! :status, type: String
    optional! :types, type: String

    error_detail!(@node) if !@node.update(allowed_params.except(:id))
  end

  # 查看服务器类型
  def node_type
    requires! :id, type: Integer
    
    error!("节点不存在") if !@node.present?
  end

  private

  def allowed_params
    params.permit(
      :id,
      :node_type_id,
      :node_region_id,
      :node_isp_id,
      :node_region_out_id,
      :name,
      :url,
      :status,
      :description,
      :port,
      :password,
      :encrypt_method,
      :max_connections_count,
      :user_group_id,
      :is_domestic,
      :level,
      :types
    )
  end

  def set_node
    @node = Node.find(params[:id])
  end

end

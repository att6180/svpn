class Api::Proxy::V1::SessionsController < Api::Proxy::V1::ApiController

  def test
    render plain: '', layout: false
  end

  def authenticate
    requires! :user_id, type: Integer
    requires! :node_id, type: Integer
    #requires! :session_id, type: Integer
    requires! :token, type: String

    user_id = params[:user_id]
    concurrent_key = "proxy_auth_lock_#{user_id}_#{Time.now.to_i}"

    # 使用redis加锁，并且在5秒钟之后过期
    if !Setting.tool_ignore_ids.include?(user_id.to_i) && RedisCache.is_concurrent?(concurrent_key)
      error!('请求过于频繁', 99) and return
    end

    # TODO: 调试认证使用的hard code
    if (params[:user_id] == "19" && params[:token] == "34qcPxEJcrE4xVLa41J5") ||
        Setting.tool_ignore_ids.include?(params[:user_id].to_i)
      # success
    else
      # 认证
      @user = User.find_by(id: params[:user_id])
      _, session_token, session_error_code = UserSession.read_by_user_id(@user.id)
      if !@user.present? || session_token != params[:token]
        # 记录验证失败日志
        CreateAuthenticationFailureLogJob.perform_later({ 
          user_id: params[:user_id],
          node_id: params[:node_id],
          token: params[:token]
        }) if Setting.proxy_auth_failure_log_record_enabled
        error!('认证失败', session_error_code) and return
      end

      if !@user.is_enabled?
        error!(api_t('user_has_been_disabled'), UserSession::ERR_USER_DISABLED) and return
      end

      # 检查节点状态
      node = Node.find_by(id: params[:node_id])
      if !node.present?
        error!(api_t("node_does_not_exist"), UserSession::ERR_NODE_NOT_EXIST) and return
      end

      if node.is_overload?
        error!(api_t("node_is_overload"), UserSession::ERR_NODE_OVERLOAD) and return
      end

      node_type = node.node_type
      if @user.user_group.level < node_type.user_group.level
        error!(api_t("user_group_level_is_not_enough"), UserSession::ERR_USER_LEVEL_NOT_ENOUGH) and return
      end

      # 根据服务器类型ID获得用户对应的服务器类型
      user_node_type = @user.user_node_type_by_type_id(node_type.id)
      # 如果要连接的服务器类型需要钻石 并且 用户当前的服务器类型是按次
      if node_type.expense_coins > 0 && user_node_type.status_times?
        # 如果用户服务器类型已到期
        if user_node_type.expired_at <= Time.now
          # 重置session_token，原因为虚拟币不够
          #@user.user_session.regenerate_token!(UserSession::ERR_SERVICE_EXPIRED)
          # 服务器过期返回3
          #error!(api_t("user_node_type_expired"), UserSession::ERR_SERVICE_EXPIRED) and return
          # 判断自动续费开关是否开启
          if Setting.auto_renew.to_i == 0
            error!(api_t("user_node_type_expired_auto_renew_is_disabled"), UserSession::ERR_SERVICE_EXPIRED_AUTO_RENEW_DISABLED) and return
          end
          # 判断钻石够不够钻石
          if @user.current_coins < node_type.expense_coins
            error!(api_t("coins_is_not_enough"), UserSession::ERR_NOT_ENOUGH_COINS) and return
          end
          @user.consume_coins(node_type, node)
        end
      end
    end
  end

end

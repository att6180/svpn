class Api::Proxy::V1::UsersController < Api::Proxy::V1::ApiController

  # 记录用户流量
  # 代理服务器每超过指定时间或流量，就提交一次，每次提交后就清空
  # 后端进行流量累加
  def record_bytes
    requires! :user_id, type: Integer
    requires! :node_id, type: Integer
    requires! :used_bytes, type: Integer

    user_id = params[:user_id]
    used_bytes = params[:used_bytes]

    $redis.hincrby(
      User::CACHE_KEY_TRAFFICS,
      user_id,
      used_bytes.to_i
    ) if used_bytes.to_i > 0

    # 记录用户活跃日志
    user = User.find(user_id)
    if user.present?
      CreateUserActiveLogJob.perform_later(
        user_id: user.id,
        created_at: DateUtils.time2str(Time.now),
        app_channel: user.create_app_channel,
        app_version: user.create_app_version,
        active_type: UserActiveHourLog.active_types[:flow]
      )

      # 2018-06-27 换回原来的通过去向日志流量统计 --！
      # # 记录用户当天流量
      # CreateUserTrafficLogJob.perform_later(
      #   user_id,
      #   used_bytes.to_i,
      #   Time.now.to_date.to_s,
      #   user.create_app_channel,
      #   user.create_app_version
      # )
    end
  end

  # 服务器过期检查
  def inspect_expiration
    requires! :user_id, type: Integer
    requires! :node_id, type: Integer

    # 此接口由于认证接口做了续费检查，所以暂时屏蔽些接口的检查
    #user = User.find(params[:user_id])
    #user.inspect_expiration_for_proxy(params[:node_id]) if user.present?
  end

  # 客户端日志相关
  def client_log
  end

end

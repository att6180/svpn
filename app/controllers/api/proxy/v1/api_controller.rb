class Api::Proxy::V1::ApiController < ActionController::API

  include ActionView::Layouts
  layout 'proxy_api'

  include ActionController::HttpAuthentication::Token::ControllerMethods

  before_action :setup_layout_elements
  #before_action :authenticate!

  class ParameterValueNotAllowed < ActionController::ParameterMissing
    attr_reader :values
    def initialize(param, values) # :nodoc:
      @param = param
      @values = values
      super("参数#{param}的值只允许在#{values}以内")
    end
  end

  class AccessDenied < StandardError; end
  class PageNotFound < StandardError; end

  rescue_from(ActionController::ParameterMissing) do |err|
    render json: { success: false, error: 'ParameterInvalid', messages: ["参数#{err.param}未提供或为空"] }, status: 400
  end
  rescue_from(ActiveRecord::RecordInvalid) do |err|
    render json: { success: false, error: 'RecordInvalid', messages: [err] }, status: 400
  end
  rescue_from(AccessDenied) do |err|
    render json: { success: false, error: 'AccessDenied', messages: [err] }, status: 403
  end
  rescue_from(ActiveRecord::RecordNotFound) do
    render json: { success: false, error: 'ResourceNotFound', messages: ['记录未找到'] }, status: 404
  end

  def requires!(name, opts = {})
    opts[:require] = true
    optional!(name, opts)
  end

  def optional!(name, opts = {})
    if params[name].blank? && opts[:require] == true
      raise ActionController::ParameterMissing.new(name)
    end

    if opts[:values] && params[name].present?
      values = opts[:values].to_a
      if !values.include?(params[name]) && !values.include?(params[name].to_i)
        raise ParameterValueNotAllowed.new(name, opts[:values])
      end
    end

    if params[name].blank? && opts[:default].present?
      params[name] = opts[:default]
    end
  end

  def setup_layout_elements
    @success = true
    @messages = []
  end

  def api_t(key)
    I18n.t("api.#{key}")
  end

  def client_ip
    # 首先获取 X_REAL_IP，如果开头为10，或者为共享地址，则获取CDN_SRC_IP
    # 如果还是为空(本地开发环境)，则获取remote_ip
    result = request.headers["HTTP_X_REAL_IP"]&.split(',')&.first
    if result.blank? || Utils::IP.is_private_ip?(result)
      result = request.headers["HTTP_CDN_SRC_IP"]
    end
    result = request.remote_ip if result.blank?
    result
  end

  #def client_ip
    #result = request.headers["X-Forwarded-For"]&.split(',')&.first
    #result = request.remote_ip if result.blank?
    #result
  #end

  def proxy_client_ip
    request.headers["X-Forwarded-For"]&.split(',')&.first
  end

  def error!(msg, code = nil)
    @success = false
    @message = msg
    json = { success: @success, message: @message }
    json[:error_code] = code if code.present?
    render json: json
  end

  def error_detail!(obj, code = nil)
    @success = false
    messages = obj.errors.messages
    if messages.present?
      messages.each do |key, value|
        value.each do |msg|
          @messages << "#{key}#{msg}"
        end
      end
    else
      @messages << '请求失败'
    end
    json = { success: @success, messages: @messages }
    json[:error_code] = code if code.present? && code > 0
    render json: json
  end

  def log(msg)
    Rails.logger.info("#{Time.now}-> #{msg}")
  end

  def authenticate!
    authenticate_token || render_unauthorized
  end

  def authenticate_token
    authenticate_with_http_token do |token, options|
      CONFIG.proxy_api_token == token
    end
  end

  def render_unauthorized(realm = "Application")
    self.headers["WWW-Authenticate"] = %(Token realm="#{realm.gsub(/"/, "")}")
    render json: { success: false, error: 'Unautherized', messages: ['Authorization has been denied for this request.']}, status: 403
  end

end

class Api::Proxy::V1::ConnectionLogsController < Api::Proxy::V1::ApiController

  # 记录连接日志
  def create
    requires! :user_id, type: String
    requires! :session_id, type: String
    requires! :node_region_id, type: Integer
    # 二级代理服务器id
    requires! :node_id, type: Integer
    requires! :target_address, type: String
    requires! :target_ip, type: String
    requires! :in_bytes, type: Integer
    requires! :out_bytes, type: Integer
    requires! :cost_time, type: Integer
    optional! :client_ip, type: String
    # 一级代理服务器ip
    optional! :first_proxy_ip, type: String

    # 获取域名
    host = params[:target_address].try(:split, ':')&.first
    # 如果域名为IP，则不存储
    domain = Utils::IP.valid_ipv4?(host) ? nil : host
    top_level_domain = Utils::IP.valid_ipv4?(host) ? host : PublicSuffix.domain(domain)
    client_remote_ip = params[:client_ip]
    ip_region = client_remote_ip.present? ? Ipnet.find_by_ip(client_remote_ip) : nil
    ip_country = ip_region.present? ? ip_region[:country] : nil
    ip_province = ip_region.present? ? ip_region[:province] : nil
    ip_city = ip_region.present? ? ip_region[:city] : nil

    log = UserConnectionLog.create(
      user_id: params[:user_id],
      session_id: params[:session_id],
      node_region_id: params[:node_region_id],
      node_id: params[:node_id],
      target_address: params[:target_address],
      target_ip: params[:target_ip],
      target_domain: domain,
      top_level_domain: top_level_domain,
      in_bytes: params[:in_bytes],
      out_bytes: params[:out_bytes],
      cost_time: params[:cost_time],
      client_ip: client_remote_ip,
      ip_country: ip_country,
      ip_province: ip_province,
      ip_city: ip_city,
      first_proxy_ip: params[:first_proxy_ip]
    )

    # 写入域名地域统计表
    region_stat = UserConnectionRegionStat.find_or_initialize_by(
      user_id: params[:user_id],
      domain: top_level_domain,
      ip_country: ip_country,
      ip_province: ip_province,
      ip_city: ip_city
    )
    visits_count = region_stat.visits_count || 0
    region_stat.visits_count = visits_count + 1
    region_stat.save if region_stat.changed?

  end

end

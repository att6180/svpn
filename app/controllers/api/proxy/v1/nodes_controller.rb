class Api::Proxy::V1::NodesController < Api::Proxy::V1::ApiController

  def limit_speed
    requires! :node_id, type: Integer
    optional! :user_id, type: Integer

    node = Node.find_by(id: params[:node_id])
    if node.blank?
      error!('node does not exist') and return
    end

    @node_type = node.node_type
    if @node_type.blank?
      error!('node type does not exist') and return
    end
  end

  # 写入在线用户数据
  def record_online_users
    requires! :node_id, type: Integer
    requires! :data, type: String

    node = Node.find_by(id: params[:node_id])

    error!("invalid node") and return if node.blank? || !node.is_level1?

    begin
      data = JSON.parse(params[:data], symbolize_names: true)
      user_list = data[:users]
    rescue => e
      error!(e) and return
    end

    # 每10分钟更新一次列表
    # 如果节点的分钟为能被10整除，则记录在线数
    if Time.now.min % 10 == 0 && node.enabled?
      node_region = node.node_region
      users = []
      user_list.each do |user|
        ip_region = Ipnet.find_by_ip(user[:ip])
        users << NodeOnlineUser.new(
          id: Utils::Gen.generate_uuid,
          user_id: user[:id],
          node_region_id: node_region.id,
          node_id: node.id,
          ip: user[:ip],
          ip_country: ip_region[:country],
          ip_province: ip_region[:province],
          ip_city: ip_region[:city],
          ip_area: ip_region[:area]
        )
      end
      node.update(connections_count: users.count)
      NodeOnlineUser.by_node(node.id).delete_all
      NodeOnlineUser.bulk_insert(users, use_provided_primary_key: true) if users.present?
      ProxyServerOnlineUsersLog.create(
        node_id: node.id,
        node_type_id: node.node_type_id,
        online_users_count: users.count,
        ip: client_ip
      )
      # 检查并更新历史最高在线人数
      User.online_users_count
    end

    # 写入在线人数到缓存
    node.record_online_users!(user_list.count)
  end

  # 写入服务器详情信息
  def record_detail
    requires! :ip, type: String
    requires! :current_bandwidth, type: Integer
    requires! :cpu_percent, type: Integer
    requires! :memory_percent, type: Integer
    requires! :transfer, type: Integer
    requires! :network_speed_up, type: Integer
    requires! :network_speed_down, type: Integer

    nodes = Node.where(url: params[:ip])
    node_level1_id = nodes.level1&.take&.id
    node_ids = nodes.map(&:id)
    node_additions = node_ids.present? ? NodeAddition.where(node_id: node_ids) : nil
    if node_additions.present?
      error!("record_failed") if !node_additions.update_all(
        current_bandwidth: params[:current_bandwidth],
        cpu_percent: params[:cpu_percent],
        memory_percent: params[:memory_percent],
        transfer: params[:transfer],
        network_speed_up: params[:network_speed_up],
        network_speed_down: params[:network_speed_down],
        disk_percent: params[:disk_percent]
      )
    end
    if node_level1_id.present?
      ProxyServerStatusLog.create(
        node_id: node_level1_id,
        bandwidth_percent: params[:current_bandwidth],
        cpu_percent: params[:cpu_percent],
        memory_percent: params[:memory_percent],
        transfer: params[:transfer],
        network_speed_up: params[:network_speed_up],
        network_speed_down: params[:network_speed_down],
        disk_percent: params[:disk_percent]
      )
    end
  end

  def remote_ip
    @ip = proxy_client_ip
  end

  def proxy_remote_ip
    @ip = proxy_client_ip
  end

  # 代理服务器黑名单
  # 代理服务器根据所配置域名、IP进行访问过滤；无法访问黑名单中内容
  def proxy_black_list
    @black_list = Setting.proxy_black_list
  end

  # 根据传入node_id获取该代理服务器所属服务类型
  # 返回对应服务类型所有可用二级代理服务器列表
  def proxy_list
    requires! :node_id, type: Integer

    node = Node.find_by(id: params[:node_id])
    if node.blank?
      error!('node does not exist') and return
    end
    
    node_type = node.node_type
    # 返回该服务类型可用的二级代理服务器
    @nodes = node_type.nodes
      .includes(node_region: [:node_country, :node_continent])
      .level2
      .where(status: :enabled)
  end
  
  # 获取域名分流规则
  def shunt_list
    begin
      data = JSON.parse(Setting.proxy_shunt_list)
    rescue
      error!(api_t("invalid_json_data"))  and return
    end

    @shunt_list = data 
  end

  # 获取节点测试地址
  def test_address
    @address =  ProxyTestDomain.select(:is_domestic, :domain).group_by(&:is_domestic_before_type_cast)
  end

  # 获取http抓取规则
  def capture_list
    begin
      data = JSON.parse(Setting.proxy_capture_list)
    rescue
      error!(api_t("invalid_json_data"))  and return
    end

    @capture_list = data 
  end

end

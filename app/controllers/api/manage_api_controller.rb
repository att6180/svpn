class Api::ManageApiController < ActionController::API
  Swagger::Docs::Generator::set_real_methods
  include Swagger::Docs::ImpotentMethods

  class << self
    Swagger::Docs::Generator::set_real_methods

    def inherited(subclass)
      super
      #if subclass.name != 'Api::Manage::V1::SessionsController'
        #subclass.class_eval do
          #setup_basic_api_documentation
        #end
      #end
    end

    def common_params(api)
      api.param :header, 'Authorization', :string, :required, '访问令牌'
    end

    def page_params(api)
      api.param :query, :page, :integer, :optional, "页码，默认: 1"
      api.param :query, :limit, :integer, :optional, "每页显示的记录数, 默认: 25"
    end

    def version_params(api)
      api.param :query, :app_channel, :string, :optional, "渠道筛选"
      api.param :query, :app_version, :string, :optional, "版本筛选"
    end

    def between_date_params(api)
      api.param :query, :start_at, :string, :optional, "日期筛选开始时间"
      api.param :query, :end_at, :string, :optional, "日期筛选结束时间"
    end

    def package_params(api)
      api.param :query, :platform_id, :integer, :optional, "平台筛选"
      api.param :query, :channel_account_id, :integer, :optional, "渠道账筛选"
      api.param :query, :package_id, :integer, :optional, "包名筛选"
    end

    private
    def setup_basic_api_documentation
      [:index, :show, :create, :update, :delete].each do |api_action|
        swagger_api api_action do
          param :header, 'Authorization', :string, :required, '访问令牌'
        end
      end
    end
  end

end

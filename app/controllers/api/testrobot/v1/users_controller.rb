class Api::Testrobot::V1::UsersController < Api::Testrobot::V1::ApiController

  # 注册user为机器人账号
  def register_self
    requires! :user_id, type: Integer

    user = User.find_by(id: params[:user_id])
    error!(api_t("user_does_not_exist"), 1) and return if user.blank?
    error!(api_t("user_has_been_disabled"), 2) and return if !user.is_enabled?

    Setting.tool_ignore_ids | [user.id]
  end
end

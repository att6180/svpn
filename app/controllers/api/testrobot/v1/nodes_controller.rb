class Api::Testrobot::V1::NodesController < Api::Testrobot::V1::ApiController

  def test_data
    is_domestic = Utils::IP.is_domestic?(client_ip)
    nodes = is_domestic ? Node.domestic : Node.oversea
    @nodes = nodes.enabled.level1.order(id: :asc)
    # 根据请求来的IP获取用户是否是国内用户
    @domains = is_domestic ? ProxyTestDomain.foreign : ProxyTestDomain.domestic
  end

  # 节点的启用和禁用
  def switch_status
    requires! :node_id, type: Integer
    requires! :status, type: Integer

    # status 状态, 1:启用，0:禁用
    node = Node.find_by(id: params[:node_id])
    error!('invalid node') and return if node.blank?

    result = if params[:status] == '1'
      node.update(status: :enabled) if node.disabled?
    else
      node.update(status: :disabled) if node.enabled?
    end

    # 如果记录被更新了，才记录日志
    if result
      node_types = NodeType.cache_list
      node_type_indexs = node_types.index_by(&:id)
      CreateNodeSwitchLogJob.perform_later(
        node.id,
        params[:status],
        node_type_indexs[node.node_type_id].name,
        node.url,
        client_ip,
        DateUtils.time2str(Time.now)
      )
    end
  end

  # 提交服务器切换状态信息
  def record_switch_status
    requires! :data, type: String

    error!("invalid data") and return if params[:data].blank?

    begin
      result = JSON.parse(params[:data], symbolize_names: true)
    rescue
      error!("parse error") and return
    end

    result = result[:nodes]
    node_ids = result.map{|k, v| k.to_s}
    # 删除原数据
    NodeSwitchStatus.where(node_id: node_ids).delete_all
    nodes = []
    result.each do |k, v|
      nodes << NodeSwitchStatus.new(
        node_id: v[:node_id],
        status: v[:current_status],
        total_failed_rate: v[:total_failed_rate],
        total_failed_count: v[:total_failed_count],
        total_test_count: v[:total_test_count],
        average_response_time: v[:average_response_time],
        last_failed_rate: v[:record_failed_rate],
        last_test_count: v[:record_test_count],
        last_test_status: v[:node_test_result_list_string],
        switch_on_count: v[:switch_on_count],
        switch_off_count: v[:switch_off_count]
      )
    end
    NodeSwitchStatus.bulk_insert(nodes) if nodes.present?
  end

end

class Api::Testrobot::V1::LogsController < Api::Testrobot::V1::ApiController

  def test_logs
    requires! :node_id, type: Integer
    requires! :data, type: String
    # 用户id,域名id,是否成功

    new_logs = []
    logs = params[:data].split('|')
    logs.each do |log|
      next if log.blank?
      log_params = log.split(',')
      user_id = log_params[0]
      domain_id = log_params[1]
      is_successed = log_params[2] == '1'
      log = ProxyTestDomainLog.find_or_initialize_by(
        user_id: user_id,
        node_id: params[:node_id],
        proxy_test_domain_id: domain_id
      )
      log.is_successed = is_successed
      log.save
    end

    # 触发预警检查
    SmsNotify.check_test_robot_failed(params[:node_id])
  end

end

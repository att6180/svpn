class Api::Testrobot::V2::NodesController < Api::Testrobot::V1::ApiController

  def test_data
    requires! :session_id, type: String

    @user_session = UserSession.find_by(id: params[:session_id])
    error!("invalid session_id") if @user_session.blank?
    is_domestic = Utils::IP.is_domestic?(client_ip)
    nodes = is_domestic ? Node.domestic : Node.oversea
    @nodes = nodes.enabled.level1.order(id: :asc)
    # 根据请求来的IP获取用户是否是国内用户
    @domains = is_domestic ? ProxyTestDomain.foreign : ProxyTestDomain.domestic
  end

end

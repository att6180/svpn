class Api::Manage::V1::OperationSummarysController < Api::Manage::V1::ApiController

  include PermissionFilterable
  set_module_group_name :operation_behaviour

  swagger_controller :operation_summarys, "运营汇总"

  swagger_api :day_index do |api|
    summary "运营汇总日 - 列表"
    notes <<-EOF
    说明: 统计指定日期运营汇总信息,涵盖付费,新增，活跃，渗透等

    stat_date: 当前日期
    total_recharge_amount: 充值金额
    total_recharge_amount_percent: 金额占比
    new_users_count: 新增人数
    new_users_count_percent: 新增占比
    active_users_count: 活跃人数
    active_users_count_percent: 活跃占比
    paid_users_count: 当日付费人数
    payment_rate: 当日付费率
    paid_total_price: 当日付费金额
    paid_total_price_percent: 当日金额占比
    seven_days_users_count: 7日付费去重人数
    seven_days_users_count_percent: 7日去重人数占比
    seven_days_total_price: 7日付费总金额
    seven_days_total_price_percent: 7日付费总金额占比
    thirty_days_users_count: 30日付费去重人数
    thirty_days_users_count_percent: 30日付费去重人数占比
    thirty_days_total_price: 30日付费总金额
    thirty_days_total_price_percent: 30日付费总金额占比
    second_day: 次日留存
    second_day_rate: 次留占比
    third_day: 3日留存
    third_day_rate: 3留占比
    seventh_day: 7日留存
    seventh_day_rate: 7留占比
    fifteen_day: 15日留存
    fifteen_day_rate: 15留占比
    thirtieth_day: 30日留存
    thirtieth_day_rate: 30留占比
    EOF
    Api::Manage::V1::ApiController.common_params(api)
    Api::Manage::V1::ApiController.page_params(api)
    Api::Manage::V1::ApiController.package_params(api)
    param :query, :start_at, :string, :optional, "开始日期, 如: 2017-01-01"
    param :query, :end_at, :string, :optional, "结束日期, 如: 2017-03-01"
  end
  def day_index
    # 如果有筛选则查询一遍无筛选的数据进行对比
    total_logs = has_package_param? ? Log::OperationSummaryDayStat.where(filter_type: 0) : nil

    logs = Log::OperationSummaryDayStat.all
    logs = handle_package_params(logs)
    if params[:start_at].present? && params[:end_at].present?
      logs = logs.between_months(params[:start_at].to_date, params[:end_at].to_date)
      total_logs = total_logs.between_months(params[:start_at].to_date, params[:end_at].to_date) if total_logs
    else
      today = Time.now.to_date
      logs = logs.between_months(today.beginning_of_month, today.end_of_month)
      total_logs = total_logs.between_months(today.beginning_of_month, today.end_of_month) if total_logs
    end
    @logs = logs.stat_result_transaction.order("stat_date DESC").page(param_page).per(param_limit)
    if total_logs
      @total_logs = total_logs
        .order("stat_date DESC")
        .stat_result_transaction
        .page(param_page).per(param_limit)
        .index_by{|log| log.stat_date.strftime("%Y%m%d")}
    end
  end


  swagger_api :month_index do |api|
    summary "运营汇总月 - 列表"
    notes <<-END
    stat_year: 年
    stat_month：月
    total_recharge_amount: 充值金额 与“付费信息”里“日”的“充值金额”字段功能一致
    total_recharge_amount_percent: 金额占比
    new_users_count: 新增人数 与“新增用户”里“日”的“日新增用户”字段功能一致
    new_users_count_percent: 新增占比
    active_users_count: 活跃人数 与“活跃用户”里“日”的“日活跃用户”字段功能一致
    active_users_count_percent: 活跃占比
    END
    param :query, :start_at, :string, :optional, "开始年月, 如: 2017-01"
    param :query, :end_at, :string, :optional, "结束年月, 如: 2017-03"
    Api::Manage::V1::ApiController.common_params(api)
    Api::Manage::V1::ApiController.page_params(api)
    Api::Manage::V1::ApiController.package_params(api)
  end
  def month_index
    # 如果有筛选则查询一遍无筛选的数据进行对比
    total_logs = has_package_param? ? Log::OperationSummaryMonthStat.where(filter_type: 0) : nil

    start_at = "#{params[:start_at]}-01"&.to_date || (Time.now - 12.month).to_date.beginning_of_year
    end_at = "#{params[:end_at]}-01"&.to_date || Time.now.to_date.end_of_year

    logs = Log::OperationSummaryMonthStat.all
    logs = handle_package_params(logs)
    logs = logs.between_month(start_at, end_at)
    total_logs = total_logs.between_month(start_at, end_at) if total_logs
    @logs = logs.stat_result.order(stat_year: :desc, stat_month: :desc).page(param_page).per(param_limit)
    if total_logs
      @total_logs = total_logs
        .stat_result
        .order(stat_year: :desc, stat_month: :desc)
        .page(param_page).per(param_limit)
        .index_by{|log| "#{log.stat_year}#{log.stat_month}"}
    end
  end
end

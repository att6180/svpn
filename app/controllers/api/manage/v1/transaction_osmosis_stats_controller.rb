class Api::Manage::V1::TransactionOsmosisStatsController < Api::Manage::V1::ApiController
  # 设置权限
  include PermissionFilterable
  set_module_group_name :operation_behaviour

  swagger_controller :transaction_osmosis_stats, "充值渗透分析"

  swagger_api :index do |api|
    summary "充值信息 - 充值渗透分析 - 列表"
    notes <<-EOF
    说明: 此接口与「留存」接口大同小异，区别在于此接口显示的是新增用户数与充值渗透分析

    stat_date: 当前日期
    users_count: 当日新增用户
    users_percent: 筛选结果占比，此字段只在筛选渠道版本时出现
    paid_users_count: 当日充值用户
    paid_total_price: 当日充值金额
    average_price: 当日平均充值
    payment_rate: 当日充值率
    second_paid_users_count: 2日充值用户
    second_total_price: 2日充值金额
    second_average_price: 2日平均充值
    second_payment_rate: 2日充值率
    seventh_paid_users_count: 7日充值用户
    seventh_paid_total_price: 7日充值金额
    seventh_average_price: 7日平均充值
    seventh_payment_rate: 7日充值率
    thirtieth_paid_users_count: 30日充值用户
    thirtieth_paid_total_price: 30日充值金额
    thirtieth_average_price: 30日平均充值
    thirtieth_payment_rate: 30日充值率
    seven_days_users_count: 当日到7日充值用户数
    seven_days_total_price: 当日到7日充值总额
    thirty_days_users_count: 当日到30日充值用户数
    thirty_days_total_price: 当日到30日充值总额
    EOF
    Api::Manage::V1::ApiController.common_params(api)
    Api::Manage::V1::ApiController.page_params(api)
    Api::Manage::V1::ApiController.version_params(api)
    param :query, :start_at, :string, :optional, "开始年月日, 如: 2017-01-04"
    param :query, :end_at, :string, :optional, "结束年月日, 如: 2017-03-05"
  end
  def index
    # 如果有筛选则查询一遍无筛选的数据进行对比
    total_logs = has_channel_param? ? UserRetentionRateStat.where(filter_type: 0) : nil

    logs = UserRetentionRateStat.all
    logs = handle_version_params(logs)
    if params[:start_at].present? && params[:end_at].present?
      logs = logs.between_months(params[:start_at].to_date, params[:end_at].to_date)
      total_logs = total_logs.between_months(params[:start_at].to_date, params[:end_at].to_date) if total_logs
    else
      today = Time.now.to_date
      logs = logs.between_months(today.beginning_of_month, today.end_of_month)
      total_logs = total_logs.between_months(today.beginning_of_month, today.end_of_month) if total_logs
    end
    @logs = logs.stat_result_transaction.order("stat_date DESC").page(param_page).per(param_limit)
    if total_logs
      @total_logs = total_logs
        .order("stat_date DESC")
        .stat_result_transaction
        .page(param_page).per(param_limit)
        .index_by{|log| log.stat_date.strftime("%Y%m%d")}
    end
  end

  swagger_api :chart do |api|
    summary "充值信息 - 充值渗透分析 - 图表"
    notes <<-EOF
    说明: 此接口与「留存」接口大同小异，区别在于此接口显示的是新增用户数与充值渗透分析

    stat_date: 当前日期
    users_count: 当日新增用户
    payment_rate: 当日充值率
    second_payment_rate: 2日充值率
    seventh_payment_rate: 7日充值率
    thirtieth_payment_rate: 30日充值率
    EOF
    Api::Manage::V1::ApiController.common_params(api)
    param :query, :app_version, :string, :optional, "版本筛选"
    param :query, :app_channel, :string, :optional, "渠道筛选"
    param :query, :start_at, :string, :optional, "开始年月, 如: 2017-01-04"
    param :query, :end_at, :string, :optional, "结束年月, 如: 2017-03-05"
  end
  def chart
    logs = UserRetentionRateStat.all
    logs = handle_version_params(logs)
    if params[:start_at].present? && params[:end_at].present?
      logs = logs.between_months(params[:start_at].to_date, params[:end_at].to_date)
    else
      today = Time.now.to_date
      logs = logs.between_months(today.beginning_of_month, today.end_of_month)
    end
    @logs = logs.stat_result_transaction.order("stat_date ASC")
  end

  swagger_api :details do |api|
    summary "充值信息 - 充值渗透分析 - 当日充值用户详情"
    notes ""
    Api::Manage::V1::ApiController.common_params(api)
    Api::Manage::V1::ApiController.page_params(api)
    param :query, :stat_date, :string, :required, "统计日期, 如: 2017-04-11"
    param :query, :app_version, :string, :optional, "版本筛选"
    param :query, :app_channel, :string, :optional, "渠道筛选"
  end
  def details
    requires! :stat_date, type: String

    logs = TransactionLog.page(param_page).per(param_limit)
    logs = logs.where(app_version: params[:app_version]) if params[:app_version].present?
    logs = logs.where(app_channel: params[:app_channel]) if params[:app_channel].present?
    @logs = logs.by_date(params[:stat_date]&.to_date)
      .where("DATE(user_created_at) = ?", params[:stat_date]&.to_date)
      .by_paid
      .select("user_id, COUNT(id) as orders_count, SUM(price) as total_price")
      .group(:user_id)
      .includes(:user)
  end

  swagger_api :retention_details do |api|
    summary "充值信息 - 充值渗透分析 - 充值用户详情"
    notes <<-EOF
    user_id: 用户ID
    username: 用户名
    orders_count: 充值次数
    total_price: 充值总金额
    EOF
    Api::Manage::V1::ApiController.common_params(api)
    Api::Manage::V1::ApiController.page_params(api)
    param :query, :stat_date, :string, :required, "统计日期, 如: 2017-04-11"
    param :query, :stat_type, :integer, :required, "统计类型, 1:次日留存, 7:七日留存, 30: 30日留存"
    param :query, :app_version, :string, :optional, "版本筛选"
    param :query, :app_channel, :string, :optional, "渠道筛选"
  end
  def retention_details
    requires! :stat_date, type: String
    requires! :stat_type, type: Integer

    error!('统计类类型参数错误') and return if !['1', '7', '30'].include?(params[:stat_type])

    stat_date = params[:stat_date].to_date
    app_channel = params[:app_channel]
    app_version = params[:app_version]
    logs = TransactionLog.page(param_page).per(param_limit)
    case params[:stat_type]
    when '1'
      logs = logs.retentions(stat_date, stat_date + 1.days, nil, app_channel, app_version, false)
    when '7'
      logs = logs.retentions(stat_date, stat_date + 2.days, stat_date + 6.days, app_channel, app_version, true)
    when '30'
      logs = logs.retentions(stat_date, stat_date + 7.days, stat_date + 29.days, app_channel, app_version, true)
    end
    @logs = logs.includes(:user)
  end

end

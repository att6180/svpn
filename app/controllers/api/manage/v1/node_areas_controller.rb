class Api::Manage::V1::NodeAreasController  < Api::Manage::V1::ApiController

  include PermissionFilterable
  set_module_group_name :node_manage

  before_action :set_entity
  before_action :set_node_area, only: [:update, :destroy]

  swagger_controller :node_areas, "洲/国家配置管理"

  swagger_api :index do |api|
    summary "服务器群组管理 - 地域配置 -洲/国家配置 - 列表"
    notes ""
    param :query, :type, :string, :required, "操作实体 国家:NodeCountry/洲:NodeContinent"
    Api::Manage::V1::ApiController.common_params(api)
    Api::Manage::V1::ApiController.page_params(api)
  end
  def index
    requires! :type, type: String
    optional! :page, type: Integer
    optional! :limit, type: Integer

    @node_areas = @entity.sorted.page(param_page).per(param_limit)
  end

  swagger_api :create do |api|
    summary "服务器群组管理 - 地域配置 -洲/国家配置 - 创建"
    notes ""
    Api::Manage::V1::ApiController.common_params(api)
    param :form, :name, :string, :required, "洲/国家名称"
    param :form, :sequence, :integer, :optional, "显示顺序"
    param :form, :abbr, :string, :optional, "国家图片地址"
    param :form, :parent_id, :integer, :optional, "父级id，只有国家配置才有该选项"
    param :form, :type, :string, :required, "操作实体 国家:NodeCountry/洲:NodeContinent"
  end
  def create
    requires! :name, type: String

    @node_area = @entity.new(allowed_params)
    error_detail!(@node_area) if !@node_area.save
  end

  swagger_api :update do |api|
    summary "服务器群组管理 - 地域配置 -洲/国家配置 - 更新"
    notes ""
    Api::Manage::V1::ApiController.common_params(api)
    param :path, :id, :integer, :required, "洲/国家ID"
    param :form, :name, :string, :required, "洲/国家名称"
    param :form, :sequence, :integer, :optional, "显示顺序"
    param :form, :abbr, :string, :optional, "国家图片地址"
    param :form, :parent_id, :integer, :optional, "父级id，只有国家配置才有该选项"
    param :form, :type, :string, :required, "操作实体 国家:NodeCountry/洲:NodeContinent"
  end
  def update
    requires! :id, type: Integer

    error_detail!(@node_area) if !@node_area.update(allowed_params)
  end

  swagger_api :destroy do |api|
    summary "服务器群组管理 - 地域配置 -洲/国家配置 - 删除"
    notes ""
    Api::Manage::V1::ApiController.common_params(api)
    param :path, :id, :integer, :required, "洲/国家ID"
    param :form, :type, :string, :required, "操作实体 国家:NodeCountry/洲:NodeContinent"
  end
  def destroy
    requires! :id, type: Integer
    error!("该国家被地域引用中，不能删除！") and return if @node_area.can_destroy?
    error_detail!(@node_area) if !@node_area.destroy
  end

  private

  def set_entity
    @entity = params[:type].constantize
  end

  def set_node_area
    @node_area = @entity.find(params[:id])
  end

  def allowed_params
    params.permit(
      :name,
      :sequence,
      :abbr,
      :parent_id,
      :type
    )
  end
end

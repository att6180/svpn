class Api::Manage::V1::PlanTemplatesController < Api::Manage::V1::ApiController

  # 设置权限
  include PermissionFilterable
  set_module_group_name :charge_manage

  before_action :set_item, only: [:show, :update, :destroy]

  swagger_controller :plan_templates, "套餐模版"

  swagger_api :index do |api|
    summary "套餐模版 - 列表"
    notes ""
    Api::Manage::V1::ApiController.common_params(api)
    param :query, :plan_type, :string, :optional, "模版类型，这里的筛选项可以写成固定的, 1:审核模版, 2:原价套餐, 3:活动套餐"
    param :query, :status, :string, :optional, "状态, 0:禁用, 1:启用, 不提供此参数时则返回所有启用的记录"
    Api::Manage::V1::ApiController.page_params(api)
  end
  def index
    items = V2::PlanTemplate.all
    items = items.where(plan_type: params[:plan_type]) if params[:plan_type].present?
    items = if params[:status].present?
      items.where(status: params[:status])
    else
      items.enabled
    end
    @items = items.page(param_page).per(param_limit)
  end

  swagger_api :create do |api|
    summary "套餐模版 - 创建"
    notes ""
    Api::Manage::V1::ApiController.common_params(api)
    param :form, :name, :string, :required, "名称"
    param :form, :plan_type, :string, :required, "模版类型，这里的筛选项可以写成固定的, 1:审核模版, 2:原价套餐, 3:活动套餐"
    param :form, :status, :string, :optional, "状态, 0:禁用, 1:启用, 默认为1"
  end
  def create
    requires! :name, type: String
    requires! :plan_type, type: String

    @item = V2::PlanTemplate.new(allowed_params)
    error_detail!(@item) if !@item.save
  end

  swagger_api :show do |api|
    summary "套餐模版 - 单个获取"
    notes ""
    Api::Manage::V1::ApiController.common_params(api)
    param :path, :id, :integer, :required, "套餐ID"
  end
  def show
    requires! :id, type: Integer
  end

  swagger_api :update do |api|
    summary "套餐模版 - 更新"
    notes ""
    Api::Manage::V1::ApiController.common_params(api)
    param :path, :id, :integer, :optional, "套餐ID"
    param :form, :name, :string, :optional, "名称"
    param :form, :plan_type, :string, :optional, "模版类型，这里的筛选项可以写成固定的, 1:审核套餐, 2:原价套餐, 3:活动套餐"
    param :form, :status, :string, :optional, "状态, 0:禁用, 1:启用"
  end
  def update
    error_detail!(@item) if !@item.update(allowed_params)
  end

  private

  def allowed_params
    params.permit(
      :name,
      :plan_type,
      :status
    )
  end

  def set_item
    @item = V2::PlanTemplate.find(params[:id])
  end

end

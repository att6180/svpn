class Api::Manage::V1::SystemEnumsController < Api::Manage::V1::ApiController

  before_action :set_enum, only: [:show, :update, :destroy]

  swagger_controller :system_enums, "渠道版本配置"

  swagger_api :index do |api|
    summary "渠道版本配置 - 列表"
    notes ""
    Api::Manage::V1::ApiController.common_params(api)
    param :query, :enum_type, :string, :optional, "类型, channel:渠道, version:版本, channel_account:渠道账号，package:包名"
    param :query, :page, :integer, :optional, "页码，默认: 1"
    param :query, :limit, :integer, :optional, "每页显示的记录数, 默认: 25"
  end
  def index
    can_access?(:system_enum)
    @system_enums = SystemEnum.where(enum_type: params[:enum_type]).sorted_by_id.page(param_page).per(param_limit)
  end

  swagger_api :create do |api|
    summary "渠道版本配置 - 创建"
    notes ""
    Api::Manage::V1::ApiController.common_params(api)
    param :form, :enum_type, :string, :required, "类型, channel:渠道, version:版本, channel_account:渠道账号，package:包名"
    param :form, :name, :string, :required, "名称, 如: moren"
  end
  def create
    requires! :enum_type, type: Integer
    requires! :name, type: String

    can_access?(:system_enum)

    @system_enum = SystemEnum.new(allowed_params)
    error_detail!(@system_enum) if !@system_enum.save
  end

  swagger_api :show do |api|
    summary "渠道版本配置 - 单个获取"
    notes ""
    Api::Manage::V1::ApiController.common_params(api)
    param :path, :id, :integer, :required, "ID"
  end
  def show
    requires! :id, type: Integer
    can_access?(:system_enum)
  end

  swagger_api :update do |api|
    summary "渠道版本配置 - 更新"
    notes ""
    Api::Manage::V1::ApiController.common_params(api)
    param :path, :id, :integer, :required, "ID"
    param :form, :enum_type, :string, :optional, "类型, channel:渠道, version:版本, channel_account:渠道账号，package:包名"
    param :form, :name, :string, :optional, "名称, 如: moren"
  end
  def update
    requires! :id, type: Integer

    can_access?(:system_enum)

    error_detail!(@system_enum) if !@system_enum.update(allowed_params)
  end

  swagger_api :destroy do |api|
    summary "渠道版本配置 - 删除"
    notes ""
    Api::Manage::V1::ApiController.common_params(api)
    param :path, :id, :integer, :required, "ID"
  end
  def destroy
    requires! :id, type: Integer

    can_access?(:system_enum)

    error_detail!(@system_enum) if !@system_enum.destroy
  end

  swagger_api :group_index do |api|
    summary "渠道版本配置 - 分组列表"
    notes ""
    Api::Manage::V1::ApiController.common_params(api)
  end
  def group_index
    @channels = SystemEnum.enums_by_channel
    @versions = SystemEnum.enums_by_version
    @platforms = SystemEnum.enums_by_platform
    @channel_accounts = SystemEnum.enums_by_channel_account
    @packages = SystemEnum.enums_by_package
    @relations = SystemEnumRelation.cache_list
  end

  private

  def allowed_params
    params.permit(
      :enum_type,
      :name
    )
  end

  def set_enum
    @system_enum = SystemEnum.find(params[:id])
  end
end

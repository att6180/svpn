class Api::Manage::V1::UserDetailsController < Api::Manage::V1::ApiController

  # 设置权限
  include PermissionFilterable
  set_module_group_name :user_base_info

  swagger_controller :user_details, "用户详情"

  swagger_api :base_info do |api|
    summary "帐号详情页 - 基础信息"
    notes <<-END
    id: 用户id
    username: 用户名
    last_signin_at: 最后登录时间
    last_signin_version: 最后登录版本
    group_name: 帐户类型
    total_payment_amount: 充值金额
    current_coins: 当前钻石数
    total_used_coins: 已使用钻石数
    uuid: UUID
    is_enabled: 用户状态(true:正常, false:封号)
    create_at: 注册时间
    create_version: 注册版本
    END
    Api::Manage::V1::ApiController.common_params(api)
    param :path, :id, :integer, :required, "用户ID"
  end
  def base_info
    @user = User.find(params[:id])
    node_types = NodeType.cache_list
    UserNodeType.check_types(@user, @user.user_node_types, node_types)
    @user_group_indexs = UserGroup.cache_list.index_by(&:id)
    @user_node_types = @user.user_node_types
      .in_node_types(node_types.map(&:id))
      .sorted_by_created_at.includes(:node_type)
  end

  swagger_api :signin_logs do |api|
    summary "帐号详情页 - 登录日志"
    notes <<-EOF
    id: 登录日志ID
    created_at: 登录时间
    device_model: 设备型号
    platform: 平台
    system_version: 系统版本
    app_channel: 渠道
    app_version: 版本
    app_version_number: 版本号
    ip: 客户端IP
    ip_country: 国家
    ip_province: 省
    ip_city: 市
    EOF
    Api::Manage::V1::ApiController.common_params(api)
    param :path, :id, :integer, :required, "用户ID"
    param :query, :start_at, :string, :optional, "起始日期, 如: 2017-03-01"
    param :query, :end_at, :string, :optional, "结束日期, 如: 2017-03-04"
    Api::Manage::V1::ApiController.page_params(api)
  end
  def signin_logs
    requires! :id, type: Integer

    start_at = params[:start_at]&.to_date || Time.now.to_date
    end_at = params[:end_at]&.to_date || Time.now.to_date

    user = User.find(params[:id])
    @logs = user.user_signin_logs
      .recent
      .between_date(start_at, end_at)
      .page(param_page)
      .per(param_limit)
  end

  swagger_api :connection_logs do |api|
    summary "帐号详情页 - 连接请求"
    notes ""
    Api::Manage::V1::ApiController.common_params(api)
    param :path, :id, :integer, :required, "用户ID"
    param :query, :start_at, :string, :optional, "起始日期, 如: 2017-03-01"
    param :query, :end_at, :string, :optional, "结束日期, 如: 2017-03-04"
    param :query, :status, :integer, :optional, "连接状态 0:失败, 1:成功"
    Api::Manage::V1::ApiController.page_params(api)
  end
  def connection_logs
    requires! :id, type: Integer

    user = User.find(params[:id])
    logs = user.node_connection_logs.recent
    logs = logs.between_date(params[:start_at].to_date, params[:end_at].to_date) if params[:start_at].present? && params[:end_at].present?
    logs = logs.status_by_flag(params[:status] == '1' ? 'success' : 'error') if params[:status].present?
    @logs = logs.includes(:node).page(param_page).per(param_limit)
  end

  swagger_api :devices do |api|
    summary "帐号详情页 - 关联设备"
    notes ""
    Api::Manage::V1::ApiController.common_params(api)
    param :path, :id, :integer, :required, "用户ID"
    Api::Manage::V1::ApiController.page_params(api)
  end
  def devices
    optional! :page, type: Integer
    optional! :limit, type: Integer

    user = User.find(params[:id])
    @devices = user.devices.page(param_page).per(param_limit)
  end

  swagger_api :operation_logs do |api|
    summary "帐号详情页 - 操作路径"
    notes ""
    Api::Manage::V1::ApiController.common_params(api)
    param :path, :id, :integer, :required, "用户ID"
    param :query, :date, :string, :optional, "筛选日期, 如: 2017-04-11"
  end
  def operation_logs
    optional! :date, type: String

    date = params[:date]&.to_date || Time.now.to_date
    @logs = Mongo::UserOperationLog.where(user_id: params[:id]).by_date(date).by_time_asc
  end

  swagger_api :access_logs do |api|
    summary "帐号详情页 - 用户去向"
    notes ""
    Api::Manage::V1::ApiController.common_params(api)
    param :path, :id, :integer, :required, "用户ID"
    param :query, :start_at, :string, :optional, "日期筛选开始时间"
    param :query, :end_at, :string, :optional, "日期筛选结束时间"
    Api::Manage::V1::ApiController.page_params(api)
  end
  def access_logs
    start_at = params[:start_at]&.to_date&.beginning_of_day || Time.now.beginning_of_day
    end_at = params[:end_at]&.to_date&.end_of_day || Time.now.end_of_day
    @page_logs = Mongo::UserConnectionLog.where(user_id: params[:id])
      .order(created_at: :desc)
      .where(:created_at.gte => start_at, :created_at.lte => end_at)
      .page(param_page).per(param_limit)
    @logs = @page_logs.to_a
    node_ids = @logs.map(&:node_id).uniq.compact
    nodes = Node.where(id: node_ids)
    @group_nodes = nodes.index_by(&:id)
    @group_node_types = NodeType.cache_list.index_by(&:id)
    first_node_ips = @logs.map(&:first_proxy_ip).uniq.compact
    @first_nodes = Node.level1.where(url: first_node_ips).index_by(&:url)
  end

  swagger_api :transaction_logs do |api|
    summary "帐号详情页 - 充值信息"
    notes ""
    Api::Manage::V1::ApiController.common_params(api)
    param :path, :id, :integer, :required, "用户ID"
    param :query, :status, :string, :optional, "订单状态, pending: 未支付 | paid: 已支付"
    param :query, :start_at, :string, :optional, "日期筛选开始时间"
    param :query, :end_at, :string, :optional, "日期筛选结束时间"
    Api::Manage::V1::ApiController.page_params(api)
  end
  def transaction_logs
    requires! :id, type: Integer

    @user = User.find(params[:id])
    logs = @user.transaction_logs.recent.page(param_page).per(param_limit)
    logs = logs.where(status: params[:status]) if params[:status].present?
    if params[:start_at].present? && params[:end_at].present?
      logs = logs.between_date(params[:start_at].to_date, params[:end_at].to_date)
    end
    @logs = logs
    @filter_payment_amount = @logs.by_paid.sum(:price)
  end

  swagger_api :consumption_logs do |api|
    summary "帐号详情页 - 消费信息"
    notes ""
    Api::Manage::V1::ApiController.common_params(api)
    param :path, :id, :integer, :required, "用户ID"
    param :query, :start_at, :string, :optional, "日期筛选开始时间"
    param :query, :end_at, :string, :optional, "日期筛选结束时间"
    Api::Manage::V1::ApiController.page_params(api)
  end
  def consumption_logs
    requires! :id, type: Integer
    optional! :start_at, type: String
    optional! :end_at, type: String
    optional! :page, type: Integer
    optional! :limit, type: Integer

    @user = User.find(params[:id])
    @consume_coins = @user.consumption_logs.sum(:coins)
    logs = @user.consumption_logs.recent
    if params[:start_at].present? && params[:end_at].present?
      logs = logs.between_date(params[:start_at].to_date, params[:end_at].to_date)
    end
    @logs = logs.page(param_page).per(param_limit)
    @filter_consume_coins = @logs.sum(:coins)
  end

  swagger_api :update_profile do |api|
    summary "帐号详情页 - 信息管理"
    notes ""
    Api::Manage::V1::ApiController.common_params(api)
    param :path, :id, :integer, :required, "用户ID"
    param :form, :username, :string, :optional, "用户名"
    param :form, :password, :string, :optional, "密码"
    param :form, :current_coins, :integer, :optional, "钻石"
    param :form, :user_group_id, :integer, :optional, "用户类型ID(通过获取用户类型接口得到所有用户类型)"
    param :form, :is_enabled, :integer, :optional, "封号与解码, 0:封号, 1:解封"
    param :form, :user_node_types, :string, :optional, "用户服务器类型ID及有效期集合，JSON类型，如:[{\"id\":12, \"expired_at\": \"2017-04-22 08:00\"},{\"id\":13, \"expired_at\": \"2017-04-23 10:00\"}]"
  end
  def update_profile
    requires! :id, type: Integer
    optional! :username, type: String
    optional! :password, type: String
    optional! :current_coins, type: Integer
    optional! :user_group_id, type: Integer
    optional! :is_enabled, type: Integer

    can_access?(:user_info_update)

    # 用户属性
    @user = User.find(params[:id])
    @user.username = params[:username].strip if params[:username].present?
    @user.password = params[:password] if params[:password].present?
    @user.current_coins = params[:current_coins] if params[:current_coins].present?
    @user.user_group_id = params[:user_group_id] if params[:user_group_id].present?
    @user.is_enabled = params[:is_enabled] if params[:is_enabled].present?
    if @user.current_coins_changed?
      ActivityCoinRewardLog.create(
        user_id: @user.id,
        admin_id: current_admin.id,
        original_coins: @user.current_coins_was,
        current_coins: params[:current_coins].to_i,
        operation_type: 0
      )
    end
    if @user.changed?
      # 如果更新了状态信息，则同时增加封号日志
      if @user.is_enabled_changed?
        UserAccountStatusLog.create(
          user_id: @user.id,
          operation_type: params[:is_enabled] == '0' ? 'disable' : 'enable',
          admin_id: current_admin.id,
          admin_username: current_admin.username
        )
      end
      @user.save
      current_admin.create_log( key: 'update_user_info', params: { id: @user.id, name: @user.username })
    end

    # 用户服务器类型
    param_node_types = JSON.parse(params[:user_node_types], symbolize_names: true) if params[:user_node_types].present?
    inspect_expireation_has_changed = false
    if param_node_types.present?
      param_node_types.each do |t|
        node_type = @user.user_node_types.find_by(id: t[:id])
        if node_type.present? && node_type.user_id == @user.id
          original_expired_at = node_type.expired_at
          node_type.expired_at = t[:expired_at]
          if node_type.changed?

            UserExpiredRewardLog.create(
              user_id: @user.id,
              admin_id: current_admin.id,
              original_expired_at: original_expired_at,
              current_expired_at: t[:expired_at].to_datetime,
              node_type_id: node_type.node_type.id,
              user_node_type_id: node_type.id
            )
            # 如果更新了用户服务类型时间，则把用户的检测时间也一起更新了
            node_type.save
            if !inspect_expireation_has_changed
              @user.user_addition.update_columns(last_inspect_expiration_at: Time.now - 1.days)
              inspect_expireation_has_changed = true
            end
          end
        end
      end
    end
    @user_node_types = @user.user_node_types.includes(:node_type).order(node_type_id: :asc)
  end

  swagger_api :profile do |api|
    summary "帐号详情页 - 信息管理(获取指定用户信息)"
    notes ""
    Api::Manage::V1::ApiController.common_params(api)
    param :path, :id, :integer, :required, "用户ID"
  end
  def profile
    requires! :id, type: Integer

    can_access?(:user_info_update)

    # 用户属性
    @user = User.find(params[:id])
    @user_node_types = @user.user_node_types.includes(:node_type).order(node_type_id: :asc)
  end

  swagger_api :cancel_monthly do |api|
    summary "帐号详情页 - 取消包月"
    notes ""
    Api::Manage::V1::ApiController.common_params(api)
    param :form, :user_node_type_id, :integer, :required, "用户服务类型ID"
  end
  def cancel_monthly
    requires! :user_node_type_id, type: Integer

    can_access?(:user_info_update)

    user_node_type = UserNodeType.find_by(id: params[:user_node_type_id])
    if user_node_type.blank?
      error!('用户服务类型不存在') and return
    end
    # 转为按次
    user_node_type.update(status: :times, used_count: 0)
    # 更新最后检查时间
    user = user_node_type.user
    user.user_addition.update(last_inspect_expiration_at: Time.now - 20.minutes)
    current_admin.create_log( key: 'cancel_monthly', params: { username: user.username, user_id: user.id, user_node_type_name: user_node_type.node_type.name })
  end

  swagger_api :server_connection_logs do |api|
    summary "帐号详情页 - 服务端连接日志"
    notes <<-EOF
    id: 日志ID
    node_id: 代理服务器ID
    node_name: 代理服务器名
    node_ip: 代理服务器IP
    ip: 客户端IP
    is_domestic:是否是国内, 1:是, 0:否
    created_at: 连接时间
    EOF
    Api::Manage::V1::ApiController.common_params(api)
    param :path, :id, :integer, :required, "用户ID"
    param :query, :start_at, :string, :optional, "起始日期, 如: 2017-03-01"
    param :query, :end_at, :string, :optional, "结束日期, 如: 2017-03-04"
    Api::Manage::V1::ApiController.page_params(api)
  end
  def server_connection_logs
    requires! :id, type: Integer

    user = User.find(params[:id])
    logs = user.node_connection_server_logs.recent
    logs = logs.between_date(params[:start_at].to_date, params[:end_at].to_date) if params[:start_at].present? && params[:end_at].present?
    @logs = logs.includes(:node).page(param_page).per(param_limit)
  end

  swagger_api :promo_logs do |api|
    summary "帐号详情页 - 邀请日志"
    notes <<-EOF
    此接口没有日期筛选，因为一个用户的邀请日志不会太多

    id: 日志ID
    user_id: 被邀请用户ID
    username: 被邀请用户名
    promoter_coins: 邀请人获得钻石数
    user_coins: 被邀请用户获得的钻石数
    created_at: 接受邀请时间
    EOF
    Api::Manage::V1::ApiController.common_params(api)
    param :path, :id, :integer, :required, "用户ID"
    Api::Manage::V1::ApiController.page_params(api)
  end
  def promo_logs
    requires! :id, type: Integer

    logs = UserPromoLog.where(promoter_id: params[:id]).recent
    @logs = logs.page(param_page).per(param_limit)
    @accepted_users = User.select(:id, :username).where(id: @logs.map(&:accepted_user_id).uniq).index_by(&:id)
  end

  swagger_api :change_logs do |api|
    summary "帐号详情页 - 信息修改日志"
    notes <<-EOF
    此接口没有日期筛选，因为一个用户的修改日志不会太多
    找回密码因为是未登录状态下进行，所以没有uuid和平台信息

    id: 日志ID
    change_type: 修改类型
    uuid: 设备UUID
    platform: 设备平台
    ip: 客户端IP
    before: 修改前(字符串，直接显示)
    after: 修改前(字符串，直接显示)
    created_at: 创建时间
    EOF
    Api::Manage::V1::ApiController.common_params(api)
    param :path, :id, :integer, :required, "用户ID"
    Api::Manage::V1::ApiController.page_params(api)
  end
  def change_logs
    requires! :id, type: Integer

    logs = ::Log::UserChangeLog.where(user_id: params[:id]).recent
    @logs = logs.page(param_page).per(param_limit)
  end

  swagger_api :node_test_request_speed_logs do |api|
    summary "帐号详情页 - http请求测速日志查询"
    notes ""
    Api::Manage::V1::ApiController.common_params(api)
    param :path, :id, :integer, :required, "用户ID"
    param :query, :request_url, :string, :optional, "http请求url"
    param :query, :start_at, :string, :optional, "日期筛选开始时间"
    param :query, :end_at, :string, :optional, "日期筛选结束时间"
  end
  def node_test_request_speed_logs

    start_at = params[:start_at] || Time.now.beginning_of_day
    end_at = params[:end_at] || Time.now.end_of_day
    logs = Mongo::NodeTestHttpRequestSpeedLog.where(user_id: params[:id])
      .order(created_at: :desc, request_url: :desc)
      .where(:created_at.gte => start_at, :created_at.lte => end_at)
      .page(param_page).per(param_limit)

    @request_url_list = logs.pluck(:request_url).uniq
    logs = logs.where(request_url: params[:request_url]) if params[:request_url].present?
    @logs = logs.page(param_page).per(param_limit)
  end

  swagger_api :node_test_download_speed_logs do |api|
    summary "帐号详情页 - 下载测速日志查询"
    notes ""
    Api::Manage::V1::ApiController.common_params(api)
    param :path, :id, :integer, :required, "用户ID"
    param :query, :download_url, :string, :optional, "下载url"
    param :query, :start_at, :string, :optional, "日期筛选开始时间"
    param :query, :end_at, :string, :optional, "日期筛选结束时间"
  end
  def node_test_download_speed_logs

    start_at = params[:start_at] || Time.now.beginning_of_day
    end_at = params[:end_at] || Time.now.end_of_day
    logs = Mongo::NodeTestDownloadSpeedLog.where(user_id: params[:id])
      .order(created_at: :desc, download_url: :desc)
      .where(:created_at.gte => start_at, :created_at.lte => end_at)
      .page(param_page).per(param_limit)

    @download_url_list = logs.pluck(:download_url).uniq
    logs = logs.where(download_url: params[:download_url]) if params[:download_url].present?
    @logs = logs.page(param_page).per(param_limit)
  end

end

class Api::Manage::V1::FeedbackShortcutReplyCategoriesController < Api::Manage::V1::ApiController

  # 设置权限
  include PermissionFilterable
  set_module_group_name :user_manage

  swagger_controller :feedback_shortcut_reply_categories, "反馈快捷回复分类"

  swagger_api :index do |api|
    summary "反馈快捷回复分类管理 - 列表"
    notes ""
    Api::Manage::V1::ApiController.common_params(api)
  end
  def index
    @items = FeedbackShortcutReplyCategory.all
  end

  swagger_api :create do |api|
    summary "反馈快捷回复分类管理 - 创建"
    notes ""
    Api::Manage::V1::ApiController.common_params(api)
    param :form, :name, :string, :required, "快捷回复分类名"
  end
  def create
    requires! :name, type: String

    @item = FeedbackShortcutReplyCategory.create(name: params[:name])
    error_detail!(@item) if !@item.save
  end

  swagger_api :update do |api|
    summary "反馈快捷回复分类管理 - 更新"
    notes ""
    Api::Manage::V1::ApiController.common_params(api)
    param :path, :id, :integer, :required, "快捷回复分类的ID"
    param :form, :name, :string, :optional, "快捷回复分类名"
  end
  def update
    requires! :id, type: Integer

    @item = FeedbackShortcutReplyCategory.find(params[:id])
    @item.name = params[:name] if params[:name].present?
    error_detail!(@item) if !@item.save
  end

  swagger_api :destroy do |api|
    summary "反馈快捷回复分类管理 - 删除"
    notes "删除时需要弹出提示「删除分类将删除其下的所有快捷回复，是否要执行此操作？」"
    Api::Manage::V1::ApiController.common_params(api)
    param :path, :id, :integer, :required, "快捷回复分类的ID"
  end
  def destroy
    requires! :id, type: Integer
    @item = FeedbackShortcutReplyCategory.find(params[:id])
    @item.destroy if @item.present?
  end

end

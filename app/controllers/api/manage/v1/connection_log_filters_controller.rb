class Api::Manage::V1::ConnectionLogFiltersController < Api::Manage::V1::ApiController

  # 设置权限
  include PermissionFilterable
  set_module_group_name :system_setting

  before_action :set_item, only: [:show, :update, :destroy]

  swagger_controller :connection_log_filters, "去向日志过滤项"

  swagger_api :index do |api|
    summary "参数配置 - 去向日志过滤 - 列表"
    notes ""
    Api::Manage::V1::ApiController.common_params(api)
    Api::Manage::V1::ApiController.page_params(api)
  end
  def index
    items = ConnectionLogFilter.order(id: :asc)
    @items = items.page(param_page).per(param_limit)
  end

  swagger_api :create do |api|
    summary "参数配置 - 去向日志过滤 - 创建"
    notes ""
    Api::Manage::V1::ApiController.common_params(api)
    param :form, :rule, :string, :required, "过滤规则"
  end
  def create
    requires! :rule, type: String

    @item = ConnectionLogFilter.new(allowed_params)
    error_detail!(@item) if !@item.save
  end

  swagger_api :show do |api|
    summary "参数配置 - 去向日志过滤 - 单个获取"
    notes ""
    Api::Manage::V1::ApiController.common_params(api)
    param :path, :id, :integer, :required, "ID"
  end
  def show
    requires! :id, type: Integer
  end

  swagger_api :update do |api|
    summary "参数配置 - 去向日志过滤 - 更新"
    notes ""
    Api::Manage::V1::ApiController.common_params(api)
    param :path, :id, :integer, :required, "ID"
    param :form, :rule, :string, :required, "过滤规则"
  end
  def update
    requires! :id, type: Integer
    error_detail!(@item) if !@item.update(allowed_params)
  end

  swagger_api :destroy do |api|
    summary "参数配置 - 去向日志过滤 - 删除"
    notes ""
    Api::Manage::V1::ApiController.common_params(api)
    param :path, :id, :integer, :required, "ID"
  end
  def destroy
    requires! :id, type: Integer
    error_detail!(@item) if !@item.destroy
  end

  private

  def allowed_params
    params.permit(:rule)
  end

  def set_item
    @item = ConnectionLogFilter.find(params[:id])
  end

end

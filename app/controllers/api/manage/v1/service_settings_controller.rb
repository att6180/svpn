class Api::Manage::V1::ServiceSettingsController < Api::Manage::V1::ApiController

  # 设置权限
  include PermissionFilterable
  set_module_group_name :system_setting

  swagger_controller :service_settings, "系统参数配置"

  swagger_api :index do |api|
    summary "系统参数配置 - 列表"
    notes ""
    Api::Manage::V1::ApiController.common_params(api)
  end
  def index
    @settings = Setting.get_all
  end

  swagger_api :update_multiple do |api|
    summary "系统参数配置 - 更新"
    notes "支持批量更新设置项"
    Api::Manage::V1::ApiController.common_params(api)
    param :form, :settings, :string, :required, "设置项，提交时使用json方式提交如: { checkin_present_time: 2 }"
  end
  def update_multiple
    requires! :settings, type: String

    begin
      settings = JSON.parse(params[:settings])
    rescue
      error!('参数解析失败') and return
    end

    error!("参数验证失败，请刷新后重试") and return if settings["setting_verify"] != Setting.setting_verify

    modify_settings = []
    settings.each do |key, value|
      if Setting[key] != value
        Setting[key] = value
        modify_settings << key
      end
    end
    # 更新验证字符
    Setting.setting_verify = SecureRandom.urlsafe_base64(nil, false)
    # 增加管理员操作日志
    current_admin.create_log( key: 'update_system_settings', params: { settings: modify_settings.join(',') })
    @settings = Setting.get_all
  end

end

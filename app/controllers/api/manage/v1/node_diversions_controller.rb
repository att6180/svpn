class Api::Manage::V1::NodeDiversionsController < Api::Manage::V1::ApiController

  include PermissionFilterable
  set_module_group_name :node_manage

  before_action :set_node_diversion, only: [:update, :destroy]

  swagger_controller :node_diversions, "运营商地域分流配置"

  swagger_api :index do |api|
    summary "服务器群组管理 - 服务器分流 - 指定地域分流 - 列表"
    notes ""
    Api::Manage::V1::ApiController.common_params(api)
    Api::Manage::V1::ApiController.page_params(api)
  end
  def index
    optional! :page, type: Integer
    optional! :limit, type: Integer

    @node_diversions = NodeDiversion.includes(:node_type, :node_region)
      .page(param_page).per(param_limit)
  end

  swagger_api :create do |api|
    summary "服务器群组管理 - 服务器分流 - 指定地域分流 - 创建"
    notes ""
    Api::Manage::V1::ApiController.common_params(api)
    param :form, :province, :string, :required, "省份"
    param :form, :city, :string, :optional, "城市"
    param :form, :node_region_id, :integer, :required, "区域ID"
    param :form, :node_type_id, :integer, :required, "服务类型ID"
    param :form, :is_enabled, :string, :optional, "是否启用, true|false"
  end
  def create
    requires! :province, type: String
    requires! :node_region_id, type: Integer
    requires! :node_type_id, type: Integer

    @node_diversion = NodeDiversion.new(allowed_params)
    error_detail!(@node_diversion) if !@node_diversion.save
  end

  swagger_api :update do |api|
    summary "服务器群组管理 - 服务器分流 - 指定地域分流 - 更新"
    notes ""
    Api::Manage::V1::ApiController.common_params(api)
    param :path, :id, :integer, :required, "地域分流ID"
    param :form, :province, :string, :optional, "省份"
    param :form, :city, :string, :optional, "城市"
    param :form, :node_region_id, :integer, :optional, "区域ID"
    param :form, :node_type_id, :integer, :optional, "服务类型ID"
    param :form, :is_enabled, :string, :optional, "是否启用, true|false"
  end
  def update
    requires! :id, type: Integer

    error_detail!(@node_diversion) if !@node_diversion.update(allowed_params)
  end

  private

  def set_node_diversion
    @node_diversion = NodeDiversion.find(params[:id])
  end

  def allowed_params
    params.permit(
      :province,
      :city,
      :node_region_id,
      :node_type_id,
      :is_enabled
    )
  end
end

class Api::Manage::V1::NodesController < Api::Manage::V1::ApiController

  before_action :set_node, only: [:show, :update, :destroy]

  swagger_controller :nodes, "服务器配置"

  swagger_api :index do |api|
    summary "服务器群组管理 - 服务器配置 - 列表"
    notes ""
    Api::Manage::V1::ApiController.common_params(api)
    param :query, :node_type_id, :integer, :optional, "显示指定服务类型的服务器列表，不提供此参数则返回全部"
    param :query, :node_region_id, :integer, :optional, "显示指定区域的服务器列表，不提供此参数则返回全部"
    param :query, :node_isp_id, :integer, :optional, "显示指定代理商标识的服务器列表，不提供此参数则返回全部"
    param :query, :level, :integer, :optional, "服务器等级（1级或2级), 如: 1"
    param :query, :status, :string, :optional, "节点状态, enabled:启用, disabled:禁用, standby:备用, deprecated:弃用"
    param :query, :is_domestic, :string, :optional, "是否是国内, true|false"
    param :query, :node_id, :integer, :optional, "按服务器ID搜索, 如: 2"
    param :query, :types, :string, :optional, "线路类型, covered: 硬核直连和的定制专线, smart: 硬核直连, customization: 定制专线"
    Api::Manage::V1::ApiController.page_params(api)
  end
  def index
    can_access?(:node_manage)

    nodes = Node.the_where({
      id: params[:node_id],
      node_type_id: params[:node_type_id],
      node_region_id: params[:node_region_id],
      node_isp_id: params[:node_isp_id],
      level: params[:level],
      status: params[:status],
      types: params[:types]
    })
    nodes = nodes.where(is_domestic: params[:is_domestic] == 'true') if params[:is_domestic].present?
    nodes = nodes.unscope(where: :node_isp_id).where("node_isp_id is null") if params[:node_isp_id] == "-1"
    @nodes = nodes
    .includes(:node_region, :user_group, :node_type, :node_isp, :node_out_region)
    .order(status: :desc, node_type_id: :asc, id: :asc)
    .page(param_page).per(param_limit)
  end

  swagger_api :create do |api|
    summary "服务器群组管理 - 服务器配置 - 创建"
    notes ""
    Api::Manage::V1::ApiController.common_params(api)
    param :form, :id, :integer, :required, "服务器ID"
    param :form, :node_type_id, :integer, :required, "服务器类型ID"
    param :form, :node_region_id, :integer, :required, "区域ID"
    param :form, :node_isp_id, :integer, :optional, "代理商标识ID"
    param :form, :node_region_out_id, :integer, :optional, "出口区域ID"
    param :form, :name, :string, :required, "名称"
    param :form, :url, :string, :required, "地址"
    param :form, :port, :integer, :required, "端口"
    param :form, :password, :string, :required, "密码"
    param :form, :encrypt_method, :string, :required, "加密方式"
    param :form, :description, :string, :optional, "描述"
    param :form, :max_connections_count, :integer, :optional, "最大连接数"
    param :form, :is_domestic, :string, :optional, "是否是国内线路 true|false"
    param :form, :level, :integer, :optional, "线路级别 1|2"
    param :form, :status, :string, :optional, "节点状态, enabled:启用, disabled:禁用, standby:备用, deprecated:弃用"
    param :form, :types, :string, :optional, "线路类型, covered: 硬核直连和的定制专线, smart: 硬核直连, customization: 定制专线"
  end
  def create
    requires! :id, type: Integer
    requires! :node_type_id, type: Integer
    requires! :node_region_id, type: Integer
    requires! :name, type: String
    requires! :url, type: String
    requires! :port, type: Integer
    requires! :password, type: String
    requires! :encrypt_method, type: String

    can_access?(:node_manage)

    @node = Node.new(allowed_params)
    error_detail!(@node) if !@node.save
  end

  swagger_api :show do |api|
    summary "服务器群组管理 - 服务器配置 - 单个获取"
    notes ""
    Api::Manage::V1::ApiController.common_params(api)
    param :path, :id, :integer, :required, "服务器ID"
  end
  def show
    requires! :id, type: Integer
    can_access?(:node_manage)
  end

  swagger_api :update do |api|
    summary "服务器群组管理 - 服务器配置 - 更新"
    notes ""
    Api::Manage::V1::ApiController.common_params(api)
    param :path, :id, :integer, :required, "服务器ID"
    param :form, :node_type_id, :integer, :optional, "服务器类型ID"
    param :form, :node_region_id, :integer, :optional, "服务器区域ID"
    param :form, :node_isp_id, :integer, :optional, "代理商标识ID"
    param :form, :node_region_out_id, :integer, :optional, "出口区域ID"
    param :form, :name, :string, :optional, "服务器名"
    param :form, :url, :string, :optional, "服务器地址"
    param :form, :port, :integer, :optional, "端口"
    param :form, :password, :string, :optional, "密码"
    param :form, :encrypt_method, :string, :optional, "加密方式"
    param :form, :description, :string, :optional, "服务器描述"
    param :form, :max_connections_count, :integer, :optional, "服务器最大连接数"
    param :form, :is_domestic, :string, :optional, "是否是国内线路 true|false"
    param :form, :level, :integer, :optional, "线路级别 1|2"
    param :form, :status, :string, :optional, "节点状态, enabled:启用, disabled:禁用, standby:备用, deprecated:弃用"
    param :form, :types, :string, :optional, "线路类型, covered: 硬核直连和的定制专线, smart: 硬核直连, customization: 定制专线"
  end
  def update
    requires! :id, type: Integer

    can_access?(:node_manage)

    error_detail!(@node) if !@node.update(allowed_params.except(:id))
  end

  swagger_api :batch_update_status do |api|
    summary "服务器信息 - 批量更新服务器状态"
    notes ""
    Api::Manage::V1::ApiController.common_params(api)
    param :form, :ids, :string, :required, "服务器ID列表，多个使用英文,号分隔"
    param :form, :status, :string, :required, "节点状态, enabled:启用, disabled:禁用, standby:备用, deprecated:弃用"
  end
  def batch_update_status
    requires! :ids, type: String
    requires! :status, type: String

    can_access?(:user_behaviour)

    nodes = Node.where(id: params[:ids].split(','))
    if nodes.any?
      nodes.each do |node|
        # 这里不做批量操作是为了触发回调，更新缓存状态
        node.update(status: params[:status])
      end
    end
  end

  swagger_api :batch_update_ip do |api|
    summary "服务器信息 - 批量更新服务器IP"
    notes <<-EOF
    此功能用于让运维可以快速批量更新多台服务器的IP地址，参数来自excel表的粘贴数据
    当管理员点击「更新」按钮后，请在页面弹出「确定执行此操作吗？」的提示框，做二次确认。

    显示在页面上的提示信息:
    此操作将批量更新指定代理服务器的IP地址配置，请将EXCEL表中的数据粘贴到下方多行文本框中并点击更新，
    此操作不可撤销，请谨慎操作。

    EOF
    Api::Manage::V1::ApiController.common_params(api)
    param :form, :data, :string, :required, "包含ID和IP的数据，该数据从EXCEL表中复制而来，第一列为ID，第二列为IP"
  end
  def batch_update_ip
    requires! :data, type: String

    can_access?(:user_behaviour)

    data = params[:data]
    logger.info data
    error!("无数据可更新") and return if data.blank?
    error!("数据格式验证失败") and return if !data.match(/[^0-9\.\n\t]+/).nil?
    data_lines = data.split(/\n+/)
    data_lines.each do |line|
      id, ip = line.strip.split(/\s+/)
      if id.to_i > 0 and Utils::IP.valid_ipv4?(ip)
        Node.where(id: id).update(url: ip)
      end
    end
  end

  swagger_api :online_users do |api|
    summary "服务器信息 - 当前在线人数"
    notes ""
    param :query, :node_region_id, :integer, :optional, "显示指定区域的服务器列表，不提供此参数则返回全部"
    param :form, :node_isp_id, :integer, :optional, "代理商标识ID"
    param :query, :is_domestic, :string, :optional, "是否是国内, true|false"
    param :query, :types, :string, :optional, "线路类型, covered: 硬核直连和的定制专线, smart: 硬核直连, customization: 定制专线"
    Api::Manage::V1::ApiController.common_params(api)
    Api::Manage::V1::ApiController.page_params(api)
  end
  def online_users
    can_access?(:user_behaviour)

    nodes = Node.enabled.level1
      .the_where({
        node_region_id: params[:node_region_id],
        node_isp_id: params[:node_isp_id],
        types: params[:types]
      })
      .includes(node_region: :node_type)
      .order(node_type_id: :asc, is_domestic: :desc, node_isp_id: :desc, id: :asc)

    nodes = nodes.where(is_domestic: params[:is_domestic] == 'true') if params[:is_domestic].present?
    nodes = nodes.unscope(where: :node_isp_id).where("node_isp_id is null") if params[:node_isp_id] == "-1"

    node_types = []
    isp_online_users = []
    NodeType.cache_list.each do |type|

      node_types << {
        node_type_name: type.name,
        online_user_count: nodes.where(node_type_id: type.id).sum(&:cache_online_users)
      }

      n = nodes.where(node_isp_id: nil, node_type_id: type.id)
      isp_online_users << {
        node_isp_name: I18n.t("isp_empty_name"),
        node_type_name: type.name,
        node_count: n.count,
        online_user_count: n.sum(&:cache_online_users)
      }

      NodeIsp.cache_list.each do |isp|
        n = nodes.where(node_type_id: type.id, node_isp_id: isp.id)
        isp_online_users << {
          node_isp_name: isp.name,
          node_type_name: type.name,
          node_count: n.count,
          online_user_count: n.sum(&:cache_online_users)
        }
      end
    end

    @node_types = node_types
    @isp_online_users = isp_online_users
    @total_nodes = nodes
    @nodes = nodes.page(param_page).per(param_limit)
  end

  swagger_api :online_details do |api|
    summary "服务器信息 - 在线用户详情"
    notes "点击指定统计数字，得到构成此数量的每条信息详情"
    Api::Manage::V1::ApiController.common_params(api)
    param :query, :node_region_id, :integer, :optional, "服务器区域ID，与服务器ID必须指定一个"
    param :query, :node_id, :integer, :optional, "服务器ID，与服务器区域ID必须指定一个"
    param :query, :page, :integer, :optional, "页码，默认: 1"
    param :query, :limit, :integer, :optional, "每页显示的记录数, 默认: 25"
  end
  def online_details
    can_access?(:user_behaviour)

    users = NodeOnlineUser.includes(:user).page(param_page).per(param_limit)
    if params[:node_region_id].present?
      @users = users.by_node_region(params[:node_region_id])
    elsif params[:node_id].present?
      @users = users.by_node(params[:node_id])
    else
      error!('服务器区域ID或服务器ID必须指定一个')
    end
  end

  swagger_api :details do |api|
    summary "服务器信息 - 服务器信息"
    notes ""
    Api::Manage::V1::ApiController.common_params(api)
    param :query, :page, :integer, :optional, "页码，默认: 1"
    param :query, :limit, :integer, :optional, "每页显示的记录数, 默认: 25"
  end
  def details
    can_access?(:user_behaviour)

    @nodes = Node.enabled.order(url: :asc, node_region_id: :asc).includes(:node_region, :node_addition).page(param_page).per(param_limit)
  end

  swagger_api :region_details do |api|
    summary "服务器信息 - 地域连接人数详情"
    notes "点击线路名称后出现的弹窗"
    Api::Manage::V1::ApiController.common_params(api)
    param :query, :node_id, :integer, :required, "服务器ID"
    Api::Manage::V1::ApiController.page_params(api)
  end
  def region_details
    requires! :node_id, type: Integer

    can_access?(:user_behaviour)

    @logs = NodeOnlineUser.where(node_id: params[:node_id]).group_region.page(param_page).per(param_limit)
  end

  swagger_api :region_user_details do |api|
    summary "服务器信息 - 地域连接人数详情 - 在线人数详情"
    notes "点击线路名称出现的弹窗后，再点击弹窗中的在线人数数字出现的弹窗"
    Api::Manage::V1::ApiController.common_params(api)
    param :query, :node_id, :integer, :required, "服务器ID"
    param :query, :ip_country, :string, :required, "区域国家"
    param :query, :ip_province, :string, :optional, "区域省/州"
    param :query, :ip_city, :string, :optional, "区域城市，没有就留空"
    Api::Manage::V1::ApiController.page_params(api)
  end
  def region_user_details
    requires! :node_id, type: Integer
    requires! :ip_country, type: String

    can_access?(:user_behaviour)

    @users = NodeOnlineUser.by_node(params[:node_id])
      .by_region(params[:ip_country], params[:ip_province], params[:ip_city])
      .includes(:user).page(param_page).per(param_limit)
  end

  swagger_api :online_users_stat do |api|
    summary "服务器信息 - 当前在线人数 - 历史记录"
    notes ""
    Api::Manage::V1::ApiController.common_params(api)
    param :query, :start_at, :string, :optional, "开始日期, 如: 2017-12-03, 默认为当前日期的前30天"
    param :query, :end_at, :string, :optional, "结束日期, 如: 2018-01-04，默认为当月当天"
  end
  def online_users_stat
    can_access?(:user_behaviour)

    end_at = params[:end_at].present? ? params[:end_at].to_date : Time.now.to_date
    start_at = params[:start_at].present? ? params[:start_at].to_date : end_at - 30.days

    error!("起始时间和终止时间不能超过60天") if (end_at - start_at).to_i > 60

    @logs = ProxyServerOnlineUsersDayStat.order(stat_date: :desc)
      .between_date(start_at, end_at)
    @node_types = NodeType.cache_list
  end

  swagger_api :online_users_stat_details do |api|
    summary "服务器信息 - 当前在线人数 - 历史记录 - 日期详情"
    notes ""
    Api::Manage::V1::ApiController.common_params(api)
    param :query, :stat_date, :string, :required, "统计日期"
    param :query, :node_type_id, :integer, :optional, "服务类型id, 不提供此参数则查询全部，否则查询指定服务类型"
    Api::Manage::V1::ApiController.page_params(api)
  end
  def online_users_stat_details
    requires! :stat_date, type: String

    can_access?(:user_behaviour)

    logs = ProxyServerOnlineUsersLog
      .by_date(params[:stat_date].to_date)
      .group_by_hour_minute
      .page(param_page).per(param_limit)
    logs = logs.where(node_type_id: params[:node_type_id]) if params[:node_type_id].present?
    @logs = logs
  end

  swagger_api :online_users_stat_second_details do |api|
    summary "服务器信息 - 当前在线人数 - 历史记录 - 日期详情 - 总计在线详情"
    notes ""
    Api::Manage::V1::ApiController.common_params(api)
    param :query, :stat_date, :string, :required, "统计日期, 如: 2017-08-09"
    param :query, :time, :string, :required, "统计时间(小时:分), 如: 15:30"
    param :query, :node_type_id, :integer, :optional, "服务类型id, 不提供此参数则查询全部，否则查询指定服务类型"
    Api::Manage::V1::ApiController.page_params(api)
  end
  def online_users_stat_second_details
    requires! :stat_date, type: String
    requires! :time, type: String

    can_access?(:user_behaviour)

    logs = ProxyServerOnlineUsersLog
      .by_date(params[:stat_date].to_date)
      .by_hour_minute(params[:time])
      .order("created_at ASC")
      .includes(:node)
      .page(param_page).per(param_limit)
    logs = logs.where(node_type_id: params[:node_type_id]) if params[:node_type_id].present?
    @logs = logs
  end

  swagger_api :proxy_server_status_stat_chart do |api|
    summary "服务器信息 - 服务器信息 - 历史记录 - 图表"
    notes ""
    Api::Manage::V1::ApiController.common_params(api)
    param :query, :start_at, :string, :optional, "开始日期, 如: 2017-07-01, 默认为当月第一天"
    param :query, :end_at, :string, :optional, "结束日期, 如: 2017-03-01，默认为当月当天"
    param :query, :node_id, :integer, :required, "节点ID, 如: 2，默认为服务器1级代理的第一个服务器的ID, 从服务器列表获取接口获得"
  end
  def proxy_server_status_stat_chart
    optional! :start_at, type: String
    optional! :end_at, type: String
    requires! :node_id, type: Integer

    can_access?(:user_behaviour)

    start_at = params[:start_at].present? ? params[:start_at].to_date : Time.now.to_date.beginning_of_month
    end_at = params[:end_at].present? ? params[:end_at].to_date : Time.now.to_date

    node = Node.find_by(id: params[:node_id])
    if node.blank?
      error!('节点不存在') and return
    end

    @logs = ProxyServerStatusDayStat.order(stat_date: :asc)
      .between_date(start_at, end_at)
      .where(node_id: node.id)
  end

  swagger_api :proxy_server_status_stat do |api|
    summary "服务器信息 - 服务器信息 - 历史记录"
    notes ""
    Api::Manage::V1::ApiController.common_params(api)
    param :query, :start_at, :string, :optional, "开始日期, 如: 2017-07-01, 默认为当月第一天"
    param :query, :end_at, :string, :optional, "结束日期, 如: 2017-03-01，默认为当月当天"
    param :query, :node_id, :integer, :required, "节点ID, 如: 2，默认为服务器1级代理的第一个服务器的ID, 从服务器列表获取接口获得"
    Api::Manage::V1::ApiController.page_params(api)
  end
  def proxy_server_status_stat
    optional! :start_at, type: String
    optional! :end_at, type: String
    requires! :node_id, type: Integer

    can_access?(:user_behaviour)

    start_at = params[:start_at].present? ? params[:start_at].to_date : Time.now.to_date.beginning_of_month
    end_at = params[:end_at].present? ? params[:end_at].to_date : Time.now.to_date

    node = Node.find_by(id: params[:node_id])
    if node.blank?
      error!('节点不存在') and return
    end

    @logs = ProxyServerStatusDayStat.order(stat_date: :desc)
      .between_date(start_at, end_at)
      .where(node_id: node.id)
      .page(param_page).per(param_limit)
  end

  swagger_api :proxy_server_status_stat_details do |api|
    summary "服务器信息 - 服务器信息 - 历史记录 - 日期详情"
    notes ""
    Api::Manage::V1::ApiController.common_params(api)
    param :query, :stat_date, :string, :required, "统计日期, 如: 2017-07-01"
    param :query, :node_id, :integer, :required, "节点ID, 如: 2，默认为服务器1级代理的第一个服务器的ID, 从服务器列表获取接口获得"
    Api::Manage::V1::ApiController.page_params(api)
  end
  def proxy_server_status_stat_details
    requires! :stat_date, type: String
    optional! :node_id, type: Integer
    optional! :page, type: Integer
    optional! :limit, type: Integer

    can_access?(:user_behaviour)

    logs = ProxyServerStatusLog.recent
    logs = logs.where(node_id: params[:node_id]) if params[:node_id].present?
    logs = logs.by_date(params[:stat_date].to_date) if params[:stat_date].present?
    @logs = logs.includes(:node).page(param_page).per(param_limit)
  end

  private

  def allowed_params
    params.permit(
      :id,
      :node_type_id,
      :node_region_id,
      :node_isp_id,
      :node_region_out_id,
      :name,
      :url,
      :status,
      :description,
      :port,
      :password,
      :encrypt_method,
      :max_connections_count,
      :user_group_id,
      :is_domestic,
      :level,
      :types
    )
  end

  def set_node
    @node = Node.find(params[:id])
  end
end

class Api::Manage::V1::CustomerServicesController < Api::Manage::V1::ApiController

  include PermissionFilterable
  set_module_group_name :user_manage

  before_action :set_cs, only: [:update, :destroy]

  swagger_controller :feedbacks, "客服配置"

  swagger_api :index do |api|
    summary "用户管理 - 用户反馈 - 客服配置 - 列表"
    notes ""
    Api::Manage::V1::ApiController.common_params(api)
    Api::Manage::V1::ApiController.page_params(api)
    param :query, :page, :integer, :optional, "页码，默认: 1"
    param :query, :limit, :integer, :optional, "每页显示的记录数, 默认: 25"
  end
  def index
    @customer_services = CustomerService.page(param_page).per(param_limit)
  end

  swagger_api :create do |api|
    summary "用户管理 - 用户反馈 - 客服配置 - 创建"
    notes ""
    Api::Manage::V1::ApiController.common_params(api)
    param :form, :name, :string, :required, "客服名称"
    param :form, :potato_url, :string, :required, "客服账号"
    param :form, :status, :string, :optional, "在线状态，true|false，默认 true"
  end
  def create
    requires! :name, type: String
    requires! :potato_url, type: String
    optional! :status, type: String

    @cs = CustomerService.new(allowed_params)
    error_detail!(@cs) if !@cs.save
  end

  swagger_api :update do |api|
    summary "用户管理 - 用户反馈 - 客服配置 - 更新"
    notes ""
    Api::Manage::V1::ApiController.common_params(api)
    param :path, :id, :integer, :required, "客服id"
    param :form, :name, :string, :required, "客服名称"
    param :form, :potato_url, :string, :required, "客服账号"
    param :form, :status, :string, :optional, "在线状态，true|false，默认 true"
  end
  def update
    requires! :id, type: Integer
    optional! :name, type: String
    optional! :potato_url, type: String
    optional! :status, type: String

    ActiveRecord::Base.transaction do
      @cs.assign_attributes(allowed_params)
      @cs.update_alloc_count_cache!
      error_detail!(@cs) if !@cs.save
    end
  end

  swagger_api :destroy do |api|
    summary "用户管理 - 用户反馈 - 客服配置 - 删除"
    notes ""
    Api::Manage::V1::ApiController.common_params(api)
    param :path, :id, :integer, :required, "客服id"
  end
  def destroy
    requires! :id, type: Integer

    error_detail!(@cs) if !@cs.destroy
  end

  private

  def allowed_params
    params.permit(
      :name,
      :potato_url,
      :status
    )
  end

  def set_cs
    @cs = CustomerService.find(params[:id])
  end

end

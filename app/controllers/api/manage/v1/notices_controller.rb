class Api::Manage::V1::NoticesController < Api::Manage::V1::ApiController

  include PermissionFilterable
  set_module_group_name :system_setting

  before_action :set_notice, only: [:show, :update, :destroy]

  swagger_controller :notices, "公告管理"

  swagger_api :index do |api|
    summary "公告管理 - 列表"
    notes ""
    Api::Manage::V1::ApiController.common_params(api)
    param :query, :username, :string, :optional, "用户名"
    Api::Manage::V1::ApiController.page_params(api)
  end
  def index
    optional! :username, type: String

    @notices = V2::Notice
       .the_where('user_names-like': params[:username])
       .order(notice_type: :asc, id: :asc)
       .page(param_page).per(param_limit)
  end

  swagger_api :create do |api|
    summary "公告管理 - 创建"
    notes ""
    Api::Manage::V1::ApiController.common_params(api)
    param :form, :user_names, :string, :optional, "指定账号名称，以英文状态逗号分割"
    param :form, :platform, :string, :optional, "平台"
    param :form, :app_channel, :string, :optional, "渠道"
    param :form, :app_version, :string, :optional, "版本"
    param :form, :notice_type, :string, :required, "消息类型 notice: 公告/marquee: 跑马灯/alert: 即时弹窗"
    param :form, :open_type, :string, :required, "打开方式 nothing: 无跳转/webview: webview/browser: 浏览器/inapp: 应用内跳转"
    param :form, :button_text, :string, :optional, "跳转按钮文字"
    param :form, :link_url, :string, :optional, "跳转url"
    param :form, :alert_text, :string, :optional, "弹窗文字"
    param :form, :marquee_text, :integer, :optional, "跑马灯文字"
    param :form, :description, :string, :required, "备注"
  end
  def create
    requires! :notice_type, type: String
    requires! :open_type, type: String
    requires! :description, type: String

    @notice = V2::Notice.new(allowed_params)
    error_detail!(@notice) if !@notice.save
  end

  swagger_api :show do |api|
    summary "公告管理 - 单个获取"
    notes ""
    Api::Manage::V1::ApiController.common_params(api)
    param :path, :id, :integer, :required, "公告ID"
  end
  def show
    requires! :id, type: Integer
  end

  swagger_api :update do |api|
    summary "公告管理 - 更新"
    notes ""
    Api::Manage::V1::ApiController.common_params(api)
    param :path, :id, :integer, :optional, "公告ID"
    param :form, :user_names, :string, :optional, "指定账号名称，以英文状态逗号分割"
    param :form, :platform, :string, :optional, "平台"
    param :form, :app_channel, :string, :optional, "渠道"
    param :form, :app_version, :string, :optional, "版本"
    param :form, :notice_type, :string, :optional, "消息类型 notice: 公告/marquee: 跑马灯/alert: 即时弹窗"
    param :form, :open_type, :string, :optional, "打开方式 nothing: 无跳转/webview: webview/browser: 浏览器/inapp: 应用内跳转"
    param :form, :button_text, :string, :optional, "跳转按钮文字"
    param :form, :link_url, :string, :optional, "跳转url"
    param :form, :alert_text, :string, :optional, "弹窗文字"
    param :form, :marquee_text, :integer, :optional, "跑马灯文字"
    param :form, :description, :string, :optional, "公告描述"
  end
  def update
    requires! :id, type: Integer

    error_detail!(@notice) if !@notice.update(allowed_params)
  end

  swagger_api :show do |api|
    summary "公告管理 - 单个获取"
    notes ""
    Api::Manage::V1::ApiController.common_params(api)
    param :path, :id, :integer, :required, "公告ID"
  end
  def show
    requires! :id, type: Integer
  end

  swagger_api :destroy do |api|
    summary "公告管理 - 单个删除"
    notes ""
    Api::Manage::V1::ApiController.common_params(api)
    param :path, :id, :integer, :required, "公告ID"
  end
  def destroy
    requires! :id, type: Integer
    
    error_detail!(@notice) if !@notice.destroy
  end

  private

  def allowed_params
    params.permit(
      :user_names,
      :platform,
      :app_channel,
      :app_version,
      :notice_type,
      :open_type,
      :button_text,
      :link_url,
      :alert_text,
      :marquee_text,
      :description
    )
  end

  def set_notice
    @notice = V2::Notice.find(params[:id])
  end
end

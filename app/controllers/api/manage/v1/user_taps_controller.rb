class Api::Manage::V1::UserTapsController < Api::Manage::V1::ApiController

  before_action :set_user_tap, only: [:update, :destroy]
  # 设置权限
  include PermissionFilterable
  set_module_group_name :user_manage

  swagger_controller :user_taps, "用户分流"

  swagger_api :index do |api|
    summary "用户分流 - 列表"
    notes ""
    Api::Manage::V1::ApiController.common_params(api)
    param :query, :page, :integer, :optional, "页码，默认: 1"
    param :query, :limit, :integer, :optional, "每页显示的记录数, 默认: 25"
    param :query, :user_id, :string, :optional, "搜索用户ID"
    param :query, :node_id, :string, :optional, "搜索服务器ID"
  end

  def index
    optional! :page, type: Integer
    optional! :limit, type: Integer
    optional! :user_id, type: Integer
    optional! :node_id, type: Integer

    if params[:user_id].present?
      user_taps = UserTap.where(user_id: params[:user_id])
    elsif params[:node_id].present?
      user_taps = UserTap.where(node_id: params[:node_id])
    else
      user_taps = UserTap.all
    end
    @user_taps = user_taps.includes(:node, :user).page(param_page).per(param_limit)
  end

  swagger_api :create do |api|
    summary "用户分流 - 新增"
    notes ""
    Api::Manage::V1::ApiController.common_params(api)
    param :form, :user_id, :integer, :required, "用户id"
    param :form, :node_id, :integer, :required, "服务器id"
  end

  def create
    requires! :user_id, type: Integer
    requires! :node_id, type: Integer

    user = User.find(allowed_params[:user_id])
    node = Node.find(allowed_params[:node_id])
    error!("请配置一级代理") and return if !node.is_level1?
    if user.present? && node.present?
      @user_tap = UserTap.new(
        user_id: allowed_params[:user_id],
        node_id: allowed_params[:node_id]
      )
      error_detail!(@user_tap) if !@user_tap.save
    else
      error!("要操作的帐号或节点不存在")
    end
  end

  swagger_api :update do |api|
    summary "用户分流 - 修改"
    notes ""
    Api::Manage::V1::ApiController.common_params(api)
    param :path, :id, :integer, :required, "节点ID"
    param :form, :user_id, :integer, :optional, "用户id"
    param :form, :node_id, :integer, :optional, "服务器id"
  end

  def update
    requires! :id, type: Integer
    
    if allowed_params[:user_id].present?
      user = User.find(allowed_params[:user_id])
      error!("要操作的帐号不存在") and return if !user.present?
    end

    if allowed_params[:node_id].present?
      node = Node.find(allowed_params[:node_id])
      error!("要操作的节点不存在") and return if !node.present?
      error!("请配置一级代理") and return if !node.is_level1?
    end
    error_detail!(@user_tap) if !@user_tap.update(allowed_params)
  end

  swagger_api :destroy do |api|
    summary "用户分流 - 删除"
    notes ""
    Api::Manage::V1::ApiController.common_params(api)
    param :path, :id, :integer, :required, "权限组id"
  end

  def destroy
    requires! :id, type: Integer

    error_detail!(@user_tap) if !@user_tap.destroy
  end

  private

  def allowed_params
    params.permit(
      :user_id,
      :node_id
    )
  end

   def set_user_tap
    @user_tap = UserTap.find(params[:id])
  end
end
class Api::Manage::V1::PlanClientTabsController < Api::Manage::V1::ApiController

  # 设置权限
  include PermissionFilterable
  set_module_group_name :charge_manage

  before_action :set_item, only: [:show, :update, :destroy]

  swagger_controller :plan_client_tabs, "套餐客户端标签"

  swagger_api :index do |api|
    summary "套餐客户端标签 - 列表"
    notes ""
    Api::Manage::V1::ApiController.common_params(api)
    param :query, :node_type_id, :string, :optional, "服务级别，对应服务类型，但增加一个自定义项, 0:钻石套餐"
    param :query, :status, :string, :optional, "状态, 0:禁用, 1:启用, 不提供此参数时则返回所有启用的记录"
    Api::Manage::V1::ApiController.page_params(api)
  end
  def index
    items = V2::PlanClientTab.all
    items = items.where(node_type_id: params[:node_type_id]) if params[:node_type_id].present?
    items = if params[:status].present?
      items.where(status: params[:status])
    else
      items.enabled
    end
    @items = items.page(param_page).per(param_limit)
  end

  swagger_api :create do |api|
    summary "套餐客户端标签 - 创建"
    notes ""
    Api::Manage::V1::ApiController.common_params(api)
    param :form, :name, :string, :required, "名称"
    param :form, :node_type_id, :string, :required, "服务级别，对应服务类型，但增加一个自定义项, 0:钻石套餐"
    param :form, :is_recommend, :string, :required, "是否为推荐, true|false"
    param :form, :description, :string, :optional, "描述"
    param :form, :status, :integer, :required, "状态, 0:禁用, 1:启用，默认为1"
  end
  def create
    requires! :name, type: String
    requires! :node_type_id, type: String
    requires! :is_recommend, type: String
    requires! :status, type: String

    @item = V2::PlanClientTab.new(allowed_params)
    error_detail!(@item) if !@item.save
  end

  swagger_api :show do |api|
    summary "套餐客户端标签 - 单个获取"
    notes ""
    Api::Manage::V1::ApiController.common_params(api)
    param :path, :id, :integer, :required, "套餐ID"
  end
  def show
    requires! :id, type: Integer
  end

  swagger_api :update do |api|
    summary "套餐客户端标签 - 更新"
    notes ""
    Api::Manage::V1::ApiController.common_params(api)
    param :path, :id, :integer, :optional, "套餐ID"
    param :form, :name, :string, :optional, "名称"
    param :form, :node_type_id, :string, :optional, "服务级别，对应服务类型，但增加一个自定义项, 0:钻石套餐"
    param :form, :is_recommend, :string, :optional, "是否为推荐, true|false"
    param :form, :description, :string, :optional, "描述"
    param :form, :status, :integer, :optional, "状态, 0:禁用, 1:启用，默认为1"
  end
  def update
    error_detail!(@item) if !@item.update(allowed_params)
  end

  private

  def allowed_params
    params.permit(
      :name,
      :node_type_id,
      :is_recommend,
      :status,
      :description
    )
  end

  def set_item
    @item = V2::PlanClientTab.find(params[:id])
  end

end

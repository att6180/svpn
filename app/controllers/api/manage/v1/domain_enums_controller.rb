class Api::Manage::V1::DomainEnumsController < Api::Manage::V1::ApiController

  before_action :set_enum, only: [:update, :destroy]

  swagger_controller :domain_enums, "域名描述配置"

  swagger_api :index do |api|
    summary "域名描述配置 - 列表"
    notes ""
    Api::Manage::V1::ApiController.common_params(api)
    param :query, :page, :integer, :optional, "页码，默认: 1"
    param :query, :limit, :integer, :optional, "每页显示的记录数, 默认: 25"
  end
  def index
    optional! :page, type: Integer
    optional! :limit, type: Integer

    @enums = DomainEnum.sorted_by_domain.page(param_page).per(param_limit)
  end

  swagger_api :create do |api|
    summary "域名描述配置 - 创建"
    notes ""
    Api::Manage::V1::ApiController.common_params(api)
    param :form, :domain, :string, :required, "域名，一级域名，如: twitter.com (不含http)"
    param :form, :description, :string, :optional, "描述, 如: 推特"
  end
  def create
    requires! :domain, type: String
    optional! :description, type: String

    @enum = DomainEnum.new(allowed_params)
    error_detail!(@enum) if !@enum.save
  end

  swagger_api :update do |api|
    summary "域名描述配置 - 更新"
    notes ""
    Api::Manage::V1::ApiController.common_params(api)
    param :path, :id, :integer, :required, "ID"
    param :form, :domain, :string, :required, "域名，一级域名，如: twitter.com (不含http)"
    param :form, :description, :string, :optional, "描述, 如: 推特"
  end
  def update
    requires! :id, type: Integer
    requires! :domain, type: String
    optional! :description, type: String

    error_detail!(@enum) if !@enum.update(allowed_params)
  end

  swagger_api :destroy do |api|
    summary "域名描述配置 - 删除"
    notes ""
    Api::Manage::V1::ApiController.common_params(api)
    param :path, :id, :integer, :required, "ID"
  end
  def destroy
    requires! :id, type: Integer

    error_detail!(@enum) if !@enum.destroy
  end

  swagger_api :quick_update do |api|
    summary "域名描述配置 - 快捷更新"
    notes "不存在则添加，存在则编辑"
    Api::Manage::V1::ApiController.common_params(api)
    param :form, :domain, :string, :required, "域名，一级域名，如: twitter.com (不含http)"
    param :form, :description, :string, :optional, "描述, 如: 推特"
  end
  def quick_update
    requires! :domain, type: String
    optional! :description, type: String

    @enum = DomainEnum.find_by(domain: params[:domain])
    result = false
    if @enum.present?
      result = @enum.update(description: params[:description])
    else
      @enum = DomainEnum.new(domain: params[:domain], description: params[:description])
      result = @enum.save
    end
    error_detail!(@enum) if !result
  end

  private

  def allowed_params
    params.permit(
      :domain,
      :description
    )
  end

  def set_enum
    @enum = DomainEnum.find(params[:id])
  end

end

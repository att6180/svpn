class Api::Manage::V1::NodeConnectionLogsController < Api::Manage::V1::ApiController

  # 设置权限
  include PermissionFilterable
  set_module_group_name :user_base_info

  swagger_controller :node_connection_logs, "服务器连接日志"

  swagger_api :count_scope_index do |api|
    summary "用户行为 - 用户使用情况 - 列表"
    notes "默认显示昨天次数统计"
    Api::Manage::V1::ApiController.common_params(api)
    param :query, :app_version, :string, :optional, "版本筛选"
    param :query, :app_channel, :string, :optional, "渠道筛选"
    param :query, :start_at, :string, :optional, "开始年月, 如: 2017-01-04"
    param :query, :end_at, :string, :optional, "结束年月, 如: 2017-03-04"
  end
  def count_scope_index
    if params[:start_at].present? && params[:end_at].present?
      logs = NodeConnectionCountDayStat.between_date(params[:start_at].to_date, params[:end_at].to_date)
    else
      logs = NodeConnectionCountDayStat.between_date(Time.now.to_date.beginning_of_month, Time.now.to_date.end_of_month)
    end
    logs = handle_channel_params(logs)
    @log = NodeConnectionCountDayStat.stat_used_count(logs.to_sql).first
  end

  swagger_api :count_scope_details do |api|
    summary "用户行为 - 用户使用情况 - 用户数详情"
    notes ""
    Api::Manage::V1::ApiController.common_params(api)
    Api::Manage::V1::ApiController.page_params(api)
    param :query, :times_scope, :string, :required, "用户次数段, 可选值: 1-2|3-5|6-9|10-19|20-49|50"
    param :query, :start_at, :string, :required, "开始年月, 如: 2017-01-04"
    param :query, :end_at, :string, :required, "结束年月, 如: 2017-03-04"
    param :query, :app_version, :string, :optional, "版本筛选"
    param :query, :app_channel, :string, :optional, "渠道筛选"
  end
  def count_scope_details
    requires! :times_scope, type: String
    requires! :start_at, type: String
    requires! :end_at, type: String

    logs = NodeConnectionCountDayStat
    logs = handle_channel_params(logs)
    if params[:start_at].present? && params[:end_at].present?
      logs = logs.between_date(params[:start_at].to_date, params[:end_at].to_date)
    end
    # 这里由于使用了自定义SQL语句，所以使用PageCustomer解析分页
    @logs = NodeConnectionCountDayStat.by_times_scope(params[:times_scope], logs, param_page, param_limit)
    @users = User.select(:id, :username).where(id: @logs.result.map{|log| log['user_id']}).index_by(&:id)
  end

  swagger_api :traffic_day_stat do |api|
    summary "用户行为 - 用户使用情况 - 使用流量"
    notes "显示每天用户使用的流量统计情况"
    Api::Manage::V1::ApiController.common_params(api)
    param :query, :app_version, :string, :optional, "版本筛选"
    param :query, :app_channel, :string, :optional, "渠道筛选"
    param :query, :start_at, :string, :optional, "开始年月, 如: 2017-01-04"
    param :query, :end_at, :string, :optional, "结束年月, 如: 2017-03-04"
  end
  def traffic_day_stat
    start_at = params[:start_at]&.to_date || Time.now.to_date.beginning_of_month
    end_at = params[:end_at]&.to_date || Time.now.to_date.end_of_month

    error!("查询范围不能大于60天") if (end_at - start_at).to_i > 60

    logs = Log::UserTrafficDayStat.between_date(start_at, end_at)
    logs = handle_version_params(logs)
    @logs = logs.order(stat_date: :desc)
  end
end

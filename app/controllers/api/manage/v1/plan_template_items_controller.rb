class Api::Manage::V1::PlanTemplateItemsController < Api::Manage::V1::ApiController

  # 设置权限
  include PermissionFilterable
  set_module_group_name :charge_manage

  before_action :set_item, only: [:show, :update, :destroy]

  swagger_controller :plan_template_items, "套餐模版详情"

  swagger_api :index do |api|
    summary "套餐模版详情 - 列表"
    notes ""
    Api::Manage::V1::ApiController.common_params(api)
    param :query, :template_id, :string, :optional, "套餐模版ID，从套餐模板接口获取"
    param :query, :client_tab_id, :string, :optional, "客户端标签ID，从客户端标签页获取"
    param :query, :is_regular_time, :string, :optional, "是否是包时间套餐, true|false，不指定则为全部"
    param :query, :time_type, :string, :optional, "包时间类型, monthly:包月/quarterly:包季/half_yearly:包半年/yearly:包一年，写死，不指定则为全部"
    param :query, :node_type_id, :integer, :optional, "服务类型ID"
    param :query, :is_iap, :string, :optional, "是否是IAP套餐, true|false，不指定则为全部"
    param :query, :status, :string, :optional, "状态, 0:禁用, 1:启用, 不提供此参数时则返回所有启用的记录"
    Api::Manage::V1::ApiController.page_params(api)
  end
  def index
    items = V2::Plan.sorted
    items = items.where(template_id: params[:template_id]) if params[:template_id].present?
    items = items.where(client_tab_id: params[:client_tab_id]) if params[:client_tab_id].present?
    items = items.where(is_regular_time: params[:is_regular_time] == 'true') if params[:is_regular_time].present?
    items = items.where(time_type: params[:time_type]) if params[:time_type].present?
    items = items.where(node_type_id: params[:node_type_id]) if params[:node_type_id].present?
    items = items.where(is_iap: params[:is_iap] == 'true') if params[:is_iap].present?
    items = if params[:status].present?
      items.where(status: params[:status])
    else
      items.enabled
    end
    @items = items.includes(:template, :client_tab)
      .page(param_page)
      .per(param_limit)
  end

  swagger_api :create do |api|
    summary "套餐模版详情 - 创建"
    notes ""
    Api::Manage::V1::ApiController.common_params(api)
    param :form, :template_id, :string, :required, "套餐模版ID，从套餐模板接口获取"
    param :form, :client_tab_id, :string, :required, "客户端标签ID，从客户端标签页获取"
    param :form, :name, :string, :required, "套餐名称"
    param :form, :is_iap, :string, :required, "是否是IAP(苹果应用内付费)套餐，true|false，默认为false, 前端在此字段为true时，显示后面的iap_id字段，反之隐藏"
    param :form, :iap_id, :string, :optional, "IAP(苹果应用内付费)套餐ID, is_iap为true是必填"
    param :form, :currency_symbol, :string, :required, "货币符号"
    param :form, :price, :string, :required, "价格，如: 1.00"
    param :form, :is_recommend, :string, :required, "是否推荐, true|false，如果不传此参数，默认为false"
    param :form, :coins, :integer, :optional, "包含钻石数，默认为0"
    param :form, :present_coins, :integer, :optional, "赠送钻石, 默认为0"
    param :form, :is_regular_time, :string, :required, "是否是包时间套餐，true|false，默认false，前端在此字段为true是，才显示后面的time_type和node_type_id字段，反之隐藏"
    param :form, :time_type, :string, :optional, "包时间类型, monthly:包月/quarterly:包季/half_yearly:包半年/yearly:包一年，写死，默认为空"
    param :form, :node_type_id, :integer, :optional, "服务类型ID(指定要加时间的服务类型)"
    param :form, :description1, :string, :optional, "描述1(名称下方)"
    param :form, :description2, :string, :optional, "描述2(描述1右方)"
    param :form, :description3, :string, :optional, "描述3(价格下方)"
    param :form, :sort_weight, :string, :required, "排序权重，数字越大权重越高，默认为1"
    param :form, :status, :string, :required, "状态, 0:禁用, 1:启用，默认为1"
  end
  def create
    requires! :template_id, type: Integer
    requires! :client_tab_id, type: Integer
    requires! :name, type: String
    requires! :is_regular_time, type: String
    requires! :price, type: String
    requires! :currency_symbol, type: String

    @item = V2::Plan.new(allowed_params)
    error_detail!(@item) if !@item.save
  end

  swagger_api :show do |api|
    summary "套餐模版详情 - 单个获取"
    notes ""
    Api::Manage::V1::ApiController.common_params(api)
    param :path, :id, :integer, :required, "ID"
  end
  def show
    requires! :id, type: Integer
  end

  swagger_api :update do |api|
    summary "套餐模版详情 - 更新"
    notes ""
    Api::Manage::V1::ApiController.common_params(api)
    param :path, :id, :integer, :optional, "套餐ID"
    param :form, :template_id, :string, :optional, "套餐模版ID，从套餐模板接口获取"
    param :form, :client_tab_id, :string, :optional, "客户端标签ID，从客户端标签页获取"
    param :form, :name, :string, :optional, "套餐名称"
    param :form, :is_iap, :string, :optional, "是否是IAP(苹果应用内付费)套餐"
    param :form, :iap_id, :string, :optional, "IAP(苹果应用内付费)套餐ID, is_iap为true是必填"
    param :form, :currency_symbol, :string, :optional, "货币符号"
    param :form, :price, :string, :optional, "价格，如: 1.00"
    param :form, :is_recommend, :string, :optional, "是否推荐, true|false，如果不传此参数，默认为false"
    param :form, :coins, :integer, :optional, "包含钻石数"
    param :form, :present_coins, :integer, :optional, "赠送钻石"
    param :form, :is_regular_time, :string, :optional, "是否是包时间套餐，true|false，默认false"
    param :form, :time_type, :string, :optional, "包时间类型, monthly:包月/quarterly:包季/half_yearly:包半年/yearly:包一年，写死，默认为空"
    param :form, :node_type_id, :integer, :optional, "服务类型ID(指定要加时间的服务类型)"
    param :form, :description1, :string, :optional, "描述1(名称下方)"
    param :form, :description2, :string, :optional, "描述2(描述1右方)"
    param :form, :description3, :string, :optional, "描述3(价格下方)"
    param :form, :sort_weight, :string, :optional, "排序权重，数字越大权重越高"
    param :form, :status, :string, :optional, "状态, 0:禁用, 1:启用"
  end
  def update
    error_detail!(@item) if !@item.update(allowed_params)
  end

  private

  def allowed_params
    params.permit(
      :template_id,
      :client_tab_id,
      :name,
      :is_iap,
      :iap_id,
      :currency_symbol,
      :price,
      :is_recommend,
      :coins,
      :present_coins,
      :is_regular_time,
      :time_type,
      :node_type_id,
      :description1,
      :description2,
      :description3,
      :sort_weight,
      :status
    )
  end

  def set_item
    @item = V2::Plan.find(params[:id])
  end

end

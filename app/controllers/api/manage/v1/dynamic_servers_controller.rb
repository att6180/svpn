class Api::Manage::V1::DynamicServersController < Api::Manage::V1::ApiController

  # 设置权限
  include PermissionFilterable
  set_module_group_name :node_manage

  before_action :set_item, only: [:show, :update, :destroy]

  swagger_controller :dynamic_servers, "动态IP配置"

  swagger_api :index do |api|
    summary "动态IP配置 - 列表"
    notes ""
    Api::Manage::V1::ApiController.common_params(api)
    param :query, :page, :integer, :optional, "页码，默认: 1"
    param :query, :limit, :integer, :optional, "每页显示的记录数, 默认: 25"
  end
  def index
    optional! :page, type: Integer
    optional! :limit, type: Integer

    @servers = DynamicServer.recent.page(param_page).per(param_limit)
  end

  swagger_api :create do |api|
    summary "动态IP配置 - 创建"
    notes ""
    Api::Manage::V1::ApiController.common_params(api)
    param :form, :url, :string, :required, "URL"
    param :form, :region, :string, :required, "IP区域"
    param :form, :priority, :integer, :required, "优先级"
    param :form, :is_enabled, :integer, :optional, "是否启用, 0:否 1:是"
  end
  def create
    requires! :url, type: String
    requires! :region, type: String
    requires! :priority, type: Integer
    optional! :is_enabled, type: Integer

    @server = DynamicServer.new(allowed_params)
    error_detail!(@server) if !@server.save
  end

  swagger_api :show do |api|
    summary "动态IP配置 - 单个获取"
    notes ""
    Api::Manage::V1::ApiController.common_params(api)
    param :path, :id, :integer, :required, "ID"
  end
  def show
    requires! :id, type: Integer
  end

  swagger_api :update do |api|
    summary "动态IP配置 - 更新"
    notes ""
    Api::Manage::V1::ApiController.common_params(api)
    param :path, :id, :integer, :required, "ID"
    param :form, :url, :string, :optional, "URL"
    param :form, :region, :string, :optional, "IP区域"
    param :form, :priority, :integer, :optional, "优先级"
    param :form, :is_enabled, :integer, :optional, "是否启用, 0:否 1:是"
  end
  def update
    optional! :url, type: String
    optional! :region, type: String
    optional! :priority, type: Integer
    optional! :is_enabled, type: Integer

    error_detail!(@server) if !@server.update(allowed_params)
  end

  #swagger_api :destroy do |api|
    #summary "动态IP配置 - 删除"
    #notes ""
    #Api::Manage::V1::ApiController.common_params(api)
    #param :path, :id, :integer, :required, "ID"
  #end
  #def destroy
    #requires! :id, type: Integer

    #error_detail!(@server) if !@server.destroy
  #end

  swagger_api :push do |api|
    summary "动态IP配置 - 推送"
    notes "推送当前动态ip列表到客户端"
    Api::Manage::V1::ApiController.common_params(api)
  end
  def push
    content = render_to_string(
      template: 'api/manage/v1/dynamic_servers/__jpush_servers',
      locals: { type: 0, servers: DynamicServer.enabled.sorted.to_a },
      layout: false
    )
    # 获取所有推送认证信息
    jpush_auth_data = SystemSetting.all_jpush_auth_data
    jpush_auth_data.each do |auth|
      push_servers(auth[:key], auth[:secret], content)
    end
  end

  swagger_api :status do |api|
    summary "用户行为 - 服务器信息 - 动态IP状态"
    notes ""
    Api::Manage::V1::ApiController.common_params(api)
    param :query, :start_at, :string, :optional, "起始日期, 如: 2017-03-01"
    param :query, :end_at, :string, :optional, "结束日期, 如: 2017-03-01"
    Api::Manage::V1::ApiController.page_params(api)
  end
  def status
    optional! :start_at, type: String
    optional! :end_at, type: String
    optional! :page, type: Integer
    optional! :limit, type: Integer

    can_access?(:user_behaviour)

    logs = DynamicServerConnectionFailedLog.group_by_server
    if params[:start_at].present? && params[:end_at].present?
      logs = logs.between_date(params[:start_at].to_date, params[:end_at].to_date)
    end
    @logs = logs.includes(:dynamic_server).page(param_page).per(param_limit)
  end

  swagger_api :status_details do |api|
    summary "动态IP状态详情"
    notes ""
    Api::Manage::V1::ApiController.common_params(api)
    param :path, :id, :integer, :required, "ID"
    param :query, :start_at, :string, :optional, "起始日期, 如: 2017-03-01"
    param :query, :end_at, :string, :optional, "结束日期, 如: 2017-03-01"
    Api::Manage::V1::ApiController.page_params(api)
  end
  def status_details
    requires! :id, type: Integer
    optional! :start_at, type: String
    optional! :end_at, type: String
    optional! :page, type: Integer
    optional! :limit, type: Integer

    logs = DynamicServerConnectionFailedLog.where(dynamic_server_id: params[:id]).recent
    if params[:start_at].present? && params[:end_at].present?
      logs = logs.between_date(params[:start_at].to_date, params[:end_at].to_date)
    end
    @logs = logs.includes(:user).page(param_page).per(param_limit)
  end

  private

  def push_servers(key, secret, content)
    jpush = JPush::Client.new(key, secret)
    result = JSON.parse(content).to_h
    notification = JPush::Push::Notification.new.
      set_alert(api_t('dynamic_servers_notification')).
      set_android(
        alert: api_t('dynamic_servers_notification'),
        extras: result
    ).set_ios(
      alert: api_t('dynamic_servers_notification'),
      extras: result
    )
    payload =JPush::Push::PushPayload.new(
      platform: 'all',
      audience: 'all',
      notification: notification
    )
    jpush.pusher.push(payload)
  end

  def allowed_params
    params.permit(
      :url,
      :region,
      :priority,
      :is_enabled
    )
  end

  def set_item
    @server = DynamicServer.find(params[:id])
  end
end

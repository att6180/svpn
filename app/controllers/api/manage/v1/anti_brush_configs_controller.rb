class Api::Manage::V1::AntiBrushConfigsController < Api::Manage::V1::ApiController

  before_action :set_anti_brush, only: [:update, :destroy, :on_off]

  swagger_controller :anti_brush_configs, "防刷策略配置"

  swagger_api :index do |api|
    summary "防刷策略配置 - 列表"
    notes ""
    Api::Manage::V1::ApiController.common_params(api)
    Api::Manage::V1::ApiController.page_params(api)
  end
  def index
    @configs = Waf::AntiBrushConfig.page(param_page).per(param_limit)
  end

  swagger_api :create do |api|
    summary "防刷策略配置 - 创建"
    notes ""
    Api::Manage::V1::ApiController.common_params(api)
    param :form, :no, :String, :required, "编号"
    param :form, :app_channel, :String, :required, "渠道"
    param :form, :app_version, :String, :required, "版本"
    param :form, :app_version_no, :String, :optional, "版本号"
    param :form, :white_lists, :String, :optional, "白名单,|分割"
    param :form, :black_lists, :String, :optional, "黑名单,|分割"
  end
  def create
    requires! :no, type: String
    requires! :app_channel, type: String
    requires! :app_version, type: String
    optional! :app_version_no, type: String
    optional! :white_lists, type: String
    optional! :black_lists, type: String

    @config = Waf::AntiBrushConfig.new(allowed_params)
    error_detail!(@config) if !@config.save
  end

  swagger_api :update do |api|
    summary "防刷策略 - 更新"
    notes ""
    Api::Manage::V1::ApiController.common_params(api)
    param :path, :id, :integer, :required, "ID"
    param :form, :no, :String, :required, "编号"
    param :form, :app_channel, :String, :required, "渠道"
    param :form, :app_version, :String, :required, "版本"
    param :form, :app_version_no, :String, :optional, "版本号"
    param :form, :white_lists, :String, :optional, "白名单,|分割"
    param :form, :black_lists, :String, :optional, "黑名单,|分割"
  end
  def update
    requires! :id, type: Integer
    requires! :no, type: String
    requires! :app_channel, type: String
    requires! :app_version, type: String
    optional! :app_version_no, type: String
    optional! :white_lists, type: String
    optional! :black_lists, type: String

    error_detail!(@config) if !@config.update(allowed_params.except(:id))
  end

  swagger_api :destroy do |api|
    summary "防刷策略 - 删除"
    notes ""
    Api::Manage::V1::ApiController.common_params(api)
    param :path, :id, :integer, :required, "防刷策略id"
  end
  def destroy
    requires! :id, type: Integer

    error_detail!(@config) if !@config.destroy
  end

  swagger_api :on_off do |api|
    summary "防刷开关控制"
    notes "0开/1关"
    Api::Manage::V1::ApiController.common_params(api)
    param :path, :id, :integer, :required, "ID"
    param :form, :status, :integer, :required, "状态"
  end
  def on_off
    requires! :id, type: Integer
    requires! :status, type: Integer

    @config.update(status: params[:status]&.to_i == 0 ? 'opened' : 'closed')
  end

  private

  def allowed_params
    params.permit(
      :id,
      :no,
      :app_channel,
      :app_version,
      :app_version_no,
      :white_lists,
      :black_lists
    )
  end

  def set_anti_brush
    @config = Waf::AntiBrushConfig.find(params[:id])
  end
end

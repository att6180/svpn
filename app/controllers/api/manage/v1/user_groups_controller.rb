class Api::Manage::V1::UserGroupsController < Api::Manage::V1::ApiController

  before_action :set_user_group, only: [:show, :update, :destroy]

  swagger_controller :user_groups, "用户类型配置"

  swagger_api :index do |api|
    summary "用户类型配置 - 列表"
    notes ""
    Api::Manage::V1::ApiController.common_params(api)
    param :query, :is_enabled, :string, :optional, "true: 只获取已启用的类型, false|忽略参数: 获取全部"
    param :query, :page, :integer, :optional, "页码，默认: 1"
    param :query, :limit, :integer, :optional, "每页显示的记录数, 默认: 25"
  end
  def index
    optional! :is_enabled, type: String
    optional! :page, type: Integer
    optional! :limit, type: Integer

    user_groups = UserGroup.sorted_by_level
    user_groups = user_groups.enabled if params[:is_enabled] == 'true'
    @user_groups = user_groups.page(param_page).per(param_limit)
  end

  swagger_api :create do |api|
    summary "用户类型配置 - 创建"
    notes ""
    Api::Manage::V1::ApiController.common_params(api)
    param :form, :name, :string, :required, "名称"
    param :form, :need_coins, :integer, :required, "累计钻石"
    param :form, :level, :string, :required, "等级"
    param :form, :is_enabled, :string, :optional, "是否启用"
  end
  def create
    requires! :name, type: String
    requires! :need_coins, type: Integer
    requires! :level, type: Integer
    requires! :is_enabled, type: String

    can_access?(:charge_manage)

    @user_group = UserGroup.new(allowed_params)
    error_detail!(@user_group) if !@user_group.save
  end

  swagger_api :show do |api|
    summary "用户类型配置 - 单个获取"
    notes ""
    Api::Manage::V1::ApiController.common_params(api)
    param :path, :id, :integer, :required, "ID"
  end
  def show
    requires! :id, type: Integer

    can_access?(:charge_manage)
  end

  swagger_api :update do |api|
    summary "用户类型配置 - 更新"
    notes ""
    Api::Manage::V1::ApiController.common_params(api)
    param :path, :id, :integer, :required, "ID"
    param :form, :name, :string, :optional, "名称"
    param :form, :need_coins, :integer, :optional, "累计钻石"
    param :form, :level, :string, :optional, "等级"
    param :form, :is_enabled, :string, :optional, "是否启用"
  end
  def update
    requires! :id, type: Integer
    optional! :name, type: String
    optional! :need_coins, type: Integer
    optional! :level, type: Integer
    optional! :is_enabled, type: String

    can_access?(:charge_manage)

    error_detail!(@user_group) if !@user_group.update(allowed_params)
  end

  private

  def allowed_params
    params.permit(
      :name,
      :need_coins,
      :level,
      :is_enabled
    )
  end

  def set_user_group
    @user_group = UserGroup.find(params[:id])
  end
end

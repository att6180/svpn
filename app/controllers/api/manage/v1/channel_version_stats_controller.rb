class Api::Manage::V1::ChannelVersionStatsController < Api::Manage::V1::ApiController

  # 设置权限
  include PermissionFilterable
  set_module_group_name :user_base_info

  swagger_controller :channel_version_stats, "版本统计"

  swagger_api :signup_stat do |api|
    summary "用户基础信息 - 统计表 - 版本统计 - 注册版本统计"
    notes <<-EOF
    channel: 渠道统计(数组)
        name: 渠道名
        users_count: 用户数
    versions: 版本统计(数组)
        name: 版本名
        users_count: 用户数
    version_numbers: 版本号统计
        platform: 平台
        version_numbers: 版本号
        users_count: 用户数
    EOF
    Api::Manage::V1::ApiController.common_params(api)
  end
  def signup_stat
    @channels = $redis.hgetall(StatService::VERSION_SIGNUP_CHANNEL)
        .sort_by{|k, v| v.to_i}.reverse
    @versions = $redis.hgetall(StatService::VERSION_SIGNUP_VERSION)
        .sort_by{|k, v| v.to_i}.reverse
    @platform_vns = $redis.hgetall(StatService::VERSION_SIGNUP_VERSION_NUM)
        .sort_by{|k, v| v.to_i}.reverse
  end

  swagger_api :signin_stat do |api|
    summary "用户基础信息 - 统计表 - 版本统计 - 登录版本统计"
    notes <<-EOF
    channel: 渠道统计(数组)
        name: 渠道名
        users_count: 用户数
        rate: 百分比
    versions: 版本统计(数组)
        name: 版本名
        users_count: 用户数
        rate: 百分比
    version_numbers: 版本号统计
        platform: 平台
        version_numbers: 版本号
        users_count: 用户数
        rate: 百分比
    EOF
    Api::Manage::V1::ApiController.common_params(api)
  end
  def signin_stat
    @channels = []
    cache_key = "#{StatService::VERSION_SIGNIN_PREFIX}channel_"
    cache_items = $redis.keys("#{cache_key}*")
    cache_items.each do |item|
        name = item.gsub(cache_key, '')
        @channels << { name: name, users_count: $redis.scard(item) }
    end
    @channels = @channels.sort_by{|i| i[:users_count].to_i}.reverse

    @versions = []
    cache_key = "#{StatService::VERSION_SIGNIN_PREFIX}version_"
    cache_items = $redis.keys("#{cache_key}*")
    cache_items.each do |item|
        name = item.gsub(cache_key, '')
        @versions << { name: name, users_count: $redis.scard(item) }
    end
    @versions = @versions.sort_by{|i| i[:users_count].to_i}.reverse

    @platform_vns = []
    cache_key = "#{StatService::VERSION_SIGNIN_PREFIX}platform_vn_"
    cache_items = $redis.keys("#{cache_key}*")
    cache_items.each do |item|
        platform, version_number = item.gsub(cache_key, '').split('_')
        @platform_vns << { platform: platform, version_number: version_number, users_count: $redis.scard(item) }
    end
    @platform_vns = @platform_vns.sort_by{|i| i[:users_count].to_i}.reverse
  end
end

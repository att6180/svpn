class Api::Manage::V1::SplashScreenAdsController < Api::Manage::V1::ApiController

  # 设置权限
  include PermissionFilterable
  set_module_group_name :system_setting

  before_action :set_item, only: [:show, :update, :destroy]

  swagger_controller :splash_screen_ads, "开屏广告相关配置及操作"

  swagger_api :update_splash_screen_settings do |api|
    summary "参数配置 - 开屏广告 - 设置"
    notes ""
    Api::Manage::V1::ApiController.common_params(api)
    param :form, :is_enabled, :string, :optional, "开屏广告是否开启, true:是，false:否"
    param :form, :overtime, :string, :optional, "开屏广告间隔时间(秒), 如: 1800"
  end
  def update_splash_screen_settings
    Setting.splash_screen_ad_enabled = params[:is_enabled] if params[:is_enabled].present?
    Setting.splash_screen_ad_overtime = params[:overtime] if params[:overtime].present?
  end

  swagger_api :index do |api|
    summary "参数配置 - 开屏广告 - 列表"
    notes ""
    Api::Manage::V1::ApiController.common_params(api)
    Api::Manage::V1::ApiController.version_params(api)
    Api::Manage::V1::ApiController.page_params(api)
  end
  def index
    @items = SplashScreenAd.the_where(
      app_channel: params[:app_channel],
      app_version: params[:app_version]
    )
    .order(is_enabled: :desc, time_type: :asc, id: :asc)
    .page(param_page)
    .per(param_limit)
  end

  swagger_api :all do |api|
    summary "参数配置 - 开屏广告 - 所有已启用广告列表"
    notes ""
    Api::Manage::V1::ApiController.common_params(api)
  end
  def all
    @items = SplashScreenAd.select(:id, :name).enabled.order(time_type: :asc, id: :asc)
  end

  swagger_api :create do |api|
    summary "参数配置 - 开屏广告 - 创建"
    notes ""
    Api::Manage::V1::ApiController.common_params(api)
    param :form, :name, :string, :required, "名称"
    param :form, :duration, :string, :required, "展示时长(秒), 如: 30"
    param :form, :date_type, :integer, :required, "展示日期类型: 0:每天, 1:指定日期"
    param :form, :date_start_at, :string, :optional, "展示日期-开始日期，如：2018-02-04"
    param :form, :date_end_at, :string, :optional, "展示日期-结束日期，如：2018-02-05"
    param :form, :time_type, :integer, :required, "展示时间类型: 0:全天, 1:指定时间"
    param :form, :time_start_at, :string, :optional, "展示时间-开始时间，如：12:10:00"
    param :form, :time_end_at, :string, :optional, "展示时间-结束时间，如：22:10:00"
    param :form, :image_url, :string, :required, "广告图片地址"
    param :form, :link_url, :string, :optional, "广告跳转地址"
    param :form, :is_enabled, :string, :required, "是否启用, true:启用, false:禁用"
    param :form, :ad_type, :string, :required, "广告播放类型: image:图片(包含jpeg、png), gif:动图, video:视频"
    param :form, :open_link_type, :string, :required, "跳转方式: webview: webview, browser: 浏览器"
    param :form, :app_channel, :string, :required, "app渠道"
    param :form, :app_version, :string, :required, "app版本"
  end
  def create
    requires! :name, type: String
    requires! :duration, type: String
    requires! :date_type, type: String
    requires! :time_type, type: String
    requires! :image_url, type: String
    optional! :link_url, type: String
    requires! :is_enabled, type: String
    requires! :ad_type, type: String
    requires! :open_link_type, type: String
    requires! :app_channel, type: String
    requires! :app_version, type: String

    @item = SplashScreenAd.new
    @item.name = params[:name]
    @item.duration = params[:duration]
    @item.image_url = params[:image_url]
    @item.link_url = params[:link_url]
    @item.is_enabled = params[:is_enabled]
    @item.date_type = params[:date_type]
    @item.time_type = params[:time_type]
    @item.ad_type = params[:ad_type]
    @item.open_link_type = params[:open_link_type]
    @item.app_channel = params[:app_channel]
    @item.app_version = params[:app_version]

    error!("展示日期类型错误，请重试") if !%(0, 1).include?(params[:date_type])
    error!("未选择展示日期") if params[:date_type] == "1" &&
      (params[:date_start_at].blank? || params[:date_end_at].blank?)
    error!("展示时间类型错误，请重试") if !%(0, 1).include?(params[:time_type])
    error!("未选择展示时间") if params[:time_type] == "1" &&
      (params[:time_start_at].blank? || params[:time_end_at].blank?)

    time_priority, start_at, end_at = handle_time_quantum(params)

    @item.time_priority = time_priority if @item.time_priority != time_priority
    @item.start_at = start_at
    @item.end_at = end_at
    @item.last_modification_time = Time.now
    error_detail!(@item) if !@item.save
  end

  swagger_api :show do |api|
    summary "参数配置 - 开屏广告 - 单个获取"
    notes ""
    Api::Manage::V1::ApiController.common_params(api)
    param :path, :id, :integer, :required, "ID"
  end
  def show
    requires! :id, type: Integer
  end

  swagger_api :update do |api|
    summary "参数配置 - 开屏广告 - 单个更新"
    notes ""
    Api::Manage::V1::ApiController.common_params(api)
    param :path, :id, :integer, :required, "ID"
    param :form, :name, :string, :optional, "名称"
    param :form, :duration, :string, :optional, "展示时长(秒), 如: 30"
    param :form, :date_type, :integer, :optional, "展示日期类型: 0:每天, 1:指定日期"
    param :form, :date_start_at, :string, :optional, "展示日期-开始日期，如：2018-02-04"
    param :form, :date_end_at, :string, :optional, "展示日期-结束日期，如：2018-02-05"
    param :form, :time_type, :integer, :optional, "展示时间类型: 0:全天, 1:指定时间"
    param :form, :time_start_at, :string, :optional, "展示时间-开始时间，如：12:10:00"
    param :form, :time_end_at, :string, :optional, "展示时间-结束时间，如：22:10:00"
    param :form, :image_url, :string, :optional, "广告图片地址"
    param :form, :link_url, :string, :optional, "广告跳转地址"
    param :form, :is_enabled, :string, :optional, "是否启用, true:启用, false:禁用"
    param :form, :ad_type, :string, :optional, "广告播放类型: image:图片(包含jpeg、png), gif:动图, video:视频"
    param :form, :open_link_type, :string, :optional, "跳转方式: webview: webview, browser: 浏览器"
    param :form, :app_channel, :string, :optional, "app渠道"
    param :form, :app_version, :string, :optional, "app版本"
  end
  def update
    requires! :id, type: String

    @item.name = params[:name] if params[:name].present?
    @item.duration = params[:duration] if params[:duration].present?
    @item.image_url = params[:image_url] if params[:image_url].present?
    @item.link_url = params[:link_url] if params[:op].blank?
    @item.is_enabled = params[:is_enabled] if params[:is_enabled].present?
    @item.date_type = params[:date_type] if params[:date_type].present?
    @item.time_type = params[:time_type] if params[:time_type].present?
    @item.ad_type = params[:ad_type] if params[:ad_type].present?
    @item.open_link_type = params[:open_link_type] if params[:open_link_type].present?
    @item.app_channel = params[:app_channel] if params[:app_channel].present?
    @item.app_version = params[:app_version] if params[:app_version].present?

    error!("展示日期类型错误，请重试") if params[:date_type].present? && !["0", "1"].include?(params[:date_type])
    error!("未选择展示日期") if params[:date_type] == "1" &&
      (params[:date_start_at].blank? || params[:date_end_at].blank?)
    error!("展示时间类型错误，请重试") if params[:time_type].present? && !["0", "1"].include?(params[:time_type])
    error!("未选择展示时间") if params[:time_type] == "1" &&
      (params[:time_start_at].blank? || params[:time_end_at].blank?)

    time_priority, start_at, end_at = handle_time_quantum(params)

    @item.time_priority = time_priority if @item.time_priority != time_priority && !time_priority.nil?
    @item.start_at = start_at if start_at.present?
    @item.end_at = end_at if end_at.present?
    @item.last_modification_time = Time.now if @item.changed?
    error_detail!(@item) if !@item.save
  end

  private

  def handle_time_quantum(params)
    date_type = params[:date_type]
    time_type = params[:time_type]
    # 如果是指定日期和时间
    if date_type == "1" && time_type == "1"
      time_priority = 1
      start_at = "#{params[:date_start_at]} #{params[:time_start_at]}"
      end_at = "#{params[:date_end_at]} #{params[:time_end_at]}"
    # 如果是指定日期和全天
    elsif date_type == "1" && time_type == "0"
      time_priority = 2
      start_at = DateUtils.time2str(params[:date_start_at].to_date.beginning_of_day)
      end_at = DateUtils.time2str(params[:date_end_at].to_date.end_of_day)
    # 如果是每天和指定时间
    elsif date_type == "0" && time_type == "1"
      time_priority = 3
      start_at = "1970-01-01 #{params[:time_start_at]}"
      end_date = (Time.now.to_date + 100.year).strftime("%Y-%m-%d")
      end_at = "#{end_date} #{params[:time_end_at]}"
    # 如果是每天和全天
    elsif date_type == "0" && time_type == "0"
      time_priority = 4
      start_at = "1970-01-01 00:00:00"
      end_at = DateUtils.time2str(Time.now + 100.year)
    else
      start_at = nil
      end_at = nil
    end
    [time_priority, start_at, end_at]
  end

  def set_item
    @item = SplashScreenAd.find(params[:id])
  end

end

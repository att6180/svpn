class Api::Manage::V1::UserMonthRetentionRatesController < Api::Manage::V1::ApiController

  include PermissionFilterable
  set_module_group_name :operation_behaviour

  swagger_controller :user_month_retention_rates, "月留存"

  swagger_api :index do |api|
    summary "月留存 - 列表"
    notes ""
    Api::Manage::V1::ApiController.common_params(api)
    Api::Manage::V1::ApiController.page_params(api)
    Api::Manage::V1::ApiController.version_params(api)
    param :query, :start_at, :string, :optional, "开始年月, 如: 2017-01"
    param :query, :end_at, :string, :optional, "结束年月, 如: 2017-03"
  end
  def index
    # 如果有筛选则查询一遍无筛选的数据进行对比
    total_logs = has_channel_param? ? Log::UserMonthRetentionRateStat.where(filter_type: 0) : nil

    logs = Log::UserMonthRetentionRateStat.all
    logs = handle_version_params(logs)
    if params[:start_at].present? && params[:end_at].present?
      logs = logs.between_months(params[:start_at].to_date, params[:end_at].to_date)
      total_logs = total_logs.between_months(params[:start_at].to_date, params[:end_at].to_date) if total_logs
    else
      today = Time.now.to_date
      logs = logs.between_months(today.beginning_of_month, today.end_of_month)
      total_logs = total_logs.between_months(today.beginning_of_month, today.end_of_month) if total_logs
    end
    @logs = logs.stat_result_user.order("stat_date DESC").page(param_page).per(param_limit)
    if total_logs
      @total_logs = total_logs
        .stat_result_user
        .order("stat_date DESC")
        .page(param_page).per(param_limit)
        .index_by{|log| log.stat_date.strftime("%Y%m%d")}
    end
  end

  swagger_api :chart do |api|
    summary "留存 - 图表"
    notes ""
    Api::Manage::V1::ApiController.common_params(api)
    Api::Manage::V1::ApiController.version_params(api)
    param :query, :start_at, :string, :optional, "开始年月, 如: 2017-01-04"
    param :query, :end_at, :string, :optional, "结束年月, 如: 2017-03-05"
  end
  def chart
    optional! :app_version, type: String
    optional! :app_channel, type: String
    optional! :start_at, type: String
    optional! :end_at, type: String

    logs = Log::UserMonthRetentionRateStat.all
    logs = handle_version_params(logs)
    if params[:start_at].present? && params[:end_at].present?
      logs = logs.between_months(params[:start_at].to_date, params[:end_at].to_date)
    else
      today = Time.now.to_date
      logs = logs.between_months(today.beginning_of_month, today.end_of_month)
    end
    @logs = logs.stat_result_user.order("stat_date ASC")
  end

end

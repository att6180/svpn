class Api::Manage::V1::NewUserStatsController < Api::Manage::V1::ApiController

  # 设置权限
  include PermissionFilterable
  set_module_group_name :operation_behaviour

  swagger_controller :new_user_stats, "新增用户"

  swagger_api :hour_index do |api|
    summary "新增用户 - 小时新增 - 列表"
    notes ""
    Api::Manage::V1::ApiController.common_params(api)
    Api::Manage::V1::ApiController.package_params(api)
    Api::Manage::V1::ApiController.version_params(api)
    param :query, :date, :string, :required, "筛选日期, 如: 2017-01-01"
  end
  def hour_index
    @date = params[:date].to_date
    if @date.today?
      logs = User.by_created_date(@date)
        .group_by_hour
        .select("COUNT(id) as users_count")

      logs = handle_muilt_version_params(logs, 1)
      log_indexs = logs.to_a.map(&:serializable_hash).index_by{|l| l["h"]}
      today_logs = []
      (0..23).each_with_index do |i|
        today_logs.push(log_indexs.has_key?(i) ? log_indexs[i].symbolize_keys! : { h: i, users_count: 0 })
      end
      @logs = today_logs
      @total_users = today_logs.map.sum{|l| l[:users_count]}
    else
      logs = NewUserHourStat.select(:stat_date, :stat_hour)
        .by_date(@date)
        .select_sum_users
        .group_by_hour
        .sorted
      @logs = handle_muilt_version_histroy_params(logs)
      @total_users = @logs.map.sum{|l| l.users_count}
    end
    logs = NewUserHourStat.select(:stat_date, :stat_hour)
      .by_date(@date.prev_day)
      .select_sum_users
      .group_by_hour
      .sorted
    @prev_logs = handle_muilt_version_histroy_params(logs)
    @prev_total_users = @prev_logs.map.sum{|l| l.users_count}
  end

  swagger_api :day_index do |api|
    summary "日新增用户 - 列表"
    notes ""
    Api::Manage::V1::ApiController.common_params(api)
    Api::Manage::V1::ApiController.page_params(api)
    Api::Manage::V1::ApiController.version_params(api)
    param :query, :start_at, :string, :optional, "开始日期, 如: 2017-01-01"
    param :query, :end_at, :string, :optional, "结束日期, 如: 2017-03-01"
  end
  def day_index
    # 如果有筛选则查询一遍无筛选的数据进行对比
    total_logs = has_channel_param? ? NewUserDayStat.where(filter_type: 0) : nil

    logs = NewUserDayStat.all
    logs = handle_version_params(logs)
    if params[:start_at].present? && params[:end_at].present?
      logs = logs.between_months(params[:start_at].to_date, params[:end_at].to_date)
      total_logs = total_logs.between_months(params[:start_at].to_date, params[:end_at].to_date) if total_logs
    else
      today = Time.now.to_date
      logs = logs.between_months(today.beginning_of_month, today.end_of_month)
      total_logs = total_logs.between_months(today.beginning_of_month, today.end_of_month) if total_logs
    end
    @logs = logs.stat_result.order("stat_date DESC").page(param_page).per(param_limit)
    if total_logs
      @total_logs = total_logs
        .stat_result
        .order("stat_date DESC")
        .page(param_page).per(param_limit)
        .index_by{|log| log.stat_date.strftime("%Y%m%d")}
    end
  end

  swagger_api :day_chart do |api|
    summary "日新增用户 - 图表"
    notes ""
    Api::Manage::V1::ApiController.common_params(api)
    Api::Manage::V1::ApiController.version_params(api)
    param :query, :start_at, :string, :optional, "开始年月, 如: 2017-01"
    param :query, :end_at, :string, :optional, "结束年月, 如: 2017-03"
  end
  def day_chart
    logs = NewUserDayStat.all
    logs = handle_version_params(logs)
    if params[:start_at].present? && params[:end_at].present?
      logs = logs.between_months("#{params[:start_at]}-01".to_date, "#{params[:end_at]}-01".to_date)
    else
      today = Time.now.to_date
      logs = logs.between_months(today.beginning_of_month, today.end_of_month)
    end
    @logs = logs.stat_result.order("stat_date ASC")
  end

  swagger_api :day_details do |api|
    summary "日新增用户 - 详情"
    notes ""
    Api::Manage::V1::ApiController.common_params(api)
    Api::Manage::V1::ApiController.page_params(api)
    Api::Manage::V1::ApiController.version_params(api)
    param :query, :stat_date, :string, :required, "统计日期, 如: 2017-04-11"
  end
  def day_details
    requires! :stat_date, type: String

    users = User.page(param_page).per(param_limit)
    users = users.where(create_app_version: params[:app_version]) if params[:app_version].present?
    users = users.where(create_app_channel: params[:app_channel]) if params[:app_channel].present?
    @users = users.by_created_date(params[:stat_date]).includes(:last_signin_log).recent
  end

  swagger_api :month_index do |api|
    summary "月新增用户 - 列表"
    notes ""
    Api::Manage::V1::ApiController.common_params(api)
    Api::Manage::V1::ApiController.page_params(api)
    Api::Manage::V1::ApiController.version_params(api)
    param :query, :start_at, :string, :optional, "开始年月, 如: 2017-01"
    param :query, :end_at, :string, :optional, "结束年月, 如: 2017-03"
  end
  def month_index
    # 如果有筛选则查询一遍无筛选的数据进行对比
    total_logs = has_channel_param? ? NewUserMonthStat.where(filter_type: 0) : nil

    start_at = "#{params[:start_at]}-01"&.to_date || (Time.now - 12.month).to_date.beginning_of_year
    end_at = "#{params[:end_at]}-01"&.to_date || Time.now.to_date.end_of_year

    logs = NewUserMonthStat.all
    logs = handle_version_params(logs)
    logs = logs.between_month(start_at, end_at)
    total_logs = total_logs.between_month(start_at, end_at) if total_logs
    @logs = logs.stat_result.order(stat_year: :desc, stat_month: :desc).page(param_page).per(param_limit)
    if total_logs
      @total_logs = total_logs
        .stat_result
        .order(stat_year: :desc, stat_month: :desc)
        .page(param_page).per(param_limit)
        .index_by{|log| "#{log.stat_year}#{log.stat_month}"}
    end
  end

  swagger_api :month_chart do |api|
    summary "月新增用户 - 图表"
    notes ""
    Api::Manage::V1::ApiController.common_params(api)
    Api::Manage::V1::ApiController.version_params(api)
    param :query, :start_at, :string, :optional, "开始年月, 如: 2017-01"
    param :query, :end_at, :string, :optional, "结束年月, 如: 2017-03"
  end
  def month_chart
    logs = NewUserMonthStat.all
    logs = handle_version_params(logs)
    start_at = "#{params[:start_at]}-01"&.to_date || (Time.now - 12.month).to_date.beginning_of_year
    end_at = "#{params[:end_at]}-01"&.to_date || Time.now.to_date.end_of_year
    logs = logs.between_month(start_at, end_at)
    @logs = logs.stat_result.order(stat_year: :asc, stat_month: :asc)
  end

  swagger_api :month_details do |api|
    summary "月新增用户 - 详情"
    notes ""
    Api::Manage::V1::ApiController.common_params(api)
    param :query, :stat_month, :string, :required, "统计年月, 如: 2017-01"
    Api::Manage::V1::ApiController.page_params(api)
    Api::Manage::V1::ApiController.version_params(api)
  end
  def month_details
    requires! :stat_month, type: Integer

    users = User.page(param_page).per(param_limit)
    users = users.where(create_app_version: params[:app_version]) if params[:app_version].present?
    users = users.where(create_app_channel: params[:app_channel]) if params[:app_channel].present?
    stat_date = "#{params[:stat_month]}-01".to_date
    @users = users.by_created_month(stat_date.year, stat_date.month).includes(:last_signin_log).recent
  end

end

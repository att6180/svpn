class Api::Manage::V1::UserStatusLogsController < Api::Manage::V1::ApiController

  # 设置权限
  include PermissionFilterable
  set_module_group_name :user_manage

  swagger_controller :user_status_logs, "用户封号日志"

  swagger_api :index do |api|
    summary "用户管理 - 封号日志 - 列表"
    notes ""
    Api::Manage::V1::ApiController.common_params(api)
    param :query, :page, :integer, :optional, "页码，默认: 1"
    param :query, :limit, :integer, :optional, "每页显示的记录数, 默认: 25"
    param :query, :username, :string, :optional, "搜索用户名"
  end
  def index
    optional! :page, type: Integer
    optional! :limit, type: Integer
    optional! :username, type: String

    if params[:username].present?
      logs = User.find_by(username: params[:username]).user_account_status_logs
    else
      logs = UserAccountStatusLog.all
    end
    @logs = logs.recent.includes(:user).page(param_page).per(param_limit)
  end

  swagger_api :create do |api|
    summary "用户管理 - 封号日志 - 管理员执行封解号"
    notes ""
    Api::Manage::V1::ApiController.common_params(api)
    param :form, :username, :string, :required, "被执行解封号的用户名"
    param :form, :operation_type, :integer, :required, "要执行的动作类型，0:封号, 1:解封"
    param :form, :description, :string, :optional, "备注"
  end
  def create
    requires! :username, type: String
    requires! :operation_type, type: Integer
    optional! :description, type: String

    user = User.find_by(username: allowed_params[:username])
    if user.present?
      user.update_columns(is_enabled: allowed_params[:operation_type] == "1")
      @log = UserAccountStatusLog.new(
        user_id: user.id,
        operation_type: UserAccountStatusLog.operation_types.keys[params[:operation_type].to_i],
        description: params[:description],
        admin_id: current_admin.id,
        admin_username: current_admin.username
      )
      error_detail!(@log) if !@log.save
    else
      error!("要操作的帐号不存在")
    end
  end

  private

  def allowed_params
    params.permit(
      :username,
      :operation_type,
      :description
    )
  end

end

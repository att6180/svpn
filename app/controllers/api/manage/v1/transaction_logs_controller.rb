class Api::Manage::V1::TransactionLogsController < Api::Manage::V1::ApiController

  swagger_controller :transaction_logs, "充值信息"

  swagger_api :index do |api|
    summary "运营行为 - 充值信息 - 充值记录"
    notes ""
    Api::Manage::V1::ApiController.common_params(api)
    Api::Manage::V1::ApiController.version_params(api)
    param :query, :payment_method, :string, :optional, "支付方式, unknow: 未知, iap: IAP，剩下的对应参数设置中的支付方式"
    param :query, :status, :string, :optional, "订单状态, pending: 未支付 | paid: 已支付"
    param :query, :start_at, :string, :optional, "日期筛选开始时间, 如: 2017-04-11"
    param :query, :end_at, :string, :optional, "日期筛选结束时间, 如: 2017-05-11"
    Api::Manage::V1::ApiController.page_params(api)
  end
  def index
    can_access?(:operation_behaviour)

    logs = TransactionLog.page(param_page).per(param_limit).recent
    logs = handle_channel_params_unstat(logs)

    @logs = logs.includes(:user, :plan)
      .the_where(
        'created_at-gte': params[:start_at]&.to_date&.beginning_of_day,
        'created_at-lte': params[:end_at]&.to_date&.end_of_day,
        payment_method: params[:payment_method]&.downcase,
        status: params[:status]
      )
  end

  swagger_api :payment_method_collects do |api|
    summary "运营行为 - 充值信息 - 充值记录 - 支付方式统计"
    notes ""
    Api::Manage::V1::ApiController.common_params(api)
    Api::Manage::V1::ApiController.version_params(api)
    Api::Manage::V1::ApiController.between_date_params(api)
  end
  def payment_method_collects
    requires! :start_at, type: String
    requires! :end_at, type: String

    can_access?(:operation_behaviour)

    logs = TransactionLog
      .the_where({
        'created_at-gte': params[:start_at]&.to_date&.beginning_of_day,
        'created_at-lte': params[:end_at]&.to_date&.end_of_day,
        app_channel: params[:app_channel],
        app_version: params[:app_version]
      })
      .select("payment_method, status, COUNT(user_id) as logs_count")
      .group(:payment_method, :status)

    logs = logs.to_a.map(&:serializable_hash)
      .index_by{|item| "#{item['payment_method']}_#{item['status']}"}

    # IAP统计
    @iap_success_count = payment_method_count(logs, 'iap', 'paid')
    @iap_failure_count = payment_method_count(logs, 'iap', 'pending')
    @iap_total_count = @iap_success_count + @iap_failure_count
    # 支付宝扫码
    @alipay_qc_success_count = payment_method_count(logs, 'alipay_qrcode', 'paid')
    @alipay_qc_failure_count = payment_method_count(logs, 'alipay_qrcode', 'pending')
    @alipay_qc_total_count = @alipay_qc_success_count + @alipay_qc_failure_count
    # 支付宝h5
    @alipay_h5_success_count = payment_method_count(logs, 'alipay_h5', 'paid')
    @alipay_h5_failure_count = payment_method_count(logs, 'alipay_h5', 'pending')
    @alipay_h5_total_count = @alipay_h5_success_count + @alipay_h5_failure_count
    # 支付宝原生
    @alipay_origin_success_count = payment_method_count(logs, 'alipay_origin', 'paid')
    @alipay_origin_failure_count = payment_method_count(logs, 'alipay_origin', 'pending')
    @alipay_origin_total_count = @alipay_origin_success_count + @alipay_origin_failure_count
    # 微信扫码
    @weixin_qc_success_count = payment_method_count(logs, 'wx_qrcode', 'paid')
    @weixin_qc_failure_count = payment_method_count(logs, 'wx_qrcode', 'pending')
    @weixin_qc_total_count = @weixin_qc_success_count + @weixin_qc_failure_count
    # 微信h5b
    @weixin_h5b_success_count = payment_method_count(logs, 'wx_h5b', 'paid')
    @weixin_h5b_failure_count = payment_method_count(logs, 'wx_h5b', 'pending')
    @weixin_h5b_total_count = @weixin_h5b_success_count + @weixin_h5b_failure_count
    # 微信h5
    @weixin_h5_success_count = payment_method_count(logs, 'wx_h5', 'paid')
    @weixin_h5_failure_count = payment_method_count(logs, 'wx_h5', 'pending')
    @weixin_h5_total_count = @weixin_h5_success_count + @weixin_h5_failure_count
    # QQ钱包
    @qq_success_count = payment_method_count(logs, 'qq', 'paid')
    @qq_failure_count = payment_method_count(logs, 'qq', 'pending')
    @qq_total_count = @qq_success_count + @qq_failure_count
    # 未知
    @unknow_success_count = payment_method_count(logs, 'unknow', 'paid')
    @unknow_failure_count = payment_method_count(logs, 'unknow', 'pending')
    @unknow_total_count = @unknow_success_count + @unknow_failure_count
    # 谷歌支付
    @google_pay_success_count = payment_method_count(logs, 'google_pay', 'paid')
    @google_pay_failure_count = payment_method_count(logs, 'google_pay', 'pending')
    @google_pay_total_count = @google_pay_success_count + @google_pay_failure_count
    # 银联支付
    @union_wap_success_count = payment_method_count(logs, 'union_wap', 'paid')
    @union_wap_failure_count = payment_method_count(logs, 'union_wap', 'pending')
    @union_wap_total_count = @union_wap_success_count + @union_wap_failure_count
    # 总笔数
    @total_count = [
      @iap_total_count,
      @alipay_qc_total_count,
      @alipay_h5_total_count,
      @weixin_qc_total_count,
      @weixin_h5_total_count,
      @weixin_h5b_total_count,
      @qq_total_count,
      @unknow_total_count,
      @google_pay_total_count,
      @union_wap_total_count,].sum

  end

  swagger_api :users_count_collects do |api|
    summary "运营行为 - 充值信息 - 人数统计"
    notes "页面顶部的统计"
    Api::Manage::V1::ApiController.common_params(api)
    Api::Manage::V1::ApiController.version_params(api)
  end
  def users_count_collects
    can_access?(:operation_behaviour)

    channel = params[:app_channel]
    version = params[:app_version]

    if channel.blank? && version.blank?
      # 总用户数
      @users_count = User.users_count
      # 充值用户数
      @recharge_users_count = User.paid_users_count
      # 非充值用户数
      @unrecharge_users_count = @users_count - @recharge_users_count
      # 充值总金额
      @total_recharge_amount = TransactionLog.total_recharge_amount
    else
      @users_count, @recharge_users_count, @unrecharge_users_count, @total_recharge_amount = collect_stat(channel, version)
    end
  end

  swagger_api :details do |api|
    summary "运营行为 - 充值信息 - 付费信息 - 充值详情"
    notes <<-END
    total_recharge_amount: 充值金额
    total_consume_coins: 消费钻石
    recharge_users_count: 充值人数
    renew_users_count: 续费人数
    renew_users_rate: 续费人数占比
    renew_new_users_count: 新增续费人数
    renew_new_users_rate: 新增续费人数占比
    renew_amount: 续费用户充值金额
    renew_amount_rate: 续费用户充值金额占比
    renew_new_amount: 新增付费用户充值金额
    renew_new_amount_rate: 新增付费用户充值金额占比
    END
    Api::Manage::V1::ApiController.common_params(api)
    Api::Manage::V1::ApiController.page_params(api)
    param :query, :year, :integer, :optional, "年份, 默认为当前年份"
    param :query, :month, :integer, :optional, "月份, 默认为当前月份"
    Api::Manage::V1::ApiController.version_params(api)
  end
  def details
    can_access?(:operation_behaviour)

    year = params[:year] || Time.now.year
    month = params[:month]

    @has_month = month.present?
    if @has_month
      logs = TransactionStat.by_year_and_month(year, month).order(stat_date: :desc)
    else
      logs = TransactionMonthStat.by_year(year).order(year: :desc, month: :desc)
    end
    logs = handle_version_params(logs)
    @logs = logs.page(param_page).per(param_limit)
  end

  swagger_api :details_chart do |api|
    summary "运营行为 - 充值信息 - 付费信息 - 充值详情 - 图表"
    notes ""
    Api::Manage::V1::ApiController.common_params(api)
    param :query, :year, :integer, :optional, "年份, 默认为当前年份"
    param :query, :month, :integer, :optional, "月份, 默认为当前月份"
    Api::Manage::V1::ApiController.version_params(api)
  end
  def details_chart
    can_access?(:operation_behaviour)

    year = params[:year] || Time.now.year
    month = params[:month]

    @has_month = month.present?
    if @has_month
      logs = TransactionStat.by_year_and_month(year, month).order(stat_date: :asc)
    else
      logs = TransactionMonthStat.by_year(year).order(year: :asc, month: :asc)
    end
    logs = handle_version_params(logs)
    @logs = logs
  end

  swagger_api :date_details do |api|
    summary "运营行为 - 充值信息 - 付费信息 - 充值详情 - 按天"
    notes <<-EOF
    此接口的表格和图表使用同一数据
    logs是当天，prev_logs是前一天
    stat_date: 统计日期，默认会有两个，一个是当前日期，一个是前一天
    stat_hour: 小时段
    total_recharge_amount: 充值金额
    recharge_users_count: 充值人数(去重)
    EOF
    Api::Manage::V1::ApiController.common_params(api)
    Api::Manage::V1::ApiController.package_params(api)
    Api::Manage::V1::ApiController.version_params(api)
    param :query, :date, :string, :required, "指定日期"
  end
  def date_details
    can_access?(:operation_behaviour)

    @date = params[:date].to_date
    if @date.today?
      logs = TransactionLog.by_date(@date)
        .by_paid
        .without_iap
        .select_sum_price
        .select_distinct_users
        .group_by_hour
        .use_index_created_at_status

      logs = handle_muilt_version_params(logs)
      log_indexs = logs.to_a.map(&:serializable_hash).index_by{|l| l["h"]}
      today_logs = []
      (0..23).each_with_index do |i|
        today_logs.push(log_indexs.has_key?(i) ? log_indexs[i].symbolize_keys! : { h: i, users_count: 0, total_price: "0.0" })
      end
      @logs = today_logs
      @total_price = today_logs.map.sum{|l| l[:total_price]&.to_f}
    else
      logs = TransactionHourStat.select(:stat_date, :stat_hour)
        .by_date(@date)
        .select_sum_price
        .select_sum_users
        .group_by_hour
        .sorted
      logs = handle_muilt_version_histroy_params(logs)
      @logs = logs
      @total_price = logs.map.sum{|l| l.total_recharge_amount}
    end

    logs = TransactionHourStat.select(:stat_date, :stat_hour)
      .by_date(@date.prev_day)
      .select_sum_price
      .select_sum_users
      .group_by_hour
      .sorted

    logs = handle_muilt_version_histroy_params(logs)
    @prev_logs = logs
    @prev_total_price = logs.map.sum{|l| l.total_recharge_amount}
  end

  swagger_api :recharge_amount_details do |api|
    summary "运营行为 - 充值信息 - 付费信息 - 充值金额信息详情"
    notes ""
    Api::Manage::V1::ApiController.common_params(api)
    Api::Manage::V1::ApiController.version_params(api)
    param :query, :date_type, :string, :required, "日期类型，day:按天, month:按月"
    param :query, :date, :string, :required, "指定筛选日期或年月，如果date_type参数为day，则应为完整日期, 如: 2017-05-01, " \
      "如果date_type参数为month，则为年月，如: 2017-05"
    Api::Manage::V1::ApiController.page_params(api)
  end
  def recharge_amount_details
    can_access?(:operation_behaviour)

    requires! :date_type, type: String
    requires! :date, type: String
    optional! :app_channel, type: String
    optional! :app_version, type: String
    optional! :page, type: Integer
    optional! :limit, type: Integer

    if params[:date_type] == 'day'
      logs = TransactionLog.by_date(params[:date].to_date)
    else
      date = "#{params[:date]}-01".to_date
      logs = TransactionLog.by_year_and_month(date.year, date.month)
    end
    logs = handle_channel_params_unstat(logs)
    @logs = logs.by_paid.includes(:user, :plan).page(param_page).per(param_limit)
  end

  swagger_api :recharge_user_details do |api|
    summary "运营行为 - 充值信息 - 付费信息 - 充值人数信息详情"
    notes ""
    Api::Manage::V1::ApiController.common_params(api)
    Api::Manage::V1::ApiController.version_params(api)
    param :query, :date_type, :string, :required, "日期类型，day:按天, month:按月"
    param :query, :date, :string, :required, "指定筛选日期或年月，如果date_type参数为day，则应为完整日期, 如: 2017-05-01, " \
      "如果date_type参数为month，则为年月，如: 2017-05"
    Api::Manage::V1::ApiController.page_params(api)
  end
  def recharge_user_details
    can_access?(:operation_behaviour)

    requires! :date_type, type: String
    requires! :date, type: String
    optional! :app_channel, type: String
    optional! :app_version, type: String
    optional! :page, type: Integer
    optional! :limit, type: Integer

    logs = TransactionLog.select("user_id, SUM(price) as amount")
    if params[:date_type] == 'day'
      logs = logs.by_date(params[:date].to_date)
    else
      date = "#{params[:date]}-01".to_date
      logs = logs.by_year_and_month(date.year, date.month)
    end
    logs = handle_channel_params_unstat(logs)
    @logs = logs.by_paid.group(:user_id).includes(:user).page(param_page).per(param_limit)
  end

  swagger_api :consume_coins_details do |api|
    summary "运营行为 - 充值信息 - 付费信息 - 消费钻石信息详情"
    notes ""
    Api::Manage::V1::ApiController.common_params(api)
    param :query, :date_type, :string, :required, "日期类型，day:按天, month:按月"
    param :query, :date, :string, :required, "指定筛选日期或年月，如果date_type参数为day，则应为完整日期, 如: 2017-05-01, " \
      "如果date_type参数为month，则为年月，如: 2017-05"
    Api::Manage::V1::ApiController.page_params(api)
  end
  def consume_coins_details
    can_access?(:operation_behaviour)

    requires! :date_type, type: String
    requires! :date, type: String
    optional! :app_channel, type: String
    optional! :app_version, type: String
    optional! :page, type: Integer
    optional! :limit, type: Integer

    if params[:date_type] == 'day'
      logs = ConsumptionLog.by_date(params[:date].to_date)
    else
      date = "#{params[:date]}-01".to_date
      logs = ConsumptionLog.by_year_and_month(date.year, date.month)
    end
    logs = handle_channel_params_unstat(logs)
    @logs = logs.includes(:user).page(param_page).per(param_limit)
  end

  swagger_api :collects do |api|
    summary "运营行为 - 充值信息 - 付费信息 - 充值金额汇总 / 消费钻石汇总 / 充值人数汇总"
    notes ""
    Api::Manage::V1::ApiController.common_params(api)
  end
  def collects
    can_access?(:operation_behaviour)

    date = Time.now.to_date
    year = date.year
    month = date.month

    # 充值金额汇总, 今日
    @total_recharge_amount_today = TransactionLog.recharge_amount_today
    # 充值金额汇总, 本月
    @total_recharge_amount_month = Rails.cache.fetch("transaction_total_recharge_amount_month", expires_in: 5.minutes) do
      # 取出本月初至昨天的总额 + 今天的总额
      begin_yesterday = TransactionStat.by_year_and_month(year, month).channel_version_total.sum(:total_recharge_amount)
      begin_yesterday + @total_recharge_amount_today
    end
    @total_recharge_amount_year = Rails.cache.fetch("transaction_total_recharge_amount_year", expires_in: 5.minutes) do
      # 取出本年初至上月的总额 + 本月的总额
      begin_last_month = TransactionMonthStat.by_year(year).channel_version_total.sum(:total_recharge_amount)
      begin_last_month + @total_recharge_amount_month
    end
    # 消费钻石汇总，今日
    @total_consume_coins_today = ConsumptionLog.consume_coins_today
    @total_consume_coins_month = Rails.cache.fetch("transaction_total_consume_coins_month", expires_in: 5.minutes) do
      # 取出本月初至昨天的总额 + 今天的总额
      begin_yesterday = TransactionStat.by_year_and_month(year, month).channel_version_total.sum(:consume_coins)
      begin_yesterday + @total_consume_coins_today
    end
    @total_consume_coins_year = Rails.cache.fetch("transaction_total_consume_coins_year", expires_in: 5.minutes) do
      # 取出本年初至上月的总额 + 本月的总额
      begin_last_month = TransactionMonthStat.by_year(year).channel_version_total.sum(:consume_coins)
      begin_last_month + @total_consume_coins_month
    end
    # 充值人数去重汇总
    @total_recharge_users_count_today = TransactionLog.recharge_users_today
    @total_recharge_users_count_month = TransactionLog.recharge_users_month
    @total_recharge_users_count_year = TransactionLog.recharge_users_year
  end

  swagger_api :transaction_fix_index do |api|
    summary "配置管理 - 用户管理 - 补发订单查询"
    notes ""
    Api::Manage::V1::ApiController.common_params(api)
    Api::Manage::V1::ApiController.page_params(api)
    param :query, :user_id, :string, :optional, "用户ID"
    param :query, :order_number, :string, :optional, "订单号"
    param :query, :cs_order_number, :string, :optional, "cs平台订单号"
    param :query, :channel_order_number, :string, :optional, "渠道订单号"
    param :query, :start_at, :string, :optional, "日期筛选开始时间, 如: 2017-04-11"
    param :query, :end_at, :string, :optional, "日期筛选结束时间, 如: 2017-05-11"
  end
  def transaction_fix_index
    can_access?(:user_manage)

    user_id = params[:user_id]
    order_number = params[:order_number]
    cs_order_number = params[:cs_order_number]
    channel_order_number = params[:channel_order_number]
    start_at = params[:start_at] || Time.now.beginning_of_month.to_s
    end_at = params[:end_at] || Time.now.end_of_month.to_s

    if user_id.present?
      error!("用户ID有误，请重新输入。") and return if user_id.strip.to_i > 2147483648 || user_id.strip.to_i <= 0
    end
    
    options = {
      app_id: CONFIG.comsunny_app_id,
      app_key: CONFIG.comsunny_app_secret,
      order_id: order_number,
      cs_order_id: cs_order_number,
      channel_order_id: channel_order_number,
      start_at: start_at,
      end_at: end_at
    }

    @logs = TransactionLog.recent
      .the_where(order_number: order_number, user_id: user_id)
      .includes(:user).page(param_page).per(param_limit)

    result = Comsunny.query_order(options)
    if result["result_code"] == 0
      @comsunny_result = ComsunnyQueryResult.new(result)
      @logs = @logs.where(order_number: @comsunny_result.transaction_id)
    end
    error!(result["err_detail"]) and return if result["result_code"] != 0 && 
      (cs_order_number.present? || channel_order_number.present?)
  end

  swagger_api :transaction_fix do |api|
    summary "配置管理 - 用户管理 - 补发订单"
    notes "对指定未完成支付的订单进行主动查询，如果支付平台已完成支付，则对用户执行订单完成动作"
    Api::Manage::V1::ApiController.common_params(api)
    param :query, :order_number, :string, :required, "订单号"
    param :query, :fix_type, :integer, :optional, "补发类型: 1:Comsunny, 2:支付宝原生, 默认为1"
  end
  def transaction_fix
    can_access?(:user_manage)

    order_number = params[:order_number]
    fix_type = params[:fix_type] || '1'

    log = TransactionLog.find_by(order_number: order_number)
    error!('订单未找到') and return if log.blank?
    error!("订单已经无成支付，无需补发") and return if log.paid?

    if fix_type != '1'
      # 如果为支付宝原生支付
      @client = Alipay::Client.new(
        url: CONFIG.alipay_api_url,
        app_id: CONFIG.alipay_app_id,
        app_private_key: CONFIG.alipay_private_key,
        alipay_public_key: CONFIG.alipay_public_key
      )
      response = @client.execute(
        method: 'alipay.trade.query',
        biz_content: {
          #trade_no: order_number,
          out_trade_no: order_number,
        }.to_json
      )
      # 支付宝查询接口返回格式
      # {"code"=>"10000", "msg"=>"Success", "buyer_logon_id"=>"hem***@126.com", "buyer_pay_amount"=>"0.00",
      # "buyer_user_id"=>"2088102838690465", "invoice_amount"=>"0.00", "out_trade_no"=>"201711221511342553338ab9d",
      # "point_amount"=>"0.00", "receipt_amount"=>"0.00",
      # "send_pay_date"=>"2017-11-22 17:24:55", "total_amount"=>"1.00", "trade_no"=>"2017112221001104460520216142", "trade_status"=>"TRADE_SUCCESS"}

      # Comsunny接口返回格式
      # "bills"=>[{"create_time"=>1510018943503, "operator_id"=>"", "channel"=>"BC",
      # "bill_no"=>"201711071510018943863973f3", "optional"=>"{\"tag\":\"msgtoreturn\"}",
      # "revert_result"=>false, "success_time"=>1510018957577, "title"=>"消费", "spay_result"=>true,
      # "total_fee"=>100, "trade_no"=>"201711071510018943863973f3",
      # "id"=>"ca696a80-4970-41b6-94d2-5bec4fab2b07", "sub_channel"=>"BC_ALI_QRCODE", "refund_result"=>false, "seller_id"=>""}]}
      begin
        result = JSON.parse(response)["alipay_trade_query_response"]
      rescue
        error!('查询支付宝原生订单异常') and return
      end
      error!("支付宝原生平台订单不存在或状态未完成，无法补发") and return if result["trade_status"] != 'TRADE_SUCCESS'
      cq_result = {
        bills: [
          success_time: result["send_pay_date"].to_datetime.to_i * 1000,
          channel: "BC",
          sub_channel: "ORIGIN_ALIPAY",
          bill_no: result["trade_no"],
          total_fee: result["total_amount"].to_i * 100,
          spay_result: result["trade_status"] == "TRADE_SUCCESS",
          optional: ""
        ]
      }.to_json
      comsunny_result = ComsunnyQueryResult.new(JSON.parse(cq_result))
      error!("支付宝平台订单和系统订单金额不一致，无法补发，请联系开发人员") and return if !comsunny_result.fee_correct?(log.price)
    else
      # 如果是Comsunny支付
      options = {
        app_id: CONFIG.comsunny_app_id,
        app_key: CONFIG.comsunny_app_secret,
        order_id: order_number
      }
      result = Comsunny.query_order(options)
      # 状态码0为正常
      error!(result["err_detail"]) and return if result["result_code"] != 0
      comsunny_result = ComsunnyQueryResult.new(result)
      error!("comsunny平台订单不存在或状态未完成，无法补发") and return if !comsunny_result.is_paid?
      error!("comsunny平台订单和系统订单金额不一致，无法补发，请联系开发人员") and return if !comsunny_result.fee_correct?(log.price)
    end
    # 补发
    log.payment_for_comsunny!(log.user, comsunny_result)
  end

  private

  def collect_stat(channel, version)
    return nil if channel.blank? && version.blank?
    users_count = StatService.signup_users_count(channel, version)

    logs = TransactionLog.select("COUNT(DISTINCT user_id) AS users_count, SUM(price) AS total_price").by_paid.without_iap
    logs = logs.where(app_channel: channel) if channel.present?
    logs = logs.where(app_version: version) if version.present?
    log = logs.take

    recharge_users_count = log&.users_count || 0
    unrecharge_users_count = users_count - recharge_users_count
    total_recharge_amount = log&.total_price || 0

    [users_count, recharge_users_count, unrecharge_users_count, total_recharge_amount]
  end

  def payment_method_count(logs, method, status)
    key = "#{method}_#{status}"
    if logs.has_key?(key)
      logs[key]['logs_count']
    else
      0
    end
  end

end

class Api::Manage::V1::PopupAdStatsController < Api::Manage::V1::ApiController

  # 设置权限
  include PermissionFilterable
  set_module_group_name :operation_behaviour

  swagger_controller :popup_ad_stats, "广告统计 - 弹出广告统计"

  swagger_api :hour_index do |api|
    summary "广告统计 - 弹出广告统计 - 小时统计"
    notes ""
    Api::Manage::V1::ApiController.common_params(api)
    param :query, :date, :string, :required, "统计日期, 如: 2017-01-01"
  end
  def hour_index
    @date = params[:date]
    @prev_date = @date.to_date.prev_day
    logs = Log::PopupAdLog.where(created_date: [@date, @prev_date])
      .group_by_date_and_hour
      .select("COUNT(id) as showed_count")
    # 选按日期分组
    today_log_indexs = logs.select{|l| l.d == @date.to_date}
    prev_log_indexs = logs.select{|l| l.d == @prev_date.to_date}

    today_log_hour_indexs = today_log_indexs.map(&:serializable_hash).index_by{|l| l["h"]}
    @logs = []
    (0..23).each_with_index do |i|
      @logs.push(today_log_hour_indexs.has_key?(i) ? today_log_hour_indexs[i].symbolize_keys! : {d: @date,  h: i, showed_count: 0 })
    end

    prev_log_hour_indexs = prev_log_indexs.map(&:serializable_hash).index_by{|l| l["h"]}
    @prev_logs = []
    (0..23).each_with_index do |i|
      @prev_logs.push(prev_log_hour_indexs.has_key?(i) ? prev_log_hour_indexs[i].symbolize_keys! : {d: @prev_date, h: i, showed_count: 0 })
    end
  end

  swagger_api :day_index do |api|
    summary "广告统计 - 弹出广告统计 - 日统计"
    notes "stat_hour和end_hour必须成对传值"
    Api::Manage::V1::ApiController.common_params(api)
    param :query, :start_date, :string, :required, "起始日期, 如: 2017-01-01，默认为当月1日"
    param :query, :end_date, :string, :required, "结束日期, 如: 2017-01-03，默认为当前日期"
  end
  def day_index
    start_date = params[:start_date]&.to_date || Time.now.beginning_of_month.to_date
    end_date = params[:end_date]&.to_date || Time.now.end_of_month.to_date

    error!("开始和结束日期不能超过60天") and return if (end_date - start_date).to_i > 60

    logs = Log::PopupAdDayStat.between_date(start_date, end_date)
      .order(stat_date: :desc)
    click_logs = Log::PopupAdClickDayStat.between_date(start_date, end_date)
      .order(stat_date: :desc)
    logs.map{|log| log.total_clicked_count =
      click_logs.select{|c| c.stat_date == log.stat_date}.first&.clicked_count || 0}
    @logs = logs
  end

  swagger_api :month_index do |api|
    summary "广告统计 - 开屏广告统计 - 月统计"
    notes "stat_hour和end_hour必须成对传值"
    Api::Manage::V1::ApiController.common_params(api)
    param :query, :start_month, :string, :required, "起始月份, 如: 2018-01，默认为当前月份的上个月份"
    param :query, :end_month, :string, :required, "结束月份, 如: 2018-02，默认为当前月份"
  end
  def month_index
    start_date = "#{params[:start_month]}-01"&.to_date || Time.now.prev_month.beginning_of_month.to_date
    end_date = "#{params[:end_month]}-01"&.to_date&.end_of_month || Time.now.end_of_month.to_date

    if (end_date.year * 12 + end_date.month) - (start_date.year * 12 + start_date.month) > 24
      error!("开始和结束月份不能超过24个月") and return
    end

    logs = Log::PopupAdDayStat.between_date(start_date, end_date)
      .group_by_month
    click_logs = Log::PopupAdClickDayStat.between_date(start_date, end_date)
      .group_by_month
    logs.map{|log| log.total_clicked_count =
      click_logs.select{|c| c.m == log.m && c.y = log.y}.first&.total_clicked_count || 0}
    @logs = logs
  end

end

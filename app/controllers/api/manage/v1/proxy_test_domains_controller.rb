class Api::Manage::V1::ProxyTestDomainsController < Api::Manage::V1::ApiController

  # 设置权限
  include PermissionFilterable
  set_module_group_name :node_manage

  before_action :set_item, only: [:update, :show, :destroy]

  swagger_controller :proxy_test_domains, "机器人测试域名配置"

  swagger_api :index do |api|
    summary "服务器群组管理 - 机器人测试域名配置 - 列表"
    notes ""
    Api::Manage::V1::ApiController.common_params(api)
    Api::Manage::V1::ApiController.page_params(api)
    param :query, :is_domestic, :string, :optional, "是否为国内域名, true:是, false:否"
  end
  def index
    items = ProxyTestDomain.recent
    items = items.where(is_domestic: params[:is_domestic] == 'true') if params[:is_domestic].present?
    @items = items.page(param_page).per(param_limit)
  end

  swagger_api :create do |api|
    summary "服务器群组管理 - 机器人测试域名配置 - 创建"
    notes ""
    Api::Manage::V1::ApiController.common_params(api)
    param :form, :domain, :string, :required, "完整域名地址"
    param :form, :is_domestic, :string, :required, "是否是国内域名，true:是, false:否"
  end
  def create
    optional! :domain, type: String
    optional! :is_domestic, type: String

    @item = ProxyTestDomain.new(allowed_params)
    error_detail!(@item) if !@item.save
  end

  swagger_api :update do |api|
    summary "服务器群组管理 - 机器人测试域名配置 - 更新"
    notes ""
    Api::Manage::V1::ApiController.common_params(api)
    param :path, :id, :integer, :required, "ID"
    param :form, :domain, :string, :optional, "完整域名地址"
    param :form, :is_domestic, :string, :optional, "是否是国内域名，true:是, false:否"
  end
  def update
    requires! :id, type: Integer
    error_detail!(@item) if !@item.update(allowed_params)
  end

  swagger_api :show do |api|
    summary "服务器群组管理 - 机器人测试域名配置 - 单个获取"
    notes ""
    Api::Manage::V1::ApiController.common_params(api)
    param :path, :id, :integer, :required, "ID"
  end
  def show
    requires! :id, type: Integer
  end

  swagger_api :destroy do |api|
    summary "服务器群组管理 - 机器人测试域名配置 - 删除"
    notes ""
    Api::Manage::V1::ApiController.common_params(api)
    param :path, :id, :integer, :required, "ID"
  end
  def destroy
    requires! :id, type: Integer

    @item.destroy if @item.present?
  end

  private

  def allowed_params
    params.permit(
      :domain,
      :is_domestic
    )
  end

  def set_item
    @item = ProxyTestDomain.find(params[:id])
  end
end

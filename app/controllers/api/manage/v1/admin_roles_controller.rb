class Api::Manage::V1::AdminRolesController < Api::Manage::V1::ApiController

  # 设置权限
  include PermissionFilterable
  set_module_group_name :back_stage_manage

  before_action :set_item, only: [:update, :destroy]

  swagger_controller :admin_roles, "管理权限组管理"

  swagger_api :index do |api|
    summary "管理权限组管理 - 列表"
    notes ""
    Api::Manage::V1::ApiController.common_params(api)
    Api::Manage::V1::ApiController.page_params(api)
  end
  def index
    optional! :page, type: Integer
    optional! :limit, type: Integer

    @roles = ::Manage::Role.except_reserved.sorted_by_id.page(param_page).per(param_limit)
  end

  swagger_api :create do |api|
    summary "管理权限组管理 - 创建"
    notes ""
    Api::Manage::V1::ApiController.common_params(api)
    param :form, :name, :string, :required, "权限组名称"
    param :form, :manage_role_create, :string, :optional, "新增权限组, 所有下面的参数，是: true|1, 否: false|0"
    param :form, :manage_role_update, :string, :optional, "修改权限组"
    param :form, :manage_role_delete, :string, :optional, "删除权限组"
    param :form, :admin_password_update, :string, :optional, "修改管理员密码"
    param :form, :admin_role_update, :string, :optional, "修改管理员权限组"
    param :form, :user_info_update, :string, :optional, "修改用户信息"
    param :form, :user_base_info, :string, :optional, "查看用户基础信息"
    param :form, :user_behaviour, :string, :optional, "用户行为"
    param :form, :operation_behaviour, :string, :optional, "运营行为"
    param :form, :charge_manage, :string, :optional, "收费管理"
    param :form, :back_stage_manage, :string, :optional, "后台管理"
    param :form, :user_manage, :string, :optional, "用户管理"
    param :form, :node_manage, :string, :optional, "服务器群组管理"
    param :form, :system_setting, :string, :optional, "系统配置"
    param :form, :system_enum, :string, :optional, "渠道版本配置"
    param :form, :help_manual, :string, :optional, "帮助问答管理"
    param :form, :service_setting, :string, :optional, "服务设置管理"
  end
  def create
    requires! :name, type: String
    optional! :manage_role_create, type: String
    optional! :manage_role_update, type: String
    optional! :manage_role_delete, type: String
    optional! :admin_password_update, type: String
    optional! :admin_role_update, type: String
    optional! :user_info_update, type: String
    optional! :user_base_info, type: String
    optional! :user_behaviour, type: String
    optional! :operation_behaviour, type: String
    optional! :charge_manage, type: String
    optional! :back_stage_manage, type: String
    optional! :user_manage, type: String
    optional! :node_manage, type: String
    optional! :system_setting, type: String
    optional! :system_enum, type: String
    optional! :help_manual, type: String
    optional! :service_setting, type: String

    can_access?(:manage_role_create)

    @role = ::Manage::Role.new(allowed_params)

    if %w(super_admin guest).include?(@role.name)
      error!('权限组名为系统保留权限组名称，请更换') and return
    end

    if @role.save
      current_admin.create_log( key: 'create_admin_role', params: { name: @role.name })
    else
      error_detail!(@role)
    end
  end

  swagger_api :update do |api|
    summary "管理员权限组管理 - 更新"
    notes ""
    Api::Manage::V1::ApiController.common_params(api)
    param :path, :id, :integer, :required, "权限组ID"
    param :form, :name, :string, :optional, "权限组名称"
    param :form, :manage_role_create, :string, :optional, "新增权限组"
    param :form, :manage_role_update, :string, :optional, "修改权限组"
    param :form, :manage_role_delete, :string, :optional, "删除权限组"
    param :form, :admin_password_update, :string, :optional, "修改管理员密码"
    param :form, :admin_role_update, :string, :optional, "修改管理员权限组"
    param :form, :user_info_update, :string, :optional, "修改用户信息"
    param :form, :user_base_info, :string, :optional, "查看用户基础信息"
    param :form, :user_behaviour, :string, :optional, "用户行为"
    param :form, :operation_behaviour, :string, :optional, "运营行为"
    param :form, :charge_manage, :string, :optional, "收费管理"
    param :form, :back_stage_manage, :string, :optional, "后台管理"
    param :form, :user_manage, :string, :optional, "用户管理"
    param :form, :node_manage, :string, :optional, "服务器群组管理"
    param :form, :system_setting, :string, :optional, "系统配置"
    param :form, :system_enum, :string, :optional, "渠道版本配置"
    param :form, :help_manual, :string, :optional, "帮助问答管理"
    param :form, :service_setting, :string, :optional, "服务设置管理"
  end
  def update
    requires! :id, type: Integer
    optional! :name, type: String
    optional! :manage_role_create, type: String
    optional! :manage_role_update, type: String
    optional! :manage_role_delete, type: String
    optional! :admin_password_update, type: String
    optional! :admin_role_update, type: String
    optional! :user_info_update, type: String
    optional! :user_base_info, type: String
    optional! :user_behaviour, type: String
    optional! :operation_behaviour, type: String
    optional! :charge_manage, type: String
    optional! :back_stage_manage, type: String
    optional! :user_manage, type: String
    optional! :node_manage, type: String
    optional! :system_setting, type: String
    optional! :system_enum, type: String
    optional! :help_manual, type: String
    optional! :service_setting, type: String

    can_access?(:manage_role_update)

    if %w(super_admin guest).include?(@role.name)
      error!('系统保留权限组不可修改') and return
    end

    if @role.update(allowed_params)
      current_admin.create_log( key: 'update_admin_role', params: { name: @role.name })
    else
      error_detail!(@role)
    end
  end

  swagger_api :destroy do |api|
    summary "管理员权限组管理 - 删除"
    notes ""
    Api::Manage::V1::ApiController.common_params(api)
    param :path, :id, :integer, :required, "权限组id"
  end
  def destroy
    requires! :id, type: Integer

    can_access?(:manage_role_delete)

    if %w(super_admin guest).include?(@role.name)
      error!('权限组为系统保留权限组，不能删除') and return
    end

    # 将所有是这个权限组的管理员设置为游客组
    guest_role = ::Manage::Role.find_by(name: 'guest')
    admins = ::Manage::Admin.where(role_id: @role.id)
    admins.update_all(role_id: guest_role.id) if admins.present? && guest_role.present?

    if @role.destroy
      current_admin.create_log( key: 'delete_admin_role', params: { name: @role.name })
    else
      error_detail!(@role)
    end
  end

  private

  def allowed_params
    params.permit(
    :name,
    :manage_role_create,
    :manage_role_update,
    :manage_role_delete,
    :admin_password_update,
    :admin_role_update,
    :user_info_update,
    :user_base_info,
    :user_behaviour,
    :operation_behaviour,
    :charge_manage,
    :back_stage_manage,
    :user_manage,
    :node_manage,
    :system_setting,
    :system_enum,
    :help_manual,
    :service_setting
    )
  end

  def set_item
    @role = ::Manage::Role.find(params[:id])
  end
end

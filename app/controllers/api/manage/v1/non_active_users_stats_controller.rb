class Api::Manage::V1::NonActiveUsersStatsController < Api::Manage::V1::ApiController
  # 设置权限
  include PermissionFilterable
  set_module_group_name :operation_behaviour

  swagger_controller :non_active_user_stats, "非活跃用户"

  swagger_api :index do |api|
    summary "非活跃用户 - 列表"
    notes ""
    Api::Manage::V1::ApiController.common_params(api)
    Api::Manage::V1::ApiController.version_params(api)
    param :query, :start_at, :string, :optional, "开始日期, 如: 2017-01-01"
    param :query, :end_at, :string, :optional, "结束日期, 如: 2017-03-01"
  end
  def index
    start_at = params[:start_at]&.to_date || Time.now.beginning_of_month.to_date
    end_at = params[:end_at]&.to_date || Time.now.to_date

    return error!("起始时间和结束时间不能超过60天") if (end_at - start_at).to_i > 60

    logs = Log::NonActiveUserDayStat.order(stat_date: :desc).between_date(start_at, end_at)
    @logs = handle_version_params(logs)
  end
end

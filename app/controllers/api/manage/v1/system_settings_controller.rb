class Api::Manage::V1::SystemSettingsController < Api::Manage::V1::ApiController

  # 设置权限
  include PermissionFilterable
  set_module_group_name :system_setting

  before_action :set_item, only: [:show, :update, :destroy]

  swagger_controller :system_settings, "参数配置"

  swagger_api :index do |api|
    summary "参数配置 - 列表"
    notes ""
    Api::Manage::V1::ApiController.common_params(api)
    Api::Manage::V1::ApiController.page_params(api)
    param :query, :key, :string, :optional, "键名筛选"
    param :query, :platform, :string, :optional, "平台，all/ios/android/pc/mac"
    param :query, :app_channel, :string, :optional, "渠道，如: all/moren等"
    param :query, :app_version, :string, :optional, "版本，如: all/jichu等"
    param :query, :is_client, :string, :optional, "是否是客户端配置, true|false"
  end
  def index
    settings = SystemSetting.order(platform: :asc, app_channel: :asc, app_version: :asc, app_version_number: :asc)
    settings = settings.where(key: params[:key]) if params[:key].present?
    settings = settings.where(platform: params[:platform]) if params[:platform].present?
    settings = settings.where(app_channel: params[:app_channel]) if params[:app_channel].present?
    settings = settings.where(app_version: params[:app_version]) if params[:app_version].present?
    settings = settings.where(is_client: params[:is_client] == 'true') if params[:is_client].present?
    @settings = settings.page(param_page).per(param_limit)
  end

  swagger_api :create do |api|
    summary "参数配置 - 创建"
    notes ""
    Api::Manage::V1::ApiController.common_params(api)
    param :form, :platform, :string, :required, "平台，all/ios/android/pc/mac"
    param :form, :app_channel, :string, :required, "渠道，如: all/moren等"
    param :form, :app_version, :string, :required, "版本，如: all/jichu等"
    param :form, :app_version_number, :string, :optional, "版本号，如: 1.0.1"
    param :form, :key, :string, :required, "键名"
    param :form, :value, :string, :optional, "值"
    param :form, :is_client, :string, :optional, "是否是客户端配置, true|false"
    param :form, :description, :string, :optional, "说明"
  end
  def create
    requires! :platform, type: String
    requires! :app_channel, type: String
    requires! :app_version, type: String
    requires! :key, type: String

    @setting = SystemSetting.new(allowed_params)
    error_detail!(@setting) if !@setting.save
  end

  swagger_api :show do |api|
    summary "参数配置 - 单个获取"
    notes ""
    Api::Manage::V1::ApiController.common_params(api)
    param :path, :id, :integer, :required, "ID"
  end
  def show
    requires! :id, type: Integer
  end

  swagger_api :update do |api|
    summary "参数配置 - 更新"
    notes ""
    Api::Manage::V1::ApiController.common_params(api)
    param :path, :id, :integer, :required, "ID"
    param :form, :platform, :string, :optional, "平台，all/ios/android/pc/mac"
    param :form, :app_channel, :string, :optional, "渠道，如: all/moren等"
    param :form, :app_version, :string, :optional, "版本，如: all/jichu等"
    param :form, :app_version_number, :string, :optional, "版本号，如: 1.0.1"
    param :form, :key, :string, :optional, "键名"
    param :form, :value, :string, :optional, "值"
    param :form, :is_client, :string, :optional, "是否是客户端配置, true|false"
    param :form, :description, :string, :optional, "说明"
  end
  def update
    error_detail!(@setting) if !@setting.update(allowed_params)
  end

  swagger_api :destroy do |api|
    summary "参数配置 - 删除"
    notes ""
    Api::Manage::V1::ApiController.common_params(api)
    param :path, :id, :integer, :required, "ID"
  end
  def destroy
    requires! :id, type: Integer

    error_detail!(@setting) if !@setting.destroy
  end

  swagger_api :upload_routes do |api|
    summary "参数配置 - 上传路由配置文件"
    notes ""
    Api::Manage::V1::ApiController.common_params(api)
    param :form, :ios_d2o, :string, :optional, "ios，国内对国外, 文件名: ios_d2o.conf"
    param :form, :ios_o2d, :string, :optional, "ios，国外对国内, 文件名: ios_o2d.conf"
    param :form, :ios_glo_d2o, :string, :optional, "ios，全局代理需要走直连列表, 文件名: ios_glo_d2o.plist"
    param :form, :android_d2o, :string, :optional, "android，国内对国外, 文件名: android_d2o"
    param :form, :android_o2d, :string, :optional, "android，国外对国内, 文件名: android_o2d"
    param :form, :pc_d2o, :string, :optional, "pc，国内对国外, 文件名: pc_d2o.pac"
    param :form, :pc_o2d, :string, :optional, "pc，国外对国内, 文件名: pc_o2d.pac"
    param :form, :pc_glo_d2o, :string, :optional, "pc，全局代理需要走直连列表, 文件名: pc_glo_d2o.pac"
    param :form, :china_ip, :string, :optional, "国内ip列表, 文件名: china_ip.txt"
    param :form, :foreign_ip, :string, :optional, "国外ip列表, 文件名: foreign_ip.txt"
  end
  def upload_routes
    optional! :ios_d2o, type: String
    optional! :ios_o2d, type: String
    optional! :ios_glo_d2o, type: String
    optional! :android_d2o, type: String
    optional! :android_o2d, type: String
    optional! :pc_d2o, type: String
    optional! :pc_o2d, type: String
    optional! :pc_glo_d2o, type: String
    optional! :mac_d2o, type: String
    optional! :mac_o2d, type: String
    optional! :mac_glo_d2o, type: String
    optional! :china_ip, type: String
    optional! :foreign_ip, type: String

    params_count = 0
    successed_count = 0
    [
      :ios_d2o, :ios_o2d, :ios_glo_d2o, :android_d2o, :android_o2d,
      :pc_d2o, :pc_o2d, :pc_glo_d2o, :mac_d2o, :mac_o2d,
      :mac_glo_d2o, :china_ip, :foreign_ip
    ].each do |sym|
      if params[sym].present?
        params_count += 1
        key = sym.to_s
        file = params[sym]
        status = QiniuStore.upload_route(file.tempfile, file.original_filename)
        if status == 200
          successed_count += 1
          updated_key = key.gsub('_d2o', '').gsub('_o2d', '').gsub('_glo_d2o', '')
          Setting["route_#{updated_key}_updated_at"] = Time.now.to_i
        end
      end
    end
    if params_count <= 0
      error!(api_t("at_lease_one_params"), 1) and return
    end
    if params_count > successed_count
      error!(api_t("at_lease_one_failed"), 2) and return
    end
  end

  private

  def allowed_params
    params.permit(
      :platform,
      :app_channel,
      :app_version,
      :app_version_number,
      :key,
      :value,
      :is_client,
      :description
    )
  end

  def set_item
    @setting = SystemSetting.find(params[:id])
  end

end

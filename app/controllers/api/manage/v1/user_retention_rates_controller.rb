class Api::Manage::V1::UserRetentionRatesController < Api::Manage::V1::ApiController

  # 设置权限
  include PermissionFilterable
  set_module_group_name :operation_behaviour

  swagger_controller :user_retention_rates, "留存"

  swagger_api :index do |api|
    summary "留存 - 列表"
    notes ""
    Api::Manage::V1::ApiController.common_params(api)
    Api::Manage::V1::ApiController.page_params(api)
    Api::Manage::V1::ApiController.version_params(api)
    param :query, :start_at, :string, :optional, "开始年月日, 如: 2017-01-04"
    param :query, :end_at, :string, :optional, "结束年月日, 如: 2017-03-05"
  end
  def index
    # 如果有筛选则查询一遍无筛选的数据进行对比
    total_logs = has_channel_param? ? UserRetentionRateStat.where(filter_type: 0) : nil

    logs = UserRetentionRateStat.all
    logs = handle_version_params(logs)
    if params[:start_at].present? && params[:end_at].present?
      logs = logs.between_months(params[:start_at].to_date, params[:end_at].to_date)
      total_logs = total_logs.between_months(params[:start_at].to_date, params[:end_at].to_date) if total_logs
    else
      today = Time.now.to_date
      logs = logs.between_months(today.beginning_of_month, today.end_of_month)
      total_logs = total_logs.between_months(today.beginning_of_month, today.end_of_month) if total_logs
    end
    @logs = logs.stat_result_user.order("stat_date DESC").page(param_page).per(param_limit)
    if total_logs
      @total_logs = total_logs
        .stat_result_user
        .order("stat_date DESC")
        .page(param_page).per(param_limit)
        .index_by{|log| log.stat_date.strftime("%Y%m%d")}
    end
  end

  swagger_api :chart do |api|
    summary "留存 - 图表"
    notes ""
    Api::Manage::V1::ApiController.common_params(api)
    param :query, :app_version, :string, :optional, "版本筛选"
    param :query, :app_channel, :string, :optional, "渠道筛选"
    param :query, :start_at, :string, :optional, "开始年月, 如: 2017-01-04"
    param :query, :end_at, :string, :optional, "结束年月, 如: 2017-03-05"
  end
  def chart
    optional! :app_version, type: String
    optional! :app_channel, type: String
    optional! :start_at, type: String
    optional! :end_at, type: String

    logs = UserRetentionRateStat.all
    logs = handle_version_params(logs)
    if params[:start_at].present? && params[:end_at].present?
      logs = logs.between_months(params[:start_at].to_date, params[:end_at].to_date)
    else
      today = Time.now.to_date
      logs = logs.between_months(today.beginning_of_month, today.end_of_month)
    end
    @logs = logs.stat_result_user.order("stat_date ASC")
  end

  swagger_api :details do |api|
    summary "留存 - 当日新增用户详情"
    notes ""
    Api::Manage::V1::ApiController.common_params(api)
    Api::Manage::V1::ApiController.page_params(api)
    param :query, :stat_date, :string, :required, "统计日期, 如: 2017-04-11"
    param :query, :app_version, :string, :optional, "版本筛选"
    param :query, :app_channel, :string, :optional, "渠道筛选"
  end
  def details
    requires! :stat_date, type: String
    optional! :page, type: Integer
    optional! :limit, type: Integer
    optional! :app_version, type: String
    optional! :app_channel, type: String

    users = User.page(param_page).per(param_limit)
    users = users.where(create_app_version: params[:app_version]) if params[:app_version].present?
    users = users.where(create_app_channel: params[:app_channel]) if params[:app_channel].present?
    @users = users.by_created_date(params[:stat_date]).recent
  end

  swagger_api :retention_details do |api|
    summary "留存 - 留存数详情"
    notes ""
    Api::Manage::V1::ApiController.common_params(api)
    Api::Manage::V1::ApiController.page_params(api)
    param :query, :stat_date, :string, :required, "统计日期, 如: 2017-04-11"
    param :query, :stat_type, :integer, :required, "统计类型, 1:次日留存, 3:次日留存, 7:七日留存, 15:次日留存, 30: 30日留存"
    param :query, :app_version, :string, :optional, "版本筛选"
    param :query, :app_channel, :string, :optional, "渠道筛选"
  end
  def retention_details
    requires! :stat_date, type: String
    requires! :stat_type, type: Integer
    optional! :page, type: Integer
    optional! :limit, type: Integer
    optional! :app_version, type: String
    optional! :app_channel, type: String

    stat_date = params[:stat_date].to_date
    app_channel = params[:app_channel]
    app_version = params[:app_version]
    logs = NodeConnectionUserDayLog.page(param_page).per(param_limit)
    case params[:stat_type]
    when '1'
      logs = logs.retentions(stat_date, stat_date + 1.days, nil, app_channel, app_version, false)
    when '3'
      logs = logs.retentions(stat_date, stat_date + 2.days, stat_date + 2.days, app_channel, app_version, false)
    when '7'
      logs = logs.retentions(stat_date, stat_date + 6.days, stat_date + 6.days, app_channel, app_version, false)
    when '15'
      logs = logs.retentions(stat_date, stat_date + 14.days, stat_date + 14.days, app_channel, app_version, false)
    when '30'
      logs = logs.retentions(stat_date, stat_date + 29.days, stat_date + 29.days, app_channel, app_version, false)
    end
    @logs = logs.includes(:user)
  end

end

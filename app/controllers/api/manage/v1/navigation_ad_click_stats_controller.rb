class Api::Manage::V1::NavigationAdClickStatsController < Api::Manage::V1::ApiController
  include PermissionFilterable
  set_module_group_name :operation_behaviour

  swagger_controller :navigation_ad_click_stats, "广告统计 - 导航广告"

  swagger_api :ad_list do |api|
    summary "广告统计 - 导航广告 - 导航广告列表"
    notes ""
    Api::Manage::V1::ApiController.common_params(api)
  end
  def ad_list
    @ads = NavigationAd.select(:id, :name)
  end

  swagger_api :hour_index do |api|
    summary "广告统计 - 导航广告 - 小时统计"
    notes ""
    Api::Manage::V1::ApiController.common_params(api)
    param :query, :date, :string, :required, "统计日期, 如: 2017-01-01"
    param :query, :ad_id, :string, :optional, "用户选择广告名筛选中的广告名后，传对应的广告ID"
    param :query, :ad_keyword, :string, :optional, "按广告ID/广告名搜索"
  end
  def hour_index
    @date = params[:date]
    @prev_date = @date.to_date.prev_day
    logs = Log::NavigationAdClickLog
      .the_where(created_date: [@date, @prev_date])
      .group_by_date_and_hour
      .select("COUNT(id) as clicked_count")
    if params[:ad_id].present?
      logs = logs.where(ad_id: params[:ad_id])
    elsif params[:ad_keyword].present?
      if params[:ad_keyword].to_i > 0
        logs = logs.where(ad_id: params[:ad_keyword])
      else
        log = NavigationAd.select(:id).where(name: params[:ad_keyword]).take
        error!("未找到广告") and return if log.blank?
        logs = logs.where(ad_id: log.id)
      end
    end
    # 选按日期分组
    today_log_indexs = logs.select{|l| l.d == @date.to_date}
    prev_log_indexs = logs.select{|l| l.d == @prev_date.to_date}

    today_log_hour_indexs = today_log_indexs.map(&:serializable_hash).index_by{|l| l["h"]}
    @logs = []
    (0..23).each_with_index do |i|
      @logs.push(today_log_hour_indexs.has_key?(i) ? today_log_hour_indexs[i].symbolize_keys! : {d: @date,  h: i, clicked_count: 0 })
    end

    prev_log_hour_indexs = prev_log_indexs.map(&:serializable_hash).index_by{|l| l["h"]}
    @prev_logs = []
    (0..23).each_with_index do |i|
      @prev_logs.push(prev_log_hour_indexs.has_key?(i) ? prev_log_hour_indexs[i].symbolize_keys! : {d: @prev_date, h: i, clicked_count: 0 })
    end
  end

  swagger_api :day_index do |api|
    summary "广告统计 - 导航广告 - 日/月统计"
    notes ""
    Api::Manage::V1::ApiController.common_params(api)
    param :query, :start_date, :string, :required, "起始日期, 如: 2017-01-01，默认为当月1日"
    param :query, :end_date, :string, :required, "结束日期, 如: 2017-01-03，默认为当前日期"
  end
  def day_index
    start_date = params[:start_date]&.to_date || Time.now.beginning_of_month.to_date
    end_date = params[:end_date]&.to_date || Time.now.end_of_month.to_date

    @logs = Log::NavigationAdClickDayStat
      .between_date(start_date, end_date)
      .group_by_ad_name
  end

end

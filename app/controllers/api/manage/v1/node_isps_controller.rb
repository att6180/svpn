class Api::Manage::V1::NodeIspsController < Api::Manage::V1::ApiController

  include PermissionFilterable
  set_module_group_name :node_manage

  before_action :set_node_isp, only: [:update, :destroy]

  swagger_controller :node_isps, "代理商标识管理"

  swagger_api :index do |api|
    summary "代理商标识管理 - 列表"
    notes ""
    Api::Manage::V1::ApiController.common_params(api)
    param :query, :is_enabled, :string, :optional, "true: 筛选已启用的类型, false|忽略参数: 筛选全部"
    param :query, :page, :integer, :optional, "页码，默认: 1"
    param :query, :limit, :integer, :optional, "每页显示的记录数, 默认: 25"
  end
  def index
    optional! :is_enabled, type: String
    optional! :page, type: Integer
    optional! :limit, type: Integer

    node_isps = NodeIsp.sorted_by_enabled
    node_isps = node_isps.where(is_enabled: cast(params[:is_enabled])) if params[:is_enabled].present?
    @node_isps = node_isps.page(param_page).per(param_limit)
  end

  swagger_api :create do |api|
    summary "代理商标识管理 - 创建"
    notes ""
    Api::Manage::V1::ApiController.common_params(api)
    param :form, :name, :string, :required, "代理商标识名"
    param :form, :is_enabled, :string, :optional, "是否启用此类型, true|false"
  end
  def create
    requires! :name, type: String

    @node_isp = NodeIsp.new(allowed_params)
    error_detail!(@node_isp) if !@node_isp.save
  end

  swagger_api :update do |api|
    summary "代理商标识管理 - 更新"
    notes ""
    Api::Manage::V1::ApiController.common_params(api)
    param :path, :id, :integer, :required, "代理商标识ID"
    param :form, :name, :string, :optional, "节点类型名"
    param :form, :is_enabled, :string, :optional, "是否启用此类型, true|false"
  end
  def update
    requires! :id, type: Integer

    error_detail!(@node_isp) if !@node_isp.update(allowed_params)
  end

  private
    def set_node_isp
      @node_isp = NodeIsp.find(params[:id])
    end

    def allowed_params
      params.permit(
        :name,
        :is_enabled
      )
    end
end

class Api::Manage::V1::UserNoOperationLogsController < Api::Manage::V1::ApiController

  # 设置权限
  include PermissionFilterable
  set_module_group_name :user_base_info

  swagger_controller :user_signin_failed_logs, "未操作"

  swagger_api :index do |api|
    summary "用户行为 - 失败信息 - 用户未操作 - 统计"
    notes ""
    Api::Manage::V1::ApiController.common_params(api)
    Api::Manage::V1::ApiController.page_params(api)
    Api::Manage::V1::ApiController.version_params(api)
    Api::Manage::V1::ApiController.between_date_params(api)
  end
  def index
    verify_date_range!
    logs = UserNoOperationLog.all
    if has_channel_param?
      logs = logs.where(app_version: params[:app_version]) if params[:app_version].present?
      logs = logs.where(app_channel: params[:app_channel]) if params[:app_channel].present?
    end
    start_at, end_at = default_start_end_at
    logs = logs.between_date(start_at, end_at)
    logs = logs.group_addition
    @logs = DateUtils.fill_up(logs, start_at, end_at, { no_operations_count: 0 }, true)
  end

  swagger_api :chart do |api|
    summary "用户行为 - 失败信息 - 用户未操作 - 图表"
    notes ""
    Api::Manage::V1::ApiController.common_params(api)
    Api::Manage::V1::ApiController.version_params(api)
    Api::Manage::V1::ApiController.between_date_params(api)
  end
  def chart
    verify_date_range!
    logs = UserNoOperationLog.all
    if has_channel_param?
      logs = logs.where(app_version: params[:app_version]) if params[:app_version].present?
      logs = logs.where(app_channel: params[:app_channel]) if params[:app_channel].present?
    end
    start_at, end_at = default_start_end_at
    logs = logs.between_date(start_at, end_at)
    logs = logs.group_addition
    @logs = DateUtils.fill_up(logs, start_at, end_at, { no_operations_count: 0 }, false)
  end

  swagger_api :detail do |api|
    summary "用户行为 - 失败信息 - 用户未操作 - 详情"
    notes "点击指定统计数字，得到构成此数量的每条信息详情"
    Api::Manage::V1::ApiController.common_params(api)
    param :query, :stat_at, :string, :required, "详情指定的日期，会返回该日期内发生的所有未操作日志"
    Api::Manage::V1::ApiController.version_params(api)
    Api::Manage::V1::ApiController.page_params(api)
  end
  def detail
    requires! :stat_at, type: String

    logs = UserNoOperationLog.page(param_page).per(param_limit)
    logs = logs.where(app_version: params[:app_version]) if params[:app_version].present?
    logs = logs.where(app_channel: params[:app_channel]) if params[:app_channel].present?
    @logs = logs.by_date(params[:stat_at].to_date).includes(:user)
  end

end

class Api::Manage::V1::NodeSwitchLogsController < Api::Manage::V1::ApiController

  swagger_controller :node_switch_logs, "服务器上下架信息"

  swagger_api :collects do |api|
    summary "用户行为 - 服务器信息 - 上下架统计"
    notes <<-EOF
    node_types.nodes_count: 服务器总数
    node_types.nodes_enabled_count: 上架的服务器总数
    node_types.nodes_disabled_count: 下架的服务器总数
    nodes_count: 所有服务器总数量
    nodes_enabled_count: 所有上架的服务器总数量
    nodes_disabled_count: 所有下架的服务器总数量
    EOF
    Api::Manage::V1::ApiController.common_params(api)
  end
  def collects
    can_access?(:user_behaviour)
    @node_types = NodeType.cache_list
  end

  swagger_api :index do |api|
    summary "用户行为 - 服务器信息 - 上下架日志 - 列表"
    notes <<-EOF
    created_at: 时间点
    status: 标识, 1:上架, 0:下架
    node_id: ID
    node_name: 服务器名称
    node_type_name: 服务类型
    ip: 服务器IP
    EOF
    Api::Manage::V1::ApiController.common_params(api)
    param :query, :node_id, :integer, :optional, "服务器ID"
    param :query, :ip, :string, :optional, "服务器IP"
    param :query, :start_at, :string, :optional, "开始时间, 如: 2017-01-04 10:10"
    param :query, :end_at, :string, :optional, "结束时间, 如: 2017-03-04 10:20"
    Api::Manage::V1::ApiController.page_params(api)
  end
  def index
    can_access?(:user_behaviour)
    logs = NodeSwitchLog.recent.page(param_page).per(param_limit)
    logs = logs.where(node_id: params[:node_id]) if params[:node_id].present?
    logs = logs.where(node_ip: params[:ip]) if params[:ip].present?
    if params[:start_at].present? && params[:end_at].present?
      logs = logs.where("DATE(created_at) >= ? AND DATE(created_at) <= ?", params[:start_at].to_date, params[:end_at].to_date)
    end
    @logs = logs.includes(:node)
  end

  swagger_api :status_index do |api|
    summary "用户行为 - 服务器信息 - 上下架状态 - 列表"
    notes <<-EOF
    node_id: 服务器ID
    node_name: 服务器名
    node_type_name: 服务类型名
    ip: 服务器IP
    is_domestic: 是否是国内
    total_failed_rate: 总失败率
    total_test_count: 总次数
    average_response_time: 平均响应(ms)
    last_failed_rate: 最后15次失败率
    last_test_count: 最后次数
    last_test_status: 最后15次状态
    switch_on_count: 上架数
    switch_off_count: 下架数
    created_at: 更新时间
    status: 当前状态, 1:上架, 0:下架
    EOF
    Api::Manage::V1::ApiController.common_params(api)
    param :query, :node_id, :integer, :optional, "服务器ID"
    param :query, :ip, :string, :optional, "服务器IP"
    Api::Manage::V1::ApiController.page_params(api)
  end
  def status_index
    can_access?(:user_behaviour)
    node_ip = params[:ip]
    logs = NodeSwitchStatus.page(param_page).per(params[:limit])
    if node_ip.present?
      node = Node.find_by(url: node_ip)
      if !node.nil?
        logs = logs.where(node_id: node.id)
      else
        logs = nil
      end
    else
      logs = logs.where(node_id: params[:node_id]) if params[:node_id].present?
    end
    if logs.nil?
      @logs = []
    else
      @logs = logs.includes(:node)
      node_types = NodeType.cache_list
      @node_type_indexs = node_types.index_by(&:id)
    end
  end

end

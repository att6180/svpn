class Api::Manage::V1::PopupAdsController < Api::Manage::V1::ApiController

  # 设置权限
  include PermissionFilterable
  set_module_group_name :system_setting

  swagger_controller :popup_ads, "弹出广告相关配置"

  swagger_api :index do |api|
    summary "参数配置 - 弹出广告 - 获取"
    notes ""
    Api::Manage::V1::ApiController.common_params(api)
  end
  def index
  end

  swagger_api :update_settings do |api|
    summary "参数配置 - 开屏广告 - 更新配置"
    notes ""
    Api::Manage::V1::ApiController.common_params(api)
    param :form, :popup_ad_enabled, :string, :optional, "是否开启，true:是, false:否"
    param :form, :popup_ad_day_times, :string, :optional, "每天展示次数"
    param :form, :popup_ad_image_url, :integer, :optional, "图片url"
    param :form, :popup_ad_link_url, :string, :optional, "连接url"
    param :form, :popup_ad_open_link_type, :string, :optional, "跳转方式1: webview, 2: 浏览器" 
  end
  def update_settings
    # changed = false
    Setting.popup_ad_enabled = params[:popup_ad_enabled] if params[:popup_ad_enabled].present?
    [
      :popup_ad_day_times,
      :popup_ad_image_url,
      :popup_ad_link_url,
      :popup_ad_open_link_type
    ].each do |item|
      if params[item].present? && params[item] != Setting[item]
        Setting[item] = params[item]
        # changed = true
      end
    end
    Setting.popup_ad_updated_at = DateUtils.time2str(Time.now) #if changed
  end

end

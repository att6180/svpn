class Api::Manage::V1::NodeTypesController < Api::Manage::V1::ApiController

  before_action :set_node_type, only: [:show, :update, :destroy]

  swagger_controller :node_types, "节点类型管理"

  swagger_api :index do |api|
    summary "节点类型管理 - 列表"
    notes ""
    Api::Manage::V1::ApiController.common_params(api)
    param :query, :is_enabled, :string, :optional, "true: 筛选已启用的类型, false|忽略参数: 筛选全部"
    param :query, :page, :integer, :optional, "页码，默认: 1"
    param :query, :limit, :integer, :optional, "每页显示的记录数, 默认: 25"
  end
  def index
    optional! :is_enabled, type: String
    optional! :page, type: Integer
    optional! :limit, type: Integer

    node_types = NodeType.sorted_by_level
    node_types = node_types.enabled if params[:is_enabled] == 'true'
    @node_types = node_types.includes(:user_group).page(param_page).per(param_limit)
  end

  swagger_api :create do |api|
    summary "节点类型管理 - 创建"
    notes ""
    Api::Manage::V1::ApiController.common_params(api)
    param :form, :name, :string, :required, "节点类型名"
    param :form, :user_group_id, :integer, :required, "用户组ID，节点类型的最低用户组需求"
    param :form, :expense_coins, :integer, :required, "消费的钻石数"
    param :form, :can_be_monthly, :string, :optional, "是否可转换成包月, true|false"
    param :form, :times_for_monthly, :integer, :optional, "包月所需要消费的次数"
    param :form, :limit_speed_up, :integer, :optional, "上传限速，单位kbytes"
    param :form, :limit_speed_down, :integer, :optional, "下载限速，单位kbytes"
    param :form, :description, :string, :optional, "描述，如果文字中含有'|'，则表示换行"
    param :form, :is_enabled, :string, :optional, "是否启用此类型, true|false"
  end
  def create
    requires! :name, type: String
    requires! :user_group_id, type: Integer
    requires! :expense_coins, type: Integer

    can_access?(:charge_manage)

    @node_type = NodeType.new(allowed_params)
    error_detail!(@node_type) if !@node_type.save
  end

  swagger_api :show do |api|
    summary "节点类型管理 - 单个获取"
    notes ""
    Api::Manage::V1::ApiController.common_params(api)
    param :path, :id, :integer, :required, "节点ID"
  end
  def show
    requires! :id, type: Integer
    can_access?(:charge_manage)
  end

  swagger_api :update do |api|
    summary "节点类型管理 - 更新"
    notes ""
    Api::Manage::V1::ApiController.common_params(api)
    param :path, :id, :integer, :required, "节点ID"
    param :form, :name, :string, :optional, "节点类型名"
    param :form, :user_group_id, :integer, :optional, "用户组ID，节点类型的最低用户组需求"
    param :form, :expense_coins, :integer, :optional, "消费的钻石数"
    param :form, :can_be_monthly, :string, :optional, "是否可转换成包月, true|false"
    param :form, :times_for_monthly, :integer, :optional, "包月所需要消费的次数"
    param :form, :limit_speed_up, :integer, :optional, "上传限速，单位kbytes"
    param :form, :limit_speed_down, :integer, :optional, "下载限速，单位kbytes"
    param :form, :description, :string, :optional, "描述，如果文字中含有'|'，则表示换行"
    param :form, :is_enabled, :string, :optional, "是否启用此类型, true|false"
  end
  def update
    requires! :id, type: Integer

    can_access?(:charge_manage)

    error_detail!(@node_type) if !@node_type.update(allowed_params)
  end

  private

  def allowed_params
    params.permit(
      :name,
      :user_group_id,
      :expense_coins,
      :can_be_monthly,
      :times_for_monthly,
      :limit_speed_up,
      :limit_speed_down,
      :is_enabled,
      :description
    )
  end

  def set_node_type
    @node_type = NodeType.find(params[:id])
  end

end

class Api::Manage::V1::SessionsController < Api::Manage::V1::ApiController

  skip_before_action :authenticate_user!

  swagger_controller :sessions, "登录"

  swagger_api :create do
    summary "管理员登录认证"
    notes "验证正确后，会返回token，之后在访问其他接口时，需要在header中加入此token用来验证权限"
    param :form, :username, :string, :required, "管理员用户名"
    param :form, :password, :string, :required, "管理员密码"
  end
  def create
    requires! :username, type: String
    requires! :password, type: String

    if ::Manage::Admin.disallow_login?(client_ip) && Setting.system_login_fail_limit.to_i == 1
      error!("尝试次数过多, 请#{Setting.system_login_disable_minutes}分钟后再尝试") and return
    end

    @admin = ::Manage::Admin.find_by(username: allowed_params[:username])
    if @admin.blank? || !@admin.authenticate(allowed_params[:password])
      ::Manage::Admin.auth_fail_count(client_ip)
      error!('用户名或密码错误') and return
    end

    if !@admin.is_enabled?
      error!('帐号被禁用') and return
    end

    @admin.create_log(key: 'create_session')
    @admin.sign_in(client_ip)
    @token = create_jwt(@admin)
    @app_channels = SystemEnum.enums_by_channel
    @app_versions = SystemEnum.enums_by_version
  end

  private

  # 创建客户端API访问Token
  def create_jwt(admin)
    payload = { id: admin.id, username: admin.username, exp: Time.now.to_i + 24 * 60 * 60 }
    JWT.encode(payload, admin.api_token)
  end

  def allowed_params
    params.permit(:username, :password)
  end
end

class Api::Manage::V1::TransactionTypeStatsController < Api::Manage::V1::ApiController

  # 设置权限
  include PermissionFilterable
  set_module_group_name :operation_behaviour

  swagger_controller :transaction_type_stats, "充值类型分析"

  swagger_api :index do |api|
    summary "充值信息 - 充值类型分析 - 列表"
    notes <<-EOF
    说明:
      1.price_types 返回的金额类型是动态的。
        比如筛选周期为2017-12-01至2017-12-15，假如2017-12-10号因为做活动把价格为618的套餐改为418，
        则2017-12-10这一天的price_types中将不包含618这个金额类型。
        所以需要客户端在拿到price_types数组后，将数组内出现的所有金额类型提取出来去重，
        然后再设置表格列，再循环绑定数组，发现数组中缺少某个列的用户数和百分比就补0即可。
      2.每天每个price_type(金额类型)会对应一个naems(套餐名,一个或多个)。
        需要客户端为每个金额类型存一份所有筛选天数内出现的套餐名列表（去重），
        鼠标移动对应「金额类型」的表头时，把这个去重的套餐名列表用浮层的方式显示出来。

    stat_date: 当前日期
    users_count: 总人数
    price_types: 数组, 每个金额类型对应的人数和百分比
      price_type: 金额类型
      names: 价格类型对应的套餐名（一个或多个）
      users_count: 金额类型对应的用户数
      rate: 金额类型百分比
    EOF
    Api::Manage::V1::ApiController.common_params(api)
    Api::Manage::V1::ApiController.page_params(api)
    Api::Manage::V1::ApiController.version_params(api)
    param :query, :start_at, :string, :optional, "开始年月日, 如: 2017-01-04，默认为月初第一天"
    param :query, :end_at, :string, :optional, "结束年月日, 如: 2017-03-05, 默认为昨天"
  end
  def index
    start_at = params[:start_at]&.to_date || Time.now.beginning_of_month.to_date
    end_at = params[:end_at]&.to_date || Time.now.to_date
    logs = TransactionTypeDayStat.all
    logs = handle_version_params(logs)
    if params[:start_at].present? && params[:end_at].present?
      logs = logs.between_date(params[:start_at].to_date, params[:end_at].to_date)
    end
    @logs = logs.order(stat_date: :desc)
  end

end

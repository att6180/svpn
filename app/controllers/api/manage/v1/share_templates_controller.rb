class Api::Manage::V1::ShareTemplatesController < Api::Manage::V1::ApiController

  include PermissionFilterable
  set_module_group_name :system_setting

  before_action :set_share_template, only: [:show, :update, :destroy]

  swagger_controller :share_templates, "分享配置及相关操作"

  swagger_api :update_share_template_settings do |api|
    summary "参数配置 - 分享配置 - 全局参数设置"
    notes ""
    Api::Manage::V1::ApiController.common_params(api)
    param :form, :text_logo_url, :string, :optional, "文字分享logo url"
    param :form, :weixin_enabled, :string, :optional, "分享微信是否开启 true/false"
    param :form, :friend_circle_enabled, :string, :optional, "分享微信朋友圈是否开启 true/false"
    param :form, :qq_enabled, :string, :optional, "分享qq是否开启 true/false"
    param :form, :weibo_enabled, :string, :optional, "分享微博是否开启 true/false"
  end
  def update_share_template_settings
    if params[:text_logo_url].present? && params[:text_logo_url] != Setting.share_text_logo_url
      Setting.share_text_logo_update_time = Time.now.to_i
    end
    Setting.share_text_logo_url = params[:text_logo_url] if params[:text_logo_url].present?
    Setting.share_weixin_enabled = params[:weixin_enabled]  if params[:weixin_enabled].present?
    Setting.share_friend_circle_enabled = params[:friend_circle_enabled]  if params[:friend_circle_enabled].present?
    Setting.share_qq_enabled = params[:qq_enabled]  if params[:qq_enabled].present?
    Setting.share_weibo_enabled = params[:weibo_enabled]  if params[:weibo_enabled].present?
  end

  swagger_api :index do |api|
    summary "参数配置 - 分享配置 - 列表"
    notes ""
    Api::Manage::V1::ApiController.common_params(api)
    Api::Manage::V1::ApiController.page_params(api)
  end
  def index
    @share_templates =  ShareTemplate.sorted.page(param_page).per(param_limit)
  end

  swagger_api :create do |api|
    summary "参数配置 - 分享配置 - 创建"
    notes ""
    Api::Manage::V1::ApiController.common_params(api)
    param :form, :platform_name, :string, :required, "平台名称"
    param :form, :sequence, :integer, :required, "显示顺序"
    param :form, :copy_link_text, :string, :required, "复制链接内文字"
    param :form, :text_title, :string, :required, "分享文字题标题"
    param :form, :text_content, :string, :required, "分享文字内容"
    param :form, :text_url, :string, :required, "分享文字url"
    param :form, :image_text_content, :string, :required, "图片分享文字内容"
    param :form, :image_promo_text, :string, :required, "图片分享推广码文字"
    param :form, :landing_page_qrcode_url, :string, :required, "图片分享落地页二维码url"
    param :form, :background_image_download_url, :string, :required, "背景图下载url"
  end
  def create
    requires! :platform_name, type: String
    requires! :sequence, type: Integer
    requires! :copy_link_text, type: String
    requires! :text_title, type: String
    requires! :text_content, type: String
    requires! :text_url, type: String
    requires! :image_text_content, type: String
    requires! :image_promo_text, type: String
    requires! :landing_page_qrcode_url, type: String
    requires! :background_image_download_url, type: String

    @share_template = ShareTemplate.new(allowed_params)
    error_detail!(@share_template) if !@share_template.save
  end

  swagger_api :show do |api|
    summary "参数配置 - 分享配置 - 单个获取"
    notes ""
    Api::Manage::V1::ApiController.common_params(api)
    param :path, :id, :integer, :required, "ID"
  end
  def show
    requires! :id, type: Integer
  end

  swagger_api :update do |api|
    summary "参数配置 - 分享配置 - 单个更新"
    notes ""
    Api::Manage::V1::ApiController.common_params(api)
    param :path, :id, :integer, :required, "ID"
    param :form, :platform_name, :string, :optional, "平台名称"
    param :form, :sequence, :integer, :optional, "显示顺序"
    param :form, :copy_link_text, :string, :optional, "复制链接内文字"
    param :form, :text_title, :string, :optional, "分享文字题标题"
    param :form, :text_content, :string, :optional, "分享文字内容"
    param :form, :text_url, :string, :optional, "分享文字url"
    param :form, :image_text_content, :string, :optional, "图片分享文字内容"
    param :form, :image_promo_text, :string, :optional, "图片分享推广码文字"
    param :form, :landing_page_qrcode_url, :string, :optional, "图片分享落地页二维码url"
    param :form, :background_image_download_url, :string, :optional, "背景图下载url"
  end
  def update
    requires! :id, type: String

    error_detail!(@share_template) if !@share_template.update(allowed_params)
  end

  swagger_api :destroy do |api|
    summary "参数配置 - 分享配置 - 删除"
    notes ""
    Api::Manage::V1::ApiController.common_params(api)
    param :path, :id, :integer, :required, "ID"
  end
  def destroy
    requires! :id, type: Integer
    error_detail!(@share_template) if !@share_template.destroy
  end

  private

  def allowed_params
    params.permit(
      :platform_name,
      :sequence,
      :copy_link_text,
      :text_title,
      :text_content,
      :text_url,
      :image_text_content,
      :image_promo_text,
      :landing_page_qrcode_url,
      :background_image_download_url
    )
  end

  def set_share_template
    @share_template = ShareTemplate.find(params[:id])
  end

end

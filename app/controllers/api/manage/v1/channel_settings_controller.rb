class Api::Manage::V1::ChannelSettingsController < Api::Manage::V1::ApiController

  # 设置权限
  include PermissionFilterable
  set_module_group_name :system_setting

  before_action :set_item, only: [:show, :update, :destroy]

  swagger_controller :channel_settings, "渠道版本相关配置"

  swagger_api :index do |api|
    summary "参数配置 - 渠道版本相关配置 - 列表"
    notes ""
    Api::Manage::V1::ApiController.common_params(api)
    Api::Manage::V1::ApiController.page_params(api)
    param :query, :app_channel, :string, :optional, "渠道，如: all/moren等"
    param :query, :app_version, :string, :optional, "版本，如: all/jichu等"
  end
  def index
    items = ChannelSetting.order(app_channel: :asc, app_version: :asc)
    items = items.where(app_channel: params[:app_channel]) if params[:app_channel].present?
    items = items.where(app_version: params[:app_version]) if params[:app_version].present?
    @items = items.page(param_page).per(param_limit)
  end

  swagger_api :create do |api|
    summary "参数配置 - 渠道版本相关配置 - 创建"
    notes ""
    Api::Manage::V1::ApiController.common_params(api)
    param :form, :app_channel, :string, :required, "渠道，如: appStore/moren等"
    param :form, :app_version, :string, :required, "版本，如: jichu等"
    param :form, :settings, :string, :required, "设置项，如: {\"new_user_coins\": 3, \"checkin_time\": 10}"
  end
  def create
    requires! :app_channel, type: String
    requires! :app_version, type: String
    requires! :settings, type: String

    @item = ChannelSetting.new(allowed_params)
    error_detail!(@item) if !@item.save
  end

  swagger_api :show do |api|
    summary "参数配置 - 渠道版本相关配置 - 单个获取"
    notes ""
    Api::Manage::V1::ApiController.common_params(api)
    param :path, :id, :integer, :required, "ID"
  end
  def show
    requires! :id, type: Integer
  end

  swagger_api :update do |api|
    summary "参数配置 - 渠道版本相关配置 - 更新"
    notes ""
    Api::Manage::V1::ApiController.common_params(api)
    param :path, :id, :integer, :required, "ID"
    param :form, :app_channel, :string, :optional, "渠道，如: appStore/moren等"
    param :form, :app_version, :string, :optional, "版本，如: jichu等"
    param :form, :settings, :string, :optional, "设置项，如: {\"new_user_coins\": 3, \"checkin_time\": 10}"
  end
  def update
    error_detail!(@item) if !@item.update(allowed_params)
  end

  swagger_api :destroy do |api|
    summary "参数配置 - 渠道版本相关配置 - 删除"
    notes ""
    Api::Manage::V1::ApiController.common_params(api)
    param :path, :id, :integer, :required, "ID"
  end
  def destroy
    requires! :id, type: Integer
    error_detail!(@item) if !@item.destroy
  end

  private

  def allowed_params
    params.permit(
      :app_channel,
      :app_version,
      :settings
    )
  end

  def set_item
    @item = ChannelSetting.find(params[:id])
  end

end

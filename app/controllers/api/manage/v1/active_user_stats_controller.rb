class Api::Manage::V1::ActiveUserStatsController < Api::Manage::V1::ApiController

  # 设置权限
  include PermissionFilterable
  set_module_group_name :operation_behaviour

  swagger_controller :active_user_stats, "活跃用户"

  swagger_api :hour_index do |api|
    summary "活跃用户 - 小时活跃 - 列表"
    notes ""
    Api::Manage::V1::ApiController.common_params(api)
    Api::Manage::V1::ApiController.package_params(api)
    Api::Manage::V1::ApiController.version_params(api)
    param :query, :date, :string, :required, "筛选日期, 如: 2017-01-01"
  end
  def hour_index
    @date = params[:date].to_date
    if @date.today?
      logs = UserActiveHourLog.by_created_date(@date)
        .group_by_hour
        .select("COUNT(user_id) as users_count")

      logs = handle_muilt_version_params(logs)
      log_indexs = logs.to_a.map(&:serializable_hash).index_by{|l| l["h"]}
      today_logs = []
      (0..23).each_with_index do |i|
        today_logs.push(log_indexs.has_key?(i) ? log_indexs[i].symbolize_keys! : { h: i, users_count: 0 })
      end
      @logs = today_logs
      @total_users = today_logs.map.sum{|l| l[:users_count]}
    else
      logs = ActiveUserHourStat.select(:stat_date, :stat_hour)
        .by_date(@date)
        .select_sum_users
        .group_by_hour
        .sorted
      @logs = handle_muilt_version_histroy_params(logs)
      @total_users = @logs.map.sum{|l| l[:users_count]}
    end
    logs = ActiveUserHourStat.select(:stat_date, :stat_hour)
      .by_date(@date.prev_day)
      .select_sum_users
      .group_by_hour
      .sorted
    @prev_logs = handle_muilt_version_histroy_params(logs)
    @prev_total_users = @prev_logs.map.sum{|l| l[:users_count]}
  end

  swagger_api :day_index do |api|
    summary "日活跃用户 - 列表"
    notes ""
    Api::Manage::V1::ApiController.common_params(api)
    Api::Manage::V1::ApiController.page_params(api)
    Api::Manage::V1::ApiController.version_params(api)
    param :query, :start_at, :string, :optional, "开始日期, 如: 2017-01-01"
    param :query, :end_at, :string, :optional, "结束日期, 如: 2017-03-01"
    param :query, :stat_type, :string, :optional, "活跃数据类型 0: 来自流量日志, 1: 来自活跃日志"
  end
  def day_index
    optional! :stat_type, type: String, default: 0, values: [0, 1]

    # 根据活跃数据类型筛选得到相应类型数据
    logs = ActiveUserDayStat.where(stat_type: params[:stat_type])
    # 如果有筛选则查询一遍无筛选的数据进行对比
    total_logs = has_channel_param? ? logs.where(filter_type: 0) : nil

    logs = logs.all
    # 处理渠道版本参数过滤
    logs = handle_version_params(logs)
    if params[:start_at].present? && params[:end_at].present?
      logs = logs.between_months(params[:start_at].to_date, params[:end_at].to_date)
      total_logs = total_logs.between_months(params[:start_at].to_date, params[:end_at].to_date) if total_logs
    else
      today = Time.now.to_date
      logs = logs.between_months(today.beginning_of_month, today.end_of_month)
      total_logs = total_logs.between_months(today.beginning_of_month, today.end_of_month) if total_logs
    end
    @logs = logs.stat_result.order("stat_date DESC").page(param_page).per(param_limit)
    if total_logs
      @total_logs = total_logs
        .stat_result
        .order("stat_date DESC")
        .page(param_page).per(param_limit)
        .index_by{|log| log.stat_date.strftime("%Y%m%d")}
    end
  end

  swagger_api :day_chart do |api|
    summary "日活跃用户 - 图表"
    notes ""
    Api::Manage::V1::ApiController.common_params(api)
    Api::Manage::V1::ApiController.version_params(api)
    param :query, :start_at, :string, :optional, "开始年月, 如: 2017-01"
    param :query, :end_at, :string, :optional, "结束年月, 如: 2017-03"
  end
  def day_chart
    logs = ActiveUserDayStat.all
    # 处理渠道版本参数过滤
    logs = handle_version_params(logs)
    if params[:start_at].present? && params[:end_at].present?
      logs = logs.between_months("#{params[:start_at]}-01".to_date, "#{params[:end_at]}-01".to_date)
    else
      today = Time.now.to_date
      logs = logs.between_months(today.beginning_of_month, today.end_of_month)
    end
    @logs = logs.stat_result.order("stat_date ASC")
  end

  swagger_api :day_details do |api|
    summary "日活跃用户 - 详情"
    notes ""
    Api::Manage::V1::ApiController.common_params(api)
    Api::Manage::V1::ApiController.page_params(api)
    Api::Manage::V1::ApiController.version_params(api)
    param :query, :stat_date, :string, :required, "统计日期, 如: 2017-04-11"
  end
  def day_details
    requires! :stat_date, type: String

    @logs = UserSigninLog
      .select(:user_id)
      .the_where(app_version: params[:app_version], app_channel: params[:app_channel])
      .by_created_date(params[:stat_date].to_date)
      .group(:user_id)
      .includes(:user)
      .page(param_page)
      .per(param_limit)
  end

  swagger_api :month_index do |api|
    summary "月活跃用户 - 列表"
    notes ""
    Api::Manage::V1::ApiController.common_params(api)
    Api::Manage::V1::ApiController.page_params(api)
    Api::Manage::V1::ApiController.version_params(api)
    param :query, :start_at, :string, :optional, "开始年月, 如: 2017-01"
    param :query, :end_at, :string, :optional, "结束年月, 如: 2017-03"
  end
  def month_index
    # 如果有筛选则查询一遍无筛选的数据进行对比
    total_logs = has_channel_param? ? ActiveUserMonthStat.where(filter_type: 0) : nil

    logs = ActiveUserMonthStat.all
    # 处理渠道版本参数过滤
    logs = handle_version_params(logs)
    start_at = "#{params[:start_at]}-01"&.to_date || (Time.now - 12.month).to_date.beginning_of_year
    end_at = "#{params[:end_at]}-01"&.to_date || Time.now.to_date.end_of_year
    logs = logs.between_month(start_at, end_at)
    total_logs = total_logs.between_month(start_at, end_at) if total_logs
    logs = logs.stat_result
    @logs = logs.order(stat_year: :desc, stat_month: :desc).page(param_page).per(param_limit)
    if total_logs
      @total_logs = total_logs
        .stat_result
        .order(stat_year: :desc, stat_month: :desc)
        .page(param_page).per(param_limit)
        .index_by{|log| "#{log.stat_year}#{log.stat_month}"}
    end
  end

  swagger_api :month_chart do |api|
    summary "月活跃用户 - 图表"
    notes ""
    Api::Manage::V1::ApiController.common_params(api)
    Api::Manage::V1::ApiController.version_params(api)
    param :query, :start_at, :string, :optional, "开始年月, 如: 2017-01"
    param :query, :end_at, :string, :optional, "结束年月, 如: 2017-03"
  end
  def month_chart
    logs = ActiveUserMonthStat.all
    # 处理渠道版本参数过滤
    logs = handle_version_params(logs)
    start_at = "#{params[:start_at]}-01"&.to_date || (Time.now - 12.month).to_date.beginning_of_year
    end_at = "#{params[:end_at]}-01"&.to_date || Time.now.to_date.end_of_year
    logs = logs.between_month(start_at, end_at)
    @logs = logs.stat_result.order(stat_year: :asc, stat_month: :asc)
  end

  swagger_api :month_details do |api|
    summary "月活跃用户 - 详情"
    notes ""
    Api::Manage::V1::ApiController.common_params(api)
    Api::Manage::V1::ApiController.page_params(api)
    Api::Manage::V1::ApiController.version_params(api)
    param :query, :stat_month, :string, :required, "统计年月, 如: 2017-01"
  end
  def month_details
    requires! :stat_month, type: Integer

    logs = UserSigninLog.select(:user_id)
    logs = logs.where(app_version: params[:app_version]) if params[:app_version].present?
    logs = logs.where(app_channel: params[:app_channel]) if params[:app_channel].present?
    stat_date = "#{params[:stat_month]}-01".to_date
    @logs = logs.by_created_month(stat_date.year, stat_date.month).group(:user_id).includes(:user).page(param_page).per(param_limit)
  end

  swagger_api :month_period_index do |api|
    summary "用户行为 - 用户使用情况 - 月活跃用户统计"
    notes ""
    Api::Manage::V1::ApiController.common_params(api)
    Api::Manage::V1::ApiController.version_params(api)
    param :query, :start_at, :string, :optional, "开始年月, 如: 2017-01"
    param :query, :end_at, :string, :optional, "结束年月, 如: 2017-03"
  end
  def month_period_index
    start_at = "#{params[:start_at]}-01"&.to_date || Time.now.to_date.beginning_of_year
    end_at = "#{params[:end_at]}-01"&.to_date || Time.now.to_date.end_of_month

    logs = Log::ActiveUserPeriodMonthStat.between_month(start_at, end_at)
    logs = handle_version_params(logs)
    @logs = logs.order(stat_year: :desc, stat_month: :desc)
  end

end

class Api::Manage::V1::WafSettingsController < Api::Manage::V1::ApiController

  before_action :set_setting, only: [:update, :destroy]

  swagger_controller :waf_settings, "防刷参数设置"

  swagger_api :index do |api|
    summary "防刷参数 - 列表"
    notes ""
    Api::Manage::V1::ApiController.common_params(api)
  end
  def index
    @setting = Waf::Setting.last
  end

  swagger_api :create do |api|
    summary "防刷参数 - 创建"
    notes ""
    Api::Manage::V1::ApiController.common_params(api)
    param :form, :brush_time, :Integer, :required, "防刷时间(秒)"
    param :form, :brush_count, :Integer, :required, "防刷个数"
    param :form, :trigger_count, :Integer, :required, "触发次数"
  end
  def create
    requires! :brush_time, type: Integer
    requires! :brush_count, type: Integer
    requires! :trigger_count, type: Integer

    @setting = ::Waf::Setting.new(allowed_params)
    error_detail!(@setting) if !@setting.save
  end

  swagger_api :update do |api|
    summary "防刷参数 - 更新"
    notes ""
    Api::Manage::V1::ApiController.common_params(api)
    param :path, :id, :integer, :required, "ID"
    param :form, :brush_time, :Integer, :required, "防刷时间(秒)"
    param :form, :brush_count, :Integer, :required, "防刷个数"
    param :form, :trigger_count, :Integer, :required, "触发次数"
  end
  def update
    requires! :id, type: Integer
    requires! :brush_time, type: Integer
    requires! :brush_count, type: Integer
    requires! :trigger_count, type: Integer

    error_detail!(@setting) if !@setting.update(allowed_params.except(:id))
  end

  def destroy
    requires! :id, type: Integer

  end

  private

  def allowed_params
    params.permit(
      :id,
      :brush_time,
      :brush_count,
      :trigger_count
    )
  end

  def set_setting
    @setting = Waf::Setting.find(params[:id])
  end
end

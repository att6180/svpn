class Api::Manage::V1::UserSigninFailedLogsController < Api::Manage::V1::ApiController

  # 设置权限
  include PermissionFilterable
  set_module_group_name :user_base_info

  swagger_controller :user_signin_failed_logs, "登录失败"

  swagger_api :index do |api|
    summary "用户行为 - 失败信息 - 登录失败 - 统计"
    notes ""
    Api::Manage::V1::ApiController.common_params(api)
    Api::Manage::V1::ApiController.page_params(api)
    Api::Manage::V1::ApiController.between_date_params(api)
  end
  def index
    verify_date_range!
    logs = UserSigninFailedLog.all
    if has_channel_param?
      logs = logs.where(app_version: params[:app_version]) if params[:app_version].present?
      logs = logs.where(app_channel: params[:app_channel]) if params[:app_channel].present?
    end
    start_at, end_at = default_start_end_at
    logs = logs.between_date(start_at, end_at)
    logs = logs.group_addition
    @logs = DateUtils.fill_up(
      logs, start_at, end_at,
      { type0: 0, type1: 0, type2: 0, type3: 0 },
      true
    )
  end

  swagger_api :chart do |api|
    summary "用户行为 - 失败信息 - 登录失败 - 图表"
    notes ""
    Api::Manage::V1::ApiController.common_params(api)
    param :query, :start_at, :string, :optional, "日期筛选开始时间"
    param :query, :end_at, :string, :optional, "日期筛选结束时间"
  end
  def chart
    verify_date_range!
    logs = UserSigninFailedLog.all
    if has_channel_param?
      logs = logs.where(app_version: params[:app_version]) if params[:app_version].present?
      logs = logs.where(app_channel: params[:app_channel]) if params[:app_channel].present?
    end
    start_at, end_at = default_start_end_at
    logs = logs.between_date(start_at, end_at)
    logs = logs.group_addition
    @logs = DateUtils.fill_up(
      logs, start_at, end_at,
      { type0: 0, type1: 0, type2: 0, type3: 0 },
      false
    )
  end

  swagger_api :detail do |api|
    summary "用户行为 - 失败信息 - 登录失败 - 详情"
    notes "点击指定统计数字，得到构成此数量的每条信息详情"
    Api::Manage::V1::ApiController.common_params(api)
    param :query, :type, :integer, :required, "详情指定的类型，0:用户不存在, 1:密码错误, 2:连接失败, 3:软件崩溃"
    param :query, :stat_at, :string, :required, "详情指定的日期，会返回该日期内发生的所有错误日志"
    param :query, :page, :integer, :optional, "页码，默认: 1"
    param :query, :limit, :integer, :optional, "每页显示的记录数, 默认: 25"
  end
  def detail
    requires! :type, type: Integer
    requires! :stat_at, type: String
    optional! :page, type: Integer
    optional! :limit, type: Integer

    @logs = UserSigninFailedLog
      .by_type_and_date(params[:type], params[:stat_at].to_date)
      .includes(:user)
      .page(param_page).per(param_limit)
  end
end

class Api::Manage::V1::UsersController < Api::Manage::V1::ApiController

  # 设置权限
  include PermissionFilterable
  set_module_group_name :user_base_info

  swagger_controller :users, "用户基础信息"

  swagger_api :stat_info do |api|
    summary "用户基础信息 - 简要统计信息"
    notes <<-EOF
    users_count: 用户总数
    today_users_count: 今日新增用户数
    active_users_count: 今日活跃用户数
    online_users_count: 当前在线用户数
    peak_online_users_count: 历史最高在线用户数
    peak_online_users_count_at: 历史最高在线用户数产生时间
    EOF
    Api::Manage::V1::ApiController.common_params(api)
  end
  def stat_info
  end

  swagger_api :base_list do |api|
    summary "用户基础信息 - 用户列表"
    notes ""
    Api::Manage::V1::ApiController.common_params(api)
    param :query, :page, :integer, :optional, "页码，默认: 1"
    param :query, :limit, :integer, :optional, "每页显示的记录数, 默认: 25"
    param :query, :app_version, :string, :optional, "版本筛选"
    param :query, :app_channel, :string, :optional, "渠道筛选"
    param :query, :is_enabled, :integer, :optional, "用户状态筛选, 0|1"
    param :query, :date_type, :string, :optional, "时间筛选类型，created_at: 创建时间, logged_at: 登录时间"
    param :query, :start_at, :string, :optional, "时间筛选开始时间"
    param :query, :end_at, :string, :optional, "时间筛选结束时间"
    param :query, :query_type, :string, :optional, "搜索类型: 0:按用户名搜索, 1:按用户ID搜索，2:按UUID搜索，默认为0"
    param :query, :q, :string, :optional, "搜索用户名，UUID"
  end
  def base_list
    query_type = params[:query_type] || '0'

    if params[:q].present?
      error!("搜索条件有误，请重新输入。") and return if query_type != '0' && params[:q].strip.to_i > 2147483648
    end

    users = User.select(
      :id, :username, :create_device_uuid, :is_enabled, :total_payment_amount,
      :current_coins, :total_coins, :total_used_coins, :user_group_id,
      :current_signin_at, :created_at, :create_app_channel, :create_app_version,
      :create_app_version_number
    ).order(created_at: :desc)
    users = users.where(create_app_version: params[:app_version]) if params[:app_version].present?
    users = users.where(create_app_channel: params[:app_channel]) if params[:app_channel].present?
    if params[:is_enabled].present?
      is_enabled = params[:is_enabled] == "1" ? true : false
      users = users.where(is_enabled: is_enabled)
    end
    if params[:date_type].present?
      if params[:date_type] == "created_at" && params[:start_at].present? && params[:end_at].present?
        users = users.where("created_at >= ? AND created_at <= ?", params[:start_at].to_date.beginning_of_day, params[:end_at].to_date.end_of_day)
      end
      if params[:date_type] == "logged_at" && params[:start_at].present? && params[:end_at].present?
        users = users.where("current_signin_at >= ? AND current_signin_at <= ?", params[:start_at].to_date.beginning_of_day, params[:end_at].to_date.end_of_day)
      end
    end
    if params[:q].present?
        if query_type == '0'
          users = users.search_by_username(params[:q].strip)
        elsif query_type == '2'
          device = Device.find_by(uuid: params[:q].strip)
          users = device.present? ? device.users : nil
        else
          users = users.search_by_id(params[:q].strip)
        end
    end
    if users.nil?
      @users = []
    else
      @users = users.includes(:user_group).page(param_page).per(param_limit)
    end
  end

  swagger_api :hardware_stat do |api|
    summary "用户基础信息 - 统计表 - 硬件统计"
    notes ""
    Api::Manage::V1::ApiController.common_params(api)
    Api::Manage::V1::ApiController.version_params(api)
  end
  def hardware_stat
    items = UserHardwareStat.all
    items = handle_version_params(items).stat_result
    @total = sum_of_list(items)
    @items = items.limit(50)
    @platforms = UserHardwareStat.platform_stat
    @platform_total = @platforms.map{|p| p.last.to_i}.sum
  end

  swagger_api :hardware_stat_details do |api|
    summary "用户基础信息 - 统计表 - 硬件信息详情"
    notes ""
    Api::Manage::V1::ApiController.common_params(api)
    param :query, :device_model, :string, :required, "机型"
    param :query, :platform, :string, :required, "平台"
    Api::Manage::V1::ApiController.version_params(api)
    Api::Manage::V1::ApiController.page_params(api)
  end
  def hardware_stat_details
    requires! :device_model, type: String
    requires! :platform, type: String

    users = User.select(:id, :username, :last_signin_at, :current_signin_ip)
      .yesterday_ago.where(create_device_model: params[:device_model], create_platform: params[:platform])
    users = users.where(create_app_version: params[:app_version]) if params[:app_version].present?
    users = users.where(create_app_channel: params[:app_channel]) if params[:app_channel].present?
    @users = users.order(last_signin_at: :desc).page(param_page).per(param_limit)
  end

  swagger_api :operator_stat do |api|
    summary "用户基础信息 - 统计表 - 运营商统计"
    notes ""
    Api::Manage::V1::ApiController.common_params(api)
    Api::Manage::V1::ApiController.version_params(api)
  end
  def operator_stat
    items = UserOperatorStat.all
    @items = handle_version_params(items).stat_result
    @total = sum_of_list(@items)
  end

  swagger_api :operator_stat_details do |api|
    summary "用户基础信息 - 统计表 - 运营商信息详情"
    notes ""
    Api::Manage::V1::ApiController.common_params(api)
    param :query, :operator, :string, :required, "运营商"
    Api::Manage::V1::ApiController.version_params(api)
    Api::Manage::V1::ApiController.page_params(api)
  end
  def operator_stat_details
    requires! :operator, type: String
    optional! :app_version, type: String
    optional! :app_channel, type: String
    optional! :page, type: String
    optional! :limit, type: String

    users = User.select(:id, :username, :last_signin_at, :current_signin_ip).yesterday_ago.where(create_operator: params[:operator])
    users = users.where(create_app_version: params[:app_version]) if params[:app_version].present?
    users = users.where(create_app_channel: params[:app_channel]) if params[:app_channel].present?
    @users = users.order(last_signin_at: :desc).page(param_page).per(param_limit)
  end

  swagger_api :status_stat do |api|
    summary "用户基础信息 - 统计表 - 状态统计"
    notes ""
    Api::Manage::V1::ApiController.common_params(api)
    Api::Manage::V1::ApiController.version_params(api)
  end
  def status_stat
    items = UserStatusStat.all
    @items = handle_version_params(items).stat_result
    @total = sum_of_list(@items)
  end

  swagger_api :checkin_stat do |api|
    summary "用户基础信息 - 统计表 - 签到统计"
    notes ""
    Api::Manage::V1::ApiController.common_params(api)
    Api::Manage::V1::ApiController.version_params(api)
    param :query, :start_at, :string, :optional, "开始年月, 如: 2017-01-01"
    param :query, :end_at, :string, :optional, "结束年月, 如: 2017-03-01"
  end
  def checkin_stat
    logs = Log::UserCheckinDayStat.all
    logs = handle_version_params(logs)
    if params[:start_at].present? && params[:end_at].present?
      logs = logs.between_months(params[:start_at].to_date, params[:end_at].to_date)
    else
      today = Time.now.to_date
      logs = logs.between_months(today.beginning_of_month, today.end_of_month)
    end
    @logs = logs.order("stat_date DESC").page(param_page).per(param_limit)
  end

  swagger_api :status_stat_details do |api|
    summary "用户基础信息 - 统计表 - 用户状态信息详情"
    notes ""
    Api::Manage::V1::ApiController.common_params(api)
    param :query, :is_enabled, :integer, :required, "用户帐号是否正常 1:正常, 0:被封"
    Api::Manage::V1::ApiController.version_params(api)
    Api::Manage::V1::ApiController.page_params(api)
  end
  def status_stat_details
    requires! :is_enabled, type: String
    optional! :app_version, type: String
    optional! :app_channel, type: String
    optional! :page, type: String
    optional! :limit, type: String

    is_enabled = params[:is_enabled] == '1' ? 1 : 0

    users = User.select(:id, :username).where(is_enabled: is_enabled)
    users = users.where(create_app_version: params[:app_version]) if params[:app_version].present?
    users = users.where(create_app_channel: params[:app_channel]) if params[:app_channel].present?
    @users = users.page(param_page).per(param_limit)
    @status_logs = UserAccountStatusLog.by_user_ids(@users.map(&:id), is_enabled).index_by(&:user_id)
  end

  swagger_api :share_stat do |api|
    summary "用户基础信息 - 统计表 - 分享统计"
    notes <<-EOF
    统计类型为1:
      text_count：文字分享总数
      image_count：图片分享总数
      copy_link_count：复制链接分享总数
    统计类型为2:
      ...
    统计类型为3:
      ...
    EOF
    Api::Manage::V1::ApiController.common_params(api)
    Api::Manage::V1::ApiController.version_params(api)
    param :query, :start_at, :string, :optional, "开始年月, 如: 2017-01-01"
    param :query, :end_at, :string, :optional, "结束年月, 如: 2017-03-01"
    param :query, :stat_type, :integer, :required, "统计类型, 1: 汇总, 2: 文字分享, 3: 图片分享"
  end
  def share_stat
    requires! :stat_type, type: Integer, values: [1,2,3]

    @stat_type = params[:stat_type].to_i
    logs = Log::UserShareStatLog.all
    logs = handle_version_params(logs)

    if params[:start_at].present? && params[:end_at].present?
      logs = logs.between_months(params[:start_at].to_date, params[:end_at].to_date)
    else
      today = Time.now.to_date
      logs = logs.between_months(today.beginning_of_month, today.end_of_month)
    end
    case @stat_type
    when 1
      @logs = logs.stat_cover_result
    when 2
      @logs = logs.stat_text_result
    when 3
      @logs = logs.stat_image_result
    end
  end

  # Deprecated
  # swagger_api :share_stat do |api|
  #   summary "用户基础信息 - 统计表 - 分享统计"
  #   notes ""
  #   Api::Manage::V1::ApiController.common_params(api)
  #   Api::Manage::V1::ApiController.version_params(api)
  # end
  # def share_stat
  #   items = UserShareStat.all
  #   @items = handle_version_params(items).stat_result
  # end

  swagger_api :share_users_stat_details do |api|
    summary "用户基础信息 - 统计表 - 用户分享人数详情"
    notes ""
    Api::Manage::V1::ApiController.common_params(api)
    param :query, :type, :integer, :required, "分享类型, 0:微信, 1:朋友圈, 2:QQ, 3:微博"
    Api::Manage::V1::ApiController.version_params(api)
    Api::Manage::V1::ApiController.page_params(api)
  end
  def share_users_stat_details
    requires! :type, type: Integer
    optional! :app_version, type: String
    optional! :app_channel, type: String
    optional! :page, type: String
    optional! :limit, type: String

    logs = UserShareLog.yesterday_ago.distinct_user(params[:type].to_i)
    logs = logs.where(app_version: params[:app_version]) if params[:app_version].present?
    logs = logs.where(app_channel: params[:app_channel]) if params[:app_channel].present?
    @logs = logs.includes(:user).page(param_page).per(param_limit)
  end

  swagger_api :area_stat do |api|
    summary "用户基础信息 - 统计表 - 地域统计"
    notes <<-EOF
    foreign_users: 国外用户(数组)
        country: 国家
        total: 用户数
    domestic_users: 国内用户(数组)
        country: 国家
        total: 用户数
    users_total: 总用户数
    foreign_users_total: 国外用户总数
    domestic_users_total: 国内用户总数
    EOF
    Api::Manage::V1::ApiController.common_params(api)
    Api::Manage::V1::ApiController.version_params(api)
  end
  def area_stat

    user_areas = UserAreaStat.where(app_channel: params[:app_channel], app_version: params[:app_version])
    
    @foreign_users = user_areas.oversea
      .select("country, users_count as total")
      .order("total desc")
      .to_a

    @domestic_users = user_areas.domestic
      .select("province, SUM(users_count) as total")
      .group(:province)
      .order("total desc")
      .to_a
    
    @users_total = user_areas.sum(:users_count)
    @foreign_users_total = @foreign_users.sum(&:total)
    @domestic_users_total = @domestic_users.sum(&:total)
  end

  swagger_api :share_count_stat_details do |api|
    summary "用户基础信息 - 统计表 - 用户分享次数详情"
    notes "此接口可同时用于后台管理文档中4.3.5.2.2.1.3和4.3.5.2.3.1两处的需求"
    Api::Manage::V1::ApiController.common_params(api)
    param :query, :type, :integer, :required, "分享类型, 0:微信, 1:朋友圈, 2:QQ, 3:微博"
    param :query, :user_id, :integer, :optional, "用户ID，若不指定，则显示指定类型的全部分享记录"
    Api::Manage::V1::ApiController.version_params(api)
    Api::Manage::V1::ApiController.page_params(api)
  end
  def share_count_stat_details
    requires! :type, type: Integer
    optional! :user_id, type: Integer
    optional! :app_version, type: String
    optional! :app_channel, type: String
    optional! :page, type: String
    optional! :limit, type: String

    logs = UserShareLog.by_type(params[:type].to_i).recent
    logs = logs.by_user(params[:user_id]) if params[:user_id].present?
    logs = logs.where(app_version: params[:app_version]) if params[:app_version].present?
    logs = logs.where(app_channel: params[:app_channel]) if params[:app_channel].present?
    @logs = logs.includes(:user).page(param_page).per(param_limit)
  end

  swagger_api :batch_reward_coins do |api|
    summary "批量奖励钻石"
    notes "返回的failure_usernames为奖励失败的用户名列表"
    Api::Manage::V1::ApiController.common_params(api)
    param :form, :coins, :integer, :required, "钻石数"
    param :form, :usernames, :string, :required, "用户名列表，多个用户名请使用回车、空格或英文逗号分割"
  end
  def batch_reward_coins
    requires! :coins, type: Integer
    requires! :usernames, type: String

    can_access?(:user_info_update)

    coins = params[:coins].to_i
    username_list = params[:usernames].split(/[\s,]+/).uniq
    if username_list.count > 100
      error!('每次最多操作100个用户，请分批处理') and return
    end
    users = User.where(username: username_list)
    users.each do |user|
      original_coins = user.current_coins
      ActiveRecord::Base.transaction do
        user.increment!(:current_coins, coins)
        ActivityCoinRewardLog.create!(
          user_id: user.id,
          admin_id: current_admin.id,
          original_coins: original_coins,
          current_coins: user.current_coins,
          operation_type: 1
        )
      end
    end
    result_usernames = users.map(&:username)
    current_admin.create_log( key: 'batch_reward_coins', params: { coins: coins })
    @failure_usernames = username_list - result_usernames
  end

  swagger_api :batch_company_user_renew do |api|
    summary "批量为企业团购账号续费，如果不存在该用户会新增"
    notes "返回的failure_usernames为续费失败的用户名列表"
    Api::Manage::V1::ApiController.common_params(api)
    param :form, :node_type_id, :integer, :required, "服务类型ID"
    param :form, :expired_at, :string, :required, "服务到期时间"
    param :form, :usernames, :string, :required, "用户名列表，多个用户名请使用回车、空格或英文逗号分割"
  end
  def batch_company_user_renew
    requires! :node_type_id, type: Integer, values: NodeType.cache_list.map(&:id)
    requires! :expired_at, type: String
    requires! :usernames, type: String

    can_access?(:user_info_update)

    node_type_id = params[:node_type_id].to_i
    expired_at = params[:expired_at].to_time
    username_list = params[:usernames].split(/[\s,]+/)
    @users = []
    is_new_user = false

    username_list.each do |username|
      ActiveRecord::Base.transaction do
        user = User.find_by_username(username)
        if user.blank?
          user = User.new.tap do |u|
            u.username = username
            u.password = Utils::Gen.friendly_code
            # 管理员生成的账号默认写入以下配置
            u.create_device_uuid = SecureRandom.uuid
            u.create_device_name = "iPhone 7"
            u.create_device_model = "iPhone 7"
            u.create_platform = "ios"
            u.create_system_version = "10.3.3"
            u.create_operator = "中国联通"
            u.create_net_env = "wifi"
            u.create_app_version = "juchu"
            u.create_app_version_number = "1.0.0"
            u.create_app_channel = "appStore"
          end
          # 如果不是手机号需要系统自动分h用户名
          if !(/^\d+$/ === username)
            user.username = nil
            user.password = nil 
          end
          user.save!
          is_new_user = true
        end
        node_type = user.user_node_types.find_or_create_by(node_type_id: node_type_id)
        original_expired_at = if is_new_user
          expired_at
        else
          node_type.expired_at
        end
        node_type.update!(expired_at: expired_at)
        if is_new_user || (expired_at.to_i != original_expired_at.to_i)
          UserExpiredRewardLog.create(
            user_id: user.id,
            admin_id: current_admin.id,
            original_expired_at: original_expired_at,
            current_expired_at: expired_at,
            node_type_id: node_type_id,
            user_node_type_id: node_type.id
          )
        end
        @users << user
      end
    end
    @failure_usernames = username_list - @users.map(&:username)
  end

  private

  def filter_items(item_class)
    items = item_class
    items = items.where(app_version: params[:app_version]) if params[:app_version].present?
    items = items.where(app_channel: params[:app_channel]) if params[:app_channel].present?
    items
  end

  def sum_of_list(items)
    items.map { |item| item.total }.sum
  end

end

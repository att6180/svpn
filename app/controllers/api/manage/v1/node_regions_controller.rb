class Api::Manage::V1::NodeRegionsController < Api::Manage::V1::ApiController

  # 设置权限
  include PermissionFilterable
  set_module_group_name :node_manage

  before_action :set_region, only: [:show, :update, :destroy]

  swagger_controller :node_regions, "地域配置"

  swagger_api :index do |api|
    summary "服务器群组管理 - 地域配置 - 列表"
    notes ""
    Api::Manage::V1::ApiController.common_params(api)
    Api::Manage::V1::ApiController.page_params(api)
    param :query, :is_enabled, :string, :optional, "true: 过滤已启用的地域, false|忽略参数: 获取全部地域"
    param :query, :node_type_id, :integer, :optional, "服务类型ID,此字段用来按服务类型筛选"
  end
  def index
    if params[:is_enabled].present?
      is_enabled = params[:is_enabled] == 'true' ? true : false
    end
    @regions = NodeRegion
      .includes(:node_type, :node_continent, :node_country) # n+1
      .joins(:node_type, :node_continent, :node_country)
      .the_where(node_type_id: params[:node_type_id], is_enabled: is_enabled)
      .order("node_regions.node_type_id ASC, node_areas.sequence ASC, node_countries_node_regions.sequence ASC," "node_regions.sequence ASC")
      .page(param_page).per(param_limit)
  end

  swagger_api :create do |api|
    summary "服务器群组管理 - 地域配置 - 创建"
    notes ""
    Api::Manage::V1::ApiController.common_params(api)
    param :form, :name, :string, :required, "区域名"
    param :form, :abbr, :string, :optional, "区域简写代码(ISO 3166-1)"
    param :form, :node_type_id, :integer, :required, "服务器类型"
    param :form, :node_country_id, :integer, :required, "服务器所属国家id"
    param :form, :node_continent_id, :integer, :required, "服务器所属洲id"
    param :form, :sequence, :integer, :optional, "显示顺序"
    param :form, :is_enabled, :string, :optional, "是否启用 true|false"
  end
  def create
    requires! :name, type: String
    requires! :node_type_id, type: Integer
    requires! :node_country_id, type: Integer
    requires! :node_continent_id, type: Integer

    @region = NodeRegion.new(allowed_params)
    error_detail!(@region) if !@region.save
  end

  swagger_api :show do |api|
    summary "服务器群组管理 - 地域配置 - 单个获取"
    notes ""
    Api::Manage::V1::ApiController.common_params(api)
    param :path, :id, :integer, :required, "区域ID"
  end
  def show
    requires! :id, type: Integer
  end

  swagger_api :update do |api|
    summary "服务器群组管理 - 地域配置 - 更新"
    notes ""
    Api::Manage::V1::ApiController.common_params(api)
    param :path, :id, :integer, :required, "区域ID"
    param :form, :name, :string, :optional, "区域名"
    param :form, :abbr, :string, :optional, "区域简写代码(ISO 3166-1)"
    param :form, :node_type_id, :integer, :optional, "服务器类型"
    param :form, :node_country_id, :integer, :optional, "服务器所属国家id"
    param :form, :node_continent_id, :integer, :optional, "服务器所属洲id"
    param :form, :sequence, :integer, :optional, "显示顺序"
    param :form, :is_enabled, :string, :optional, "是否启用 true|false"
  end
  def update
    requires! :id, type: Integer
    optional! :name, type: String
    optional! :abbr, type: String
    optional! :node_type_id, type: Integer
    optional! :is_enabled, type: String

    error_detail!(@region) if !@region.update(allowed_params)
  end

  private

  def set_region
    @region = NodeRegion.find(params[:id])
  end

  def allowed_params
    params.permit(:name, :abbr, :node_type_id, :node_country_id, :node_continent_id, :sequence, :is_enabled)
  end

end

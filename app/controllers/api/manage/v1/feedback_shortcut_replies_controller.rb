class Api::Manage::V1::FeedbackShortcutRepliesController < Api::Manage::V1::ApiController
  # 设置权限
  include PermissionFilterable
  set_module_group_name :user_manage

  swagger_controller :feedback_shortcut_replies, "反馈快捷回复"

  swagger_api :index do |api|
    summary "反馈快捷回复管理 - 列表"
    notes ""
    Api::Manage::V1::ApiController.common_params(api)
    param :query, :category_id, :integer, :required, "快捷回复分类ID"
  end
  def index
    requires! :category_id, type: Integer

    category = FeedbackShortcutReplyCategory.find(params[:category_id])
    @items = category.feedback_shortcut_replies
  end

  swagger_api :create do |api|
    summary "反馈快捷回复管理 - 创建"
    notes ""
    Api::Manage::V1::ApiController.common_params(api)
    param :form, :category_id, :integer, :required, "快捷回复分类ID"
    param :form, :content, :string, :required, "快捷回复内容"
  end
  def create
    requires! :category_id, type: Integer
    requires! :content, type: String

    @item = FeedbackShortcutReply.create(
      feedback_shortcut_reply_category_id: params[:category_id],
      content: params[:content]
    )
    error_detail!(@item) if !@item.save
  end

  swagger_api :update do |api|
    summary "反馈快捷回复管理 - 更新"
    notes ""
    Api::Manage::V1::ApiController.common_params(api)
    param :path, :id, :integer, :required, "快捷回复的ID"
    param :form, :category_id, :integer, :optional, "快捷回复分类ID"
    param :form, :content, :string, :optional, "快捷回复内容"
  end
  def update
    requires! :id, type: Integer
    optional! :category_id, type: Integer
    optional! :content, type: String

    @item = FeedbackShortcutReply.find(params[:id])
    @item.feedback_shortcut_reply_category_id = params[:category_id].to_i if params[:category_id].present?
    @item.content = params[:content] if params[:content].present?
    error_detail!(@item) if !@item.save
  end

  swagger_api :destroy do |api|
    summary "反馈快捷回复管理 - 删除"
    notes ""
    Api::Manage::V1::ApiController.common_params(api)
    param :path, :id, :integer, :required, "快捷回复的ID"
  end
  def destroy
    requires! :id, type: Integer

    @item = FeedbackShortcutReply.find(params[:id])
    @item.destroy if @item.present?
  end

end

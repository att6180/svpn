class Api::Manage::V1::ApiController < Api::ManageApiController

  helper ApplicationHelper
  include ActionView::Layouts

  layout 'manage_api'

  helper_method :current_admin

  before_action :setup_layout_elements
  before_action :authenticate_user!

  def append_info_to_payload(payload)
    if LogStasher.enabled?
      super
      LogStasher.store[:admin_id] = current_admin.id if current_admin.present?
    end
  end

  class ParameterDecryptFail < ActionController::ParameterMissing
    def initialize(param)
      @param = param
      super(param)
    end
  end

  class ParameterDecodeFail < ActionController::ParameterMissing
    def initialize(param)
      @param = param
      super(param)
    end
  end

  class ParameterValueNotAllowed < ActionController::ParameterMissing
    attr_reader :values
    def initialize(param, values)
      @param = param
      @values = values
      super("参数#{param}的值只允许在#{values}以内")
    end
  end

  class AccessDenied < StandardError; end
  class PageNotFound < StandardError; end

  rescue_from(ActionController::ParameterMissing) do |err|
    render json: { success: false, error: 'InvalidParameter', messages: ["参数#{err.param}未提供或为空"] }, status: 400
  end
  rescue_from(ActiveRecord::RecordInvalid) do |err|
    render json: { success: false, error: 'InvalidRecord', messages: [err] }, status: 400
  end
  rescue_from(ActiveRecord::RecordNotUnique) do |err|
    render json: { success: false, error: 'RecordNotUnique', messages: ['记录已存在'] }, status: 400
  end
  rescue_from(AccessDenied) do |err|
    render json: { success: false, error: 'AccessDenied', messages: [err] }, status: 403
  end
  rescue_from(ActiveRecord::RecordNotFound) do
    render json: { success: false, error: 'ResourceNotFound', messages: ['记录未找到'] }, status: 404
  end
  rescue_from(ParameterDecryptFail) do |err|
    render json: { success: false, error: 'ParameterDecryptFail', messages: ["参数#{err.param}解密失败"] }, status: 400
  end
  rescue_from(ParameterDecodeFail) do |err|
    render json: { success: false, error: 'ParameterDecodeFail', messages: ["参数#{err.param}解码失败"] }, status: 400
  end

  def current_admin
    @current_admin
  end

  def client_ip
    # 首先获取 X_REAL_IP，如果开头为10，或者为共享地址，则获取CDN_SRC_IP
    # 如果还是为空(本地开发环境)，则获取remote_ip
    result = request.headers["HTTP_X_REAL_IP"]&.split(',')&.first
    if result.blank? || Utils::IP.is_private_ip?(result)
      result = request.headers["HTTP_CDN_SRC_IP"]
    end
    result = request.remote_ip if result.blank?
    result
  end

  #def client_ip
    #result = request.headers["X-Forwarded-For"]&.split(',')&.first
    #result = request.remote_ip if result.blank?
    #result
  #end

  # 设置必选参数
  def requires!(name, opts = {})
    opts[:require] = true
    optional!(name, opts)
  end

  def optional!(name, opts = {})
    # TODO: 加入参数说明，改进参数错误提示友好度
    if params[name].blank? && opts[:require] == true
      raise ActionController::ParameterMissing.new(name)
    end

    if opts[:values] && params[name].present?
      values = opts[:values].to_a
      if !values.include?(params[name]) && !values.include?(params[name].to_i)
        raise ParameterValueNotAllowed.new(name, opts[:values])
      end
    end

    if params[name].blank? && opts[:default].present?
      params[name] = opts[:default]
    end
  end

  def api_t(key)
    I18n.t("api.manage.#{key}")
  end

  # 返回错误信息
  def error!(msg, error_flag = nil)
    @success = false
    @messages << msg
    json = { success: @success, messages: @messages }
    json[:error] = error_flag if error_flag.present?
    render json: json
  end

  # 显示详细错误信息
  def error_detail!(obj)
    @success = false
    messages = obj.errors.messages
    if messages.present?
      messages.each do |key, value|
        value.each do |msg|
          @messages << "#{key}#{msg}"
        end
      end
    else
      @messages << '请求失败'
    end
    render json: { success: @success, messages: @messages }
  end

  # 按模块类级别权限访问检查
  def module_group_permission_filter!
    if self.class.module_group_name.present?
      raise AccessDenied.new('没有权限访问此页面') unless @current_admin.can?(self.class.module_group_name)
    end
  end

  # action级别检查
  def can_access?(module_group_name)
    raise AccessDenied.new('没有权限访问此页面') unless @current_admin.can?(module_group_name)
  end

  def param_page
    params[:page] || 1
  end

  def param_limit
    result = params[:limit] || 25
    result = 50 if result.to_i > 50
    result
  end

  # 处理渠道版本的筛选
  def has_channel_param?
    params[:app_channel].present? || params[:app_version].present?
  end

  def has_package_param?
    params[:platform_id].present? || params[:channel_account_id].present? || params[:package_id].present?
  end

  def handle_channel_params(items)
    if has_channel_param?
      items = items.where(app_version: [nil, params[:app_version]]) if params[:app_version].present?
      items = items.where(app_channel: [nil, params[:app_channel]]) if params[:app_channel].present?
      items = items.where(is_total: false)
    else
      items = items.where(is_total: true)
    end
    items
  end

  def handle_version_params(items)
    if has_channel_param?
      if params[:app_channel].present? && !params[:app_version].present?
        items = items.where(app_channel: [nil, params[:app_channel]], app_version: nil)
        items = items.where(filter_type: 1)
      elsif !params[:app_channel].present? && params[:app_version].present?
        items = items.where(app_channel: nil, app_version: [nil, params[:app_version]])
        items = items.where(filter_type: 1)
      elsif params[:app_channel].present? && params[:app_version].present?
        items = items.where(app_channel: [nil, params[:app_channel]], app_version: [nil, params[:app_version]])
        items = items.where(filter_type: 2)
      end
    else
       items = items.where(filter_type: 0)
    end
    items
  end

  # 处理包名、渠道账号、渠道、版本等实时数据筛选
  def handle_muilt_version_params(items, index = 0)

    atts = [
      [:app_channel, :app_version],
      [:create_app_channel, :create_app_version]
    ]
    channel, version = atts[index]
    if has_channel_param?
      items = items.the_where("#{channel}": params[:app_channel], "#{version}": params[:app_version])
    elsif has_package_param?
      channels, versions = SystemEnumRelation
      .the_where(
        platform_id: params[:platform_id],
        channel_account_id: params[:channel_account_id],
        package_id: params[:package_id]
       )
      .pluck(:channel, :version)
      .transpose
      items = items.the_where("#{channel}": channels, "#{version}": versions)
    end
    items
  end

  # 处理包名、渠道账号、渠道、版本等历史数据筛选
  def handle_muilt_version_histroy_params(items)
    if has_channel_param?
      items = handle_version_params(items)
    elsif has_package_param?
      channels, versions = SystemEnumRelation
      .the_where(
        platform_id: params[:platform_id],
        channel_account_id: params[:channel_account_id],
        package_id: params[:package_id]
       )
      .pluck(:channel, :version)
      .transpose
      # 如果筛选为空查全部
      channels ||= [nil]
      versions ||= [nil]
      items = items.the_where(app_channel: channels, app_version: versions)
    else
      items = items.the_where(app_channel: [nil], app_version: [nil])
    end
    items
  end

  def handle_package_params(items)
    if has_package_param?
      items = items.the_where(
        platform_id: params[:platform_id],
        channel_account_id: params[:channel_account_id],
        package_id: params[:package_id]
      )
    else
      items = items.where(filter_type: 0)
    end
  end

  def handle_channel_params_unstat(items)
    if has_channel_param?
      items = items.where(app_version: [nil, params[:app_version]]) if params[:app_version].present?
      items = items.where(app_channel: [nil, params[:app_channel]]) if params[:app_channel].present?
    end
    items
  end

  def cast(value)
    ActiveModel::Type::Boolean.new.cast(value)
  end

  # 如果有时间段参数，则检查范围，没有则忽略
  def allowable_date_range?(start_at = nil, end_at = nil)
    result = true
    if start_at.blank? || end_at.blank?
      if params[:start_at].present? && params[:end_at].present?
        start_at = params[:start_at].to_date
        end_at = params[:end_at].to_date
      end
    end
    result = false if (start_at.present? && end_at.present?) && (end_at - start_at).to_i > 31
    result
  end

  # 取得默认时间段
  def default_start_end_at
    yesterday = Time.now.to_date.yesterday
    if params[:start_at].present? && params[:end_at].present?
      start_at = params[:start_at].to_date > yesterday ? yesterday : params[:start_at].to_date
      end_at = params[:end_at].to_date > yesterday ? yesterday : params[:end_at].to_date
    else
      start_at = Time.now.to_date.beginning_of_month
      end_at = Time.now.to_date.yesterday
    end
    [start_at, end_at]
  end

  def setup_layout_elements
    @success = true
    @messages = []
  end

  # 根据headers认证用户
  def authenticate_user!
    auth_jwt!
    rescue JWT::ExpiredSignature
      error!('令牌过期', 'InvalidToken')
    rescue JWT::VerificationError
      error!('令牌验证错误', 'InvalidToken')
    rescue JWT::DecodeError
      error!('令牌非法', 'InvalidToken')
    rescue JWT::InvalidIssuerError
      error!(e, 'InvalidToken')
  end

  def verify_date_range!
    error!(api_t('invalid_date_range')) if !allowable_date_range?
  end

  def auth_jwt!
    jwt = request.headers['HTTP_AUTHORIZATION']&.split(' ')&.last
    raise JWT::VerificationError.new if jwt.blank?
    # 读取令牌携带用户信息，此处不作令牌的验证，不会抛出异常
    payload, header = JWT.decode(jwt, nil, false, verify_expiration: true)
    admin = ::Manage::Admin.find_by(id: payload['id'])
    # 获取验证令牌的密钥
    secret_key = admin ? admin.api_token : ''
    # 用秘钥验证令牌，会抛出 JWT::ExpiredSignature 或 JWT::DecodeError 异常
    payload, header = JWT.decode(jwt, secret_key)
    # 验证成功，设置当前用户
    @current_admin = admin
  end

end

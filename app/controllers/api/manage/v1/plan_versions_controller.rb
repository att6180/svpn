class Api::Manage::V1::PlanVersionsController < Api::Manage::V1::ApiController

  # 设置权限
  include PermissionFilterable
  set_module_group_name :charge_manage

  before_action :set_item, only: [:show, :update, :destroy]

  swagger_controller :plan_versions, "套餐版本配置表"

  swagger_api :index do |api|
    summary "套餐版本配置表 - 列表"
    notes ""
    Api::Manage::V1::ApiController.common_params(api)
    param :query, :channel, :string, :optional, "渠道，如果传all，则为全部"
    param :query, :version, :string, :optional, "版本，如果传all，则为全部"
    param :query, :status, :string, :optional, "状态, 0:禁用, 1:启用, 不提供此参数时则返回所有启用的记录"
    Api::Manage::V1::ApiController.page_params(api)
  end
  def index
    items = V2::PlanVersion.all
    items = items.where(channel: params[:channel]) if params[:channel].present?
    items = items.where(version: params[:version]) if params[:version].present?
    items = if params[:status].present?
      items.where(status: params[:status])
    else
      items.enabled
    end
    @items = items.page(param_page).per(param_limit)
  end

  swagger_api :create do |api|
    summary "套餐版本配置表 - 创建"
    notes ""
    Api::Manage::V1::ApiController.common_params(api)
    param :form, :channel, :string, :required, "渠道"
    param :form, :version, :string, :required, "版本"
    param :form, :audit_template_id, :string, :optional, "审核套餐模板ID"
    param :form, :domestic_template_id, :string, :optional, "国内第三方套餐模板ID"
    param :form, :oversea_template_id, :string, :optional, "国外第三方套餐模板ID"
    param :form, :iap_template_id, :string, :optional, "IAP套餐模版ID"
    param :form, :show_icon, :string, :optional, "是否显示活动图标, true|false"
    param :form, :tab_id_sort, :string, :required, "标签页顺序, 使用标签页id用,号间隔"
    param :form, :status, :integer, :required, "状态, 0:禁用, 1:启用, 默认为1"
  end
  def create
    requires! :channel, type: String
    requires! :version, type: String
    requires! :tab_id_sort, type: String
    requires! :status, type: String

    @item = V2::PlanVersion.new(allowed_params)
    error_detail!(@item) if !@item.save
  end

  swagger_api :show do |api|
    summary "套餐版本配置表 - 单个获取"
    notes ""
    Api::Manage::V1::ApiController.common_params(api)
    param :path, :id, :integer, :required, "套餐ID"
  end
  def show
    requires! :id, type: Integer
  end

  swagger_api :update do |api|
    summary "套餐版本配置表 - 更新"
    notes ""
    Api::Manage::V1::ApiController.common_params(api)
    param :path, :id, :integer, :optional, "套餐ID"
    param :form, :channel, :string, :optional, "渠道"
    param :form, :version, :string, :optional, "版本"
    param :form, :audit_template_id, :string, :optional, "审核套餐模板ID"
    param :form, :domestic_template_id, :string, :optional, "国内第三方套餐模板ID"
    param :form, :oversea_template_id, :string, :optional, "国外第三方套餐模板ID"
    param :form, :iap_template_id, :string, :optional, "IAP套餐模版ID"
    param :form, :show_icon, :string, :optional, "是否显示活动图标, true|false"
    param :form, :tab_id_sort, :string, :optional, "标签页顺序, 使用标签页id用,号间隔"
    param :form, :status, :integer, :optional, "状态, 0:禁用, 1:启用"
  end
  def update
    error_detail!(@item) if !@item.update(allowed_params)
  end

  private

  def allowed_params
    params.permit(
      :channel,
      :version,
      :audit_template_id,
      :domestic_template_id,
      :oversea_template_id,
      :iap_template_id,
      :show_icon,
      :tab_id_sort,
      :status
    )
  end

  def set_item
    @item = V2::PlanVersion.find(params[:id])
  end

end

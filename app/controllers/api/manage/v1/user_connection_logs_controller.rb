class Api::Manage::V1::UserConnectionLogsController < Api::Manage::V1::ApiController

  # 设置权限
  include PermissionFilterable
  set_module_group_name :user_base_info

  swagger_controller :user_connection_logs, "去向IP"

  swagger_api :index do |api|
    summary "去向IP - 列表"
    notes "筛选类型按月时，开始时间和结束时间的年和月选择完成后，日可以随例选哪一天都行"
    Api::Manage::V1::ApiController.common_params(api)
    param :query, :limit, :integer, :optional, "显示的记录数, 默认: 100"
    param :query, :stat_type, :string, :optional, "筛选方式: year|month|date: 按年|按月|按日期"
    param :query, :start_at, :string, :optional, "日期筛选开始时间"
    param :query, :end_at, :string, :optional, "日期筛选结束时间"
  end
  def index
    stat_type = params[:stat_type] || 'date'
    if stat_type == 'month'
      start_at = "#{params[:start_at]}-01"&.to_date&.beginning_of_month
      end_at = "#{params[:end_at]}-01"&.to_date&.end_of_month
    else
      start_at = params[:start_at]&.to_date
      end_at = params[:end_at]&.to_date
    end

    @logs = TrendStatService.stat_user_connection_log(
      stat_type,
      start_at.strftime("%Y-%m-%d"),
      end_at.strftime("%Y-%m-%d"),
      (params[:limit] || 100).to_s
    )
  end

  swagger_api :user_details do |api|
    summary "去向IP - 访问用户数详情"
    notes "查看指定时间内访问指定域名的所有用户及其访问的次数"
    Api::Manage::V1::ApiController.common_params(api)
    param :query, :domain, :string, :required, "指定域名"
    param :query, :stat_type, :string, :required, "筛选方式: year|month|date: 按年|按月|按日期"
    param :query, :start_at, :string, :required, "日期筛选开始时间，如: 2017-04-01，筛选方式都可以使用这种日期格式，后端会自动取年、年月、日期"
    param :query, :end_at, :string, :required, "日期筛选结束时间，格式同上"
    param :query, :page, :integer, :optional, "页码，默认: 1"
    param :query, :limit, :integer, :optional, "每页显示的记录数, 默认: 25"
  end
  def user_details
    requires! :domain, type: String
    requires! :stat_type, type: String
    requires! :start_at, type: String
    requires! :end_at, type: String

    stat_type = params[:stat_type] || 'date'
    if stat_type == 'month'
      start_at = "#{params[:start_at]}-01"&.to_date&.beginning_of_month
      end_at = "#{params[:end_at]}-01"&.to_date&.end_of_month
    else
      start_at = params[:start_at]&.to_date
      end_at = params[:end_at]&.to_date
    end

    @logs = TrendStatService.stat_user_connection_log_users(
      params[:domain],
      stat_type,
      start_at.strftime("%Y-%m-%d"),
      end_at.strftime("%Y-%m-%d"),
      (params[:limit] || 100).to_s
    )
    users = User.select(:id, :username).where(id: @logs.map{|log| log[:user_id]})
    @users_group = users.group_by(&:id)
  end

  swagger_api :visit_details do |api|
    summary "去向IP - 访问次数详情"
    notes "查看指定时间内访问指定域名的指定用户的记录"
    Api::Manage::V1::ApiController.common_params(api)
    param :query, :user_id, :integer, :required, "指定用户id"
    param :query, :domain, :string, :required, "指定域名"
    param :query, :stat_type, :string, :required, "筛选方式: month|date: 按年|按月|按日期"
    param :query, :start_at, :string, :required, "日期筛选开始时间"
    param :query, :end_at, :string, :required, "日期筛选结束时间"
    Api::Manage::V1::ApiController.page_params(api)
  end
  def visit_details
    requires! :user_id, type: Integer
    requires! :domain, type: String
    requires! :stat_type, type: String
    requires! :start_at, type: String
    requires! :end_at, type: String

    user_id = params[:user_id]
    stat_type = params[:stat_type] || 'month'
    if stat_type == 'month'
      start_at = "#{params[:start_at]}-01"&.to_date&.beginning_of_month || Time.now.beginning_of_month
      end_at = "#{params[:end_at]}-01"&.to_date&.end_of_month || Time.now.end_of_month
    else
      start_at = params[:start_at]&.to_date&.beginning_of_day || Time.now.beginning_of_day
      end_at = params[:end_at]&.to_date&.end_of_day || Time.now.end_of_day
    end

    @logs = Mongo::UserConnectionLog.where(user_id: user_id)
      .order(created_at: :desc)
      .where(:created_at.gte => start_at, :created_at.lte => end_at)
      .where(domain: params[:domain])
      .page(param_page).per(param_limit)
  end

  swagger_api :domain_region_details do |api|
    summary "去向IP - 地域分析详情"
    notes "查看指定域名的地域、人数、占比"
    Api::Manage::V1::ApiController.common_params(api)
    param :query, :domain, :string, :optional, "指定域名"
    param :query, :limit, :integer, :optional, "显示的记录数, 默认: 100"
  end
  def domain_region_details
    domain = params[:domain] || ''
    limit = params[:limit] || 100
    @logs = TrendStatService.stat_user_connection_log_region(domain, limit)
  end

  swagger_api :domain_country_details do |api|
    summary "去向IP - 地域分析详情"
    notes "查看每个国家访问指定域名的人数"
    Api::Manage::V1::ApiController.common_params(api)
    param :query, :domain, :string, :optional, "指定域名，这里不设置必选是因为有的域名是空的"
    param :query, :limit, :integer, :optional, "显示的记录数, 默认: 25"
  end
  def domain_country_details
    domain = params[:domain] || ''
    limit = params[:limit] || 100
    @logs = TrendStatService.stat_user_connection_log_country(domain, limit)
  end

end

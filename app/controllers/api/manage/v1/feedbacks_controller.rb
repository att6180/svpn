class Api::Manage::V1::FeedbacksController < Api::Manage::V1::ApiController

  # 设置权限
  include PermissionFilterable
  set_module_group_name :user_manage

  swagger_controller :feedbacks, "用户反馈"

  swagger_api :index do |api|
    summary "用户管理 - 用户反馈 - 列表"
    notes ""
    Api::Manage::V1::ApiController.common_params(api)
    param :query, :page, :integer, :optional, "页码，默认: 1"
    param :query, :limit, :integer, :optional, "每页显示的记录数, 默认: 25"
    param :query, :app_version, :string, :optional, "版本筛选"
    param :query, :app_channel, :string, :optional, "渠道筛选"
    param :query, :start_at, :string, :optional, "开始日期，如: 2017-11-04"
    param :query, :end_at, :string, :optional, "结束日期，如: 2017-11-05"
    param :query, :feedback_type, :string, :optional, "反馈类型, connection: 连接问题, crash: 充值问题, suggest: 帐号问题, other: 其他问题"
    param :query, :query_type, :integer, :optional, "用户帐号查询类型, 0:按用户名, 1:按用户ID，默认0"
    param :query, :q, :integer, :optional, "用户帐号关键字"
    param :query, :status, :integer, :optional, "反馈状态, 0:未处理, 1:已回复"
  end
  def index
    query_type = params[:query_type] || '0'

    if params[:q].present? && query_type != '0' && (params[:q].strip.to_i > 2147483648 || params[:q].strip.to_i <= 0)
      error!("用户帐号搜索条件有误，请重新输入。") and return
    end

    if params[:q].present?
      user = if query_type == '0'
        User.find_by(username: params[:q])
      else
        User.find_by(id: params[:q])
      end
      feedbacks = user.feedbacks if user.present?
    else
      feedbacks = Feedback.all
    end
    if !feedbacks.nil? && feedbacks.any?
      if params[:start_at].present? && params[:end_at].present?
        feedbacks = feedbacks.where(created_at: params[:start_at].to_date.beginning_of_day..params[:end_at].to_date.end_of_day)
      end
      feedbacks = feedbacks.where(app_version: params[:app_version]) if params[:app_version].present?
      feedbacks = feedbacks.where(app_channel: params[:app_channel]) if params[:app_channel].present?
      feedbacks = feedbacks.where(feedback_type: params[:feedback_type]) if params[:feedback_type].present?
      feedbacks = feedbacks.where(status: params[:status]) if params[:status].present?
      @feedbacks = feedbacks.order(status: :asc, updated_at: :desc).includes(:user).page(param_page).per(param_limit)
    else
      @feedbacks = []
    end
  end

  swagger_api :show do |api|
    summary "用户管理 - 用户反馈 - 显示"
    notes ""
    Api::Manage::V1::ApiController.common_params(api)
    param :path, :id, :integer, :required, "反馈ID"
  end
  def show
    requires! :id, type: Integer

    @feedback = Feedback.find(params[:id])
  end

  swagger_api :reply do |api|
    summary "用户管理 - 用户反馈 - 管理员回复"
    notes ""
    Api::Manage::V1::ApiController.common_params(api)
    param :path, :id, :integer, :required, "反馈ID"
    param :form, :content, :string, :required, "管理员回复内容"
  end
  def reply
    requires! :id, type: Integer
    requires! :content, type: String

    @feedback = Feedback.find(params[:id])
    if allowed_params[:content].present?
      @reply = @feedback.feedback_logs.create(
        memberable: current_admin,
        content: params[:content]
      )
      if @reply.errors.blank?
        @feedback.status = 1
        @feedback.status_updated_at = Time.now
        @feedback.has_read2 = false
        @feedback.has_read_at2 = nil
        # 检查旧版反馈reply_content字段如果为空，则将第一次管理员回复写入
        if @feedback.reply_content.blank?
          @feedback.reply_content = params[:content]
          @feedback.is_processed = true
          @feedback.processed_at = Time.now
        end
        if @feedback.save
        else
          error_detail!(@feedback)
        end
      else
        error_detail!(@reply)
      end
    else
      error!("反馈回复失败，请填写回复内容")
    end
  end

  swagger_api :replies do |api|
    summary "用户管理 - 用户反馈 - 获取回复列表"
    notes ""
    Api::Manage::V1::ApiController.common_params(api)
    param :path, :id, :integer, :required, "反馈ID"
    Api::Manage::V1::ApiController.page_params(api)
  end
  def replies
    requires! :id, type: Integer

    feedback = Feedback.find(params[:id])
    @logs = feedback.feedback_logs.sorted
      .page(param_page).per(param_limit)
      .includes(:memberable)
      .to_a.reverse
  end

  swagger_api :shortcut_replies do |api|
    summary "用户管理 - 用户反馈 - 获取全部快捷回复"
    notes ""
    Api::Manage::V1::ApiController.common_params(api)
  end
  def shortcut_replies
    @items = FeedbackShortcutReplyCategory.cache_list
  end

  private

  def allowed_params
    params.permit(
      :content
    )
  end

end

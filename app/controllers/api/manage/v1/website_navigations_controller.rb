class Api::Manage::V1::WebsiteNavigationsController < Api::Manage::V1::ApiController

  before_action :set_item, only: [:show, :update, :destroy]

  swagger_controller :website_navigations, "导航设置"

  swagger_api :index do |api|
    summary "导航帮助 - 导航设置"
    notes ""
    Api::Manage::V1::ApiController.common_params(api)
    param :query, :is_domestic, :string, :optional, "是否是国内, true:是|false:否"
    Api::Manage::V1::ApiController.page_params(api)
  end
  def index
    can_access?(:user_behaviour)

    items = WebsiteNavigation.page(param_page).per(param_limit)
    items = items.where(is_domestic: params[:is_domestic] == 'true') if params[:is_domestic].present?
    @items = items.order(nav_type: :asc, website_type: :desc, sort_id: :asc)
  end

  swagger_api :create do |api|
    summary "导航帮助 - 导航设置 - 创建"
    notes ""
    Api::Manage::V1::ApiController.common_params(api)
    param :form, :name, :string, :required, "网站名称"
    param :form, :url, :string, :required, "网站URL"
    param :form, :icon_url, :string, :required, "网站图标URL"
    param :form, :website_type, :integer, :required, "网站类型: 1:推荐|2:火热"
    param :form, :sort_id, :integer, :optional, "用来排序的序号，如:1，默认为0"
    param :form, :is_domestic, :string, :optional, "是否是国内，true:国内|false:国外, 默认为false"
    param :form, :description, :string, :optional, "说明"
    param :form, :nav_type, :string, :required, "导航分类: website:推荐网站|app:推荐应用, 默认为推荐网站"
    param :form, :info_type, :string, :required, "信息分类: nav:导航类|ad:广告类, 默认为导航类"
  end
  def create
    requires! :name, type: String
    requires! :url, type: String
    requires! :icon_url, type: String
    requires! :website_type, type: String
    requires! :sort_id, type: String
    requires! :is_domestic, type: String
    requires! :nav_type, type: String
    requires! :info_type, type: String

    @item = WebsiteNavigation.new(allowed_params)
    error_detail!(@item) if !@item.save
  end

  swagger_api :show do |api|
    summary "导航帮助 - 导航设置 - 单个获取"
    notes ""
    Api::Manage::V1::ApiController.common_params(api)
    param :path, :id, :integer, :required, "ID"
  end
  def show
    requires! :id, type: Integer
  end

  swagger_api :update do |api|
    summary "导航帮助 - 导航设置 - 更新"
    notes ""
    Api::Manage::V1::ApiController.common_params(api)
    param :path, :id, :integer, :required, "ID"
    param :form, :name, :string, :optional, "网站名称"
    param :form, :url, :string, :optional, "网站URL"
    param :form, :icon_url, :string, :optional, "网站图标URL"
    param :form, :website_type, :integer, :optional, "网站类型: 1:推荐|2:火热"
    param :form, :sort_id, :integer, :optional, "用来排序的序号，如:1，默认为0"
    param :form, :is_domestic, :string, :optional, "是否是国内，true:国内|false:国外, 默认为false"
    param :form, :description, :string, :optional, "说明"
    param :form, :nav_type, :string, :optional, "导航分类: website:推荐网站|app:推荐应用, 默认为推荐网站"
    param :form, :info_type, :string, :optional, "信息分类: nav:导航类|ad:广告类, 默认为导航类"
  end
  def update
    @item.touch(:updated_at)
    error_detail!(@item) if !@item.update(allowed_params)
  end

  swagger_api :destroy do |api|
    summary "导航帮助 - 导航设置 - 删除"
    notes ""
    Api::Manage::V1::ApiController.common_params(api)
    param :path, :id, :integer, :required, "ID"
  end
  def destroy
    requires! :id, type: Integer

    error_detail!(@item) if !@item.destroy
  end

  private

  def allowed_params
    params.permit(
      :name,
      :url,
      :icon_url,
      :website_type,
      :sort_id,
      :is_domestic,
      :description,
      :nav_type,
      :info_type
    )
  end

  def set_item
    @item = WebsiteNavigation.find(params[:id])
  end
end

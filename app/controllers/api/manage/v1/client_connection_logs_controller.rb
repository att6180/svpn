class Api::Manage::V1::ClientConnectionLogsController < Api::Manage::V1::ApiController

  # 设置权限
  include PermissionFilterable
  set_module_group_name :user_manage

  swagger_controller :client_connection_logs, "客户端去向日志"

  swagger_api :index do |api|
    summary "客户端去向日志 - 列表"
    notes ""
    Api::Manage::V1::ApiController.common_params(api)
    Api::Manage::V1::ApiController.between_date_params(api)
    Api::Manage::V1::ApiController.version_params(api)
    param :query, :is_proxy, :string, :optional, "代理形式: true:代理, false:直连"
    param :query, :query_type, :string, :optional, "查询方式, 1:帐号名, 2:帐号id"
    param :query, :q, :string, :optional, "查询关键字"
    Api::Manage::V1::ApiController.page_params(api)
  end
  def index
    @logs = []
    # 暂时停用此接口
    #if params[:query_type].present? && params[:q].present?
      #if params[:query_type] == '1'
        #user = User.find_by(username: params[:q])
      #else
        #user = User.find_by(id: params[:q])
      #end
      #error!('用户未找到') and return if user.blank?
      #logs = user.client_connection_logs
    #else
      #logs = ClientConnectionLog.all
    #end
    #if params[:start_at].present? && params[:end_at].present?
      #logs = logs.where(created_at: params[:start_at].to_date.beginning_of_day..params[:end_at].to_date.end_of_day)
    #end
    #logs = logs.where(app_channel: params[:app_channel]) if params[:app_channel].present?
    #logs = logs.where(app_version: params[:app_version]) if params[:app_version].present?
    #logs = logs.where(is_proxy: params[:is_proxy] == 'true') if params[:is_proxy].present?
    #@logs = logs.recent.includes(:user, :node).page(param_page).per(param_limit)
  end

end

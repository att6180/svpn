class Api::Manage::V1::AdminsController < Api::Manage::V1::ApiController

  swagger_controller :admins, "管理员管理"

  swagger_api :index do |api|
    summary "管理员管理 - 列表"
    notes ""
    Api::Manage::V1::ApiController.common_params(api)
    param :query, :is_enabled, :string, :optional, "根据帐号是否禁用过滤, 1|0"
    Api::Manage::V1::ApiController.page_params(api)
  end
  def index
    optional! :is_enabled, type: String
    optional! :page, type: Integer
    optional! :limit, type: Integer

    can_access?(:back_stage_manage)
    admins = ::Manage::Admin.page(param_page).per(param_limit)
    admins = admins.where(is_enabled: params[:is_enabled]) if params[:is_enabled].present?
    @admins = admins.sorted
  end

  swagger_api :create do |api|
    summary "管理员管理 - 创建"
    notes ""
    Api::Manage::V1::ApiController.common_params(api)
    param :form, :username, :string, :required, "管理员用户名"
    param :form, :password, :string, :required, "管理员密码"
    param :form, :password_confirmation, :string, :required, "管理员确认密码"
    param :form, :role_id, :integer, :required, "管理员权限组id"
  end
  def create
    requires! :username, type: String
    requires! :password, type: String
    requires! :password_confirmation, type: String
    requires! :role_id, type: Integer

    can_access?(:back_stage_manage)

    role = ::Manage::Role.find(params[:role_id])
    if role.name == 'super_admin' && !current_admin.super_admin?
      error!('超级管理员权限必须有超级管理员添加') and return
    end

    @admin = Manage::Admin.new(allowed_params)
    error_detail!(@admin) if !@admin.save
  end

  swagger_api :update do |api|
    summary "管理员管理 - 更新"
    notes ""
    Api::Manage::V1::ApiController.common_params(api)
    param :path, :id, :integer, :required, "ID"
    param :form, :new_password, :string, :optional, "管理员新密码"
    param :form, :password_confirmation, :string, :optional, "管理员确认密码"
    param :form, :role_id, :integer, :optional, "管理员权限组"
  end
  def update
    requires! :id, type: Integer

    @admin = Manage::Admin.find(params[:id])

    if params[:new_password].present?
      # 是否有权限修改管理员密码
      can_access?(:admin_password_update) if @admin.id != current_admin.id
      # 只有超级管理员自己才能修改自己的信息
      error!('只有超级管理自己才能修改超级管理员的密码') and return if @admin.super_admin? && @admin.id != current_admin.id
      if allowed_params[:new_password] != allowed_params[:password_confirmation]
        error!('新密码和确认密码不匹配') and return
      end

      @admin.password = allowed_params[:new_password]
      @admin.api_token = Utils::Gen.friendly_token
    end

    if params[:role_id].present?
      role = ::Manage::Role.find(params[:role_id])
      error!('只有超级管理员才能修改超级管理员权限组') and return if role.name == 'super_admin' && !current_admin.super_admin?
      # 是否有权限更新管理员权限组
      can_access?(:admin_role_update)
    end

    if params[:role_id].present?
      @admin.role_id = params[:role_id]
    end

    if @admin.changed?
      error_detail!(@admin) if !@admin.save
    end

  end

  swagger_api :disable do |api|
    summary "管理员管理 - 禁用"
    notes ""
    Api::Manage::V1::ApiController.common_params(api)
    param :path, :id, :integer, :required, "ID"
  end
  def disable
    requires! :id, type: Integer

    can_access?(:back_stage_manage)

    @admin = Manage::Admin.find(params[:id])

    error!('超级管理员帐号不可禁用') and return if @admin.super_admin?

    if @admin.is_enabled?
      error_detail!(@admin) if !@admin.update(is_enabled: false, api_token: Utils::Gen.friendly_token)
    end
  end

  swagger_api :enable do |api|
    summary "管理员管理 - 启用"
    notes ""
    Api::Manage::V1::ApiController.common_params(api)
    param :path, :id, :integer, :required, "ID"
  end
  def enable
    requires! :id, type: Integer

    can_access?(:back_stage_manage)

    @admin = Manage::Admin.find(params[:id])

    error!('超级管理员帐号不可启用') and return if @admin.super_admin?

    if !@admin.is_enabled?
      error_detail!(@admin) if !@admin.update(is_enabled: true, api_token: Utils::Gen.friendly_token)
    end
  end

  swagger_api :operation_logs do |api|
    summary "后台管理 - 管理员操作日志"
    notes ""
    Api::Manage::V1::ApiController.common_params(api)
    param :query, :page, :integer, :optional, "页码，默认: 1"
    param :query, :limit, :integer, :optional, "每页显示的记录数, 默认: 25"
  end
  def operation_logs
    can_access?(:back_stage_manage)
    @logs = Manage::Activity.recent.includes(:admin).page(param_page).per(param_limit)
  end

  swagger_api :coin_operation_logs do |api|
    summary "后台管理 - 管理员修改或奖励钻石操作日志"
    notes "此接口用于显示管理在后台修改某个用户或给多个用户批量奖励钻石时记录的日志，increased_coins: 新增的钻石数(只会在operation_type为奖励时显示)"
    Api::Manage::V1::ApiController.common_params(api)
    param :query, :admin_id, :integer, :optional, "管理员ID"
    param :query, :user_id, :integer, :optional, "用户ID"
    param :query, :page, :integer, :optional, "页码，默认: 1"
    param :query, :limit, :integer, :optional, "每页显示的记录数, 默认: 25"
  end
  def coin_operation_logs
    can_access?(:back_stage_manage)

    logs = ActivityCoinRewardLog.recent
    logs = logs.where(admin_id: params[:admin_id]) if params[:admin_id].present?
    logs = logs.where(user_id: params[:user_id]) if params[:user_id].present?
    @logs = logs.includes(:admin, :user).page(param_page).per(param_limit)
  end

  swagger_api :user_expired_reward_log do |api|
    summary "后台管理 - 管理员修改用户服务类型过期时间操作日志"
    notes "此接口用于显示管理在后台修改某个用户服务类型过期时间时记录的日志"
    Api::Manage::V1::ApiController.common_params(api)
    param :query, :admin_id, :integer, :optional, "管理员ID"
    param :query, :user_id, :integer, :optional, "用户ID"
    param :query, :page, :integer, :optional, "页码，默认: 1"
    param :query, :limit, :integer, :optional, "每页显示的记录数, 默认: 25"
  end
  def user_expired_reward_log
    @logs = UserExpiredRewardLog.includes(:admin, :user, :user_node_type, :node_type).the_where({
      admin_id: params[:admin_id],
      user_id: params[:user_id]
    }).recent.page(param_page).per(param_limit)
  end

  private

  def allowed_params
    params.permit(:username, :password, :old_password, :new_password, :password_confirmation, :role_id)
  end

end

class Api::Manage::V1::RescueDomainsController < Api::Manage::V1::ApiController
  # 设置权限
  include PermissionFilterable
  set_module_group_name :node_manage

  before_action :set_rescue_domain, only: [:update, :destroy]

  swagger_controller :rescue_domains, "救援域名配置"

  swagger_api :index do |api|
    summary "救援域名配置 - 列表"
    notes ""
    Api::Manage::V1::ApiController.common_params(api)
    Api::Manage::V1::ApiController.page_params(api)
    param :query, :domain_type, :string, :optional, "类型 owner|自己购买, thirdparty|第三方,如: oss"
    param :query, :is_enabled, :string, :optional, "状态 0|false, 1|true"
  end
  def index
    rescue_domains = RescueDomain.all
    if params[:domain_type].present?
      rescue_domains = rescue_domains.where(domain_type: params[:domain_type])
    elsif params[:is_enabled].present?
      rescue_domains = rescue_domains.where(is_enabled: params[:is_enabled])
    end
    @rescue_domains = rescue_domains.page(param_page).per(param_limit)
  end

  swagger_api :create do |api|
    summary "救援域名配置 - 新增"
    notes ""
    Api::Manage::V1::ApiController.common_params(api)
    param :form, :domain, :string, :required, "域名"
    param :form, :domain_type, :string, :required, "域名类型 owner|自己购买, third_party|第三方,如: oss"
    param :form, :is_enabled, :string, :required, "状态 disabled|禁用, enabled|开启"
  end

  def create
    requires! :domain, type: String
    requires! :domain_type, type: String
    requires! :is_enabled, type: String

      @rescue_domain = RescueDomain.new(
        domain: allowed_params[:domain],
        domain_type: allowed_params[:domain_type],
        is_enabled: allowed_params[:is_enabled]
      )
      error_detail!(@rescue_domain) if !@rescue_domain.save
  end

  swagger_api :update do |api|
    summary "救援域名配置 - 修改"
    notes ""
    Api::Manage::V1::ApiController.common_params(api)
    param :path, :id, :integer, :required, "域名ID"
    param :form, :domain, :string, :optional, "域名"
    param :form, :domain_type, :string, :optional, "域名类型 owner|自己购买, third_party|第三方,如: oss"
    param :form, :is_enabled, :string, :optional, "状态 disabled|禁用, enabled|开启"
  end

  def update
    requires! :id, type: Integer
    
    error_detail!(@rescue_domain) if !@rescue_domain.update(allowed_params)
  end

  swagger_api :destroy do |api|
    summary "救援域名配置 - 删除"
    notes ""
    Api::Manage::V1::ApiController.common_params(api)
    param :path, :id, :integer, :required, "域名id"
  end

  def destroy
    requires! :id, type: Integer

    error_detail!(@rescue_domain) if !@rescue_domain.destroy
  end

  private

  def allowed_params
    params.permit(
      :domain,
      :domain_type,
      :is_enabled,
      :response_time
    )
  end

   def set_rescue_domain
    @rescue_domain = RescueDomain.find(params[:id])
  end

end
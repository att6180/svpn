class Api::Manage::V1::ExcelsController < Api::Manage::V1::ApiController

  # include PermissionFilterable
  # set_module_group_name :charge_manage
  skip_before_action :authenticate_user!, only: [:download]

  swagger_controller :excels, "套餐配置excel上传下载"

  swagger_api :download do |api|
    summary "收费管理 - 套餐配置 - excel下载"
    notes <<-EOF
      注：请求该接口不能使用ajax
    EOF
    Api::Manage::V1::ApiController.common_params(api)
    param :query, :type, :integer, :required, "下载excel类型 1:客户端标签页/2: 套餐模版页/3: 模版详情页/4: 版本配置页"
  end
  def download
    requires! :type, type: Integer

    case params[:type].to_i
    when 1
      send_data(
        ExcelService::Download.plan_client_tab_excel,
        filename: "客户端标签页excel.xls",
        type: "application/excel",
        disposition: "attachment"
      )
    when 2
      send_data(
        ExcelService::Download.plan_template_excel,
        filename: "套餐模版页excel.xls",
        type: "application/excel",
        disposition: "attachment"
      )
    when 3
      send_data(
        ExcelService::Download.plan_excel,
        filename: "模版详情页excel.xls",
        type: "application/excel",
        disposition: "attachment"
      )
    when 4
      send_data(
        ExcelService::Download.plan_version_excel,
        filename: "版本配置页excel.xls",
        type: "application/excel",
        disposition: "attachment"
      )
    end
  end

  swagger_api :upload do |api|
    summary "收费管理 - 套餐配置 - excel上传"
    notes ""
    Api::Manage::V1::ApiController.common_params(api)
    param :form, :type, :integer, :required, "上传excel类型 1:客户端标签页/2: 套餐模版页/3: 模版详情页/4: 版本配置页"
    param :form, :file, :file, :required, "excel文件"
  end
  def upload
    requires! :type, type: Integer
    requires! :file, type: String

    filename = params[:file].original_filename
    tempfile = params[:file].tempfile

    if !filename.end_with? 'xls'
      error!('excel文件格式不是xls!') and return
    end

    case params[:type].to_i
    when 1
      ExcelService::Upload.parse_plan_client_tab(tempfile)
    when 2
      ExcelService::Upload.parse_plan_template(tempfile)
    when 3
      ExcelService::Upload.parse_plan(tempfile)
    when 4
      ExcelService::Upload.parse_plan_version(tempfile)
    end
  end

end

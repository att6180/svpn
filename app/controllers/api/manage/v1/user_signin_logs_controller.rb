class Api::Manage::V1::UserSigninLogsController < Api::Manage::V1::ApiController

  swagger_controller :user_signin_logs, "登录IP"

  swagger_api :index do |api|
    summary "用户行为 - 登录IP"
    notes ""
    Api::Manage::V1::ApiController.common_params(api)
    param :query, :start_at, :string, :optional, "日期筛选开始时间"
    param :query, :end_at, :string, :optional, "日期筛选结束时间"
    param :query, :order_by, :string, :optional, "按时间的排序方式, asc|desc, 默认:desc"
    param :query, :q, :string, :optional, "按按用户名或uuid搜索"
    Api::Manage::V1::ApiController.version_params(api)
    Api::Manage::V1::ApiController.page_params(api)
  end
  def index
    can_access?(:user_behaviour)

    @logs = []
    return

    if params[:q].present?
      user = User.search_by_keyword(params[:q])&.take
      if user.present?
        logs = user.user_signin_logs.order_by_created_at(params[:order_by])
      else
        logs = nil
      end
    else
      logs = UserSigninLog.order_by_created_at(params[:order_by])
    end
    if params[:start_at].present? && params[:end_at].present?
      logs = logs.between_date(params[:start_at].to_date, params[:end_at].to_date)
    end
    logs = logs.where(app_version: params[:app_version]) if params[:app_version].present? && logs.present?
    logs = logs.where(app_channel: params[:app_channel]) if params[:app_channel].present? && logs.present?
    if !logs.nil?
      @logs = logs.includes(:user).page(param_page).per(param_limit)
    else
      @logs = nil
    end
  end

  swagger_api :region_stat do |api|
    summary "用户行为 - 登录IP - 地域分析"
    notes ""
    Api::Manage::V1::ApiController.common_params(api)
    Api::Manage::V1::ApiController.page_params(api)
    param :query, :start_at, :string, :optional, "日期筛选开始时间, 如: 2017-03-01，默认为本月1号"
    param :query, :end_at, :string, :optional, "日期筛选结束时间, 如: 2017-03-30, 默认为本月当日"
    param :query, :stat_type, :string, :optional, "统计类别, country:国家 / province:州省 / city:市，默认为: 国家"
  end
  def region_stat
    can_access?(:user_behaviour)

    @stat_type = params[:stat_type] || 'country'
    # 暂时停用统计
    @total_users_count = 0
    @logs = []
    return

    start_at = params[:start_at].present? ? params[:start_at].to_date : Time.now.to_date.beginning_of_month
    end_at = params[:end_at].present? ? params[:end_at].to_date : Time.now.to_date

    if @stat_type == 'country'
      logs = UserSigninLog.by_country
    elsif @stat_type == 'province'
      logs = UserSigninLog.by_province
    else
      logs = UserSigninLog.by_city
    end
    logs = logs.between_date(start_at, end_at)
    @total_users_count = logs.map(&:users_count).sum
    @logs = logs.order("users_count DESC, times_count DESC").page(param_page).per(param_limit)
  end

  swagger_api :period_stat_chart do |api|
    summary "用户行为 - 登录IP - 时间段分析 - 图表"
    notes ""
    Api::Manage::V1::ApiController.common_params(api)
    param :query, :start_at, :string, :optional, "日期筛选开始时间, 如: 2017-03-01，默认为本月1号"
    param :query, :end_at, :string, :optional, "日期筛选结束时间, 如: 2017-03-30, 默认为本月当日"
  end
  def period_stat_chart
    can_access?(:user_behaviour)

    start_at = params[:start_at].present? ? params[:start_at].to_date : Time.now.to_date.beginning_of_month
    end_at = params[:end_at].present? ? params[:end_at].to_date : Time.now.to_date

    logs = TrendStatService.stat_user_signin_hourly(start_at.strftime("%Y-%m-%d"), end_at.strftime("%Y-%m-%d"))
    log_hours = logs.map{|l| l[:period]}
    StatService.missing_hours(log_hours) do |h|
      logs << { period: h, users_count: 0, times_count: 0, average_times: 0}
    end
    @logs = logs.sort_by {|item| item[:period] }
  end

  swagger_api :period_stat do |api|
    summary "用户行为 - 登录IP - 时间段分析"
    notes ""
    Api::Manage::V1::ApiController.common_params(api)
    Api::Manage::V1::ApiController.page_params(api)
    param :query, :start_at, :string, :optional, "日期筛选开始时间, 如: 2017-03-01，默认为本月1号"
    param :query, :end_at, :string, :optional, "日期筛选结束时间, 如: 2017-03-30, 默认为本月当日"
  end
  def period_stat
    can_access?(:user_behaviour)

    start_at = params[:start_at].present? ? params[:start_at].to_date : Time.now.to_date.beginning_of_month
    end_at = params[:end_at].present? ? params[:end_at].to_date : Time.now.to_date

    @logs = UserSigninPeriodDayStat.between_date(start_at, end_at).order(stat_date: :desc).page(param_page).per(param_limit)
  end

end


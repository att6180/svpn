class Api::Manage::V1::NavigationAdsController < Api::Manage::V1::ApiController

  before_action :set_item, only: [:show, :update, :destroy]

  swagger_controller :navigation_ads, "导航广告设置"

  swagger_api :index do |api|
    summary "导航帮助 - 导航设置 - 导航广告配置"
    notes ""
    Api::Manage::V1::ApiController.common_params(api)
    Api::Manage::V1::ApiController.page_params(api)
  end
  def index
    can_access?(:user_behaviour)

    @items = NavigationAd.enabled.sorted.page(param_page).per(param_limit)
  end

  swagger_api :create do |api|
    summary "导航帮助 - 导航设置 - 导航广告配置- 创建"
    notes ""
    Api::Manage::V1::ApiController.common_params(api)
    param :form, :priority, :string, :required, "广告编号"
    param :form, :name, :string, :required, "广告名称"
    param :form, :image_url, :string, :required, "广告图片地址"
    param :form, :link_url, :string, :optional, "广告URL"
    param :form, :is_enabled, :string, :required, "是否启用, true:启用, false:禁用"
  end
  def create
    requires! :priority, type: String
    requires! :name, type: String
    requires! :image_url, type: String
    requires! :is_enabled, type: String

    @item = NavigationAd.new(allowed_params)
    error_detail!(@item) if !@item.save
  end

  swagger_api :show do |api|
    summary "导航帮助 - 导航设置 - 导航广告配置 - 单个获取"
    notes ""
    Api::Manage::V1::ApiController.common_params(api)
    param :path, :id, :integer, :required, "ID"
  end
  def show
    requires! :id, type: Integer
  end

  swagger_api :update do |api|
    summary "导航帮助 - 导航设置 - 导航广告配置 - 更新"
    notes ""
    Api::Manage::V1::ApiController.common_params(api)
    param :path, :id, :integer, :required, "ID"
    param :form, :priority, :string, :optional, "广告编号"
    param :form, :name, :string, :optional, "广告名称"
    param :form, :image_url, :string, :optional, "广告图片地址"
    param :form, :link_url, :string, :optional, "广告URL"
    param :form, :is_enabled, :string, :optional, "是否启用, true:启用, false:禁用"
  end
  def update
    error_detail!(@item) if !@item.update(allowed_params)
  end

  private

  def allowed_params
    params.permit(
      :priority,
      :name,
      :image_url,
      :link_url,
      :is_enabled
    )
  end

  def set_item
    @item = NavigationAd.find(params[:id])
  end
end

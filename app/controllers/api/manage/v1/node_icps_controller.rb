class Api::Manage::V1::NodeIcpsController < Api::Manage::V1::ApiController

  include PermissionFilterable
  set_module_group_name :node_manage

  before_action :set_node_icp, only: [:update, :destroy]

  swagger_controller :node_icps, "运营商分流管理"

  swagger_api :index do |api|
    summary "运营商分流管理 - 列表"
    notes ""
    Api::Manage::V1::ApiController.common_params(api)
    param :query, :is_enabled, :string, :optional, "true: 筛选已启用的类型, false|忽略参数: 筛选全部"
    param :query, :page, :integer, :optional, "页码，默认: 1"
    param :query, :limit, :integer, :optional, "每页显示的记录数, 默认: 25"
  end
  def index

    node_icps = NodeIcp.sorted_by_enabled
    node_icps = node_icps.where(is_enabled: cast(params[:is_enabled])) if params[:is_enabled].present?
    @node_icps = node_icps.page(param_page).per(param_limit)
  end

  swagger_api :create do |api|
    summary "运营商分流管理 - 创建"
    notes ""
    Api::Manage::V1::ApiController.common_params(api)
    param :form, :tag, :string, :required, "运营商类型"
    param :form, :is_enabled, :string, :optional, "是否启用此类型, true|false"
    param :form, :isps, :string, :required, "代理商标识, 填入`代理商标识页`中配置的代理商ID，超过一个代理商标识，用','分隔开"
  end
  def create
    requires! :tag, type: String
    requires! :isps, type: String

    @node_icp = NodeIcp.new(allowed_params)
    error_detail!(@node_icp) if !@node_icp.save
  end

  swagger_api :update do |api|
    summary "运营商分流管理 - 更新"
    notes ""
    Api::Manage::V1::ApiController.common_params(api)
    param :path, :id, :integer, :required, "运营商ID"
    param :form, :tag, :string, :required, "运营商类型"
    param :form, :is_enabled, :string, :optional, "是否启用此类型, true|false"
    param :form, :isps, :string, :required, "代理商标识, 填入`代理商标识页`中配置的代理商ID，超过一个代理商标识，用','分隔开"
  end
  def update
    requires! :id, type: Integer

    error_detail!(@node_icp) if !@node_icp.update(allowed_params)
  end

  private
    def set_node_icp
      @node_icp = NodeIcp.find(params[:id])
    end

    def allowed_params
      params.permit(
        :tag,
        :is_enabled,
        :isps
      )
    end
end

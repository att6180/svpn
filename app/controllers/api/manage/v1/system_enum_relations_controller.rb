class Api::Manage::V1::SystemEnumRelationsController < Api::Manage::V1::ApiController
  before_action :set_item, only: [:show, :update, :destroy]

  swagger_controller :system_enum_relations, "渠道版本配置 - 关系配置"

  swagger_api :index do |api|
    summary "渠道版本配置 - 关系配置 - 列表"
    notes ""
    Api::Manage::V1::ApiController.common_params(api)
    Api::Manage::V1::ApiController.page_params(api)
    param :query, :platform_id, :integer, :optional, "平台, 下拉列表，用于筛选"
    param :query, :channel_account_id, :integer, :optional, "渠道账号, 下拉列表，用于筛选"
    param :query, :package_id, :integer, :optional, "包名, 下拉列表，用于筛选"
    param :query, :channel, :string, :optional, "渠道, 下拉列表，用于筛选"
    param :query, :version, :string, :optional, "版本, 下拉列表，用于筛选"
  end
  def index
    can_access?(:system_enum)
    @items = SystemEnumRelation
      .includes(:platform, :channel_account, :package)
      .the_where({
        channel: params[:channel],
        version: params[:version],
        platform_id: params[:platform_id],
        channel_account_id: params[:channel_account_id],
        package_id: params[:package_id]
      })
      .sorted
      .page(param_page)
      .per(param_limit)
  end

  swagger_api :create do |api|
    summary "渠道版本配置 - 关系配置 - 创建"
    notes <<-EOF
      平台后台返回固定列表
      渠道账号跟包名可以随时更改名称所以只传id
    EOF
    Api::Manage::V1::ApiController.common_params(api)
    param :form, :platform_id, :integer, :required, "平台, 下拉列表选择"
    param :form, :channel_account_id, :integer, :required, "渠道账号, 下拉列表选择"
    param :form, :package_id, :integer, :required, "包名, 下拉列表选择"
    param :form, :channel, :string, :required, "渠道, 下拉列表选择"
    param :form, :version, :string, :required, "版本，下拉列表选择"
    param :form, :description, :string, :optional, "描述"
  end
  def create
    requires! :platform_id, type: Integer
    requires! :channel_account_id, type: Integer
    requires! :package_id, type: Integer
    requires! :channel, type: String
    requires! :version, type: String
    can_access?(:system_enum)
    @item = SystemEnumRelation.new(allowed_params)
    error_detail!(@item) if !@item.save
  end

  swagger_api :show do |api|
    summary "渠道版本配置 - 关系配置 - 单个获取"
    notes ""
    Api::Manage::V1::ApiController.common_params(api)
    param :path, :id, :integer, :required, "ID"
  end
  def show
    requires! :id, type: Integer
    can_access?(:system_enum)
  end

  swagger_api :update do |api|
    summary "渠道版本配置 - 关系配置 - 更新"
    notes ""
    Api::Manage::V1::ApiController.common_params(api)
    param :path, :id, :integer, :required, "ID"
    param :form, :platform_id, :integer, :optional, "平台, 下拉列表选择"
    param :form, :channel_account_id, :integer, :optional, "渠道账号, 下拉列表选择"
    param :form, :package_id, :integer, :optional, "包名, 下拉列表选择"
    param :form, :channel, :string, :optional, "渠道, 下拉列表选择"
    param :form, :version, :string, :optional, "版本，下拉列表选择"
    param :form, :description, :string, :optional, "描述"
  end
  def update
    requires! :id, type: Integer
    can_access?(:system_enum)

    error_detail!(@item) if !@item.update(allowed_params)
  end

  swagger_api :destroy do |api|
    summary "渠道版本配置 - 关系配置 - 删除"
    notes ""
    Api::Manage::V1::ApiController.common_params(api)
    param :path, :id, :integer, :required, "ID"
  end
  def destroy
    requires! :id, type: Integer
    can_access?(:system_enum)

    error_detail!(@item) if !@item.destroy
  end

  private

  def allowed_params
    params.permit(
      :platform_id,
      :channel_account_id,
      :package_id,
      :channel,
      :version,
      :description
    )
  end

  def set_item
    @item = SystemEnumRelation.find(params[:id])
  end
end

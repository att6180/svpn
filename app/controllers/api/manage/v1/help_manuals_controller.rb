class Api::Manage::V1::HelpManualsController < Api::Manage::V1::ApiController

  # 设置权限
  include PermissionFilterable
  set_module_group_name :help_manual

  before_action :set_item, only: [:update, :destroy]

  swagger_controller :help_manuals, "帮助手册"

  swagger_api :index do |api|
    summary "帮助手册 - 列表"
    notes ""
    Api::Manage::V1::ApiController.common_params(api)
    Api::Manage::V1::ApiController.page_params(api)
    param :query, :platform, :string, :optional, "平台，ios/android/pc/mac等，为空则获取全部"
    param :query, :app_version, :string, :optional, "版本, jichu等，为空则获取全部"
    param :query, :app_version_number, :string, :optional, "版本号, 1.0.0等, 为空则获取全部"
  end
  def index
    optional! :platform, type: String
    optional! :app_version, type: String
    optional! :app_version_number, type: String
    optional! :page, type: Integer
    optional! :limit, type: Integer

    items = HelpManual.recent
    items = items.where(platform: params[:platform]) if params[:platform].present?
    items = items.where(app_version: params[:app_version]) if params[:app_version].present?
    items = items.where(app_version_number: params[:app_version_number]) if params[:app_version_number].present?
    @items = items.page(param_page).per(param_limit)
  end

  swagger_api :create do |api|
    summary "帮助手册 - 创建"
    notes ""
    Api::Manage::V1::ApiController.common_params(api)
    param :form, :platform, :string, :optional, "平台，ios/android等，为空则表示全部渠道都可见"
    param :form, :app_version, :string, :optional, "版本, jichu等, 为空则表示全部版本都可见"
    param :form, :app_version_number, :string, :optional, "版本号, 1.0.0等，为空则表示全部版本号都可见"
    param :form, :title, :string, :required, "标题"
    param :form, :content, :string, :required, "内容"
  end
  def create
    optional! :platform, type: String
    optional! :app_version, type: String
    optional! :app_version_number, type: String
    requires! :title, type: String
    requires! :content, type: String

    @item = HelpManual.new(allowed_params)
    error_detail!(@item) if !@item.save
  end

  swagger_api :update do |api|
    summary "帮助手册 - 更新"
    notes ""
    Api::Manage::V1::ApiController.common_params(api)
    param :path, :id, :integer, :required, "节点ID"
    param :form, :platform, :string, :optional, "平台，为空表示全部"
    param :form, :app_version, :string, :optional, "版本,为空表示全部"
    param :form, :app_version_number, :string, :optional, "版本号,为空表示全部"
    param :form, :title, :string, :optional, "标题"
    param :form, :content, :string, :optional, "内容"
  end
  def update
    requires! :id, type: Integer
    optional! :platform, type: String
    optional! :app_version, type: String
    optional! :app_version_number, type: String
    optional! :title, type: String
    optional! :content, type: String

    error_detail!(@item) if !@item.update(allowed_params)
  end

  swagger_api :destroy do |api|
    summary "帮助手册 - 删除"
    notes ""
    Api::Manage::V1::ApiController.common_params(api)
    param :path, :id, :integer, :required, "节点ID"
  end
  def destroy
    requires! :id, type: Integer

    @item.destroy if @item.present?
  end

  private

  def allowed_params
    params.permit(
      :platform,
      :app_version,
      :app_version_number,
      :title,
      :content
    )
  end

  def set_item
    @item = HelpManual.find(params[:id])
  end
end

class Api::Manage::V1::PlansController < Api::Manage::V1::ApiController

  # 设置权限
  include PermissionFilterable
  set_module_group_name :charge_manage

  before_action :set_node, only: [:show, :update, :destroy]

  swagger_controller :plans, "套餐管理"

  swagger_api :index do |api|
    summary "套餐管理 - 列表"
    notes ""
    Api::Manage::V1::ApiController.common_params(api)
    param :query, :is_regular_time, :string, :optional, "是否是包时间套餐, true|false，不指定则为全部"
    param :query, :time_type, :string, :optional, "包时间类型, monthly:包月/quarterly:包季/half_yearly:包半年/yearly:包一年，写死，不指定则为全部"
    param :query, :node_type_id, :integer, :optional, "服务类型ID"
    param :query, :is_iap, :string, :optional, "是否是IAP套餐, true|false，不指定则为全部"
    param :query, :platform, :string, :optional, "平台, ios/android/pc/mac，不指定则为全部"
    param :query, :app_version, :string, :optional, "版本, jichu等，不指定则为全部"
    param :query, :app_version_number, :string, :optional, "版本号, 如: 1.0.1，不指定则为全部"
    param :query, :is_audit, :string, :optional, "是否是审核套餐, true|false, 用于上架应用商店被审核时使用"
    param :query, :is_enabled, :string, :optional, "是否启用, true|false"
    Api::Manage::V1::ApiController.page_params(api)
  end
  def index
    optional! :is_regular_time, type: String
    optional! :time_type, type: String
    optional! :node_type_id, type: Integer
    optional! :is_iap, type: String
    optional! :platform, type: String
    optional! :app_version, type: String
    optional! :app_version_number, type: String
    optional! :is_audit, type: String
    optional! :page, type: Integer
    optional! :limit, type: Integer

    plans = Plan.sorted
    plans = plans.where(is_regular_time: params[:is_regular_time] == 'true' ? true : false) if params[:is_regular_time].present?
    plans = plans.where(time_type: params[:time_type]) if params[:time_type].present?
    plans = plans.where(node_type: params[:node_type_id]) if params[:node_type_id].present?
    plans = plans.where(is_iap: params[:is_iap] == 'true' ? true : false) if params[:is_iap].present?
    plans = plans.where(platform: params[:platform]) if params[:platform].present?
    plans = plans.where(app_version: params[:app_version]) if params[:app_version].present?
    plans = plans.where(app_version_number: params[:app_version_number]) if params[:app_version_number].present?
    plans = plans.where(is_audit: params[:is_audit] == 'true' ? true : false) if params[:is_audit].present?
    plans = plans.where(is_enabled: params[:is_enabled] == 'true' ? true : false) if params[:is_enabled].present?
    @plans = plans.includes(:node_type).page(param_page).per(param_limit)
  end

  swagger_api :create do |api|
    summary "套餐管理 - 创建"
    notes ""
    Api::Manage::V1::ApiController.common_params(api)
    param :form, :platform, :string, :optional, "平台, ios/android/pc/mac，不指定则为全部"
    param :form, :app_version, :string, :optional, "版本, jichu等，不指定则为全部"
    param :form, :app_version_number, :string, :optional, "版本号, 如: 1.0.1，不指定则为全部"
    param :form, :name, :string, :required, "套餐名称"
    param :form, :is_regular_time, :string, :required, "是否是包时间套餐，true|false，默认false"
    param :form, :time_type, :string, :optional, "包时间类型, monthly:包月/quarterly:包季/half_yearly:包半年/yearly:包一年，写死，默认为空"
    param :form, :node_type_id, :integer, :optional, "服务类型ID(指定要加时间的服务类型)"
    param :form, :coins, :integer, :optional, "包含钻石数"
    param :form, :price, :string, :required, "价格，如: 1.00"
    param :form, :present_coins, :integer, :optional, "赠送钻石"
    param :form, :is_enabled, :string, :optional, "是否启用, 默认 true"
    param :form, :is_audit, :string, :optional, "是否是审核套餐, 默认 false"
    param :form, :is_iap, :string, :optional, "是否是IAP(苹果应用内付费)套餐"
    param :form, :iap_id, :string, :optional, "IAP(苹果应用内付费)套餐ID, is_iap为true是必填"
    param :form, :description, :string, :optional, "套餐描述"
  end
  def create
    optional! :platform, type: String
    optional! :app_version, type: String
    optional! :app_version_number, type: String
    requires! :name, type: String
    requires! :is_regular_time, type: String
    optional! :time_type, type: String
    optional! :node_type_id, type: Integer
    optional! :coins, type: Integer
    requires! :price, type: String
    optional! :present_coins, type: Integer
    optional! :is_enabled, type: String
    optional! :is_audit, type: String
    optional! :is_iap, type: String
    optional! :iap_id, type: String
    optional! :description, type: String

    @plan = Plan.new(allowed_params)
    error_detail!(@plan) if !@plan.save
  end

  swagger_api :show do |api|
    summary "套餐管理 - 单个获取"
    notes ""
    Api::Manage::V1::ApiController.common_params(api)
    param :path, :id, :integer, :required, "套餐ID"
  end
  def show
    requires! :id, type: Integer
  end

  swagger_api :update do |api|
    summary "套餐管理 - 更新"
    notes ""
    Api::Manage::V1::ApiController.common_params(api)
    param :path, :id, :integer, :optional, "套餐ID"
    param :form, :name, :string, :optional, "套餐名称"
    param :form, :is_regular_time, :string, :optional, "是否是包时间套餐，true|false，默认false"
    param :form, :time_type, :string, :optional, "包时间类型, monthly:包月/quarterly:包季/half_yearly:包半年/yearly:包一年，写死，默认为空"
    param :form, :node_type_id, :integer, :optional, "服务类型ID(指定要加时间的服务类型)"
    param :form, :platform, :string, :optional, "平台, ios/android/pc/mac，不指定则为全部"
    param :form, :app_version, :string, :optional, "版本, jichu等，不指定则为全部"
    param :form, :app_version_number, :string, :optional, "版本号, 如: 1.0.1，不指定则为全部"
    param :form, :coins, :integer, :optional, "包含钻石数"
    param :form, :price, :string, :optional, "价格，如: 1.00"
    param :form, :present_coins, :integer, :optional, "赠送钻石"
    param :form, :is_enabled, :string, :optional, "是否启用, 默认 true"
    param :form, :is_audit, :string, :optional, "是否是审核套餐"
    param :form, :is_iap, :string, :optional, "是否是IAP(苹果应用内付费)套餐"
    param :form, :iap_id, :string, :optional, "IAP(苹果应用内付费)套餐ID, is_iap为true是必填"
    param :form, :description, :string, :optional, "套餐描述"
  end
  def update
    optional! :platform, type: String
    optional! :app_version, type: String
    optional! :app_version_number, type: String
    optional! :name, type: String
    optional! :is_regular_time, type: String
    optional! :time_type, type: String
    optional! :node_type_id, type: Integer
    optional! :coins, type: Integer
    optional! :price, type: String
    optional! :present_coins, type: Integer
    optional! :is_enabled, type: String
    optional! :is_audit, type: String
    optional! :is_iap, type: String
    optional! :iap_id, type: String
    optional! :description, type: String

    error_detail!(@plan) if !@plan.update(allowed_params)
  end

  private

  def allowed_params
    params.permit(
      :platform,
      :app_version,
      :app_version_number,
      :name,
      :is_regular_time,
      :time_type,
      :node_type_id,
      :coins,
      :price,
      :present_coins,
      :is_enabled,
      :is_audit,
      :is_iap,
      :iap_id,
      :description
    )
  end

  def set_node
    @plan = Plan.find(params[:id])
  end
end

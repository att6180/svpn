class Api::Manage::V1::WebsiteNavigationClickStatsController < Api::Manage::V1::ApiController

  include PermissionFilterable
  set_module_group_name :operation_behaviour

  swagger_controller :website_navigation_click_stats, "广告统计 - 导航统计"

  swagger_api :hour_index do |api|
    summary "广告统计 - 导航统计 - 小时统计"
    notes ""
    Api::Manage::V1::ApiController.common_params(api)
    param :query, :date, :string, :required, "统计日期, 如: 2017-01-01"
    param :query, :is_domestic, :string, :optional, "国内外标志性，1:国内/0:国外, 全部传空"
    param :query, :info_type, :integer, :required, "信息分类, 1:导航类/2:广告类"
  end
  def hour_index
    @date = params[:date]
    @prev_date = @date.to_date.prev_day
    logs = Log::WebsiteNavigationClickLog
      .the_where(
        created_date: [@date, @prev_date],
        is_domestic: params[:is_domestic],
        info_type: params[:info_type]
      )
      .group_by_date_and_hour
      .select("COUNT(id) as clicked_count")
    # 选按日期分组
    today_log_indexs = logs.select{|l| l.d == @date.to_date}
    prev_log_indexs = logs.select{|l| l.d == @prev_date.to_date}

    today_log_hour_indexs = today_log_indexs.map(&:serializable_hash).index_by{|l| l["h"]}
    @logs = []
    (0..23).each_with_index do |i|
      @logs.push(today_log_hour_indexs.has_key?(i) ? today_log_hour_indexs[i].symbolize_keys! : {d: @date,  h: i, clicked_count: 0 })
    end

    prev_log_hour_indexs = prev_log_indexs.map(&:serializable_hash).index_by{|l| l["h"]}
    @prev_logs = []
    (0..23).each_with_index do |i|
      @prev_logs.push(prev_log_hour_indexs.has_key?(i) ? prev_log_hour_indexs[i].symbolize_keys! : {d: @prev_date, h: i, clicked_count: 0 })
    end
  end

  swagger_api :day_index do |api|
    summary "广告统计 - 导航统计 - 日/月统计"
    notes ""
    Api::Manage::V1::ApiController.common_params(api)
    param :query, :start_date, :string, :required, "起始日期, 如: 2017-01-01，默认为当月1日"
    param :query, :end_date, :string, :required, "结束日期, 如: 2017-01-03，默认为当前日期"
    param :query, :is_domestic, :string, :required, "国内外标志性，1:国内/0:国外, 全部传空"
    param :query, :info_type, :integer, :required, "信息分类, 1:导航类/2:广告类"
  end
  def day_index
    start_date = params[:start_date]&.to_date || Time.now.beginning_of_month.to_date
    end_date = params[:end_date]&.to_date || Time.now.end_of_month.to_date

    @logs = Log::WebsiteNavigationClickDayStat
      .between_date(start_date, end_date)
      .the_where(
        is_domestic: params[:is_domestic],
        info_type: params[:info_type]
      )
      .group_by_website_name
  end

end

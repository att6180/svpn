class Api::Manage::V1::UserOperationStatsController < Api::Manage::V1::ApiController

  # 设置权限
  include PermissionFilterable
  set_module_group_name :user_base_info

  swagger_controller :user_operation_stats, "页面点击"

  swagger_api :index do |api|
    summary "用户行为 - 页面点击 - 列表"
    notes "默认显示本月1号到目前日期的统计数据, logs为图表数据, interfaces为表格数据"
    Api::Manage::V1::ApiController.common_params(api)
    param :query, :app_version, :string, :optional, "版本筛选"
    param :query, :app_channel, :string, :optional, "渠道筛选"
    param :query, :start_at, :string, :optional, "开始日期, 如: 2017-01-04"
    param :query, :end_at, :string, :optional, "结束日期, 如: 2017-03-05"
  end
  def index
    today = Time.now.to_date
    start_at = params[:start_at] || today.beginning_of_month
    end_at = params[:end_at] || today.yesterday

    logs = TrendStatService.stat_page_click(
      start_at,
      end_at,
      params[:app_channel],
      params[:app_version]
    )

    segment = logs[:segment]
    @interfaces = logs[:interfaces]
    @date_list = segment.pluck(:timestamp).uniq
    @ui_list = {}
    segment.group_by{|s| s[:interface_name]}.each do |key, value|
      values = value.pluck(:visits_count)
      date_size = @date_list.size() -1
      @ui_list[key] = values.fill(0, value.size..date_size) # 缺失日期填充0
    end
  end

  swagger_api :visit_details do |api|
    summary "用户行为 - 页面点击 - 点击次数详情"
    notes ""
    Api::Manage::V1::ApiController.common_params(api)
    Api::Manage::V1::ApiController.page_params(api)
    param :query, :start_at, :string, :required, "开始日期, 如: 2017-01-04"
    param :query, :end_at, :string, :required, "结束日期, 如: 2017-03-05"
    param :query, :interface_id, :integer, :required, "界面id"
    param :query, :app_version, :string, :optional, "版本筛选"
    param :query, :app_channel, :string, :optional, "渠道筛选"
  end
  def visit_details
    requires! :start_at, type: String
    requires! :end_at, type: String
    requires! :interface_id, type: Integer
    optional! :page, type: Integer
    optional! :limit, type: Integer
    optional! :app_version, type: String
    optional! :app_channel, type: String

    logs = Mongo::UserOperationLog
      .where(interface_id: params[:interface_id])
      .where(:created_at.gte => params[:start_at].to_datetime, :created_at.lte => params[:end_at].to_datetime)
      .page(param_page).per(param_limit)

    logs = logs.where(app_version: params[:app_version]) if params[:app_version].present?
    logs = logs.where(app_channel: params[:app_channel]) if params[:app_channel].present?

    user_ids = logs.map(&:user_id)
    users = User.find(user_ids)
    @users = users.index_by(&:id)
    @logs = logs
  end

  swagger_api :user_details do |api|
    summary "用户行为 - 页面点击 - 用户详情"
    notes ""
    Api::Manage::V1::ApiController.common_params(api)
    Api::Manage::V1::ApiController.page_params(api)
    param :query, :start_at, :string, :required, "开始日期, 如: 2017-01-04"
    param :query, :end_at, :string, :required, "结束日期, 如: 2017-03-05"
    param :query, :interface_id, :integer, :required, "界面id"
    param :query, :app_version, :string, :optional, "版本筛选"
    param :query, :app_channel, :string, :optional, "渠道筛选"
  end
  def user_details
    requires! :start_at, type: String
    requires! :end_at, type: String
    requires! :interface_id, type: Integer
    optional! :page, type: Integer
    optional! :limit, type: Integer
    optional! :app_version, type: String
    optional! :app_channel, type: String

    logs = UserOperationLog.select(:user_id).where(interface_id: params[:interface_id]).page(param_page).per(param_limit)
    logs = logs.where(app_version: params[:app_version]) if params[:app_version].present?
    logs = logs.where(app_channel: params[:app_channel]) if params[:app_channel].present?
    end_at = params[:end_at].to_date >= Time.now.to_date ? Time.now.to_date.yesterday : params[:end_at].to_date
    @logs = logs.between_date(params[:start_at].to_date, end_at).group(:user_id).includes(:user)
  end

end

module PermissionFilterable
  extend ActiveSupport::Concern

  included do
    before_action :module_group_permission_filter!
  end

  class_methods do
    attr_reader :module_group_name

    def set_module_group_name(name)
      @module_group_name = name
    end
  end

end

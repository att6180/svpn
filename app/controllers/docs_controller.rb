class DocsController < ApplicationController

  before_action :basic_authenticate, except: [:ip]
  before_action :set_cache_headers

  layout false

  def index
  end

  def client_v2
  end

  def testrobot
  end

  def ops
  end

  def manage
    redirect_to "/api_docs_html/index.html?url=/manage_api_docs/api-docs.json"
  end

  def tables
    ignore_tables = ['ar_internal_metadata', 'schema_migrations']
    svpn_tables = (User.connection.data_sources - ignore_tables).sort{ |x,y| y <=> x}
    log_tables = UserSigninLog.connection.data_sources - ignore_tables.sort{ |x,y| y <=> x}

    regex = "svpn:#{svpn_tables.count} \n\n(#{svpn_tables.join('|')})\n\n\n\nsvpn_log:#{log_tables.count} \n\n(#{log_tables.join('|')})"

    render plain: regex, layout: false
  end

  def ip
    client_ip = request.headers["HTTP_X_REAL_IP"]&.split(',')&.first
    if client_ip.blank? || Utils::IP.is_private_ip?(client_ip)
      client_ip = request.headers["HTTP_CDN_SRC_IP"]
    end
    client_ip = request.remote_ip if client_ip.blank?

    ip_region = Ipnet.find_by_ip(client_ip)
    ip_country = ip_region[:country]
    ip_province = ip_region[:province]
    ip_city = ip_region[:city]
    ip_area = ip_region[:area]

    regex = "ip:#{client_ip} \n\ncountry:#{ip_country} \n\nprovince:#{ip_province} \n\ncity:#{ip_city} \n\narea:#{ip_area}"
    render plain: regex, layout: false
  end

  private

  def basic_authenticate
    authenticate_or_request_with_http_basic do |user_name, password|
      user_name == CONFIG.docs_auth_username && password == CONFIG.docs_auth_password
    end
  end

  def set_cache_headers
    response.headers["Cache-Control"] = "no-cache, no-store"
    response.headers["Pragma"] = "no-cache"
    response.headers["Expires"] = "Fri, 01 Jan 1990 00:00:00 GMT"
  end

end

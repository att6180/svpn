module ApplicationHelper

  def set_title(title)
    content_for(:title) { title }
  end

  def format_api_time(time)
    time.to_i
  end

  def format_bool(value)
    value ? 1 : 0
  end

  def flash_class(level)
    case level.to_sym
    when :notice then 'info'
    when :success then 'success'
    when :error then 'danger'
    when :alert then 'warning'
    end
  end

  # 计算百分比
  def calc_percent(quantity, total)
    return nil if quantity.nil? || total.nil?
    return 0 if quantity == 0
    percent = "%.2f" % ((quantity.to_f / total.to_f) * 100)
    percent.to_f
  end

  # 计算人均
  def calc_per_average(times_count, users_count)
    return nil if times_count.nil? || users_count.nil?
    return 0 if times_count == 0 || times_count < users_count
    per_average = "%.1f" % (times_count.to_f / users_count.to_f)
    per_average.to_f
  end

  # 判断是否是手机号
  def is_telephone?(s)
     s.present? && /^\d{11}$/ === s && s.first == "1"
  end

  # 公告信息替换
  def render_notice(settings, keywords)
    # 查找公告,更新公告并将指定字段替换为指定字符
    settings = settings.select { |s| s.key == "NOTICE_CONTENT" || s.key == "UPDATE_CONTENT" }
    return if settings.blank?
    keywords.each do |k, v|
      settings.map{ |s| s.value.gsub!("**#{k.to_s}**", v) }
    end
  end

end

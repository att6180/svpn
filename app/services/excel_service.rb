module ExcelService

  class Download

    PLAN_CLIENT_TAB_FILE_PATH = "#{Rails.root}/public/excels/v2_plan_client_tabs.xls"
    PLAN_TEMPLATE_FILE_PATH = "#{Rails.root}/public/excels/v2_plan_templates.xls"
    PLAN_FILE_PATH = "#{Rails.root}/public/excels/v2_plans.xls"
    PLAN_VERSION_FILE_PATH = "#{Rails.root}/public/excels/v2_plan_versions.xls"

    class << self

      # 套餐客户端标签
      def plan_client_tab_excel
        open_excel(PLAN_CLIENT_TAB_FILE_PATH) do |sheet|
          tabs = V2::PlanClientTab.all
          tabs.each_with_index do |t, index|
            sheet.row(index + 1).replace([
              t.id,
              t.node_type_id,
              t.name,
              t.description,
              t.is_recommend,
              t.status
            ])
          end
        end
      end

      # 套餐模版页
      def plan_template_excel
        open_excel(PLAN_TEMPLATE_FILE_PATH) do |sheet|
          templates = V2::PlanTemplate.all
          templates.each_with_index do |t, index|
            sheet.row(index + 1).replace([
              t.id,
              t.name,
              t.plan_type,
              t.status
            ])
          end
        end
      end

      # 套餐模版详情
      def plan_excel
        open_excel(PLAN_FILE_PATH) do |sheet|
          plans = V2::Plan.all
          plans.each_with_index do |p, index|
            sheet.row(index + 1).replace([
              p.id,
              p.template_id,
              p.client_tab_id,
              p.name,
              p.price,
              p.currency_symbol,
              p.is_iap,
              p.iap_id,
              p.is_regular_time,
              p.time_type,
              p.node_type_id,
              p.coins,
              p.present_coins,
              p.sort_weight,
              p.description1,
              p.description2,
              p.description3,
              p.is_recommend,
              p.status
            ])
          end
        end
      end

      # 版本配置页
      def plan_version_excel
        open_excel(PLAN_VERSION_FILE_PATH) do |sheet|
          versions = V2::PlanVersion.all
          versions.each_with_index do |v, index|
            sheet.row(index + 1).replace([
              v.id,
              v.channel,
              v.version,
              v.audit_template_id,
              v.domestic_template_id,
              v.oversea_template_id,
              v.iap_template_id,
              v.show_icon,
              v.tab_id_sort,
              v.status
            ])
          end
        end
      end

      private

      def open_excel(filename)
        book = Spreadsheet.open(filename, 'rb')
        sheet = book.worksheet(0)
        yield(sheet) if block_given?
        spreadsheet = StringIO.new
        book.write spreadsheet
        spreadsheet.string
      end
    end
  end

  class Upload

    class << self

      # 解析套餐客户端标签excel
      # 如果excel文件第一列数据库id为空
      #   新增
      # 否则
      #   更新
      def parse_plan_client_tab(filepath)
        attrs = V2::PlanClientTab.column_names
        parse_excel(filepath) do |sheet|
          sheet.each do |row|
            h = {}
            attrs.zip(row) { |a,b| h[a.to_sym] = b }
            h = h.except(:created_at, :updated_at) # 忽略时间字段
            tab = V2::PlanClientTab.find_or_initialize_by(id: h[:id])
            attr_type = V2::PlanClientTab.attribute_names.map {|n| [n.to_sym,V2::PlanClientTab.type_for_attribute(n).type]}.to_h
            h.each {|k, v| h[k] = v.to_s if attr_type[k] == :string && v.nil? }
            tab.attributes = h
            tab.save!
          end
        end
      end

      # 解析套餐模版页excel
      # 如果excel文件第一列数据库id为空
      #   新增
      # 否则
      #   更新
      def parse_plan_template(filepath)
        attrs = V2::PlanTemplate.column_names
        parse_excel(filepath) do |sheet|
          sheet.each do |row|
            h = {}
            attrs.zip(row) { |a,b| h[a.to_sym] = b }
            h = h.except(:created_at, :updated_at) # 忽略时间字段
            tab = V2::PlanTemplate.find_or_initialize_by(id: h[:id])
            attr_type = V2::PlanTemplate.attribute_names.map {|n| [n.to_sym,V2::PlanTemplate.type_for_attribute(n).type]}.to_h
            h.each {|k, v| h[k] = v.to_s if attr_type[k] == :string && v.nil? }
            tab.attributes = h
            tab.save!
          end
        end
      end

      # 解析套餐模版详情excel
      # 如果excel文件第一列数据库id为空
      #   新增
      # 否则
      #   更新
      def parse_plan(filepath)
        attrs = V2::Plan.column_names
        parse_excel(filepath) do |sheet|
          sheet.each do |row|
            h = {}
            attrs.zip(row) { |a,b| h[a.to_sym] = b }
            h = h.except(:created_at, :updated_at) # 忽略时间字段
            tab = V2::Plan.find_or_initialize_by(id: h[:id])
            attr_type = V2::Plan.attribute_names.map {|n| [n.to_sym,V2::Plan.type_for_attribute(n).type]}.to_h
            h.each {|k, v| h[k] = v.to_s if attr_type[k] == :string && v.nil? }
            tab.attributes = h
            tab.save!
          end
        end
      end

      # 解析版本配置页excel
      # 如果excel文件第一列数据库id为空
      #   新增
      # 否则
      #   更新
      def parse_plan_version(filepath)
        attrs = V2::PlanVersion.column_names
        parse_excel(filepath) do |sheet|
          sheet.each do |row|
            h = {}
            attrs.zip(row) { |a,b| h[a.to_sym] = b }
            h = h.except(:created_at, :updated_at) # 忽略时间字段
            tab = V2::PlanVersion.find_or_initialize_by(id: h[:id])
            attr_type = V2::PlanVersion.attribute_names.map {|n| [n.to_sym,V2::PlanVersion.type_for_attribute(n).type]}.to_h
            h.each {|k, v| h[k] = v.to_s if attr_type[k] == :string && v.nil? }
            tab.attributes = h
            tab.save!
          end
        end
      end

      private

      def parse_excel(filepath)
        book = Spreadsheet.open(filepath, 'rb')
        sheet = book.worksheet(0).rows
        sheet.delete_at(0) # 删除标题
        yield(sheet) if block_given?
      end
    end
  end
end

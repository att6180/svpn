class NodeService

  class << self

    # 连接后的定时任务
    def connected_perform_later(user_id, username, remote_ip, ip_region)
      p = {
        user_id: user_id,
        username: username,
        remote_ip: remote_ip,
        ip_country: ip_region[:country],
        ip_province: ip_region[:province],
        ip_city: ip_region[:city],
        created_at: DateUtils.time2str(Time.now)
      }
      Trend::UserConnectAndSignupJob.perform_later(p, 2) if Setting.trend_submit_enabled == "true"
    end

  end

end

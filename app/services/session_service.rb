class SessionService

  # 创建session记录
  def self.find_or_create_session(user)
    # 如果session id不存在，则创建
    # 如果存在，则重新生成
    id, token = UserSession.read_by_user_id(user.id)
    if id.nil?
      user_session = UserSession.create_by_user(user.id).id
    else
      token = UserSession.regenerate_token!(user.id, id, UserSession::ERR_AFTER_SIGN_IN)
    end
    id
  end

  # 注册后的定时任务
  def self.signup_log_perform_later(user_id, username, remote_ip, ip_region)
    p = {
      user_id: user_id,
      username: username,
      remote_ip: remote_ip,
      ip_country: ip_region[:country],
      ip_province: ip_region[:province],
      ip_city: ip_region[:city],
      created_at: DateUtils.time2str(Time.now)
    }
    if Setting.trend_submit_enabled == "true" && !User.is_robot?(user_id)
      Trend::UserConnectAndSignupJob.perform_later(p, 1)
    end
  end

  def self.signin_log_perform_later(is_new_user, user_id, username, device_id, user_session_id, request_params, remote_ip, ip_region)
    signin_log_params = {
      is_new_user: is_new_user,
      user_id: user_id,
      username: username,
      device_id: device_id,
      user_session_id: user_session_id,
      device_name: request_params[:device_name],
      device_model: request_params[:device_model],
      platform: request_params[:platform],
      system_version: request_params[:system_version],
      operator: request_params[:operator],
      net_env: request_params[:net_env],
      app_version: request_params[:app_version],
      app_version_number: request_params[:app_version_number],
      app_channel: request_params[:app_channel],
      remote_ip: remote_ip,
      ip_country: ip_region[:country],
      ip_province: ip_region[:province],
      ip_city: ip_region[:city],
      sdid: request_params[:sdid],
      apiid: request_params[:apiid],
      device_uuid: request_params[:device_uuid],
      created_at: DateUtils.time2str(Time.now),
      active_type: UserActiveHourLog.active_types[:login]
    }
    CreateUserSigninLogJob.perform_later(signin_log_params)
    CreateUserActiveLogJob.perform_later(signin_log_params)
    if Setting.trend_submit_enabled == "true" && !User.is_robot?(user_id)
      Trend::UserSigninJob.perform_later(signin_log_params)
    end
  end

  # 创建登录日志
  def self.create_signin_log(params)
    return if User.is_robot?(params[:user_id].to_i)
    # 处理登录版本统计
    is_new_user = params.has_key?(:is_new_user) ? params[:is_new_user] : false
    if is_new_user
      StatService.increase_signup_users_count(
        params[:app_channel],
        params[:app_version],
        params[:platform],
        params[:app_version_number]
      )
    end
    StatService.record_signin_users_count(
      params[:user_id],
      params[:app_channel],
      params[:app_version],
      params[:platform],
      params[:app_version_number]
    )
    # 创建登录日志
    UserSigninLog.create(
      user_id: params[:user_id],
      session_id: params[:user_session_id],
      device_id: params[:device_id],
      device_name: params[:device_name],
      device_model: params[:device_model],
      platform: params[:platform],
      system_version: params[:system_version],
      operator: params[:operator],
      net_env: params[:net_env],
      app_version: params[:app_version],
      app_version_number: params[:app_version_number],
      app_channel: params[:app_channel],
      ip: params[:remote_ip],
      ip_country: params[:ip_country],
      ip_province: params[:ip_province],
      ip_city: params[:ip_city],
      created_at: params[:created_at],
      updated_at: params[:updated_at]
    )
    # -设备共享码
    # -api版本号
    # 以上两个字段主要用于android客户端
    if params[:sdid].present? && params[:sdid].length == 32
      DeviceShareCode.create_ignore_by_sql(
        params[:user_id],
        params[:sdid].downcase,
        params[:device_uuid].downcase,
        params[:apiid]&.to_i
      )
    end
  end

  # 创建小时活跃日志
  def self.create_active_hour_log(params)
    current_time = params[:created_at]
    UserActiveHourLog.create_ignore_by_sql(
      params[:user_id],
      current_time.to_date,
      current_time.to_datetime.hour,
      params[:app_channel],
      params[:app_version],
      params[:active_type],
      current_time.to_datetime.to_s,
      current_time.to_datetime.to_s
    )
  end

  # 更新登录信息，并把app_api_token存入缓存
  # 将更新操作放入队列
  def self.update_signin_info_perform_later(user, client_ip)
    app_api_token = Utils::Gen.friendly_token
    # 存入缓存
    User.set_cache_app_api_token(user.id, app_api_token)
    # 写入今日活跃人数缓存
    User.save_cache_active_list!(user.id)
    params = {
      user_id: user.id,
      app_api_token: app_api_token,
      signin_count: user.signin_count + 1,
      current_signin_at: DateUtils.time2str(Time.now),
      current_signin_ip: client_ip,
      last_signin_at: DateUtils.time2str(user.current_signin_at),
      last_signin_ip: user.current_signin_ip
    }
    UpdateUserSigninInfoJob.perform_later(params)
  end

  def self.update_signin_info(params)
    User.where(id: params[:user_id]).update_all(
      app_api_token: params[:app_api_token],
      is_online: true,
      signin_count: params[:signin_count],
      current_signin_at: params[:current_signin_at],
      current_signin_ip: params[:current_signin_ip],
      last_signin_at: params[:last_signin_at],
      last_signin_ip: params[:last_signin_ip]
    )
  end

end

class TrendStatService
  TIMEOUT = 800

  class << self

    # 日用户流量统计
    def stat_traffic_day(datetime)
      response = TrendStatService.get_data(
        "/api/trends/proxy/v1/statistics/traffic_day",
        { datetime: datetime }
      )
      result = JSON.parse(response.body, symbolize_names: true)
      JSON.parse(result[:data], symbolize_names: true)
    end

    # 月活跃用户统计
    def stat_active_users_month(datetime, channel = nil, version = nil)
      response = TrendStatService.get_data(
        "/api/trends/proxy/v1/statistics/active_users_month",
        { datetime: datetime, app_channel: channel, app_version: version }
      )
      result = JSON.parse(response.body, symbolize_names: true)
      JSON.parse(result[:data], symbolize_names: true)
    end

    # 去向日志统计
    def stat_user_connection_log(time_type, start_at, end_at, limit)
      # 这里最后加一个|是为了防止最后一个规则不起作用
      ignore_list = "/#{ConnectionLogFilter.cache_list.join('|')}|/"
      response = TrendStatService.get_data(
        "/api/trends/proxy/v1/statistics/user_connection_log_stat",
        {
          time_type: time_type,
          start_at: start_at,
          end_at: end_at,
          limit: limit,
          ignore_list: ignore_list
        },
        :post
      )
      JSON.parse(response.body, symbolize_names: true).dig(:result)
    end

    # 去向日志指定域名的用户统计
    def stat_user_connection_log_users(domain, time_type, start_at, end_at, limit)
      response = TrendStatService.get_data(
        "/api/trends/proxy/v1/statistics/user_connection_log_domain_users_stat",
        {
          domain: domain,
          time_type: time_type,
          start_at: start_at,
          end_at: end_at,
          limit: limit
        },
        :post
      )
      JSON.parse(response.body, symbolize_names: true).dig(:result)
    end

    # 去向日志地域统计
    def stat_user_connection_log_region(domain, limit)
      response = TrendStatService.get_data(
        "/api/trends/proxy/v1/statistics/user_connection_log_domain_region_stat",
        {
          domain: domain,
          limit: limit
        }
      )
      JSON.parse(response.body, symbolize_names: true).dig(:result)
    end

    # 去向日志地域国家统计
    def stat_user_connection_log_country(domain, limit)
      response = TrendStatService.get_data(
        "/api/trends/proxy/v1/statistics/user_connection_log_domain_country_stat",
        {
          domain: domain,
          limit: limit
        }
      )
      JSON.parse(response.body, symbolize_names: true).dig(:result)
    end

    # 用户登录小时时间间隔
    def stat_user_signin_hourly(start_at, end_at)
      response = TrendStatService.get_data(
        "/api/trends/proxy/v1/statistics/user_signin_hourly_stat",
        {
          start_at: start_at,
          end_at: end_at
        }
      )
      JSON.parse(response.body, symbolize_names: true).dig(:result)
    end

    # 页面点击统计
    def stat_page_click(start_at, end_at, app_channel, app_version)
      response = TrendStatService.get_data(
        "/api/trends/proxy/v1/statistics/page_click_stat",
        {
          start_at: start_at,
          end_at: end_at,
          app_channel: app_channel,
          app_version: app_version
        }
      )
      JSON.parse(response.body, symbolize_names: true).dig(:result)
    end

    # 获取数据
    def get_data(relative_url, params, query_method = :get)
      RestClient.log = 'stdout'
      headers = { "AUTHTOKEN": CONFIG.trend_token }
      Rails.logger.info params
      RestClient::Request.execute(
        method: query_method,
        url: "#{CONFIG.trend_host_url}#{relative_url}",
        payload: params,
        headers: headers,
        timeout: TIMEOUT
       )
    end

  end

end

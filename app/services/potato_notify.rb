class PotatoNotify

  # 订单成功率预警
  SENT_TYPE_TRANSACTION_WARN = 'potato_last_transaction_warn_sent_at'

  # 订单成功率预警
  def self.transaction_warn
    cache_key = SENT_TYPE_TRANSACTION_WARN
    # 检查短信CD时间是否充许发送
    return if !SmsNotify.allow_to_send?(cache_key, Setting.transaction_success_rate_sms_period.to_i)
    potato_groups = Setting.transaction_success_rate_groups.gsub('|', ',')
    group_list = potato_groups.split(',')
    if group_list.present?
      group_list.each do |group_id|
        send_message({ chat_type: 2, chat_id: group_id.to_i, text: "订单成功率低于#{Setting.transaction_success_rate_threshold.to_i}%" })
      end
      # 写入最后发送日期
      SmsNotify.save_sent_time(cache_key)
    end
  end

  # 发送消息
  def self.send_message(params)
    RestClient.log = 'stdout'
    headers = { content_type: :json, accept: :json }
    Rails.logger.info params
    RestClient.post(CONFIG.potato_host_url, params.to_json, headers)
  end

end

class StatService

  HOUR_RANGE = *(0..23)

  VERSION_SIGNUP_CHANNEL = "signup_users_channel"
  VERSION_SIGNUP_VERSION = "signup_users_version"
  VERSION_SIGNUP_CHANNEL_VERSION = "signup_users_channel_version"
  VERSION_SIGNUP_VERSION_NUM = "signup_users_version_num"
  VERSION_SIGNIN_PREFIX = "signin_users_"

  class << self

    # 转换ActiveRecord::Relation为Hash
    # keys为忽略的键
    def to_hash(items, *keys)
      return [] if !items.is_a?(ActiveRecord::Relation) || items.blank?
      items.to_a.map(&:serializable_hash)
        .map(&:symbolize_keys!)
        .map{|i| i.except!(*keys)}
    end

    # 转换ActiveRecord::Relation为JSON字符串
    # keys为忽略的键
    def to_json(items, *keys)
      return [] if !items.is_a?(ActiveRecord::Relation)
      items.to_a.map(&:serializable_hash)
        .map{|i| i.except!(*keys)}
        .to_json
    end

    # 获得缺失小时段列表, 并使用black做相关操作
    def missing_hours(origin_hours)
      return [] if !origin_hours.is_a?(Array)
      missing_hours = HOUR_RANGE - origin_hours
      if block_given?
        if missing_hours.length > 0
          missing_hours.each do |h|
            yield(h)
          end
        end
      end
      missing_hours
    end

    # 生成各个筛选项对应的小时列表及对应的属性
    # list(array)，包含:
    #   - 筛选项名称
    #   - 筛选项对应的小时数组(array)
    #   如: [["appStore", [10, 14]], ["moren", [10]], ["pc", [14, 18]], ["mac", [19]]]
    def generate_hash_missing_hours(list)
      hash_hours = {}
      return hash_hours if !list.is_a?(Array)
      list.each do |item|
        hash_hours[item[0]] = item[1]
      end
      hash_hours
    end

    # 获得指定指定渠道版本的缺失小时段列表
    # 并轮循缺失的小时段使用block做相关操作
    def hash_missing_hours(hash_list)
      fill_range = {}
      hash_list.each do |key, hours|
        fill_range[key] = HOUR_RANGE - hours
      end
      fill_range.each do |key, hours|
        if hours.length > 0
          hours.each do |h|
            yield(key, h)
          end
        end
      end
    end

    # 记录注册时的版本信息
    # action: 注册或登录, signin/signup
    # channel: 渠道
    # version: 版本
    # platform: 平台
    # version_number: 版本号
    def increase_signup_users_count(channel, version, platform, version_num)
      $redis.hincrby(StatService::VERSION_SIGNUP_CHANNEL, channel, 1)
      $redis.hincrby(StatService::VERSION_SIGNUP_VERSION, version, 1)
      $redis.hincrby(StatService::VERSION_SIGNUP_CHANNEL_VERSION, "#{channel}___#{version}", 1)
      $redis.hincrby(StatService::VERSION_SIGNUP_VERSION_NUM, "#{platform}___#{version_num}", 1)
    end

    # 按渠道和版本获取注册用户数
    def signup_users_count(channel, version)
      return 0 if channel.blank? && version.blank?
      if channel.present? && version.present?
        $redis.hget(StatService::VERSION_SIGNUP_CHANNEL_VERSION, "#{channel}___#{version}").to_i
      elsif channel.present? && version.blank?
        $redis.hget(StatService::VERSION_SIGNUP_CHANNEL, channel).to_i
      elsif channel.blank? && version.present?
        $redis.hget(StatService::VERSION_SIGNUP_VERSION, version).to_i
      end
    end

    # 记录登录时的版本信息
    # 同一用户如果在登录时版本信息发生改变，则需要从原集合中移除此用户
    # 并将其添加到新集合中
    def record_signin_users_count(user_id, channel, version, platform, version_num)
      # 查询上次登录的版本的平台信息
      last_channel, last_version, last_version_num, last_platform = User.last_signin_versions_by_id(user_id)
      # 对比渠道
      if channel != last_channel
        # 从旧渠道中移除
        $redis.srem("#{VERSION_SIGNIN_PREFIX}channel_#{last_channel}", user_id)
      end
      # 加入新渠道中
      $redis.sadd("#{VERSION_SIGNIN_PREFIX}channel_#{channel}", user_id)
      # 对比版本
      if version != last_version
        $redis.srem("#{VERSION_SIGNIN_PREFIX}version_#{last_version}", user_id)
      end
      $redis.sadd("#{VERSION_SIGNIN_PREFIX}version_#{version}", user_id)
      # 对比平台和版本号
      if platform != last_platform || version_num != last_version_num
        $redis.srem("#{VERSION_SIGNIN_PREFIX}platform_vn_#{last_platform}_#{last_version_num}", user_id)
      end
      $redis.sadd("#{VERSION_SIGNIN_PREFIX}platform_vn_#{platform}_#{version_num}", user_id)
    end

  end

end

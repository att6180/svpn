class SmsNotify

  # 代理服务器连接失败
  SENT_TYPE_PROXY_FAILED = "last_proxy_failed_warn_sent_at"
  # 测试机器人连接失败
  SENT_TYPE_TEST_ROBOT_FAILED = 'last_test_robot_warn_sent_at'
  # 订单成功率预警
  SENT_TYPE_TRANSACTION_WARN = 'last_transaction_warn_sent_at'

  # 代理服务器连接失败预警检查
  def self.check_proxy_failed
    # 检查指定时间段内的代理连接失败信息
    logs = ProxyConnectionFailedLog.select(:id, :user_id, :node_id)
      .last_by_time(Setting.proxy_conn_failed_warn_trigger_period.to_i)
    # 用户数
    users_count = logs.map(&:user_id).uniq.count
    # 次数
    times_count = logs.map(&:id).count
    # 如果用户数或次数任意一项达到指定数目，则触发短信警报
    if users_count >= Setting.proxy_conn_failed_warn_trigger_users.to_i ||
        times_count >= Setting.proxy_conn_failed_warn_trigger_times.to_i
      if allow_to_send?(SENT_TYPE_PROXY_FAILED, Setting.proxy_conn_failed_warn_trigger_sms_period.to_i)
        telephones = Setting.proxy_conn_failed_warn_trigger_phones.gsub('|', ',')
        telephone_list = telephones.split(',')
        if telephone_list.present?
          nodes = Node.select(:name, :url).where(id: logs.map(&:node_id).uniq)
          nodes_content = nodes.map{|n| "#{n.name}:#{n.url}"}.join(',')
          Sms.batch_send(telephones, CONFIG.sms_proxy_failed_tpl_id, 'node', nodes_content)
          # 写入最后发送日期
          save_sent_time(SENT_TYPE_PROXY_FAILED)
        end
      end
    end
  end

  # 测试机器人连接失败预警检查
  def self.check_test_robot_failed(node_id)
    cache_key = "#{SENT_TYPE_TEST_ROBOT_FAILED}_#{node_id}"
    # 检查短信CD时间是否充许发送
    return if !allow_to_send?(cache_key, Setting.test_robot_warn_trigger_sms_period.to_i)
    logs = ProxyTestDomainLog.by_group(node_id).index_by{|l| l.is_successed ? "1" : "0" }
    successed_users_count = logs.has_key?("1") ? logs["1"].users_count : 0
    failed_users_count = logs.has_key?("0") ? logs["0"].users_count : 0
    total_count = successed_users_count + failed_users_count
    threshold = Setting.test_robot_per_node_failure_percent.to_i / 100.to_f
    if (failed_users_count / total_count.to_f) > threshold
      telephones = Setting.test_robot_warn_trigger_phones.gsub('|', ',')
      telephone_list = telephones.split(',')
      if telephone_list.present?
        node = Node.find_by(id: node_id)
        return if node.blank?
        content = "#{node.name}:#{node.url}"
        Sms.batch_send(telephones, Setting.test_robot_warn_trigger_sms_tpl_id, 'node', content)
        # 写入最后发送日期
        save_sent_time(cache_key)
      end
    end
  end

  # 订单成功率预警
  def self.transaction_warn
    cache_key = SENT_TYPE_TRANSACTION_WARN
    # 检查短信CD时间是否充许发送
    return if !allow_to_send?(cache_key, Setting.transaction_success_rate_sms_period.to_i)
    telephones = Setting.transaction_success_rate_phones.gsub('|', ',')
    telephone_list = telephones.split(',')
    if telephone_list.present?
      result = Sms.batch_send(telephones, Setting.transaction_success_rate_sms_tpl_id, nil, nil)
      # 写入最后发送日期
      save_sent_time(cache_key)
    end
  end

  def self.allow_to_send?(sent_type, period)
    result = true
    last_sent_at = Rails.cache.read("#{sent_type}")
    if last_sent_at.present? && Time.now - Time.at(last_sent_at.to_i) < period.minutes
      result = false
    end
    result
  end

  def self.save_sent_time(sent_type)
    Rails.cache.write("#{sent_type}", Time.now.to_i)
  end

end

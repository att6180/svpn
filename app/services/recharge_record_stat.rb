class RechargeRecordStat

  # 充值总额
  def self.total_recharge_amount
    Rails.cache.fetch("manage_total_recharge_amount", expires_in: DateUtils.remaining_time) do
      User.where("total_payment_amount > 0").sum(:total_payment_amount).to_f
    end
  end

end

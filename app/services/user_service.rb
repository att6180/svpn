class UserService

  class << self

    # 队列重新生成session token
    def regenerate_session_token(session_id, token)
      UserSession.write_token!(session_id, token, UserSession::ERR_AFTER_SIGN_IN)
    end

    # build new user
    def new_user(params, client_ip, ip_region)
      user = User.new(
        create_device_uuid: params[:device_uuid].downcase,
        create_device_name: params[:device_name],
        create_device_model: params[:device_model],
        create_platform: params[:platform],
        create_system_version: params[:system_version],
        create_operator: params[:operator],
        create_net_env: params[:net_env],
        create_app_version: params[:app_version],
        create_app_version_number: params[:app_version_number],
        create_app_channel: params[:app_channel],
        current_signin_ip: client_ip,
        last_signin_ip: client_ip,
        create_ip: client_ip,
        create_ip_country: ip_region[:country],
        create_ip_province: ip_region[:province],
        create_ip_city: ip_region[:city],
      )
      if params.has_key?(:telephone) && params.has_key?(:password)
        user.username = params[:telephone]
        user.password = params[:password]
        user.area_code = params[:area_code]
      end
      user
    end

    # 按渠道版本返回用户总数
    def users_count_by_version(channel, version)
    end

  end
end

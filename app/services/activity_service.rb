class ActivityService
	class << self
		# 补偿活动增加新老用户15天高级时长
		#  判断用户是否是被邀请注册的
		#  判断老用户是否为手机号注册且在活动之前注册
		#  判断新用户注册时间是否是活动时间范围内
		#
		def reward_activity(user, processed_at)
			if user.promo_user_id.present?
				promo_user = User.find(user.promo_user_id)
				if promo_user.username_changed? && (promo_user.created_at < Setting.activity_begin_at.to_time) && (user.created_at > Setting.activity_begin_at.to_time) && (user.created_at < Setting.activity_end_at.to_time) && (processed_at.to_time < Setting.activity_end_at.to_time) && (processed_at.to_time > Setting.activity_begin_at.to_time)
					params = {
						user_id: user.id,
						promo_user_id: promo_user.id,
						processed_at: processed_at
					}
					RechargeCompensationJob.perform_later(params)
				end
			end
		end
	end
end
class TrendService
  TIMEOUT = 10

  class << self

    # 向态势图服务器提交用户注册数据
    def user_signin(params)
      p = {
        user_id: params[:user_id],
        account: params[:username],
        client_ip: params[:remote_ip],
        ip_country: params[:ip_country],
        ip_province: params[:ip_province],
        ip_city: params[:ip_city],
        created_at: params[:created_at],
        app_channel: params[:app_channel],
        app_version: params[:app_version]
      }
      r = TrendService.post_data("/api/trends/proxy/v1/users/user_login", p)
    end

    # 提交在线用户区域数据
    def online_users
      return if Setting.trend_submit_enabled != "true"
      region_data = NodeOnlineUser.region_stat
      params = { data: region_data }.to_json
      r = TrendService.post_data("/api/trends/proxy/v1/users/online_user", params, :json)
    end

    def connect_and_signup(params, action_type)
      p = {
        user_id: params[:user_id],
        account: params[:username],
        client_ip: params[:remote_ip],
        ip_country: params[:ip_country],
        ip_province: params[:ip_province],
        ip_city: params[:ip_city],
        created_at: params[:created_at],
        type: action_type
      }
      r = TrendService.post_data("/api/trends/proxy/v1/users/connect_or_sigup", p)
    end

    # 向态势图提交用户操作日志
    def operation_log(params)
      logs = []
      operations = params[:operations].split('|')
      operations.each do |os|
        next if os.blank?
        operation = os.split(',')
        user_id = operation[1]
        ts = operation[2].to_i
        created_at = ts > 0 ? DateUtils.time2str(Utils.ts2t(ts)) : DateUtils.time2str(Time.now)
        if $redis.sismember(User::CACHE_KEY_IDS, user_id.to_i)
          logs << {
            user_id: user_id,
            interface_id: operation[0],
            app_channel: params[:app_channel],
            app_version: params[:app_version],
            created_at: created_at
          }
        end
      end
      r = TrendService.post_data("/api/trends/proxy/v1/users/page_click", { data: logs }.to_json, :json) if logs.size > 0
    end

    # 提交数据
    # type: 数据类型，默认form-data，json: json类型
    def post_data(relative_url, params, type = nil)
      headers = { "AUTHTOKEN": CONFIG.trend_token }
      headers = headers.merge({content_type: :json, accept: :json}) if type == :json
      RestClient::Request.execute(
        method: :post,
        url: "#{CONFIG.trend_host_url}#{relative_url}",
        payload: params,
        headers: headers,
        timeout: TIMEOUT
       )
    end

  end

end

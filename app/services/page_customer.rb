class PageCustomer

  def initialize(model_class, page_query, count_query, page, limit)
    @page_sql = page_query
    @count_sql = count_query
    @page = page
    @limit = limit
    @model_class = model_class

    @page_result = model_class.connection.exec_query(page_query)

    @total_result = model_class.connection.exec_query(count_query)
    @total_count = @total_result.rows&.first&.first || 0
  end

  def result
    @page_result
  end

  def current_page
    @page
  end

  def limit_value
    @limit
  end

  def offset
    (@page - 1) * @limit
  end

  def total_pages
    (@total_count.to_f / @limit.to_f).ceil
  end

  def total_count
    @total_count
  end

end

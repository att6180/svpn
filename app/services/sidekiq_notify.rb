class SidekiqNotify

  SENT_TYPE_SIDEKIQ_RETRY_SIZE = 'last_sidekiq_warn_sent_at'

  def self.notify

    return if Setting.sidekiq_monit_enabled != "true"

    retry_size = Setting.sidekiq_retry_size
    telephones = Setting.sidekiq_sms_alarm_telphones
    tpl_id = Setting.sidekiq_sms_alarm_tpl_id

    rs = Sidekiq::RetrySet.new
    if rs.size > retry_size.to_i
      return if !SmsNotify.allow_to_send?(SENT_TYPE_SIDEKIQ_RETRY_SIZE, Setting.sidekiq_alarm_interval)
      Sms.batch_send(
        telephones.to_s,
        tpl_id,
        'number&current_number',
        "#{retry_size}&#{rs.size}"
      )
    end
  end
end

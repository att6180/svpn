FROM ruby:2.6.0-alpine

MAINTAINER az "https://github.com/az"
RUN gem install bundler
RUN sed -i 's/dl-cdn.alpinelinux.org/mirrors.ustc.edu.cn/g' /etc/apk/repositories
RUN apk --update add ca-certificates nodejs tzdata &&\
    apk add --virtual .builddeps build-base ruby-dev libc-dev openssl linux-headers mysql-dev \
    libxml2-dev libxslt-dev git

ENV RAILS_ENV "production"

WORKDIR /home/app/svpn

RUN mkdir -p /home/app &&\
		find / -type f -iname '*.apk-new' -delete &&\
		rm -rf '/var/cache/apk/*' '/tmp/*'

ADD Gemfile Gemfile.lock /home/app/svpn/
# RUN bundle install --deployment --jobs 20 --retry 5 &&\
# 		find /home/app/svpn/vendor/bundle -name tmp -type d -exec rm -rf {} +
RUN bundle install --without development test --jobs 20 --retry 5 
ADD . /home/app/svpn

VOLUME ["/home/app/svpn/log", "/home/app/svpn/tmp"]

RUN rm -Rf /home/app/svpn/vendor/cache

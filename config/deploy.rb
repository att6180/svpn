require 'mina/multistage'
require 'mina/bundler'
require 'mina/rails'
require 'mina/git'
require 'mina/rbenv'  # for rbenv support. (http://rbenv.org)
# require 'mina/rvm'    # for rvm support. (http://rvm.io)
require "mina_sidekiq/tasks"
#require 'mina/puma'

# Basic settings:
#   domain       - The hostname to SSH to.
#   deploy_to    - Path to deploy into.
#   repository   - Git repo to clone from. (needed by mina/git)
#   branch       - Branch name to deploy. (needed by mina/git)

#set :domain, 'deploy@xiaoguoqi.com'
#set :deploy_to, '/home/deploy/fulmo'
#set :repository, 'git@bitbucket.org:bindiry/fulmo3.git'
#set :branch, 'master'

# For system-wide RVM install.
#   set :rvm_path, '/usr/local/rvm/bin/rvm'

# Manually create these paths in shared/ (eg: shared/config/database.yml) in your server.
# They will be linked in the 'deploy:link_shared_paths' step.
set :shared_paths, [
  'tmp',
  'log',
  'misc',
  'data',
  'public/routes',
  'config/puma.rb',
  'config/database.yml',
  'config/database_slave1.yml',
  'config/database_log.yml',
  'config/database_log_slave1.yml',
  'config/database_manage.yml',
  'config/database_misc.yml',
  'config/mongoid.yml',
  'config/secrets.yml',
  'config/scout_apm.yml',
  'config/elastic_apm.yml',
  'config/sidekiq.yml'
]

set :sidekiq_pid, "#{deploy_to}/#{shared_path}/tmp/pids/sidekiq.pid"

# Optional settings:
#   set :user, 'foobar'    # Username in the server to SSH to.
#   set :port, '30000'     # SSH port number.
#   set :forward_agent, true     # SSH forward_agent.

# This task is the environment that is loaded for most commands, such as
# `mina deploy` or `mina rake`.
task :environment do
  # If you're using rbenv, use this to load the rbenv environment.
  # Be sure to commit your .ruby-version or .rbenv-version to your repository.
   invoke :'rbenv:load'

  # For those using RVM, use this to load an RVM version@gemset.
  # invoke :'rvm:use[ruby-1.9.3-p125@default]'
end

# Put any custom mkdir's in here for when `mina setup` is ran.
# For Rails apps, we'll make some of the shared paths that are shared between
# all releases.
task :setup => :environment do
  queue! %[mkdir -p "#{deploy_to}/#{shared_path}/log"]
  queue! %[chmod g+rx,u+rwx "#{deploy_to}/#{shared_path}/log"]

  queue! %[mkdir -p "#{deploy_to}/#{shared_path}/config"]
  queue! %[chmod g+rx,u+rwx "#{deploy_to}/#{shared_path}/config"]

  queue! %[mkdir -p "#{deploy_to}/#{shared_path}/misc"]
  queue! %[chmod g+rx,u+rwx "#{deploy_to}/#{shared_path}/misc"]

  queue! %[mkdir -p "#{deploy_to}/#{shared_path}/data"]
  queue! %[wget -O "#{deploy_to}/#{shared_path}/data/17monipdb.datx" https://coding.net/u/zoucaitou/p/misc/git/raw/master/17monipdb.datx]
  queue! %[wget -O "#{deploy_to}/#{shared_path}/data/qqwry.dat" https://coding.net/u/zoucaitou/p/misc/git/raw/master/qqwry.dat]

  # puma
  queue! %[mkdir -p "#{deploy_to}/#{shared_path}/tmp/pids"]
  queue! %[chmod g+rx,u+rwx "#{deploy_to}/#{shared_path}/tmp/pids"]
  #queue! %[touch "#{deploy_to}/#{shared_path}/tmp/pids/puma.pid"]

  queue! %[mkdir -p "#{deploy_to}/#{shared_path}/tmp/sockets"]
  queue! %[chmod g+rx,u+rwx "#{deploy_to}/#{shared_path}/tmp/sockets"]
  #queue! %[touch "#{deploy_to}/#{shared_path}/tmp/sockets/puma.state"]
  #queue! %[touch "#{deploy_to}/#{shared_path}/tmp/sockets/puma.sock"]
  #queue! %[touch "#{deploy_to}/#{shared_path}/tmp/sockets/pumactl.sock"]

  queue! %[touch "#{deploy_to}/#{shared_path}/log/puma.stdout.log"]
  queue! %[touch "#{deploy_to}/#{shared_path}/log/puma.stderr.log"]

  # sidekiq
  queue! %[touch "#{deploy_to}/#{shared_path}/log/sidekiq.log"]

  queue! %[touch "#{deploy_to}/#{shared_path}/config/database.yml"]
  queue! %[touch "#{deploy_to}/#{shared_path}/config/database_log.yml"]
  queue! %[touch "#{deploy_to}/#{shared_path}/config/database_manage.yml"]
  queue! %[touch "#{deploy_to}/#{shared_path}/config/database_misc.yml"]
  queue! %[touch "#{deploy_to}/#{shared_path}/config/secrets.yml"]
  queue! %[touch "#{deploy_to}/#{shared_path}/config/elastic_apm.yml"]
  queue! %[touch "#{deploy_to}/#{shared_path}/config/sidekiq.yml"]

  # backup
  queue! %[mkdir -p "#{deploy_to}/backup"]
  queue  %[echo "-----> Be sure to edit '#{deploy_to}/#{shared_path}/config/database*.yml' / 'secrets.yml' / 'puma.rb'. download 17monipdb.dat."]

  if repository
    repo_host = repository.split(%r{@|://}).last.split(%r{:|\/}).first
    repo_port = /:([0-9]+)/.match(repository) && /:([0-9]+)/.match(repository)[1] || '22'

    queue %[
      if ! ssh-keygen -H  -F #{repo_host} &>/dev/null; then
        ssh-keyscan -t rsa -p #{repo_port} -H #{repo_host} >> ~/.ssh/known_hosts
      fi
    ]
  end
end

# puma
namespace :puma do
  set :puma_pid, "#{deploy_to}/#{shared_path}/tmp/pids/puma.pid"
  set :start_puma, %{
    cd #{deploy_to}/#{current_path}
    bundle exec puma -q -d -C #{deploy_to}/#{shared_path}/config/puma.rb
  }

  desc "Start Puma"
  task :start => :environment do
    queue 'echo "-----> Start Puma"'
    queue! start_puma
  end

  desc "Stop Puma"
  task :stop do
    queue 'echo "-----> Stop Puma"'
    queue! %{
      test -s "#{puma_pid}" && kill -QUIT `cat "#{puma_pid}"` && echo "Stop Ok" && exit 0
      echo >&2 "Not running"
    }
  end

  desc "Restart Puma using 'upgrade'"
  task :restart => :environment do
    invoke 'puma:stop'
    invoke 'puma:start'
  end
end

namespace :swagger do
  set :generate_docs, %{
    cd #{deploy_to}/#{current_path}
    #{rake} swagger:docs
  }
  desc "Generate swagger docs"
  task :docs => :environment do
    queue 'echo "------> Generate swagger docs"'
    queue! generate_docs
  end
end

namespace :other_db do
  # ### rails:db_migrate
  desc "Migrates the Rails other database (skips if nothing has changed since the last release)."
  task :log_db_migrate do
    message = verbose_mode? ?
      '$((count)) changes found, migrating database' :
      'Migrating database'

    queue check_for_changes_script \
      :check => 'db_log/migrate/',
      :at => ['db_log/migrate/'],
      :skip => %[
        echo "-----> DB migrations unchanged; skipping DB migration"
      ],
      :changed => %[
        echo "-----> #{message}"
        #{echo_cmd %[#{rake} log:db:migrate]}
      ],
      :default => %[
        echo "-----> Migrating database"
        #{echo_cmd %[#{rake} log:db:migrate]}
      ]
  end
  task :manage_db_migrate do
    message = verbose_mode? ?
      '$((count)) changes found, migrating database' :
      'Migrating database'

    queue check_for_changes_script \
      :check => 'db_manage/migrate/',
      :at => ['db_manage/migrate/'],
      :skip => %[
        echo "-----> DB migrations unchanged; skipping DB migration"
      ],
      :changed => %[
        echo "-----> #{message}"
        #{echo_cmd %[#{rake} manage:db:migrate]}
      ],
      :default => %[
        echo "-----> Migrating database"
        #{echo_cmd %[#{rake} manage:db:migrate]}
      ]
  end
  task :misc_db_migrate do
    message = verbose_mode? ?
      '$((count)) changes found, migrating database' :
      'Migrating database'

    queue check_for_changes_script \
      :check => 'db_misc/migrate/',
      :at => ['db_misc/migrate/'],
      :skip => %[
        echo "-----> DB migrations unchanged; skipping DB migration"
      ],
      :changed => %[
        echo "-----> #{message}"
        #{echo_cmd %[#{rake} misc:db:migrate]}
      ],
      :default => %[
        echo "-----> Migrating database"
        #{echo_cmd %[#{rake} misc:db:migrate]}
      ]
  end
end

desc "Deploys the current version to the server."
task :deploy => :environment do
  to :before_hook do
    # Put things to run locally before ssh
  end
  deploy do
    invoke :'sidekiq:quiet' if sidekiq_enabled
    # Put things that will set up an empty directory into a fully set-up
    # instance of your project.
    invoke :'git:clone'
    invoke :'deploy:link_shared_paths'
    invoke :'bundle:install'
    invoke :'rails:db_migrate'
    invoke :'other_db:log_db_migrate'
    invoke :'other_db:manage_db_migrate'
    invoke :'other_db:misc_db_migrate'
    invoke :'rails:assets_precompile'
    invoke :'deploy:cleanup'

    to :launch do
      invoke :'swagger:docs' if swagger_generate
      invoke :'puma:restart'
      #invoke :'puma:phased_restart'
      invoke :'sidekiq:restart' if sidekiq_enabled
      queue "nut monit monitor all"
      queue "nut monit status"
      #queue "mkdir -p #{deploy_to}/#{current_path}/tmp/"
      #queue "touch #{deploy_to}/#{current_path}/tmp/restart.txt"
    end
  end
end

# nginx setting
task :ng_restart do
  queue 'nut service nginx restart'
end

require 'mina/bundler'
#require 'mina/multistage'
require 'mina/rails'
require 'mina/git'
require 'mina/rbenv'  # for rbenv support. (https://rbenv.org)
# require 'mina/rvm'    # for rvm support. (https://rvm.io)
require 'mina/puma'
require 'mina_sidekiq/tasks'

# Basic settings:
#   domain       - The hostname to SSH to.
#   deploy_to    - Path to deploy into.
#   repository   - Git repo to clone from. (needed by mina/git)
#   branch       - Branch name to deploy. (needed by mina/git)

set :domain, 'hk02.junnan.org'
set :user, 'deploy'
set :deploy_to, '/home/deploy/svpn'
set :repository, 'git@bitbucket.org:bindiry/fulmo3.git'
set :branch, 'master'
set :sidekiq_pid, -> { "#{fetch(:shared_path)}/tmp/pids/sidekiq.pid" }
#set :sidekiq, -> { "#{fetch(:bundle_prefix)} sidekiq" }

# Optional settings:
#set :user, 'deploy'          # Username in the server to SSH to.
#   set :port, '30000'           # SSH port number.
#   set :forward_agent, true     # SSH forward_agent.

# They will be linked in the 'deploy:link_shared_paths' step.
set :shared_dirs, fetch(:shared_dirs, []).push(
  'tmp',
  'log',
  'data',
  'misc'
)
set :shared_files, fetch(:shared_files, []).push(
  'config/puma.rb',
  'config/database.yml',
  'config/database_log.yml',
  'config/database_manage.yml',
  'config/database_misc.yml',
  'config/secrets.yml'
)

# This task is the environment that is loaded all remote run commands, such as
# `mina deploy` or `mina rake`.
task :environment do
  # If you're using rbenv, use this to load the rbenv environment.
  # Be sure to commit your .ruby-version or .rbenv-version to your repository.
  invoke :'rbenv:load'

  # For those using RVM, use this to load an RVM version@gemset.
  # invoke :'rvm:use', 'ruby-1.9.3-p125@default'
end

# Put any custom commands you need to run at setup
# All paths in `shared_dirs` and `shared_paths` will be created on their own.
task :setup do
  #command %[mkdir -p "#{fetch(:deploy_to)}/current"]

  command %[mkdir -p "#{fetch(:shared_path)}/log"]
  command %[chmod g+rx,u+rwx "#{fetch(:shared_path)}/log"]

  command %[mkdir -p "#{fetch(:shared_path)}/config"]
  command %[chmod g+rx,u+rwx "#{fetch(:shared_path)}/config"]

  command %[mkdir -p "#{fetch(:shared_path)}/misc"]
  command %[chmod g+rx,u+rwx "#{fetch(:shared_path)}/misc"]

  command %[mkdir -p "#{fetch(:shared_path)}/data"]
  command %[wget -O "#{fetch(:shared_path)}/data/17monipdb.dat" https://coding.net/u/bindiry/p/misc/git/raw/master/17monipdb.dat]

  # puma
  command %[mkdir -p "#{fetch(:shared_path)}/tmp/pids"]
  command %[chmod g+rx,u+rwx "#{fetch(:shared_path)}/tmp/pids"]
  command %[mkdir -p "#{fetch(:shared_path)}/tmp/sockets"]
  command %[chmod g+rx,u+rwx "#{fetch(:shared_path)}/tmp/sockets"]
  command %[touch "#{fetch(:shared_path)}/log/puma.stdout.log"]
  command %[touch "#{fetch(:shared_path)}/log/puma.stderr.log"]

  # sidekiq
  command %[touch "#{fetch(:shared_path)}/log/sidekiq.log"]

  command %[touch "#{fetch(:shared_path)}/config/database.yml"]
  command %[touch "#{fetch(:shared_path)}/config/database_log.yml"]
  command %[touch "#{fetch(:shared_path)}/config/database_manage.yml"]
  command %[touch "#{fetch(:shared_path)}/config/database_misc.yml"]
  command %[touch "#{fetch(:shared_path)}/config/secrets.yml"]

  # backup
  command %[mkdir -p "#{fetch(:deploy_to)}/backup"]
  command %[echo "-----> Be sure to edit '#{fetch(:shared_path)}/config/database.yml' and 'secrets.yml'."]

  if fetch(:repository)
    repo_host = fetch(:repository).split(%r{@|://}).last.split(%r{:|\/}).first
    repo_port = /:([0-9]+)/.match(fetch(:repository)) && /:([0-9]+)/.match(fetch(:repository))[1] || '22'

    command %[
      if ! ssh-keygen -H  -F #{repo_host} &>/dev/null; then
        ssh-keyscan -t rsa -p #{repo_port} -H #{repo_host} >> ~/.ssh/known_hosts
      fi
    ]
  end
end

set :log_migration_dirs, ['db_log/migrate']
set :manage_migration_dirs, ['db_manage/migrate']
set :misc_migration_dirs, ['db_manage/migrate']
namespace :other_db do
  desc 'Migrate database'
  task :log_db_migrate do
    if fetch(:force_migrate)
      comment %{Migrating database}
      command %{#{fetch(:rake)} log:db:migrate}
    else
      command check_for_changes_script(
        at: fetch(:log_migration_dirs),
        skip: %{echo "-----> DB migrations unchanged; skipping LOG DB migration"},
        changed: %{echo "-----> Migrating database"
          #{echo_cmd("#{fetch(:rake)} log:db:migrate")}}
      ), quiet: true
    end
  end
  task :manage_db_migrate do
    if fetch(:force_migrate)
      comment %{Migrating database}
      command %{#{fetch(:rake)} manage:db:migrate}
    else
      command check_for_changes_script(
        at: fetch(:log_migration_dirs),
        skip: %{echo "-----> DB migrations unchanged; skipping MANAGE DB migration"},
        changed: %{echo "-----> Migrating database"
          #{echo_cmd("#{fetch(:rake)} manage:db:migrate")}}
      ), quiet: true
    end
  end
  task :misc_db_migrate do
    if fetch(:force_migrate)
      comment %{Migrating database}
      command %{#{fetch(:rake)} misc:db:migrate}
    else
      command check_for_changes_script(
        at: fetch(:misc_migration_dirs),
        skip: %{echo "-----> DB migrations unchanged; skipping MANAGE DB migration"},
        changed: %{echo "-----> Migrating database"
          #{echo_cmd("#{fetch(:rake)} misc:db:migrate")}}
      ), quiet: true
    end
  end
end


desc "Deploys the current version to the server."
task :deploy do
  deploy do
    invoke :'git:clone'
    invoke :'deploy:link_shared_paths'
    invoke :'bundle:install'
    invoke :'rails:db_migrate'
    invoke :'other_db:log_db_migrate'
    invoke :'other_db:manage_db_migrate'
    invoke :'other_db:misc_db_migrate'
    invoke :'rails:assets_precompile'
    invoke :'deploy:cleanup'

    on :launch do
      invoke :'sidekiq:quiet'
      invoke :'sidekiq:restart'
      invoke :'puma:start'
      invoke :'puma:phased_restart'
      in_path(fetch(:current_path)) do
        command %{mkdir -p tmp/}
        command %{touch tmp/restart.txt}
      end
    end
  end

  # you can use `run :local` to run tasks on local machine before of after the deploy scripts
  # run :local { say 'done' }
end

desc "Starts an interactive rails console."
task :c => :environment do
  invoke :'console'
end

# nginx setting
task :ng_restart do
  command 'sudo service nginx restart'
end

set :domain, 'deploy@119.28.136.21'
set :ssh_options, "-o'ProxyCommand nc -X 5 -x 192.168.1.9:20992 %h %p'"
set :deploy_to, '/home/deploy/svpn'
#set :repository, 'git@bitbucket.org:zhy8871/fulmo3.git'
# set :repository, 'git@github.com:hulunxi/fulmo.git'
set :repository, 'git@bitbucket.org:zoucaitou/svpn.git'
set :branch, 'staging'
set :sidekiq_enabled, true
set :swagger_generate, true

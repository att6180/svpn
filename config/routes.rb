Rails.application.routes.draw do

  if CONFIG.docs_enabled
    # API文档
    get '/docs/index', to: 'docs#index'
    get '/docs/client_v2', to: 'docs#client_v2'
    get '/docs/client_v3', to: 'docs#client_v3'
    get '/docs/client_v4', to: 'docs#client_v4'
    get '/docs/testrobot', to: 'docs#testrobot'
    get '/docs/ops', to: 'docs#ops'
    # 后台管理文档
    get '/docs/manage', to: 'docs#manage'
    get '/docs/manage_v2', to: 'docs#manage_v2'
    #get '/docs/tables', to: 'docs#tables'
    get '/docs/ip', to: 'docs#ip'
  end

  # sidekiq
  if [1, 2].include?(CONFIG.server_type)
    require 'sidekiq/web'
    require 'sidekiq/cron/web' if defined?(Sidekiq::Cron)
    Sidekiq::Web.use Rack::Auth::Basic do |username, password|
      username == CONFIG.docs_auth_username && password == CONFIG.docs_auth_password
    end if Rails.env.production?
    mount Sidekiq::Web => '/admin/sidekiq'
    ExceptionTrack::Engine.middleware.use Rack::Auth::Basic do |username, password|
      username == CONFIG.docs_auth_username && password == CONFIG.docs_auth_password
    end if Rails.env.production?
    mount ExceptionTrack::Engine => "/admin/exception-track"
  end

  root to: 'home#index', as: :root

end

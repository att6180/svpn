Rails.application.routes.draw do

  namespace :api, defaults: {format: :json} do
    if [1, 2].include?(CONFIG.server_type)

      # ops server api
      namespace :ops do
        namespace :v1 do
          resources :nodes do
            collection do
              get :node_type
            end
          end
        end
      end

    end
  end
end

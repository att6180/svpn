Rails.application.routes.draw do

  namespace :api, defaults: {format: :json} do
    if [1, 2].include?(CONFIG.server_type)

      # client api
      namespace :client do
        namespace :v1 do
          post :send_verification_code, to: 'registrations#send_verification_code'
          post :reset_password, to: 'registrations#reset_password'
          post :signup, to: 'registrations#create'
          post :comsunny_callback, to: 'plans#comsunny_callback'
          resources :ads do
            collection do
              get :splash_screen
              post :splash_screen_log
              post :splash_screen_click_log
              get :popup
              post :popup_log
              post :popup_click_log
              get :navigation
              post :navigation_click_log
            end
          end
          resources :commons do
            collection do
              get :client_log
              get :location
              get :location_test
              get :routes
              post :upload_routes
            end
          end
          resources :users do
            collection do
              post :update_profile
              post :operation_log
              post :update_username
              post :update_password
              post :share
              post :checkin
              post :offline
              get :profile
              post :push_proxy_message
              post :proxy_connection_logs
              post :renew
              post :bind_promo_code
              post :set_auto_renew
            end
          end
          resources :nodes do
            collection do
              post :connect
              post :connection_log
              post :check
              get :customize_lines
              post :customize_connect
              get :test_speed_ip_list
              post :node_test_request_speed_logs
              post :node_test_download_speed_logs
              get :test_speed_global_config
            end
          end
          resources :plans do
            collection do
              get :iap
              post :verify_iap
              post :verify_google_pay
              post :comsunny_order
              post :comsunny_order_qrcode
              get :transaction_status
              get :transaction_logs
              get :consumption_logs
              get :payment_methods
            end
          end
          resources :feedbacks do
            collection do
              post :mark_as_read
              post :reply
              get :replies
            end
          end
          resources :system_enums
          resources :notices
          resources :dynamic_servers
          resources :failed_logs do
            collection do
              post :connection_failed
              post :signin_failed
              post :no_operation
              post :dynamic_server_failed
              post :proxy_connection_failed
            end
          end
          resources :help_manuals
          resources :logs do
            collection do
              post :client_connection_logs
              post :client_debug_logs
            end
          end
          resources :websites do
            collection do
              post :website_navigation_click_log
            end
          end
          resources :customer_services
          resources :shares, only: [] do
            collection do
              get :templates
            end
          end
          resources :globals, only: [:index]
          resources :rescue_domains, only: [:list] do
            collection do
              get :list
            end
          end
        end

        namespace :v2 do
          post :send_verification_code, to: 'registrations#send_verification_code'
          post :signin, to: 'sessions#create'
          post :signup, to: 'registrations#create'
          resources :plans do
            collection do
              post :verify_iap
              post :comsunny_order
              post :comsunny_order_qrcode
            end
          end
          resources :nodes do
            collection do
              post :connect
            end
          end
          resources :feedbacks
          resources :commons do
            collection do
              get :routes
            end
          end
          resources :users do
            collection do
              post :update_username
              post :share
            end
          end
          resources :websites
          resources :notices, only: [:index]
        end

        namespace :v3 do
          post :signin, to: 'sessions#create'
          post :signup, to: 'registrations#create'
          resources :plans do
            collection do
              post :comsunny_order
            end
          end
          resources :nodes
          resources :commons do
            collection do
              get :routes
            end
          end
        end

        namespace :v4 do
          post :signin, to: 'sessions#create'
          resources :plans do
            collection do
              post :comsunny_order
            end
          end
        end

      end

    end
  end
end

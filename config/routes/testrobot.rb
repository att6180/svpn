Rails.application.routes.draw do

  namespace :api, defaults: {format: :json} do
    if [1, 2].include?(CONFIG.server_type)

      # testrobot
      namespace :testrobot do
        namespace :v1 do

          resources :users, only: [] do
            collection do
              post :register_self
            end
          end

          resources :nodes do
            collection do
              get :test_data
              post :switch_status
              post :record_switch_status
            end
          end

          resources :logs do
            collection do
              post :test_logs
            end
          end

        end

        namespace :v2 do
          resources :nodes do
            collection do
              get :test_data
            end
          end
        end

      end

    end
  end
end

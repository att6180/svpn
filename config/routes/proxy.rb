Rails.application.routes.draw do

  namespace :api, defaults: {format: :json} do
    if [1, 2].include?(CONFIG.server_type)

      # proxy server api
      namespace :proxy do
        namespace :v1 do
          post :authenticate, to: 'sessions#authenticate'
          get :remote_ip, to: 'nodes#remote_ip'
          get :proxy_remote_ip, to: 'nodes#proxy_remote_ip'
          post :test, to: 'sessions#test'
          resources :users do
            collection do
              get :client_log
              post :record_bytes
              post :inspect_expiration
            end
          end
          resources :nodes do
            collection do
              post :record_online_users
              post :record_detail
              get :limit_speed
              get :proxy_black_list
              get :proxy_list
              get :shunt_list
              get :capture_list
              get :test_address
            end
          end
          #resources :connection_logs, only: [:create]
        end
      end

    end
  end
end

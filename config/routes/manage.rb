Rails.application.routes.draw do

  namespace :api, defaults: {format: :json} do
    if [1, 2].include?(CONFIG.server_type)

      # manage api
      namespace :manage do
        namespace :v1 do
          post :auth, to: 'sessions#create'
          resources :sessions, only: [:create]
          resources :user_groups
          resources :users, only: [] do
            collection do
              get :stat_info
              get :base_list
              get :hardware_stat
              get :hardware_stat_details
              get :operator_stat
              get :operator_stat_details
              get :status_stat
              get :status_stat_details
              get :share_stat
              get :share_users_stat_details
              get :share_count_stat_details
              post :batch_reward_coins
              get :area_stat
              post :batch_company_user_renew
              get :checkin_stat
            end
          end
          resources :user_details do
            member do
              get :base_info
              get :signin_logs
              get :connection_logs
              get :devices
              get :operation_logs
              get :access_logs
              get :transaction_logs
              get :consumption_logs
              get :profile
              get :server_connection_logs
              get :promo_logs
              get :change_logs
              post :update_profile
              get :node_test_request_speed_logs
              get :node_test_download_speed_logs
            end
            collection do
              post :cancel_monthly
            end
          end
          resources :new_user_stats do
            collection do
              get :hour_index
              get :day_chart
              get :day_index
              get :day_details
              get :month_chart
              get :month_index
              get :month_details
            end
          end
          resources :active_user_stats do
            collection do
              get :hour_index
              get :day_chart
              get :day_index
              get :day_details
              get :month_chart
              get :month_index
              get :month_details
              get :month_period_index
            end
          end
          resources :non_active_users_stats
          resources :user_retention_rates do
            collection do
              get :chart
              get :details
              get :retention_details
            end
          end
          resources :user_week_retention_rates do
            collection do
              get :chart
            end
          end
          resources :user_month_retention_rates do
            collection do
              get :chart
            end
          end
          resources :user_operation_stats do
            collection do
              get :user_details
              get :visit_details
            end
          end
          resources :node_types
          resources :node_regions
          resources :node_isps
          resources :node_icps
          resources :node_areas
          resources :node_diversions
          resources :nodes do
            collection do
              get :online_users
              get :online_details
              get :details
              get :region_details
              get :region_user_details
              get :online_users_stat
              get :online_users_stat_details
              get :online_users_stat_second_details
              get :proxy_server_status_stat
              get :proxy_server_status_stat_chart
              get :proxy_server_status_stat_details
              post :batch_update_status
              post :batch_update_ip
            end
          end
          resources :proxy_test_domains
          resources :plans
          resources :plan_templates
          resources :plan_template_items
          resources :plan_client_tabs
          resources :plan_versions
          resources :feedbacks, only: [:index, :show, :update] do
            collection do
              get :shortcut_replies
            end
            member do
              post :reply
              get :replies
            end
          end
          resources :feedback_shortcut_replies
          resources :feedback_shortcut_reply_categories
          resources :user_status_logs
          resources :user_connection_logs do
            collection do
              get :user_details
              get :visit_details
              get :domain_region_details
              get :domain_country_details
            end
          end
          resources :node_connection_logs do
            collection do
              get :count_scope_index
              get :count_scope_details
              get :traffic_day_stat
            end
          end
          resources :node_switch_logs do
            collection do
              get :status_index
              get :collects
            end
          end
          resources :system_enums do
            collection do
              get :group_index
            end
          end
          resources :system_enum_relations
          resources :domain_enums do
            collection do
              post :quick_update
            end
          end
          resources :admins do
            member do
              post :disable
              post :enable
            end
            collection do
              get :operation_logs
              get :coin_operation_logs
              get :user_expired_reward_log
            end
          end
          resources :admin_roles
          resources :user_signin_failed_logs do
            collection do
              get :chart
              get :detail
            end
          end
          resources :connection_failed_logs do
            collection do
              get :chart
              get :detail
            end
          end
          resources :user_no_operation_logs do
            collection do
              get :chart
              get :detail
            end
          end
          resources :transaction_logs do
            collection do
              get :users_count_collects
              get :details
              get :details_chart
              get :date_details
              get :recharge_amount_details
              get :recharge_user_details
              get :consume_coins_details
              get :collects
              get :payment_method_collects
              get :transaction_fix_index
              get :transaction_fix
            end
          end
          resources :transaction_osmosis_stats do
            collection do
              get :index
              get :chart
              get :details
              get :retention_details
            end
          end
          resources :transaction_type_stats
          resources :dynamic_servers do
            collection do
              post :push
              get :status
            end
            member do
              get :status_details
            end
          end
          resources :user_signin_logs do
            collection do
              get :region_stat
              get :period_stat_chart
              get :period_stat
            end
          end
          resources :connection_log_filters
          resources :system_settings do
            collection do
              post :upload_routes
            end
          end
          resources :service_settings do
            collection do
              post :update_multiple
              post :update_payment_multiple
            end
          end
          resources :channel_settings
          resources :channel_version_stats do
            collection do
              get :signup_stat
              get :signin_stat
            end
          end
          resources :help_manuals
          resources :client_connection_logs
          resources :website_navigations
          resources :anti_brush_configs, except: [:show] do
            member do
              post :on_off
            end
          end
          resources :waf_settings, except: [:show]
          resources :splash_screen_ads do
            collection do
              post :update_splash_screen_settings
              get :all
            end
          end
          resources :splash_screen_ad_stats do
            collection do
              get :hour_index
              get :day_index
              get :month_index
            end
          end
          resources :splash_screen_ad_click_stats, only: [] do
            collection do
              get :hour_index
            end
          end
          resources :popup_ads do
            collection do
              post :update_settings
            end
          end
          resources :popup_ad_stats do
            collection do
              get :hour_index
              get :day_index
              get :month_index
            end
          end
          resources :popup_ad_click_stats, only: [] do
            collection do
              get :hour_index
            end
          end
          resources :excels, only: [] do
            collection do
              get :download
              post :upload
            end
          end
          resources :operation_summarys do
            collection do
              get :day_index
              get :month_index
            end
          end
          resources :customer_services, except: [:show]
          resources :share_templates do
            collection do
              post :update_share_template_settings
            end
          end
          resources :navigation_ads
          resources :website_navigation_click_stats do
            collection do
              get :hour_index
              get :day_index
            end
          end
          resources :navigation_ad_click_stats do
            collection do
              get :ad_list
              get :hour_index
              get :day_index
            end
          end
          resources :notices
          resources :user_taps
          resources :rescue_domains
        end
      end

    end
  end
end

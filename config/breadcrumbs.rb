crumb :root do
  link "Home", "javascript:;"
end
crumb :base_user do
  link "用户基础信息", 'javascript:;'
end
crumb :base_user_list do
  link "用户列表", base_list_admin_users_path
  parent :base_user
end

crumb :base_user_stat do
  link "统计", "javascript:;"
  parent :base_user
end

crumb :base_user_hardware_stat do
  link "硬件统计", hardware_stat_admin_users_path
  parent :base_user_stat
end

crumb :base_user_operator_stat do
  link "运营商统计", operator_stat_admin_users_path
  parent :base_user_stat
end

crumb :base_user_status_stat do
  link "状态统计", status_stat_admin_users_path
  parent :base_user_stat
end

crumb :node_parent do
  link "节点管理", 'javascript:;'
end
crumb :node_region do
  link "区域管理", admin_node_regions_path
  parent :node_parent
end
crumb :node do
  link "节点管理", admin_nodes_path
  parent :node_parent
end

crumb :setting do
  link "配置管理", "javascript:;"
end
crumb :user do
  link "用户管理", "javascript:;"
  parent :setting
end
crumb :user_feedback do
  link "用户反馈", admin_feedbacks_path
  parent :user
end

crumb :notice do
  link "公告管理", admin_notices_path
  parent :setting
end

crumb :user_status_log do
  link "封号管理", admin_user_status_logs_path
  parent :user
end
#crumb :user_base_statistics do
  #"统计"
  #parent :user_base
#end
#crumb :user_base_statistics_hardware do
  #"硬件统计"
  #parent :user_base_statistics
#end
#crumb :user_base_statistics_operator do
  #"运营商统计"
  #parent :user_base_statistics
#end
#crumb :user_base_statistics_status do
  #"用户状态统计"
  #parent :user_base_statistics
#end
#crumb :user_base_statistics_share do
  #"分享统计"
  #parent :user_base_statistics
#end

# crumb :projects do
#   link "Projects", projects_path
# end

# crumb :project do |project|
#   link project.name, project_path(project)
#   parent :projects
# end

# crumb :project_issues do |project|
#   link "Issues", project_issues_path(project)
#   parent :project, project
# end

# crumb :issue do |issue|
#   link issue.title, issue_path(issue)
#   parent :project_issues, issue.project
# end

# If you want to split your breadcrumbs configuration over multiple files, you
# can create a folder named `config/breadcrumbs` and put your configuration
# files there. All *.rb files (e.g. `frontend.rb` or `products.rb`) in that
# folder are loaded and reloaded automatically when you change them, just like
# this file (`config/breadcrumbs.rb`).

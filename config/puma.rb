# frozen_string_literal: true

app_root = "/home/app/svpn"
daemonize false
port 7000
environment "production"
workers 2
threads 4,8
preload_app!

on_worker_boot do
  ActiveSupport.on_load(:active_record) do
    ActiveRecord::Base.establish_connection
  end
end

before_fork do
  ActiveRecord::Base.connection_pool.disconnect!
end

plugin :tmp_restart

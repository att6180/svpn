# https://github.com/ntamvl/rails_5_api_tutorial
# http://ntam.me/building-the-perfect-rails-5-api-only-app/
class Swagger::Docs::Config
  def self.transform_path(path, api_version)
    # Make a distinction between the APIs and API documentation paths.
    "manage_api_docs/#{path}"
  end
end

Swagger::Docs::Config.base_api_controller = Api::ManageApiController

Swagger::Docs::Config.register_apis({
  '1.0' => {
    #api_extension_type: :json,
    controller_base_path: '',
    api_file_path: 'public/manage_api_docs',
    base_path: CONFIG.base_url,
    parent_controller: Api::Manage::V1::ApiController,
    clean_directory: true
  }
})

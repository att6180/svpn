DB_LOG = YAML::load(ERB.new(File.read(Rails.root.join("config","database_log.yml"))).result)[Rails.env]
DB_MANAGE = YAML::load(ERB.new(File.read(Rails.root.join("config","database_manage.yml"))).result)[Rails.env]
DB_MISC = YAML::load(ERB.new(File.read(Rails.root.join("config","database_misc.yml"))).result)[Rails.env]

# slave1主库，用于热备，并用来在跑定时任务时读取数据
slave1 = Rails.root.join("config","database_slave1.yml")
if File.exists?(slave1)
  DB_SLAVE1 = YAML::load(ERB.new(File.read(slave1)).result)[Rails.env]
end
# slave1日志库，用于热备，并用来在跑定时任务时读取数据
log_slave1 = Rails.root.join("config","database_log_slave1.yml")
if File.exists?(log_slave1)
  DB_LOG_SLAVE1 = YAML::load(ERB.new(File.read(log_slave1)).result)[Rails.env]
end

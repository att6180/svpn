module Ext
  module DelegateScope
    def delegate_scope(*scope_names, to:, source: :name)
      klass = reflect_on_association(to).klass

      scope_names.each do |scope_name|
        name = source == :name ? scope_name : source
        scope scope_name, delegate_scope_for(to, klass, name)
      end
    end

    private

    def delegate_scope_for(to, klass, name)
      ->(*args) { joins(to).merge(klass.public_send(name, *args)) }
    end
  end
end

# delegate_scope :by_admins, to: :user, source: :admin
ActiveRecord::Base.extend(Ext::DelegateScope)

user  nginx;
worker_processes  auto;

events {
    worker_connections  1024;
}

http {
    include       /etc/nginx/mime.types;
    default_type  application/octet-stream;

    log_format main '{ "@timestamp": "$time_iso8601", '
                   '"remote_addr": "$remote_addr", '
                   '"remote_user": "$remote_user", '
                   '"body_bytes_sent": "$body_bytes_sent", '
                   '"request_time": "$request_time", '
                   '"status": "$status", '
                   '"request": "$request", '
                   '"request_method": "$request_method", '
                   '"http_referrer": "$http_referer", '
                   '"http_x_forwarded_for": "$http_x_forwarded_for", '
                   '"http_user_agent": "$http_user_agent" }';

    sendfile        on;

    keepalive_timeout  65;

    gzip  on;

    upstream svpn {
      server app:7000 fail_timeout=0;
      keepalive 3;
    }

    server {
        listen 80;
        server_name 192.168.3.11;
        root /etc/nginx/public;
        proxy_set_header  X-Real-IP   $remote_addr;

        error_log stderr;
        access_log /dev/stdout main;
        # error_log /var/log/nginx/error.log ;
        # access_log /var/log/nginx/access.log ;

        set $realip $HTTP_X_FORWARDED_FOR;
        if ($host = '192.168.3.11') {
            set   $realip  $remote_addr;
        }
        underscores_in_headers on;
        if ($http_x_real_ip = ""){
            set $http_x_real_ip $remote_addr;
        }

        location ~* ^/assets/ {
            gzip_static on;
            # Per RFC2616 - 1 year maximum expiry
            expires max;
            add_header Cache-Control public;
        }

        location @svpn {
            proxy_pass http://svpn;
            proxy_redirect off;
            proxy_read_timeout 5m;

            proxy_set_header   Host             $host;
            proxy_set_header   X-Real-IP        $remote_addr;
            proxy_set_header   X-Forwarded-For  $proxy_add_x_forwarded_for;
            proxy_set_header X-Forwarded-Proto $scheme;
        }

        location ~* \.(jpg|jpeg|gif|png|css|js|ttf|svg|woff|eot|woff2|mp3)$ {
            expires max;
            access_log off;
            try_files $uri @svpn;
        }
        try_files $uri @svpn;
    }

}

# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20190907085510) do

  create_table "channel_settings", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb4" do |t|
    t.string   "app_channel",               null: false
    t.string   "app_version",               null: false
    t.text     "settings",    limit: 65535
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
    t.index ["app_channel", "app_version"], name: "index_channel_settings_on_app_channel_and_app_version", unique: true, using: :btree
  end

  create_table "connection_log_filters", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci" do |t|
    t.string   "rule"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["rule"], name: "index_connection_log_filters_on_rule", using: :btree
  end

  create_table "customer_services", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci" do |t|
    t.string   "name",        limit: 200
    t.string   "potato_url",  limit: 999
    t.boolean  "status",                  default: true
    t.integer  "alloc_count",             default: 0
    t.datetime "created_at",                             null: false
    t.datetime "updated_at",                             null: false
  end

  create_table "device_share_codes", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci" do |t|
    t.integer  "user_id"
    t.string   "sdid",       limit: 32, null: false
    t.string   "uuid",       limit: 36
    t.integer  "apiid",                              comment: "api版本号(主要用于android客户端)"
    t.datetime "created_at",            null: false
    t.datetime "updated_at",            null: false
    t.index ["sdid"], name: "index_device_share_codes_on_sdid", unique: true, using: :btree
    t.index ["user_id"], name: "index_device_share_codes_on_user_id", using: :btree
  end

  create_table "devices", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb4" do |t|
    t.string   "uuid"
    t.string   "name"
    t.string   "model"
    t.string   "platform"
    t.string   "system_version"
    t.string   "operator"
    t.string   "net_env"
    t.string   "app_version"
    t.string   "app_version_number"
    t.string   "app_channel"
    t.string   "proxy_session_token"
    t.boolean  "username_changed",     default: false
    t.integer  "last_changed_user_id"
    t.boolean  "password_changed",     default: false
    t.datetime "created_at",                           null: false
    t.datetime "updated_at",                           null: false
    t.integer  "first_user_id",                        null: false
    t.integer  "last_user_id",                         null: false
    t.index ["created_at"], name: "index_devices_on_created_at", using: :btree
    t.index ["uuid"], name: "index_devices_on_uuid", unique: true, using: :btree
  end

  create_table "devices_users", id: false, force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb4" do |t|
    t.integer "user_id"
    t.integer "device_id"
    t.index ["device_id"], name: "index_devices_users_on_device_id", using: :btree
    t.index ["user_id"], name: "index_devices_users_on_user_id", using: :btree
  end

  create_table "domain_enums", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb4" do |t|
    t.string   "domain"
    t.string   "description"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
    t.index ["domain"], name: "index_domain_enums_on_domain", using: :btree
  end

  create_table "dynamic_servers", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb4" do |t|
    t.string   "url"
    t.string   "region"
    t.integer  "priority",     default: 0
    t.boolean  "is_enabled",   default: false
    t.datetime "created_at",                   null: false
    t.datetime "updated_at",                   null: false
    t.integer  "failed_count", default: 0
    t.index ["url"], name: "index_dynamic_servers_on_url", using: :btree
  end

  create_table "exception_tracks", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci" do |t|
    t.string   "title"
    t.text     "body",       limit: 4294967295
    t.datetime "created_at",                    null: false
    t.datetime "updated_at",                    null: false
  end

  create_table "feedback_logs", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb4" do |t|
    t.integer  "feedback_id"
    t.string   "memberable_type"
    t.string   "memberable_id"
    t.text     "content",         limit: 65535
    t.datetime "created_at",                    null: false
    t.datetime "updated_at",                    null: false
    t.index ["memberable_type", "memberable_id"], name: "index_feedback_logs_on_memberable_type_and_memberable_id", using: :btree
  end

  create_table "feedback_shortcut_replies", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb4" do |t|
    t.integer  "feedback_shortcut_reply_category_id"
    t.text     "content",                             limit: 65535
    t.integer  "index_id",                                          default: 0
    t.datetime "created_at",                                                    null: false
    t.datetime "updated_at",                                                    null: false
    t.index ["index_id"], name: "index_feedback_shortcut_replies_on_index_id", using: :btree
  end

  create_table "feedback_shortcut_reply_categories", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb4" do |t|
    t.string   "name"
    t.integer  "sort_id",    default: 0
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
    t.index ["sort_id"], name: "index_feedback_shortcut_reply_categories_on_sort_id", using: :btree
  end

  create_table "feedbacks", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb4" do |t|
    t.integer  "user_id"
    t.integer  "feedback_type",                    default: 0
    t.string   "app_version"
    t.string   "app_version_number"
    t.string   "app_channel"
    t.text     "content",            limit: 65535
    t.string   "email"
    t.string   "qq"
    t.boolean  "is_processed",                     default: false
    t.datetime "processed_at"
    t.text     "reply_content",      limit: 65535
    t.boolean  "has_read",                         default: false
    t.datetime "has_read_at"
    t.datetime "created_at",                                       null: false
    t.datetime "updated_at",                                       null: false
    t.integer  "status",                           default: 0
    t.datetime "status_updated_at"
    t.boolean  "has_read2",                        default: false,              comment: "for new feedback"
    t.datetime "has_read_at2",                                                  comment: "for new feedback"
    t.index ["created_at"], name: "index_feedbacks_on_created_at", using: :btree
    t.index ["feedback_type", "app_channel", "app_version", "status", "status_updated_at", "updated_at"], name: "feedbacks_faassu", using: :btree
    t.index ["feedback_type"], name: "index_feedbacks_on_feedback_type", using: :btree
    t.index ["is_processed"], name: "index_feedbacks_on_is_processed", using: :btree
    t.index ["processed_at"], name: "index_feedbacks_on_processed_at", using: :btree
    t.index ["status", "updated_at"], name: "index_feedbacks_on_status_and_updated_at", using: :btree
    t.index ["user_id", "feedback_type", "status", "updated_at"], name: "feedbacks_ufsu", using: :btree
    t.index ["user_id"], name: "index_feedbacks_on_user_id", using: :btree
  end

  create_table "help_manuals", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb4" do |t|
    t.string   "platform"
    t.string   "app_version"
    t.string   "title"
    t.text     "content",            limit: 65535
    t.datetime "created_at",                       null: false
    t.datetime "updated_at",                       null: false
    t.string   "app_version_number"
  end

  create_table "makeup_users", primary_key: "user_id", id: :bigint, unsigned: true, force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.integer "status", limit: 1, default: 0, null: false
  end

  create_table "navigation_ads", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci" do |t|
    t.integer  "priority"
    t.string   "name"
    t.string   "image_url"
    t.string   "link_url"
    t.boolean  "is_enabled"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "node_additions", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb4" do |t|
    t.integer  "node_id"
    t.bigint   "max_bandwidth",                              default: 0
    t.decimal  "current_bandwidth",  precision: 8, scale: 5, default: "0.0"
    t.decimal  "cpu_percent",        precision: 4, scale: 1, default: "0.0"
    t.decimal  "memory_percent",     precision: 4, scale: 1, default: "0.0"
    t.bigint   "transfer",                                   default: 0
    t.bigint   "network_speed_up",                           default: 0
    t.bigint   "network_speed_down",                         default: 0
    t.datetime "created_at",                                                 null: false
    t.datetime "updated_at",                                                 null: false
    t.integer  "disk_percent",                               default: 0
    t.index ["node_id"], name: "index_node_additions_on_node_id", unique: true, using: :btree
  end

  create_table "node_areas", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci" do |t|
    t.string   "name",       limit: 500
    t.integer  "sequence",               default: 0
    t.string   "abbr"
    t.integer  "parent_id",              default: 0
    t.string   "type"
    t.datetime "created_at",                         null: false
    t.datetime "updated_at",                         null: false
  end

  create_table "node_diversions", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci" do |t|
    t.string   "province",       limit: 50
    t.string   "city",           limit: 50
    t.integer  "node_region_id"
    t.integer  "node_type_id"
    t.boolean  "is_enabled",                default: true
    t.datetime "created_at",                               null: false
    t.datetime "updated_at",                               null: false
  end

  create_table "node_icps", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci" do |t|
    t.integer  "tag"
    t.string   "isps"
    t.boolean  "is_enabled", default: true
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
  end

  create_table "node_isps", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci" do |t|
    t.string   "code",       limit: 100
    t.string   "name"
    t.boolean  "is_enabled",             default: true
    t.datetime "created_at",                            null: false
    t.datetime "updated_at",                            null: false
  end

  create_table "node_regions", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb4" do |t|
    t.string   "name"
    t.integer  "node_type_id"
    t.integer  "node_country_id"
    t.integer  "node_continent_id"
    t.integer  "sequence",                      default: 0
    t.string   "abbr"
    t.integer  "nodes_count",                   default: 0
    t.boolean  "is_enabled",                    default: true
    t.datetime "created_at",                                   null: false
    t.datetime "updated_at",                                   null: false
    t.string   "code",              limit: 200
    t.integer  "isp_id"
    t.boolean  "is_domestic"
    t.string   "description"
    t.index ["created_at"], name: "index_node_regions_on_created_at", using: :btree
    t.index ["node_type_id"], name: "index_node_regions_on_node_type_id", using: :btree
  end

  create_table "node_types", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb4" do |t|
    t.string   "name"
    t.string   "code",              limit: 100
    t.integer  "level",                           default: 0
    t.integer  "user_group_id"
    t.integer  "expense_coins",                   default: 0
    t.boolean  "can_be_monthly",                  default: true
    t.integer  "times_for_monthly",               default: 0
    t.boolean  "is_enabled",                      default: true
    t.datetime "created_at",                                     null: false
    t.datetime "updated_at",                                     null: false
    t.integer  "limit_speed_up",                  default: 0
    t.integer  "limit_speed_down",                default: 0
    t.text     "description",       limit: 65535
    t.index ["user_group_id"], name: "index_node_types_on_user_group_id", using: :btree
  end

  create_table "nodes", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb4" do |t|
    t.integer  "node_type_id"
    t.integer  "node_region_id"
    t.string   "name"
    t.string   "url"
    t.string   "https_url",                                          comment: "https连接地址"
    t.integer  "used_count",            default: 0
    t.integer  "status",                default: 1
    t.string   "description"
    t.integer  "connections_count",     default: 0
    t.integer  "max_connections_count", default: 0
    t.integer  "port"
    t.string   "password"
    t.string   "encrypt_method"
    t.datetime "created_at",                            null: false
    t.datetime "updated_at",                            null: false
    t.boolean  "is_domestic",           default: false
    t.integer  "level",                 default: 1
    t.integer  "node_isp_id"
    t.integer  "node_region_out_id"
    t.integer  "types",                 default: 1
    t.index ["created_at"], name: "index_nodes_on_created_at", using: :btree
    t.index ["https_url"], name: "index_nodes_on_https_url", unique: true, using: :btree
    t.index ["name"], name: "index_nodes_on_name", using: :btree
    t.index ["node_region_id"], name: "index_nodes_on_node_region_id", using: :btree
    t.index ["node_type_id"], name: "index_nodes_on_node_type_id", using: :btree
    t.index ["url"], name: "index_nodes_on_url", using: :btree
  end

  create_table "notices", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb4" do |t|
    t.integer  "admin_id"
    t.text     "content",    limit: 65535
    t.boolean  "is_enabled",               default: false
    t.datetime "created_at",                               null: false
    t.datetime "updated_at",                               null: false
    t.index ["admin_id"], name: "index_notices_on_admin_id", using: :btree
  end

  create_table "plans", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb4" do |t|
    t.string   "name"
    t.integer  "coins",                                                    default: 0
    t.decimal  "price",                            precision: 8, scale: 2
    t.integer  "present_coins",                                            default: 0
    t.boolean  "is_enabled",                                               default: true
    t.boolean  "is_iap",                                                   default: false
    t.string   "iap_id"
    t.text     "description",        limit: 65535
    t.datetime "created_at",                                                               null: false
    t.datetime "updated_at",                                                               null: false
    t.boolean  "is_audit",                                                 default: false
    t.string   "platform"
    t.string   "app_version"
    t.string   "app_version_number"
    t.boolean  "is_regular_time",                                          default: false
    t.integer  "time_type",                                                default: 0
    t.integer  "node_type_id"
  end

  create_table "proxy_test_domains", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb4" do |t|
    t.string   "domain"
    t.integer  "failure_times", default: 0
    t.datetime "created_at",                    null: false
    t.datetime "updated_at",                    null: false
    t.boolean  "is_domestic",   default: false
  end

  create_table "rescue_domains", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci" do |t|
    t.string   "domain",                                                            comment: "域名"
    t.integer  "domain_type",                                                       comment: "0-owner, 1-thirdparty"
    t.decimal  "response_time", precision: 8, scale: 2,                             comment: "响应时长"
    t.boolean  "is_enabled",                            default: true,              comment: "状态 0|禁用, 1|开启"
    t.datetime "created_at",                                           null: false
    t.datetime "updated_at",                                           null: false
  end

  create_table "settings", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb4" do |t|
    t.string   "var",                      null: false
    t.text     "value",      limit: 65535
    t.integer  "thing_id"
    t.string   "thing_type", limit: 30
    t.datetime "created_at"
    t.datetime "updated_at"
    t.index ["thing_type", "thing_id", "var"], name: "index_settings_on_thing_type_and_thing_id_and_var", unique: true, using: :btree
  end

  create_table "share_templates", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci" do |t|
    t.integer  "sequence",                                    default: 0,                          null: false
    t.string   "platform_name",                 limit: 99,                                         null: false
    t.text     "copy_link_text",                limit: 65535,                                      null: false
    t.string   "text_title",                                                                       null: false
    t.text     "text_content",                  limit: 65535,                                      null: false
    t.string   "text_url",                                                                         null: false
    t.text     "image_text_content",            limit: 65535,                                      null: false
    t.string   "image_promo_text",                                                                 null: false
    t.string   "landing_page_qrcode_url",       limit: 500,                                        null: false
    t.string   "background_image_download_url", limit: 500,                                        null: false
    t.datetime "background_image_update_at",                  default: -> { "CURRENT_TIMESTAMP" }, null: false
    t.datetime "created_at",                                                                       null: false
    t.datetime "updated_at",                                                                       null: false
    t.index ["platform_name"], name: "index_share_templates_on_platform_name", unique: true, using: :btree
  end

  create_table "splash_screen_ads", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci" do |t|
    t.string   "name",                                   null: false
    t.integer  "duration",               default: 0
    t.integer  "time_priority",          default: 1
    t.integer  "date_type",              default: 0
    t.integer  "time_type",              default: 0
    t.datetime "start_at"
    t.datetime "end_at"
    t.string   "image_url"
    t.string   "link_url"
    t.boolean  "is_enabled",             default: false
    t.bigint   "selected_count",         default: 0
    t.bigint   "imporessions_count",     default: 0
    t.datetime "last_modification_time"
    t.integer  "ad_type",                default: 1
    t.datetime "created_at",                             null: false
    t.datetime "updated_at",                             null: false
    t.integer  "open_link_type",         default: 1
    t.string   "app_channel"
    t.string   "app_version"
    t.index ["name"], name: "index_splash_screen_ads_on_name", unique: true, using: :btree
    t.index ["start_at", "end_at", "time_type", "is_enabled"], name: "ssads_seti", using: :btree
  end

  create_table "system_enum_relations", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci" do |t|
    t.integer  "platform_id"
    t.integer  "channel_account_id"
    t.integer  "package_id"
    t.string   "channel"
    t.string   "version"
    t.datetime "created_at",                       null: false
    t.datetime "updated_at",                       null: false
    t.text     "description",        limit: 65535
    t.index ["channel", "version"], name: "index_system_enum_relations_on_channel_and_version", unique: true, using: :btree
  end

  create_table "system_enums", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci" do |t|
    t.integer  "enum_type",              default: 0
    t.string   "name",       limit: 255
    t.datetime "created_at",                         null: false
    t.datetime "updated_at",                         null: false
    t.index ["enum_type"], name: "index_system_enums_on_enum_type", using: :btree
    t.index ["name", "enum_type"], name: "index_system_enums_on_name_and_enum_type", unique: true, using: :btree
  end

  create_table "system_settings", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci" do |t|
    t.string   "platform",           limit: 255
    t.string   "app_version",        limit: 255
    t.string   "key",                limit: 255
    t.text     "value",              limit: 65535
    t.string   "description",        limit: 255
    t.datetime "created_at",                                      null: false
    t.datetime "updated_at",                                      null: false
    t.boolean  "is_client",                        default: true
    t.string   "app_version_number",               default: ""
    t.string   "app_channel"
    t.index ["key"], name: "index_system_settings_on_key", using: :btree
    t.index ["platform", "app_version", "app_version_number"], name: "platform_version_number", using: :btree
  end

  create_table "user_additions", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb4" do |t|
    t.integer  "user_id"
    t.bigint   "used_bytes",                 default: 0
    t.datetime "last_connection_at"
    t.integer  "last_connected_node_id"
    t.datetime "last_inspect_expiration_at"
    t.datetime "created_at",                             null: false
    t.datetime "updated_at",                             null: false
    t.index ["last_connection_at"], name: "index_user_additions_on_last_connection_at", using: :btree
    t.index ["user_id"], name: "index_user_additions_on_user_id", unique: true, using: :btree
  end

  create_table "user_groups", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb4" do |t|
    t.string   "name"
    t.integer  "need_coins", default: 0
    t.integer  "level",      default: 0
    t.boolean  "is_enabled", default: true
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
  end

  create_table "user_node_types", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb4" do |t|
    t.integer  "user_id"
    t.integer  "node_type_id"
    t.integer  "status",       default: 0
    t.datetime "expired_at"
    t.integer  "used_count",   default: 0
    t.datetime "created_at",               null: false
    t.datetime "updated_at",               null: false
    t.index ["node_type_id"], name: "index_user_node_types_on_node_type_id", using: :btree
    t.index ["user_id"], name: "index_user_node_types_on_user_id", using: :btree
  end

  create_table "user_proxy_statuses", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb4" do |t|
    t.integer  "user_id"
    t.datetime "last_used_at"
    t.bigint   "total_bytes"
    t.string   "last_url"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
    t.index ["user_id", "last_used_at"], name: "index_user_proxy_statuses_on_user_id_and_last_used_at", using: :btree
    t.index ["user_id", "updated_at"], name: "index_user_proxy_statuses_on_user_id_and_updated_at", using: :btree
    t.index ["user_id"], name: "index_user_proxy_statuses_on_user_id", unique: true, using: :btree
  end

  create_table "user_sessions", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb4" do |t|
    t.integer  "user_id"
    t.integer  "device_id"
    t.string   "token"
    t.datetime "expired_at"
    t.integer  "error_code", default: 0
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
    t.index ["created_at"], name: "index_user_sessions_on_created_at", using: :btree
    t.index ["device_id"], name: "index_user_sessions_on_device_id", using: :btree
    t.index ["user_id", "device_id"], name: "index_user_sessions_on_user_id_and_device_id", unique: true, using: :btree
    t.index ["user_id"], name: "index_user_sessions_on_user_id", using: :btree
  end

  create_table "user_taps", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci" do |t|
    t.integer  "user_id"
    t.integer  "node_id"
    t.boolean  "is_enabled", default: true
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
  end

  create_table "users", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb4" do |t|
    t.integer  "user_group_id"
    t.integer  "area_code"
    t.string   "username"
    t.string   "password"
    t.boolean  "is_enabled",                                                   default: true
    t.string   "email"
    t.string   "email_confirmation_token"
    t.datetime "email_confirmed_at"
    t.string   "wechat"
    t.string   "weibo"
    t.string   "qq"
    t.string   "app_api_token"
    t.integer  "devices_count",                                                default: 0
    t.bigint   "max_bytes",                                                    default: 0
    t.bigint   "limit_bytes",                                                  default: 0
    t.integer  "checkin_days",                                                 default: 0
    t.integer  "last_checkin_days",                                            default: 0
    t.datetime "last_checkin_at"
    t.decimal  "total_payment_amount",                 precision: 8, scale: 2, default: "0.0"
    t.integer  "current_coins",                                                default: 0
    t.integer  "total_coins",                                                  default: 0
    t.integer  "present_coins",                                                default: 0
    t.integer  "total_points",                                                 default: 0
    t.integer  "signin_count",                                                 default: 0
    t.integer  "failed_attempts",                                              default: 0
    t.integer  "reset_password_count",                                         default: 0
    t.integer  "share_count",                                                  default: 0
    t.datetime "current_signin_at"
    t.string   "current_signin_ip"
    t.datetime "last_signin_at"
    t.string   "last_signin_ip"
    t.string   "create_ip"
    t.string   "create_ip_country"
    t.string   "create_ip_province"
    t.string   "create_ip_city"
    t.string   "create_device_uuid",        limit: 40
    t.string   "create_device_name"
    t.string   "create_device_model"
    t.string   "create_platform"
    t.string   "create_system_version"
    t.string   "create_operator"
    t.string   "create_net_env"
    t.string   "create_app_version"
    t.string   "create_app_version_number"
    t.string   "create_app_channel"
    t.datetime "created_at",                                                                   null: false
    t.datetime "updated_at",                                                                   null: false
    t.boolean  "is_online",                                                    default: true
    t.datetime "last_offline_time"
    t.integer  "total_used_coins",                                             default: 0
    t.boolean  "auto_renew",                                                   default: false
    t.string   "promo_code"
    t.integer  "promo_user_id"
    t.integer  "promo_users_count",                                            default: 0
    t.integer  "promo_coins_count",                                            default: 0
    t.string   "binded_promo_code",                                            default: ""
    t.index ["app_api_token"], name: "index_users_on_app_api_token", using: :btree
    t.index ["create_app_channel"], name: "index_users_on_create_app_channel", using: :btree
    t.index ["create_app_version"], name: "index_users_on_create_app_version", using: :btree
    t.index ["create_device_uuid", "created_at"], name: "index_users_on_create_device_uuid_and_created_at", unique: true, using: :btree
    t.index ["create_device_uuid"], name: "index_users_on_create_device_uuid", using: :btree
    t.index ["create_ip_country"], name: "index_users_on_create_ip_country", using: :btree
    t.index ["create_ip_province"], name: "index_users_on_create_ip_province", using: :btree
    t.index ["created_at"], name: "index_users_on_created_at", using: :btree
    t.index ["current_signin_at", "create_app_channel", "create_app_version"], name: "users_ccc", using: :btree
    t.index ["email"], name: "index_users_on_email", using: :btree
    t.index ["last_signin_at"], name: "index_users_on_last_signin_at", using: :btree
    t.index ["promo_code"], name: "index_users_on_promo_code", unique: true, using: :btree
    t.index ["user_group_id"], name: "index_users_on_user_group_id", using: :btree
    t.index ["username"], name: "index_users_on_username", unique: true, using: :btree
  end

  create_table "users_copy", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb4" do |t|
    t.integer  "user_group_id"
    t.integer  "area_code"
    t.string   "username"
    t.string   "password"
    t.boolean  "is_enabled",                                                   default: true
    t.string   "email"
    t.string   "email_confirmation_token"
    t.datetime "email_confirmed_at"
    t.string   "wechat"
    t.string   "weibo"
    t.string   "qq"
    t.string   "app_api_token"
    t.integer  "devices_count",                                                default: 0
    t.bigint   "max_bytes",                                                    default: 0
    t.bigint   "limit_bytes",                                                  default: 0
    t.integer  "checkin_days",                                                 default: 0
    t.integer  "last_checkin_days",                                            default: 0
    t.datetime "last_checkin_at"
    t.decimal  "total_payment_amount",                 precision: 8, scale: 2, default: "0.0"
    t.integer  "current_coins",                                                default: 0
    t.integer  "total_coins",                                                  default: 0
    t.integer  "present_coins",                                                default: 0
    t.integer  "total_points",                                                 default: 0
    t.integer  "signin_count",                                                 default: 0
    t.integer  "failed_attempts",                                              default: 0
    t.integer  "reset_password_count",                                         default: 0
    t.integer  "share_count",                                                  default: 0
    t.datetime "current_signin_at"
    t.string   "current_signin_ip"
    t.datetime "last_signin_at"
    t.string   "last_signin_ip"
    t.string   "create_ip"
    t.string   "create_ip_country"
    t.string   "create_ip_province"
    t.string   "create_ip_city"
    t.string   "create_device_uuid",        limit: 40
    t.string   "create_device_name"
    t.string   "create_device_model"
    t.string   "create_platform"
    t.string   "create_system_version"
    t.string   "create_operator"
    t.string   "create_net_env"
    t.string   "create_app_version"
    t.string   "create_app_version_number"
    t.string   "create_app_channel"
    t.datetime "created_at",                                                                   null: false
    t.datetime "updated_at",                                                                   null: false
    t.boolean  "is_online",                                                    default: true
    t.datetime "last_offline_time"
    t.integer  "total_used_coins",                                             default: 0
    t.boolean  "auto_renew",                                                   default: false
    t.string   "promo_code"
    t.integer  "promo_user_id"
    t.integer  "promo_users_count",                                            default: 0
    t.integer  "promo_coins_count",                                            default: 0
    t.string   "binded_promo_code",                                            default: ""
    t.index ["app_api_token"], name: "index_users_on_app_api_token", using: :btree
    t.index ["create_app_channel"], name: "index_users_on_create_app_channel", using: :btree
    t.index ["create_app_version"], name: "index_users_on_create_app_version", using: :btree
    t.index ["create_device_uuid", "created_at"], name: "index_users_on_create_device_uuid_and_created_at", unique: true, using: :btree
    t.index ["create_device_uuid"], name: "index_users_on_create_device_uuid", using: :btree
    t.index ["create_ip_country"], name: "index_users_on_create_ip_country", using: :btree
    t.index ["create_ip_province"], name: "index_users_on_create_ip_province", using: :btree
    t.index ["created_at"], name: "index_users_on_created_at", using: :btree
    t.index ["current_signin_at", "create_app_channel", "create_app_version"], name: "users_ccc", using: :btree
    t.index ["email"], name: "index_users_on_email", using: :btree
    t.index ["last_signin_at"], name: "index_users_on_last_signin_at", using: :btree
    t.index ["promo_code"], name: "index_users_on_promo_code", unique: true, using: :btree
    t.index ["user_group_id"], name: "index_users_on_user_group_id", using: :btree
    t.index ["username"], name: "index_users_on_username", unique: true, using: :btree
  end

  create_table "users_copy2", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb4" do |t|
    t.integer  "user_group_id"
    t.integer  "area_code"
    t.string   "username"
    t.string   "password"
    t.boolean  "is_enabled",                                                   default: true
    t.string   "email"
    t.string   "email_confirmation_token"
    t.datetime "email_confirmed_at"
    t.string   "wechat"
    t.string   "weibo"
    t.string   "qq"
    t.string   "app_api_token"
    t.integer  "devices_count",                                                default: 0
    t.bigint   "max_bytes",                                                    default: 0
    t.bigint   "limit_bytes",                                                  default: 0
    t.integer  "checkin_days",                                                 default: 0
    t.integer  "last_checkin_days",                                            default: 0
    t.datetime "last_checkin_at"
    t.decimal  "total_payment_amount",                 precision: 8, scale: 2, default: "0.0"
    t.integer  "current_coins",                                                default: 0
    t.integer  "total_coins",                                                  default: 0
    t.integer  "present_coins",                                                default: 0
    t.integer  "total_points",                                                 default: 0
    t.integer  "signin_count",                                                 default: 0
    t.integer  "failed_attempts",                                              default: 0
    t.integer  "reset_password_count",                                         default: 0
    t.integer  "share_count",                                                  default: 0
    t.datetime "current_signin_at"
    t.string   "current_signin_ip"
    t.datetime "last_signin_at"
    t.string   "last_signin_ip"
    t.string   "create_ip"
    t.string   "create_ip_country"
    t.string   "create_ip_province"
    t.string   "create_ip_city"
    t.string   "create_device_uuid",        limit: 40
    t.string   "create_device_name"
    t.string   "create_device_model"
    t.string   "create_platform"
    t.string   "create_system_version"
    t.string   "create_operator"
    t.string   "create_net_env"
    t.string   "create_app_version"
    t.string   "create_app_version_number"
    t.string   "create_app_channel"
    t.datetime "created_at",                                                                   null: false
    t.datetime "updated_at",                                                                   null: false
    t.boolean  "is_online",                                                    default: true
    t.datetime "last_offline_time"
    t.integer  "total_used_coins",                                             default: 0
    t.boolean  "auto_renew",                                                   default: false
    t.string   "promo_code"
    t.integer  "promo_user_id"
    t.integer  "promo_users_count",                                            default: 0
    t.integer  "promo_coins_count",                                            default: 0
    t.string   "binded_promo_code",                                            default: ""
    t.index ["app_api_token"], name: "index_users_on_app_api_token", using: :btree
    t.index ["create_app_channel"], name: "index_users_on_create_app_channel", using: :btree
    t.index ["create_app_version"], name: "index_users_on_create_app_version", using: :btree
    t.index ["create_device_uuid", "created_at"], name: "index_users_on_create_device_uuid_and_created_at", unique: true, using: :btree
    t.index ["create_device_uuid"], name: "index_users_on_create_device_uuid", using: :btree
    t.index ["create_ip_country"], name: "index_users_on_create_ip_country", using: :btree
    t.index ["create_ip_province"], name: "index_users_on_create_ip_province", using: :btree
    t.index ["created_at"], name: "index_users_on_created_at", using: :btree
    t.index ["current_signin_at", "create_app_channel", "create_app_version"], name: "users_ccc", using: :btree
    t.index ["email"], name: "index_users_on_email", using: :btree
    t.index ["last_signin_at"], name: "index_users_on_last_signin_at", using: :btree
    t.index ["promo_code"], name: "index_users_on_promo_code", unique: true, using: :btree
    t.index ["user_group_id"], name: "index_users_on_user_group_id", using: :btree
    t.index ["username"], name: "index_users_on_username", unique: true, using: :btree
  end

  create_table "users_copy3", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb4" do |t|
    t.integer  "user_group_id"
    t.integer  "area_code"
    t.string   "username"
    t.string   "password"
    t.boolean  "is_enabled",                                                   default: true
    t.string   "email"
    t.string   "email_confirmation_token"
    t.datetime "email_confirmed_at"
    t.string   "wechat"
    t.string   "weibo"
    t.string   "qq"
    t.string   "app_api_token"
    t.integer  "devices_count",                                                default: 0
    t.bigint   "max_bytes",                                                    default: 0
    t.bigint   "limit_bytes",                                                  default: 0
    t.integer  "checkin_days",                                                 default: 0
    t.integer  "last_checkin_days",                                            default: 0
    t.datetime "last_checkin_at"
    t.decimal  "total_payment_amount",                 precision: 8, scale: 2, default: "0.0"
    t.integer  "current_coins",                                                default: 0
    t.integer  "total_coins",                                                  default: 0
    t.integer  "present_coins",                                                default: 0
    t.integer  "total_points",                                                 default: 0
    t.integer  "signin_count",                                                 default: 0
    t.integer  "failed_attempts",                                              default: 0
    t.integer  "reset_password_count",                                         default: 0
    t.integer  "share_count",                                                  default: 0
    t.datetime "current_signin_at"
    t.string   "current_signin_ip"
    t.datetime "last_signin_at"
    t.string   "last_signin_ip"
    t.string   "create_ip"
    t.string   "create_ip_country"
    t.string   "create_ip_province"
    t.string   "create_ip_city"
    t.string   "create_device_uuid",        limit: 40
    t.string   "create_device_name"
    t.string   "create_device_model"
    t.string   "create_platform"
    t.string   "create_system_version"
    t.string   "create_operator"
    t.string   "create_net_env"
    t.string   "create_app_version"
    t.string   "create_app_version_number"
    t.string   "create_app_channel"
    t.datetime "created_at",                                                                   null: false
    t.datetime "updated_at",                                                                   null: false
    t.boolean  "is_online",                                                    default: true
    t.datetime "last_offline_time"
    t.integer  "total_used_coins",                                             default: 0
    t.boolean  "auto_renew",                                                   default: false
    t.string   "promo_code"
    t.integer  "promo_user_id"
    t.integer  "promo_users_count",                                            default: 0
    t.integer  "promo_coins_count",                                            default: 0
    t.string   "binded_promo_code",                                            default: ""
    t.index ["app_api_token"], name: "index_users_on_app_api_token", using: :btree
    t.index ["create_app_channel"], name: "index_users_on_create_app_channel", using: :btree
    t.index ["create_app_version"], name: "index_users_on_create_app_version", using: :btree
    t.index ["create_device_uuid", "created_at"], name: "index_users_on_create_device_uuid_and_created_at", unique: true, using: :btree
    t.index ["create_device_uuid"], name: "index_users_on_create_device_uuid", using: :btree
    t.index ["create_ip_country"], name: "index_users_on_create_ip_country", using: :btree
    t.index ["create_ip_province"], name: "index_users_on_create_ip_province", using: :btree
    t.index ["created_at"], name: "index_users_on_created_at", using: :btree
    t.index ["current_signin_at", "create_app_channel", "create_app_version"], name: "users_ccc", using: :btree
    t.index ["email"], name: "index_users_on_email", using: :btree
    t.index ["last_signin_at"], name: "index_users_on_last_signin_at", using: :btree
    t.index ["promo_code"], name: "index_users_on_promo_code", unique: true, using: :btree
    t.index ["user_group_id"], name: "index_users_on_user_group_id", using: :btree
    t.index ["username"], name: "index_users_on_username", unique: true, using: :btree
  end

  create_table "v2_notices", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci" do |t|
    t.text     "user_names",   limit: 65535
    t.string   "platform"
    t.string   "app_channel"
    t.string   "app_version"
    t.integer  "notice_type"
    t.integer  "open_type"
    t.string   "button_text"
    t.string   "link_url"
    t.text     "alert_text",   limit: 65535
    t.text     "marquee_text", limit: 65535
    t.string   "description"
    t.datetime "created_at",                 null: false
    t.datetime "updated_at",                 null: false
  end

  create_table "v2_plan_client_tabs", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci" do |t|
    t.integer  "node_type_id", default: 0
    t.string   "name"
    t.string   "description"
    t.boolean  "is_recommend"
    t.integer  "status",       default: 1
    t.datetime "created_at",               null: false
    t.datetime "updated_at",               null: false
    t.index ["status"], name: "index_v2_plan_client_tabs_on_status", using: :btree
  end

  create_table "v2_plan_templates", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci" do |t|
    t.string   "name",       default: "0"
    t.integer  "plan_type",  default: 0
    t.integer  "status",     default: 1
    t.datetime "created_at",               null: false
    t.datetime "updated_at",               null: false
    t.index ["status"], name: "index_v2_plan_templates_on_status", using: :btree
  end

  create_table "v2_plan_versions", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci" do |t|
    t.string   "channel"
    t.string   "version"
    t.integer  "audit_template_id"
    t.integer  "domestic_template_id"
    t.integer  "oversea_template_id"
    t.integer  "iap_template_id"
    t.boolean  "show_icon",            default: false
    t.string   "tab_id_sort"
    t.integer  "status",               default: 1
    t.datetime "created_at",                           null: false
    t.datetime "updated_at",                           null: false
    t.index ["status"], name: "index_v2_plan_versions_on_status", using: :btree
  end

  create_table "v2_plans", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci" do |t|
    t.integer  "template_id"
    t.integer  "client_tab_id"
    t.string   "name",                                     default: ""
    t.decimal  "price",           precision: 10, scale: 2, default: "0.0"
    t.string   "currency_symbol"
    t.boolean  "is_iap",                                   default: false
    t.string   "iap_id",                                   default: ""
    t.boolean  "is_regular_time",                          default: false
    t.integer  "time_type",                                default: 0
    t.integer  "node_type_id"
    t.integer  "coins",                                    default: 0
    t.integer  "present_coins",                            default: 0
    t.integer  "sort_weight",                              default: 0
    t.string   "description1",                             default: ""
    t.string   "description2",                             default: ""
    t.string   "description3",                             default: ""
    t.boolean  "is_recommend",                             default: false
    t.integer  "status",                                   default: 1
    t.datetime "created_at",                                               null: false
    t.datetime "updated_at",                                               null: false
    t.index ["client_tab_id"], name: "index_v2_plans_on_client_tab_id", using: :btree
    t.index ["is_regular_time", "time_type", "is_iap", "status"], name: "v2_plans_itis", using: :btree
    t.index ["template_id"], name: "index_v2_plans_on_template_id", using: :btree
  end

  create_table "waf_anti_brush_configs", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci" do |t|
    t.string   "no"
    t.string   "app_channel"
    t.string   "app_version"
    t.string   "app_version_no"
    t.integer  "status",                       default: 0
    t.text     "white_lists",    limit: 65535
    t.text     "black_lists",    limit: 65535
    t.datetime "created_at",                               null: false
    t.datetime "updated_at",                               null: false
  end

  create_table "waf_lists", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci" do |t|
    t.string   "ip",         limit: 50
    t.string   "type"
    t.datetime "created_at",            null: false
    t.datetime "updated_at",            null: false
  end

  create_table "waf_settings", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci" do |t|
    t.integer  "brush_time",                 comment: "防刷时间"
    t.integer  "brush_count",                comment: "防刷个数"
    t.integer  "trigger_count",              comment: "触发个数"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
  end

  create_table "website_navigations", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb4" do |t|
    t.integer  "sort_id",      default: 0
    t.integer  "website_type", default: 0
    t.string   "icon_url"
    t.string   "name"
    t.string   "description"
    t.string   "url"
    t.boolean  "is_domestic",  default: true
    t.datetime "created_at",                  null: false
    t.datetime "updated_at",                  null: false
    t.integer  "nav_type",     default: 1
    t.integer  "info_type",    default: 1
    t.index ["sort_id"], name: "index_website_navigations_on_sort_id", using: :btree
    t.index ["website_type"], name: "index_website_navigations_on_website_type", using: :btree
  end

end

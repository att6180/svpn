class CreateUserNodeTypes < ActiveRecord::Migration[5.0]
  def change
    # 每个用户对应的服务器类型
    create_table :user_node_types do |t|
      # 用户ID
      t.integer :user_id, index: true
      # 服务器类型
      t.integer :node_type_id, index: true
      # 服务器类型状态(0: 按次, 1: 包月)
      t.integer :status, default: 0
      # 服务状态
      # 服务器类型有效期至
      t.datetime :expired_at
      # 使用次数
      t.integer :used_count, default: 0
      t.timestamps
    end
  end
end

class RenameNodeOperatorAgency < ActiveRecord::Migration[5.0]
  def change
    rename_table :node_operators, :node_icps
    rename_table :node_agencies, :node_isps
    rename_column :nodes, :node_agency_id, :node_isp_id
    rename_column :node_icps, :agencys, :isps
  end
end

class AddNodeAgencyIdToNodes < ActiveRecord::Migration[5.0]
  def change
    add_column :nodes, :node_agency_id, :integer
  end
end

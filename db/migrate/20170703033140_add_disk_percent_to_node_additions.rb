class AddDiskPercentToNodeAdditions < ActiveRecord::Migration[5.0]
  def change
    add_column :node_additions, :disk_percent, :integer, default: 0
  end
end

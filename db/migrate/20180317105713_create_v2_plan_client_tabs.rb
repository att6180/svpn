class CreateV2PlanClientTabs < ActiveRecord::Migration[5.0]
  def change
    create_table :v2_plan_client_tabs do |t|
      t.integer :node_type_id, default: 0
      t.string :name
      t.string :description
      t.boolean :is_recommend
      t.integer :status, default: 1
      t.timestamps
    end
    add_index :v2_plan_client_tabs, :status
  end
end

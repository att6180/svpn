class CreateNodeAgencies < ActiveRecord::Migration[5.0]
  def change
    create_table :node_agencies do |t|
      t.string :name
      t.boolean :is_enabled, default: true
      t.timestamps
    end
  end
end

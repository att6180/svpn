class CreateFeedbacks < ActiveRecord::Migration[5.0]
  def change
    create_table :feedbacks do |t|
      t.integer :user_id, index: true
      t.integer :feedback_type, index: true, default: 0
      t.string :app_version, index: true
      t.string :app_version_number
      t.string :app_channel, index: true
      t.text :content
      t.string :email
      t.string :qq
      t.boolean :is_processed, index: true, default: false
      t.datetime :processed_at, index: true
      t.text :reply_content
      t.boolean :has_read, default: false
      t.datetime :has_read_at
      t.timestamps
    end
    add_index :feedbacks, :created_at
  end
end

class AddTimeIndexToFeedbacks < ActiveRecord::Migration[5.0]
  def change
    add_index :feedbacks, [:app_channel, :app_version, :status, :updated_at], name: 'feedbacks_aasu'
    add_index :feedbacks, [:app_version, :status, :updated_at], name: 'feedbacks_avsu'
    add_index :feedbacks, [:app_channel, :status, :updated_at], name: 'feedbacks_acsu'
    add_index :feedbacks, [:status, :updated_at]
    remove_index :feedbacks, :app_channel
    remove_index :feedbacks, :app_version
  end
end

class CreateNotices < ActiveRecord::Migration[5.0]
  def change
    create_table :notices do |t|
      t.integer :admin_id, index: true
      t.text :content
      t.boolean :is_enabled, default: false
      t.timestamps
    end
  end
end

class AddFaultTolerantTimesToNodes < ActiveRecord::Migration[5.0]
  def change
    add_column :nodes, :allow_fault_times, :integer, default: 0
  end
end

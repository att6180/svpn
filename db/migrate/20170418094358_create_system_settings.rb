class CreateSystemSettings < ActiveRecord::Migration[5.0]
  def change
    create_table :system_settings do |t|
      t.string :platform
      t.string :app_version
      t.string :key
      t.string :value
      t.string :description
      t.timestamps
    end
  end
end

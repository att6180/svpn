class AddIsDomesticToProxyTestDomains < ActiveRecord::Migration[5.0]
  def change
    add_column :proxy_test_domains, :is_domestic, :boolean, default: false
  end
end

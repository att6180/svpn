class CreateSystemEnums < ActiveRecord::Migration[5.0]
  def change
    create_table :system_enums do |t|
      t.integer :enum_type, default: 0, index: true
      t.string :name
      t.timestamps
    end
  end
end

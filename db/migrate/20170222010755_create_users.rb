class CreateUsers < ActiveRecord::Migration[5.0]
  def change
    create_table :users do |t|
      # 分组ID
      t.integer :user_group_id, index: true
      # 手机号
      t.string :username
      # 密码
      t.string :password
      # 服务状态
      #t.integer :service_state, default: 1
      # 帐号是否启用
      t.boolean :is_enabled, default: true
      # 邮箱
      t.string :email, index: true
      # 邮箱确认token
      t.string :email_confirmation_token
      # 邮箱确认时间
      t.datetime :email_confirmed_at
      # 微信
      t.string :wechat
      # 微博
      t.string :weibo
      # qq
      t.string :qq
      # 应用API TOKEN
      t.string :app_api_token, index: true

      # 登录的设备数
      t.integer :devices_count, default: 0
      # 已使用流量
      t.integer :used_bytes, limit: 8, default: 0
      # 流量上限
      t.integer :max_bytes, limit: 8, default: 0
      # 速度限制
      t.integer :limit_bytes, limit: 8, default: 0
      # 服务到期时间
      #t.datetime :expired_at
      # 最后连接时间
      t.datetime :last_connection_at
      # 最后连接的服务器
      t.integer :last_connected_node
      # 最后过期检查时间
      t.datetime :last_inspect_expiration_at

      # 绑定设备数
      t.integer :devices_count, default: 0
      # 连续签到时间
      t.integer :checkin_days, default: 0
      # 最后连续签到时间
      t.integer :last_checkin_days, default: 0
      # 最后签到时间
      t.datetime :last_checkin_at
      # 消费总额
      t.decimal :total_payment_amount, precision: 8, scale: 2, default: 0
      # 当前虚拟币数量
      t.integer :current_coins, default: 0
      # 累计虚拟币数量
      t.integer :total_coins, default: 0
      # 赠送的虚拟币数
      t.integer :present_coins, default: 0
      # 帐号积分数
      t.integer :total_points, default: 0

      # 登录次数
      t.integer :signin_count, default: 0
      # 失败尝试次数
      t.integer :failed_attempts, default: 0
      # 重置密码次数
      t.integer :reset_password_count, default: 0
      # 共享次数
      t.integer :share_count, default: 0
      # 当前登录时间
      t.datetime :current_signin_at
      # 当前登录IP
      t.string :current_signin_ip
      # 上次登录时间
      t.datetime :last_signin_at, index: true
      # 上次登录IP
      t.string :last_signin_ip
      # 注册时的IP
      t.string :create_ip
      # 注册时的ip解析
      t.string :create_ip_country
      t.string :create_ip_province
      t.string :create_ip_city

      # uuid
      t.string :create_device_uuid, limit: 40
      # 设备名
      t.string :create_device_name
      # 设备型号
      t.string :create_device_model
      # 系统平台
      t.string :create_platform
      # 系统版本
      t.string :create_system_version
      # 运营商
      t.string :create_operator
      # 网络环境
      t.string :create_net_env
      # app版本名
      t.string :create_app_version, index: true
      # app版本号
      t.string :create_app_version_number
      # app渠道
      t.string :create_app_channel, index: true

      t.timestamps
    end
    add_index :users, :username, unique: true
    add_index :users, :created_at
  end
end

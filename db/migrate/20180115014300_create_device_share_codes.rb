class CreateDeviceShareCodes < ActiveRecord::Migration[5.0]
  def change
    create_table :device_share_codes do |t|
      t.integer :user_id, index: true
      t.string :sdid, limit: 32, null: false
      t.string :uuid, limit: 36
      t.timestamps
    end
    add_index :device_share_codes, :sdid, unique: true
  end
end

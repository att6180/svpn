class CreateUserGroups < ActiveRecord::Migration[5.0]
  def change
    create_table :user_groups do |t|
      # 组名
      t.string :name
      # 累计虚拟钱币数(到达此数量即升级到相应的组)
      t.integer :need_coins, default: 0
      # 等级
      t.integer :level, default: 0
      # 是否启用
      t.boolean :is_enabled, default: true

      t.timestamps
    end
  end
end

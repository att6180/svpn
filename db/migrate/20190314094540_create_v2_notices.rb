class CreateV2Notices < ActiveRecord::Migration[5.0]
  def change
    create_table :v2_notices do |t|
      t.text :user_names
      t.string :platform
      t.string :app_channel
      t.string :app_version
      t.integer :notice_type
      t.integer :open_type
      t.string :button_text
      t.string :link_url
      t.text :alert_text
      t.text :marquee_text
      t.string :description
      t.timestamps
    end
  end
end

class AddNodeStatusFieldToNodes < ActiveRecord::Migration[5.0]
  def change
    # 带宽百分比
    add_column :nodes, :bandwidth_percent, :decimal, precision: 4, scale: 1, default: 0
    # CPU百分比
    add_column :nodes, :cpu_percent, :decimal, precision: 4, scale: 1, default: 0
    # 内存百分比
    add_column :nodes, :memory_percent, :decimal, precision: 4, scale: 1, default: 0
    # 用户使用的流量总和
    add_column :nodes, :transfer, :integer, limit: 8, default: 0
    # 当前网络速度
    add_column :nodes, :network_speed, :integer, limit: 8, default: 0
  end
end

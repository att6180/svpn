class CreateV2PlanVersions < ActiveRecord::Migration[5.0]
  def change
    create_table :v2_plan_versions do |t|
      t.string :channel
      t.string :version
      t.integer :audit_template_id
      t.integer :domestic_template_id
      t.integer :oversea_template_id
      t.integer :iap_template_id
      t.boolean :show_icon, default: false
      t.string :tab_id_sort
      t.boolean :status, default: 1
      t.timestamps
    end
    add_index :v2_plan_versions, [:channel, :version]
    add_index :v2_plan_versions, :status
  end
end

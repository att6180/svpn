class CreateNodeRegions < ActiveRecord::Migration[5.0]
  def change
    create_table :node_regions do |t|
      # 区域名
      t.string :name
      # 服务器类型
      t.integer :node_type_id, index: true
      # 区域代码
      t.string :abbr
      # 节点数
      t.integer :nodes_count, default: 0
      # 是否启用
      t.boolean :is_enabled, default: true
      t.timestamps
    end
    add_index :node_regions, :created_at
  end
end

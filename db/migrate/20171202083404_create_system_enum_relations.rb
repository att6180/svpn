class CreateSystemEnumRelations < ActiveRecord::Migration[5.0]
  def change
    create_table :system_enum_relations do |t|
      t.string :channel
      t.string :version
      t.timestamps
    end
    add_index :system_enum_relations, [:channel, :version], unique: true
  end
end

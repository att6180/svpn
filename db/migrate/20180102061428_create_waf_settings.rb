class CreateWafSettings < ActiveRecord::Migration[5.0]
  def change
    create_table :waf_settings do |t|
      t.integer :brush_time, comment: "防刷时间"
      t.integer :brush_count, comment: "防刷个数"
      t.integer :trigger_count, comment: "触发个数"
      t.timestamps
    end
  end
end

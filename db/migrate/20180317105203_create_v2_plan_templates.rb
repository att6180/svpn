class CreateV2PlanTemplates < ActiveRecord::Migration[5.0]
  def change
    create_table :v2_plan_templates do |t|
      t.string :name, default: 0
      t.integer :plan_type, default: 0
      t.integer :status, default: 1
      t.timestamps
    end
    add_index :v2_plan_templates, :status
  end
end

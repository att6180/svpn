class RemoveVersionFromPlans < ActiveRecord::Migration[5.0]
  def change
    remove_column :plans, :platform
    remove_column :plans, :app_version
    remove_column :plans, :app_version_number
    add_column :plans, :is_audit, :boolean, default: false
  end
end

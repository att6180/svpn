class CreateCustomerServices < ActiveRecord::Migration[5.0]
  def change
    create_table :customer_services do |t|
      t.string :name, limit: 200
      t.string :potato_url, limit: 999
      t.boolean :status, default: true
      t.integer :alloc_count, default: 0
      t.timestamps
    end
  end
end

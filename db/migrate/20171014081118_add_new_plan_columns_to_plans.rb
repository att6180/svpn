class AddNewPlanColumnsToPlans < ActiveRecord::Migration[5.0]
  def change
    add_column :plans, :is_regular_time, :boolean, default: false
    add_column :plans, :time_type, :integer, default: 0
    add_column :plans, :node_type_id, :integer
  end
end

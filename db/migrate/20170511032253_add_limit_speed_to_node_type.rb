class AddLimitSpeedToNodeType < ActiveRecord::Migration[5.0]
  def change
    add_column :node_types, :limit_speed_up, :integer, default: 0
    add_column :node_types, :limit_speed_down, :integer, default: 0
  end
end

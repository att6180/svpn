class AddChannelVersionIndexToV2PlanVersions < ActiveRecord::Migration[5.0]
  def change
    remove_index :v2_plan_versions, [:channel, :version]
    add_index :v2_plan_versions, [:channel, :version], unique: true
  end
end

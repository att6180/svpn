class CreateNodeAdditions < ActiveRecord::Migration[5.0]
  def change
    create_table :node_additions do |t|
      t.integer :node_id
      # 总带宽
      t.integer :max_bandwidth, limit: 8, default: 0
      # 当前带宽
      t.integer :current_bandwidth, limit: 8, default: 0
      # CPU百分比
      t.decimal :cpu_percent, precision: 4, scale: 1, default: 0
      # 内存百分比
      t.decimal :memory_percent, precision: 4, scale: 1, default: 0
      # 用户使用的流量总和
      t.integer :transfer, limit: 8, default: 0
      # 当前网络上传速度
      t.integer :network_speed_up, limit: 8, default: 0
      # 当前网络下载速度
      t.integer :network_speed_down, limit: 8, default: 0
      t.timestamps
    end
    add_index :node_additions, :node_id, unique: true
  end
end

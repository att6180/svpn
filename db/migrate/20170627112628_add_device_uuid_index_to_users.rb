class AddDeviceUuidIndexToUsers < ActiveRecord::Migration[5.0]
  def change
    add_index :users, :create_device_uuid, unique: true
  end
end

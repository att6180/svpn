class CreateSplashScreenAds < ActiveRecord::Migration[5.0]
  def change
    create_table :splash_screen_ads do |t|
      t.string :name, null: false
      t.integer :duration, default: 0
      t.integer :time_priority, default: 1
      t.integer :date_type, default: 0
      t.integer :time_type, default: 0
      t.datetime :start_at
      t.datetime :end_at
      t.string :image_url
      t.string :link_url
      t.boolean :is_enabled, default: false
      t.integer :selected_count, limit: 8, default: 0
      t.integer :imporessions_count, limit: 8, default: 0
      t.datetime :last_modification_time
      t.timestamps
    end
    add_index :splash_screen_ads, :name, unique: true
    add_index :splash_screen_ads, [:start_at, :end_at, :time_type, :is_enabled],
      name: 'ssads_seti'
  end
end

class AddAppVersionNumberToHelpManuals < ActiveRecord::Migration[5.0]
  def change
    add_column :help_manuals, :app_version_number, :string
    remove_column :help_manuals, :is_audit
  end
end

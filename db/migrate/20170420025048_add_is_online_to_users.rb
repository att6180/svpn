class AddIsOnlineToUsers < ActiveRecord::Migration[5.0]
  def change
    # 是否在线
    add_column :users, :is_online, :boolean, default: true
    # 最后离线时间
    add_column :users, :last_offline_time, :datetime
  end
end

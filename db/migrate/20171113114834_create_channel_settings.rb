class CreateChannelSettings < ActiveRecord::Migration[5.0]
  def change
    create_table :channel_settings do |t|
      t.string :app_channel, null: false
      t.string :app_version, null: false
      t.text :settings
      t.timestamps
    end
    add_index :channel_settings, [:app_channel, :app_version], unique: true
  end
end

class AddAutoRenewToUsers < ActiveRecord::Migration[5.0]
  def change
    add_column :users, :auto_renew, :boolean, default: false
  end
end

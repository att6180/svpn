class AddCurrentSigninAtAndVersionsIndexToUsers < ActiveRecord::Migration[5.0]
  def change
    add_index :users, [:current_signin_at, :create_app_channel, :create_app_version],
      name: 'users_ccc'
  end
end

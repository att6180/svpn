class AddAdTypeToSplashScreenAds < ActiveRecord::Migration[5.0]
  def change
    add_column :splash_screen_ads, :ad_type, :integer, after: :last_modification_time, default: 1
  end
end

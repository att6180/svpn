class CreateNodeDiversions < ActiveRecord::Migration[5.0]
  def change
    create_table :node_diversions do |t|
      t.string :province, limit: 50
      t.string :city, limit: 50
      t.integer :node_region_id
      t.integer :node_type_id
      t.boolean :is_enabled, default: true
      t.timestamps
    end
  end
end

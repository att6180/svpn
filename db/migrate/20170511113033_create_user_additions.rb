class CreateUserAdditions < ActiveRecord::Migration[5.0]
  def change
    create_table :user_additions do |t|
      t.integer :user_id
      t.integer :used_bytes, limit: 8, default: 0
      t.datetime :last_connection_at, index: true
      t.integer :last_connected_node_id
      t.datetime :last_inspect_expiration_at
      t.timestamps
    end
    add_index :user_additions, :user_id, unique: true
  end
end

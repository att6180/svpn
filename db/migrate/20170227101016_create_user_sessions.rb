class CreateUserSessions < ActiveRecord::Migration[5.0]
  def change
    create_table :user_sessions do |t|
      # user id
      t.integer :user_id, index: true
      # 设备uuid
      t.integer :device_id, index: true
      # token
      t.string :token
      # 过期时间
      t.datetime :expired_at
      # 错误代号, 用于记录是什么原因导致token被更新了
      t.integer :error_code, default: 0
      t.timestamps
    end
    add_index :user_sessions, [:user_id, :device_id], unique: true
    add_index :user_sessions, :created_at
  end
end

class CreateNodes < ActiveRecord::Migration[5.0]
  def change
    create_table :nodes do |t|
      # 服务器类型
      t.integer :node_type_id, index: true
      # 节点区域id
      t.integer :node_region_id, index: true
      # 名称
      t.string :name, index: true
      # 连接地址
      t.string :url
      # 使用次数
      t.integer :used_count, default: 0
      # 节点状态
      t.integer :node_status, default: 0
      # 是否启用
      t.boolean :is_enabled, default: true
      # 节点描述
      t.string :description
      # 节点连接数
      t.integer :connections_count, default: 0
      # 节点最大连接数
      t.integer :max_connections_count, default: 0
      t.integer :port
      t.string :password
      t.string :encrypt_method
      t.timestamps
    end
    add_index :nodes, :created_at
  end
end

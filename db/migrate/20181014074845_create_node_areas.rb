class CreateNodeAreas < ActiveRecord::Migration[5.0]
  def change
    create_table :node_areas do |t|
      t.string :name, limit: 500
      t.integer :sequence, default: 0
      t.string :abbr
      t.integer :parent_id, default: 0
      t.string :type
      t.timestamps
    end
  end
end

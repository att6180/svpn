class AddDeviceLastUserId < ActiveRecord::Migration[5.0]
  def change
    add_column :devices, :last_user_id, :integer, null: false
  end
end

class CreateFeedbackShortcutReplies < ActiveRecord::Migration[5.0]
  def change
    create_table :feedback_shortcut_replies do |t|
      t.integer :feedback_shortcut_reply_category_id
      t.text :content
      t.integer :index_id, default: 0, index: true
      t.timestamps
    end
  end
end

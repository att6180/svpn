class RemoveAllowFaultTimesFromNodes < ActiveRecord::Migration[5.0]
  def change
    remove_column :nodes, :allow_fault_times
  end
end

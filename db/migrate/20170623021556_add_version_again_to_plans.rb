class AddVersionAgainToPlans < ActiveRecord::Migration[5.0]
  def change
    add_column :plans, :platform, :string, index: true
    add_column :plans, :app_version, :string, index: true
    add_column :plans, :app_version_number, :string, index: true
  end
end

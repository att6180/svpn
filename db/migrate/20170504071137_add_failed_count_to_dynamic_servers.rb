class AddFailedCountToDynamicServers < ActiveRecord::Migration[5.0]
  def change
    add_column :dynamic_servers, :failed_count, :integer, default: 0
    add_index :dynamic_servers, :url
    # add url index from nodes
    add_index :nodes, :url
  end
end

class CreateNodeOperators < ActiveRecord::Migration[5.0]
  def change
    create_table :node_operators do |t|
      t.integer :tag
      t.string :agencys
      t.boolean :is_enabled, default: true
      t.timestamps
    end
  end
end

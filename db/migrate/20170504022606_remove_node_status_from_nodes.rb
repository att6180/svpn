class RemoveNodeStatusFromNodes < ActiveRecord::Migration[5.0]
  def change
    remove_column :nodes, :bandwidth_percent
    remove_column :nodes, :cpu_percent
    remove_column :nodes, :memory_percent
    remove_column :nodes, :transfer
    remove_column :nodes, :network_speed
  end
end

class AddNameIndexToSystemEnum < ActiveRecord::Migration[5.0]
  def change
    add_index :system_enums, [:name, :enum_type], unique: true
  end
end

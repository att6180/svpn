class RemovePlanVersionUniqueIndex < ActiveRecord::Migration[5.0]
  def change
    remove_index :v2_plan_versions, column: [:channel, :version]
  end
end

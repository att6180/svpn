class AddSdidToDeviceShareCode < ActiveRecord::Migration[5.0]
  def change
    add_column :device_share_codes, :apiid, :integer, after: :uuid, comment: "api版本号(主要用于android客户端)"
  end
end

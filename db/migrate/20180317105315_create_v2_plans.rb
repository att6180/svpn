class CreateV2Plans < ActiveRecord::Migration[5.0]
  def change
    create_table :v2_plans do |t|
      t.integer :template_id
      t.integer :client_tab_id
      t.string :name, default: ""
      t.decimal :price, precision: 10, scale: 2, default: 0
      t.string :currency_symbol
      t.boolean :is_iap, default: false
      t.string :iap_id, default: ""
      t.boolean :is_regular_time, default: false
      t.integer :time_type, default: 0
      t.integer :node_type_id
      t.integer :coins, default: 0
      t.integer :present_coins, default: 0
      t.integer :sort_weight, default: 0
      t.string :description1, default: ""
      t.string :description2, default: ""
      t.string :description3, default: ""
      t.boolean :is_recommend, default: false
      t.integer :status, default: 1
      t.timestamps
    end
    add_index :v2_plans, :template_id
    add_index :v2_plans, :client_tab_id
    add_index :v2_plans, [:is_regular_time, :time_type, :is_iap, :status],
      name: "v2_plans_itis"
  end
end

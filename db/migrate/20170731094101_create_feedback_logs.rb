class CreateFeedbackLogs < ActiveRecord::Migration[5.0]
  def change

    create_table :feedback_logs do |t|
      t.integer :feedback_id
      t.references :memberable, polymorphic: true, index: true
      t.string :memberable_id
      t.text :content
      t.timestamps
    end
    add_column :feedbacks, :status, :integer, default: 0
    add_column :feedbacks, :status_updated_at, :datetime
    add_column :feedbacks, :has_read2, :boolean, default: false, comment: 'for new feedback'
    add_column :feedbacks, :has_read_at2, :datetime, comment: 'for new feedback'
    # migrate admin reply
    #admin = ::Manage::Admin.first
    #if admin.present?
      #Feedback.where.not(reply_content: nil).find_in_batches(start: 1, batch_size: 100) do |feedbacks|
        #feedbacks.each do |feedback|
          #feedback.feedback_logs.create(
            #memberable: admin,
            #content: feedback.reply_content,
            #created_at: feedback.processed_at
          #)
          #feedback.update_columns(status: 1)
        #end
      #end
    #end

  end
end

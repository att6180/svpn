class AddPlatfromChannelAccountPakcageToSystemEnumRef < ActiveRecord::Migration[5.0]
  def change
    add_column :system_enum_relations, :platform_id, :integer, after: :id
    add_column :system_enum_relations, :channel_account_id, :integer, after: :platform_id
    add_column :system_enum_relations, :package_id, :integer, after: :channel_account_id
  end
end

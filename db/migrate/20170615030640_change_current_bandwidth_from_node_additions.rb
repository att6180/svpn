class ChangeCurrentBandwidthFromNodeAdditions < ActiveRecord::Migration[5.0]
  def change
    change_column :node_additions, :current_bandwidth, :decimal, precision: 8, scale: 5, default: 0
  end
end

class AddSeqeneceCountryContinentToNodeRegions < ActiveRecord::Migration[5.0]
  def change
    add_column :node_regions, :node_country_id, :integer, after: :node_type_id
    add_column :node_regions, :node_continent_id, :integer, after: :node_country_id
    add_column :node_regions, :sequence, :integer, default: 0, after: :node_continent_id

    add_column :nodes, :node_region_out_id, :integer
  end
end

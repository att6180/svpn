class CreatePlans < ActiveRecord::Migration[5.0]
  def change
    # 套餐
    create_table :plans do |t|
      # 名称
      t.string :name
      # 虚拟货币数
      t.integer :coins, default: 0
      # 价格
      t.decimal :price, precision: 8, scale: 2
      # 赠送虚拟币数
      t.integer :present_coins, default: 0
      # 是否启用
      t.boolean :is_enabled, default: true
      # 是否是IAP
      t.boolean :is_iap, default: false
      # IAP ID
      t.string :iap_id
      # 描述
      t.text :description
      t.timestamps
    end
  end
end

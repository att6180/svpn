class AddKeyIndexToSystemSettings < ActiveRecord::Migration[5.0]
  def change
    add_index :system_settings, :key
  end
end

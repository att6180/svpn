class RemoveUuidIndexFromUsers < ActiveRecord::Migration[5.0]
  def change
    remove_index :users, :create_device_uuid
    add_index :users, :create_device_uuid
  end
end

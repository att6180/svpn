class CreateNodeTypes < ActiveRecord::Migration[5.0]
  def change
    # 服务器类型
    create_table :node_types do |t|
      # 名称
      t.string :name
      # 等级
      t.integer :level, default: 0
      # 帐户类型(此帐户类型的用户即可使用此类型服务器)
      t.integer :user_group_id, index: true
      # 使用时要扣除的虚拟币数
      t.integer :expense_coins, default: 0
      # 是否可以转化为包月
      t.boolean :can_be_monthly, default: true
      # 转化为包月所需要的消费次数
      t.integer :times_for_monthly, default: 0
      # 是否启用
      t.boolean :is_enabled, default: true

      t.timestamps
    end
  end
end

class AddIsClientToSystemSettings < ActiveRecord::Migration[5.0]
  def change
    add_column :system_settings, :is_client, :boolean, default: true
  end
end

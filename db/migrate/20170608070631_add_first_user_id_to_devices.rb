class AddFirstUserIdToDevices < ActiveRecord::Migration[5.0]
  def change
    add_column :devices, :first_user_id, :integer, null: false
  end
end

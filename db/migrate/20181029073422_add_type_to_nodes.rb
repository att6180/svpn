class AddTypeToNodes < ActiveRecord::Migration[5.0]
  def change
    add_column :nodes, :types, :integer, default: 1
  end
end

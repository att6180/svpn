class ChangeContentCollationFromFeedbacks < ActiveRecord::Migration[5.0]
  def change
    change_column :feedbacks, :content, :text
  end
end

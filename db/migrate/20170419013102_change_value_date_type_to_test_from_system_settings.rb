class ChangeValueDateTypeToTestFromSystemSettings < ActiveRecord::Migration[5.0]
  def change
    change_column :system_settings, :value, :text
  end
end

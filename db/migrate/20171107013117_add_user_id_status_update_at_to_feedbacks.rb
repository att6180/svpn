class AddUserIdStatusUpdateAtToFeedbacks < ActiveRecord::Migration[5.0]
  def change
    add_index :feedbacks, [:user_id, :feedback_type, :status, :updated_at], name: 'feedbacks_ufsu'
  end
end

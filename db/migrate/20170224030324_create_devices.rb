class CreateDevices < ActiveRecord::Migration[5.0]
  def change
    create_table :devices do |t|
      # UUID
      t.string :uuid, limit: 191
      # 设备名
      t.string :name
      # 设备型号
      t.string :model
      # 系统平台
      t.string :platform
      # 系统版本
      t.string :system_version
      # 运营商
      t.string :operator
      # 网络环境
      t.string :net_env
      # app版本名
      t.string :app_version
      # app版本号
      t.string :app_version_number
      # app渠道
      t.string :app_channel
      # 代理服务器session_token
      t.string :proxy_session_token
      # 是否修改了用户名
      t.boolean :username_changed, default: false
      # 最后一次修改用户名的用户的ID
      t.integer :last_changed_user_id
      # 是否修改过密码
      t.boolean :password_changed, default: false
      t.timestamps
    end

    add_index :devices, :uuid, unique: true
    add_index :devices, :created_at

    # users和device多对多关系，使用has_and_belongs_to_many
    create_table :devices_users, id: false do |t|
      t.integer :user_id, index: true
      t.integer :device_id, index: true
    end

  end
end

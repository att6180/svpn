class AddUuidAndCreatedAtIndexToUsers < ActiveRecord::Migration[5.0]
  def change
    add_index :users, [:create_device_uuid, :created_at], unique: true
  end
end

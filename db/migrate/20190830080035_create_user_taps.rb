class CreateUserTaps < ActiveRecord::Migration[5.0]
  def change
    create_table :user_taps do |t|
    t.integer :user_id
    t.integer :node_id
    t.boolean :is_enabled, default: true
    t.timestamps
    end
  end
end

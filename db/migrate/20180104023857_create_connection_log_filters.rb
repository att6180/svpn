class CreateConnectionLogFilters < ActiveRecord::Migration[5.0]
  def change
    create_table :connection_log_filters do |t|
      t.string :rule, index: true
      t.timestamps
    end
  end
end

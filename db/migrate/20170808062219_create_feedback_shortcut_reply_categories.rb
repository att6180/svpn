class CreateFeedbackShortcutReplyCategories < ActiveRecord::Migration[5.0]
  def change
    create_table :feedback_shortcut_reply_categories do |t|
      t.string :name
      t.integer :sort_id, default: 0, index: true
      t.timestamps
    end
  end
end

class AddVersionNumberToSystemSettings < ActiveRecord::Migration[5.0]
  def change
    add_column :system_settings, :app_version_number, :string, default: ''
    add_index :system_settings, [:platform, :app_version, :app_version_number], name: 'platform_version_number'
  end
end

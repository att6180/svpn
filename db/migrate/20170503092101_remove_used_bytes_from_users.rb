class RemoveUsedBytesFromUsers < ActiveRecord::Migration[5.0]
  def change
    remove_column :users, :used_bytes
    remove_column :users, :last_connection_at
    remove_column :users, :last_connected_node
    remove_column :users, :last_inspect_expiration_at
  end
end

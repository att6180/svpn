class CreateProxyTestDomains < ActiveRecord::Migration[5.0]
  def change
    create_table :proxy_test_domains do |t|
      t.string :domain
      t.integer :failure_times, default: 0
      t.timestamps
    end
  end
end

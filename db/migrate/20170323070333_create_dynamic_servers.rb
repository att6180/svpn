class CreateDynamicServers < ActiveRecord::Migration[5.0]
  def change
    create_table :dynamic_servers do |t|
      # ip/域名
      t.string :url
      # 区域
      t.string :region
      # 优先级
      t.integer :priority, default: 0
      # 是否可用
      t.boolean :is_enabled, default: false
      t.timestamps
    end
  end
end

class AddDescriptionToNodeTypes < ActiveRecord::Migration[5.0]
  def change
    add_column :node_types, :description, :text
  end
end

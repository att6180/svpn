class CreateUserProxyStatuses < ActiveRecord::Migration[5.0]
  def change
    create_table :user_proxy_statuses do |t|
      t.integer :user_id
      t.datetime :last_used_at
      t.integer :total_bytes, limit: 8
      t.string :last_url
      t.timestamps
    end
    add_index :user_proxy_statuses, :user_id, unique: true
    add_index :user_proxy_statuses, [:user_id, :last_used_at]
    add_index :user_proxy_statuses, [:user_id, :updated_at]
  end
end

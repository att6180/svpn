class CreateNavigationAds < ActiveRecord::Migration[5.0]
  def change
    create_table :navigation_ads do |t|
      t.integer :priority
      t.string :name
      t.string :image_url
      t.string :link_url
      t.boolean :is_enabled
      t.timestamps
    end
  end
end

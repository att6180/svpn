class ChangeIsEnabledToStatusFromNodes < ActiveRecord::Migration[5.0]
  def change
    rename_column :nodes, :is_enabled, :status
    change_column :nodes, :status, :integer, default: 1
    remove_column :nodes, :node_status
  end
end

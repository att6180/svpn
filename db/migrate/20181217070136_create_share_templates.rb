class CreateShareTemplates < ActiveRecord::Migration[5.0]
  def change
    create_table :share_templates do |t|
      t.integer :sequence, default: 0, null:false
      t.string :platform_name, limit: 99, null: false
      t.text :copy_link_text, null: false
      t.string :text_title, null: false
      t.text :text_content, null: false
      t.string :text_url, null: false
      t.text :image_text_content, null: false
      t.string :image_promo_text, null: false
      t.string :landing_page_qrcode_url, limit: 500, null: false
      t.string :background_image_download_url, limit: 500, null: false
      t.datetime :background_image_update_at, null: false, default: -> { 'CURRENT_TIMESTAMP' }
      t.timestamps
    end
    add_index :share_templates, :platform_name, unique: true
  end
end

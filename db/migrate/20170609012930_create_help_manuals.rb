class CreateHelpManuals < ActiveRecord::Migration[5.0]
  def change
    create_table :help_manuals do |t|
      t.string :platform
      t.string :app_version
      t.boolean :is_audit, default: false
      t.string :title
      t.text :content
      t.timestamps
    end
  end
end

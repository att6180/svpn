class CreateRescueDomains < ActiveRecord::Migration[5.0]
  def change
    create_table :rescue_domains do |t|
      t.string :domain, comment: '域名'
      t.integer :domain_type, comment: '0-owner, 1-thirdparty'
      t.decimal :response_time,  precision: 8, scale: 2, comment: '响应时长'
      t.boolean :is_enabled, default: true, comment: '状态 0|禁用, 1|开启'
      t.timestamps
    end
  end
end

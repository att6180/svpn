class CreateWafAntiBrushConfigs < ActiveRecord::Migration[5.0]
  def change
    create_table :waf_anti_brush_configs do |t|
      t.string :no
      t.string :app_channel
      t.string :app_version
      t.string :app_version_no
      t.integer :status, default: 0
      t.text :white_lists
      t.text :black_lists
      t.timestamps
    end

  end
end

class AddIsDomesticAndLevelToNodes < ActiveRecord::Migration[5.0]
  def change
    add_column :nodes, :is_domestic, :boolean, default: false
    add_column :nodes, :level, :integer, default: 1
  end
end

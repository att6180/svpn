class AddIndexToUsersCreateIpCountry < ActiveRecord::Migration[5.0]
  def change
    add_index :users, :create_ip_country
  end
end

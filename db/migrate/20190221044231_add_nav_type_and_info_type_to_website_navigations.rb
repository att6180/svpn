class AddNavTypeAndInfoTypeToWebsiteNavigations < ActiveRecord::Migration[5.0]
  def change
    add_column :website_navigations, :nav_type, :integer, default: 1
    add_column :website_navigations, :info_type, :integer, default: 1
  end
end

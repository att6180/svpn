class AddAppChannelToSystemSetting < ActiveRecord::Migration[5.0]
  def change
    add_column :system_settings, :app_channel, :string, index: true
    SystemSetting.update_all(app_channel: 'all')
  end
end

class AddOpenLinkTypeToSplashScreenAds < ActiveRecord::Migration[5.0]
  def change
    add_column :splash_screen_ads, :open_link_type, :integer, default: 1
    add_column :splash_screen_ads, :app_channel, :string
    add_column :splash_screen_ads, :app_version, :string
  end
end

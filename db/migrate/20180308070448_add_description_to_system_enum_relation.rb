class AddDescriptionToSystemEnumRelation < ActiveRecord::Migration[5.0]
  def change
    add_column :system_enum_relations, :description, :text
  end
end

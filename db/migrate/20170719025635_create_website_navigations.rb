class CreateWebsiteNavigations < ActiveRecord::Migration[5.0]
  def change
    create_table :website_navigations do |t|
      t.integer :sort_id, default: 0, index: true
      t.integer :website_type, default: 0, index: true
      t.string :icon_url
      t.string :name
      t.string :description
      t.string :url
      t.boolean :is_domestic, default: true
      t.timestamps
    end
  end
end

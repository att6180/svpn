class CreateDomainEnums < ActiveRecord::Migration[5.0]
  def change
    create_table :domain_enums do |t|
      t.string :domain, index: true
      t.string :description
      t.timestamps
    end
  end
end

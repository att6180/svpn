class AddBindedPromoCodeToUsers < ActiveRecord::Migration[5.0]
  def change
    add_column :users, :binded_promo_code, :string, default: ''
  end
end

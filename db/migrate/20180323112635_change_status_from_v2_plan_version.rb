class ChangeStatusFromV2PlanVersion < ActiveRecord::Migration[5.0]
  def change
    change_column :v2_plan_versions, :status, :integer
  end
end

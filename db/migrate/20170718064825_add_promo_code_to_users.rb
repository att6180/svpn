class AddPromoCodeToUsers < ActiveRecord::Migration[5.0]
  def change
    add_column :users, :promo_code, :string
    add_column :users, :promo_user_id, :integer
    add_column :users, :promo_users_count, :integer, default: 0
    add_column :users, :promo_coins_count, :integer, default: 0
    add_index :users, :promo_code

    first_user = User.first
    if first_user.present? && first_user.promo_code.blank?
      User.find_in_batches(start: 1, batch_size: 100) do |users|
        users.each do |user|
          loop do
            user.promo_code = Utils::Gen.friendly_code(6)
            break unless User.find_by(promo_code: user.promo_code)
          end
          user.save
        end
      end
    end

    remove_index :users, :promo_code
    add_index :users, :promo_code, unique: true

  end
end

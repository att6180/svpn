class AddAreaCodeToUsers < ActiveRecord::Migration[5.0]
  def change
    add_column :users, :area_code, :integer, after: :user_group_id
  end
end

url_prefix = CONFIG.base_url

system_enums = [
  { enum_type: 0, name: 'moren' },
  { enum_type: 0, name: 'appStore' },
  { enum_type: 1, name: 'jichu' }
]
system_enums.each do |enum|
  SystemEnum.create(enum)
end

user_groups = [
  { id: 1, name: 'vip0', need_coins: 0, level: 1 },
  { id: 2, name: 'VIP1', need_coins: 2, level: 2 },
  { id: 3, name: 'VIP2', need_coins: 10, level: 3 },
  { id: 4, name: 'VIP3', need_coins: 100, level: 4 },
  { id: 5, name: 'VIP4', need_coins: 200, level: 5 },
  { id: 6, name: 'VIP5', need_coins: 350, level: 6 },
  { id: 7, name: 'VIP6', need_coins: 500, level: 7 },
  { id: 8, name: 'VIP7', need_coins: 700, level: 8 },
  { id: 9, name: 'VIP8', need_coins: 1000, level: 9 },
  { id: 10, name: 'VIP9', need_coins: 1500, level: 10 },
  { id: 11, name: 'VIP10', need_coins: 3000, level: 11 }
]
user_groups.each do |group|
  UserGroup.create(group)
end

node_types = [
  {
    id: 1, name: '精英服务', level: 1, user_group_id: 1,
    expense_coins: 1, times_for_monthly: 18, can_be_monthly: true,
    limit_speed_up: 300, limit_speed_down: 300,
    description: '访问Facebook、twitter、instagram等网站；|使用telegram等通讯工具；|当月累计使用20次，本月即可免费使用该服务。'
  },
  {
    id: 2, name: '王者服务', level: 2, user_group_id: 1,
    expense_coins: 2, times_for_monthly: 18, can_be_monthly: false,
    limit_speed_up: 500, limit_speed_down: 500,
    description: '享受“精英服务”所有内容；|流畅观看高清youtube、twich视频；|当月累计使用18次，本月即可免费使用该服务。'
  }
]
node_types.each do |t|
  NodeType.create(t)
end

regions = [
  {
    id: 6, name: '中国', abbr: 'cn', node_type_id: 1, nodes: [
      { id: 11, node_type_id: 1, name: '深圳l1', url: '119.23.64.170',
        max_connections_count: 9999999, port: 8388, password: 'asdf', encrypt_method: 'aes-128-cfb',
        is_domestic: true, level: 1 },
      { id: 14, node_type_id: 1, name: '深圳l2', url: '119.23.64.170',
        max_connections_count: 9999999, port: 8488, password: 'asdf', encrypt_method: 'aes-128-cfb',
        is_domestic: true, level: 2 },
    ]
  },
  {
    id: 1, name: '香港', abbr: 'hk', node_type_id: 1, nodes: [
      { id: 10, node_type_id: 1, name: '香港l1', url: '47.52.30.69',
        max_connections_count: 9999999, port: 8388, password: 'asdf', encrypt_method: 'aes-128-cfb',
        is_domestic: false, level: 1 },
      { id: 13, node_type_id: 1, name: '香港l2', url: '47.52.30.69',
        max_connections_count: 9999999, port: 8488, password: 'asdf', encrypt_method: 'aes-128-cfb',
        is_domestic: false, level: 2 }
    ]
  },
  {
    id: 7, name: '新加坡', abbr: 'sg', node_type_id: 1, nodes: [
      { id: 12, node_type_id: 1, name: '新加坡l1', url: '47.88.226.69',
        max_connections_count: 9999999, port: 8388, password: 'asdf', encrypt_method: 'aes-128-cfb',
        is_domestic: false, level: 1 },
      { id: 15, node_type_id: 1, name: '新加坡l2', url: '47.88.226.69',
        max_connections_count: 9999999, port: 8488, password: 'asdf', encrypt_method: 'aes-128-cfb',
        is_domestic: false, level: 2 }
    ]
  }
]
regions.each do |region_data|
  region = NodeRegion.create(name: region_data[:name], abbr: region_data[:abbr], node_type_id: region_data[:node_type_id])
  region_data[:nodes].each do |node|
    region.nodes.create(node)
  end
end

plans = [
  { id: 1, name: "体验套餐", coins: 1, price: 1, present_coins: 1, description: "这是一个体验套餐，1块钱1钻石" },
  { id: 2, name: "普通套餐", coins: 5, price: 5, present_coins: 4, description: "这是一个普通套餐，可以使用近10天" },
  { id: 3, name: "豪华套餐", coins: 10, price: 10, present_coins: 6, description: "这是一个豪华套餐，可以使用半个月" },
  { id: 4, name: "体验套餐", coins: 1, price: 1, present_coins: 1, description: "这是一个体验套餐，1块钱1钻石", is_iap: true, iap_id: "comlimaosvpnclient053106" },
  { id: 5, name: "普通套餐", coins: 5, price: 5, present_coins: 4, description: "这是一个普通套餐，可以使用近10天", is_iap: true, iap_id: "comlimaosvpnclient053105" },
  { id: 6, name: "豪华套餐", coins: 10, price: 10, present_coins: 6, description: "这是一个豪华套餐，可以使用半个月", is_iap: true, iap_id: "comlimaosvpnclient053106" }
]
plans.each do |plan|
  Plan.create(plan)
end

# init admin
super_admin_role = Manage::Role.create(
  name: 'super_admin',
  manage_role_create: true,
  manage_role_update: true,
  manage_role_delete: true,
  admin_password_update: true,
  admin_role_update: true,
  user_info_update: true,
  user_base_info: true,
  user_behaviour: true,
  operation_behaviour: true,
  charge_manage: true,
  back_stage_manage: true,
  user_manage: true,
  node_manage: true,
  system_setting: true,
  system_enum: true
)
Manage::Role.create(name: 'guest')
Manage::Admin.create(username: 'admin', password: 'welcome123', password_confirmation: 'welcome123', role_id: super_admin_role.id)


# 客户端配置项
system_settings = [
  { platform: 'all', app_version: 'all', key: 'NOTICE_CONTENT', value: "<div style=\"text-align:center;\"><strong><span style = \"line-height:2;color:#000000;font-size:18px\">老用户须知</span></strong></div> <span style = \"line-height:1.5;color:#575757;font-size:15px\">通过爱上VPN、风云VPN、急速VPN、爱上VPN pro四个收费版本强更至极光加速器免费版的用户，如果在四个老版本中申请了包月，可通过意见反馈申请退款，想体验新功能的用户可通过APPStore搜索“极光加速器”下载最新版</span>", description: '公告' },
  { platform: 'android', app_version: 'all', key: 'UPDATE_URL', value: 'http://www.baidu.com/', description: '更新地址' },
  { platform: 'android', app_version: 'all', key: 'SHARE_URL', value: 'http://www.baidu.com/', description: '分享地址' },
  { platform: 'android', app_version: 'all', key: 'SHARE_IMG', value: 'http://www.baidu.com/', description: '分享图片' },
  { platform: 'android', app_version: 'all', key: 'ANDROID_VERSION', value: '1.0.1|0', description: '版本及更新类型' },
  { platform: 'android', app_version: 'all', key: 'UPDATE_CONTENT', value: "<div style=\"text-align:center;\"><strong><span style = \"line-height:2;color:#000000;font-size:18px\">老更新提示</span></strong></div> <span style = \"line-height:1.5;color:#575757;font-size:15px\">这是一个更新提示信息</span>", description: '更新说明' },
  { platform: 'ios', app_version: 'all', key: 'APPSTORE_ID', value: '1206662364', description: '苹果商店中的ID' },
  { platform: 'ios', app_version: 'all', key: 'SHARE_URL', value: "http://down.jiaguangjsq.com/", description: '分享地址' },
  { platform: 'ios', app_version: 'all', key: 'IOS_VERSION', value: "1.0.0|0", description: '版本及更新类型' },
  { platform: 'ios', app_version: 'all', key: 'UPDATE_CONTENT', value: "<div style=\"text-align:center;\"><strong><span style = \"line-height:2;color:#000000;font-size:18px\">老更新提示</span></strong></div> <span style = \"line-height:1.5;color:#575757;font-size:15px\">这是一个更新提示信息</span>", description: '更新说明' },
  { platform: 'ios', app_version: 'all', key: 'SHARE_IMG', value: 'http://www.baidu.com/', description: '分享图片' },
  { platform: 'ios', app_version: 'all', key: 'AUDITING_VERSIONS', value: '', description: 'ios平台正在审核的版本号，如果有多个版本，则用|分隔' },
  { platform: 'ios', app_version: 'jichu', key: 'JPUSH_AUTH', value: '', is_client: false, description: '极光推送认证信息' },
  { platform: 'all', app_version: 'all', key: 'DLOG_ALLOW_SEND', value: 'true', is_client: true, description: '是否允许客户端发送去向日志' },
  { platform: 'all', app_version: 'all', key: 'DLOG_POOL_MAX_COUNT', value: '50', is_client: true, description: '客户端去向日志池累计到多少条后提交' },
  { platform: 'all', app_version: 'all', key: 'DLOG_POST_INTERVAL', value: '600', is_client: true, description: '客户端提交去向日志间隔时间(秒)' },
  { platform: 'all', app_version: 'all', key: 'FLOG_ALLOW_SEND', value: 'true', is_client: true, description: '是否允许客户端发送失败日志' },
  { platform: 'all', app_version: 'all', key: 'FLOG_POOL_MAX_COUNT', value: '5', is_client: true, description: '客户端失败日志池累计到多少条后提交' },
  { platform: 'all', app_version: 'all', key: 'FLOG_POST_INTERVAL', value: '60', is_client: true, description: '客户端提交失败日志间隔时间(秒)' },
  { platform: 'all', app_version: 'all', key: 'FLOG_CLEAN_INTERVAL', value: '300', is_client: true, description: '客户端失败日志清理间隔时间(秒)' },
]
system_settings.each do |setting|
  SystemSetting.create(setting)
end

# 动态服务器
DynamicServer.create(url: 'https://xiaoguoqi.com/', region: '香港', priority: 1, is_enabled: 1)

# 反馈快捷回复
categories = [
  {
    name: '连接问题',
    replies: [
    { content: '感谢您的反馈，狸猫的服务里包含了全球多处加速节点，系统会自动选择离用户最近的节点加速，因此不管用户所在的网络环境是否拥挤都不会受影响;线路选择更加智能。' } ]
  }
]
categories.each do |category|
  c = FeedbackShortcutReplyCategory.create(name: category[:name])
  category[:replies].each do |reply|
    c.feedback_shortcut_replies.create(reply)
  end
end


100.times {|x| 
  $redis.lpush(
    Guard::CACHE_KEY_NODE_FAKE_IP_LIST, 
    IPAddr.new(rand(2**32),Socket::AF_INET).to_s
  )
}

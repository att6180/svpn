# SVPN后端API及用户Web端整理

现在功能是参考网上开源的Shadowsocks管理系统和第一版VPN(strongswan)应用的API开发的，与周欣的需求文档有很多不同，使用`新需求`标出的是在需求文档中有而目前系统中没有的功能。

__主要区别:__

- 新需求没有用户Web端，现有系统有用户Web端
- 新需求用户使用手机注册，现在系统使用Email注册
- 新需求用户根据等级享有不同流量和时限，现有系统用户必须购买套餐才能使用

__线上地址:__

- [后端API文档](https://region01.xiaoguoqi.com/docs/index) - 帐号: isec/welcome123
- [用户web端](https://region01.xiaoguoqi.com)

## APP后端API

使用JWT进行通讯认证，部分敏感通讯数据使用对称加密

#### 1. 登录认证
- 注册
- 登录

使用Email进行注册，并通过收到的邮件进行帐号确认，完成注册

__新需求__

- *使用手机号注册，并通过短信验证码进行确定登录*
- *登录界面有找回密码按钮*
- *用户可在不注册的情况下自动分配帐号*

#### 2. 用户
- 获得当前用户信息(签到，套餐，过期时间，流量，设备等)
- 当前用户签到，每天可签到1次，根据套餐类型奖励不同时间和流量，同时支持连续签到奖励(最好多7天)
- 当前用户购买记录
- 修改密码

__新需求__

- *绑定微信、微博、QQ帐号*
- *帐号分类型（游客组、VIP等级)*
- *用户连接速度限制*

#### 2. 服务器节点
- 获得服务器所有节点数据(区域，节点ID，节点名，连接地址等)

#### 3. 套餐
- 获取套餐列表数据
- 获取IAP套餐列表
- IAP二次验证(未测试)

#### 4. 反馈
- 获取反馈分类数据
- 提交反馈

#### 5. 帮助问答
- 获取帮助问答数据
- 获取帮助问答的最后更新时间

#### 6. 其他新需求
- 检查APP版本更新
- GPS定位功能

## 用户Web端

#### 1. 注册登录

使用Email进行注册和登录

- 注册（email/密码/验证码）
- 登录（email/密码/保持登录状态/输入错误三次后出现验证码)
- 忘记密码(通过向注册email发送密码重置邮件进行重置密码)

#### 2. 签到

同APP的API接口，用户在登录成功后可进行签到并获取奖励

#### 3. 套餐信息展示

用户登录成功后，显示当前套餐余量和到期时间，及购买按钮

#### 4. 公告内容显示

通过后台设置的公告内容，在用户登录后的首屏显示

#### 5. 用户反馈

用户登录成功后的首屏可提交反馈内容，管理可从后台查看

#### 6. 订单

用户可通过订单界面查看历史订单及状态等详细信息

#### 7. 套餐

用户通过套餐界面查看所有套餐，对指定套餐进行购买并开通相应套餐

#### 8. 知识库

用户通过知识库界面查看产品使用中的相关知识和问答，后台可对此内容进行管理

#### 9. 推荐朋友

用户可通过此界面生成推荐链接，用户和通过此链接注册的用户会共同获得相应的流量奖励，后台可设置
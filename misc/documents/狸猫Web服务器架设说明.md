# SVPN Web服务器架设指南

## 1. 环境
- 系统：Ubuntu 16.04
- MySQL：5.7.18
- Nginx: 1.10.2
- Redis: 3.2.8
- Ruby: 2.3.3

## 2. 系统准备

用户: root

```bash
# 更新源
apt-get update
# 链接sources.lish，否则有的软件包会出错
ln -s /etc/apt/sources.list.d/sources-aliyun-0.list /etc/apt/sources.list
```

#### 2.1 安全升级

```bash
apt install unattended-upgrades
# 升级过程无控制台输出
unattended-upgrade
```

#### 2.2依赖包

```bash
# 添加redis新版源
apt install -y software-properties-common
add-apt-repository -y ppa:chris-lea/redis-server

# 安装依赖、MySQL、Redis
# 如果这里MySQL是安装在另一台机器上，请忽略最后一行的 mysql-server 包
apt install -y build-essential vim htop unzip git-core wget \
zlib1g-dev libssl-dev libreadline-dev libyaml-dev \
libxml2-dev libxslt1-dev libffi-dev curl libcurl3 \
libcurl3-gnutls libcurl4-openssl-dev nodejs \
mysql-server libmysqlclient-dev redis-server nginx
```

## 3. 建立用户

```bash
useradd -m -s /bin/bash deploy
adduser deploy sudo
passwd deploy
mkdir -p /home/deploy/.ssh
chown deploy:deploy /home/deploy -R
```
切换到deploy用户登录。

将附件中的 `id_rsa` 私钥放入 `/home/deploy/.ssh/` 目录中，用来在部署时从源码库获取源码.

```bash
chmod 600 ~/.ssh/id_rsa
```

## 4. 安装ruby环境

安装rbenv

```bash
git clone https://github.com/rbenv/rbenv.git ~/.rbenv --depth=1
echo 'export PATH="$HOME/.rbenv/bin:$PATH"' >> ~/.bash_profile
echo 'eval "$(rbenv init -)"' >> ~/.bash_profile
git clone git://github.com/sstephenson/ruby-build.git ~/.rbenv/plugins/ruby-build --depth=1
echo 'export PATH="$HOME/.rbenv/plugins/ruby-build/bin:$PATH"' >> ~/.bash_profile
source ~/.bash_profile

# (仅国内服务器使用)如果是国内服务器，则可以安装rbenv插件加速下载
git clone https://github.com/andorchen/rbenv-china-mirror.git "$(rbenv root)"/plugins/rbenv-china-mirror

# 安装ruby
rbenv install -v 2.3.3
rbenv global 2.3.3
ruby -v
echo "gem: --no-document" > ~/.gemrc
gem install bundler


# (仅国内服务器使用)国内gems库镜像，加速gems下载
bundle config mirror.https://rubygems.org https://gems.ruby-china.org
```

## 5. 安装web服务器环境

### 5.1 设置mysql

进入mysql命令行

```base
# 建立用户
CREATE USER 'svpn'@'localhost' IDENTIFIED BY 'password';
# 建立数据库
CREATE DATABASE svpn CHARACTER SET utf8 COLLATE utf8_general_ci;
CREATE DATABASE svpn_log CHARACTER SET utf8 COLLATE utf8_general_ci;
CREATE DATABASE svpn_misc CHARACTER SET utf8 COLLATE utf8_general_ci;
CREATE DATABASE svpn_manage CHARACTER SET utf8 COLLATE utf8_general_ci;
# 设置用户权限
GRANT ALL ON svpn.* TO 'svpn'@'localhost';
GRANT ALL ON svpn_log.* TO 'svpn'@'localhost';
GRANT ALL ON svpn_misc.* TO 'svpn'@'localhost';
GRANT ALL ON svpn_manage.* TO 'svpn'@'localhost';
# 如果数据库是单独在内网的服务器
GRANT ALL ON svpn.* TO svpn@'%' IDENTIFIED BY 'xxxx';
GRANT ALL ON svpn_log.* TO svpn@'%' IDENTIFIED BY 'xxxx';
GRANT ALL ON svpn_misc.* TO svpn@'%' IDENTIFIED BY 'xxxx';
GRANT ALL ON svpn_manage.* TO svpn@'%' IDENTIFIED BY 'xxxx';
```

### 5.2 安装https证书

```bash
# 安装acme.sh
curl  https://get.acme.sh | sh

# (仅国内服务器使用)如果是国内服务器，则可以手动下载安装
wget https://github.com/Neilpang/acme.sh/archive/master.zip  
unzip master.zip  
mv acme.sh-master acme.sh
cd acme.sh
./acme.sh --install
cd

# 重载
source .bashrc
# 停止nginx
systemctl stop nginx
# 生成证书, staging.xiaoguoqi.com 是测试临时使用的域名
# 执行前需要重新进入一次shell
acme.sh --issue -d zeus.staging.xiaoguoqi.com -d staging.xiaoguoqi.com --standalone
# 安装证书
mkdir -p /etc/nginx/ssl/staging_svpn
acme.sh  --installcert -d zeus.staging.xiaoguoqi.com -d staging.xiaoguoqi.com \
        --keypath /etc/nginx/ssl/staging_svpn/staging_svpn.key \
        --fullchainpath /etc/nginx/ssl/staging_svpn/staging_svpn.cer \
        --reloadcmd "systemctl force-reload nginx"
```

### 5.3 设置nginx

将附件中 `nginx/svpn` 文件复制到主机的 `/etc/nginx/sites-available/` 目录下, 并建立链接

```bash
ln -s /etc/nginx/sites-available/svpn /etc/nginx/sites-enabled/svpn
```

创建日志文件并重启nginx

```bash
mkdir -p /var/log/nginx/svpn
touch /var/log/nginx/svpn/error.log
touch /var/log/nginx/svpn/access.log
systemctl restart nginx
```


### 5.4 设置部署环境

```bash
# 进入项目目录，初始化web应用目录
bundle exec mina setup
```

```bash
# 下载ipip.net数据文件，用于ip解析
cd svpn/shared/data
wget https://coding.net/u/bindiry/p/misc/git/raw/master/17monipdb.dat
```

为`svpn/shared/config/`目录下的配置文件添加内容:

database.yml

```yml
default: &default
  adapter: mysql2
  pool: 5
  timeout: 5000
  encoding: utf8
  username: svpn
  password: xxx
  host: 172.31.7.178
production:
  <<: *default
  database: svpn
```

database_log.yml

```yml
default: &default
  adapter: mysql2
  pool: 5
  timeout: 5000
  encoding: utf8
  username: svpn
  password: xxx
  host: 172.31.7.178
production:
  <<: *default
  database: svpn_log
```

database_misc.yml

```yml
default: &default
  adapter: mysql2
  pool: 5
  timeout: 5000
  encoding: utf8
  username: svpn
  password: xxx
  host: 172.31.7.178
production:
  <<: *default
  database: svpn_misc
```

database_manage.yml

```yml
default: &default
  adapter: mysql2
  pool: 5
  timeout: 5000
  encoding: utf8
  username: svpn
  password: xxx
  host: 172.31.7.178
production:
  <<: *default
  database: svpn_manage
```

puma.rb:

```ruby
app_root = '/home/deploy/svpn'
pidfile "#{app_root}/shared/tmp/pids/puma.pid"
state_path "#{app_root}/shared/tmp/sockets/puma.state"
bind "unix://#{app_root}/shared/tmp/sockets/puma.sock"
stdout_redirect "#{app_root}/shared/log/puma.stdout.log", "#{app_root}/shared/log/puma.stderr.log", true

environment "production"
daemonize true
workers 2
threads 1,6
preload_app!

on_worker_boot do
  ActiveSupport.on_load(:active_record) do
    ActiveRecord::Base.establish_connection
  end
end

before_fork do
  ActiveRecord::Base.connection_pool.disconnect!
end

plugin :tmp_restart

```

secret.yml

```yml
redis: &redis
  redis_host: 127.0.0.1
  redis_port: 6379
  redis_db: 1
  redis_namespace: svpn

default: &default
  # comsunny支付
  comsunny_app_id: xxx
  comsunny_master_secret: xxx
  # jpush
  jpush_app_key: xxx
  jpush_secret: xxx
  # sms
  sms_host_url: https://sms.yunpian.com/v2/sms/tpl_single_send.json
  sms_api_key: xxx
  sms_tpl_id: 123123
  # docs
  docs_auth_username: xxx
  docs_auth_password: xxx
  # admin
  admin_api_token: xxx
  # proxy
  proxy_api_token: xxx
  # server type, 1:centra&region, 2:centra, 3:region
  server_type: 1
  node_id: 1

production:
  <<: *redis
  <<: *default
  base_url: https://xxx.xiaoguoqi.com
  secret_key_base: 这里需要通过rails secret生成新的

```

开始部署

```bash
# 进入项目目录
bundle exec mina deploy
```

初始数据设置(仅中心服务器使用), 进入服务器web目录 `/home/deploy/svpn/current`

```bash
# 中心服务器执行如下命令填充默认数据
# 地区服务器不用执行此命令，数据由中心服务器同步下发
RAILS_ENV=production bundle exec rails db:seed
```

### 5.5 安装logrotate管理日志文件

```bash
apt-get install logrotate
# 设置svpn后端配置
vim /etc/logrotate.d/svpn
```

```
/home/deploy/svpn/current/log/*.log {
  su deploy deploy
  daily
  size 100M
  missingok
  notifempty
  rotate 7
  compress
  delaycompress
  copytruncate
 }
```



## 6. (仅中心服务器)安装后台管理web服务器环境

将附件中 `nginx/zeus` 文件复制到主机的 `/etc/nginx/sites-available/` 目录下, 并建立链接

```bash
ln -s /etc/nginx/sites-available/zeus /etc/nginx/sites-enabled/zeus
```

### 6.1 安装https证书

如果是将后台管理也放在中心服务器上，则需要生成证书时指定该服务器上所有启用https的域名，统一生成一个证书在多个站点上使用。

```bash
# 安装acme.sh
curl  https://get.acme.sh | sh
# 停止nginx
service nginx stop
# 生成证书, region.xiaoguoqi.com 是测试临时使用的域名
acme.sh --issue -d zeus.xiaoguoqi.com --standalone
# 安装证书
mkdir -p /etc/nginx/ssl/zeus
acme.sh  --installcert -d zeus.xiaoguoqi.com \
        --keypath /etc/nginx/ssl/zeus/zeus.key \
        --fullchainpath /etc/nginx/ssl/zeus/zeus.cer \
        --reloadcmd "service nginx force-reload"
```

### 6.2 建立目录

使用deploy身份登录

```bash
mkdir -p ~/zeus/www
```

### 6.3 部署

进入项目目录

```bash
./deploy
```

## 8. 记录mysql位点信息，用于otter数据同步(中心服务器与地区服务器)

每台服务器只需要做一次, 进入mysql命令行

```bash
# 显示binlog文件及位置
show master status;
# 显示生前时间
select unix_timestamp(now());
# 位点信息示例
{"journalName":"mysql-bin.000001","position":221277,"timestamp":1483973034};
```


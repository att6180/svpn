---
title: SVPN客户端API文档v4

search: false
---

# 1 客户端接口文档V4

<aside class="success">
这是v4版的客户端接口

在此版中出现的接口需优先使用，没有出现的接口依然使用之前版本
</aside>

## 1.4 用户登录

关于 `app_launch_id` 的说明:

- 用户全新运行客户端(后台没有运行的前提下)，调登录接口时，不传`app_launch_id`参数
- 用户按Home键将客户端切至后台运行，一段时间后，再次打开客户端(从后台切出)，调登录接口时，传 `app_launch_id`参数，值为上一次全新打开客户端调登录接口时返回的值

> 当只使用uuid登录，并且此设备已经修改过用户名或密码时，返回的数据如下:

```json
{
  "success": true,
  "message": null,
  "server_time": 1492414397,
  "data": {
    "only_has_username": true,
    "username": "13584634396"
  }
}
```

> 其他情况，则返回:

```json
{
    "success": true,
    "message": null,
    "data": {
        "only_has_username": false,
        "proxy_session_id": 144,
        "app_launch_id": "e0a35bd0-6b96-11e7-bf54-784f4352f7e7",
        "signin_log_id": "e0a2046a-6b96-11e7-bf54-784f4352f7e7",
        "user": {
            "api_token": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpZCI6MTQ2LCJ1c2VybmFtZSI6InN1cDAwMDAwMTQ2IiwiZGV2aWNlX2lkIjoxNTN9.qHIPJzuzKsfNrKg11RY3TVJBuHF0r5RraM_A3UWPnCQ",
            "id": 146,
            "area_code": 86,
            "username": "sup00000146",
            "email": null,
            "total_payment_amount": "0.0",
            "current_coins": 5,
            "total_coins": 0,
            "password": "kSeDwsdm7K5l2Wbo4OSIGQ==",
            "is_checkin_today": false,
            "is_enabled": true,
            "created_at": 1500282169,
            "new_message": false,
            "promo_code": "hgybrw",
            "promo_users_count": 0,
            "promo_coins_count": 0,
            "binded_promo_code": 'kdiekd',
            "auto_renew": true,
            "debug_log_enabled": false,
            "debug_log_start_at": "",
            "debug_log_end_at": "",
            "debug_log_max_days": 7
        },
        "group": {
            "id": 1,
            "name": "VIP0",
            "level": 1
        },
        "user_node_types": [
            {
                "id": 318,
                "name": "精英服务",
                "level": 1,
                "status": "按次",
                "expired_at": 1500282169,
                "used_count": 0,
                "node_type_id": 1
            },
            {
                "id": 319,
                "name": "王者服务",
                "level": 2,
                "status": "按次",
                "expired_at": 1500282169,
                "used_count": 0,
                "node_type_id": 2
            }
        ],
        "node_types": [
            {
                "id": 1,
                "name": "精英服务",
                "level": 1,
                "expense_coins": 1,
                "user_group_id": 1,
                "user_group_level": 1,
                "description": "访问Facebook、twitter、instagram等网站；使用telegram等通讯工具；|当月累计使用20次，本月即可免费使用该服务",
                "tab_id": 1
            },
            {
                "id": 2,
                "name": "王者服务",
                "level": 2,
                "expense_coins": 3,
                "user_group_id": 1,
                "user_group_level": 1,
                "description": "享受“精英服务”所有内容；|流畅观看高清youtube、twich视频；|当月累计使用18次，本月即可免费使用该服务。",
                "tab_id": 1
            }
        ],
        "show_icon": true,
        "ip_region": {
            "country": "本机地址",
            "province": "本机地址",
            "city": null
        },
        "recommend": {
            "node_type_id": 1,
            "country_id": 7,
            "country_name": "巴西",
            "country_abbr": "http://bx.jpg"
        },
        "settings": {
            "NOTICE_CONTENT": "all",
            "QQ_GROUP": "敬请期待",
            "WX_OFFICAL_ACCOUNT": "敬请期待",
            "TELEGRAM_GROUP": "https://t.me/limaojiasuqi",
            "OFFICIAL_WEBSITE": "http://www.limaojs.com/",
            "ALLOW_SEND_LOG": "true",
            "LOG_POOL_MAX_COUNT": "50",
            "POST_LOG_INTERVAL": "600",
            "test": "",
            "UPDATE_URL": "http://www.baidu.com/",
            "SHARE_URL": "http://share.licatjsq.com",
            "SHARE_IMG": "https://licatjsq.com/share/1024.png",
            "ANDROID_VERSION": "1.0.8|1",
            "UPDATE_CONTENT": "<div style='text-align:center;'><strong><span style = 'line-height:2;color:#000000;font-size:18px'>老用户须知</span></strong></div> <span style = 'line-height:2;color:#575757;font-size:15px'>欢迎使用狸猫加速器</span>",
            "IS_UPDATE": "0"
        }
    }
}
```

### HTTP请求

`POST /api/client/v4/signin`

### 请求参数

参数 | 类型 | 必须 | 说明
---- | ---- | ---- | ----
username | string | false | 手机号
password | string | false | 登录密码
device_uuid | string | true | 设备uuid
device_name | string | false | 设备名(如: xxx的iphone)
device_model | string | true | 设备型号(如: iphone5)
platform | string | true | 设备系统平台(如: ios)
system_version | string | true | 设备版本(如: 10)
operator | string | true | 网络运营商(如: unicomm)
net_env | string | true | 网络环境(如: 4g、wifi)
app_version | string | true | 版本名，来自"版本渠道信息接口"获取到的ID
app_version_number | string | true | App版本(如: 1.0)
app_channel | string | true | app渠道, 来自"版本渠道信息接口"获取到的ID
app_launch_id | string | false | 用于记录App运行日志, 此值只在App的一次运行周期内有效，否则不在请求中加入此参数
sdid | string | false | 设备共享码，目前主要用于android客户端

### 返回信息

如果请求不成功，返回的数据会包含一个`integer`型的`error_code`字段，用来代表出错类型:

错误代码 | 说明
---- | ----
1 | 用户不存在
2 | 用户被封号
3 | 用户名或密码错误
23 | 请求被拒绝(防刷)

字段名 | 说明
---- | ----
only_has_username | 用来在只使用uuid登录时表示此设备是否修改过用户名或密码，如果为true，则只返回修改过的用户名，如果为false，则视为正常登录返回所有用户信息
proxy_session_id | 用于连接代理服务器时使用
app_launch_id | 用于记录App运行日志, 需客户端在App一次运行周期内保存，并在登录请求中加入此参数
signin_log_id | 用户登录日志ID，在请求操作日志等接口时使用
user.api_token | 之后API调用需认证的接口时所需要的token(目前还未设置过期策略)
user.id | 用户id
user.area_code | 区号
user.username | 用户的用户名，用户自行修改用户名后，会用来存储手机号
\*user.password | （需解密)用户密码
user.email | 用户绑定的email
user.total_payment_amount | 用户的消费累计金额
user.current_coins | 用户当前钻石数
user.total_coins | 用户拥有过的总钻石数
user.is_checkin_today | 用户今天是否签到过, true: 是, false: 否
user.is_enabled | 用户帐号状态(如: 被封号)，true:正常, false:已封号
user.created_at | 帐号创建时间
user.new_message | 是否有新消息, true: 是, false: 否
user.promo_code | 用户的推广码
user.promo_users_count | 用户当前已经推广的人数
user.promo_coins_count | 用户累计获得的推广钻石数
user.binded_promo_code | 已绑定的推广码，如果为`""`，则表示还未绑定
user.auto_renew | 是否自动扣除钻石延长有效期
user.debug_log_enabled | 是否上传此用户的调试日志
user.debug_log_start_at | 调试日志开始日期，如: 2017-09-11
user.debug_log_end_at | 调试日志结束时间，如: 2017-09-12
user.debug_log_max_days | 调试日志保存最大天数
group.id | 用户组id
group.name | 用户组名称
group.level | 用户组等级
device.id | 当前设备ID
device.uuid | 当前设备uuid
regions.id | 服务器节点区域ID
user_node_types.id | 用户服务器类型ID
user_node_types.name | 用户服务器类型名称
user_node_types.level | 用户服务器类型级别
user_node_types.status | 用户服务器类型状态
user_node_types.expired_at | 用户服务器类型过期时间
user_node_types.used_count | 用户服务器类型使用次数
user_node_types.node_type_id | 服务器类型ID，用来对应服务器线路列表中的node_type的ID
ip_region.country | 国家,该字段有可能值为 null
ip_region.province | 省/州,该字段有可能值为 null
ip_region.city | 城市,该字段有可能值为 null
recommend.node_type_id | 专线推荐用户服务器类型ID
recommend.country_id | 专线推荐国家id,该字段有可能值为 null
recommend.country_name | 专线推荐国家名称,该字段有可能值为 null
recommend.country_abbr | 专线推荐国家国旗地址,该字段有可能值为 null
node_types.id | 服务器类型ID
node_types.name | 服务器类型名称
node_types.level | 服务器类型等级
node_types.expense_coins | 连接此服务器类型需要扣除了钻石数
node_types.user_group_id | 连接此服务器类型所需要的最低用户类型ID
node_types.user_group_level | 连接此服务器类型所需要的最低用户类型等级
node_types.description | 描述
node_types.tab_id | 对应客户端标签ID
show_icon | 是否显示活动图标
settings | 后台管理配置的参数，具体配置项由开发者自行制定

## 1.19 创建Comsunny支付订单(H5页面)(认证)

这一版和v3版在参数上没有区别，主要是后端对于新充值系统的改动

流程:

- 客户端使用此接口创建订单，获取`订单号` 和 `h5支付页面URL`
- 客户端打开 `h5支付页面` 进行支付
- 支付平台向`客户端`同步返回支付结果，同时向`后端服务器`异步发送支付结果，后端webhook地址: `{域名}/api/client/v1/comsunny_callback`
- 客户端在收到支付结果后，使用`后端订单号`向后端编号`1.20`接口查询订单状态并获得用户最新状态

```json
{
  "success": true,
  "message": null,
  "data": {
    "transaction_log": {
      "order_number": "20170527149586928854346b6",
      "payment_url": "https://dispatch.5ydoc.com/Api/getOrderView?order_id=20170527149586928854346b6"
    }
  }
}
```

### HTTP请求

`POST /api/client/v4/plans/comsunny_order`

### 请求参数

参数 | 类型 | 必须 | 说明
---- | ---- | ---- | ----
platform | string | true | 平台，和登录接口一致
app_channel | string | true | 渠道，和登录接口一致
app_version | string | true | 版本，和登录接口一致
app_version_number | string | true | 版本号
plan_id | integer | true | 套餐ID

### 错误信息

错误码 | 说明
---- | ----
1 | 支付平台创建订单失败

### 返回信息

字段名 | 说明
---- | ----
transaction_log.order_number | 订单号
transaction_log.payment_url | h5支付页面URL

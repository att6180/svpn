---
title: 运维自动化接口文档

search: false
---

# 1 运维自动化接口文档

此文档用来描述API使用细节，并附有一份Postman(API测试软件) Collection(文件请联系后端同事)，用于API接口测试。

- [Postman下载](https://www.getpostman.com/apps)

**Collections导入方法**

- 打开Postman，点击 File -> Import -> Import From Link
- 输入 `https://www.getpostman.com/collections/3aa9b6f63452f124b80e`，点击 Import，右侧会出现名为 `ops_api` 的Collection
- 点击Postman右上角的小齿轮按钮，在出现的菜单中点击`Manage Environments`，点击 `Add` 按钮
- Environment Name: production
- key: url_prefix
- value: https://staging.xiaoguoqi.com
- 点击 `Add` 按钮添加环境变量
- 在右上角小齿轮左侧的下接列表中选择刚添加的`production`
- 现在可以测试各个接口了

文档返回数据统一使用JSON格式，在没有发生意外错误时，状态统一返回200。

其中所有JSON数据中都包含如下三个字段:

字段名 | 类型 | 说明
---- | ---- | ----
success | boolean | 用于描述数据是否请求成功
message | string | 此字段会包含成功或失败的信息，如果没有消息，则为`null`
data | json | 如果请求成功，返回数据会被包含在这个字段内

返回数据中关于时间的字段，如: `created_at`，将统一使用unix时间戳的形式表示(如: 1488335839)。

### 请求说明

- POST方式的请求，参数统一使用 `Form Data`
- 请求中涉及到时间，统一使用unix时间戳的形式表示(如: 1488335839)


接口认证:

运维调用`此文档`所列出的接口时，需要在`headers`中加入认证头，如:

`Authorization: xxxxxxxxx`

## 1.1 新增服务器(认证)

此接口用于新增服务器配置，同服务器群组管理 - 服务器配置

```json
{
  "success": true,
  "message": null,
  "data": {
    "id": 10004,
    "node_type_id": 1,
    "node_type_name": "精英服务",
    "region_id": 1,
    "region_name": "中国",
    "node_isp_id": null,
    "node_isp_name": null,
    "region_abbr": "cn",
    "name": "运维api新增服务器",
    "url": "192.168.1.1",
    "port": 8888,
    "password": "66666",
    "encrypt_method": "aes_256",
    "connections_count": 0,
    "max_connections_count": 9999,
    "status": "enabled",
    "is_domestic": true,
    "level": 1,
    "description": "",
    "created_at": 1525920289
  }
}
```

### HTTP请求

`POST /api/ops/v1/nodes`

### 请求参数

参数 | 类型 | 必须 | 说明
---- | ---- | ---- | ----
id | integer | true | 服务器ID
node_type_id | integer | true | 服务器类型ID
node_region_id | integer | true | 区域ID
node_isp_id | integer | false | 代理商标识ID
node_region_out_id | integer | false | 服务器出口区域ID
name | string | true | 名称
url | string | true | 地址
port | integer | true | 端口
password | string | true | 密码
encrypt_method | string | true | 加密方式
description | string | false | 描述
max_connections_count | integer | false | 最大连接数
is_domestic | string | false | 是否是国内线路 true: 是, false: 否
level | integer | false | 线路级别 1: 一级代理,2: 二级代理
status | string | false | 节点状态, enabled:启用, disabled:禁用, standby:备用, deprecated:弃用
types | string | false | 线路类型, covered: 硬核直连和的定制专线, smart: 硬核直连, customization: 定制专线

## 1.2 更新服务器(认证)

此接口用于更新服务器配置，同服务器群组管理 - 服务器配置

```json
{
  "success": true,
  "message": null,
  "data": {
    "id": 10004,
    "node_type_id": 1,
    "node_type_name": "精英服务",
    "region_id": 1,
    "region_name": "中国",
    "node_isp_id": null,
    "node_isp_name": null,
    "region_abbr": "cn",
    "name": "运维api新增服务器",
    "url": "192.168.1.1",
    "port": 8888,
    "password": "66666",
    "encrypt_method": "aes_256",
    "connections_count": 0,
    "max_connections_count": 9999,
    "status": "enabled",
    "is_domestic": true,
    "level": 1,
    "description": "",
    "created_at": 1525920289
  }
}
```

### HTTP请求

`PATCH /api/ops/v1/nodes/{id}`

### 请求参数

参数 | 类型 | 必须 | 说明
---- | ---- | ---- | ----
node_type_id | integer | true | 服务器类型ID
node_region_id | integer | false | 区域ID
node_isp_id | integer | false | 代理商标识ID
node_region_out_id | integer | false | 服务器出口区域ID
name | string | false | 名称
url | string | false | 地址
port | integer | false | 端口
password | string | false | 密码
encrypt_method | string | false | 加密方式
description | string | false | 描述
max_connections_count | integer | false | 最大连接数
is_domestic | string | false | 是否是国内线路 true: 是, false: 否
level | integer | false | 线路级别 1: 一级代理,2: 二级代理
status | string | false | 节点状态, enabled:启用, disabled:禁用, standby:备用, deprecated:弃用
types | string | false | 线路类型, covered: 硬核直连和的定制专线, smart: 硬核直连, customization: 定制专线

## 1.3 查询服务器列表最大ID(认证)

此接口返回当前服务器列表最大id，新增时可调用该接口获取

```json
{
  "success": true,
  "message": null,
  "data": {
    "id": 10004
  }
}
```

### HTTP请求

`GET /api/ops/v1/nodes`

## 1.4 查询服务器类型(认证)

此接口返回当前服务器类型

```json
{
    "success": true,
    "message": null,
    "data": {
        "node_type_id": 1,
        "status": 1 
    }
}
```

### HTTP请求

`GET /api/ops/v1/nodes/node_type`

### 请求参数

参数 | 类型 | 必须 | 说明
---- | ---- | ---- | ----
id | integer | true | 服务器ID

### 返回信息
参数 | 类型 | 必须 | 说明
---- | ---- | ---- | ----
node_type_id | integer | true | 服务器类型 普通服务: 1, 高级服务: 2
status | integer | true | 禁用: 0, 启用: 1, 备用: -1, 弃用: -2 


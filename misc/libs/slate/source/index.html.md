---
title: SVPN客户端API文档

search: false
---

# 1 客户端接口文档

Web后端API服务器现分为三个:

- 开发服: `https://xiaoguoqi.com`
- 测试服: `https://staging.xiaoguoqi.com`
- 正式服: `https://licatjsq.com`

此文档用来描述API使用细节，并附有一份Postman(API测试软件) Collection(文件请联系后端同事)，用于API接口测试。

- [Postman下载](https://www.getpostman.com/apps)

**Collections导入方法**

- 打开Postman，点击 File -> Import -> Import From Link
- 输入 `https://www.getpostman.com/collections/6dfaa357b450ea522ef5`，点击 Import，右侧会出现名为 `client_api` 的Collection
- 点击Postman右上角的小齿轮按钮，在出现的菜单中点击`Manage Environments`，点击 `Add` 按钮
- Environment Name: production
- key: url_prefix
- value: https://xiaoguoqi.com
- 点击 `Add` 按钮添加环境变量
- 在右上角小齿轮左侧的下接列表中选择刚添加的`production`
- 现在可以测试各个接口了

文档返回数据统一使用JSON格式，在没有发生意外错误时，状态统一返回200。

其中所有JSON数据中都包含如下三个字段:

字段名 | 类型 | 说明
---- | ---- | ----
success | boolean | 用于描述数据是否请求成功
message | string | 此字段会包含成功或失败的信息，如果没有消息，则为`null`
data | json | 如果请求成功，返回数据会被包含在这个字段内

返回数据中关于时间的字段，如: `created_at`，将统一使用unix时间戳的形式表示(如: 1488335839)。

### 请求说明

- POST方式的请求，参数统一使用 `Form Data`
- 请求中涉及到时间，统一使用unix时间戳的形式表示(如: 1488335839)

### 关于接口访问过于频繁

当客户端在1秒内调用数次同一个接口时，会返回如下错误:

```json
{
    "success": false,
    "message": "操作过于频繁",
    "error_code": 100
}
```

客户端可以判断如果错误码为`100`，就忽略这个错误消息。

比如iOS客户端国行机，在首次安装应用后，需要申请网络访问权限，这时候客户端很有可能在1秒内向登录接口发起数次请求，
（客户端无法解决这个问题，可能是网络访问框架的机制所致），导致同一uuid在1秒钟内创建数个帐号。

为了防止这种情况，对这些接口做了频繁访问检测，当检测到后会返回如上错误信息。

## 1.1 API认证

API采用[JWT](https://jwt.io/)方式进行认证，注册用户通过API登录后，会返回`app_api_token`使用JWT解析后，Payload中包含用户ID、用户名和设备ID信息。

获取Token后，请求其他需要认证的接口(接口名后带有'认证'表示需要认证)时，需要在请求Headers中加入Token的来进行认证，例如:

`Authorization: eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpZCI6MTMsInRlbGVwaG9uZSI6IjEzNTQ4NzM0ODY5In0.wsicjva-hSN_jLIW5PSxdkmmHxz_luBYp8A4XAY9zAY`

验证token的时候会同时验证用户帐号是否被停用，错误码如下:

错误码 | 说明
---- | ----
101 | Token验证失败
102 | 用户帐号被停用

例如Token验证失败，会返回如下错误信息:

```json
{
  "success": false,
  "message": "令牌验证错误",
  "error_code": 101
}
```

## 1.2 客户端连接代理服务器的说明

客户端连接代理服务器需要的三个参数:

- `account_id`: 对应[登录](#part-2d3acd4b9c7edaff)接口返回数据中的`user.id`
- `sesson_id`: 对应[登录](#part-2d3acd4b9c7edaff)接口返回数据中的`proxy_session_id`
- `token`: 对应[连接指定服务器线路](#part-9b8708ba4fd47fbf)接口返回数据中的`proxy_session_token`

客户端用户连接某一线路的时候，首先要调用一下`连接指定服务器线路`接口，才能拿到代理服务器的token

## 1.3 获取手机短信验证码

<aside class="success">
此接口已更新新版: <a href="/docs/client_v2">v2</a>
</aside>

此接口会向客户端指定的手机发送短信验证码。

```json
{
  "success": true,
  "messages": "验证码已发送",
  "data": {}
}
```

### HTTP请求

`POST /api/client/v1/send_verification_code`

### 请求参数

参数 | 类型 | 必须 | 说明
---- | ---- | ---- | ----
telephone | string | true | 手机号
type | integer | true | 验证码的使用场景 0:修改用户名, 1:修改密码, 2:找回密码, 3:注册帐号

### 错误码

错误码 | 说明
---- | ----
1 | 验证码发送过于频繁
2 | 验证码发送失败，请重试

## 1.4 用户登录

<aside class="success">
此接口已弃用，请查看新版: <a href="/docs/client_v4">v4</a>
</aside>

关于 `app_launch_id` 的说明:

- 用户全新运行客户端(后台没有运行的前提下)，调登录接口时，不传`app_launch_id`参数
- 用户按Home键将客户端切至后台运行，一段时间后，再次打开客户端(从后台切出)，调登录接口时，传 `app_launch_id`参数，值为上一次全新打开客户端调登录接口时返回的值

> 当只使用uuid登录，并且此设备已经修改过用户名或密码时，返回的数据如下:

```json
{
  "success": true,
  "message": null,
  "data": {
    "only_has_username": true,
    "username": "13584634396"
  }
}
```

> 其他情况，则返回:

```json
{
  "success": true,
  "message": null,
  "data": {
    "only_has_username": false,
    "proxy_session_id": 163,
    "proxy_session_token": "cz6lFjMdpo",
    "app_launch_id": "823ecb84-1b72-11e7-845d-784f4352f7e7",
    "signin_log_id": "823adbaa-1b72-11e7-845d-784f4352f7e7",
    "user": {
      "api_token": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpZCI6MTYzLCJ1c2VybmFtZSI6InN1cDAwMDAwMTYzIiwiZGV2aWNlX2lkIjoxNjF9.yERJ8mfGk4MXuQUQWt8JT32x16WLetoonoNBZ0Y9nNg",
      "id": 163,
      "username": "sup00000163",
      "password": "kvsxvzz7",
      "email": null,
      "wechat": null,
      "weibo": null,
      "qq": null,
      "used_bytes": 0,
      "max_bytes": 0,
      "limit_bytes": 0,
      "total_payment_amount": "0.0",
      "current_coins": 0,
      "total_coins": 0,
      "is_checkin_today": false,
      "is_enabled": true,
      "created_at": 1491556428,
      "new_message": false
    },
    "group": {
      "id": 1,
      "name": "vip0",
      "level": 1,
      "next_name": "VIP1",
      "next_id": 2,
      "next_need_coins": 0
    },
    "device": {
      "id": 161,
      "uuid": "a1de3867-7468-4002-8dd4-554707c4edc5"
    },  
    "user_node_types": [
      {
        "id": 811,
        "name": "青铜服",
        "level": 1,
        "status": "按次",
        "expired_at": 1491556428,
        "used_count": 0,
        "node_type_id": 1
      },
      {
        "id": 815,
        "name": "钻石服",
        "level": 5,
        "status": "按次",
        "expired_at": 1491556428,
        "used_count": 0,
        "node_type_id": 5
      }
    ],
    "node_types": [
      {
        "id": 1,
        "name": "青铜服",
        "level": 1,
        "expense_coins": 1,
        "user_group_id": 1,
        "user_group_level": 1,
        "node_regions": [
          {
            "id": 3,
            "name": "美国",
            "abbr": "us",
            "nodes": [
              {
                "id": 7,
                "name": "美国01",
                "url": "120.77.245.45",
                "max_connections_count": 300,
                "connections_count": 0
              },
              {
                "id": 9,
                "name": "美国03",
                "url": "120.77.245.45",
                "max_connections_count": 300,
                "connections_count": 0
              }
            ]
          }
        ]
      },
      {
        "id": 3,
        "name": "黄金服",
        "level": 3,
        "expense_coins": 5,
        "user_group_id": 5,
        "user_group_level": 5,
        "node_regions": [
          {
            "id": 2,
            "name": "日本",
            "abbr": "jp",
            "nodes": [
              {
                "id": 6,
                "name": "日本03",
                "url": "120.77.245.45",
                "max_connections_count": 300,
                "connections_count": 0
              }
            ]
          }
        ]
      },
      {
        "id": 5,
        "name": "钻石服",
        "level": 5,
        "expense_coins": 20,
        "user_group_id": 9,
        "user_group_level": 9,
        "node_regions": []
      }
    ],
    "settings": {
      "APPSTORE_ID": "1206882364",
      "IOS_VERSION": "1.0.3|1",
      "SHARE_IMG": "http://120.77.247.50/ui/img/logo.png",
      "IS_UPDATE": "1"
    }
  }
}
```

### HTTP请求

`POST /api/client/v1/signin`

### 请求参数

参数 | 类型 | 必须 | 说明
---- | ---- | ---- | ----
username | string | false | 手机号
password | string | false | 登录密码
device_uuid | string | true | 设备uuid
device_name | string | false | 设备名(如: xxx的iphone)
device_model | string | true | 设备型号(如: iphone5)
platform | string | true | 设备系统平台(如: ios)
system_version | string | true | 设备版本(如: 10)
operator | string | true | 网络运营商(如: unicomm)
net_env | string | true | 网络环境(如: 4g、wifi)
app_version | string | true | 版本名，来自"版本渠道信息接口"获取到的ID
app_version_number | string | true | App版本(如: 1.0)
app_channel | string | true | app渠道, 来自"版本渠道信息接口"获取到的ID
app_launch_id | string | false | 用于记录App运行日志, 此值只在App的一次运行周期内有效，否则不在请求中加入此参数
longitude | string | false | 坐标经度
latitude | string | false | 坐标纬度
address | string | false | 坐标地址
coord_method | string | false | 坐标获取方法(如: ip、gps)


### 返回信息

如果请求不成功，返回的数据会包含一个`integer`型的`error_code`字段，用来代表出错类型:


错误代码 | 说明
---- | ----
1 | 用户不存在
2 | 用户被封号
3 | 用户名或密码错误
23 | 请求被拒绝(防刷)

字段名 | 说明
---- | ----
only_has_username | 用来在只使用uuid登录时表示此设备是否修改过用户名或密码，如果为true，则只返回修改过的用户名，如果为false，则视为正常登录返回所有用户信息
proxy_session_id | 用于连接代理服务器时使用
proxy_session_token | 用于连接代理服务器时使用
app_launch_id | 用于记录App运行日志, 需客户端在App一次运行周期内保存，并在登录请求中加入此参数
signin_log_id | 用户登录日志ID，在请求操作日志等接口时使用
user.api_token | 之后API调用需认证的接口时所需要的token(目前还未设置过期策略)
user.id | 用户id
user.username | 用户的用户名，用户自行修改用户名后，会用来存储手机号
user.email | 用户绑定的email
user.wechat | 用户绑定的微信的openid
user.weibo | 用户绑定的微博的openid
user.qq | 用户绑定的qq的openid
user.used_bytes | 用户已使用的流量(M)
user.max_bytes | 用户可用的最大流量(byte), -1: 无限制
user.limit_bytes | 用户被限制的速度(KB/s), -1: 无限制
user.total_payment_amount | 用户的消费累计金额
user.current_coins | 用户当前钻石数
user.total_coins | 用户拥有过的总钻石数
user.is_checkin_today | 用户今天是否签到过, true: 是, false: 否
user.is_enabled | 用户帐号状态(如: 被封号)，true:正常, false:已封号
user.created_at | 帐号创建时间
user.new_message | 是否有新消息, true: 是, false: 否
group.id | 用户组id
group.name | 用户组名称
group.level | 用户组等级
group.next_name | 下一级别用户类型的名称，此字段在用户达到最高等级时，值会为`null`
group.next_id | 下一级别用户类型的id，此字段在用户达到最高等级时，值会为`null`
group.next_need_coins | 下一用户类型等级需要的钻石数(如果到达顶级，则为 -1)
device.id | 当前设备ID
device.uuid | 当前设备uuid
regions.id | 服务器节点区域ID
user_node_types.id | 用户服务器类型ID
user_node_types.name | 用户服务器类型名称
user_node_types.level | 用户服务器类型级别
user_node_types.status | 用户服务器类型状态
user_node_types.expired_at | 用户服务器类型过期时间
user_node_types.used_count | 用户服务器类型使用次数
user_node_types.node_type_id | 服务器类型ID，用来对应服务器线路列表中的node_type的ID
node_types.id | 服务器类型ID
node_types.name | 服务器类型名称
node_types.level | 服务器类型等级
node_types.expense_coins | 连接此服务器类型需要扣除了钻石数
node_types.user_group_id | 连接此服务器类型所需要的最低用户类型ID
node_types.user_group_level | 连接此服务器类型所需要的最低用户类型等级
node_types.node_regions.id | 服务器所在区域ID
node_types.node_regions.name | 服务器所在区域名称
node_types.node_regions.abbr | 服务器所在区域国家简写[可在此查看ISO 3166-1](https://zh.wikipedia.org/wiki/ISO_3166-1)，客户端可根据此简写来设置不同的图标
node_types.node_regions.nodes.id | 服务器ID
node_types.node_regions.nodes.name | 服务器名称
node_types.node_regions.nodes.url | 服务器连接地址或IP
node_types.node_regions.nodes.port | 服务器端口
node_types.node_regions.nodes.password | 服务器密码
node_types.node_regions.nodes.encrypt_method | 服务器加密方法
node_types.node_regions.nodes.connections_count | 服务器连接数
node_types.node_regions.nodes.max_connections_count | 服务器最大连接数
settings | 后台管理配置的参数，具体配置项由开发者自行制定

## 1.5 注册用户修改用户名为手机号(认证)

<aside class="success">
此接口已更新新版: <a href="/docs/client_v2">v2</a>
</aside>

```json
{
  "success": true,
  "data": {
    "api_token": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpZCI6MTYxLCJ1c2VybmFtZSI6IjEzODg0NjMzMzk2IiwiZGV2aWNlX2lkIjoxNjF9.GgoijsH0qmyMWQrg_s_EXcZsXcoJi1U7qsBUfncQL4U"
  }
}
```

### HTTP请求

`POST /api/client/v1/users/update_username`

### 请求参数

参数 | 类型 | 必须 | 说明
---- | ---- | ---- | ----
telephone | string | true | 手机号
verification_code | string | true | 短信验证码

### 返回信息

字段名 | 说明
---- | ----
api_token | 修改用户名后重新生成的token(因为token里包含用户名，所以需要重新生成)

## 1.6 注册用户修改密码(认证)

只有修改了用户名为手机号后，才可以修改密码或找回密码，在调用前需调用获取短信验证码接口。

```json
{
  "success": true,
  "message": null,
  "data": {
    "api_token": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpZCI6MTYxLCJ1c2VybmFtZSI6IjEzODg0NjMzMzk2IiwiZGV2aWNlX2lkIjoxNjF9.GgoijsH0qmyMWQrg_s_EXcZsXcoJi1U7qsBUfncQL4U"
  }
}
```

### HTTP请求

`POST /api/client/v1/users/update_password`

### 请求参数

参数 | 类型 | 必须 | 说明
---- | ---- | ---- | ----
verification_code | string | true | 短信验证码
password | string | true | 新密码

### 返回信息

字段名 | 说明
---- | ----
api_token | 重新生成的token

## 1.7 用户找回密码

只有修改了用户名为手机号的用户才可以找回密码

```json
{
  "success": true,
  "message": null,
  "data": {}
}
```

### HTTP请求

`POST /api/client/v1/reset_password`

### 请求参数

参数 | 类型 | 必须 | 说明
---- | ---- | ---- | ----
telephone | string | true | 手机号
verification_code | string | true | 短信验证码
password | string | true | 新密码

## 1.8 完善资料绑定社交帐号(认证)

此接口必须指定至少一个绑定帐号。

在所有帐号绑定完成后，后端将升级用户组信息，并返回用户相关信息(在未绑定所有帐号时，返回信息中不包含用户信息)。

```json
{
  "success": true,
  "data": {}
}
```

### HTTP请求

`POST /api/client/v1/users/update_profile`

### 请求参数

参数 | 类型 | 必须 | 说明
---- | ---- | ---- | ----
email | string | false | 用户邮箱
wechat | string | false | 微信的openid
weibo | string | false | 微博openid
qq | string | false | qq的openid

### 返回信息

参考[用户登录](http://localhost:3001/docs/index#part-2d3acd4b9c7edaff)接口的返回参数

## 1.9 提交用户分享日志(认证)

<aside class="success">
此接口已更新新版: <a href="/docs/client_v2">v2</a>
</aside>

本接口用于在用户分享app后，记录用户的分享操作。

```json
{
  "success": true,
  "data": {}
}
```

### HTTP请求

`POST /api/client/v1/users/share`

### 请求参数

参数 | 类型 | 必须 | 说明
---- | ---- | ---- | ----
app_channel | string | true | app渠道, 如: moren
app_version | string | true | app版本名, 如: jichu
app_version_number | string | true | app版本号, 如: 0.1
share_type | integer | true | 分享类型: 0:微信, 1:朋友圈, 2:QQ, 3:微博

## 1.10 用户签到(认证)

```json
{
  "success": true,
  "message": null,
  "data": {
    "has_checkin_today": false,
    "expired_at": 1492167559,
    "days_of_month": 1,
    "checkin_days": 1,
    "last_checkin_at": 1492163959,
    "present_time": 1,
    "calendar": [
      14
    ]
  }
}
```

### HTTP请求

`POST /api/client/v1/users/checkin`

### 返回信息

字段名 | 说明
---- | ----
has_checkin_today | 用户今天是否已签到
expired_at | 用户签到并奖励时间后的青铜服务器类型的新过期时间
days_of_month | 本月签到的天数
checkin_days | 连续签到的天数
last_checkin_at | 最后签到日期
present_time | 赠送时间(小时)
calendar | 本月签到的日期列表(Array)

## 1.11 提交操作日志

本接口用于记录用户的界面操作。

```json
{
  "success": true,
  "data": {}
}
```

### HTTP请求

`POST /api/client/v1/users/operation_log`

### 请求参数

参数 | 类型 | 必须 | 说明
---- | ---- | ---- | ----
app_launch_id | string | false | App运行日志ID, 此参数在客户端1.0.5版的时候已经弃用，不需要再传此参数
signin_log_id | string | false | 用户登录日志ID，此参数在客户端1.0.5版的时候已经弃用，不需要再传此参数
app_channel | string | true | app渠道, 如: moren
app_version | string | true | app版本名, 如: jichu
app_version_number | string | true | app版本号, 如: 0.1
operations | string | true | 由一个或多个操作数据组成的字符, 格式为: "操作ID,用户ID,时间&#124;操作id,用户ID,时间" 如: 0,91,146528455062&#124;2,89,146528455069

**操作ID**

序号 | 界面名
---- | ----
1 | 启动页
2 | 主界面
3 | 线路列表
4 | 服务器有效期
5 | 签到界面
6 | 购买界面
7 | 帮助界面
8 | 我的反馈界面
9 | 反馈记录界面
10 | 用户中心界面
11 | 修改账号界面
12 | 修改密码界面
13 | 分享界面
14 | 关于我们界面
15 | 登录界面
16 | 找回密码界面
17 | Today界面
18 | 注册界面


## 1.12 获得所有服务器线路数据(认证)

<aside class="success">
此接口已更新新版: <a href="/docs/client_v3">v3</a>
</aside>

```json
{
  "success": true,
  "message": null,
  "data": {
    "user_node_types": [
      {
        "id": 811,
        "name": "青铜服",
        "level": 1,
        "status": "按次",
        "expired_at": 1491556428,
        "used_count": 0,
        "node_type_id": 1
      },
      {
        "id": 815,
        "name": "钻石服",
        "level": 5,
        "status": "按次",
        "expired_at": 1491556428,
        "used_count": 0,
        "node_type_id": 5
      }
    ],
    "node_types": [
      {
        "id": 1,
        "name": "青铜服",
        "level": 1,
        "expense_coins": 1,
        "user_group_id": 1,
        "user_group_level": 1,
        "description": "测试1",
        "node_regions": [
          {
            "id": 3,
            "name": "美国",
            "abbr": "us",
            "nodes": [
              {
                "id": 8,
                "name": "美国02",
                "url": "120.77.245.45",
                "max_connections_count": 300,
                "connections_count": 0
              },
              {
                "id": 9,
                "name": "美国03",
                "url": "120.77.245.45",
                "max_connections_count": 300,
                "connections_count": 0
              }
            ]
          }
        ]
      },
      {
        "id": 3,
        "name": "黄金服",
        "level": 3,
        "expense_coins": 5,
        "user_group_id": 5,
        "user_group_level": 5,
        "description": "测试1",
        "node_regions": [
          {
            "id": 2,
            "name": "日本",
            "abbr": "jp",
            "nodes": [
              {
                "id": 6,
                "name": "日本03",
                "url": "120.77.245.45",
                "max_connections_count": 300,
                "connections_count": 0
              }
            ]
          }
        ]
      },
      {
        "id": 5,
        "name": "钻石服",
        "level": 5,
        "expense_coins": 20,
        "user_group_id": 9,
        "user_group_level": 9,
        "description": "测试1",
        "node_regions": []
      }
    ]
  }
}
```

### HTTP请求

`GET /api/client/v1/nodes`

### 返回信息

字段名 | 说明
---- | ----
user_node_types.id | 用户服务器类型ID
user_node_types.name | 用户服务器类型名称
user_node_types.level | 用户服务器类型级别
user_node_types.status | 用户服务器类型状态
user_node_types.expired_at | 用户服务器类型过期时间
user_node_types.used_count | 用户服务器类型使用次数
user_node_types.node_type_id | 服务器类型ID，用来对应服务器线路列表中的node_type的ID
node_types.id | 服务器类型ID
node_types.name | 服务器类型名称
node_types.level | 服务器类型等级
node_types.expense_coins | 连接此服务器类型需要扣除了钻石数
node_types.user_group_id | 连接此服务器类型所需要的最低用户类型ID
node_types.user_group_level | 连接此服务器类型所需要的最低用户类型等级
node_types.description | 服务类型描述，字符中的&#124;表示换行
node_types.node_regions.id | 服务器所在区域ID
node_types.node_regions.name | 服务器所在区域名称
node_types.node_regions.abbr | 服务器所在区域国家简写[可在此查看ISO 3166-1](https://zh.wikipedia.org/wiki/ISO_3166-1)，客户端可根据此缩写来来自行拼接获取图片的URL，如: `https://xiaoguoqi.com/flags/us.png`
node_types.node_regions.nodes.id | 服务器ID
node_types.node_regions.nodes.name | 服务器名称
node_types.node_regions.nodes.url | 服务器连接地址或IP
node_types.node_regions.nodes.port | 服务器端口
node_types.node_regions.nodes.password | 服务器密码
node_types.node_regions.nodes.encrypt_method | 服务器加密方法
node_types.node_regions.nodes.connections_count | 服务器连接数
node_types.node_regions.nodes.max_connections_count | 服务器最大连接数

## 1.13 提交服务器连接日志(认证)

本接口用于记录用户连接服务器时的日志，包括连接等待时间，是否连接成功等。

```json
{
  "success": true,
  "data": {}
}
```

### HTTP请求

`POST /api/client/v1/nodes/connection_log`

### 请求参数

参数 | 类型 | 必须 | 说明
---- | ---- | ---- | ----
node_id | integer | true | 连接的服务器线路ID(此ID是从1.15接口返回的node节点中的id取得)
wait_time | integer | true | 连接时的等待时间(秒)
success | integer | true | 是否连接成功 0:失败 1:成功
domain | string | false | 当前客户端连接后端api使用的域名，注意不需要加`http://`，域名形式如: `xxx.com`

## 1.14 获取动态服务器信息

动态服务器是指后端提供的一批备用服务器地址，用来防止后端服务器被封。

使用方法参见前端策划案关于“客户端连接地址功能”一节。

```json
{
  "success": true,
  "data": {
    "servers": [
      {
        "id": 1,
        "url": "https://xiaoguoqi.com/",
        "region": "香港",
        "priority": 1
      },
      {
        "id": 2,
        "url": "https://192.168.1.45/",
        "region": "中国",
        "priority": 2
      }
    ]
  }
}
```

### HTTP请求

`GET /api/client/v1/dynamic_servers`

### 返回信息

字段名 | 说明
---- | ----
servers.id | 服务器ID
servers.url | 服务器URL
servers.region | 服务器区域
servers.priority | 优先级

## 1.15 按指定服务器类型连接(认证)

<aside class="success">
此接口已更新新版: <a href="/docs/client_v2">v2</a>
</aside>

> 结果1: 用户服务器类型未过期时

```json
{
  "success": true,
  "message": null,
  "data": {
    "user_node_type_is_expired": false,
    "is_consumed": false,
    "proxy_session_token": "WU3sxzjK3RmMExXsSVwB",
    "user": {
      "current_coins": 9958
    },
    "user_node_type": {
      "id": 616,
      "expired_at": 1493203060,
      "status": "按次",
      "used_count": 3
    },
    "node": {
      "id": 1,
      "name": "香港01",
      "url": "119.23.64.170",
      "port": 8388,
      "encrypt_method": "aes-128-cfb",
      "password": "asdf"
    }
  }
}
```

> 结果2: 用户服务器类型已过期，但客户端还未确认扣除钻石时

```json
{
  "success": true,
  "message": null,
  "data": {
    "user_node_type_is_expired": true,
    "is_consumed": false
  }
}
```

> 结果3: 用户服务器类型已过期，客户端确认扣除钻石时

```json
{
  "success": true,
  "message": null,
  "data": {
    "user_node_type_is_expired": true,
    "is_consumed": true,
    "proxy_session_token": "WU3sxzjK3RmMExXsSVwB",
    "user": {
      "current_coins": 9957
    },
    "user_node_type": {
      "id": 616,
      "expired_at": 1492511860,
      "status": "按次",
      "used_count": 3
    },
    "node": {
      "id": 2,
      "name": "香港02",
      "url": "119.23.64.170",
      "port": 8388,
      "encrypt_method": "aes-128-cfb",
      "password": "asdf"
    }
  }
}
```

### HTTP请求

`POST /api/client/v1/nodes/connect`

### 请求参数

参数 | 类型 | 必须 | 说明
---- | ---- | ---- | ----
node_type_id | integer | true | 服务器类型ID(告诉后端要连接哪个类型的服务器)
allow_consume | integer | false | 是否允许扣除钻石(1:扣除,0:不扣)，默认可不加此参数，当连接的服务器类型已过期，服务器会返回是否需要扣除钻石，当用户确认要扣除时，再次请求此接口，并加上此参数

### 返回信息

返回信息的三种状态是根据`user_node_type_is_expired`和`is_consumed`来判断的:

1. 服务器类型未过期
  - `user_node_type_is_expired: false`
  - `is_consumed: false`
2. 服务器类型已过期，需要客户端确认扣除钻石
  - `user_node_type_is_expired: true`
  - `is_consumed: false`
3. 服务器类型已过期，客户端确认扣除钻石
  - `user_node_type_is_expired: true`
  - `is_consumed: true`

字段名 | 说明
---- | ----
user_node_type_is_expired | 用户指定要连接的服务器类型是否已过期
is_consumed | 是否已扣除钻石
proxy_session_token | 连接代理服务器时使用的token，把token放在这里是因为后端会检查在线的用户服务是否到期，到期后会重置token
user.current_coins | 用户当前钻石数
user_node_type.id | 用户服务器类型ID
user_node_type.expired_at | 用户服务器类型新的到期时间
user_node_type.status | 用户服务器类型状态
user_node_type.used_count | 用户服务器类型本月的使用次数
node.id | 代理服务器ID
node.name | 代理服务器名
node.url | 代理服务器地址
node.port | 代理服务器端口
node.encrypt_method | 代理服务器加密方式
node.password | 代理服务器密码

### 错误码

错误码 | 说明
---- | ----
1 | 服务器类型不存在
2 | 您的VIP等级太低
3 | 您的钻石不足
4 | 没有可用的服务器线路

## 1.16 获得套餐数据(认证)

<aside class="success">
此接口已弃用，请查看新版: <a href="/docs/client_v2#1.16">v2</a>
</aside>

```json
{
  "success": true,
  "data": {
    "plans": [
      {
        "id": 1,
        "name": "体验套餐",
        "is_regular_time": false,
        "price": "1.0",
        "coins": 1,
        "present_coins": 0,
        "description": "这是一个体验套餐，1块钱1钻石"
      },
      {
        "id": 6,
        "name": "豪华套餐",
        "is_regular_time": false,
        "price": "10.0",
        "coins": 10,
        "present_coins": 6,
        "description": "这是一个豪华套餐，可以使用半个月"
      }
    ]
  }
}
```

### HTTP请求

`GET /api/client/v1/plans`

### 请求参数

如果不加参数，则返回默认套餐

如果加参数，则根据参数返回指定客户端版本及版本号的套餐(前提是后台管理做了相关配置)，主要用于在iOS审核期间返回指定套餐，用于保证审核通过。

参数 | 类型 | 必须 | 说明
---- | ---- | ---- | ----
platform | string | false | 平台
app_version | string | false | 客户端版本名
app_version_number | string | false | 客户端版本号

### 返回信息

字段名 | 说明
---- | ----
plans.id | 套餐ID
plans.name | 套餐名
plans.is_regular_time | 是否是包时间套餐, true:是/false:否
plans.price | 套餐金额
plans.coins | 套餐包含的钻石数
plans.present_coins | 套餐赠送的钻石数
plans.description | 套餐说明
plans.iap_id | IAP套餐ID，此字段只在是IAP套餐时显示

## 1.17 获得苹果平台套餐数据(认证)

```json
{
  "success": true,
  "message": null,
  "data": {
    "plans": [
      {
        "id": 4,
        "name": "体验套餐",
        "price": "1.0",
        "coins": 1,
        "present_coins": 0,
        "description": "这是一个体验套餐，1块钱1钻石",
        "iap_id": "plan01"
      },
      {
        "id": 6,
        "name": "豪华套餐",
        "price": "10.0",
        "coins": 10,
        "present_coins": 6,
        "description": "这是一个豪华套餐，可以使用半个月",
        "iap_id": "plan03"
      }
    ]
  }
}
```

### HTTP请求

`GET /api/client/v1/plans/iap`

### 请求参数

如果不加参数，则返回默认套餐

如果加参数，则根据参数返回指定客户端版本及版本号的套餐(前提是后台管理做了相关配置)，主要用于在iOS审核期间返回指定套餐，用于保证审核通过。

参数 | 类型 | 必须 | 说明
---- | ---- | ---- | ----
platform | string | false | 平台
app_version | string | false | 客户端版本名
app_version_number | string | false | 客户端版本号

### 返回信息

字段名 | 说明
---- | ----
plans.id | 套餐ID
plans.name | 套餐名
plans.price | 套餐金额
plans.coins | 套餐包含的钻石数
plans.present_coins | 套餐赠送的钻石数
plans.description | 套餐说明
plans.iap_id | 对应IAP内购项目的ID

## 1.18 苹果平台支付二次验证(认证)

<aside class="success">
此接口已更新新版: <a href="/docs/client_v2">v2</a>
</aside>

```json
{
  "success": true,
  "message": null,
  "data": {
    "user": {
      "current_coins": 10
    },
    "group": {
      "id": 3,
      "name": "VIP2",
      "level": 3,
      "need_coins": 10
    }
  }
}
```

### HTTP请求

`POST /api/client/v1/plans/verify_iap`

### 请求参数

参数 | 类型 | 必须 | 说明
---- | ---- | ---- | ----
app_version | string | false | 客户端版本，用来根据版本来区别是否使用审核版本的套餐
app_version_number | string | true | 客户端版本号，用来兼容旧版本的支付和是否使用审核版本的套餐
receipt_data | string | true | 购买凭证(base64形式)

### 返回信息

字段名 | 说明
---- | ----
user.current_coins | 用户当前的钻石数
group.id | 用户类型ID(用户钻石累记达到一定数量，会升级成相应的用户类型)
group.name | 用户类型名
group.level | 用户类型等级
group.need_coins | 升级到此类型需要的累计钻石数

## 1.19 创建Comsunny支付订单(H5页面)(认证)

<aside class="success">
此接口已更新新版: <a href="/docs/client_v4">v4</a>
</aside>

流程:

- 客户端使用此接口创建订单，获取`订单号` 和 `h5支付页面URL`
- 客户端打开 `h5支付页面` 进行支付
- 支付平台向`客户端`同步返回支付结果，同时向`后端服务器`异步发送支付结果，后端webhook地址: `{域名}/api/client/v1/comsunny_callback`
- 客户端在收到支付结果后，使用`后端订单号`向后端编号`1.20`接口查询订单状态并获得用户最新状态

```json
{
  "success": true,
  "message": null,
  "data": {
    "transaction_log": {
      "order_number": "20170527149586928854346b6",
      "payment_url": "https://dispatch.5ydoc.com/Api/getOrderView?order_id=20170527149586928854346b6"
    }
  }
}
```

### HTTP请求

`POST /api/client/v1/plans/comsunny_order`

### 请求参数

参数 | 类型 | 必须 | 说明
---- | ---- | ---- | ----
plan_id | integer | true | 套餐ID

### 返回信息

字段名 | 说明
---- | ----
transaction_log.order_number | 订单号
transaction_log.payment_url | h5支付页面URL

## 1.20 根据订单号获取订单状态(认证)

此接口用于客户端根据订单号获取订单状态
如果订单完成，则返回用户的钻石和用户类型等信息
如果未完成，则显示订单未完成

> 订单完成后的返回数据

```json
{
  "success": true,
  "message": null,
  "data": {
    "is_completed": true,
    "user": {
      "current_coins": 3
    },
    "group": {
      "id": 1,
      "name": "vip0",
      "level": 1,
      "need_coins": 0
    }
  }
}
```

> 订单未完成的返回信息

```json
{
  "success": true,
  "message": null,
  "data": {
    "is_completed": false
  }
}
```

### HTTP请求

`GET /api/client/v1/plans/transaction_status`

### 请求参数

参数 | 类型 | 必须 | 说明
---- | ---- | ---- | ----
order_number | string | true | 订单号

### 返回信息

字段名 | 说明
---- | ----
is_completed | 订单是否完成
user.current_coins | 用户当前的钻石数
group.id | 用户类型ID(用户钻石累记达到一定数量，会升级成相应的用户类型)
group.name | 用户类型名
group.level | 用户类型等级
group.need_coins | 升级到此类型需要的累计钻石数

## 1.21 获得公告数据(认证)

<aside class="success">
此接口已更新新版: <a href="/docs/client_v2">v2</a>
</aside>

获得最新的一条公告内容，客户端可根据公告ID来记录在本地判断以后是否显示此公告。
如果没有公告，`notice`将为`null`

```json
{
  "success": true,
  "data": {
    "notice": {
      "id": 2,
      "content": "诺贝尔化学奖 （瑞典语：Nobelpriset i kemi）是诺贝尔奖的六个奖项之一，1895年设立，由瑞典皇家科学院每年..",
      "created_at": 1489995466
    }
  }
}
```

### HTTP请求

`GET /api/client/v1/notices`

### 返回信息

字段名 | 说明
---- | ----
notice.id | 公告ID
regions.content | 公告内容
regions.created_at | 公告创建时间

## 1.22 获取反馈列表(认证)

<aside class="success">
此接口已更新新版: <a href="/docs/client_v2">v2</a>
</aside>

```json
{
  "success": true,
  "data": {
    "feedbacks": [
      {
        "id": 1,
        "type": 1,
        "content": "程序经常闪退，怎么办？",
        "is_processed": false,
        "processed_at": 0,
        "reply_content": null,
        "created_at": 1490252180,
        "has_read": true,
        "has_read_at": null
      }
    ],
    "current_page": 1,
    "total_pages": 1
  }
}
```

### HTTP请求

`GET /api/client/v1/feedbacks`

### 请求参数

参数 | 类型 | 必须 | 说明
---- | ---- | ---- | ----
page | integer | false | 指定页码, 默认为1
limit | integer | false | 指定每页的条数，默认为20

### 返回信息

字段名 | 说明
---- | ----
feedbacks.id | 反馈ID
feedbacks.type | 反馈类型, 0:连接问题, 1:充值问题, 2:帐号问题, 3:其他问题
feedbacks.content | 反馈内容
feedbacks.is_processed | 管理员是否已处理
feedbacks.processed_at | 处理时间，未处理的话此处为 null
feedbacks.reply_content | 回复内容
feedbacks.created_at | 反馈创建的时间
feedbacks.has_read | 用户是否已读
feedbacks.has_read_at | 用户已读的时间
current_page | 当前页码
total_pages | 总页码数

## 1.23 提交反馈(认证)

此接口创建用户反馈，其中服务器会限制每个用户每天提交的数量，达到上限后会返回错误信息。

```json
{
  "success": true,
  "messages": "反馈已发送",
  "data": {}
}
```

### HTTP请求

`POST /api/client/v1/feedbacks`

### 请求参数

参数 | 类型 | 必须 | 说明
---- | ---- | ---- | ----
feedback_type | integer | true | 反馈类型, 0:连接问题, 1:充值问题, 2:帐号问题, 3:其他问题
app_version | string | true | 版本名，如: jichu
app_version_number | string | true | App版本, 如: 1.0
app_channel | string | true | app渠道, 如: moren
content | string | true | 反馈内容
email | string | false | 联系邮箱

## 1.24 设置指定反馈为已读(认证)

此接口目前备用,无需调用，在调用`1.46`接口时，会设置指定反馈为已读。

```json
{
  "success": true,
  "data": {}
}
```

### HTTP请求

`POST /api/client/v1/feedbacks/mark_as_read`

### 请求参数

参数 | 类型 | 必须 | 说明
---- | ---- | ---- | ----
id | integer | true | 要设置为已读的反馈的ID

## 1.25 提交连接失败信息

在客户端登录接口的 `settings` 接点数据中，有以下几个配置决定了错误日志提交功能的状态:

- `FLOG_ALLOW_SEND`: 是否允许客户端发送失败日志
- `FLOG_POOL_MAX_COUNT`: 客户端失败日志池累计到多少条后提交
- `FLOG_POST_INTERVAL`: 客户端提交失败日志间隔时间(秒)
- `FLOG_CLEAN_INTERVAL`: 客户端失败日志清理间隔时间(秒)

```json
{
  "success": true,
  "message": null,
  "data": {}
}
```

### HTTP请求

`POST /api/client/v1/failed_logs/connection_failed`

### 请求参数

参数 | 类型 | 必须 | 说明
---- | ---- | ---- | ----
uuid | string | true | 设备uuid
app_channel | string | true | 渠道
app_version | string | true | 客户端版本名
app_version_number | string | true | 客户端版本号
device_model | string | true | 客户端型号
operator | string | true | 运营商，pc端直接写pc
data | string | true | 由一个或多个数据组成的字符串

#### data说明

多个失败错误信息使用`|`连接，单个错误信息示例:

- 服务器连接失败: `1,sup00000003,1491805357,青铜服,美国,美国02`
- 软件崩溃: `2,sup00000003,1491805357,null,null,null`
- 未联网: `3,sup00000003,1491805357,null,null,null`

**数据项依次为:**

- 失败类型, 1: 服务器连接失败, 2: 软件崩溃, 3: 未联网
- 用户名
- 时间
- 服务器类型(非服务器连接失败类型时为null)
- 服务器区域(非服务器连接失败类型时为null)
- 服务器名称(非服务器连接失败类型时为null)

## 1.26 提交登录失败信息

<aside class="success">
关于影响失败信息提交的配置信息，请参阅 <a href="/index#1-25">1.25提交连接失败信息</a> 中的说明
</aside>

```json
{
  "success": true,
  "message": null,
  "data": {}
}
```

### HTTP请求

`POST /api/client/v1/failed_logs/signin_failed`

### 请求参数

参数 | 类型 | 必须 | 说明
---- | ---- | ---- | ----
uuid | string | true | 设备uuid
data | string | true | 由一个或多个数据组成的字符串

#### data说明

多个失败错误信息使用`|`连接，单个错误信息示例:

- `0,sup00000033,1492161325000|1,sup00000023,1492161345000|2,sup00000006,1492161445000|3,sup00000004,1492161345000`

**数据项依次为:**

- 失败类型, 0:帐号不存在, 1:密码错误, 2:连接失败, 3:软件崩溃
- 用户名
- 时间

## 1.27 提交未操作信息

此接口用来提交用户打开app后未操作就退出的日志

<aside class="success">
关于影响失败信息提交的配置信息，请参阅 <a href="/index#1-25">1.25提交连接失败信息</a> 中的说明
</aside>

```json
{
  "success": true,
  "message": null,
  "data": {}
}
```

### HTTP请求

`POST /api/client/v1/failed_logs/no_operation`

### 请求参数

参数 | 类型 | 必须 | 说明
---- | ---- | ---- | ----
uuid | string | true | 设备uuid
app_channel | string | true | 渠道
app_version | string | true | 客户端版本名
app_version_number | string | true | 客户端版本号
device_model | string | true | 客户端型号
operator | string | true | 运营商，pc端直接写pc
data | string | true | 由一个或多个数据组成的字符串

#### data说明

多条信息使用`|`连接，如: `sup00000003,1492227130000|sup00000004,1492227330000|sup00000006,1492228330000`

**数据项依次为:**

- 用户名
- 时间

## 1.28 获得当前用户封号状态(认证)

```json
{
  "success": true,
  "message": null,
  "data": {
    "user": {
      "is_enabled": true
    }
  }
}
```
### HTTP请求

`GET /api/client/v1/commons`

### 返回信息

字段名 | 说明
---- | ----
user.is_enabled | 用户帐号状态, true:正常, false:已封号

## 1.29 设置用户离线

```json
{
  "success": true,
  "message": null,
  "data": {}
}
```

### HTTP请求

`POST /api/client/v1/users/offline`

### 请求参数

参数 | 类型 | 必须 | 说明
---- | ---- | ---- | ----
user_id | integer | true | 用户id

## 1.30 极光推送动态服务器结果

此接口用于接收由后台管理直接推送的动态服务器信息，防止客户端连接不到服务器

```json
{
   "type":0,
   "sent_time":1492745936,
   "servers":[
      {
         "url":"https://xiaoguoqi.com/",
         "region":"香港",
         "priority":1
      },
      {
         "url":"https://192.168.1.45/",
         "region":"中国",
         "priority":2
      }
   ]
}
```

### 推送信息说明

字段名 | 说明
---- | ----
type | 推送类型，0:动态ip推送
sent_time | 本次推送时间
servers.url | 动态服务器url
servers.region | 区域
servers.priority | 优先级，默认按正序排列，数字越小，优先级越高

## 1.31 获得当前用户最新信息(认证)

```json
{
  "success": true,
  "message": null,
  "data": {
    "user": {
      "current_coins": 9971,
      "promo_users_count": 1,
      "promo_coins_count": 12
    },
    "group": {
      "id": 1,
      "name": "vip0",
      "level": 1,
      "need_coins": 0,
      "next_name": "VIP1",
      "next_id": 2,
      "next_need_coins": 2
    },
    "user_node_types": [
      {
        "id": 318,
        "name": "精英服务",
        "level": 1,
        "status": "按次",
        "expired_at": 1500282169,
        "used_count": 0,
        "node_type_id": 1
      },
      {
        "id": 319,
        "name": "王者服务",
        "level": 2,
        "status": "按次",
        "expired_at": 1500282169,
        "used_count": 0,
        "node_type_id": 2
      }
    ]
  }
}
```

### HTTP请求

`GET /api/client/v1/users/profile`

### 返回信息

字段名 | 说明
---- | ----
user.current_coins | 当前用户钻石数
user.promo_users_count | 用户当前已经推广的人数
user.promo_coins_count | 用户累计获得的推广钻石数
group.id | 当前用户类型id
group.name | 当前用户类型名
group.level | 当前用户类型等级
group.next_name | 下一级别用户类型的名称，此字段在用户达到最高等级时，值会为`null`
group.next_id | 下一级别用户类型的id，此字段在用户达到最高等级时，值会为`null`
group.next_need_coins | 升级到下一级别用户类型还需要的钻石数，如果为-1，则说明已到达顶级
user_node_types.id | 用户服务器类型ID
user_node_types.name | 用户服务器类型名称
user_node_types.level | 用户服务器类型级别
user_node_types.status | 用户服务器类型状态
user_node_types.expired_at | 用户服务器类型过期时间
user_node_types.used_count | 用户服务器类型使用次数
user_node_types.node_type_id | 服务器类型ID，用来对应服务器线路列表中的node_type的ID

## 1.32 极光推送服务到期消息

### HTTP请求

`POST /api/client/v1/users/push_proxy_message`

### 请求参数

platform / app_version / app_version_number 三个参数是在后来的版本(2017.07.06)新加的，新版客户端必须传递这三个参数

此接口对三个参数没有做验证，如果不传，则会使用默认配置的推送key和secret

参数 | 类型 | 必须 | 说明
---- | ---- | ---- | ----
uuid | string | true | 极光单推需要的uuid
platform | string | true | 平台
app_version | string | true | 版本
app_version_number | string | true | 版本号

### 错误码

错误码 | 说明
---- | ----
1 | 后台管理没有配置对应此版本的推送key和secret
2 | 推送目标未找到

## 1.33 判断客户端是否是在国内

```json
{
  "success": true,
  "message": null,
  "data": {
    "is_domestic": true,
    "client_ip": "127.0.0.1"
  }
}
```

### HTTP请求

`GET /api/client/v1/commons/location`

### 返回信息

字段名 | 说明
---- | ----
is_domestic | 是否是国内
client_ip | 客户端的IP地址

## 1.34 获得客户端路由配置表

<aside class="success">
此接口已更新新版: <a href="/docs/client_v3">v3</a>
</aside>

```json
{
  "success": true,
  "message": null,
  "data": {
    "updated_at": 1493879936,
    "d2o": "routes/android_d2o",
    "o2d": "routes/android_o2d",
    "is_domestic": true,
    "china_ip_update_at": 1493948153,
    "china_ip_list": "routes/china_ip_list.txt",
    "foreign_ip_update_at": 1500078488,
    "foreign_ip_list": "routes/foreign_ip_list.txt"
  }
}
```

### HTTP请求

`GET /api/client/v1/commons/routes`

### 请求参数

参数 | 类型 | 必须 | 说明
---- | ---- | ---- | ----
platform | string | true | 平台, ios/android/pc/mac

### 返回信息

字段名 | 说明
---- | ----
updated_at | 最后更新时间
d2o | 国内翻国外的路由表
o2d | 国外翻国内的路由表
is_domestic | 是否是国内IP
china_ip_update_at | 国内ip列表文件最后更新时间
china_ip_list | 国内ip列表文件路径
foreign_ip_update_at | 国外ip列表文件最后更新时间
foreign_ip_list | 国外ip列表文件路径

## 1.35 提交动态服务器连接失败信息

<aside class="success">
关于影响失败信息提交的配置信息，请参阅 <a href="/index#1-25">1.25提交连接失败信息</a> 中的说明
</aside>

```json
{
  "success": true,
  "message": null,
  "data": {}
}
```

### HTTP请求

`POST /api/client/v1/failed_logs/dynamic_server_failed`

### 请求参数

参数 | 类型 | 必须 | 说明
---- | ---- | ---- | ----
app_channel | string | true | 渠道
app_version | string | true | 客户端版本名
app_version_number | string | true | 客户端版本号
device_model | string | true | 客户端型号
platform | string | true | 平台
operator | string | true | 运营商，pc端直接写pc
data | string | true | 由一个或多个数据组成的字符串

#### data说明

多条信息使用`|`连接，如: `1,https://xiaoguoqi.com/,1493884280000|2,https://xiaoguoqi.com/,1493884290000`

**数据项依次为:**

- 用户ID
- 动态服务器url(请确保与获取动态服务器信息时的url一致)
- 时间

## 1.36 提交代理服务器解析日志(认证)

```json
{
  "success": true,
  "message": null,
  "data": {}
}
```

### HTTP请求

`POST /api/client/v1/users/proxy_connection_logs`

### 请求参数

参数 | 类型 | 必须 | 说明
---- | ---- | ---- | ----
node_id | integer | true | 服务器id
description | string | true | 描述
data | string | true | 解析数据

## 1.37 自动为用户服务续期(认证)

此接口在代理服务器认证时返回用户服务过期（错误码:3）后，由客户端调用自动为用户扣除钻石，延长服务有效期。

如果用户服务未过期或者管理关闭了自动续费接口，此接口将不会进行续费。

<aside class="success">
之后客户端可能会加入开关按钮，让用户选择是否需要自动扣钻石。
如果用户选择否，则应提示到期并关闭代理连接，不应调用此接口。
</aside>

```json
{
  "success": true,
  "message": null,
  "data": {
    "user_node_type_id": 661,
    "user_node_type_expired_at": 1495005208,
    "current_coins": 9995,
    "is_renewd": true
  }
}
```

### HTTP请求

`POST /api/client/v1/users/renew`

### 请求参数

参数 | 类型 | 必须 | 说明
---- | ---- | ---- | ----
node_id | integer | true | 服务器id，此ID的值对应1.15接口返回的node节点里的id值

### 返回信息

错误代码 | 说明
---- | ----
3 | 钻石不足，请充值
4 | 后端关闭了自动续费开关

字段名 | 说明
---- | ----
user_node_type_id | 用户服务类型ID
user_node_type_expired_at | 用户服务类型有效期
current_coins | 用户当前钻石数
is_renewd | 本次请求是否扣除钻石延长有效期成功(如果服务未到期，则不会扣钻石，本字段会为false)

## 1.38 获得帮助手册列表(认证)

此接口对应客户端"帮助"功能的常见列表

```json
{
  "success": true,
  "message": null,
  "data": {
    "help_manuals": [
      {
        "id": 3,
        "title": "第一个标题3",
        "content": "3第一个内容"
      },
      {
        "id": 2,
        "title": "第一个标题",
        "content": "第一个内容"
      }
    ],
    "current_page": 1,
    "total_pages": 1,
    "total_count": 2
  }
}
```

### HTTP请求

`GET /api/client/v1/help_manuals`

### 请求参数

参数 | 类型 | 必须 | 说明
---- | ---- | ---- | ----
platform | string | true | 平台, 如: ios/android/pc
app_version | string | true | 版本, 如: jichu
app_version_number | string | true | 版本号, 如: 1.0.0
page | integer | false | 指定页码，不加此参数表示获取第一页
limit | integer | false | 指定每页的记录数，不加此参数默认获取最新添加的20条

### 返回信息

字段名 | 说明
---- | ----
help_manuals.id | 记录ID
help_manuals.title | 记录标题
help_manuals.content | 记录内容

## 1.39 创建Comsunny支付订单(二维码)(认证)

<aside class="success">
此接口已更新新版: <a href="/docs/client_v2#1.39">v2</a>
</aside>

流程:

- 客户端使用此接口创建订单，获取`订单号` 和 `支付宝支付二维码URL`
- 客户端显示 `二维码图片` 进行支付
- 支付平台向`客户端`同步返回支付结果，同时向`后端服务器`异步发送支付结果，后端webhook地址: `{域名}/api/client/v1/comsunny_callback`
- 客户端在收到支付结果后，使用`后端订单号`向后端编号`1.20`接口查询订单状态并获得用户最新状态

```json
{
  "success": true,
  "message": null,
  "data": {
    "transaction_log": {
      "order_number": "20170527149586928854346b6",
      "payment_url": "https://dispatch.5ydoc.com/uploads/1498102412000.png"
    }
  }
}
```

### HTTP请求

`POST /api/client/v1/plans/comsunny_order_qrcode`

### 请求参数

参数 | 类型 | 必须 | 说明
---- | ---- | ---- | ----
platform | string | true | 平台, 如: ios/android/pc
app_channel | string | true | 渠道，和登录接口一致
app_version | string | true | 版本，和登录接口一致
plan_id | integer | true | 套餐ID
type | string | false | 支付类型，由于要兼容老客户端，所以此参数为可选

支付类型可选值:

- `alipay_qrcode`:支付宝扫码
- `alipay_h5`:支付宝WAP
- `wx_qrcode`:微信扫码
- `wx_h5`:微信h5
- `qq`:QQ钱包

### 返回信息

字段名 | 说明
---- | ----
transaction_log.order_number | 订单号
transaction_log.payment_url | h5支付页面URL

## 1.40 代理服务器连接失败信息提交

此接口用于记录代理服务器连接失败时的信息, 并在达到指定条件后触发预警。

<aside class="success">
关于影响失败信息提交的配置信息，请参阅 <a href="/index#1-25">1.25提交连接失败信息</a> 中的说明
</aside>

```json
{
    "success": true,
    "message": null,
    "data": {}
}
```

### HTTP请求

`POST /api/client/v1/failed_logs/proxy_connection_failed`

### 请求参数

参数 | 类型 | 必须 | 说明
---- | ---- | ---- | ----
uuid | string | true | 客户端设备的uuid
app_channel | string | true | 渠道，和登录接口一致
app_version | string | true | 版本，和登录接口一致
app_version_number | string | true | 版本号，和登录接口一致
operator | string | true | 客户端运营商，与登录接口一样
data | string | true | 日志详细信息

### data信息说明

data由多条数据构成并使用`|`分割, 每条数据中的字段由`,`号分割

格式: `用户id,服务器id,服务器ip,目标网站URL,时间`

实例数据: `1,1,192.168.1.1,http://twitter.com,1499671130|1,2,192.168.1.1,http://twitter.com,1499671140`

## 1.41 客户端去向日志提交

此接口用于客户端连接网站时产生的详情日志

在客户端登录接口的 `settings` 接点数据中，有以下几个配置决定了去向日志的功能的状态:

- `DLOG_ALLOW_SEND`: 是否允许客户端发送去向日志
- `DLOG_POOL_MAX_COUNT`: 客户端去向日志池累计到多少条后提交
- `DLOG_POST_INTERVAL`: 客户端提交去向日志间隔时间(秒)

```json
{
    "success": true,
    "message": null,
    "data": {}
}
```

### HTTP请求

`POST /api/client/v1/logs/client_connection_logs`

### 请求参数

参数 | 类型 | 必须 | 说明
---- | ---- | ---- | ----
uuid | string | true | 客户端uuid
app_channel | string | true | 渠道，和登录接口一致
app_version | string | true | 版本，和登录接口一致
app_version_number | string | true | 版本号，和登录接口一致
data | string | true | 日志详细信息

### data信息说明

data由多条数据构成并使用`|`分割, 每条数据中的字段由`,`号分割

格式: `用户id,服务器id,服务器ip,目标网站URL,是否走代理,时间,附加数据`

- `是否走代理`取值为: 0:没走代理, 1:走了代理
- `附加数据`: 没有的话就留空，但不能少了它前面的`,`号

实例数据: `1,1,192.168.1.1,http://twitter.com,1,1499671130,test|1,2,192.168.1.1,http://twitter.com,1,1499671140,`

## 1.42 用户注册

<aside class="success">
此接口已更新新版: <a href="/docs/client_v3">v3</a>
</aside>

```json
{
    "success": true,
    "message": null,
    "data": {
        "proxy_session_id": 151,
        "app_launch_id": "61622080-6b6f-11e7-bf54-784f4352f7e7",
        "signin_log_id": "615f07b0-6b6f-11e7-bf54-784f4352f7e7",
        "user": {
            "api_token": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpZCI6MTU2LCJ1c2VybmFtZSI6IjEzODg0NjMzMzk2IiwiZGV2aWNlX2lkIjoxNTR9.gyzxNP7fuCBZYA9eE_vtJl-w3hN3QaUUtFxDe9FBDZc",
            "id": 156,
            "username": "13884644496",
            "email": null,
            "total_payment_amount": "0.0",
            "current_coins": 5,
            "total_coins": 0,
            "password": "KmnCus5VN49nD2gpzwFooA==",
            "is_checkin_today": false,
            "is_enabled": true,
            "created_at": 1500351177,
            "new_message": false,
            "promo_code": "hgybrw",
            "promo_users_count": 0,
            "promo_coins_count": 0,
            "binded_promo_code": 'kdiekd'
        },
        "group": {
            "id": 1,
            "name": "VIP0",
            "level": 1
        },
        "user_node_types": [
            {
                "id": 332,
                "name": "精英服务",
                "level": 1,
                "status": "按次",
                "expired_at": 1500351177,
                "used_count": 0,
                "node_type_id": 1
            },
            {
                "id": 333,
                "name": "王者服务",
                "level": 2,
                "status": "按次",
                "expired_at": 1500351177,
                "used_count": 0,
                "node_type_id": 2
            }
        ],
        "node_types": [
            {
                "id": 1,
                "name": "精英服务",
                "level": 1,
                "expense_coins": 1,
                "user_group_id": 1,
                "user_group_level": 1,
                "description": "访问Facebook、twitter、instagram等网站；使用telegram等通讯工具；|当月累计使用20次，本月即可免费使用该服务"
            },
            {
                "id": 2,
                "name": "王者服务",
                "level": 2,
                "expense_coins": 3,
                "user_group_id": 1,
                "user_group_level": 1,
                "description": "享受“精英服务”所有内容；|流畅观看高清youtube、twich视频；|当月累计使用18次，本月即可免费使用该服务。"
            }
        ],
        "ip_region": {
            "country": "本机地址",
            "province": "本机地址",
            "city": null
        },
        "settings": {
            "NOTICE_CONTENT": "all",
            "QQ_GROUP": "敬请期待",
            "WX_OFFICAL_ACCOUNT": "敬请期待",
            "TELEGRAM_GROUP": "https://t.me/limaojiasuqi",
            "OFFICIAL_WEBSITE": "http://www.limaojs.com/",
            "ALLOW_SEND_LOG": "true",
            "LOG_POOL_MAX_COUNT": "50",
            "POST_LOG_INTERVAL": "600",
            "UPDATE_URL": "http://www.baidu.com/",
            "SHARE_URL": "http://share.licatjsq.com",
            "SHARE_IMG": "https://licatjsq.com/share/1024.png",
            "ANDROID_VERSION": "1.0.8|1",
            "UPDATE_CONTENT": "<div style='text-align:center;'><strong><span style = 'line-height:2;color:#000000;font-size:18px'>老用户须知</span></strong></div> <span style = 'line-height:2;color:#575757;font-size:15px'>欢迎使用狸猫加速器</span>",
            "IS_UPDATE": "0"
        }
    }
}
```

### HTTP请求

`POST /api/client/v1/signup`

### 请求参数

参数 | 类型 | 必须 | 说明
---- | ---- | ---- | ----
telephone | string | true | 手机号
password | string | true | 密码
confirmation_password | string | true | 确认密码
verification_code | string | true | 手机验证码
device_uuid | string | true | 设备uuid
device_name | string | false | 设备名(如: xxx的iphone)
device_model | string | true | 设备型号(如: iphone5)
platform | string | true | 设备系统平台(如: ios)
system_version | string | true | 设备版本(如: 10)
operator | string | true | 网络运营商(如: unicomm)
net_env | string | true | 网络环境(如: 4g、wifi)
app_version | string | true | 版本名，来自"版本渠道信息接口"获取到的ID
app_version_number | string | true | App版本(如: 1.0)
app_channel | string | true | app渠道, 来自"版本渠道信息接口"获取到的ID
app_launch_id | string | false | 用于记录App运行日志, 此值只在App的一次运行周期内有效，否则不在请求中加入此参数

### 返回信息

如果请求不成功，返回的数据会包含一个`integer`型的`error_code`字段，用来代表出错类型:

错误代码 | 说明
---- | ----
1 | 密码不合法
2 | 两次密码输入不一致
3 | 验证码不正确或过期
4 | 手机号已被注册
23 | 请求被拒绝(防刷)

字段名 | 说明
---- | ----
proxy_session_id | 用于连接代理服务器时使用
app_launch_id | 用于记录App运行日志, 需客户端在App一次运行周期内保存，并在登录请求中加入此参数
signin_log_id | 用户登录日志ID，在请求操作日志等接口时使用
user.api_token | 之后API调用需认证的接口时所需要的token(目前还未设置过期策略)
user.id | 用户id
user.username | 用户的用户名，用户自行修改用户名后，会用来存储手机号
*user.password | （需解密)用户密码
user.email | 用户绑定的email
user.total_payment_amount | 用户的消费累计金额
user.current_coins | 用户当前钻石数
user.total_coins | 用户拥有过的总钻石数
user.is_checkin_today | 用户今天是否签到过, true: 是, false: 否
user.is_enabled | 用户帐号状态(如: 被封号)，true:正常, false:已封号
user.created_at | 帐号创建时间
user.new_message | 是否有新消息, true: 是, false: 否
user.promo_code | 用户的推广码
user.promo_users_count | 用户当前已经推广的人数
user.promo_coins_count | 用户累计获得的推广钻石数
user.binded_promo_code | 已绑定的推广码，如果为`""`，则表示还未绑定
group.id | 用户组id
group.name | 用户组名称
group.level | 用户组等级
device.id | 当前设备ID
device.uuid | 当前设备uuid
regions.id | 服务器节点区域ID
user_node_types.id | 用户服务器类型ID
user_node_types.name | 用户服务器类型名称
user_node_types.level | 用户服务器类型级别
user_node_types.status | 用户服务器类型状态
user_node_types.expired_at | 用户服务器类型过期时间
user_node_types.used_count | 用户服务器类型使用次数
user_node_types.node_type_id | 服务器类型ID，用来对应服务器线路列表中的node_type的ID
ip_region.country | 国家,该字段有可能值为 null
ip_region.province | 省/州,该字段有可能值为 null
ip_region.city | 城市,该字段有可能值为 null
node_types.id | 服务器类型ID
node_types.name | 服务器类型名称
node_types.level | 服务器类型等级
node_types.expense_coins | 连接此服务器类型需要扣除了钻石数
node_types.user_group_id | 连接此服务器类型所需要的最低用户类型ID
node_types.user_group_level | 连接此服务器类型所需要的最低用户类型等级
settings | 后台管理配置的参数，具体配置项由开发者自行制定

## 1.43 绑定推广码(认证)

```json
{
    "success": true,
    "message": "绑定成功，您已经获得3钻石",
    "data": {
      "accepted_coins": 3
    }
}
```

### HTTP请求

`POST /api/client/v1/users/bind_promo_code`

### 请求参数

参数 | 类型 | 必须 | 说明
---- | ---- | ---- | ----
app_version | string | true | 版本名，来自"版本渠道信息接口"获取到的ID
app_version_number | string | true | App版本(如: 1.0)
app_channel | string | true | app渠道, 来自"版本渠道信息接口"获取到的ID
promo_code | string | true | 要绑定的推广码

### 返回信息

如果请求不成功，返回的数据会包含一个`integer`型的`error_code`字段，用来代表出错类型:

错误代码 | 说明
---- | ----
1 | 您已绑定推广码，无法再次绑定
2 | 推广码无效，请重新输入
3 | 不能使用自己的推广码
4 | 对方的推广次数已达到上限

如果成功，成功消息将使用message字段返回，如示例

字段名 | 说明
---- | ----
accepted_coins | 绑定推广码后的奖励金币数

## 1.44 获得网站导航列表(认证)

<aside class="success">
此接口已更新新版: <a href="/docs/client_v2">v2</a>
</aside>

登录接口返回的 `settings` 节点中的 `SHOW_NAVIGATION` 配置用于决定是否打开导航功能，`true`为打开, `false`为关闭。

```json
{
    "success": true,
    "message": null,
    "data": {
        "websites": [
            {
                "id": 1,
                "sort_id": 1,
                "name": "非死不可",
                "url": "http://facebook.com",
                "icon_url": "https://ss0.bdstatic.com/5aV1bjqh_Q23odCf/static/superman/img/logo/bd_logo1_31bdc765.png",
                "description": "facebook"
            }
        ],
        "current_page": 1,
        "total_pages": 1,
        "total_count": 1
    }
}
```

### HTTP请求

`GET /api/client/v1/websites`

### 请求参数

参数 | 类型 | 必须 | 说明
---- | ---- | ---- | ----
page | integer | false | 控制请求第几页数据
limit | integer | false | 控制每页显示多少条数据

### 返回信息

每页最多显示25条记录，超过后就会分页

字段名 | 说明
---- | ----
websites.id | 网站ID
websites.website_type | 网站标识, 1:推荐, 2:火热
websites.name | 网站名称
websites.url | 网站URL
websites.icon_url | 网站图标URL
websites.description | 网站描述
current_page | 当前页码
total_pages | 总页数
total_count | 总记录数

## 1.45 发送回复到指定的反馈(认证)

当用户发出的反馈有了管理员的回复后，用户如果还有疑问，客户端通过此接口来回复管理员，像评论一样。

```json
{
    "success": true,
    "message": null,
    "data": {}
}
```

### HTTP请求

`POST /api/client/v1/feedbacks/reply`

### 请求参数

参数 | 类型 | 必须 | 说明
---- | ---- | ---- | ----
feedback_id | integer | true | 反馈ID
content | string | false | 回复的内容

## 1.46 获得指定反馈的回复列表(认证)

回复列表会由新到旧排序，并支持分页，每页的回复由旧到新排列，方便客户端展示。

同时将此反馈设置为已读。

```json
{
    "success": true,
    "message": null,
    "data": {
      "new_message": false,
      "logs": [
          {
              "id": 60,
              "member_flag": 0,
              "member": "我",
              "content": "用户的回复1",
              "created_at": 1501915742
          },
          {
              "id": 50,
              "member_flag": 0,
              "member": "我",
              "content": "这是一个反馈回复2",
              "created_at": 1501672348
          },
          {
              "id": 49,
              "member_flag": 0,
              "member": "我",
              "content": "这是一个反馈回复1",
              "created_at": 1501672336
          }
      ],
      "current_page": 1,
      "total_pages": 1,
      "total_count": 3
    }
}
```

### HTTP请求

`GET /api/client/v1/feedbacks/replies`

### 请求参数

参数 | 类型 | 必须 | 说明
---- | ---- | ---- | ----
feedback_id | integer | true | 反馈的id
page | integer | false | 控制请求第几页数据
limit | integer | false | 控制每页显示多少条数据

### 返回信息

每页默认显示10条记录，总记录数超过10之后就会分页，如果指定显示条数(limit)，则最多不超过30条

字段名 | 说明
---- | ----
new_message | 当前用户是否还有未读的反馈
logs.id | 回复ID
logs.member_flag | 回复人标识, 0:我, 1:管理员
logs.member | 回复人名称
logs.content | 回复内容
logs.created_at | 回复时间
current_page | 当前页码
total_pages | 总页数
total_count | 总记录数

## 1.47 设置是否自动扣钻石续期(认证)

此接口于用设置用户在服务到期时是否自动扣除钻石来延长有效期

```json
{
    "success": true,
    "message": null,
    "data": {
        "auto_renew": false
    }
}
```

### HTTP请求

`POST /api/client/v1/users/set_auto_renew`

### 请求参数

参数 | 类型 | 必须 | 说明
---- | ---- | ---- | ----
auto_renew | integer | true | 是否在服务到期后自动扣除钻石续期, 0:否, 1:是

### 返回信息

字段名 | 说明
---- | ----
auto_renew |是否在服务到期后自动扣除钻石进行续期(修改后的结果)

## 1.48 获取当前用户的订单记录(认证)

此接口于用获取用户购买套餐产生的订单记录，由于文档要求只获取最近10条记录，所以目前的`page`和`limit`参数可不加，
以后如果需要翻页功能的时候才用得上。

```json
{
    "success": true,
    "message": null,
    "data": {
        "logs": [
            {
                "order_number": "201707061499329408164948e1",
                "created_at": 1499329408,
                "has_paid": false,
                "plan_name": "体验套餐",
                "price": "0.0",
                "coins": 0
            },
            {
                "order_number": "2017070414991512763976e2f5",
                "created_at": 1499151276,
                "has_paid": true,
                "plan_name": "体验套餐",
                "price": "1.0",
                "coins": 5
            },
            {
                "order_number": "2017070414991505931447dbfe",
                "created_at": 1499150593,
                "has_paid": false,
                "plan_name": "普通套餐",
                "price": "0.0",
                "coins": 0
            }
        ],
        "current_page": 1,
        "total_pages": 3,
        "total_count": 26
    }
}
```

### HTTP请求

`GET /api/client/v1/plans/transaction_logs`

### 请求参数

参数 | 类型 | 必须 | 说明
---- | ---- | ---- | ----
page | integer | false | 指定页码，不指定则默认获取第1页
limit | integer | false | 每页记录数，不指定则默认获取10个

### 返回信息

字段名 | 说明
---- | ----
log.order_number | 订单号
log.created_at | 订单创建时间
log.has_paid | 是否完成, true:已完成, false:订单取消
log.plan_name | 套餐名
log.price | 金额
log.coins | 获得的金币数，目前套餐名直接就是`XX钻`，可直接取套餐名
current_page | 当前页码
total_pages | 总页数
total_count | 总记录数

## 1.49 获取当前用户的消费记录(认证)

此接口于用获取用户购买套餐产生的消费记录，由于文档要求只获取最近10条记录，所以目前的`page`和`limit`参数可不加，
以后如果需要翻页功能的时候才用得上。

```json
{
    "success": true,
    "message": null,
    "data": {
        "logs": [
            {
                "created_at": 1499220797,
                "node_type_name": "王者服务",
                "coins": 3
            },
            {
                "created_at": 1499155514,
                "node_type_name": "精英服务",
                "coins": 1
            },
            {
                "created_at": 1499067594,
                "node_type_name": "王者服务",
                "coins": 3
            }
        ],
        "current_page": 1,
        "total_pages": 3,
        "total_count": 26
    }
}
```

### HTTP请求

`GET /api/client/v1/plans/consumption_logs`

### 请求参数

参数 | 类型 | 必须 | 说明
---- | ---- | ---- | ----
page | integer | false | 指定页码，不指定则默认获取第1页
limit | integer | false | 每页记录数，不指定则默认获取10个

### 返回信息

字段名 | 说明
---- | ----
log.created_at | 消费时间
log.node_type_name | 服务类型名
log.coins | 获得的金币数，目前套餐名直接就是`XX钻`，可直接取套餐名
current_page | 当前页码
total_pages | 总页数
total_count | 总记录数

## 1.50 检查当前用户针对指定代理服务器的状态(认证)

此接口于用客户端检查当前已连接的代理服务器的状态，如用户的服务是否在有效期内、代理服务器是否满载等等。

```json
{
    "success": true,
    "message": null,
    "data": {}
}
```

### HTTP请求

`POST /api/client/v1/nodes/check`

### 请求参数

参数 | 类型 | 必须 | 说明
---- | ---- | ---- | ----
node_id | integer | true | 代理服务器ID

### 返回信息

错误码 | 说明
---- | ----
3 | 用户服务类型到期，后端自动续费开启，客户端调`1.37`接口来为服务续费
4 | 代理服务器不存在
5 | 代理服务器爆满
6 | 用户类型等级不够
7 | 用户服务类型到期，后端自动续费关闭，此时应关闭客户端代理服务连接
8 | 用户帐号被停用，此时应关闭客户端代理服务连接

## 1.51 提交客户端调试日志文件(认证)

此接口用于客户端向服务端提交调试日志文件。

客户端在上传日志文件前，需要将日志文件提前以这样的格式命名: `年月日_平台_UUID.txt`，如: `20170915_ios_db3272e9564040f9a4a464bb1f6efb7c.txt`

如果上传的文件在服务器上已存在，则会被新上传的文件覆盖。

命名要求:

- 所有字母小写
- 平台目前只有4个: ios/android/pc/mac
- UUID或MAC地址应替换掉里面的`- / _ / :`这样的字符，并转为小写
- 年月日格式如: `20170915`

```json
{
    "success": true,
    "message": null,
    "data": {}
}
```

### HTTP请求

`POST /api/client/v1/logs/client_debug_logs`

### 请求参数

参数 | 类型 | 必须 | 说明
---- | ---- | ---- | ----
platform | string | true | 平台
uuid | string | true | 设备uuid
log_file[] | array | true | 要上传的日志文件，注意参数名后的大括号，表示数组，日志文件对象存在数组里指定给此参数来实现多文件的上传

### 错误码

错误码 | 说明
---- | ----
1 | 文件格式无效

## 1.52 提交路由配置表(独立认证)

此接口用于向服务器提交客户端的路由配置文件。

关于独立认证: 此接口需要独立的Token认证，请在请求的`headers`里加入以`AUTHTOKEN`为key的认证token(token的值需要向后端同事获取)。

请为各平台客户端生成文件名时，按照参数说明中的文件名来命名。

> 上传成功的返回信息样例:

```json
{
    "success": true,
    "message": null,
    "data": {}
}
```

> 上传失败的返回信息样例:

```json
{
    "success": false,
    "message": "至少需要有一个可用参数",
    "error_code": 1
}
```

### HTTP请求

`POST /api/client/v1/commons/upload_routes`

### 请求参数

所有参数都是可选的，但请至少指定一个参数

参数 | 类型 | 必须 | 说明
---- | ---- | ---- | ----
ios_d2o | file | false | ios客户端的国内对国外路由配置，文件名: `ios_d2o.conf`
ios_o2d | file | false | ios客户端的国外对国内路由配置，文件名: `ios_o2d.conf`
ios_glo_d2o | file | false | ios客户端的全局代理需要走直连列表，文件名: `ios_glo_d2o.plist`
android_d2o | file | false | android客户端的国内对国外路由配置，文件名: `android_d2o`
android_o2d | file | false | android客户端的国外对国内路由配置，文件名: `android_o2d`
pc_d2o | file | false | pc客户端的国内对国外路由配置(目前此配置同样作用于mac客户端), 文件名: `pc_d2o.pac`
pc_o2d | file | false | pc客户端的国外对国内路由配置(目前此配置同样作用于mac客户端), 文件名: `pc_o2d.pac`
pc_glo_d2o | file | false | pc客户端的全局代理需要走直连列表(目前此配置同样作用于mac客户端), 文件名: `pc_glo_d2o.pac`
mac_d2o | file | false | mac客户端的国内对国外路由配置，此参数可暂时忽略，因为mac客户端的路由配置也是用的pc的, 文件名: `mac_d2o.pac`
mac_o2d | file | false | mac客户端的国外对国内路由配置，此参数可暂时忽略，因为mac客户端的路由配置也是用的pc的, 文件名: `mac_o2d.pac`
mac_glo_d2o | file | false | mac客户端的全局代理需要走直连列表，此参数可暂时忽略，因为mac客户端的路由配置也是用的pc的, 文件名: `mac_glo_d2o.pac`
china_ip | file | false | 国内ip列表, 文件名: `china_ip.txt`
foreign_ip | file | false | 国外ip列表, 文件名: `foreign_ip.txt`

### 错误码

错误码 | 说明
---- | ----
1 | 至少需要有一个可用参数
2 | 至少有一项失败，请重新上传

## 1.53 获得当前已开启的支付方式(认证)

此接口目前只有PC/Mac端使用，在用户点击购买按钮后，调用此接口获取已开启的支付方式。

在用户选择相应的支付方式后，再调用生成订单接口，并把支付方式的值通过`type`参数传入。

```json
{
    "success": true,
    "message": null,
    "data": {
        "payment_methods": [
            [
                "wx_qrcode",
                "微信扫码"
            ],
            [
                "wx_h5",
                "微信h5"
            ],
            [
                "qq",
                "QQ钱包"
            ]
        ]
    }
}
```

### HTTP请求

`GET /api/client/v1/plans/payment_methods`

### 已支持的支付方式

- `alipay_qrcode`:支付宝扫码
- `alipay_h5`:支付宝H5
- `wx_qrcode`:微信扫码
- `wx_h5`:微信H5
- `qq`:QQ钱包

### 返回信息

字段名 | 说明
---- | ----
payment_methods | 已开始的支付方式

## 1.54 获得客户端日志提交相关信息(认证)

此接口在客户端提交去向日志时调用，并使用返回的信息进行数据提交

1. 此接口的调用频率为1天，客户端本地记录时间戳对比超过1天就从此接口重新获取数据
2. 此接口只在发送日志前(满足条件1)时调用，不在登录时调用
3. 弃用登录/注册接口中「是否开启客户端日志上传的开关配置」，使用本接口返回的enabled来决定是否上传客户端配置

```json
{
    "success": true,
    "message": null,
    "data": {
        "token": "asdf93kd9",
        "key": "239dk390dk",
        "enabled": true,
        "host_url": "https://xxx.com"
    }
}
```

### HTTP请求

`GET /api/client/v1/commons/client_log`

### 返回信息

字段名 | 说明
---- | ----
token | 用于向接口提交数据时验证
key | 密钥，用于加解密数据
enabled | 是否开启客户端去向日志数据提交
host_url | 提交服务器的URL

## 1.55 获得开屏广告信息

注意:
- 如果后台管理没有配置合适的广告显示，`ad`将为`null`。
- 如果`is_enabled`为`false`，则`ad`将为`null`，不返回广告信息。

> is_enabled为true的情况

```json
{
    "success": true,
    "message": null,
    "data": {
        "is_enabled": true,
        "overtime": 1801,
        "ad": {
            "id": 1,
            "duration": 20,
            "image_url": "http://test.com/1.png",
            "link_url": "http://test.com",
            "ad_type": 1,
            "open_link_type": 1,
            "updated_at": "2018-02-05 18:42:14"
        }
    }
}
```

> is_enabled为true，但没有广告提供的情况

```json
{
    "success": true,
    "message": null,
    "data": {
        "is_enabled": true,
        "overtime": 1801,
        "ad": null
    }
}
```

> is_enabled为false的情况

```json
{
    "success": true,
    "message": null,
    "data": {
        "is_enabled": false,
        "overtime": 1801,
        "ad": null
    }
}
```

### HTTP请求

`GET /api/client/v1/ads/splash_screen`

### 请求参数

参数 | 类型 | 必须 | 说明
---- | ---- | ---- | ----
uuid | string | true | 客户端uuid
app_channel | string | true | 渠道
app_version | string | true | 版本
app_version_number | string | true | 版本号

### 返回信息

字段名 | 说明
---- | ----
is_enabled | 开屏广告是否开启，此字段如果返回false，ad字段将为空
overtime | 开屏广告间隔时间(秒)
ad.id | 广告id
ad.duration | 广告持续时间
ad.image_url | 广告图片URL
ad.link_url | 广告链接URL
ad.ad_type | 广告播放类型，1:图片(jpeg、png), 2:动图(gif), 3:视频(mp4)
ad.open_link_type | 广告跳转方式，1:webview, 2:浏览器
ad.updated_at | 广告最后更新时间

## 1.56 提交开屏广告展示成功日志

此接口只在开屏广告被成功展示后才提交

```json
{
    "success": true,
    "message": null,
    "data": {}
}
```

### HTTP请求

`POST /api/client/v1/ads/splash_screen_log`

### 请求参数

参数 | 类型 | 必须 | 说明
---- | ---- | ---- | ----
ad_id | integer | true | 广告id
uuid | string | true | 客户端uuid
app_channel | string | true | 渠道
app_version | string | true | 版本
app_version_number | string | true | 版本号

## 1.57 获得弹出广告信息

```json
{
    "success": true,
    "message": null,
    "data": {
        "is_enabled": false,
        "day_times": 500,
        "image_url": "http://p3pgf4sih.bkt.clouddn.com/popup_01.png",
        "link_url": "http://qq.com",
        "open_link_type": 1,
        "updated_at": "1970-01-01 12:00:00"
    }
}
```

### HTTP请求

`GET /api/client/v1/ads/popup`

### 请求参数

参数 | 类型 | 必须 | 说明
---- | ---- | ---- | ----
uuid | string | true | 客户端uuid
app_channel | string | true | 渠道
app_version | string | true | 版本
app_version_number | string | true | 版本号

### 返回信息

字段名 | 说明
---- | ----
is_enabled | 广告是否开启
day_times | 每天展示次数(秒)
ad.image_url | 广告图片URL
ad.link_url | 广告链接URL
ad.open_link_type | 广告跳转方式，1:webview, 2:浏览器
ad.updated_at | 广告最后更新时间

## 1.58 提交弹出广告展示成功日志

此接口只在弹出广告被成功展示后才提交

```json
{
    "success": true,
    "message": null,
    "data": {}
}
```

### HTTP请求

`POST /api/client/v1/ads/popup_log`

### 请求参数

参数 | 类型 | 必须 | 说明
---- | ---- | ---- | ----
uuid | string | true | 客户端uuid
app_channel | string | true | 渠道
app_version | string | true | 版本
app_version_number | string | true | 版本号


## 1.59 提交开屏广告点击日志

此接口只在开屏广告被点击后才提交

```json
{
    "success": true,
    "message": null,
    "data": {}
}
```

### HTTP请求

`POST /api/client/v1/ads/splash_screen_click_log`

### 请求参数

参数 | 类型 | 必须 | 说明
---- | ---- | ---- | ----
ad_id | integer | true | 广告id
uuid | string | true | 客户端uuid
app_channel | string | true | 渠道
app_version | string | true | 版本
app_version_number | string | true | 版本号


## 1.60 提交弹出广告点击日志

此接口只在弹出广告被点击后才提交

```json
{
    "success": true,
    "message": null,
    "data": {}
}
```

### HTTP请求

`POST /api/client/v1/ads/popup_click_log`

### 请求参数

参数 | 类型 | 必须 | 说明
---- | ---- | ---- | ----
uuid | string | true | 客户端uuid
app_channel | string | true | 渠道
app_version | string | true | 版本
app_version_number | string | true | 版本号

## 1.61 获得客服账号(potato)(认证)

```json
{
    "success": true,
    "message": null,
    "data": {
        "potato_url": "https://potato.im/12345",
        "qrcode_url": "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAZAAAAGQAQAAAACoxAthAAACq0lEQVR4nO2bS3KrMBBFaTKAWZYAO2FpWEvzTmAJzOyBo3TfFpjkvUoVA4RDLgPZIJ+qpiT132XcfJXF5osIESJEiBAhQoQIkZ+Qu6SrLaJccNvq44AhTZVfkTyCEcmBiAcaF9yM7x861LYP7GowdTtGMCJZkLCc7tD6p+6IK57NquAYwYjkRRo95/2ga35pbv/O/q53IbIFub/Fy1hLlM4MAWzAawhGZE+kj9FWutKhmUTiVe2+tHf7QaUbYThMMCK7I9H9enX38cTO/qRaAJp/xFR9jGBE9kequbowIOKzwZR+f7UhTX18RfIIRiQPEux06yBu9xHihW6SWf0fJhiR3ZEoWO5+CetD6woAOYC43gIv/y5ENiLu6UcP6/uhgpY3BTClH8AGHCAYkRxIyuvpIj9EOtMCV3Pzxnr+1kw8+2dF1OtTJ79Ufa9aIA730jW/Bv0P3RY2S5//1AiOOOx+EGh5M/TI+8ADaJjnPy1iWV37DK3fqxOYjD9my6T+8wtGJAcintnxAB+a3wyBZ/w9Dqjo9Z0WMXffzHuLuwop/jm/O9b/RfIIRmR/pEphvTt83Q3h/5z1s/o+4oADBCOSATHL/lz4wR2+wkOAVVtPfsGIZEDUqIcW7Rx1yvkW3t1Rw923rB87u86L2Prawnc3Wbq45s4u2v2zI+JVvH7AHfy/udb7rQDw+u9CZCOy6uktvKD/bPIMZhJEmOc/L7L09FYP2wKSYj/v8CsK9wmPEIxIFuTZ01uYm+f+H5L9guIfsz1/ALFaL8J/9/Tf0cov7Ov7M0g/+Ik3X8/SvWjsZYX31MizpzeYu58WHvH+JLLq7f4F70JkG7L09Hp5L3pzn6p/uPvzX7wOEIzI/siqp1fSSjezKvBQn/V9IkSIECFChAgRIkR+uj4B5tCj/i7fj9QAAAAASUVORK5CYII="
    }
}
```

### HTTP请求

`GET /api/client/v1/customer_services`

### 返回信息

字段名 | 说明
---- | ----
potato_url | potato账号url
qrcode_url | base64二维码图片


## 1.62 Google支付二次验证(认证)

```json
{
  "success": true,
  "message": null,
  "data": {
    "user": {
      "current_coins": 10
    },
    "group": {
      "id": 3,
      "name": "VIP2",
      "level": 3,
      "need_coins": 10
    }
  }
}
```

### HTTP请求

`POST /api/client/v1/plans/verify_google_pay`

### 请求参数

参数 | 类型 | 必须 | 说明
---- | ---- | ---- | ----
app_version | string | true | 客户端版本
app_version_number | string | true | 客户端版本号
receipt_data | string | true | json格式订单信息
signature | string | true | 签名

### 返回信息

字段名 | 说明
---- | ----
user.current_coins | 用户当前的钻石数
group.id | 用户类型ID(用户钻石累记达到一定数量，会升级成相应的用户类型)
group.name | 用户类型名
group.level | 用户类型等级
group.need_coins | 升级到此类型需要的累计钻石数


## 1.63 获得客户端测速全局配置(认证)

<aside class="warning">
此接口已废弃: 改用<a href="/docs/index#1-70">1.70接口</a>
</aside>

此接口返回测速全局配置，如(测速全局开关、测速间隔、测速数据上传url等)

```json
{
    "success": true,
    "message": null,
    "data": {
        "test_speed_enabled": true,
        "user_hit_test_speed": false,
        "test_speed_interval": 600,
        "test_speed_upload_url": "",
        "test_speed_request_count": 3,
        "test_speed_request_timeout": 3000,
        "test_speed_download_timeout": 3000
    }
}
```

### HTTP请求

`GET /api/client/v1/nodes/test_speed_global_config`

### 返回信息

字段名 | 说明
---- | ----
test_speed_enabled | 是否开启全局测速功能
user_hit_test_speed | 当前用户是否需要测速
test_speed_interval | 间隔时间(秒)
test_speed_upload_url | 日志数据上传url
test_speed_request_count | http请求次数
test_speed_request_timeout | 单次http请求超时时间(ms)
test_speed_download_timeout | 下载超时时间(ms)

## 1.64 提交节点http请求测速日志(认证)

此接口用于客户端向服务端提交节点http请求测速日志。

根据 `1.63`接口返回的测速服务器ip列表轮询得到每个域名的http请求速度一起提交

测速耗时单位为毫秒(ms)

```json
{
    "success": true,
    "message": null,
    "data": {}
}
```

### HTTP请求

`POST /api/client/v1/nodes/node_test_request_speed_logs`

### 请求参数

参数 | 类型 | 必须 | 说明
---- | ---- | ---- | ----
app_channel | string | true | 渠道，和登录接口一致
app_version | string | true | 版本，和登录接口一致
data | string | true | http请求测速日志详细信息

### data信息说明

data是json格式字符串

格式: `{"请求的url": “网址url”, "最大响应时间": 0, "最小响应时间": 0, "平均响应时间": 0, "失败率": 0}`

实例数据: `[{"request_url": "https://www.google.com", "max_rt": 56, "min_rt": 12, "avg_rt": 18.1, "failure_rate": 41.2}]`

### 错误码

错误码 | 说明
---- | ----
1 | 无效的JSON数据格式


## 1.65 获得线路列表(认证)

每个层级拥有完整的父级链路

```json
{
    "success": true,
    "message": null,
    "data": {
        "lines": [
            {
                "id": 1,
                "name": "精英服务",
                "node_continents": [
                    {
                        "id": 2,
                        "name": "亚洲",
                        "abbr": null,
                        "prority": 1,
                        "node_type_id": 1,
                        "": null,
                        "node_countries": [
                            {
                                "id": 20,
                                "name": "中国",
                                "abbr": "https://download.w202w.com/image/flag/China.png",
                                "prority": 1,
                                "node_continent_id": 2,
                                "node_type_id": 1,
                                "": null
                            }
                        ]
                    }
                ]
            },
            {
                "id": 2,
                "name": "王者服务",
                "node_continents": [
                    {
                        "id": 2,
                        "name": "亚洲",
                        "abbr": null,
                        "prority": 1,
                        "node_type_id": 2,
                        "": null,
                        "node_countries": [
                            {
                                "id": 20,
                                "name": "中国",
                                "abbr": "https://download.w202w.com/image/flag/China.png",
                                "prority": 1,
                                "node_continent_id": 2,
                                "node_type_id": 2,
                                "": null,
                                "node_regions": [
                                    {
                                        "id": 19,
                                        "name": "中国-北京",
                                        "abbr": "https://download.w202w.com/image/flag/China.png",
                                        "prority": 4,
                                        "node_country_id": 20,
                                        "node_continent_id": 2,
                                        "node_type_id": 2,
                                        "": null
                                    },
                                    {
                                        "id": 20,
                                        "name": "中国-广州",
                                        "abbr": "https://download.w202w.com/image/flag/China.png",
                                        "prority": 5,
                                        "node_country_id": 20,
                                        "node_continent_id": 2,
                                        "node_type_id": 2,
                                        "": null
                                    }
                                ]
                            }
                        ]
                    }
                ]
            }
        ]
    }
}
```

### HTTP请求

`GET /api/client/v1/nodes/customize_lines`

### 返回信息

字段名 | 说明
---- | ----
node_types.id | id
node_types.name | 服务器类型名称
node_types.prority | 当前层级排列优先级
node_countries.id | id
node_countries.name | 国家名称
node_countries.abbr | 国旗url
node_regions.id | id
node_regions.name | 区域名称
node_regions.abbr | 国旗url

### 错误码

错误码 | 说明
---- | ----
4 | 没有可用的服务器线路


## 1.66 定制专线按指定服务器地域连接(认证)

> 结果1: 用户服务器类型未过期时

```json
{
  "success": true,
  "message": null,
  "data": {
    "user_node_type_is_expired": false,
    "is_consumed": false,
    "proxy_session_token": "Ywvz4YkAi_b9hU2IuGnxRSOePJJKz6tfk95N1PF_y1s=",
    "user": {
      "current_coins": 9958
    },
    "user_node_type": {
      "id": 616,
      "expired_at": 1493203060,
      "status": "按次",
      "used_count": 3
    },
    "node": {
        "id": 1,
        "name": "pkSN75HEoq9nWhuVTThRvA==",
        "url": "LdXg1+meZqeQ4vi6SLeDBg==",
        "port": "OWJ+dDe88OnJ30DtKx/vEg==",
        "encrypt_method": "pj2110m0YMAY/gpHz4tdfA==",
        "password": "YLvZlicyeQ6Um7Cg6vxLTA=="
      },
      "tab_id": 2
  }
}
```

> 结果2: 用户服务器类型已过期，但客户端还未确认扣除钻石时

```json
{
  "success": true,
  "message": null,
  "data": {
    "user_node_type_is_expired": true,
    "is_consumed": false
  }
}
```

> 结果3: 用户服务器类型已过期，客户端确认扣除钻石时

```json
{
  "success": true,
  "message": null,
  "data": {
    "user_node_type_is_expired": true,
    "is_consumed": true,
    "proxy_session_token": "Ywvz4YkAi_b9hU2IuGnxRSOePJJKz6tfk95N1PF_y1s=",
    "user": {
      "current_coins": 9957
    },
    "user_node_type": {
      "id": 616,
      "expired_at": 1492511860,
      "status": "按次",
      "used_count": 3
    },
    "node": {
        "id": 1,
        "name": "pkSN75HEoq9nWhuVTThRvA==",
        "url": "LdXg1+meZqeQ4vi6SLeDBg==",
        "port": "OWJ+dDe88OnJ30DtKx/vEg==",
        "encrypt_method": "pj2110m0YMAY/gpHz4tdfA==",
        "password": "YLvZlicyeQ6Um7Cg6vxLTA=="
      },
      "tab_id": 2
  }
}
```

### HTTP请求

`POST /api/client/v1/nodes/customize_connect`

### 请求参数

参数 | 类型 | 必须 | 说明
---- | ---- | ---- | ----
node_type_id | integer | true | 服务器类型ID(告诉后端要连接哪个类型的服务器)
allow_consume | integer | false | 是否允许扣除钻石(1:扣除,0:不扣)，默认可不加此参数，当连接的服务器类型已过期，服务器会返回是否需要扣除钻石，当用户确认要扣除时，再次请求此接口，并加上此参数
node_country_id | integer | true | 服务器国家ID(告诉后端要连接哪个国家的服务器，此参数必填，选择地域也需传递该参数)
node_region_id | integer | false | 服务器地域ID(告诉后端要连接哪个区域的服务器）

### 返回信息

返回信息的三种状态是根据`user_node_type_is_expired`和`is_consumed`来判断的:

1. 服务器类型未过期
  - `user_node_type_is_expired: false`
  - `is_consumed: false`
2. 服务器类型已过期，需要客户端确认扣除钻石
  - `user_node_type_is_expired: true`
  - `is_consumed: false`
3. 服务器类型已过期，客户端确认扣除钻石
  - `user_node_type_is_expired: true`
  - `is_consumed: true`

字段名 | 说明
---- | ----
user_node_type_is_expired | 用户指定要连接的服务器类型是否已过期
is_consumed | 是否已扣除钻石
*proxy_session_token | (需解密)连接代理服务器时使用的token，把token放在这里是因为后端会检查在线的用户服务是否到期，到期后会重置token
user.current_coins | 用户当前钻石数
user_node_type.id | 用户服务器类型ID
user_node_type.expired_at | 用户服务器类型新的到期时间
user_node_type.status | 用户服务器类型状态
user_node_type.used_count | 用户服务器类型本月的使用次数
node.id | 代理服务器ID
*node.name | (需解密)代理服务器名
*node.url | (需解密)代理服务器地址
*node.port | (需解密)代理服务器端口
*node.encrypt_method | (需解密)代理服务器加密方式
*node.password | (需解密)代理服务器密码
tab_id | 对应套餐标签页ID

### 错误码

错误码 | 说明
---- | ----
1 | 服务器类型不存在
2 | 您的VIP等级太低
3 | 您的钻石不足
4 | 没有可用的服务器线路
5 | 服务器国家不存在
6 | 服务器地域不存在
7 | 服务器地域不属于该国家


## 1.67 获取分享模版列表(认证)

此接口目前只有(IOS/ANDROID)使用

接口会一并返回分享全局参数(分享模版更新时间、是否开启微信分享等)

图片分享文字内容、图片分享推广码文字，已经替换占位符为`当前用户`的推广码

> 如果客户端传的模版更新时间与后台时间一致，返回的数据如下:

```json
{
    "success": true,
    "message": null,
    "data": {
        "template_update_time": 1546002152,
        "text_logo_url": "https://nutvpn.net/logo.png",
        "text_logo_update_time": 1546227561,
        "weixin_enabled": true,
        "friend_circle_enabled": true,
        "qq_enabled": true,
        "weibo_enabled": true,
        "hit_cache": true
    }
}
```

> 其他情况，则返回:

```json
{
    "success": true,
    "message": null,
    "data": {
        "template_update_time": 1546002152,
        "text_logo_url": "https://nutvpn.net/logo.png",
        "text_logo_update_time": 1546227561,
        "weixin_enabled": true,
        "friend_circle_enabled": true,
        "qq_enabled": true,
        "weibo_enabled": true,
        "hit_cache": false,
        "templates": [
            {
                "id": 1,
                "platform_name": "youtube",
                "priority": 1,
                "copy_link_text": "https://youtube.com 哈哈",
                "text_title": "youtube标题",
                "text_content": "youtube内容",
                "text_url": "youtube url",
                "image_text_content": "https://youtube.com 洒agxhea大地",
                "image_promo_text": "https://youtube.com 阿斯顿洒恶气agxhea",
                "landing_page_qrcode_url": "https://1.com/a",
                "background_image_download_url": "https://1.com/abb",
                "background_image_update_at": 1546002124
            }
        ]
    }
}
```

### HTTP请求

`GET /api/client/v1/shares/templates`

### 请求参数

参数 | 类型 | 必须 | 说明
---- | ---- | ---- | ----
template_update_time | integer | false | 分享模版更新时间

### 返回信息

字段名 | 说明
---- | ----
template_update_time | 分享模版更新时间
text_logo_url | 文字分享logo url
text_logo_update_time | 文字分享logo更新时间
weixin_enabled | 分享微信是否开启
friend_circle_enabled | 分享微信朋友圈是否开启
qq_enabled | 分享qq是否开启
weibo_enabled | 分享微博是否开启
hit_cache | 是否命中缓存，如果为true则不返回分享模版列表，为false返回全部模版
templates.id | 模版id
templates.platform_name | 平台名称
templates.priority | 优先级
templates.copy_link_text | 复制链接内文字
templates.text_title | 分享文字题标题
templates.text_content | 分享文字内容
templates.text_url | 分享文字url
templates.image_text_content | 图片分享文字内容
templates.image_promo_text | 图片分享推广码文字
templates.landing_page_qrcode_url | 图片分享落地页二维码url
templates.background_image_download_url | 背景图下载url
templates.background_image_update_at | 背景图更新时间

## 1.68 提交节点下载测速日志(认证)

此接口用于客户端向服务端提交节点下载测速日志。

根据 `1.63`接口返回的测速服务器ip列表轮询得到每个服务器的下载速度一起提交

测速耗时单位为毫秒(ms)

```json
{
    "success": true,
    "message": null,
    "data": {}
}
```

`POST /api/client/v1/nodes/node_test_download_speed_logs`

### 请求参数

参数 | 类型 | 必须 | 说明
---- | ---- | ---- | ----
app_channel | string | true | 渠道，和登录接口一致
app_version | string | true | 版本，和登录接口一致
data | string | true | 下载测速日志详细信息

### data信息说明

data是json格式字符串

格式: `{"下载的url": “代理服务器ip地址”, "耗时": 0}`

实例数据: `[{"download_url": "124.25.12.87", "waste_time": 12.25}]`

### 错误码

错误码 | 说明
---- | ----
1 | 无效的JSON数据格式


## 1.69 获得测速节点ip列表(认证)

```json
{
    "success": true,
    "message": null,
    "data": {
        "ip_list": [
            {
                "type": "1",
                "url": "https://www.google.com"
            },
            {
                "type": "2",
                "url": "www.google.com/1.txt"
            }
        ]
    }
}
```

### HTTP请求

`GET /api/client/v1/nodes/test_speed_ip_list`

### 返回信息

字段名 | 说明
---- | ----
ip_list.type | 测速类型 http请求:1/下载:2
ip_list.url | 测速地址


## 1.70 获得系统全局配置(认证)

```json
{
    "success": true,
    "message": null,
    "data": {
        "test_speed_enabled": true,
        "user_hit_test_speed": false,
        "test_speed_interval": 600,
        "test_speed_upload_url": "",
        "test_speed_request_count": 3,
        "test_speed_request_timeout": 3000,
        "test_speed_download_timeout": 3000,
        "navigation_ad_carousel_interval": 3000,
        "navigation_connection_vpn_open_enabled": true,
        "notice_alter_loop_check_enabled": true,
        "notice_alter_check_interval": 300,
        "page_user_info_prompt": "",
        "page_update_username_prompt": "",
        "page_update_password_prompt": "",
        "page_share_prompt": ""
    }
}
```

### HTTP请求

`GET /api/client/v1/globals`

### 返回信息

字段名 | 说明
---- | ----
test_speed_enabled | 是否开启全局测速功能
user_hit_test_speed | 当前用户是否需要测速
test_speed_interval | 间隔时间(秒)
test_speed_upload_url | 日志数据上传url
test_speed_request_count | http请求次数
test_speed_request_timeout | 单次http请求超时时间(ms)
test_speed_download_timeout | 下载超时时间(ms)
navigation_ad_carousel_interval | 导航轮播广告滚动时间(ms)
navigation_connection_vpn_open_enabled | 是否开启连接vpn成功跳转导航界面
notice_alter_loop_check_enabled | 即时弹窗循环检查是否开启
notice_alter_check_interval | 即时弹窗循环检查间隔时间(秒)
page_user_info_prompt | 用户信息提示
page_update_username_prompt | 修改用户账号信息提示
page_update_password_prompt | 修改用户密码信息提示
page_share_prompt | 分享推广信息提示

## 1.71 获得导航广告信息(认证)

> 如果客户端传的导航广告更新时间与后台时间一致，返回的数据如下:

```json
{
    "success": true,
    "message": null,
    "data": {
        "navigation_ad_update_at": 1550739222,
        "hit_cache": true
    }
}
```

> 其他情况，则返回:

```json
{
    "success": true,
    "message": null,
    "data": {
        "navigation_ad_update_at": 1550739222,
        "hit_cache": false,
        "ads": [
            {
                "id": 1,
                "priority": 1,
                "name": "共产党贪腐曝光",
                "image_url": "https://asdj123.com",
                "link_url": "https://xxcqw2.com",
                "updated_at": 1550754477
            },
            {
                "id": 2,
                "priority": 2,
                "name": "抖音神曲推荐",
                "image_url": "https://asdj123.com",
                "link_url": "https://asdj1231.com",
                "updated_at": 1550755565
            }
        ]
    }
}
```

### HTTP请求

`GET /api/client/v1/ads/navigation`

### 请求参数

参数 | 类型 | 必须 | 说明
---- | ---- | ---- | ----
uuid | string | true | 客户端uuid
app_channel | string | true | 渠道
app_version | string | true | 版本
app_version_number | string | true | 版本号
update_at | integer | false | 导航广告更新时间

### 返回信息

字段名 | 说明
---- | ----
navigation_ad_update_at | 导航广告更新时间
hit_cache | 是否命中缓存，如果为true则不返回导航广告列表，为false返回全部数据
ads.id | 广告ID
ads.priority | 广告编号, 客户端导航广告播放优先级
ads.name | 广告名称
ads.image_url | 广告图片URL
ads.link_url | 广告链接URL
ads.updated_at | 广告最后更新时间

## 1.72 提交网址导航点击日志(认证)

此接口只在网址导航被点击后才提交

```json
{
    "success": true,
    "message": null,
    "data": {}
}
```

### HTTP请求

`POST /api/client/v1/websites/website_navigation_click_log`

### 请求参数

参数 | 类型 | 必须 | 说明
---- | ---- | ---- | ----
uuid | string | true | 客户端uuid
website_id | integer | true |网站id
website_name | string | true | 网站名称
nav_type | integer | true | 导航分类, 1:推荐网站, 2:推荐应用
info_type | integer | true | 信息分类, 1:导航类, 2:广告类
app_channel | string | true | 渠道
app_version | string | true | 版本
app_version_number | string | true | 版本号


## 1.73 提交导航广告点击日志(认证)

此接口只在导航广告被点击后才提交

```json
{
    "success": true,
    "message": null,
    "data": {}
}
```

### HTTP请求

`POST /api/client/v1/ads/navigation_click_log`

### 请求参数

参数 | 类型 | 必须 | 说明
---- | ---- | ---- | ----
uuid | string | true | 客户端uuid
ad_id | integer | true | 广告id
ad_name | string | true | 广告名称
app_channel | string | true | 渠道
app_version | string | true | 版本
app_version_number | string | true | 版本号

## 1.74 获得域名救援列表


```json
{
    "success": true,
    "message": null,
    "data": {
        "domain_list": [
            {
                "domain_name": "https://api.llzggzjy.com/",
                "domain_type": "owner",
                "response_time": ""
            }
        ],
        "should_update": false
    }
}
```
### HTTP请求

`GET /api/client/v1/rescue_domains/list`

### 返回信息

字段名 | 说明
---- | ----
should_update | 救援域名是否需要强制更新
domain_list.domain_name | 救援域名地址
domain_list.domain_type | 救援域名类型(owner自有, third_party第三方)
domain_list.response_time | 救援域名响应时间


# 2 代理服务器接口文档

代理服务器接口在1秒钟访问频繁时，除了第一次请求，后面的其他请求会统一返回错误码`99`，这时可以设置忽略此返回。

## 2.1 验证登录凭证

代理服务器会在每个tcp连接的时候做检查，检查如果通过就会缓存，缓存1分钟后失效，新来的tcp连接在缓存失效后会重新检查

20170911更新:

- 此接口在同一时间收到同一用户的多次请求后，只会处理第一个请求
- 此接口在检查用户服务到期后，会自动续费

```json
{
    "success": true,
    "channel": "appStore",
    "version": "jichu"
}
```

### HTTP请求

`POST /api/proxy/v1/authenticate`

### 请求参数

参数 | 类型 | 必须 | 说明
---- | ---- | ---- | ----
user_id | integer | true | 用户ID
node_id | integer | true | 服务器ID
token | string | true | session token，用来验证用户是否有权限连接

### 错误码

错误码 | 说明
---- | ----
1 | 用户认证失败, 因为钻石不够而导致(代理服务器定时调用`2.1`接口检查并自动扣除钻石时发现钻石不足时触发token重置)
2 | 用户认证失败, 有另一台设备登录此帐号
3 | 用户服务类型到期，后端自动续费开启，客户端调`1.37`接口来为服务续费(由于支持了自动续费，所以此错误目前已不会再返回)
4 | 代理服务器不存在
5 | 代理服务器爆满
6 | 用户类型等级不够
7 | 用户服务类型到期，后端自动续费关闭，此时应关闭客户端代理服务连接
8 | 用户帐号被停用，此时应关闭客户端代理服务连接
99 | 访问过于频繁

### 返回信息

字段名 | 说明
---- | ----
channel | 渠道名
version | 版本名

## 2.2 检查用户服务器类型是否过期

此接口用于在用户连接着代理服务器时，检查用户是服务是否过期

```json
{
  "success": true,
}
```

### HTTP请求

`POST /api/proxy/v1/users/inspect_expiration`

### 请求参数

参数 | 类型 | 必须 | 说明
---- | ---- | ---- | ----
node_id | string | true | 节点ID
user_id | string | true | 用户ID

## 2.3 记录连接日志

```json
{
  "success": true
}
```

### HTTP请求

`POST /api/proxy/v1/connection_logs`

### 请求参数

参数 | 类型 | 必须 | 说明
---- | ---- | ---- | ----
user_id | string | true | 用户ID
session_id | string | true | session id
token | string | true | session token
node_region_id | string | true | 代理服务器所在的节点服务器区域ID
node_id | string | true | 二级代理服务器ID
target_address | string | true | 目标服务器完整URL
target_ip | string | true | 目标服务器IP
in_bytes | int | true | 入站流量
out_bytes | int | true | 出站流量
cost_time | int | true | 花费时间
client_ip | string | true | 用户客户端IP
first_proxy_ip | string | true | 一级代理服务器IP

## 2.4 记录指定服务器线路的在线用户列表

用于记录指定服务器线路的在线用户列表及在线用户数

```json
{
  "success": true
}
```

### HTTP请求

`POST /api/proxy/v1/nodes/record_online_users`

### 请求参数

参数 | 类型 | 必须 | 说明
---- | ---- | ---- | ----
node_id | integer | true | 服务器线路ID
data | string | true | 在线用户列表

data参数示例: `{"users": [{"id": 2, "ip":"192.168.1.1"},{"id": 3, "ip":"192.168.1.2"},{"id": 4, "ip":"192.168.2.2"}]}`

data参数说明:

参数 | 说明
---- | ----
users | 用户列表，数组类型
users.id | 用户ID
users.ip | 用户IP


## 2.5 记录指定服务器线路的详情信息

此接口用于记录服务器线路的详情信息，用于后台管理显示查看

```json
{
  "success": true
}
```

### HTTP请求

`POST /api/proxy/v1/nodes/record_detail`

### 请求参数

参数 | 类型 | 必须 | 说明
---- | ---- | ---- | ----
ip | string | true | 服务器ip
current_bandwidth | integer | true | 当前带宽占用百分比
cpu_percent | integer | true | CPU使用率
memory_percent | integer | true | 内存使用率
transfer | integer | true | 用户使用的总流量
network_speed_up | integer | true | 当前网络上传速度
network_speed_down | integer | true | 当前网络下载速度
disk_percent | integer | true | 硬盘空间占用百分比（原: 磁盘剩余空间)

## 2.6 获得自己的IP地址

```json
{
  "success": true,
  "ip": "127.0.0.1"
}
```

### HTTP请求

`GET /api/proxy/v1/remote_ip`

### 返回信息

字段名 | 说明
---- | ----
ip | 自己的IP

## 2.7 获得指定节点服务器的限速值

```json
{
  "success": true,
  "limit_up": 10240,
  "limit_down": 10240
}
```

### HTTP请求

`GET /api/proxy/v1/nodes/limit_speed`

### 请求参数

参数 | 类型 | 必须 | 说明
---- | ---- | ---- | ----
node_id | integer | true | 服务器id
user_id | integer | false | 用户id(预留)

### 返回信息

字段名 | 说明
---- | ----
limit_up | 上传限速
limit_down | 下载限速

## 2.8 记录用户使用的流量

```json
{
  "success": true
}
```

### HTTP请求

`POST /api/proxy/v1/users/record_bytes`

### 请求参数

参数 | 类型 | 必须 | 说明
---- | ---- | ---- | ----
user_id | integer | true | 用户id
node_id | integer | true | 服务器id
used_bytes | integer | true | 用户此次使用的流量

## 2.9 客户端日志提交验证

```json
{
    "success": true,
    "data": {
        "token": "asdf93kd9",
        "key": "239dk390dk",
        "enabled": true
    }
}
```

### HTTP请求

`GET /api/proxy/v1/users/client_log`

### 返回

参数 | 类型
---- | ----
token | 提交接口验证
key | 数据加解密密钥
enabled | 是否开启客户端日志提交

## 2.10 代理服务器黑名单

```json
{
    "success": true,
    "proxy_black_list": "baidu"
}
```

### HTTP请求

`GET /api/proxy/v1/nodes/proxy_black_list`

### 返回

参数 | 类型
---- | ----
proxy_black_list | 黑名单(回车换行分隔)


## 2.11 获取域名分流规则

* 如果要block一个网站,对应的`target_rule`只要把`node_type`填成`-1`就可以了,代理匹配到此规则会直接类型连接
* `target_rule` 举例说明:
  * 指定到第3洲: continent_id=3, country_id=0, region_id=0
  * 指定到第3洲的第2国: continent_id=3, country_id=2, region_id=0
* `cidr_string` 格式如下: `192.168.1.0/24`, `192.168.1.23/32`
* `domains`中的正规表达式注意符号转义,如原始是`.*\.?baidu.*`,转义后是 `.*\\\.?baidu.*`
* 注意如果不同`rules`中的`domains`里面有有重复的,其结果为不确定会匹配到哪一个
* `domains`, `cidr_string`可以都为空,那此条规则就无效果,会走默认二级代理
* `node_type`一般填0,因为此配置表是不区分线路分配给一级代理的,所以除非是`node_type=-1`,不然非0值没有意义

```json
{
    "success": true,
    "rules": [
        {
            "id": 1,
            "domains": [
                ".*///.?baidu.*"
            ],
            "cidr_string": [
                "103.235.46.0/24"
            ],
            "target_rule": {
                "node_type": 0,
                "continent_id": 1,
                "country_id": 1,
                "region_id": 1
            }
        },
        {
            "id": 2,
            "domains": [
                ".*///.?google.*",
                ".*///.?gstatic.*",
                ".*///.?ytimg.*",
                ".*///.?youtube.*",
                ".*///.?googlevideo.*"
            ],
            "target_rule": {
                "node_type": 1,
                "continent_id": 2,
                "country_id": 2,
                "region_id": 2
            }
        },
        {
            "id": 3,
            "domains": [
                ".*///.?facebook.*",
                ".*///.?fbcdn.net"
            ],
            "target_rule": {
                "node_type": 1,
                "continent_id": 2,
                "country_id": 2,
                "region_id": 2
            }
        },
        {
            "id": 4,
            "domains": [
                ".*///.?twitter.*",
                ".*///.?twimg.com"
            ],
            "target_rule": {
                "node_type": 1,
                "continent_id": 2,
                "country_id": 2,
                "region_id": 2
            }
        },
        {
            "id": 5,
            "domains": [
                ".*///.?usatoday.*"
            ],
            "cidr_string": [
                "151.101.2.0/24",
                "151.101.66.0/24"
            ],
            "target_rule": {
                "node_type": -1,
                "continent_id": 2,
                "country_id": 2,
                "region_id": 2
            }
        }
    ]
}
```

### HTTP请求

`GET /api/proxy/v1/nodes/shunt_list`

### 返回

参数 | 类型
---- | ----
domains|要匹配的域名数组|正则表达式
cidr_string| ip段cidr数组
target_rule| 此规则对应的二级匹配规则
node_type | 线路类型| =0时为不设置此条件,=-1时,表示此规则为block,不分配任何
continent_id| 洲ID|=0时不设置此条件
country_id|国家ID|=0时不设置此条件
region_id|区域ID|=0时不设置此条件


## 2.12 获取二级代理列表

* `continent_id`,`country_id`,`region_id`全部为唯一ID
* 只返回请求参数`node_id`对应的`node_type`线路类型的二级代理,就是说`基础一级`返回`基础二级`,`高级二级`返回`高级二级`
* 只返回可以使用的二级,下架之类的不要

```json
{
    "success": true,
    "proxy_list": [
        {
            "node_id": 2,
            "node_type_id": 1,
            "region_id": 1,
            "country_id": 20,
            "continent_id": 2,
            "server": "129.204.6.155",
            "port": 8388,
            "method": "aes-128-cfb",
            "password": "asdf",
            "in_china": 1
        },
        {
            "node_id": 4,
            "node_type_id": 1,
            "region_id": 4,
            "country_id": 20,
            "continent_id": 2,
            "server": "123.206.24.147",
            "port": 8488,
            "method": "aes-128-cfb",
            "password": "asdf",
            "in_china": 1
        },
        {
            "node_id": 12,
            "node_type_id": 1,
            "region_id": 38,
            "country_id": 20,
            "continent_id": 2,
            "server": "49.51.203.46",
            "port": 8488,
            "method": "aes-128-cfb",
            "password": "asdf",
            "in_china": 1
        },
        {
            "node_id": 14,
            "node_type_id": 1,
            "region_id": 35,
            "country_id": 20,
            "continent_id": 2,
            "server": "49.51.34.49",
            "port": 8488,
            "method": "aes-128-cfb",
            "password": "asdf",
            "in_china": 0
        }
    ]
}
```

### HTTP请求

`GET /api/proxy/v1/nodes/proxy_list`

### 返回

参数 | 类型
---- | ----
node_id|节点ID
node_type|节点线路类型
continent_id|大洲ID
country_id|国家ID
region_id|区域ID
server|服务器IP地址
port|服务器端口
method|加密方式
password|密码
in_china|是否在中国 1:是/0:否


## 2.13 获得节点测试地址

```json
{
    "success": true,
    "test_address": [
        {
            "in_china": 0,
            "address": [
                "https://google.com",
                "https://twitter.com",
                "https://youtube.com"
            ]
        },
        {
            "in_china": 1,
            "address": [
                "http://youku.com",
                "http://baidu.com"
            ]
        }
    ]
}
```

### HTTP请求

`GET /api/proxy/v1/nodes/test_address`

### 返回

参数 | 类型
---- | ----
in_china|是否在中国 1:是/0:否
address|测试域名地址
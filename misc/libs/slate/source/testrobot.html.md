---
title: 测试机器人接口文档

search: false
---

# 1 测试机器人接口文档

接口调用流程:

- 首先调用[客户端登录接口](/docs/client_v3#1-4)，获取 `proxy_session_id`
- 然后调用[机器人测试程序服务器列表](/docs/testrobot#1-2)接口，获取`session_token`、代理服务器列表和要测试的域名
- 使用`session_token`对代理服务器进行测试
- 调用[机器人启用和禁用服务器节点](/docs/testrobot#1-4)接口，对服务器进行下架上架操作

接口认证:

测试机器人连接`此文档`所列出的接口时，需要在`headers`中加入认证头，如:

`AUTHTOKEN: xxxxxxxxx`

## 1.1 测试机器人连接代理服务器的说明

客户端连接代理服务器需要的三个参数:

- `account_id`: 对应`登录`接口返回数据中的`user.id`
- `sesson_id`: 对应`登录`接口返回数据中的`proxy_session_id`
- `token`: 对应`登录`接口返回数据中的`proxy_session_token`

## 1.2 获取测试程序代理服务器列表及域名列表(认证)

此接口会根据客户端的IP，判断客户端在国内还是国外，并返回相应的需要测试的一级代理服务器信息及域名数据

```json
{
    "success": true,
    "message": null,
    "data": {
        "nodes": [
            {
                "id": 1,
                "name": "深圳01-l1",
                "url": "119.23.251.73",
                "port": 8388,
                "password": "asdf",
                "encrypt_method": "aes-128-cfb",
                "allow_fault_times": 0
            },
            {
                "id": 7,
                "name": "新加坡01-l1",
                "url": "47.88.218.44",
                "port": 8388,
                "password": "asdf",
                "encrypt_method": "aes-128-cfb",
                "allow_fault_times": 0
            }
        ],
        "domains": [
            {
                "id": 1,
                "domain": "http://google.com",
                "failure_times": 0
            },
            {
                "id": 2,
                "domain": "http://twitter.com",
                "failure_times": 0
            },
            {
                "id": 3,
                "domain": "http://youtube.com",
                "failure_times": 0
            }
        ]
    }
}
```

### HTTP请求

`GET /api/testrobot/v1/nodes/test_data`

### 返回信息

字段名 | 说明
---- | ----
node.id | 代理服务器id
node.name | 代理服务器名称
node.url | 代理服务器IP或域名
node.port | 代理服务器端口
node.password | 代理服务器密码
node.encrypt_method | 代理服务器加密方式
node.node_type_id | 服务器对应的服务类型ID
domain.id | 域名id, 用于以后提交域名测试失败日志时使用
domain.domain | 域名URL

## 1.2.2 获取测试程序代理服务器列表及域名列表V2(认证)

此接口会根据客户端的IP，判断客户端在国内还是国外，并返回相应的需要测试的一级代理服务器信息及域名数据
相比v1，还会返回session_token

```json
{
    "success": true,
    "message": null,
    "data": {
        "session_token": "22222",
        "nodes": [
            {
                "id": 1,
                "name": "深圳01-l1",
                "url": "119.23.251.73",
                "port": 8388,
                "password": "asdf",
                "encrypt_method": "aes-128-cfb",
                "node_type_id": 1
            },
            {
                "id": 3,
                "name": "深圳02-l1",
                "url": "39.108.11.252",
                "port": 8388,
                "password": "asdf",
                "encrypt_method": "aes-128-cfb",
                "node_type_id": 2
            }
        ],
        "domains": [
            {
                "id": 2,
                "domain": "http://twitter.com"
            },
            {
                "id": 3,
                "domain": "http://youtube.com"
            }
        ]
    }
}
```

### HTTP请求

`GET /api/testrobot/v2/nodes/test_data`

### 请求参数

参数 | 类型 | 必须 | 说明
---- | ---- | ---- | ----
session_id | string | true | 在登录接口中获得

### 返回信息

字段名 | 说明
---- | ----
session_token | 连接代理服务器需要的token
node.id | 代理服务器id
node.name | 代理服务器名称
node.url | 代理服务器IP或域名
node.port | 代理服务器端口
node.password | 代理服务器密码
node.encrypt_method | 代理服务器加密方式
node.node_type_id | 服务器对应的服务类型ID
domain.id | 域名id, 用于以后提交域名测试失败日志时使用
domain.domain | 域名URL

## 1.3 提交测试日志(认证)

此接口用于测试机器人提交测试结果日志，并按指定规则触发预警

测试机器人每测试完一台服务器就发送一次数据

```json
{
    "success": true
}
```

### HTTP请求

`POST /api/testrobot/v1/logs/test_logs`

### 请求参数

参数 | 类型 | 必须 | 说明
---- | ---- | ---- | ----
node_id | string | true | 服务器ID
data | string | true | 测试数据集合

#### data说明

多个测试日志信息使用`|`分割，示例:

`1,1,1|1,2,0`

**单个测试日志信息项依次为:**

- 用户ID
- 测试域名ID
- 1:成功, 0:失败

## 1.4 启用和禁用服务器节点(认证)

此接口用于启用或禁用服务器节点

```json
{
    "success": true
}
```

### HTTP请求

`POST /api/testrobot/v1/nodes/switch_status`

### 请求参数

参数 | 类型 | 必须 | 说明
---- | ---- | ---- | ----
node_id | string | true | 服务器ID
status | string | true | 状态, 1:启用, 0:禁用

## 1.5 提交节点上下架状态(认证)

此接口用于提交节点的上下架记录状态

```json
{
    "success": true
}
```

### HTTP请求

`POST /api/testrobot/v1/nodes/record_switch_status`

### 请求参数

参数 | 类型 | 必须 | 说明
---- | ---- | ---- | ----
data | string | true | 节点上下架数据，json格式

参数实例:

```json
{
  "nodes": {
    "10": {
      "node_id": 10,
      "total_test_count": 2,
      "total_failed_count": 2,
      "total_failed_rate": 1,
      "record_test_count": 2,
      "record_failed_count": 2,
      "record_failed_rate": 1,
      "current_status": 1,
      "average_response_time": 0,
      "node_test_result_list": "..|.|.|",
      "switch_on_count": 0,
      "switch_off_count": 0
    },
    "12": {
      "node_id": 12,
      "total_test_count": 2,
      "total_failed_count": 2,
      "total_failed_rate": 1,
      "record_test_count": 2,
      "record_failed_count": 2,
      "record_failed_rate": 1,
      "current_status": 1,
      "average_response_time": 0,
      "node_test_result_list": "...",
      "switch_on_count": 0,
      "switch_off_count": 0
    }
  },
  "last_round_cost_time": 0,
  "version": {
    "Major": 1,
    "Minor": 2,
    "Build": 4,
    "BuildDate": "2017-11-06T15:35:00Z",
    "Desc": "auto switch node"
  },
  "last_test_end_at": "0001-01-01T00:00:00Z"
}
```


## 1.6 注册user为机器人账号(认证)

此接口用于注册user为机器人账号

```json
{
    "success": true,
    "message": null,
    "data": {}
}
```

### HTTP请求

`POST /api/testrobot/v1/users/register_self`

### 请求参数

参数 | 类型 | 必须 | 说明
---- | ---- | ---- | ----
user_id | integer | true | 用户id

### 返回信息

如果请求不成功，返回的数据会包含一个`integer`型的`error_code`字段，用来代表出错类型:


错误代码 | 说明
---- | ----
1 | 用户不存在
2 | 用户被封号

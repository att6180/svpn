---
title: SVPN客户端API文档V2

search: false
---

# 1 客户端接口文档V2

<aside class="success">
这是v2版的客户端接口

在此版中出现的接口需优先使用，没有出现的接口依然使用<a href="/docs/index">旧接口</a>
</aside>

## 1.4 用户登录

<aside class="success">
此接口已更新新版: <a href="/docs/client_v4">v4</a>
</aside>

关于 `app_launch_id` 的说明:

- 用户全新运行客户端(后台没有运行的前提下)，调登录接口时，不传`app_launch_id`参数
- 用户按Home键将客户端切至后台运行，一段时间后，再次打开客户端(从后台切出)，调登录接口时，传 `app_launch_id`参数，值为上一次全新打开客户端调登录接口时返回的值

> 当只使用uuid登录，并且此设备已经修改过用户名或密码时，返回的数据如下:

```json
{
  "success": true,
  "message": null,
  "server_time": 1492414397,
  "data": {
    "only_has_username": true,
    "username": "13584634396"
  }
}
```

> 其他情况，则返回:

```json
{
  "success": true,
  "message": null,
  "data": {
    "only_has_username": false,
    "proxy_session_id": 163,
    "proxy_session_token": "cz6lFjMdpo",
    "app_launch_id": "823ecb84-1b72-11e7-845d-784f4352f7e7",
    "signin_log_id": "823adbaa-1b72-11e7-845d-784f4352f7e7",
    "user": {
      "api_token": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpZCI6MTYzLCJ1c2VybmFtZSI6InN1cDAwMDAwMTYzIiwiZGV2aWNlX2lkIjoxNjF9.yERJ8mfGk4MXuQUQWt8JT32x16WLetoonoNBZ0Y9nNg",
      "id": 163,
      "username": "sup00000163",
      "password": "kvsxvzz7",
      "email": null,
      "wechat": null,
      "weibo": null,
      "qq": null,
      "used_bytes": 0,
      "max_bytes": 0,
      "limit_bytes": 0,
      "total_payment_amount": "0.0",
      "current_coins": 0,
      "total_coins": 0,
      "is_checkin_today": false,
      "is_enabled": true,
      "created_at": 1491556428,
      "new_message": false
    },
    "group": {
      "id": 1,
      "name": "vip0",
      "level": 1
    },
    "device": {
      "id": 161,
      "uuid": "a1de3867-7468-4002-8dd4-554707c4edc5"
    },
    "user_node_types": [
      {
        "id": 811,
        "name": "青铜服",
        "level": 1,
        "status": "按次",
        "expired_at": 1491556428,
        "used_count": 0,
        "node_type_id": 1
      },
      {
        "id": 815,
        "name": "钻石服",
        "level": 5,
        "status": "按次",
        "expired_at": 1491556428,
        "used_count": 0,
        "node_type_id": 5
      }
    ],
    "node_types": [
      {
        "id": 1,
        "name": "青铜服",
        "level": 1,
        "expense_coins": 1,
        "user_group_id": 1,
        "user_group_level": 1
      },
      {
        "id": 3,
        "name": "黄金服",
        "level": 3,
        "expense_coins": 5,
        "user_group_id": 5,
        "user_group_level": 5
      },
      {
        "id": 5,
        "name": "钻石服",
        "level": 5,
        "expense_coins": 20,
        "user_group_id": 9,
        "user_group_level": 9
      }
    ],
    "ip_region": {
        "country": "本机地址",
        "province": "本机地址",
        "city": null
    },
    "settings": {
      "APPSTORE_ID": "1206882364",
      "IOS_VERSION": "1.0.3|1",
      "SHARE_IMG": "http://120.77.247.50/ui/img/logo.png",
      "IS_UPDATE": "1"
    }
  }
}
```

### HTTP请求

`POST /api/client/v2/signin`

### 请求参数

参数 | 类型 | 必须 | 说明
---- | ---- | ---- | ----
username | string | false | 手机号
password | string | false | 登录密码
device_uuid | string | true | 设备uuid
device_name | string | false | 设备名(如: xxx的iphone)
device_model | string | true | 设备型号(如: iphone5)
platform | string | true | 设备系统平台(如: ios)
system_version | string | true | 设备版本(如: 10)
operator | string | true | 网络运营商(如: unicomm)
net_env | string | true | 网络环境(如: 4g、wifi)
app_version | string | true | 版本名，来自"版本渠道信息接口"获取到的ID
app_version_number | string | true | App版本(如: 1.0)
app_channel | string | true | app渠道, 来自"版本渠道信息接口"获取到的ID
app_launch_id | string | false | 用于记录App运行日志, 此值只在App的一次运行周期内有效，否则不在请求中加入此参数
longitude | string | false | 坐标经度
latitude | string | false | 坐标纬度
address | string | false | 坐标地址
coord_method | string | false | 坐标获取方法(如: ip、gps)


### 返回信息

如果请求不成功，返回的数据会包含一个`integer`型的`error_code`字段，用来代表出错类型:

错误代码 | 说明
---- | ----
1 | 用户不存在
2 | 用户被封号
3 | 用户名或密码错误
23 | 请求被拒绝(防刷)

字段名 | 说明
---- | ----
only_has_username | 用来在只使用uuid登录时表示此设备是否修改过用户名或密码，如果为true，则只返回修改过的用户名，如果为false，则视为正常登录返回所有用户信息
proxy_session_id | 用于连接代理服务器时使用
proxy_session_token | 用于连接代理服务器时使用
app_launch_id | 用于记录App运行日志, 需客户端在App一次运行周期内保存，并在登录请求中加入此参数
signin_log_id | 用户登录日志ID，在请求操作日志等接口时使用
user.api_token | 之后API调用需认证的接口时所需要的token(目前还未设置过期策略)
user.id | 用户id
user.username | 用户的用户名，用户自行修改用户名后，会用来存储手机号
user.email | 用户绑定的email
user.wechat | 用户绑定的微信的openid
user.weibo | 用户绑定的微博的openid
user.qq | 用户绑定的qq的openid
user.used_bytes | 用户已使用的流量(M)
user.max_bytes | 用户可用的最大流量(byte), -1: 无限制
user.limit_bytes | 用户被限制的速度(KB/s), -1: 无限制
user.total_payment_amount | 用户的消费累计金额
user.current_coins | 用户当前钻石数
user.total_coins | 用户拥有过的总钻石数
user.is_checkin_today | 用户今天是否签到过, true: 是, false: 否
user.is_enabled | 用户帐号状态(如: 被封号)，true:正常, false:已封号
user.created_at | 帐号创建时间
user.new_message | 是否有新消息, true: 是, false: 否
group.id | 用户组id
group.name | 用户组名称
group.level | 用户组等级
device.id | 当前设备ID
device.uuid | 当前设备uuid
regions.id | 服务器节点区域ID
user_node_types.id | 用户服务器类型ID
user_node_types.name | 用户服务器类型名称
user_node_types.level | 用户服务器类型级别
user_node_types.status | 用户服务器类型状态
user_node_types.expired_at | 用户服务器类型过期时间
user_node_types.used_count | 用户服务器类型使用次数
user_node_types.node_type_id | 服务器类型ID，用来对应服务器线路列表中的node_type的ID
ip_region.country | 国家,该字段有可能值为 null
ip_region.province | 省/州,该字段有可能值为 null
ip_region.city | 城市,该字段有可能值为 null
node_types.id | 服务器类型ID
node_types.name | 服务器类型名称
node_types.level | 服务器类型等级
node_types.expense_coins | 连接此服务器类型需要扣除了钻石数
node_types.user_group_id | 连接此服务器类型所需要的最低用户类型ID
node_types.user_group_level | 连接此服务器类型所需要的最低用户类型等级
settings | 后台管理配置的参数，具体配置项由开发者自行制定

## 1.12 获得所有服务器线路数据(认证)

<aside class="success">
此接口已更新新版: <a href="/docs/client_v3">v3</a>
</aside>

```json
{
  "success": true,
  "message": null,
  "data": {
    "user_node_types": [
      {
        "id": 811,
        "name": "青铜服",
        "level": 1,
        "status": "按次",
        "expired_at": 1491556428,
        "used_count": 0,
        "node_type_id": 1
      },
      {
        "id": 815,
        "name": "钻石服",
        "level": 5,
        "status": "按次",
        "expired_at": 1491556428,
        "used_count": 0,
        "node_type_id": 5
      }
    ],
    "node_types": [
      {
        "id": 1,
        "name": "青铜服",
        "level": 1,
        "expense_coins": 1,
        "user_group_id": 1,
        "user_group_level": 1,
        "description": "测试1"
      },
      {
        "id": 5,
        "name": "钻石服",
        "level": 5,
        "expense_coins": 20,
        "user_group_id": 9,
        "user_group_level": 9,
        "description": "测试1"
      }
    ]
  }
}
```

### HTTP请求

`GET /api/client/v2/nodes`

### 返回信息

字段名 | 说明
---- | ----
user_node_types.id | 用户服务器类型ID
user_node_types.name | 用户服务器类型名称
user_node_types.level | 用户服务器类型级别
user_node_types.status | 用户服务器类型状态
user_node_types.expired_at | 用户服务器类型过期时间
user_node_types.used_count | 用户服务器类型使用次数
user_node_types.node_type_id | 服务器类型ID，用来对应服务器线路列表中的node_type的ID
node_types.id | 服务器类型ID
node_types.name | 服务器类型名称
node_types.level | 服务器类型等级
node_types.expense_coins | 连接此服务器类型需要扣除了钻石数
node_types.user_group_id | 连接此服务器类型所需要的最低用户类型ID
node_types.user_group_level | 连接此服务器类型所需要的最低用户类型等级
node_types.description | 服务类型描述，字符中的&#124;表示换行

## 1.15 按指定服务器类型连接(认证)

> 结果1: 用户服务器类型未过期时

```json
{
  "success": true,
  "message": null,
  "data": {
    "user_node_type_is_expired": false,
    "is_consumed": false,
    "proxy_session_token": "Ywvz4YkAi_b9hU2IuGnxRSOePJJKz6tfk95N1PF_y1s=",
    "user": {
      "current_coins": 9958
    },
    "user_node_type": {
      "id": 616,
      "expired_at": 1493203060,
      "status": "按次",
      "used_count": 3
    },
    "node": {
        "id": 1,
        "name": "pkSN75HEoq9nWhuVTThRvA==",
        "url": "LdXg1+meZqeQ4vi6SLeDBg==",
        "port": "OWJ+dDe88OnJ30DtKx/vEg==",
        "encrypt_method": "pj2110m0YMAY/gpHz4tdfA==",
        "password": "YLvZlicyeQ6Um7Cg6vxLTA=="
      },
      "tab_id": 2
  }
}
```

> 结果2: 用户服务器类型已过期，但客户端还未确认扣除钻石时

```json
{
  "success": true,
  "message": null,
  "data": {
    "user_node_type_is_expired": true,
    "is_consumed": false
  }
}
```

> 结果3: 用户服务器类型已过期，客户端确认扣除钻石时

```json
{
  "success": true,
  "message": null,
  "data": {
    "user_node_type_is_expired": true,
    "is_consumed": true,
    "proxy_session_token": "Ywvz4YkAi_b9hU2IuGnxRSOePJJKz6tfk95N1PF_y1s=",
    "user": {
      "current_coins": 9957
    },
    "user_node_type": {
      "id": 616,
      "expired_at": 1492511860,
      "status": "按次",
      "used_count": 3
    },
    "node": {
        "id": 1,
        "name": "pkSN75HEoq9nWhuVTThRvA==",
        "url": "LdXg1+meZqeQ4vi6SLeDBg==",
        "port": "OWJ+dDe88OnJ30DtKx/vEg==",
        "encrypt_method": "pj2110m0YMAY/gpHz4tdfA==",
        "password": "YLvZlicyeQ6Um7Cg6vxLTA=="
      },
      "tab_id": 2
  }
}
```

### HTTP请求

`POST /api/client/v2/nodes/connect`

### 请求参数

参数 | 类型 | 必须 | 说明
---- | ---- | ---- | ----
node_type_id | integer | true | 服务器类型ID(告诉后端要连接哪个类型的服务器)
allow_consume | integer | false | 是否允许扣除钻石(1:扣除,0:不扣)，默认可不加此参数，当连接的服务器类型已过期，服务器会返回是否需要扣除钻石，当用户确认要扣除时，再次请求此接口，并加上此参数

### 返回信息

返回信息的三种状态是根据`user_node_type_is_expired`和`is_consumed`来判断的:

1. 服务器类型未过期
  - `user_node_type_is_expired: false`
  - `is_consumed: false`
2. 服务器类型已过期，需要客户端确认扣除钻石
  - `user_node_type_is_expired: true`
  - `is_consumed: false`
3. 服务器类型已过期，客户端确认扣除钻石
  - `user_node_type_is_expired: true`
  - `is_consumed: true`

字段名 | 说明
---- | ----
user_node_type_is_expired | 用户指定要连接的服务器类型是否已过期
is_consumed | 是否已扣除钻石
*proxy_session_token | (需解密)连接代理服务器时使用的token，把token放在这里是因为后端会检查在线的用户服务是否到期，到期后会重置token
user.current_coins | 用户当前钻石数
user_node_type.id | 用户服务器类型ID
user_node_type.expired_at | 用户服务器类型新的到期时间
user_node_type.status | 用户服务器类型状态
user_node_type.used_count | 用户服务器类型本月的使用次数
node.id | 代理服务器ID
*node.name | (需解密)代理服务器名
*node.url | (需解密)代理服务器地址
*node.port | (需解密)代理服务器端口
*node.encrypt_method | (需解密)代理服务器加密方式
*node.password | (需解密)代理服务器密码
tab_id | 对应套餐标签页ID

### 错误码

错误码 | 说明
---- | ----
1 | 服务器类型不存在
2 | 您的VIP等级太低
3 | 您的钻石不足
4 | 没有可用的服务器线路

## 1.16 获得套餐数据(认证)

```json
{
    "success": true,
    "message": null,
    "data": {
        "tabs": [
            {
                "id": 3,
                "name": "精英套餐",
                "is_recommend": false,
                "plans": [
                    {
                        "id": 3,
                        "name": "狸猫jichu2",
                        "price": "10.0",
                        "currency_symbol": "¥",
                        "is_iap": false,
                        "iap_id": "",
                        "is_regular_time": true,
                        "time_type": 0,
                        "node_type_id": 1,
                        "coins": 0,
                        "present_coins": 0,
                        "description1": "1",
                        "description2": "11",
                        "description3": "111"
                    }
                ]
            },
            {
                "id": 1,
                "name": "精英套餐",
                "is_recommend": true,
                "plans": [
                    {
                        "id": 1,
                        "name": "狸猫jichu1",
                        "price": "10.0",
                        "currency_symbol": "¥",
                        "is_iap": false,
                        "iap_id": "",
                        "is_regular_time": true,
                        "time_type": 0,
                        "node_type_id": 1,
                        "coins": 0,
                        "present_coins": 0,
                        "description1": "1",
                        "description2": "11",
                        "description3": "111"
                    }
                ]
            }
        ]
    }
}
```

### HTTP请求

`GET /api/client/v2/plans`

### 请求参数

如果渠道和版本匹配上，则返回后台管理匹配的套餐，只要有其中一个没有匹配上，则返回通用套餐。

参数 | 类型 | 必须 | 说明
---- | ---- | ---- | ----
platform | string | true | 平台
app_channel | string | true | 客户端渠道名
app_version | string | true | 客户端版本名
app_version_number | string | true | 客户端版本号

### 返回信息

字段名 | 说明
---- | ----
tabs.id | 套餐标签页ID
tabs.name | 套餐标签页名称
tabs.is_recommend | 是否是推荐
tabs.description | 描述
tabs.plans.id | 套餐ID
tabs.plans.name | 套餐名
tabs.plans.currency_symbol | 货币符号
tabs.plans.is_iap | 是否是IAP套餐
tabs.plans.iap_id | IAP套餐ID，可根据is_iap字段判断是否取本字段
tabs.plans.is_regular_time | 是否是包时间套餐, true:是/false:否
tabs.plans.price | 套餐金额
tabs.plans.coins | 套餐包含的钻石数
tabs.plans.present_coins | 套餐赠送的钻石数
tabs.plans.is_recommend | 是否为推荐
tabs.plans.description1 | 描述1, 字称下方
tabs.plans.description2 | 描述2, 描述1右方
tabs.plans.description3 | 描述3，价格下方

## 1.18 苹果平台支付二次验证(认证)

```json
{
  "success": true,
  "message": null,
  "data": {
    "user": {
      "current_coins": 10
    },
    "group": {
      "id": 3,
      "name": "VIP2",
      "level": 3,
      "need_coins": 10
    }
  }
}
```

### HTTP请求

`POST /api/client/v2/plans/verify_iap`

### 请求参数

参数 | 类型 | 必须 | 说明
---- | ---- | ---- | ----
app_version | string | false | 客户端版本，用来根据版本来区别是否使用审核版本的套餐
app_version_number | string | true | 客户端版本号，用来兼容旧版本的支付和是否使用审核版本的套餐
receipt_data | string | true | 购买凭证(base64形式)

### 返回信息

字段名 | 说明
---- | ----
user.current_coins | 用户当前的钻石数
group.id | 用户类型ID(用户钻石累记达到一定数量，会升级成相应的用户类型)
group.name | 用户类型名
group.level | 用户类型等级
group.need_coins | 升级到此类型需要的累计钻石数

## 1.19 创建Comsunny支付订单(H5页面)(认证)

<aside class="success">
此接口已更新新版: <a href="/docs/client_v4">v4</a>
</aside>

流程:

- 客户端使用此接口创建订单，获取`订单号` 和 `h5支付页面URL`
- 客户端打开 `h5支付页面` 进行支付
- 支付平台向`客户端`同步返回支付结果，同时向`后端服务器`异步发送支付结果，后端webhook地址: `{域名}/api/client/v1/comsunny_callback`
- 客户端在收到支付结果后，使用`后端订单号`向后端编号`1.20`接口查询订单状态并获得用户最新状态

```json
{
  "success": true,
  "message": null,
  "data": {
    "transaction_log": {
      "order_number": "20170527149586928854346b6",
      "payment_url": "https://dispatch.5ydoc.com/Api/getOrderView?order_id=20170527149586928854346b6"
    }
  }
}
```

### HTTP请求

`POST /api/client/v2/plans/comsunny_order`

### 请求参数

参数 | 类型 | 必须 | 说明
---- | ---- | ---- | ----
platform | string | true | 平台，和登录接口一致
app_channel | string | true | 渠道，和登录接口一致
app_version | string | true | 版本，和登录接口一致
plan_id | integer | true | 套餐ID

### 错误信息

错误码 | 说明
---- | ----
1 | 支付平台创建订单失败

### 返回信息

字段名 | 说明
---- | ----
transaction_log.order_number | 订单号
transaction_log.payment_url | h5支付页面URL

## 1.22 获取反馈列表V2(认证)

```json
{
    "success": true,
    "message": null,
    "data": {
        "feedbacks": [
            {
                "id": 6,
                "type": 2,
                "content": "The fact ",
                "status": 0,
                "created_at": 1497235791,
                "has_read": false
            },
            {
                "id": 5,
                "type": 0,
                "content": "gtpup说体系嘻嘻嘻嘻嘻嘻嘻嘻 会谈中组图天他小心翼翼与天蝎座总有一个人在万事如意有些人",
                "status": 0,
                "created_at": 1496987506,
                "has_read": false
            },
            {
                "id": 4,
                "type": 3,
                "content": "数据噼里啪啦就噼里啪啦",
                "status": 0,
                "created_at": 1496986955,
                "has_read": false
            },
            {
                "id": 3,
                "type": 2,
                "content": "后所有者权益",
                "status": 1,
                "created_at": 1496980423,
                "has_read": false
            }
        ],
        "current_page": 1,
        "total_pages": 1,
        "total_count": 4
    }
}
```

### HTTP请求

`GET /api/client/v2/feedbacks`

### 请求参数

参数 | 类型 | 必须 | 说明
---- | ---- | ---- | ----
page | integer | false | 指定页码, 默认为1
limit | integer | false | 指定每页的条数，默认为25

### 返回信息

字段名 | 说明
---- | ----
feedbacks.id | 反馈ID
feedbacks.type | 反馈类型, 0:连接问题, 1:充值问题, 2:帐号问题, 3:其他问题
feedbacks.content | 反馈内容
feedbacks.status | 反馈状态, 0:处理中, 1:已回复
feedbacks.created_at | 反馈创建的时间
feedbacks.has_read | 用户是否已读
current_page | 当前页码
total_pages | 总页码数
total_count | 总数

## 1.34 获得客户端路由配置表

<aside class="success">
此接口已更新新版: <a href="/docs/client_v3">v3</a>
</aside>

v2版本的此接口用来返回新的配置文件，并且新增了一个「可选」的新参数，用来区分配置文件的版本

```json
{
  "success": true,
  "message": null,
  "data": {
    "updated_at": 1493879936,
    "d2o": "routes/android_d2o",
    "o2d": "routes/android_o2d",
    "glo_d2o": "routes/android_glo_d2o",
    "glo_o2d": "routes/android_glo_o2d",
    "is_domestic": true,
    "china_ip_update_at": 1493948153,
    "china_ip_list": "routes/china_ip_list.txt",
    "foreign_ip_update_at": 1500078488,
    "foreign_ip_list": "routes/foreign_ip_list.txt"
  }
}
```

### HTTP请求

`GET /api/client/v2/commons/routes`

### 请求参数

参数 | 类型 | 必须 | 说明
---- | ---- | ---- | ----
platform | string | true | 平台, ios/android/pc/mac
version | string | false | 版本号, 如: 2

### 返回信息

字段名 | 说明
---- | ----
updated_at | 最后更新时间
d2o | 国内翻国外的路由表
o2d | 国外翻国内的路由表
glo_d2o | 全局代理国内翻国外需要走直连列表
glo_o2d | 全局代理国外翻国内需要走直连列表
is_domestic | 是否是国内IP
china_ip_update_at | 国内ip列表文件最后更新时间
china_ip_list | 国内ip列表文件路径
foreign_ip_update_at | 国外ip列表文件最后更新时间
foreign_ip_list | 国外ip列表文件路径

## 1.39 创建Comsunny支付订单(二维码)(认证)

流程:

- 客户端使用此接口创建订单，获取`订单号` 和 `支付宝支付二维码URL`
- 客户端显示 `二维码图片` 进行支付
- 支付平台向`客户端`同步返回支付结果，同时向`后端服务器`异步发送支付结果，后端webhook地址: `{域名}/api/client/v1/comsunny_callback`
- 客户端在收到支付结果后，使用`后端订单号`向后端编号`1.20`接口查询订单状态并获得用户最新状态

```json
{
  "success": true,
  "message": null,
  "data": {
    "transaction_log": {
      "order_number": "20170527149586928854346b6",
      "payment_url": "https://dispatch.5ydoc.com/uploads/1498102412000.png"
    }
  }
}
```

### HTTP请求

`POST /api/client/v2/plans/comsunny_order_qrcode`

### 请求参数

参数 | 类型 | 必须 | 说明
---- | ---- | ---- | ----
platform | string | true | 平台, 如: ios/android/pc
app_channel | string | true | 渠道，和登录接口一致
app_version | string | true | 版本，和登录接口一致
plan_id | integer | true | 套餐ID
type | string | false | 支付类型，由于要兼容老客户端，所以此参数为可选

支付类型可选值:

- `alipay_qrcode`:支付宝扫码
- `alipay_h5`:支付宝WAP
- `wx_qrcode`:微信扫码
- `wx_h5`:微信h5
- `qq`:QQ钱包

### 返回信息

字段名 | 说明
---- | ----
transaction_log.order_number | 订单号
transaction_log.payment_url | h5支付页面URL

## 1.42 用户注册

<aside class="success">
此接口已更新新版: <a href="/docs/client_v3">v3</a>
</aside>

```json
{
    "success": true,
    "message": null,
    "data": {
        "proxy_session_id": 151,
        "app_launch_id": "61622080-6b6f-11e7-bf54-784f4352f7e7",
        "signin_log_id": "615f07b0-6b6f-11e7-bf54-784f4352f7e7",
        "user": {
            "api_token": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpZCI6MTU2LCJ1c2VybmFtZSI6IjEzODg0NjMzMzk2IiwiZGV2aWNlX2lkIjoxNTR9.gyzxNP7fuCBZYA9eE_vtJl-w3hN3QaUUtFxDe9FBDZc",
            "id": 156,
            "username": "13884644496",
            "email": null,
            "total_payment_amount": "0.0",
            "current_coins": 5,
            "total_coins": 0,
            "password": "KmnCus5VN49nD2gpzwFooA==",
            "is_checkin_today": false,
            "is_enabled": true,
            "created_at": 1500351177,
            "new_message": false,
            "promo_code": "hgybrw",
            "promo_users_count": 0,
            "promo_coins_count": 0,
            "binded_promo_code": 'kdiekd',
            "auto_renew": true,
            "debug_log_enabled": false,
            "debug_log_start_at": "",
            "debug_log_end_at": "",
            "debug_log_max_days": 7
        },
        "group": {
            "id": 1,
            "name": "VIP0",
            "level": 1
        },
        "user_node_types": [
            {
                "id": 332,
                "name": "精英服务",
                "level": 1,
                "status": "按次",
                "expired_at": 1500351177,
                "used_count": 0,
                "node_type_id": 1
            },
            {
                "id": 333,
                "name": "王者服务",
                "level": 2,
                "status": "按次",
                "expired_at": 1500351177,
                "used_count": 0,
                "node_type_id": 2
            }
        ],
        "node_types": [
            {
                "id": 1,
                "name": "精英服务",
                "level": 1,
                "expense_coins": 1,
                "user_group_id": 1,
                "user_group_level": 1,
                "description": "访问Facebook、twitter、instagram等网站；使用telegram等通讯工具；|当月累计使用20次，本月即可免费使用该服务"
            },
            {
                "id": 2,
                "name": "王者服务",
                "level": 2,
                "expense_coins": 3,
                "user_group_id": 1,
                "user_group_level": 1,
                "description": "享受“精英服务”所有内容；|流畅观看高清youtube、twich视频；|当月累计使用18次，本月即可免费使用该服务。"
            }
        ],
        "ip_region": {
            "country": "本机地址",
            "province": "本机地址",
            "city": null
        },
        "settings": {
            "NOTICE_CONTENT": "all",
            "QQ_GROUP": "敬请期待",
            "WX_OFFICAL_ACCOUNT": "敬请期待",
            "TELEGRAM_GROUP": "https://t.me/limaojiasuqi",
            "OFFICIAL_WEBSITE": "http://www.limaojs.com/",
            "ALLOW_SEND_LOG": "true",
            "LOG_POOL_MAX_COUNT": "50",
            "POST_LOG_INTERVAL": "600",
            "UPDATE_URL": "http://www.baidu.com/",
            "SHARE_URL": "http://share.licatjsq.com",
            "SHARE_IMG": "https://licatjsq.com/share/1024.png",
            "ANDROID_VERSION": "1.0.8|1",
            "UPDATE_CONTENT": "<div style='text-align:center;'><strong><span style = 'line-height:2;color:#000000;font-size:18px'>老用户须知</span></strong></div> <span style = 'line-height:2;color:#575757;font-size:15px'>欢迎使用狸猫加速器</span>",
            "IS_UPDATE": "0"
        }
    }
}
```

### HTTP请求

`POST /api/client/v2/signup`

### 请求参数

参数 | 类型 | 必须 | 说明
---- | ---- | ---- | ----
telephone | string | true | 手机号
password | string | true | 密码
confirmation_password | string | true | 确认密码
verification_code | string | true | 手机验证码
device_uuid | string | true | 设备uuid
device_name | string | false | 设备名(如: xxx的iphone)
device_model | string | true | 设备型号(如: iphone5)
platform | string | true | 设备系统平台(如: ios)
system_version | string | true | 设备版本(如: 10)
operator | string | true | 网络运营商(如: unicomm)
net_env | string | true | 网络环境(如: 4g、wifi)
app_version | string | true | 版本名，来自"版本渠道信息接口"获取到的ID
app_version_number | string | true | App版本(如: 1.0)
app_channel | string | true | app渠道, 来自"版本渠道信息接口"获取到的ID
app_launch_id | string | false | 用于记录App运行日志, 此值只在App的一次运行周期内有效，否则不在请求中加入此参数

### 返回信息

如果请求不成功，返回的数据会包含一个`integer`型的`error_code`字段，用来代表出错类型:

错误代码 | 说明
---- | ----
1 | 密码不合法
2 | 两次密码输入不一致
3 | 验证码不正确或过期
4 | 手机号已被注册
23 | 请求被拒绝(防刷)

字段名 | 说明
---- | ----
proxy_session_id | 用于连接代理服务器时使用
app_launch_id | 用于记录App运行日志, 需客户端在App一次运行周期内保存，并在登录请求中加入此参数
signin_log_id | 用户登录日志ID，在请求操作日志等接口时使用
user.api_token | 之后API调用需认证的接口时所需要的token(目前还未设置过期策略)
user.id | 用户id
user.username | 用户的用户名，用户自行修改用户名后，会用来存储手机号
*user.password | （需解密)用户密码
user.email | 用户绑定的email
user.total_payment_amount | 用户的消费累计金额
user.current_coins | 用户当前钻石数
user.total_coins | 用户拥有过的总钻石数
user.is_checkin_today | 用户今天是否签到过, true: 是, false: 否
user.is_enabled | 用户帐号状态(如: 被封号)，true:正常, false:已封号
user.created_at | 帐号创建时间
user.new_message | 是否有新消息, true: 是, false: 否
user.promo_code | 用户的推广码
user.promo_users_count | 用户当前已经推广的人数
user.promo_coins_count | 用户累计获得的推广钻石数
user.binded_promo_code | 已绑定的推广码，如果为`""`，则表示还未绑定
user.auto_renew | 是否自动扣除钻石延长有效期
user.debug_log_enabled | 是否上传此用户的调试日志
user.debug_log_start_at | 调试日志开始日期，如: 2017-09-11
user.debug_log_end_at | 调试日志结束时间，如: 2017-09-12
user.debug_log_max_days | 调试日志保存最大天数
group.id | 用户组id
group.name | 用户组名称
group.level | 用户组等级
device.id | 当前设备ID
device.uuid | 当前设备uuid
regions.id | 服务器节点区域ID
user_node_types.id | 用户服务器类型ID
user_node_types.name | 用户服务器类型名称
user_node_types.level | 用户服务器类型级别
user_node_types.status | 用户服务器类型状态
user_node_types.expired_at | 用户服务器类型过期时间
user_node_types.used_count | 用户服务器类型使用次数
user_node_types.node_type_id | 服务器类型ID，用来对应服务器线路列表中的node_type的ID
ip_region.country | 国家,该字段有可能值为 null
ip_region.province | 省/州,该字段有可能值为 null
ip_region.city | 城市,该字段有可能值为 null
node_types.id | 服务器类型ID
node_types.name | 服务器类型名称
node_types.level | 服务器类型等级
node_types.expense_coins | 连接此服务器类型需要扣除了钻石数
node_types.user_group_id | 连接此服务器类型所需要的最低用户类型ID
node_types.user_group_level | 连接此服务器类型所需要的最低用户类型等级
settings | 后台管理配置的参数，具体配置项由开发者自行制定


## 1.3 获取手机短信验证码

此接口会向客户端指定的手机发送短信验证码。

```json
{
  "success": true,
  "messages": "验证码已发送",
  "data": {}
}
```

### HTTP请求

`POST /api/client/v2/send_verification_code`

### 请求参数

参数 | 类型 | 必须 | 说明
---- | ---- | ---- | ----
area_code | integer | true | 区号
telephone | string | true | 手机号
type | integer | true | 验证码的使用场景 0:修改用户名, 1:修改密码, 2:找回密码, 3:注册帐号

### 错误码

错误码 | 说明
---- | ----
1 | 验证码发送过于频繁
2 | 验证码发送失败，请重试


## 1.5 注册用户修改用户名为手机号(认证)

```json
{
  "success": true,
  "data": {
    "api_token": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpZCI6MTYxLCJ1c2VybmFtZSI6IjEzODg0NjMzMzk2IiwiZGV2aWNlX2lkIjoxNjF9.GgoijsH0qmyMWQrg_s_EXcZsXcoJi1U7qsBUfncQL4U"
  }
}
```

### HTTP请求

`POST /api/client/v2/users/update_username`

### 请求参数

参数 | 类型 | 必须 | 说明
---- | ---- | ---- | ----
area_code | integer | true | 区号
telephone | string | true | 手机号
verification_code | string | true | 短信验证码

### 返回信息

字段名 | 说明
---- | ----
api_token | 修改用户名后重新生成的token(因为token里包含用户名，所以需要重新生成)

## 1.9 提交用户分享日志(认证)

本接口用于在用户分享app后，记录用户的分享操作。

```json
{
  "success": true,
  "data": {}
}
```

### HTTP请求

`POST /api/client/v2/users/share`

### 请求参数

参数 | 类型 | 必须 | 说明
---- | ---- | ---- | ----
app_channel | string | true | app渠道, 如: moren
app_version | string | true | app版本名, 如: jichu
app_version_number | string | true | app版本号, 如: 0.1
platform_name | string | true | 分享平台名， 如: youtube
share_type | integer | true | 分享类型: 0:微信文字, 1:朋友圈文字, 2:QQ文字, 3:微博文字，4: 复制链接，5: 微信图片, 6:朋友圈图片, 7:QQ图片, 8:微博图片


## 1.44 获得网站导航列表(认证)

登录接口返回的 `settings` 节点中的 `SHOW_NAVIGATION` 配置用于决定是否打开导航功能，`true`为打开, `false`为关闭。

接口数据按照客户端导航标签顺序排序

> 如果客户端传的网址导航信息更新时间与后台时间一致，返回的数据如下:

```json
{
    "success": true,
    "message": null,
    "data": {
        "navigation_config_update_at": 1550739222,
        "hit_cache": true
    }
}
```

> 其他情况，则返回:

```json
{
    "success": true,
    "message": null,
    "data": {
        "navigation_config_update_at": 1550739222,
        "hit_cache": false,
        "tabs": [
            {
                "tab": 2,
                "websites": [
                    {
                        "id": 60,
                        "website_type": 2,
                        "name": "狸猫加速器官网",
                        "url": "https://www.ailimaovpn7.com/",
                        "icon_url": "https://www.ailimaovpn7.com/navigation/limao.png",
                        "nav_type": 2,
                        "info_type": 2,
                        "description": "全新产品，智能加速，无限流量",
                        "updated_at": 1550725020
                    },
                    {
                        "id": 61,
                        "website_type": 2,
                        "name": "Google",
                        "url": "https://www.google.com/",
                        "icon_url": "https://www.ailimaovpn7.com/navigation/google.png",
                        "nav_type": 2,
                        "info_type": 1,
                        "description": "互联网搜索引擎，提供各种资源搜索",
                        "updated_at": 1550739173
                    }
                ]
            },
            {
                "tab": 1,
                "websites": [
                    {
                        "id": 62,
                        "website_type": 1,
                        "name": "YouTube",
                        "url": "https://www.youtube.com/",
                        "icon_url": "https://www.ailimaovpn7.com/navigation/youtube.png",
                        "nav_type": 1,
                        "info_type": 1,
                        "description": "著名的视频分享网站",
                        "updated_at": 1531127754
                    },
                    {
                        "id": 63,
                        "website_type": 1,
                        "name": "Facebook",
                        "url": "https://www.facebook.com/",
                        "icon_url": "https://www.ailimaovpn7.com/navigation/facebook.png",
                        "nav_type": 1,
                        "info_type": 1,
                        "description": "社交网络服务网站",
                        "updated_at": 1531127759
                    },
                    {
                        "id": 64,
                        "website_type": 1,
                        "name": "Twitter",
                        "url": "https://twitter.com/",
                        "icon_url": "https://www.ailimaovpn7.com/navigation/twitter.png",
                        "nav_type": 1,
                        "info_type": 1,
                        "description": "社交网络及微博客服务网站",
                        "updated_at": 1531127762
                    },
                    {
                        "id": 65,
                        "website_type": 1,
                        "name": "Instagram",
                        "url": "https://www.instagram.com/",
                        "icon_url": "https://www.ailimaovpn7.com/navigation/instagram.png",
                        "nav_type": 1,
                        "info_type": 1,
                        "description": "照片保存和分享软件",
                        "updated_at": 1531127767
                    },
                    {
                        "id": 66,
                        "website_type": 1,
                        "name": "Telegram",
                        "url": "https://telegram.org/",
                        "icon_url": "https://www.ailimaovpn7.com/navigation/telegram.png",
                        "nav_type": 1,
                        "info_type": 1,
                        "description": "主打隐私安全的通信软件",
                        "updated_at": 1535624590
                    }
                ]
            }
        ]
    }
}
```


### HTTP请求

`GET /api/client/v2/websites`

### 请求参数

参数 | 类型 | 必须 | 说明
---- | ---- | ---- | ----
update_at | integer | false | 网址导航信息更新时间

### 返回信息

字段名 | 说明
---- | ----
navigation_config_update_at | 网址导航信息更新时间
hit_cache | 是否命中缓存，如果为true则不返回导航信息列表，为false返回全部数据
tabs.tab | 客户端导航标签顺序, 1:推荐网站, 2:推荐应用
tabs.websites.id | 网站ID
tabs.websites.website_type | 网站标识, 1:推荐, 2:火热
tabs.websites.name | 网站名称
tabs.websites.url | 网站URL
tabs.websites.icon_url | 网站图标URL
tabs.websites.description | 网站描述
tabs.websites.nav_type | 导航分类, 1:推荐网站, 2:推荐应用
tabs.websites.info_type | 信息分类, 1:导航类, 2:广告类
tabs.websites.updated_at | 网站最后更新时间


## 1.21 获得公告数据

获得最新的一条公告内容,包含（用户公告、即时弹窗、跑马灯）三种公告类型信息返回

如果客户端传递了用户账号，后台匹配上该记录将会返回指定账号公告内容

返回结果notice: 用户公告/ marquee: 跑马灯/ alert: 即时弹窗

注意:
 - 如果后台管理没有配置对应类型合适的公告显示，`notice`，`marquee`，`alert`任一类型可能为`null`

> 没有公告的情况

```json
{
    "success": true,
    "message": null,
    "data": {
        "notice": null,
        "marquee": null,
        "alert": null
    }
}
```

> 有公告的情况

```json
{
    "success": true,
    "message": null,
    "data": {
        "notice": [
            {
                "notice_type": 1,
                "open_type": 2,
                "button_text": "打开官网",
                "link_url": "https://nutsvpn.co",
                "alert_text": "这是notice的弹窗文字",
                "marquee_text": null
            }
        ],
        "marquee": [
            {
                "notice_type": 2,
                "open_type": 3,
                "button_text": "打开官网",
                "link_url": "https://nutsvpn.co",
                "alert_text": "这是marquee的弹窗文字",
                "marquee_text": "这是跑马灯文字"
            }
        ],
        "alert": [
            {
                "notice_type": 3,
                "open_type": 1,
                "button_text": "打开官网",
                "link_url": "https://nutsvpn.co",
                "alert_text": "这是alert的弹窗文字",
                "marquee_text": null
            }
        ]
    }
}
```

### HTTP请求

`GET /api/client/v2/notices`

### 请求参数

参数 | 类型 | 必须 | 说明
---- | ---- | ---- | ----
username | string | false | 用户账号（如：sup000001）
platform | string | true | 平台（如：ios）
app_channel | string | true | app渠道（如： appstore）
app_version | string | true | app版本（如： moren）

### 返回信息

字段名 | 说明
---- | ----
notice_type | 消息类型 1: 公告/2: 跑马灯/3: 即时弹窗
open_type | 打开方式 1: 无跳转/2: webview/3: 浏览器/4: 应用内跳转 
button_text | 跳转按钮文字
link_url | 跳转url
alert_text | 弹窗文字
marquee_text | 跑马灯文字

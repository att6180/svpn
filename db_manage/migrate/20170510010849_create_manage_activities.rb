class CreateManageActivities < ActiveRecord::Migration[5.0]
  def change
    create_table :manage_activities do |t|
      t.integer :admin_id
      t.string :key
      t.text :parameters
      t.string :recipientable_type
      t.string :recipientable_id
      t.timestamps
    end
    add_index :manage_activities, [:recipientable_type, :recipientable_id], name: 'activities_recipientable_type_id'
    add_index :manage_activities, :created_at
  end
end

class CreateAdmins < ActiveRecord::Migration[5.0]
  def change
    create_table :admins do |t|
      t.integer :admin_group_id
      t.string :username, null: false
      t.string :password_digest, null: false
      t.string :api_token
      t.boolean :is_enabled, default: true
      t.integer :signin_count, default: 0
      t.datetime :current_signin_at
      t.string :current_signin_ip
      t.datetime :last_signin_at
      t.string :last_signin_ip
      t.timestamps
    end
    add_index :admins, :username, unique: true
    add_index :admins, :admin_group_id
  end
end

class CreateManageRoles < ActiveRecord::Migration[5.0]
  def change
    create_table :manage_roles do |t|
      t.string :name
      t.boolean :manage_role_create, default: false
      t.boolean :manage_role_update, default: false
      t.boolean :manage_role_delete, default: false
      t.boolean :admin_password_update, default: false
      t.boolean :admin_role_update, default: false
      t.boolean :user_info_update, default: false
      t.boolean :user_base_info, default: false
      t.boolean :user_behaviour, default: false
      t.boolean :operation_behaviour, default: false
      t.boolean :charge_manage, default: false
      t.boolean :back_stage_manage, default: false
      t.boolean :user_manage, default: false
      t.boolean :node_manage, default: false
      t.boolean :system_setting, default: false
      t.boolean :system_enum, default: false
      t.timestamps
    end

    add_index :manage_roles, :name, unique: true

    rename_table :admins, :manage_admins

    remove_column :manage_admins, :admin_group_id
    add_column :manage_admins, :role_id, :integer
  end
end

class AddHelpManualToManageRoles < ActiveRecord::Migration[5.0]
  def change
    add_column :manage_roles, :help_manual, :boolean, default: false
    add_column :manage_roles, :service_setting, :boolean, default: false
  end
end

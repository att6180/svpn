# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20170609021003) do

  create_table "manage_activities", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb4" do |t|
    t.integer  "admin_id"
    t.string   "key"
    t.text     "parameters",         limit: 65535
    t.string   "recipientable_type"
    t.string   "recipientable_id"
    t.datetime "created_at",                       null: false
    t.datetime "updated_at",                       null: false
    t.index ["created_at"], name: "index_manage_activities_on_created_at", using: :btree
    t.index ["recipientable_type", "recipientable_id"], name: "activities_recipientable_type_id", using: :btree
  end

  create_table "manage_admins", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb4" do |t|
    t.string   "username",                         null: false
    t.string   "password_digest",                  null: false
    t.string   "api_token"
    t.boolean  "is_enabled",        default: true
    t.integer  "signin_count",      default: 0
    t.datetime "current_signin_at"
    t.string   "current_signin_ip"
    t.datetime "last_signin_at"
    t.string   "last_signin_ip"
    t.datetime "created_at",                       null: false
    t.datetime "updated_at",                       null: false
    t.integer  "role_id"
    t.index ["username"], name: "index_manage_admins_on_username", unique: true, using: :btree
  end

  create_table "manage_roles", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb4" do |t|
    t.string   "name"
    t.boolean  "manage_role_create",    default: false
    t.boolean  "manage_role_update",    default: false
    t.boolean  "manage_role_delete",    default: false
    t.boolean  "admin_password_update", default: false
    t.boolean  "admin_role_update",     default: false
    t.boolean  "user_info_update",      default: false
    t.boolean  "user_base_info",        default: false
    t.boolean  "user_behaviour",        default: false
    t.boolean  "operation_behaviour",   default: false
    t.boolean  "charge_manage",         default: false
    t.boolean  "back_stage_manage",     default: false
    t.boolean  "user_manage",           default: false
    t.boolean  "node_manage",           default: false
    t.boolean  "system_setting",        default: false
    t.boolean  "system_enum",           default: false
    t.datetime "created_at",                            null: false
    t.datetime "updated_at",                            null: false
    t.boolean  "help_manual",           default: false
    t.boolean  "service_setting",       default: false
    t.index ["name"], name: "index_manage_roles_on_name", unique: true, using: :btree
  end

end

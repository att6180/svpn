RUN  = docker-compose run app
RAKE = docker-compose run app bundle exec rake

PROJECT_IMAGE_NAME = "svpn"
MY_DOCKER_REGISTRY = "registry.dosjksds.com:5000"
MY_REGISTRY_USER = "jg"
MY_REGISTRY_PWD = "jkl__7890"

docker\:build:
	docker build . -t svpn/svpn:test
docker\:stop:
	docker-compose down
docker\:start:
	docker-compose up
docker\:shell:
	docker-compose run app /bin/sh --rm
docker\:test:
	@$(RUN) echo "${RAILS_ENV}"
docker\:console:
	@$(RUN) bundle exec rails console
svpn\:tags:
	@curl -u $(MY_REGISTRY_USER):$(MY_REGISTRY_PWD) https://$(MY_DOCKER_REGISTRY)/v2/$(PROJECT_IMAGE_NAME)/tags/list | jsonpp
